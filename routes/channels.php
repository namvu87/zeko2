<?php

use App\Models\Invoice;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('{groupId}.sale', function($user, $groupId) {
    return $user->hasRole($groupId . '.owner') ||
           $user->hasRole($groupId . '.waiters') ||
           $user->hasRole($groupId . '.admin') ||
           $user->hasRole($groupId . '.chef') ||
           $user->hasRole($groupId . '.cashier');
});
Broadcast::channel('invoice.{invoice}', function($user, Invoice $invoice) {
    $userIds = $invoice->user_ids ?? [];
    return in_array($user->id, $userIds);
});
Broadcast::channel('{invoiceId}.invoice_conversation', function($user, $invoiceId) {
    $invoice = Invoice::find($invoiceId);
    $groupId = $invoice->groupId;
    return in_array($user->id, $invoice->user_ids ?? []) ||
           $user->hasRole($groupId . '.owner') ||
           $user->hasRole($groupId . '.admin') ||
           $user->hasRole($groupId . '.waiters') ||
           $user->hasRole($groupId . '.cashier');
});
Broadcast::channel('requirement.{groupId}', function($user, $groupId) {
    return $user->can($groupId . '.requirement.list');
});
Broadcast::channel('notification.{userId}', function($user, $userId) {
    return $user->id == $userId;
});
