<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['middleware' => 'throttle:60,1'], function () {
    Route::middleware('auth:api')->get('/user', 'UserController@getUser');

    Route::get('change-language/{lang}', 'CommonController@apiChangeLanguage');
    Route::post('login', 'Auth\LoginController@login')->middleware('locale.manage');
    Route::post('register', 'Auth\RegisterController@register')->middleware('locale.manage');
    Route::post('logout', 'Auth\LoginController@logout')->middleware('auth:api');

    Route::get('captcha', 'CaptchaController@getCaptcha');

    Route::post('payment/momo/web-callback', 'Payment\WebMomoController@callback')->name('payment.momo.web_callback');
    Route::post('payment/momo/app-callback', 'Payment\AppMomoController@callback')->name('payment.momo.app_callback');

    Route::group(['middleware' => ['auth:api', 'locale.manage']], function () {
        Route::put('user/profile/update/overview', 'Profile\OverviewController@update');
        Route::put('user/profile/update/account', 'Profile\PasswordController@update');
        Route::post('user/profile/update/avatar', 'Profile\AvatarController@update');
        Route::post('user/profile/update/cover-image', 'Profile\CoverImageController@update');
        Route::get('user/groups-list', 'UserController@groupsList');
        Route::get('user/shifts-list', 'UserController@shiftsList');
    });

    Route::group(['middleware' => ['auth:api', 'verified', 'locale.manage']], function () {
        Route::get('company/change', 'GroupController@change');
        Route::get('areas', 'CommonController@getAreas');
        Route::get('areas/{areaId}', 'CommonController@getAreasAndCommuneByAreaById');
        Route::get('communes/{areaId}', 'CommonController@getCommunes');
        Route::post('group/create', 'GroupController@create');
        Route::post('group/update/cover-image', 'GroupController@updateCoverPicture');
        Route::put('firebase/push', 'FirebaseController@push');


        Route::get('home/timekeeping', 'HomeController@timekeeping');
        Route::get('home/restaurant', 'HomeController@restaurant');
        Route::get('home/restaurant-today', 'HomeController@restaurantToday');

        Route::post('timekeeping/checkin', 'Company\CheckinController@timekeeping');
        Route::post('timekeeping/checkout', 'Company\CheckoutController@timekeeping');

        Route::group(['middleware' => 'permission'], function () {
            Route::get('permissions/roles', 'PermissionController@roles');
            Route::get('permissions/roles-permissions', 'PermissionController@rolesPermissions');
            Route::put('permissions/roles-revoke', 'PermissionController@revokeRole');
            Route::put('permissions/roles-assign', 'PermissionController@assignRole');
            Route::put('permissions/update-role-permissions', 'PermissionController@updateRolePermissions');

            Route::put('group/update', 'GroupController@update');
            Route::put('group/update/delete-property', 'GroupController@deleteProperty');
            Route::put('group/update/units', 'GroupController@updateUnits');

            Route::group(['prefix' => 'requirement/handle', 'middleware' => 'permission'], function () {
                Route::put('active/{id}', 'RequirementController@active');
                Route::put('reset', 'RequirementController@reset');
                Route::put('join-group/{id}', 'RequirementController@joinGroup');
                Route::put('timekeeping-addition/{id}/accept', 'RequirementController@acceptAdditionalTimekeeping');
                Route::put('timekeeping-addition/{id}/deny', 'RequirementController@denyAdditionalTimekeeping');
                Route::put('take-leave/{id}/accept', 'RequirementController@acceptTakeLeave');
                Route::put('take-leave/{id}/deny', 'RequirementController@denyTakeLeave');
                Route::put('switch-shift/{id}/accept', 'RequirementController@acceptSwitchShift');
                Route::put('switch-shift/{id}/deny', 'RequirementController@denySwitchShift');
            });
        });

        Route::get('requirement/list', 'RequirementController@index');
        Route::get('requirement/detail/{id}', 'RequirementController@detail');
        Route::get('requirement/timekeeping/{id}', 'RequirementController@getRequirementTimekeeping');

        Route::post('import/user-into-group', 'ImportController@importUserIntoGroup');

        Route::group(['prefix' => 'notification'], function () {
            Route::get('list', 'NotificationController@getUserNotifications');
            Route::put('active/{id}', 'NotificationController@active');
            Route::put('reset', 'NotificationController@reset');
            Route::get('detail/{id}', 'NotificationController@detail');
        });

        Route::group(['namespace' => 'Company'], function () {
            Route::group(['middleware' => 'permission'], function () {
                Route::get('employee/list', 'UserController@list');
                Route::get('employee/detail/{id}', 'UserController@show');
                Route::put('employee/add', 'UserController@add');
                Route::put('employee/pull', 'UserController@pull');

                Route::get('shift/list', 'ShiftController@list');
                Route::post('shift/store', 'ShiftController@store');
                Route::get('shift/edit/{id}', 'ShiftController@edit');
                Route::put('shift/update/{id}', 'ShiftController@update');
                Route::delete('shift/delete/{id}', 'ShiftController@delete');

                Route::get('shift/user-list/{shiftId}', 'ShiftController@listUsers');
                Route::get('shift/user-add/{shiftId}', 'ShiftController@userAdd');
                Route::put('shift/user-add/{shiftId}', 'ShiftController@userStore');
                Route::put('shift/user-add/many/{shiftId}', 'ShiftController@usersStore');
                Route::delete('shift/user-delete/{shiftId}', 'ShiftController@userDelete');

                Route::get('timekeeping/list', 'TimekeepingController@list');
                Route::get('timekeeping/detail/{id}', 'TimekeepingController@detail');
                Route::get('report/timekeeping-group', 'TimekeepingController@report');
                Route::get('report/timekeeping-employee/{userId}', 'TimekeepingController@reportDetail');
            });

            Route::get('employee/search', 'UserController@search');
            Route::get('employee/search-by-phone', 'UserController@getByPhoneNumber');
            Route::get('employee/search-role-out', 'UserController@roleOut');


            Route::get('shift/user-search/{shiftId}', 'ShiftController@userSearch');
            Route::get('shift/user-add-search/{shiftId}', 'ShiftController@searchUsersWithoutShift');
        });
        Route::group(['namespace' => 'Restaurant'], function () {
            Route::group(['middleware' => 'permission'], function () {
                Route::get('place/list', 'PlaceController@list');
                Route::post('place/store', 'PlaceController@store');
                Route::post('place/store/import', 'PlaceController@import');
                Route::put('place/update/{placeId}', 'PlaceController@update');
                Route::delete('place/delete/{placeId}', 'PlaceController@delete');

                Route::get('table/list', 'TableController@list');
                Route::post('table/store', 'TableController@store');
                Route::post('table/store/import', 'TableController@import');
                Route::put('table/update/{tableId}', 'TableController@update');
                Route::delete('table/delete/{tableId}', 'TableController@delete');

                //Group menu
                Route::get('group-menus/list', 'GroupMenuController@list');
                Route::post('group-menus/store', 'GroupMenuController@store');
                Route::put('group-menus/update/{groupMenuId}', 'GroupMenuController@update');
                Route::delete('group-menus/delete/{groupMenuId}', 'GroupMenuController@delete');

                //Goods
                Route::get('goods/list/imported', 'GoodController@listImported');
                Route::post('goods/create/imported-good', 'ImportedGoodController@store');
                Route::post('goods/create/processed-good', 'ProcessedGoodController@store');
                Route::post('goods/create/import', 'GoodController@importGoods');
                Route::put('goods/update/images', 'GoodController@updateImage');
                Route::put('goods/update/add-menus', 'GoodController@addMenus');

                Route::put('invoice/disable/{invoiceId}', 'InvoiceController@disable');
                Route::get('invoice/list', 'InvoiceController@index');
                Route::get('invoice/list/report', 'InvoiceController@listInTime');

                Route::put('returned-invoice/disable/{invoiceId}', 'ReturnedInvoiceController@disable');
                Route::get('returned-invoice/list', 'ReturnedInvoiceController@index');

                Route::get('imported-invoice/list', 'ImportInvoiceController@list');
                Route::post('imported-invoice/create', 'ImportInvoiceController@store');
                Route::put('imported-invoice/pay/{id}', 'ImportInvoiceController@pay');

                Route::get('report/sale', 'ReportController@sale');
                Route::get('report/goods/imported', 'ReportController@goodsImported');
                Route::get('report/goods', 'ReportController@goods');

                Route::get('form-invoice/list', 'InvoiceFormController@list');
                Route::get('form-invoice/list/type', 'InvoiceFormController@listByType');
                Route::post('form-invoice/create', 'InvoiceFormController@store');
                Route::put('form-invoice/update/{invoiceFormId}', 'InvoiceFormController@update');
                Route::delete('form-invoice/delete/{invoiceFormId}', 'InvoiceFormController@delete');
            });

            Route::get('invoice/detail/{invoiceId}', 'InvoiceController@detail');
            Route::get('returned-invoice/detail/{invoiceId}', 'ReturnedInvoiceController@detail');
            Route::get('goods/search-import-invoice', 'GoodController@getGoodsToImportInvoice');

            Route::get('table/search', 'TableController@search');
            Route::get('place/edit/{placeId}', 'PlaceController@edit');
            Route::get('table/edit/{tableId}', 'TableController@edit');
            Route::get('group-menus/create', 'GroupMenuController@create');
            Route::get('group-menus/edit/{groupMenuId}', 'GroupMenuController@edit');
            Route::get('goods/generate-code', 'GoodController@generateCode');
            Route::get('goods/detail/{goodId}', 'GoodController@detail');
            Route::get('goods/create', 'GoodController@create');

            Route::group(['middleware' => 'permission', 'namespace' => 'ManageSale'], function () {
                Route::get('manage-sale/sale/group-menus-and-goods', 'GeneralController@getGroupMenusAndGoods');
                Route::get('manage-sale/sale/places', 'PlaceController@getPlaces');
                Route::get('manage-sale/sale/places-with-tables', 'PlaceController@getPlacesWithCondition');
                Route::get('manage-sale/sale/employees', 'EmployeeController@getEmployees');
                Route::put('manage-sale/sale/good/set-status', 'GoodController@setStatus');
                Route::put('manage-sale/sale/invoice/change-status-good', 'InvoiceController@changeStatusGood');
                Route::get('manage-sale/sale/invoices-and-tables', 'InvoiceController@getInvoicesAndTables');
                Route::get('manage-sale/sale/invoices/all', 'InvoiceController@getListInvoices');
                Route::get('manage-sale/sale/invoice/get', 'InvoiceController@getInvoice');
                Route::get('manage-sale/sale/tables/all', 'TableController@getAll');
                // Nhân viên tạo hóa đơn mua hàng
                Route::post('manage-sale/sale/invoice/create-purchase-invoice', 'InvoiceController@createPurchaseInvoice');
                Route::put('manage-sale/sale/invoice/update-goods', 'InvoiceController@updateGoods');
                Route::put('manage-sale/sale/invoice/update-status-goods', 'InvoiceController@updateStatusGoods');
                Route::put('manage-sale/sale/invoice/delete-item', 'InvoiceController@deleteItem');
                Route::delete('manage-sale/sale/invoice/delete/{invoiceId}', 'InvoiceController@deleteInvoice');
                Route::put('manage-sale/sale/invoice/set-invoice-tables', 'InvoiceController@setInvoiceTables');
                Route::put('manage-sale/sale/invoice/set-note', 'InvoiceController@setNote');
                Route::put('manage-sale/sale/invoice/set-discount', 'InvoiceController@setDiscount');
                Route::put('manage-sale/sale/invoice/combine-tables', 'InvoiceController@combineTables');
                Route::put('manage-sale/sale/invoice/combine-invoices', 'InvoiceController@combineInvoices');
                Route::put('manage-sale/sale/invoice/checkout-purchase-invoice', 'InvoiceController@checkoutPurchaseInvoice');
                Route::post('manage-sale/sale/invoice/checkout-return-invoice', 'InvoiceController@checkoutReturnInvoice');
                Route::get('manage-sale/sale/invoice/get-purchased', 'InvoiceController@getPurchasedInvoices');
                Route::get('manage-sale/sale/invoice/search-purchased', 'InvoiceController@searchPurchasedInvoices');
                // Notify without exec database
                Route::post('manage-sale/sale/notify/accept', 'NotificationController@accept');
            });
            Route::post('goods/check-file', 'GoodController@checkFile');
        });

        // Goods mobile for post methods attach image
        Route::group(['namespace' => 'Mobile', 'middleware' => 'permission'], function () {
            Route::post('goods/update/image/mobile', 'GoodController@updateImage');
            Route::post('goods/create/imported/mobile', 'ImportedGoodController@store');
            Route::post('goods/create/processed/mobile', 'ProcessedGoodController@store');
        });

        // Người dùng tạo hóa đơn mua hàng
        Route::post('manage-sale/sale/invoice/user-create-purchase-invoice', 'Restaurant\ManageSale\InvoiceController@userCreatePurchaseInvoice');
        // Người dùng yêu cầu gọi món
        Route::post('manage-sale/sale/invoice/user-require-order-goods', 'Restaurant\ManageSale\InvoiceController@userRequireOrderGoods');
        // Người dùng yêu cầu thanh toán
        Route::post('manage-sale/sale/invoice/user-checkout-purchase-invoice', 'Restaurant\ManageSale\InvoiceController@userRequireCheckoutPurchaseInvoice');

        Route::get('around', 'SearchController@findAround');
    });
});

Route::post('email/resend', 'Auth\VerificationController@resend');
Route::post('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::post('phone-number/verification/init', 'Auth\PhoneNumber\VerificationController@init');
