<?php

Route::group(['namespace' => 'Master', 'middleware' => ['auth:api', 'master']], function() {
    Route::get('home', 'HomeController@getData');

    Route::get('slider/list', 'SliderController@list');
    Route::post('slider/store', 'SliderController@store');
    Route::get('slider/edit/{id}', 'SliderController@edit');
    Route::put('slider/update/{id}', 'SliderController@update');
    Route::put('slider/change-status/{id}', 'SliderController@changeStatus');
    Route::delete('slider/delete/{id}', 'SliderController@delete');

    Route::get('tag/list/{statusTag?}', 'TagController@list');
    Route::post('tag/store', 'TagController@store');
    Route::get('tag/edit/{id}', 'TagController@edit');
    Route::put('tag/update/{id}', 'TagController@update');
    Route::put('tag/change-status', 'TagController@changeStatus');
    Route::delete('tag/delete/{id}', 'TagController@delete');
    Route::put('tag/restore/{id}', 'TagController@restore');

    Route::get('news/list', 'NewsController@list');
    Route::get('news/create', 'NewsController@create');
    Route::post('news/store', 'NewsController@store');
    Route::get('news/edit/{id}', 'NewsController@edit');
    Route::put('news/update/{id}', 'NewsController@update');
    Route::put('news/change-status', 'NewsController@changeStatus');
    Route::delete('news/delete/{id}', 'NewsController@delete');
    Route::put('news/restore/{id}', 'NewsController@restore');

    Route::get('guide/list', 'UserGuideController@list');
    Route::get('guide/create', 'UserGuideController@create');
    Route::post('guide/store', 'UserGuideController@store');
    Route::get('guide/edit/{id}', 'UserGuideController@edit');
    Route::put('guide/update/{id}', 'UserGuideController@update');
    Route::delete('guide/delete/{id}', 'UserGuideController@destroy');
    Route::put('guide/restore/{id}', 'UserGuideController@restore');

    Route::get('product/list', 'ProductController@list');
    Route::post('product/store', 'ProductController@store');
    Route::get('product/edit/{id}', 'ProductController@edit');
    Route::put('product/update/{id}', 'ProductController@update');
    Route::delete('product/delete/{id}', 'ProductController@delete');

    Route::get('customer-care/list', 'ContactController@listCustomerCare');
    Route::post('customer-care/store', 'ContactController@storeCustomerCare');
    Route::delete('customer-care/delete/{id}', 'ContactController@deleteCustomerCare');
    Route::get('customer-care/edit/{id}', 'ContactController@editCustomerCare');
    Route::put('customer-care/update/{id}', 'ContactController@updateCustomerCare');

    Route::get('contact', 'ContactController@getContactInformation');
    Route::post('contact/store', 'ContactController@setContactInformation');

    Route::get('group/list', 'GroupController@list');
    Route::get('group/edit/{id}', 'GroupController@edit');
    Route::put('group/update/{id}', 'GroupController@update');

    Route::get('images/list', 'ImageManagerController@list');
    Route::post('images/upload', 'ImageManagerController@upload');
    Route::delete('images/delete', 'ImageManagerController@delete');
});