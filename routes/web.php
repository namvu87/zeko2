<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::options('reset-password', function() {})->name('password.reset');

Route::get('horizon-login', 'HorizonController@showLoginForm');
Route::post('horizon-login', 'HorizonController@login')->name('horizon.login');
Route::post('horizon-logout', 'HorizonController@logout')->name('horizon.logout');
Route::get('horizon-home', 'HorizonController@home');

Route::get('social/google', 'Auth\OauthGoogleController@redirect');
Route::get('social/google/callback', 'Auth\OauthGoogleController@callback');
Route::get('social/facebook', 'Auth\OauthFacebookController@redirect');
Route::get('social/facebook/callback', 'Auth\OauthFacebookController@callback');

Route::get('page-not-found', function() {
    return view('errors.404');
});
Route::any('{all}', function() {
    return view('manager.entry');
})->where(['all' => '.*']);
