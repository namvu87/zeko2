<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Interfaces\Notifiable;
use App\Models\User;

class MemberShipChanged implements Notifiable
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $groupId;
    public $creator;
    public $userId;
    public $type;
    
    /**
     * Create a new event instance.
     * Nguời dùng được thêm vào nhóm, xoá khỏi nhóm, gán quyền, thu hồi quyền
     *
     * @return void
     */
    public function __construct(User $creator, string $groupId, string $userId, int $type)
    {
        $this->groupId = $groupId;
        $this->userId = $userId;
        $this->creator = $creator;
        $this->type = $type;
    }
}
