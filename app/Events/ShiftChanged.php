<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Models\User;
use App\Models\Shift;
use App\Interfaces\Notifiable;

class ShiftChanged implements Notifiable
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $creator;
    public $shift;
    public $groupId;
    public $type;
    public $userId;
    public $userIds;
    
    /**
     * Create a new event instance.
     * Nguời dùng được thêm vào nhóm, xoá khỏi nhóm, gán quyền, thu hồi quyền
     *
     * @return void
     */
    public function __construct(
        User $creator,
        Shift $shift,
        string $groupId,
        int $type,
        ?string $userId = '',
        ?array $userIds = []
    )
    {
        $this->creator = $creator;
        $this->shift = $shift;
        $this->groupId = $groupId;
        $this->type = $type;
        $this->userId = $userId;
        $this->userIds = $userIds;
    }
}
