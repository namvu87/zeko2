<?php

namespace App\Events\Restaurant;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Invoice;
use App\Models\User;
use App\Http\Resources\Invoice as InvoiceResource;
use App\Interfaces\Notifiable;

class RequestAccepted implements ShouldBroadcast, Notifiable
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $invoice;
    public $creator;
    public $fromType;
    public $exceptToken;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        Invoice $invoice,
        User $creator,
        int $fromType,
        ?string $exceptToken = ''
    )
    {
        $this->invoice = $invoice;
        $this->creator = $creator;
        $this->fromType = $fromType;
        $this->exceptToken = $exceptToken;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel($this->invoice->group_id . '.sale');
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'invoice'   => new InvoiceResource($this->invoice),
            'creator'     => [
                'fullname' => $this->creator->fullname,
                'avatar'  => $this->creator->avatars ? $this->creator->avatars['x2'] : null
            ],
            'from_type'    => self::FROM_EMPLOYEE_TYPE,
            'type'         => self::TYPE_53
        ];
    }
}
