<?php

namespace App\Events\Restaurant;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ConversationUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $payload;
    public $exceptToken;
    private $invoiceId;
    public $userIds;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        string $invoiceId,
        array $payload,
        array $userIds = [],
        ?string $exceptToken = ''
    )
    {
        $this->invoiceId = $invoiceId;
        $this->payload = $payload;
        $this->userIds = $userIds;
        $this->exceptToken = $exceptToken;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel($this->invoiceId . '.invoice_conversation');
    }
}
