<?php

namespace App\Events\Restaurant;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Invoice;
use App\Models\User;
use App\Interfaces\Notifiable;

class InvoiceDestroyed implements ShouldBroadcast, Notifiable
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $invoice;
    public $creator;
    public $type;
    public $exceptToken;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        Invoice $invoice,
        User $creator,
        int $type,
        ?string $exceptToken = ''
    )
    {
        $this->invoice = $invoice;
        $this->creator = $creator;
        $this->type = $type;
        $this->exceptToken = $exceptToken;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel($this->invoice->group_id . '.sale');
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'invoice_id'   => $this->invoice->id,
            'invoice_code' => $this->invoice->code,
            'table_ids'    => $this->invoice->table_ids ?? [],
            'creator'      => [
                'fullname' => $this->creator->fullname,
                'avatar' => $this->creator->avatars ? $this->creator->avatars['x2'] : null
            ],
            'type'         => $this->type,
            'from_type'    => self::FROM_EMPLOYEE_TYPE
        ];
    }
}
