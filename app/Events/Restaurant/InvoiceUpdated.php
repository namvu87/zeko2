<?php

namespace App\Events\Restaurant;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Invoice;
use App\Models\User;
use App\Http\Resources\Invoice as InvoiceResource;
use App\Interfaces\Notifiable;

class InvoiceUpdated implements ShouldBroadcast, Notifiable
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $invoice;
    public $type;
    public $fromType;
    public $creator;
    public $exceptToken;
    public $invoiceIds;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        Invoice $invoice,
        User $creator,
        int $type,
        int $fromType,
        ?string $exceptToken = '',
        ?array $invoiceIds = []
    )
    {
        $this->invoice = $invoice;
        $this->creator = $creator;
        $this->type = $type;
        $this->fromType = $fromType;
        $this->exceptToken = $exceptToken;
        $this->invoiceIds = $invoiceIds;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel($this->invoice->group_id . '.sale');
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'invoice'     => new InvoiceResource($this->invoice),
            'type'        => $this->type,
            'from_type'   => $this->fromType,
            'creator'     => [
                'fullname' => $this->creator->fullname,
                'avatar'  => $this->creator->avatars ? $this->creator->avatars['x2'] : null
            ],
            'invoice_ids' => $this->invoiceIds
        ];
    }
}
