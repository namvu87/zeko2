<?php

namespace App\Events\Restaurant;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Models\Invoice;
use App\Models\User;

class InvoiceMemberAdded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $invoice;
    public $creator;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, User $creator)
    {
        $this->invoice = $invoice;
        $this->creator = $creator;
    }
}
