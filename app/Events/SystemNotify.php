<?php

namespace App\Events;

use App\Interfaces\Notifiable;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Group;

class SystemNotify implements Notifiable
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $group;
    public $userIds;
    public $type;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Group $group, array $userIds, int $type)
    {
        $this->group = $group;
        $this->userIds = $userIds;
        $this->type = $type;
    }
}
