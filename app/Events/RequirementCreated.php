<?php

namespace App\Events;

use App\Models\Requirement;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Http\Resources\Requirement as RequirementResource;

class RequirementCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $requirement;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Requirement $requirement)
    {
        $this->requirement = $requirement->load(['creator:first_name,last_name,avatars']);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("requirement.{$this->requirement->group_id}");
    }

    public function broadcastWith()
    {
        return [
          'requirement' => new RequirementResource($this->requirement)
        ];
    }
}
