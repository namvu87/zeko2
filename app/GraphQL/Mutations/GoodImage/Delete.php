<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\GoodImage;

use App\Contracts\GoodImageContract;
use App\Contracts\Restaurant\GoodContract;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Error\AuthorizationError;
use Rebing\GraphQL\Support\Mutation;
use GraphQL;

class Delete extends Mutation
{
    protected $attributes = [
        'name'        => 'delete',
        'description' => 'A mutation'
    ];

    private $image;
    private $good;

    public function __construct(GoodImageContract $image, GoodContract $good)
    {
        $this->image = $image;
        $this->good = $good;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $image = $this->image->getById($args['id']);
        $count = $this->good->countByUrlImages($image->urls ?? []);
        if ($count) return new AuthorizationError('Thư mục chứa ảnh đang sử dụng. Không thể xóa!');
        $image->delete();
        foreach ($image->urls as $url) {
            $this->image->removeImage($url);
        }
        return [
            'code' => 200
        ];
    }
}
