<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\GoodImage;

use App\Contracts\GoodImageContract;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use GraphQL;

class Create extends Mutation
{
    protected $attributes = [
        'name'        => 'Create',
        'description' => 'A mutation'
    ];

    private $image;

    public function __construct(GoodImageContract $image) {
        $this->image = $image;
    }

    public function type(): Type
    {
        return GraphQL::type('good_image');
    }

    public function rules(array $args = []): array
    {
        return [
            'images' => 'required|array',
            'images.*' => 'required|image'
        ];
    }

    public function args(): array
    {
        return [
            'name' => [
                'type' => (Type::string())
            ],
            'images' => [
                'type' => Type::listOf(GraphQL::type('Upload'))
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $goodImage = $this->image->getByName($args['name']);
        $images = $this->image->saveImages($args['images']);
        if ($goodImage) return $this->image->save($goodImage, [
            'urls' => array_merge($goodImage->urls ?? [], $images)
        ]);
        return $this->image->create([
           'name' => $args['name'],
           'urls' => $images
        ]);
    }
}
