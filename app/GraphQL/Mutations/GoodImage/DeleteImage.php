<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\GoodImage;

use App\Contracts\GoodImageContract;
use App\Contracts\Restaurant\GoodContract;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Error\AuthorizationError;
use Rebing\GraphQL\Support\Mutation;
use GraphQL;

class DeleteImage extends Mutation
{
    protected $attributes = [
        'name'        => 'deleteImage',
        'description' => 'A mutation'
    ];

    private $image;
    private $good;

    public function __construct(GoodImageContract $image, GoodContract $good)
    {
        $this->image = $image;
        $this->good = $good;
    }

    public function type(): Type
    {
        return GraphQL::type('good_image');
    }

    public function args(): array
    {
        return [
            'id'  => [
                Type::nonNull(Type::string())
            ],
            'url' => [
                Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $image = $this->image->getById($args['id']);
        $count = $this->good->countByUrlImage($args['url']);
        if ($count) return new AuthorizationError('Ảnh đã được sử dụng. Không thể xóa!');
        $image->pull('urls', $args['url']);
        $this->image->removeImage($args['url']);
        return $image;
    }
}
