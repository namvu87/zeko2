<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\GoodImage;

use App\Contracts\GoodImageContract;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use GraphQL;

class AddGoodImages extends Mutation
{
    protected $attributes = [
        'name'        => 'addGoodImages',
        'description' => 'A mutation'
    ];

    private $image;

    public function __construct(GoodImageContract $image)
    {
        $this->image = $image;
    }

    public function type(): Type
    {
        return GraphQL::type('good_image');
    }

    public function rules(array $args = []): array
    {
        return [
            'images'   => 'required|array',
            'images.*' => 'image|max:2000'
        ];
    }

    public function args(): array
    {
        return [
            'id'     => [
                'type' => Type::nonNull(Type::string())
            ],
            'images' => [
                'type' => Type::listOf(GraphQL::type('Upload'))
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $image = $this->image->getById($args['id']);
        $urls = $this->image->saveImages($args['images']);
        $image->urls = array_merge($image->urls, $urls);
        $image->save();
        return $image;
    }
}
