<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\RoleContract;
use App\Contracts\Users\Role as UserRole;
use App\Contracts\UserContract;
use Rebing\GraphQL\Error\AuthorizationError;

class LeaveGroup extends Mutation
{
    protected $attributes = [
        'name' => 'LeaveGroup',
        'description' => 'Rời khỏi nhóm'
    ];

    const SUCCESS_STATUS = 200;
    private $role;
    private $userRole;
    private $user;
    
    public function __construct(RoleContract $role, UserRole $userRole, UserContract $user)
    {
        $this->role = $role;
        $this->userRole = $userRole;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $role = $this->role->getRoleUserByGroupId($context->id, $args['group_id']);
        if ($role && !$context->can('outOwner', $role)) {
            throw new AuthorizationError('unauthorized!');
        }
        if ($role) {
            $this->userRole->revoke($context, $role);
        }
        $this->user->removeUserFromGroup($context->id, $args['group_id']);

        return ['code' => self::SUCCESS_STATUS];
    }
}
