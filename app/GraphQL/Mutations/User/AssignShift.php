<?php
namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\UserContract;
use App\Contracts\ShiftContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\ShiftChanged;

class AssignShift extends Mutation
{
    use Authorize;

    const PERMISSION = ".shift.user-add";

    protected $attributes = [
        'name' => 'AddUserToShift'
    ];

    private $user;
    private $shift;

    public function __construct(UserContract $user, ShiftContract $shift)
    {
        $this->user = $user;
        $this->shift = $shift;
    }

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function args(): array
    {
        return [
            'user_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'shift_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $user = $this->user->getById($args['user_id']);
        $shift = $this->shift->getById($args['shift_id']);

        $this->verifyData($context, $user, $shift, $args['group_id']);
        $this->checkExistsShiftOfUserInGroup($user->shift_ids ?? [], $args['group_id']);

        $this->user->addUserToShift($args['user_id'], $args['shift_id']);

        event(new ShiftChanged(
            $context,
            $shift,
            $args['group_id'],
            ShiftChanged::TYPE_14,
            $args['user_id']
        ));

        return $user;
    }

    /**
     * Kiểm tra xem người dùng này đã có ca làm việc nào trong nhóm chưa
     *
     * @param array $shiftIds
     * @param string $groupId
     * @return void
     */
    private function checkExistsShiftOfUserInGroup(array $shiftIds, string $groupId)
    {
        $count = $this->shift->getByids($shiftIds)->where('group_id', $groupId)->count();
        if ($count > 0) {
            throw new AuthorizationError('Unauthorized');
        }
    }

    /**
     * Kiểm tra xem dữ liệu có tồn tại hay ko, user có thuộc về group,
     * shift có thuộc group hay ko
     *
     * @param App\Mdoels\User $user
     * @param App\Models\Shift $shift
     * @param string $groupId
     * @return void
     */
    private function verifyData($context, $user, $shift, string $groupId)
    {
        if (
            empty($shift) || empty($user) ||
            !$context->can('belongsToGroup', [$user, $groupId]) ||
            !$context->can('belongsToGroup', [$shift, $groupId])
        ) {
            throw new AuthorizationError('Unauthorized');
        }
    }
}