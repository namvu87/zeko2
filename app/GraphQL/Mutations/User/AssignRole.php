<?php
namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\UserContract;
use App\Contracts\RoleContract;
use App\Contracts\GroupContract;
use App\Contracts\Users\Role as UserRole;
use App\GraphQL\Traits\Authorize;
use App\Events\MemberShipChanged;

class AssignRole extends Mutation
{
    use Authorize;

    const PERMISSION = ".permission.role-assign";

    protected $attributes = [
        'name' => 'AssignRoleToUser'
    ];

    private $user;
    private $role;
    private $userRole;
    private $group;

    public function __construct(
        UserContract $user,
        RoleContract $role,
        UserRole $userRole,
        GroupContract $group
    )
    {
        $this->user = $user;
        $this->role = $role;
        $this->userRole = $userRole;
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('group');
    }

    public function rules(array $args = []): array
    {
        return [
            'role_id'  => 'required|string|exists:roles,_id',
            'user_id'  => 'required|string|exists:users,_id',
            'group_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'user_id' => [
                'type' => Type::string()
            ],
            'role_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    /**
     * Thêm mã thông báo firebase cho người dùng hiện tại
     *
     * Kiểm tra quyền có thuộc về nhóm chỉ định hay ko
     * Kiểm tra quyền có phải là quyền owner hay ko
     * Kiểm tra user này đã có quyền trong group chỉ định hay chưa
     *
     * @param  $root
     * @param  array $args
     * @return App\Models\Group
     */
    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $role = $this->role->getById($args['role_id']);
        $user = $this->user->getById($args['user_id']);

        $currentRole = $user->roles()->where('group_id', $args['group_id']);

        if ($currentRole->count() > 0 ||
            !$context->can('belongsToGroup', [$role, $args['group_id']])) {
            return null;
        }

        if (!$context->can('outOwner', $role)) {
            return null;
        }

        $this->userRole->assign($user, $role);

        event(new MemberShipChanged(
            $context,
            $args['group_id'],
            $args['user_id'],
            MemberShipChanged::TYPE_70
        ));
        
        return $this->group->getById($args['group_id']);
    }
}