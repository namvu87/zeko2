<?php
namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\UserContract;
use App\Contracts\RoleContract;
use App\Contracts\GroupContract;
use App\Contracts\Users\Role as UserRole;

class UpdateRole extends Mutation
{
    const PERMISSION = ".permission.role-assign";

    protected $attributes = [
        'name' => 'UpdateRoleOfUser'
    ];

    private $user;
    private $role;
    private $userRole;
    private $group;

    public function __construct(
        UserContract $user,
        RoleContract $role,
        UserRole $userRole,
        GroupContract $group
    )
    {
        $this->user = $user;
        $this->role = $role;
        $this->userRole = $userRole;
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('group');
    }

    public function rules(array $args = []): array
    {
        return [
            'role_id' => 'required|string|exists:roles,_id',
            'user_id'  => 'required|string|exists:users,_id',
            'group_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'user_id' => [
                'type' => Type::string()
            ],
            'role_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $role = $this->role->getById($args['role_id']);
        $user = $this->user->getById($args['user_id']);

        $currentRole = $user->roles->where('group_id', $args['group_id'])->first();

        // Kiểm tra user này đã được phân quyền mới được update
        // Kiểm tra quyền này phải thuộc về group đã chỉ định
        if ($currentRole->count() == 0 ||
            !$context->can('belongsToGroup', [$role, $args['group_id']])) {
            return null;
        }

        // Kiểm tra quyền được gán không phải là owner
        if (!$context->can('outOwner', $role)) {
            return null;
        }

        // Xoá vai trò hiện tại
        if (!$context->can('outOwner', $currentRole)) {
            return null;
        }

        $this->userRole->revoke($user, $currentRole);
        $this->userRole->assign($user, $role);

        return $this->group->getById($args['group_id']);
    }
}