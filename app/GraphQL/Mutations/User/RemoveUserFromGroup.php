<?php

namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\UserContract;
use App\Contracts\GroupContract;
use App\Contracts\RoleContract;
use App\Contracts\Users\Role as UserRoleable;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\MemberShipChanged;

class RemoveUserFromGroup extends Mutation
{
    use Authorize;

    const PERMISSION = ".employee.remove";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'RemoveUserFromGroup'
    ];

    private $user;
    private $role;
    private $userRole;
    private $group;

    public function __construct(UserContract $user,
                                RoleContract $role,
                                UserRoleable $userRole,
                                GroupContract $group)
    {
        $this->user = $user;
        $this->role = $role;
        $this->userRole = $userRole;
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'user_id'  => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'user_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $group = $this->group->getById($args['group_id']);
        $user = $this->user->getById($args['user_id']);

        if ($group->owner_id === $args['user_id'] || empty($user)) {
            return new AuthorizationError('Unauthorized');
        }
        
        // Xoá vai trò của user
        $role = $this->role->getRoleUserByGroupId($args['user_id'], $args['group_id']);
        if ($role) {
            $this->userRole->revoke($user, $role);
        }
        $this->user->removeUserFromGroup($args['user_id'], $args['group_id']);

        event(new MemberShipChanged(
            $context,
            $args['group_id'],
            $args['user_id'],
            MemberShipChanged::TYPE_11
        ));

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}