<?php
namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\UserContract;
use App\Contracts\RoleContract;
use App\Contracts\GroupContract;
use App\Contracts\Users\Role as UserRole;
use App\GraphQL\Traits\Authorize;
use App\Events\MemberShipChanged;
use Rebing\GraphQL\Error\AuthorizationError;

class RevokeRole extends Mutation
{
    use Authorize;

    const PERMISSION = ".permission.role-revoke";
    const SUCCESS_STATUS = 200;
    const UNAUTHORIZED_STATUS = 403;
    const ROLE_OUT_OWNER_STATUS = 402;

    protected $attributes = [
        'name' => 'RevokeRoleOfUser'
    ];

    private $user;
    private $role;
    private $userRole;
    private $group;

    public function __construct(UserContract $user, RoleContract $role, UserRole $userRole, GroupContract $group)
    {
        $this->user = $user;
        $this->role = $role;
        $this->userRole = $userRole;
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('group');
    }

    public function rules(array $args = []): array
    {
        return [
            'user_id'  => 'required|string|exists:users,_id',
            'group_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'user_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $user = $this->user->getById($args['user_id']);
        $currentRole = $user->roles->where('group_id', $args['group_id'])->first();

        // Kiểm tra user này đã được phân quyền mới được revoke
        if ($currentRole->count() == 0) {
            throw new AuthorizationError('Unauthorized');
        }

        // Xoá vai trò hiện tại
        if (!$context->can('outOwner', $currentRole)) {
            throw new AuthorizationError('Unauthorized');
        }

        $this->userRole->revoke($user, $currentRole);

        event(new MemberShipChanged(
            $context,
            $args['group_id'],
            $args['user_id'],
            MemberShipChanged::TYPE_71
        ));

        return $this->group->getById($args['group_id']);
    }
}