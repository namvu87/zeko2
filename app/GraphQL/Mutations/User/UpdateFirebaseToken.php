<?php
namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Users\Firebase as FirebaseContract;
use App\Contracts\UserContract;

class UpdateFirebaseToken extends Mutation
{
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'UpdateFirebaseToken'
    ];

    private $firebase;
    private $user;

    public function __construct(FirebaseContract $firebase, UserContract $user)
    {
        $this->firebase = $firebase;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function args(): array
    {
        return [
            'firebase_token' => [
                'type' => Type::string()
            ],
            'uuid_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        if ($args['firebase_token']) {
            $this->firebase->push($context, $args['firebase_token']);
        }
        if ($args['uuid_token']) {
            $userExist = $this->user->getByUuid($args['uuid_token']);
            if (empty($userExist)) {
                $this->firebase->pushUuid($context, $args['uuid_token']);
            }
        }

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}