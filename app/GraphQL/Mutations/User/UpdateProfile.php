<?php

namespace App\GraphQL\Mutations\User;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\UserContract;
use Illuminate\Validation\Rule;

class UpdateProfile extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateMyProfile'
    ];

    private $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function rules(array $args = []): array
    {
        return [
            'first_name' => 'required|string|max:64',
            'last_name'  => 'required|string|max:64',
            'birthday'   => 'required|date_format:Y-m-d',
            'gender'     => 'required|numeric|in:1,2,3',
            'address'    => 'required|string|max:255',
            'email'      => [
                'required', 'email',
                Rule::unique('users')->ignore(auth('api')->id(), '_id')
            ],
            'phone_number' => [
                'required', 'numeric',
                Rule::unique('users')->ignore(auth('api')->id(), '_id')
            ]
        ];
    }

    public function args(): array
    {
        return [
            'email' => [
                'type' => Type::string()
            ],
            'phone_number' => [
                'type' => Type::string()
            ],
            'first_name' => [
                'type' => Type::string()
            ],
            'last_name' => [
                'type' => Type::string()
            ],
            'birthday' => [
                'type' => Type::string()
            ],
            'gender' => [
                'type' => Type::int()
            ],
            'area' => [
                'type' => GraphQL::type('AreaInput')
            ],
            'commune' => [
                'type' => GraphQL::type('CommuneInput')
            ],
            'address' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $args['email'] = strtolower($args['email']);
        $this->user->update($context->id, $args);

        return $context->refresh();
    }
}
