<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\GraphQL\Traits\Authorize;
use App\Contracts\UserContract;
use App\Contracts\ShiftContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\ShiftChanged;

class MultiAssignShift extends Mutation
{
    use Authorize;

    const PERMISSION = ".shift.user-add";
    const SUCCESS_STATUS = 200;


    protected $attributes = [
        'name' => 'AssignMultiUsersShift',
        'description' => 'Gán nhiều users vào ca làm việc'
    ];

    private $user;
    private $shift;

    public function __construct(UserContract $user, ShiftContract $shift)
    {
        $this->user = $user;
        $this->shift = $shift;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function args(): array
    {
        return [
            'user_ids' => [
                'type' => Type::nonNull(Type::listOf(Type::string()))
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'shift_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $shift = $this->shift->getById($args['shift_id']);
        $users = $this->user->getByIds($args['user_ids']);

        $this->checkExistsShiftOfUserInGroup($users, $args['group_id']);
        $this->verifyShiftData($context, $shift, $args['group_id']);
        $this->verifyUsersData($context, $users, $args['group_id']);

        foreach ($users as $user) {
            $user->push('shift_ids', $args['shift_id'], true);
        }
        event(new ShiftChanged(
            $context,
            $shift,
            $args['group_id'],
            ShiftChanged::TYPE_14,
            null,
            $args['user_ids']
        ));
    }

    /**
     * Kiểm tra xem người dùng này đã có ca làm việc nào trong nhóm chưa
     *
     * @param array $users
     * @param string $groupId
     * @return void
     */
    private function checkExistsShiftOfUserInGroup($users, string $groupId)
    {
        foreach ($users as $user) {
            $count = $this->shift
                          ->getByids($user->shift_ids ?? [])
                          ->where('group_id', $groupId)
                          ->count();
            if ($count > 0) {
                throw new AuthorizationError(__('shift.user_already_have_shift'));
            }
        }
    }

    /**
     * Kiểm tra xem dữ liệu có tồn tại hay ko,
     * Shift có thuộc group hay ko
     *
     * @param App\Mdoels\User $user
     * @param App\Models\Shift $shift
     * @param string $groupId
     * @return void
     */
    private function verifyShiftData($context, $shift, string $groupId)
    {
        if (empty($shift) || !$context->can('belongsToGroup', [$shift, $groupId])) {
            throw new AuthorizationError('Unauthorized');
        }
    }

     /**
     * Kiểm tra xem dữ liệu có tồn tại hay ko,
     * users có thuộc group hay ko
     *
     * @param array [App\Mdoels\User] $users
     * @param string $groupId
     * @return void
     */
    private function verifyUsersData($context, $users, string $groupId)
    {
        foreach ($users as $user) {
            if (!$context->can('belongsToGroup', [$user, $groupId])) {
                throw new AuthorizationError('Unauthorized');
            }
        }
    }
}
