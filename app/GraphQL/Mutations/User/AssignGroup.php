<?php

namespace App\GraphQL\Mutations\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\UserContract;
use App\GraphQL\Traits\Authorize;
use App\Events\MemberShipChanged;

class AssignGroup extends Mutation
{
    use Authorize;

    const PERMISSION = ".employee.add";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'AddUserToGroup'
    ];

    private $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'user_id' => 'required|string|exists:users,_id'
        ];
    }

    public function args(): array
    {
        return [
            'user_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $this->user->addUserToGroup($args['user_id'], $args['group_id']);

        event(new MemberShipChanged(
            $context,
            $args['group_id'],
            $args['user_id'],
            MemberShipChanged::TYPE_10
        ));

        return ['code' => self::SUCCESS_STATUS];
    }
}
