<?php
namespace App\GraphQL\Mutations\User;

use GraphQL;
use Rebing\GraphQL\Support\UploadType;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Uploads\Avatar as AvatarUpload;

class UpdateAvatar extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateUserAvatar'
    ];

    private $avatar;

    public function __construct(AvatarUpload $avatar)
    {
        $this->avatar = $avatar;
    }

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function rules(array $args = []): array
    {
        return [
            'first_name' => 'required|string|max:64',
            'last_name'  => 'required|string|max:64',
            'birthday'   => 'required|date_format:d-m-Y',
            'gender'     => 'required|numeric|in:1,2,3',
            'address'    => 'required|string|max:255'
        ];
    }

    public function args(): array
    {
        return [
            'avatar' => [
                'type'  => UploadType::getInstance(),
                'rules' => ['required', 'image', 'max:5120']
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $data = [
            'avatars' => $this->avatar->upload($args['avatar'])
        ];

        return $this->user->save($context, $data);
    }
}