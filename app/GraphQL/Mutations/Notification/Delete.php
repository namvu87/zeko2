<?php

namespace App\GraphQL\Mutations\Notification;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\NotificationContract;
use Rebing\GraphQL\Error\AuthorizationError;

class Delete extends Mutation
{
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'DeleteNotification'
    ];

    private $notification;

    public function __construct(NotificationContract $notification)
    {
        $this->notification = $notification;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'notification_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'notification_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $notification = $this->notification->getById($args['notification_id']);

        if (!empty($notification->user_id)) {
            if ($notification->user_id !== $context->id) {
                throw new AuthorizationError('Unauthorized');
            }
            $this->notification->deleteInstance($notification);
        }

        if (!empty($notification->user_ids)) {
            if (!in_array($context->id, $notification->user_ids)) {
                throw new AuthorizationError('Unauthorized');
            } 
            if (count($notification->user_ids) > 1) {
                $notification->pull('user_ids', $context->id);
            } else {
                $this->notification->deleteInstance($notification);
            }
        }

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}