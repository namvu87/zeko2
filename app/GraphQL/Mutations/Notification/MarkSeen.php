<?php

namespace App\GraphQL\Mutations\Notification;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\NotificationContract;
use Rebing\GraphQL\Error\AuthorizationError;

class MarkSeen extends Mutation
{
    protected $attributes = [
        'name' => 'MarkSeenNotification'
    ];

    private $notification;

    public function __construct(NotificationContract $notification)
    {
        $this->notification = $notification;
    }

    public function type(): Type
    {
        return GraphQL::type('notification');
    }

    public function args(): array
    {
        return [
            'notification_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $notification = $this->notification->getById($args['notification_id']);

        if ($notification->user_id !== $context->id &&
            !in_array($context->id, $notification->user_ids ?? [])) {
            throw new AuthorizationError('Unauthorized');
        }
        $this->notification->updateInstance($notification, [
            'view' => true
        ]);

        return $this->notification->getById($args['notification_id'], $relations);
    }
}