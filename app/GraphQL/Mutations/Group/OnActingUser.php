<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Group;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\GroupContract;

class OnActingUser extends Mutation
{
    protected $attributes = [
        'name' => 'OnActingUser',
        'description' => 'Thêm trạng thái bán hàng cho người dùng'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->group->onActingUser($args['group_id'], $context->id);

        return ['code' => 200];
    }
}
