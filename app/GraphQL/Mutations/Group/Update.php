<?php
namespace App\GraphQL\Mutations\Group;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\GroupContract;
use App\GraphQL\Traits\Authorize;
use Arr;

class Update extends Mutation
{
    use Authorize;
    const PERMISSION = ".group.update-profile";

    protected $attributes = [
        'name' => 'UpdateGroup'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('group');
    }

    public function rules(array $args = []): array
    {
        return [
            'id' => 'required|string|exists:groups,_id',
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255'
        ];
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'area' => [
                'type' => GraphQL::type('AreaInput')
            ],
            'commune' => [
                'type' => GraphQL::type('CommuneInput')
            ],
            'address' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['id'], $context);
        
        $data = Arr::except($args, ['id']);

        $this->group->update($args['id'], $data);

        return $this->group->getById($args['id']);
    }
}