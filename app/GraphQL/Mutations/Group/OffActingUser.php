<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Group;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\GroupContract;

class OffActingUser extends Mutation
{
    protected $attributes = [
        'name' => 'OffActingUser',
        'description' => 'Xoá người dùng khỏi trạng thái bán hàng'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->group->offActingUser($args['group_id'], $context->id);

        return ['code' => 200];
    }
}
