<?php
namespace App\GraphQL\Mutations\Group;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\RoleContract;
use App\Contracts\PermissionContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class UpdatePermissionRole extends Mutation
{
    use Authorize;
    
    const PERMISSION = ".permission.update-role-permission";
    const SUCCESS_STATUS = 200;
    const UNAUTHORIZED_STATUS = 403;
    const ROLE_OUT_OWNER_STATUS = 402;

    protected $attributes = [
        'name' => 'UpdatePermissionOfRole'
    ];

    private $role;
    private $permission;

    public function __construct(RoleContract $role, PermissionContract $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    public function type(): Type
    {
        return GraphQL::type('role');
    }

    public function rules(array $args = []): array
    {
        return [
            'role_id'          => 'required|string|exists:roles,_id',
            'group_id'         => 'required|string',
            'permission_ids'   => 'nullable|array',
            'permission_ids.*' => 'string'
        ];
    }

    public function args(): array
    {
        return [
            'role_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'permission_ids' => [
                'type' => Type::listOf(Type::string())
            ]
        ];
    }

    /**
     * Cập nhật permission của role chỉ định
     *
     * Kiểm tra quyền có thuộc về nhóm chỉ định hay ko
     * Kiểm tra quyền có phải là quyền owner hay ko
     * Kiểm tra user này đã có quyền trong group chỉ định hay chưa và không được là owner
     *
     * @param  $root
     * @param  array $args
     * @return array
     */
    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $role = $this->role->getById($args['role_id']);
        $permissions = $this->permission->getByIds($args['permission_ids']);

        // Kiểm tra quyền này phải thuộc về group đã chỉ định
        // Kiểm tra quyền được gán không phải là owner
        if (empty($role) ||
            !$context->can('belongsToGroup', [$role, $args['group_id']]) ||
            !$context->can('outOwner', $role)
        ) {
            throw new AuthorizationError('Unauthorized');
        }

        $this->assignPermissionToRole($role, $permissions, $args['group_id'], $context);
        $this->removePermissionFromRole($role, $args['permission_ids']);

        return $this->role->getById($args['role_id']);
    }

    /**
     * Kiểm tra có những permission nào trong mảng gửi lên từ client không nằm trong
     * quyền của vai trò này thì thêm quyền đó vào vai trò
     *
     * @param  Maklad\Permission\Models\Role $role
     * @param  array  $permissions
     * @return void
     */
    private function assignPermissionToRole($role, $permissions, $groupId, $context)
    {
        foreach ($permissions as $permission) {
            if (!$context->can('belongsToGroup', [$permission, $groupId])) {
                return;
            }

            if (!in_array($permission->id, (array) $role->permission_ids)) {
                $this->role->assign($role, $permission);
            }
        }
    }

    /**
     * Kiểm tra có những permission nào không nằm trong mảng gửi từ client
     * thì xoá quyền đó khỏi vai trò chỉ định
     *
     * @param  Maklad\Permission\Models\Role $role
     * @param  array $permissionIds
     * @return void
     */
    private function removePermissionFromRole($role, array $permissionIds)
    {
        foreach ($role->permissions as $permission) {
            if (!in_array($permission->id, $permissionIds)) {
                $this->role->revoke($role, $permission);
            }
        }
    }

}