<?php

namespace App\GraphQL\Mutations\Group;

use App\Models\Group;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\GroupContract;
use App\Contracts\UserContract;

class Create extends Mutation
{
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'CreateGroup'
    ];

    private $group;
    private $user;

    public function __construct(GroupContract $group, UserContract $user)
    {
        $this->group = $group;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('group');
    }

    public function rules(array $args = []): array
    {
        return [
            'name'         => 'required|string|max:255',
            'service_type' => 'required|numeric|in:' . implode(',', $this->group->getServicesType()),
            'address'      => 'required|string|max:255'
        ];
    }

    public function args(): array
    {
        return [
            'name' => [
                'type' => Type::string()
            ],
            'service_type' => [
                'type' => Type::int()
            ],
            'area' => [
                'type' => GraphQL::type('AreaInput')
            ],
            'commune' => [
                'type' => GraphQL::type('CommuneInput')
            ],
            'address' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $args['owner_id'] = $context->id;
        $args['is_active'] = true;
        $args['care_status'] = [
            'status' => Group::NOT_CARE,
            'description' => ''
        ];

        $group = $this->group->create($args);
        $this->user->addUserToGroup($context->id, $group->id);

        return $group;
    }
}
