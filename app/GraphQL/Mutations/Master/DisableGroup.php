<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Master;

use App\Contracts\GroupContract;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use GraphQL;

class DisableGroup extends Mutation
{
    protected $attributes = [
        'name' => 'disableGroup',
        'description' => 'A mutation'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('group');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $group = $this->group->getById($args['id']);
        return $this->group->save($group, [
            'is_active' => false
        ]);
    }
}
