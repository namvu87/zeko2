<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Master;

use App\Contracts\GroupContract;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use GraphQL;

class UpdateCareStatus extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateCareStatus',
        'description' => 'A mutation'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('group');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'status' => [
                'type' => Type::nonNull(Type::int())
            ],
            'description' => [
                'type' => Type::string(),
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $group = $this->group->getById($args['group_id']);
        $careStatus = [
            'status' => (int) $args['status'],
            'description' => ''
        ];
        if ($args['status'] == 2) $careStatus['description'] = $args['description'];
        return $this->group->save($group, [
            'care_status' => $careStatus
        ]);
    }
}
