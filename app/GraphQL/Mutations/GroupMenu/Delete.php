<?php
namespace App\GraphQL\Mutations\GroupMenu;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GroupMenuContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;

class Delete extends Mutation
{
    use Authorize;

    const PERMISSION = ".group-menus.delete";
    const SUCCESS_STATUS = 200;
    const ACTION_REJECTED = 417;

    protected $attributes = [
        'name' => 'DeleteGroupMenu'
    ];

    private $menu;

    public function __construct(GroupMenuContract $menu)
    {
        $this->menu = $menu;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'group_menu_id' => [
                'required', 'string',
                Rule::exists('group_menus', '_id')->where(function ($query) use ($args) {
                    return $query->where('group_id', $args['group_id']);
                })
            ]
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_menu_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        if ($this->menu->deleteGroupMenuWithChilds($args['group_menu_id'])) {
            return ['code' => self::SUCCESS_STATUS];
        }
        return ['code' => self::ACTION_REJECTED];

    }
}