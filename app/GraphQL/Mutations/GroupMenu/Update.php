<?php
namespace App\GraphQL\Mutations\GroupMenu;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GroupMenuContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Error\AuthorizationError;
use Arr;

class Update extends Mutation
{
    use Authorize;

    const PERMISSION = ".group-menus.update";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'UpdateGroupMenu'
    ];

    private $menu;

    public function __construct(GroupMenuContract $menu)
    {
        $this->menu = $menu;
    }

    public function type(): Type
    {
        return GraphQL::type('group_menu');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'description' => 'nullable|string|max:1024',
            'name' => [
                'required', 'string', 'max:255',
                Rule::unique('group_menus', 'name')->where(function ($query) use ($args) {
                    return $query->where('group_id', $args['group_id']);
                })->ignore($args['group_menu_id'], '_id')
            ],
            'group_menu_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'group_menu_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $groupMenu = $this->menu->getById($args['group_menu_id']);
        if ($groupMenu->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }
        
        return $this->menu->save($groupMenu, Arr::only($args, ['name', 'description']));
    }
}