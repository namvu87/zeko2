<?php
namespace App\GraphQL\Mutations\GroupMenu;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GroupMenuContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use Illuminate\Validation\Rule;

class Create extends Mutation
{
    use Authorize;
    
    const PERMISSION = ".group-menus.create";

    protected $attributes = [
        'name' => 'CreateGroupMenu'
    ];

    private $menu;

    public function __construct(GroupMenuContract $menu)
    {
        $this->menu = $menu;
    }

    public function type(): Type
    {
        return GraphQL::type('group_menu');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'description' => 'nullable|string|max:1024',
            'name'     => [
                'required', 'string', 'max:255',
                Rule::unique('group_menus', 'name')->where(function ($query) use ($args) {
                    return $query->where('group_id', $args['group_id']);
                })
            ],
            'parent_id' => 'nullable|string'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'parent_id' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        return $this->menu->create([
            'name'        => $args['name'],
            'description' => $args['description'],
            'group_id'    => $args['group_id'],
            'parent_id'   => $args['parent_id'] ?? null,
            'level'       => $this->getLevel($args['parent_id'] ?? null, $args['group_id'])
        ]);
    }

    /**
     * @param  string $parentId
     * @return Int
     */
    private function getLevel($parentId, $groupId)
    {
        if (!empty($parentId)) {
            $menu = $this->menu->getById($parentId);
            if ($menu->group_id !== $groupId) {
                throw new AuthorizationError('Unauthorized');
            }
            return $menu->level + 1;
        }
        return $this->menu->getLevel0();
    }
}