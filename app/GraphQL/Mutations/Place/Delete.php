<?php
namespace App\GraphQL\Mutations\Place;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\PlaceContract;
use App\Contracts\Restaurant\TableContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class Delete extends Mutation
{
    use Authorize;

    const PERMISSION = ".place.delete";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'DeletePlace'
    ];

    private $place;
    private $table;

    public function __construct(PlaceContract $place, TableContract $table)
    {
        $this->place = $place;
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'place_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'place_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],

        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $place = $this->place->getById($args['place_id']);

        if ($place->group_id !== $args['group_id']) {
            return new AuthorizationError('Unauthorized');
        }

        $this->place->deleteInstance($place);
        $this->table->deleteByPlaceId($args['place_id']);

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}