<?php
namespace App\GraphQL\Mutations\Place;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\PlaceContract;
use Illuminate\Validation\Rule;
use App\GraphQL\Traits\Authorize;

class Create extends Mutation
{
    use Authorize;
    
    const PERMISSION = ".place.create";

    protected $attributes = [
        'name' => 'CreatePlace'
    ];

    private $place;

    public function __construct(PlaceContract $place)
    {
        $this->place = $place;
    }
    
    public function type(): Type
    {
        return GraphQL::type('place');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'name'     => [
                'required', 'string', 'max:128',
                Rule::unique('places')->where(function ($query) use ($args) {
                    return $query->where('group_id', $args['group_id'])->whereNull('deleted_at');
                })
            ],
            'description' => 'nullable|string|max:1024'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        return $this->place->createPlace($args);
    }
}