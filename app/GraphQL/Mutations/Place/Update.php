<?php
namespace App\GraphQL\Mutations\Place;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\PlaceContract;
use Illuminate\Validation\Rule;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class Update extends Mutation
{
    use Authorize;

    const PERMISSION = ".place.update";

    protected $attributes = [
        'name' => 'UpdatePlace'
    ];

    private $place;

    public function __construct(PlaceContract $place)
    {
        $this->place = $place;
    }

    public function type(): Type
    {
        return GraphQL::type('place');
    }

    /**
     * Định nghĩa các quy tắc xác thực dữ liệu (validation)
     * name phải unique trong group
     * place_id phải tồn tại trong group_id chỉ định
     *
     * @param  array  $args
     * @return aray
     */
    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'place_id' => 'required|string',
            'description' => 'nullable|string|max:1024',
            'name' => [
                'required', 'string', 'max:128',
                Rule::unique('places')->where(function ($query) use ($args) {
                    return $query->where('group_id', $args['group_id']);
                })->ignore($args['place_id'], '_id'),
            ]
        ];
    }

    public function args(): array
    {
        return [
            'place_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $place = $this->place->getById($args['place_id']);

        if ($place->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }

        return $this->place->save($place, [
            'name' => $args['name'],
            'description' => $args['description'] ?? ''
        ]);
    }
}