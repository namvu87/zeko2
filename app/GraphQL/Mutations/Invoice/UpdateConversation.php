<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Invoice;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\Restaurant\InvoiceUpdated;
use App\Events\Restaurant\ConversationUpdated;
use Carbon\Carbon;

class UpdateConversation extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.update';
    const FROM_CUSTOMER = 1;
    const FROM_RESTAURANT_EMPLOYEE = 2;

    protected $attributes = [
        'name' => 'SaleUpdateConversation',
        'description' => 'Cập nhật ghi chú cho hoá đơn'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::string()
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'content' => [
                'type' => Type::nonNull(Type::string())
            ],
            'from_type' => [
                'type' => Type::nonNull(Type::int())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->status !== $this->invoice->getStatusInited()) {
            throw new AuthorizationError('Unauthorized');
        }
        if (isset($args['group_id'])) {
            $this->customAuthorize($args['group_id'], $context);
            if ($invoice->group_id !== $args['group_id']) {
                throw new AuthorizationError('Unauthorize');
            }
        } else if (!in_array($context->id, $invoice->user_ids)) {
            throw new AuthorizationError('Unauthorized');
        }

        $conversation = $invoice->conversation;
        $data = [
            'creator' => [
                'fullname' => $context->fullname,
                'avatar' => $context->avatars ? $context->avatars['x1'] : ''
            ],
            'created_at' => Carbon::now()->toDateTimeString(),
            'from_type' =>  $args['from_type'],
            'content' => $args['content']
        ];
        $conversation[] = $data;

        $invoice = $this->invoice->save($invoice, [
            'conversation' => $conversation
        ]);

        broadcast(new ConversationUpdated(
            $args['invoice_id'],
            $data,
            $invoice->user_ids ?? [],
            $args['except_token'] ?? ''
        ))->toOthers();
        
        return $invoice;
    }
}
