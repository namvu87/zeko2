<?php
namespace App\GraphQL\Mutations\Invoice;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Events\Restaurant\InvoiceMemberAdded;
use Rebing\GraphQL\Error\AuthorizationError;

class JoinInvoice extends Mutation
{
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'JoinInvoice'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'invoice_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function resolve($root, $args, $context)
    {
        $invoice = $this->invoice->getById($args['invoice_id']);

        if (!$invoice || $invoice->status === $this->invoice->getStatusPurchased()) {
            throw new AuthorizationError('Unauthorized');
        }

        $invoice->push('user_ids', $context->id, true);

        event(new InvoiceMemberAdded($invoice, $context));

        return ['code' => self::SUCCESS_STATUS];
    }
}