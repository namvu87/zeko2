<?php
namespace App\GraphQL\Mutations\Invoice;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Events\Restaurant\InvoiceUpdated;
use Rebing\GraphQL\Error\AuthorizationError;
use Carbon\Carbon;

class RequestPurchase extends Mutation
{    
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'RequestPurchaseInvoice'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'invoice_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $invoice = $this->invoice->getById($args['invoice_id']);

        if (!in_array($context->id, $invoice->user_ids ?? [])) {
            throw new AuthorizationError('Unauthorize');
        }
        $notifications = $invoice->notifications ?? [];
        $notifications[] = [
            'from_type' => InvoiceUpdated::FROM_USER_TYPE,
            'type' => InvoiceUpdated::TYPE_41,
            'created_at' => Carbon::now()->toDateTimeString(),
            'creator' => [
                'id' => $context->id,
                'fullname' => $context->fullname,
                'avatar' => $context->avatars ? $context->avatars['x2'] : null
            ],
            'goods' => [],
            'is_execute' => false
        ];

        $invoice = $this->invoice->save($invoice, [
            'request_status' => $this->invoice->getPurchaseRequestStatus(),
            'notifications' => $notifications
        ]);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_41,
            InvoiceUpdated::FROM_USER_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();

        return ['code' => self::SUCCESS_STATUS];
    }
}