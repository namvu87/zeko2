<?php
namespace App\GraphQL\Mutations\Invoice;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\TableContract;
use Rebing\GraphQL\Error\AuthorizationError;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Events\Restaurant\InvoiceCreated;

class Create extends Mutation
{
    protected $attributes = [
        'name' => 'CreateInvoice'
    ];

    private $invoice;
    private $good;
    private $table;

    public function __construct(
        InvoiceContract $invoice,
        GoodContract $good,
        TableContract $table
    )
    {
        $this->invoice = $invoice;
        $this->good = $good;
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'table_id' => [
                'required', 'string',
                Rule::exists('tables', '_id')->where(function($query) use($args) {
                    $query->where('group_id', $args['group_id'])
                          ->where('status', $this->table->getAvailableType());
                })
            ],
            'goods'         => 'required|array',
            'goods.*.id'    => 'required|string',
            'goods.*.count' => 'required|integer|min: 1'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'table_id' => [
                'type' => Type::string()
            ],
            'goods' => [
                'type' => Type::listOf(GraphQL::type('InvoiceGoodInput'))
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        list($data, $notificationItems) = $this->resolveGoodsField($args['goods'], $args['group_id']);
        $data = [
            'code' => $this->invoice->generateCode($args['group_id']),
            'group_id' => $args['group_id'],
            'user_ids' => [$context->id],
            'table_ids' => [
                $args['table_id']
            ],
            'goods' => $data,
            'status' => $this->invoice->getStatusInited(),
            'request_status' => $this->invoice->getCreateInvoiceRequestStatus(),
            'note'           => '',
            'discount'       => 0,
            'discount_type'  => $this->invoice->getDiscountTypeFixed(),
            'notifications' => [
                [
                    'from_type' => InvoiceCreated::FROM_USER_TYPE,
                    'type' => InvoiceCreated::TYPE_40,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'creator' => [
                        'id' => $context->id,
                        'fullname' => $context->fullname,
                        'avatar' => $context->avatars ? $context->avatars['x2'] : null
                    ],
                    'goods' => $notificationItems,
                    'is_execute' => false
                ],
            ]
        ];

        $invoice = $this->invoice->create($data);

        broadcast(new InvoiceCreated(
            $invoice,
            $context,
            InvoiceCreated::FROM_USER_TYPE
        ))->toOthers();

        return $invoice;
    }

    /**
     * Tạo trường hàng hoá chuẩn dữ liệu
     *
     * @param  array $goods
     * @return array
     */
    private function resolveGoodsField(array $goods, string $groupId): array
    {
        $data = [];
        $notificationItems = [];
        foreach ($goods as $item) {
            $good = $this->good->getById($item['id']);
            if ($good->group_id !== $groupId) {
                throw new AuthorizationError('Unauthorized');
            }
            $data[] = [
                'id'            => $good->id,
                'code'          => $good->code,
                'name'          => $good->name,
                'image'         => $good->images[0] ?? '',
                'price'         => $good->price ?? 0,
                'price_origin'  => $good->price_origin ?? 0,
                'unit'          => $good->unit,
                'discount'      => $good->discount ?? 0,
                'discount_type' => $good->discount_type ?? 1,
                'count'         => (int) $item['count'],
                'orders' => [
                    [
                        'count' => (int) $item['count'],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'status' => $this->invoice->getGoodBookedStatus()
                    ]
                ]
            ];
            $notificationItems[] = [
                'id' => $good->id,
                'name' => $good->name,
                'count' => (int) $item['count']
            ];
        }
        return [$data, $notificationItems];
    }
}