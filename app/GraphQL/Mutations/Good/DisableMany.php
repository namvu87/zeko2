<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GoodContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class DisableMany extends Mutation
{
    use Authorize;

    const PERMISSION = ".goods.update";

    protected $attributes = [
        'name' => 'DisableManyGoods',
        'description' => 'Ngừng bán 1 số hàng hoá'
    ];

    private $good;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('good'));
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'   => 'required|string',
            'good_ids'   => 'required|array',
            'good_ids.*' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'good_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],

        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        foreach ($args['good_ids'] as $goodId) {
            $goods = $this->good->getById($goodId);

            if ($goods->group_id !== $args['group_id']) {
                throw new AuthorizationError('Unauthorized');
            }

            $this->good->updateInstance($goods, [
                'status' => $this->good->getStatusInactive()
            ]);

            foreach ($goods->childs as $item) {
                $this->good->updateInstance($item, [
                    'status' => $this->good->getStatusInactive()
                ]);
            }
        }

        return $this->good->getByIds($args['good_ids']);
    }
}