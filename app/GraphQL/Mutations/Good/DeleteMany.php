<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\UserContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;

class DeleteMany extends Mutation
{
    use Authorize;
    
    const PERMISSION = ".goods.delete";
    const SUCCESS_STATUS = 200;
    const UNAUTHORIZED = 403;

    protected $attributes = [
        'name' => 'DeleteManyGoods',
        'description' => 'Xoá một số hàng hoá'
    ];

    private $good;
    private $user;

    public function __construct(GoodContract $good, UserContract $user)
    {
        $this->good = $good;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'good_ids' => 'required|array',
            'good_id.*' => [
                'required', 'string',
                Rule::exists('goods', '_id')->where(function ($query) use ($args) {
                    $query->where('group_id', $args['group_id']);
                })
            ]
        ];
    }

    public function args(): array
    {
        return [
            'good_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],

        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        foreach ($args['good_ids'] as $goodId) {
            $this->good->delete($goodId);
        }

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}