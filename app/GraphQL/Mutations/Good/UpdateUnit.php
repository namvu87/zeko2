<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\GroupContract;
use App\GraphQL\Traits\Authorize;

class UpdateUnit extends Mutation
{
    use Authorize;
    
    const PERMISSION = ".goods.update";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'UpdateGoodsUnits'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'units'     => 'required|array',
            'units.*'   => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'units' => [
                'type' => Type::listOf(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $this->group->update($args['group_id'], [
            'goods_units' => array_unique($args['units'])
        ]);
        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}