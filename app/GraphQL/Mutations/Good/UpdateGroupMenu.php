<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\UserContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;

class UpdateGroupMenu extends Mutation
{
    use Authorize;

    const PERMISSION = ".goods.update";

    protected $attributes = [
        'name' => 'UpdateGoodGroupMenu'
    ];

    private $good;
    private $user;

    public function __construct(GoodContract $good, UserContract $user)
    {
        $this->good = $good;
        $this->user = $user;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('good'));
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'good_ids' => 'required|array',
            'good_ids.*' => [
                'required',
                Rule::exists('goods', '_id')->where(function ($query) use ($args) {
                    $query->where('group_id', $args['group_id']);
                })
            ],
            'group_menu_ids' => 'required|array',
            'group_menu_ids.*' => [
                'required',
                Rule::exists('group_menus', '_id')->where(function ($query) use ($args) {
                    $query->where('group_id', $args['group_id']);
                })
            ]
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'good_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'group_menu_ids' => [
                'type' => Type::listOf(Type::string())
            ],
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $this->good->updateGoodsGroupMenus($args['good_ids'], $args['group_menu_ids']);

        return $this->good->getByIds($args['good_ids'], ['groupMenus']);
    }
}