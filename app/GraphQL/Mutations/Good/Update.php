<?php

namespace App\GraphQL\Mutations\Good;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\GroupMenuContract;
use App\Contracts\UserContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Error\AuthorizationError;
use Illuminate\Support\Arr;

class Update extends Mutation
{
    use Authorize;

    const PERMISSION = ".goods.update";

    protected $attributes = [
        'name' => 'UpdateGood'
    ];

    private $good;
    private $user;
    private $menu;

    public function __construct(GoodContract $good, UserContract $user, GroupMenuContract $menu)
    {
        $this->good = $good;
        $this->user = $user;
        $this->menu = $menu;
    }

    public function type(): Type
    {
        return GraphQL::type('good');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'good_id'  => 'required|string',
            'code' => [
                'required', 'string',
                Rule::unique('goods', 'code')->where(function ($query) use ($args) {
                    return $query->where('group_id', $args['group_id']);
                })->ignore($args['good_id'], '_id')
            ],
            'group_menu_ids' => 'required|array',
            'group_menu_ids.*' => [
                'required', 'string',
                Rule::exists('group_menus', '_id')->where(function ($query) use ($args) {
                    return $query->where('group_id', $args['group_id']);
                })
            ],
            'name' => 'required|string|max:255',
            'price' => 'required|integer',
            'price_origin' => 'nullable|integer',
            'unit' => 'required|string',
            'inventory_min' => 'nullable|integer',
            'inventory_max' => 'nullable|integer',
            'description' => 'nullable|string|max:1024',
            'is_sale' => 'required|boolean',
            'discount' => 'required|numeric',
            'discount_type' => 'required_with:discount|integer',
            'properties' => 'nullable|array',
            'properties.*.name' => 'required|string|distinct',
            'properties.*.value' => 'required|string',
            'conversion_units' => 'nullable|array',
            'conversion_units.*.unit' => 'required|string|distinct',
            'conversion_units.*.value' => 'required|numeric',
            'conversion_units.*.price' => 'required|integer',
            'ingredients' => 'nullable|array',
            'system_images' => 'nullable|array',
            'system_images.*' => 'string',
            'ingredients.*.id' => 'string',
            'ingredients.*.count' => 'numeric|min:0.01',
            'ingredients.*.unit' => 'string',
            'ingredients.*.name' => 'string',
            'ingredients.*.price_origin' => 'int',
            'ingredients.*.code' => [
                'distinct',
                Rule::exists('goods', 'code')->where(function ($query) use ($args) {
                    $query->where('group_id', $args['group_id'])
                        ->where('type', $this->good->getImportType());
                })
            ]
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'good_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'price' => [
                'type' => Type::int()
            ],
            'price_origin' => [
                'type' => Type::int()
            ],
            'unit' => [
                'type' => Type::string()
            ],
            'inventory_min' => [
                'type' => Type::int()
            ],
            'inventory_max' => [
                'type' => Type::int()
            ],
            'discount' => [
                'type' => Type::float()
            ],
            'discount_type' => [
                'type' => Type::int()
            ],
            'is_sale' => [
                'type' => Type::boolean()
            ],
            'group_menu_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'system_images' => [
                'type' => Type::listOf(Type::string())
            ],
            'description' => [
                'type' => Type::string()
            ],
            'properties' => [
                'type' => Type::listOf(GraphQL::type('PropertyInput')),
                'description' => 'Các thuộc tính hàng hoá'
            ],
            'conversion_units' => [
                'type' => Type::listOf(GraphQL::type('ConversionUnitInput')),
                'description' => 'Các biến thể kích cỡ'
            ],
            'ingredients' => [
                'type' => Type::listOf(GraphQL::type('IngredientGoodInput')),
                'description' => 'Các thành phần cấu thành hàng hoá'
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $good = $this->good->getById($args['good_id']);
        if ($good->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }
        if ((count($good->images ?? []) + count($args['system_images'] ?? [])) >
            $this->good->getMaxFileUpload()
        ) {
            throw new AuthorizationError('Over max image');
        }
        $args['images'] = array_merge($good->images ?? [], $args['system_images'] ?? []);

        $rejectGroupMenu = array_diff($good->group_menu_ids, $args['group_menu_ids']);
        $addtitionGroupMenu = array_diff($args['group_menu_ids'], $good->group_menu_ids);

        $data = Arr::except($args, ['group_id', 'good_id']);

        $this->good->update($args['good_id'], $data);
        // Cập nhật các hàng hoá khác có thành phần là hàng hoá này,
        // nếu giá gốc thay đổi
        if ($good->type == $this->good->getImportType() && $args['price_origin'] !== $good->price_origin) {
            $this->updateIngredient($args['good_id'], $args['price_origin']);
        }

        // Thêm biến thể
        foreach ($args['conversion_units'] as $conversionUnit) {
            Arr::set($data, 'group_id', $args['group_id']);
            Arr::set($data, 'parent_id', $good->id);
            Arr::set($data, 'unit', $conversionUnit['unit']);
            Arr::set($data, 'price', $conversionUnit['price']);
            Arr::set($data, 'conversion_value', $conversionUnit['value']);
            Arr::set($data, 'code', $this->good->generateCode($args['group_id']));
            $this->good->create($data);
        }

        // Cập nhật những group menu thay đổi
        foreach ($rejectGroupMenu as $groupMenuId) {
            $this->menu->getById($groupMenuId)->pull('goods_ids', $good->id);
        }
        foreach ($addtitionGroupMenu as $groupMenuId) {
            $this->menu->getById($groupMenuId)->push('goods_ids', $good->id, true);
        }

        return $good->refresh();
    }

    /**
     * Cập nhật giá gốc cho hàng hoá thành phần được cấu thành từ hàng hoá chỉ định
     *
     * @param  string $goodId
     *
     * @return void
     */
    private function updateIngredient(string $goodId, $price)
    {
        $goods = $this->good->getByIngredientId($goodId);
        foreach ($goods as $good) {
            $arr = $good->ingredients;
            foreach ($arr as $key => $ingredient) {
                if ($ingredient['id'] == $goodId) {
                    $arr[$key]['price_origin'] = $price;
                }
            }
            $good->update([
                'ingredients' => $arr
            ]);
        }
    }
}
