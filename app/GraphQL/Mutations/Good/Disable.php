<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GoodContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class Disable extends Mutation
{
    use Authorize;

    const PERMISSION = ".goods.update";

    protected $attributes = [
        'name' => 'DisableGoods',
        'description' => 'Ngừng bán 1 hàng hoá'
    ];

    private $good;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    public function type(): Type
    {
        return GraphQL::type('good');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'good_id' =>  'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'good_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],

        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $goods = $this->good->getById($args['good_id']);

        if ($goods->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }

        $this->good->updateInstance($goods, [
            'status' => $this->good->getStatusInactive()
        ]);

        foreach ($goods->childs as $item) {
            $this->good->updateInstance($item, [
                'status' => $this->good->getStatusInactive()
            ]);
        }

        return $this->good->getById($args['good_id']);
    }
}