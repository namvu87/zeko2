<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\UserContract;
use App\GraphQL\Traits\Authorize;

class DeleteImage extends Mutation
{
    use Authorize;

    const PERMISSION = ".goods.update";

    protected $attributes = [
        'name' => 'DeleteGoodsImage',
        'description' => 'Xoá ảnh của 1 hàng hoá, đồng thời cập nhật cho childs'
    ];

    private $good;
    private $user;

    public function __construct(GoodContract $good, UserContract $user)
    {
        $this->good = $good;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('good');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'  => 'required|string',
            'good_id'   => 'required|string',
            'image_url' => 'required|string|max:128'
        ];
    }

    public function args(): array
    {
        return [
            'image_url' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'good_id'  => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $good = $this->good->getById($args['good_id']);

        if ($good->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }
        $good->pull('images', $args['image_url']);
        $this->updateParent($good, $args['image_url']);
        $this->updateChilds($good, $args['image_url']);
        $this->good->removeImage($args['image_url']);
        return $this->good->getById($args['good_id']);
    }

    /**
     * @param  App\Mdoels\Good $good
     * @param  string $url
     * @return void
     */
    private function updateParent($good, $url)
    {
        if ($good->parent) {
            $good->parent->pull('images', $url);
        }
    }

    /**
     * @param  App\Models\Good $good
     * @param  String $url
     * @return void
     */
    private function updateChilds($good, string $url)
    {
        if ($good->childs) {
            foreach ($good->childs as $item) {
                $item->pull('images', $url);
            }
        }
    }
}
