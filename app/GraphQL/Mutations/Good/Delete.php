<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\UserContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class Delete extends Mutation
{
    use Authorize;

    const PERMISSION = ".goods.delete";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'DeleteGoods',
        'description' => 'Xoá 1 hàng hoá'
    ];

    private $good;
    private $user;

    public function __construct(GoodContract $good, UserContract $user)
    {
        $this->good = $good;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'good_id'  => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'good_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],

        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $goods = $this->good->getById($args['good_id']);

        if ($goods->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }
        
        $this->good->deleteInstance($goods);

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}