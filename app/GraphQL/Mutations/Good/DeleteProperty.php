<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\GroupContract;
use App\GraphQL\Traits\Authorize;

class DeleteProperty extends Mutation
{
    use Authorize;
    
    const PERMISSION = ".goods.update";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'DeleteGoodsProperty',
        'description' => 'Xoá 1 thuộc tính của hàng hoá'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'property' => 'required|string|max:128'
        ];
    }

    public function args(): array
    {
        return [
            'property' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $group = $this->group->getById($args['group_id']);

        $group->pull('goods_properties', $args['property']);
        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}