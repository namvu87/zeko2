<?php
namespace App\GraphQL\Mutations\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\UserContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;

class AssignSaleGood extends Mutation
{
    use Authorize;

    const PERMISSION = ".goods.update";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'AssignSaleGood',
        'description' => 'Chỉ định 1 hàng nhập là bán được'
    ];

    private $good;
    private $user;

    public function __construct(GoodContract $good, UserContract $user)
    {
        $this->good = $good;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'good_ids' => 'required|array',
            'good_ids.*' => [
                'required', 'string',
                Rule::exists('goods', '_id')->where(function ($query) use ($args) {
                    $query->where('group_id', $args['group_id']);
                })
            ]
        ];
    }

    public function args(): array
    {
        return [
            'good_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],

        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $this->good->assignSale($args['good_ids']);

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}