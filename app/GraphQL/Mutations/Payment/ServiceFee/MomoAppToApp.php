<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Payment\ServiceFee;

use Closure;
use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use GuzzleHttp\Client;
use App\Contracts\ServiceFeeTransactionContract;
use App\Contracts\ServiceFeeContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Traits\Payment\MomoHashes;
use App\Traits\Payment\ServiceFeeHelper;

class MomoAppToApp extends Mutation
{
    use MomoHashes, ServiceFeeHelper;

    protected $attributes = [
        'name' => 'payment/ServiceFee/MomoAppToApp',
        'description' => 'Thanh toán phí dịch vụ trên mobile'
    ];

    private $transaction;
    private $fee;

    public function __construct(ServiceFeeTransactionContract $transaction, ServiceFeeContract $fee)
    {
        $this->transaction = $transaction;
        $this->fee = $fee;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('service_fee'));
    }

    public function args(): array
    {
        return [
            'customer_number' => [
                'type' => Type::nonNull(Type::string())
            ],
            'momo_token' => [
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'type' => Type::nonNull(Type::string())
            ],
            'amount' => [
                'type' => Type::nonNull(Type::int())
            ],
            'service_fees' => [
                'type' => Type::nonNull(Type::listOf(GraphQL::type('PayServiceFeeInput')))
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $selections = $fields->getSelect();
        $relations = $fields->getRelations();

        $actualAmount = $this->getAmount($args['service_fees']);

        if ($actualAmount !== $args['amount']) {
            throw new AuthorizationError(__('payment.bad_payment_info'));
        }
        $transaction = $this->transaction->create([
            'user_id' => $context->id,
            'service_fees' => $args['service_fees'],
            'amount' => $actualAmount,
            'status' => $this->transaction->getInitedState()
        ]);

        $hash = $this->encryptRSA([
            'partnerCode' => config('payment.momo.partner_code'),
            'partnerRefId' => $transaction->id,
            'amount' => $args['amount']
        ]);

        $payload = [
            'partnerCode'    => config('payment.momo.partner_code'),
            'partnerRefId'   => $transaction->id,
            'customerNumber' => $args['customer_number'],
            'appData'        => $args['momo_token'],
            'hash'           => $hash,
            'version'        => config('payment.momo.app_version'),
            'payType'        => config('payment.momo.app_pay_type'),
            'description'    => $args['description']
        ];

        $response = $this->httpClient($payload);
        if ($response['status'] !== 0) {
            throw new AuthorizationError($response['message'] ?? 'unauthorized');
        }
        $this->checkValidSignature($response);

        $this->updateServiceFeeState($transaction);

        $feeIds = collect($args['service_fees'])->pluck('service_fee_id')->toArray();
        return $this->fee->getByIds($feeIds, $relations, $selections);
    }

    /**
     * Request to momo
     *
     * @param array $payload
     * @return array
     */
    private function httpClient(array $payload): array
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
        $response = $client->post(config('payment.momo.app_payment_url'), [
            'body' => json_encode($payload)
        ]);
        return json_decode((string) $response->getBody(), true);
    }

    /**
     * Kiểm tra chữ ký điện tử trả về từ momo có hợp lệ hay không
     *
     * @param array $data
     * @return void
     */
    private function checkValidSignature(array $data)
    {
        $rawData = [
            'status'  => $data['status'],
            'message' => $data['message'],
            'amount'  => $data['amount'],
            'transid' => $data['transid']
        ];
        $hashed = $this->generateSignature($rawData);

        if (!hash_equals($data['signature'], $hashed)) {
            throw new AuthorizationError('Chữ ký điện tử không hợp lệ');
        }
    }
}
