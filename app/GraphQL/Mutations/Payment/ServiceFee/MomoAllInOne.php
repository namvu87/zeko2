<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Payment\ServiceFee;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\GroupContract;
use App\Contracts\ServiceFeeContract;
use App\Contracts\ServiceFeeTransactionContract;
use GuzzleHttp\Client;
use App\Traits\Payment\MomoHashes;
use App\Traits\Payment\ServiceFeeHelper;
use Rebing\GraphQL\Error\AuthorizationError;

class MomoAllInOne extends Mutation
{
    use MomoHashes, ServiceFeeHelper;

    const MOMO_REQUEST_TYPE = 'captureMoMoWallet';
    const PACKAGE_DEFAULT = 0;

    protected $attributes = [
        'name' => 'payment/Service/MomoAllInOne',
        'description' => 'Thanh toán dịch vụ cho ZEKO trên web'
    ];

    private $group;
    private $fee;
    private $transaction;

    public function __construct(
        GroupContract $group,
        ServiceFeeContract $fee,
        ServiceFeeTransactionContract $transaction
    ) {
        $this->group = $group;
        $this->fee = $fee;
        $this->transaction = $transaction;
    }

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'service_fees' => [
                'type' => Type::listOf(GraphQL::type('PayServiceFeeInput'))
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        list($amount, $extraData) = $this->prepareData($args);
        $transaction = $this->transaction->create([
            'user_id' => $context->id,
            'service_fees' => $args['service_fees'],
            'amount' => $amount,
            'status' => $this->transaction->getInitedState()
        ]);

        // Data để tạo chữ ký điện tử (signature) theo chuẩn từ momo
        $payload = [
            'partnerCode' => config('payment.momo.partner_code'),
            'accessKey' => config('payment.momo.access_key'),
            'requestId' => $transaction->id,
            'amount' => (string) $amount,
            'orderId' => $transaction->id,
            'orderInfo' => 'Thanh toán phí dịch vụ ' . date('Y-m'),
            'returnUrl' => config('app.url'),
            'notifyUrl' => route('payment.momo.web_callback'),
            'extraData' => $extraData
        ];
        $payload['signature'] = $this->generateSignature($payload);
        $payload['requestType'] = self::MOMO_REQUEST_TYPE;

        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
        $response = $client->post(config('payment.momo.payment_url'), [
            'body' => json_encode($payload)
        ]);
        $response = json_decode((string) $response->getBody(), true);
        if ($response['errorCode'] !== 0) {
            throw new AuthorizationError($response['localMessage']);
        }
        $this->checkValidSignature($response);
        $this->updateServiceFeeState($transaction);

        return $response['payUrl'];
    }

    /**
     * Tính amount cho nhóm nhà hàng
     *
     * @param integer $package
     * @param $serviceFee
     * @return integer
     */
    private function calculateRestaurantServiceFee(int $package, $serviceFee): int
    {
        $amount = 0;
        switch ($package) {
            case 1:
                $amount = $serviceFee['invoices_count'] * $this->getValuePackge($package);
                break;
            case 2:
                $amount = $serviceFee['revenue'] * $this->getValuePackge($package);
                break;
            case 3:
                $amount = $this->getValuePackge($package);
                break;
            default:
                break;
        }
        return (int) $amount;
    }

    private function getValuePackge($package): float
    {
        return config("payment_package.restaurant.{$package}.fee");
    }

    private function prepareData(array $args): array
    {
        $amount = 0;
        $extraData = '';

        foreach ($args['service_fees'] as $item) {
            $serviceFee = $this->fee->getById($item['service_fee_id']);
            if ($serviceFee->status !== $this->fee->getUnpaidState()) {
                throw new AuthorizationError(
                    $serviceFee->group->name . ' đã thanh toán hoặc đang trong tiến trình thanh toán'
                );
            }
            if ($serviceFee->group->service_type === $this->group->getTimekeepingType()) {
                $amount += config('payment_package.timekeeping.fee');
            } else {
                $amount += $this->calculateRestaurantServiceFee(
                    $item['package'] ?? 1,
                    $serviceFee
                );
            }
            $extraData .= $item['service_fee_id'] . '=' . ($item['package'] ?? self::PACKAGE_DEFAULT) . ';';
        }
        $extraData = substr($extraData, 0, -1);

        return [$amount, $extraData];
    }

    /**
     * Kiểm tra chữ ký điện tử có hợp lệ hay không
     *
     * @param array $data
     * @return void
     */
    private function checkValidSignature(array $data)
    {
        $rawData = [
            'requestId'    => $data['requestId'],
            'orderId'      => $data['orderId'],
            'message'      => $data['message'],
            'localMessage' => $data['localMessage'],
            'payUrl'       => $data['payUrl'],
            'errorCode'    => $data['errorCode'],
            'requestType'  => $data['requestType']
        ];
        $hashed = $this->generateSignature($rawData);

        if (!hash_equals($data['signature'], $hashed)) {
            throw new AuthorizationError('Chữ ký điện tử không hợp lệ');
        }
    }
}
