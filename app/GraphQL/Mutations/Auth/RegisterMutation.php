<?php

namespace App\GraphQL\Mutations\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\AuthContract;
use Illuminate\Auth\Events\Registered;
use Arr;

class RegisterMutation extends Mutation
{
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'RegisterUser'
    ];

    private $auth;

    public function __construct(AuthContract $auth)
    {
        $this->auth = $auth;
    }

    public function type(): Type
    {
        return GraphQL::type('auth');
    }

    public function rules(array $args = []): array
    {
        return [
            'first_name'   => ['required', 'string', 'max:64'],
            'last_name'    => ['required', 'string', 'max:64'],
            'email'        => ['required', 'email', 'max:255', 'unique:users,email'],
            'phone_number' => ['required', 'numeric', 'digits:10', 'unique:users,phone_number'],
            'password'     => ['required', 'string', 'min:8', 'confirmed']
        ];
    }

    public function args(): array
    {
        return [
            'first_name' => [
                'type' => Type::string()
            ],
            'last_name' => [
                'type' => Type::string()
            ],
            'email' => [
                'type' => Type::string()
            ],
            'phone_number' => [
                'type' => Type::string()
            ],
            'password' => [
                'type' => Type::string()
            ],
            'password_confirmation' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $data = Arr::except($args, 'password_confirmation');

        Arr::set($data, 'email', strtolower($data['email']));
        Arr::set($data, 'password', bcrypt($data['password']));

        $user = $this->auth->regist($data);

        event(new Registered($user));

        return [
            'access_token' => $user->createToken('access token')->accessToken,
            'user' => $user
        ];
    }
}
