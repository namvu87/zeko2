<?php

namespace App\GraphQL\Mutations\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\AuthContract;
use App\Contracts\UserContract;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Exception;

class OauthMutation extends Mutation
{
    const FACEBOOK_DRIVER_TYPE = 1;
    const GOOGLE_DRIVER_TYPE = 2;
    const FACEBOOK_PROFILE_ENDPOINT = 'https://graph.facebook.com/v3.3/me';
    const GOOGLE_TOKEN_INFO_ENDOINT = 'https://oauth2.googleapis.com/tokeninfo';
    const SUCCESS_STATUS = 200;
    const UNAUTHORIZED_STATUS = 401;

    protected $attributes = [
        'name' => 'SocialOauth'
    ];

    private $auth;
    private $client;
    private $user;

    public function __construct(AuthContract $auth, Client $client, UserContract $user)
    {
        $this->auth = $auth;
        $this->user = $user;
        $this->client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
    }

    public function type(): Type
    {
        return GraphQL::type('auth');
    }

    public function rules(array $args = []): array
    {
        return [
            'token'        => 'required|string',
            'driver_type'  => 'required|integer',
            'platform'     => 'nullable|string|in:ios,android'
        ];
    }

    public function args(): array
    {
        return [
            'token' => [
                'type' => Type::string()
            ],
            'driver_type' => [
                'type' => Type::int()
            ],
            'platform' => [
                'type' => Type::string()
            ]
        ];
    }

    /**
     * Kiểm tra xem người dùng này đã được tạo hay chưa
     * Nếu chưa tạo thì tạo mới và trả về thông tin người dùng
     * Nếu đã tạo rồi thì tiếng hành đăng nhập và trả về thông tin đăng nhập
     *
     * @param  $root
     * @param  array $args
     * @return App\Graphql\Queries\AuthQuery
     */
    public function resolve($root, $args)
    {
        try {
            $data = $this->getUserInfo($args);
        } catch (Exception $e) {
            return null;
        }

        $user = $this->user->findForPassport($data['email']);

        // Email đã có trên hệ thống (người dùng đã đăng ký)
        if ($user) {
            return [
                'access_token' => $user->createToken('access token')->accessToken,
                'user' => $user
            ];
        }

        // Email chưa có trên hệ thống
        $user = $this->auth->regist($data);

        return [
            'access_token' => $user->createToken('access token')->accessToken,
            'user' => $user
        ];
    }

    /**
     * Lấy thông tin user từ social qua access token
     *
     * @param  array  $args
     * @return array
     */
    private function getUserInfo(array $args): array
    {
        if ($args['driver_type'] === self::FACEBOOK_DRIVER_TYPE) {
            return $this->getUserInfoFromFacebook($args['token']);
        }
        return $this->getUserFromGoogleIdToken($args['token']);
    }

    /**
     * Lấy thông tin người dùng từ facebook qua access token
     *
     * @param  string $token
     * @return array
     */
    private function getUserInfoFromFacebook(string $token): array
    {
        $response = $this->client->request('GET', self::FACEBOOK_PROFILE_ENDPOINT, [
            'query' => [
                'access_token' => $token,
                'fields'       => 'id,first_name,last_name,middle_name,email'
            ]
        ]);
        $response = json_decode($response->getBody());

        return [
            'first_name' => ($response->middle_name ?? '') . ' ' . ($response->first_name ?? ''),
            'last_name'  => $response->last_name ?? '',
            'email'      => $response->email,
            'avatars'     => [
                'x1' => "https://graph.facebook.com/{$response->id}/picture?width=32&height=32",
                'x2' => "https://graph.facebook.com/{$response->id}/picture?width=160&height=160",
                'x3' => "https://graph.facebook.com/{$response->id}/picture?width=576&height=576"
            ],
            'social' => [
                'facebook' => [
                    'id' => $response->id,
                    'access_token' => $token
                ]
            ],
            'email_verified_at' => Carbon::now()->toDateTimeString()
        ];
    }

    /**
     * Lấy thông tin user từ google qua ID TOKEN
     *
     * @param  string $token
     * @return array
     */
    private function getUserFromGoogleIdToken(string $token): array
    {
        $response = $this->client->get(self::GOOGLE_TOKEN_INFO_ENDOINT, [
            'query' => [
                'id_token' => $token
            ]
        ]);
        $response = json_decode($response->getBody());

        return [
            'first_name' => $response->given_name ?? '',
            'last_name'  => $response->family_name ?? '',
            'email'      => $response->email,
            'avatars'     => [
                'x1' => $response->picture . '?sz=32',
                'x2' => $response->picture . '?sz=160',
                'x3' => $response->picture . '?sz=576',
            ],
            'social' => [
                'google' => [
                    'id' => $response->sub,
                    'id_token' => $token
                ]
            ],
            'email_verified_at' => Carbon::now()->toDateTimeString()
        ];
    }
}
