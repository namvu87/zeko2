<?php
namespace App\GraphQL\Mutations\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Rules\PasswordMatched;
use App\Contracts\UserContract;

class ChangePassword extends Mutation
{
    const SUCCESS_STATUS = 200;
    const UNAUTHORIZED_STATUS = 401;

    protected $attributes = [
        'name' => 'ChangePassword'
    ];

    private $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'current_password' => [
                'required',
                'string',
                new PasswordMatched
            ],
            'password' => 'required|string|min:8|confirmed'
        ];
    }

    public function args(): array
    {
        return [
            'current_password' => [
                'type' => Type::string()
            ],
            'password' => [
                'type' => Type::string()
            ],
            'password_confirmation' => [
                'type' => Type::string()
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $data = [
            'password' => bcrypt($args['password'])
        ];

        $userId = $this->user->id();
        $this->user->update($userId, $data);

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}