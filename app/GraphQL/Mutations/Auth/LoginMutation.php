<?php

namespace App\GraphQL\Mutations\Auth;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\AuthContract;
use App\Contracts\UserContract;
use Rebing\GraphQL\Error\AuthorizationError;

class LoginMutation extends Mutation
{
    const SUCCESS_STATUS = 200;
    const UNAUTHORIZED_STATUS = 401;

    protected $attributes = [
        'name' => 'LoginUser'
    ];

    private $auth;
    private $user;

    public function __construct(AuthContract $auth, UserContract $user)
    {
        $this->auth = $auth;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('auth');
    }

    public function rules(array $args = []): array
    {
        return [
            'username' => 'required|string',
            'password' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'username' => [
                'type' => Type::string()
            ],
            'password' => [
                'type' => Type::string()
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $response = $this->auth->login($args);

        if ($response->getStatusCode() !== self::SUCCESS_STATUS) {
            throw new AuthorizationError(__('auth.failed'));
        }

        $data = json_decode($response->getContent());

        return [
            'access_token' => $data->access_token,
            'user' => $this->user->findForPassport($args['username'])
        ];
    }
}
