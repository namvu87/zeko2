<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Auth;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Carbon\Carbon;
use Rebing\GraphQL\Error\AuthorizationError;

class VerifyPhoneNumber extends Mutation
{
    const EXPIRES_TIME = 300;

    protected $attributes = [
        'name' => 'auth/VerifyPhoneNumber',
        'description' => 'Xác thực số điện thoại'
    ];

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function rules(array $args = []): array
    {
        return [
            'code' => 'required|numeric'
        ];
    }

    public function args(): array
    {
        return [
            'code' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        if ($this->invalidData($context)) {
            throw new AuthorizationError('Invalid data');
        }

        if ($this->isValidCode($args['code'], $context)) {
            if (empty($context->phone_number)) {
                $context->phone_number = $context->phone_number_verify['phone_number'];
            }
            $context->phone_number_verify = [];
            $context->phone_number_verified_at = Carbon::now()->toDateTimeString();
            $context->save();

            return $context;
        }
        throw new AuthorizationError(__('esms.invalid_code'));
    }

    /**
     * Xác minh mã xác thực có hợp lệ không
     *
     * @param Request $request
     * @param App\Models\User $user
     * @return boolean
     */
    private function isValidCode(string $code, $user): bool
    {
        if (empty($user->phone_number_verify)) {
            return false;
        }
        $now = Carbon::now();
        $expiredAt = Carbon::parse($user->phone_number_verify['expired_at']);

        return $code == $user->phone_number_verify['code']
            && $expiredAt->diffInSeconds($now) <= self::EXPIRES_TIME;
    }

    /**
     * Nếu số điện thoại chưa tồn tại thì phải có số điện thoại trong mảng mới `phone_number_verify`
     *
     * @param \App\Models\User $user
     * @return boolean
     */
    private function invalidData($user): bool
    {
        return empty($user->phone_number)
            && empty($user->phone_number_verify)
            && empty($user->phone_number_verify['phone_number']);
    }
}
