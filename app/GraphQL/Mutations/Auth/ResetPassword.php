<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\AuthContract;
use App\Contracts\UserContract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use GraphQL\Error\Error;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPassword extends Mutation
{
    use ResetsPasswords;

    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'ResetPassword',
        'description' => 'Perform reset password and return access token'
    ];

    private $auth;
    private $user;

    public function __construct(UserContract $user, AuthContract $auth)
    {
        $this->user = $user;
        $this->auth = $auth;
    }

    public function type(): Type
    {
        return GraphQL::type('auth');
    }

    public function rules(array $args = []): array
    {
        return [
			'token'    => 'required',
			'email'    => 'required|email',
			'password' => 'required|confirmed|min:8'
        ];
	}

    public function args(): array
    {
        return [
            'token' => [
                'type' => Type::string()
			],
			'email' => [
                'type' => Type::string()
			],
			'password' => [
                'type' => Type::string()
			],
			'password_confirmation' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $responseReset = $this->broker()->reset($args, function ($user, $password) {
            $this->resetPassword($user, $password);
        });
        if ($responseReset !== Password::PASSWORD_RESET) {
            return new Error(__($responseReset));
        }

        $payload = [
            'username' => $args['email'],
            'password' => $args['password']
        ];
        $responseLogin = $this->auth->login($payload);

        if ($responseLogin->getStatusCode() !== self::SUCCESS_STATUS) {
            return null;
        }

        $data = json_decode($responseLogin->getContent());

        return [
            'access_token' => $data->access_token,
            'user' => $this->user->findForPassport($args['email'])
        ];

    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);
        $user->save();
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    public function validationErrorMessages(array $args = []): array
    {
        return [];
    }
}
