<?php
namespace App\GraphQL\Mutations\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\AuthContract;
use App\Contracts\Users\Firebase;

class LogoutMutation extends Mutation
{
    protected $attributes = [
        'name' => 'LogoutUser'
    ];

    private $auth;
    private $firebase;

    public function __construct(AuthContract $auth, Firebase $firebase)
    {
        $this->auth = $auth;
        $this->firebase = $firebase;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'firebase_token' => 'nullable|string'
        ];
    }

    public function args(): array
    {
        return [
            'firebase_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->auth->logout($args);
        $this->firebase->pull($context, $args['firebase_token'] ?? '');

        return [
            'code' => 200
        ];
    }
}