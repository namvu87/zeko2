<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class ForgotPassword extends Mutation
{
    const SUCCESS_STATUS = 200;
    const NOT_ALLOW_METHOD = 405;

    use SendsPasswordResetEmails;

    protected $attributes = [
        'name' => 'ForgotPassword',
        'description' => 'Send Email for reset password'
    ];

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []) :array
    {
        return [
            'email' => 'required|email'
        ];
    }

    public function args(): array
    {
        return [
            'email' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $response = $this->broker()->sendResetLink([
			'email' => $args['email']
        ]);
        
        if ($response !== Password::RESET_LINK_SENT) {
			return [
				'message' => __($response),
				'code' => self::NOT_ALLOW_METHOD
			];
        }
        return [
            'message' => __($response),
            'code' => self::SUCCESS_STATUS
		];
    }
}
