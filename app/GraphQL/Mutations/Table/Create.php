<?php
namespace App\GraphQL\Mutations\Table;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\TableContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;

class Create extends Mutation
{
    use Authorize;

    const PERMISSION = ".table.create";

    protected $attributes = [
        'name' => 'CreateTable'
    ];

    private $table;

    public function __construct(TableContract $table)
    {
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('table');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'count_seat' => 'required|numeric',
            'sort' => 'nullable|numeric',
            'description' => 'nullable|string|max:1024',
            'name'     => [
                'required', 'string', 'max:128',
                Rule::unique('tables', 'name')->where(function ($query) use ($args) {
                    return $query->where('group_id', $args['group_id'])->whereNull('deleted_at');
                })
            ],
            'place_id' => [
                'required', 'string',
                Rule::exists('places', '_id')->where(function ($query) use ($args) {
                    $query->where('group_id', $args['group_id']);
                })
            ]
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'place_id' => [
                'type' => Type::string()
            ],
            'count_seat' => [
                'type' => Type::int()
            ],
            'sort' => [
                'type' => Type::int()
            ],
            'description' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $args['status'] = $this->table->getAvailableType();
        
        return $this->table->createTable($args);
    }
}