<?php
namespace App\GraphQL\Mutations\Table;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\TableContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Error\AuthorizationError;
use Arr;

class Update extends Mutation
{
    use Authorize;
    
    const PERMISSION = ".table.update";

    protected $attributes = [
        'name' => 'UpdateTable'
    ];

    private $place;
    private $table;

    public function __construct(TableContract $table)
    {
        $this->table = $table;
    }

    /**
     * Khai báo kiểu dữ liệu trả về
     *
     * @return Array
     */
    public function type(): Type
    {
        return GraphQL::type('table');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'    => 'required|string',
            'count_seat'  => 'required|numeric',
            'sort'        => 'nullable|numeric',
            'description' => 'nullable|string|max:1024',
            'place_id' => [
                'required', 'string',
                Rule::exists('places', '_id')->where(function ($query) use ($args) {
                    $query->where('group_id', $args['group_id']);
                })
            ],
            'name' => [
                'required', 'string', 'max:128',
                Rule::unique('tables', 'name')->where(function ($query) use ($args) {
                    return $query->where('place_id', $args['place_id']);
                })->ignore($args['table_id'], '_id')
            ],
            'table_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'place_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'table_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'sort' => [
                'type' => Type::int()
            ],
            'count_seat' => [
                'type' => Type::int()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);
        
        $data = Arr::except($args, ['table_id', 'group_id']);
        $table = $this->table->getById($args['table_id']);

        if ($table->group_id !== $args['group_id']) {
            return new AuthorizationError('Unauthorized');
        }

        return $this->table->save($table, $data);
    }
}