<?php
namespace App\GraphQL\Mutations\Table;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\TableContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class Delete extends Mutation
{
    use Authorize;

    const PERMISSION = ".table.delete";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'DeleteTable'
    ];

    private $table;

    public function __construct(TableContract $table)
    {
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'table_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'table_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],

        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $table = $this->table->getById($args['table_id']);

        if ($table->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }

        $this->table->deleteInstance($table);
        
        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}