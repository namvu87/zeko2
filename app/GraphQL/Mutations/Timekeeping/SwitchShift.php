<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Timekeeping;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\TimekeepingContract;
use App\Contracts\RequirementContract;

class SwitchShift extends Mutation
{
    protected $attributes = [
        'name' => 'SwitchShiftTimekeeping',
        'description' => 'Yêu cầu chuyển ca làm việc'
    ];

    private $timekeeping;
    private $requirement;

    public function __construct(
        TimekeepingContract $timekeeping,
        RequirementContract $requirement
    )
    {
        $this->timekeeping = $timekeeping;
        $this->requirement = $requirement;
    }

    public function rules(array $args = []): array
    {
        return [
            'content'  => 'required|string',
            'date'     => 'required|date_format:Y-m-d|before:today'
        ];
    }
    
    public function type(): Type
    {
        return GraphQL::type('requirement');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'date' => [
                'type' => Type::string()
            ],
            'content' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        if (!in_array($args['group_id'], $context->group_ids)) {
            throw new AuthorizationError("Unauthorized");
        }
        
        $timekeeping = $this->timekeeping->getMyTimekeepingByDate(
            $context->id,
            $args['group_id'],
            $args['date']
        );
        
        if (empty($timekeeping)) {
            throw new AuthorizationError("Unauthorized");
        }

        try {
            return $this->requirement->create([
                'creator_id'     => $context->id,
                'group_id'       => $args['group_id'],
                'date'           => $args['date'],
                'content'        => $args['content'],
                'timekeeping_id' => $timekeeping->id,
                'type'           => $this->requirement->getWitchShiftType()
            ]);
        } catch (\Exception $e) {
            throw new AuthorizationError(__('time_keeping.duplicate_requirement'));
        }
    }
}
