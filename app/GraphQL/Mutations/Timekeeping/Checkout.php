<?php
namespace App\GraphQL\Mutations\Timekeeping;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Error\AuthorizationError;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\TimekeepingContract;
use App\Contracts\ShiftContract;

class Checkout extends Mutation
{
    const CHECKIN_TYPE = 1;
    const CHECKOUT_TYPE = 2;
    const UNAUTHORIZED_STATUS = 403;
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'CheckoutTimekeeping'
    ];

    private $shift;
    private $timekeeping;

    public function __construct(TimekeepingContract $timekeeping, ShiftContract $shift)
    {
        $this->shift = $shift;
        $this->timekeeping = $timekeeping;
    }

    public function type(): Type
    {
        return GraphQL::type('timekeeping');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'  => 'required|string',
            'location'  => 'required|array'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'location' => [
                'type' => GraphQL::type('LocationInput')
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $shift = $this->shift
                      ->getByIds($context->shift_ids)
                      ->where('group_id', $args['group_id'])
                      ->first();
        if (!$shift) {
            throw new AuthorizationError(__('company.shift.not_exists_shift'));
        }
        $timekeeping = $this->timekeeping->getTimekeepingToday($context->id, $args['group_id']);
        $currentTime = date('H:i');
        $data = [
            'date'        => date('Y-m-d'),
            'group_id'    => $args['group_id'],
            'user_id'     => $context->id,
            'shift_name'  => $shift->name,
            'shift_type'  => $shift->type,
            'target_time' => $shift->times
        ];
        $time = [
            'time'     => $currentTime,
            'location' => $args['location']
        ];

        return $this->create($data, $time, $shift, $timekeeping);
    }

    /**
     * @param  array $data
     * @param  array $time
     * @param  App\Models\Shift $shift
     * @param  App\Models\TimeKeeping $timekeeping
     * @return App\Models\TimeKeeping
     */
    public function create($data, $time, $shift, $timekeeping)
    {
        if (strtotime($time['time']) <= strtotime($shift->times[0]['checkin'])) {
            throw new AuthorizationError(__('time_keeping.over_time_checkout'));
        }
        if (empty($timekeeping)) {
            return $this->handleCheckoutCreate($data, $time, $shift);
        } else {
            return $this->handleCheckoutUpdate($time, $shift, $timekeeping);
        }
    }

    /**
     * @param  array $data
     * @param  array $time
     * @param  App\Models\Shift $shift
     * @return App\Models\TimeKeeping
     */
    private function handleCheckoutCreate($data, $time, $shift)
    {
        if ($shift->type == $this->shift->getSingleType()) {
            $data['times'][0]['checkout'] = $time;
        } else {
            if (strtotime($time['time']) <= strtotime($shift->times[1]['checkin'])) {
                $data['times'][0]['checkout'] = $time;
            } else {
                $data['times'][1]['checkout'] = $time;
            }
        }
        return $this->timekeeping->create($data);
    }

    /**
     * @param  array $time
     * @param  array $shift
     * @param  App\Models\TimeKeeping $timekeeping
     * @return App\Models\TimeKeeping
     */
    private function handleCheckoutUpdate($time, $shift, $timekeeping)
    {
        $data = $timekeeping->times;
        if ($shift->type == $this->shift->getSingleType()) {
            if (!empty($data[0]['checkout'])) {
                throw new AuthorizationError(__('time_keeping.already_checkout'));
            }
            $data[0]['checkout'] = $time;
        } else {
            if (strtotime($time['time']) <= strtotime($shift->times[1]['checkin'])) {
                if (!empty($data[0]['checkout'])) {
                    throw new AuthorizationError(__('time_keeping.already_checkout'));
                }
                $data[0]['checkout'] = $time;
            } else {
                if (!empty($data[1]['checkout'])) {
                    throw new AuthorizationError(__('time_keeping.already_checkout'));
                }
                $data[1]['checkout'] = $time;
            }
        }
        $timekeeping->times = $data;
        $timekeeping->save();
        return $timekeeping;
    }
}