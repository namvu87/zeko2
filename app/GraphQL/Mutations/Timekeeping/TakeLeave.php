<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Timekeeping;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\RequirementContract;
use Rebing\GraphQL\Error\AuthorizationError;

class TakeLeave extends Mutation
{
    protected $attributes = [
        'name' => 'TakeLeaveTimekeeping',
        'description' => 'Gửi yêu cầu xin nghỉ phép'
    ];

    private $requirement;
    
    public function __construct(RequirementContract $requirement)
    {
        $this->requirement = $requirement;
    }

    public function type(): Type
    {
        return GraphQL::type('requirement');
    }

    public function rules(array $args = []): array
    {
        return [
            'content' => 'required|string',
            'date'    => 'required|date_format:Y-m-d|after:today'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'date' => [
                'type' => Type::string()
            ],
            'content' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        if (!in_array($args['group_id'], $context->group_ids)) {
            throw new AuthorizationError('Unauthorixed');
        }
        
        try {
            return $this->requirement->create([
                'creator_id' => $context->id,
                'group_id'   => $args['group_id'],
                'content'    => $args['content'],
                'date'       => $args['date'],
                'type'       => $this->requirement->getTakeLeaveType()
            ]);
        } catch (\Exception $e) {
            throw new AuthorizationError(__('time_keeping.duplicate_requirement'));
        }
    }
}
