<?php
namespace App\GraphQL\Mutations\Timekeeping;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Contracts\RequirementContract;
use App\Contracts\TimekeepingContract;

class AdditionTimekeeping extends Mutation
{
    protected $attributes = [
        'name' => 'AdditionTimekeeping'
    ];

    private $requirement;
    private $timekeeping;

    public function __construct(
        RequirementContract $requirement,
        TimekeepingContract $timekeeping
    )
    {
        $this->requirement = $requirement;
        $this->timekeeping = $timekeeping;
    }

    public function type(): Type
    {
        return GraphQL::type('requirement');
    }

    public function rules(array $args = []): array
    {
        return [
            'content'  => 'required|string',
            'date'     => 'required|date_format:Y-m-d|before:today'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'date' => [
                'type' => Type::string()
            ],
            'content' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        if (!in_array($args['group_id'], $context->group_ids)) {
            throw new AuthorizationError("Unauthorized");
        }
        
        $timekeeping = $this->timekeeping->getMyTimekeepingByDate(
            $context->id,
            $args['group_id'],
            $args['date']
        );

        try {
            return $this->requirement->create([
                'creator_id'     => $context->id,
                'group_id'       => $args['group_id'],
                'content'        => $args['content'],
                'date'           => $args['date'],
                'timekeeping_id' => $timekeeping->id ?? null,
                'type'           => $this->requirement->getAdditionTimekeepingType()
            ]);
        } catch (\Exception $e) {
            throw new AuthorizationError(__('time_keeping.duplicate_requirement'));
        }
    }
}