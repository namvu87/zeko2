<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Kitchen;

use GraphQL\Type\Definition\ResolveInfo;
use App\Models\Invoice;
use App\Models\User;
use Closure;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\Restaurant\InvoiceUpdated;
use Carbon\Carbon;

class UpdateProcessedByInvoice extends Mutation
{
    const PERMISSION = '.invoice.update';

    protected $attributes = [
        'name' => 'KitchenUpdateProcessedByInvoice',
        'description' => 'Cập nhật trạng thái đã chế biến xong cho hoá đơn,
                          hoặc các món trong hoá đơn'
    ];  

    private $invoice;  

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function authorize(
        $root, array $args, $ctx,
        ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool 
    {
        return $ctx->can($args['group_id'] . self::PERMISSION);
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'goods_id' => 'string|required_with:created_at',
            'created_at' => 'string|required_with:goods_id',
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Required khi cập nhật 1 hoá đơn'
            ],
            'goods_id' => [
                'type' => Type::string(),
                'description' => 'Required khi cập nhật tất cả hoá đơn chứa goods_id'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Required khi cập nhật 1 order item'
            ]
        ];
    }

    /**
     * Nếu có đối số created_at -> cập nhật 1 order item theo goodsId và create_at
     * Mặc định cập nhật theo ID hoá đơn
     */
    public function resolve($root, $args, $context)
    {
        $this->checkVerifyData($args['invoice_id'], $args['group_id']);

        if (isset($args['created_at'])) {
            // $this->invoice->updateProcessedOrderItem(
            //     $args['invoice_id'],
            //     $args['goods_id'],
            //     $args['created_at']
            // );
            $invoice = $this->notifyUpdateProcessedOrderItem($args, $context);
        } else {
            $invoice = $this->notifyUpdateProcessedInvoice($args, $context);

            // $this->invoice->updateProcessedInvoice($args['invoice_id']);
        }

        $invoice = $this->invoice->getById($args['invoice_id']);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_43,
            InvoiceUpdated::FROM_KITCHEN,
            $args['except_token'] ?? ''
        ))->toOthers();

        return $invoice;
    }

    private function checkVerifyData($invoiceId, $groupId)
    {
        $invoice = $this->invoice->getById($invoiceId);

        if ($invoice->group_id !== $groupId ||
            $invoice->status !== $this->invoice->getStatusInited()) {
            throw new AuthorizationError('Unauthorized');
        }
    }

    /**
     * Cập nhật thông báo khi thya đổi trạng thái đã chế biến cho cả hoá đơn
     *
     * @param array $args
     * @param User $context
     * @return App\Models\Invoice
     */
    private function notifyUpdateProcessedInvoice(array $args, User $context): Invoice
    {
        $invoice = $this->invoice->getById($args['invoice_id']);
        $notifications = $invoice->notifications ?? [];
        $goodsArray = $invoice->goods ?? [];
        $notificationItems = [
            'from_type' => InvoiceUpdated::FROM_KITCHEN,
            'type' => InvoiceUpdated::TYPE_55,
            'created_at' => Carbon::now()->toDateTimeString(),
            'creator' => [
                'id' => $context->id,
                'fullname' => $context->fullname,
                'avatar' => $context->avatars ? $context->avatars['x2'] : null
            ],
            'is_execute' => false
        ];

        foreach ($goodsArray as &$goods) {
            foreach ($goods['orders'] as &$order) {
                if ($order['status'] === $this->invoice->getGoodBookedStatus()) {
                    $order['status'] = $this->invoice->getGoodProcessedStatus();
                    $notificationItems['goods'][] = [
                        'id'    => $goods['id'],
                        'name'  => $goods['name'],
                        'unit'  => $goods['unit'],
                        'count' => $order['count']
                    ];
                }
            }
        }
        $notifications[] = $notificationItems;
        return $this->invoice->save($invoice, [
            'goods' => $goodsArray,
            'notifications' => $notifications
        ]);
    }

    /**
     * Cập nhật thông báo hoá đơn khi update trạng thái đã chế biến cho 1 order item 
     *
     * @param array $args
     * @param App\Models\User $context
     * @return App\Models\Invoice
     */
    private function notifyUpdateProcessedOrderItem(array $args, User $context): Invoice
    {
        $invoice = $this->invoice->getById($args['invoice_id']);
        // $notifications = $invoice->notifications ?? [];
        $goodsArray = $invoice->goods ?? [];

        foreach ($goodsArray as &$goods) {
            if ($goods['id'] !== $args['goods_id']) {
                continue;
            }
            foreach ($goods['orders'] as &$order) {
                if ($order['created_at'] !== $args['created_at']) {
                    continue;
                }
                $order['status'] = $this->invoice->getGoodProcessedStatus();
                // $notifications[] = [
                //     'from_type' => InvoiceUpdated::FROM_KITCHEN,
                //     'type' => InvoiceUpdated::TYPE_55,
                //     'created_at' => Carbon::now()->toDateTimeString(),
                //     'creator' => [
                //         'id' => $context->id,
                //         'fullname' => $context->fullname,
                //         'avatar' => $context->avatars ? $context->avatars['x2'] : null
                //     ],
                //     'goods' => [
                //         [
                //             'id'    => $goods['id'],
                //             'name'  => $goods['name'],
                //             'unit'  => $goods['unit'],
                //             'count' => $order['count']
                //         ]
                //     ],
                //     'is_execute' => false
                // ];
            }
        }
        return $this->invoice->save($invoice, [
            'goods' => $goodsArray,
            // 'notifications' => $notifications
        ]);
    }
}
