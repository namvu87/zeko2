<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Kitchen;

use GraphQL\Type\Definition\ResolveInfo;
use Closure;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Events\Restaurant\InvoiceUpdated;
use Carbon\Carbon;

class UpdateProcessedByGoods extends Mutation
{
    const PERMISSION = '.invoice.update';

    protected $attributes = [
        'name' => 'KitchenUpdateProcessedByGoods',
        'description' => 'Cập nhật trạng thái đã chế biến xong cho các goods items theo goodsId'
    ];  

    private $invoice;  

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool 
    {
        return $ctx->can($args['group_id'] . self::PERMISSION);
    }

    public function type(): Type
    {
        return type::listOf(GraphQL::type('invoice'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $invoices = $this->invoice->getInvoiceByGoodsStatus(
            $args['group_id'],
            $args['goods_id'],
            $this->invoice->getGoodBookedStatus()
        );

        foreach ($invoices as &$invoice) {
            // $notifications = $invoice->notifications ?? [];
            $goodsArray = $invoice->goods ?? [];

            foreach ($goodsArray as &$goods) {
                if ($goods['id'] !== $args['goods_id']) {
                    continue;
                }
                foreach ($goods['orders'] as &$order) {
                    if ($order['status'] !== $this->invoice->getGoodBookedStatus()) {
                        continue;
                    }
                    $order['status'] = $this->invoice->getGoodProcessedStatus();
                    // $notifications[] = [
                    //     'from_type' => InvoiceUpdated::FROM_KITCHEN,
                    //     'type' => InvoiceUpdated::TYPE_55,
                    //     'created_at' => Carbon::now()->toDateTimeString(),
                    //     'creator' => [
                    //         'id' => $context->id,
                    //         'fullname' => $context->fullname,
                    //         'avatar' => $context->avatars ? $context->avatars['x2'] : null
                    //     ],
                    //     'goods' => [
                    //         [
                    //             'id'    => $goods['id'],
                    //             'name'  => $goods['name'],
                    //             'unit'  => $goods['unit'],
                    //             'count' => $order['count']
                    //         ]
                    //     ],
                    //     'is_execute' => false
                    // ];
                }
            }
            $invoice = $this->invoice->save($invoice, [
                // 'notifications' => $notifications,
                'goods'         => $goodsArray
            ]);

            // Fire event invoice updated for each invoice instance
            broadcast(new InvoiceUpdated(
                $invoice,
                $context,
                InvoiceUpdated::TYPE_43,
                InvoiceUpdated::FROM_KITCHEN,
                $args['except_token'] ?? ''
            ))->toOthers();
        }
        return $invoices;
    }
}
