<?php
namespace App\GraphQL\Mutations\Shift;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\ShiftContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class Delete extends Mutation
{
    use Authorize;

    const PERMISSION = ".shift.delete";
    const SUCCESS_STATUS = 200;
    const UNAUTHORIZED = 403;

    protected $attributes = [
        'name' => 'delete shift'
    ];

    private $shift;

    public function __construct(ShiftContract $shift)
    {
        $this->shift = $shift;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'shift_id' => 'required|string|exists:shifts,_id'
        ];
    }

    public function args(): array
    {
        return [
            'shift_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ],

        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $shift = $this->shift->getById($args['shift_id']);

        if ($shift->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }

        $this->shift->delete($args['shift_id']);

        return [
            'code' => self::SUCCESS_STATUS
        ];
    }
}