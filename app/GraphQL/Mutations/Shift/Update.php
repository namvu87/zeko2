<?php
namespace App\GraphQL\Mutations\Shift;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\ShiftContract;
use App\GraphQL\Traits\Authorize;
use App\GraphQL\Traits\ShiftRequest;
use Rebing\GraphQL\Error\AuthorizationError;

class Update extends Mutation
{
    use Authorize, ShiftRequest;

    const PERMISSION = ".shift.update";

    protected $attributes = [
        'name' => 'UpdateShift'
    ];

    private $shift;

    public function __construct(ShiftContract $shift)
    {
        $this->shift = $shift;
    }

    public function type(): Type
    {
        return GraphQL::type('shift');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => Type::int()
            ],
            'allow_time' => [
                'type' => Type::int()
            ],
            'schedules' => [
                'type' => Type::listOf(Type::string())
            ],
            'start_time_1' => [
                'type' => Type::string()
            ],
            'end_time_1' => [
                'type' => Type::string()
            ],
            'start_time_2' => [
                'type' => Type::string()
            ],
            'end_time_2' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $shift = $this->shift->getById($args['id']);

        if (empty($shift) || $shift->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }

        $data = $this->prepareData($args);
        $this->shift->updateInstance($shift, $data);

        return $this->shift->getById($args['id']);
    }
}