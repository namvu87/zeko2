<?php
namespace App\GraphQL\Mutations\Shift;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\ShiftContract;
use App\GraphQL\Traits\Authorize;
use App\GraphQL\Traits\ShiftRequest;

class Create extends Mutation
{
    use Authorize, ShiftRequest;

    const PERMISSION = ".shift.create";

    protected $attributes = [
        'name' => 'CreateShift'
    ];

    private $shift;

    public function __construct(ShiftContract $shift)
    {
        $this->shift = $shift;
    }

    public function type(): Type
    {
        return GraphQL::type('shift');
    }

    public function args(): array
    {
        return [
            'name' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => Type::int()
            ],
            'allow_time' => [
                'type' => Type::int()
            ],
            'schedules' => [
                'type' => Type::listOf(Type::string())
            ],
            'start_time_1' => [
                'type' => Type::string()
            ],
            'end_time_1' => [
                'type' => Type::string()
            ],
            'start_time_2' => [
                'type' => Type::string()
            ],
            'end_time_2' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $data = $this->prepareData($args);
        return $this->shift->create($data);
    }
}