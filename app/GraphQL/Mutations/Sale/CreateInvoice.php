<?php
namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\TableContract;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\Restaurant\InvoiceCreated;

class CreateInvoice extends Mutation
{
    use Authorize;

    const PERMISSION = ".invoice.create";

    protected $attributes = [
        'name' => 'SaleCreateInvoice'
    ];

    private $invoice;
    private $good;
    private $table;

    public function __construct(
        InvoiceContract $invoice,
        GoodContract $good,
        TableContract $table
    )
    {
        $this->invoice = $invoice;
        $this->good = $good;
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'    => 'required|string',
            'table_ids'   => 'nullable|array',
            'table_ids.*' => [
                'required', 'string',
                Rule::exists('tables', '_id')->where(function($query) use ($args) {
                    $query->where('group_id', $args['group_id'])
                          ->where('status', $this->table->getAvailableType());
                })
            ],
            'goods'         => 'required|array',
            'goods.*.id'    => 'required|string',
            'goods.*.count' => 'required|integer|min:1'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'table_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'goods' => [
                'type' => Type::listOf(GraphQL::type('InvoiceGoodInput'))
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);
        
        $data = [
            'code'           => $this->invoice->generateCode($args['group_id']),
            'group_id'       => $args['group_id'],
            'table_ids'      => $args['table_ids'] ?? [],
            'goods'          => $this->resolveGoodsField($args['goods'], $args['group_id']),
            'status'         => $this->invoice->getStatusInited(),
            'request_status' => $this->invoice->getCreateInvoiceRequestStatus(),
            'note'           => '',
            'discount'       => 0,
            'discount_type'  => $this->invoice->getDiscountTypeFixed()
        ];

        $invoice = $this->invoice->create($data);

        broadcast(new InvoiceCreated(
            $invoice,
            $context,
            InvoiceCreated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();

        return $invoice;
    }

    /**
     * Tạo trường hàng hoá chuẩn dữ liệu
     *
     * @param  array $goods
     * @return array
     */
    private function resolveGoodsField(array $goods, string $groupId): array
    {
        $data = [];
        foreach ($goods as $item) {
            $good = $this->good->getById($item['id']);
            if ($good->group_id !== $groupId) {
                throw new AuthorizationError('Unauthorized');
            }
            $data[] = [
                'id'            => $good->id,
                'code'          => $good->code,
                'name'          => $good->name,
                'image'         => $good->images[0] ?? '',
                'price'         => $good->price ?? 0,
                'price_origin'  => $good->price_origin ?? 0,
                'unit'          => $good->unit,
                'discount'      => $good->discount ?? 0,
                'discount_type' => $good->discount_type ?? 1,
                'count'         => (int) $item['count'],
                'orders' => [
                    [
                        'count' => (int) $item['count'],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'status' => $this->invoice->getGoodBookedStatus()
                    ]
                ]
            ];
        }
        return $data;
    }
}
