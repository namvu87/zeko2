<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\TableContract;
use App\Contracts\Restaurant\InvoiceContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\GraphQL\Traits\Authorize;
use App\Events\Restaurant\InvoiceUpdated;

class MergeInvoices extends Mutation
{
    use Authorize;
    const PERMISSION = '.invoice.update';

    protected $attributes = [
        'name' => 'SaleMergeInvoices',
        'description' => 'Gộp nhiều đơn vào 1 đơn hiện tại, vẫn giữ nguyên trạng thái bàn'
    ];

    private $invoice;
    private $table;

    public function __construct(InvoiceContract $invoice, TableContract $table)
    {
        $this->invoice = $invoice;
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);
        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->group_id !== $args['group_id'] ||
            $invoice->status !== $this->invoice->getStatusInited()) {
            throw new AuthorizationError('Unauthorized');
        }

        $totalDiscount = $this->resolveDiscount($invoice->discount ?? 0, $invoice->discount_type ?? 0);
        $userIds = $invoice->user_ids ?? [];
        $tableIds = $invoice->table_ids ?? [];
        $items = $invoice->goods;
        $itemIds = array_column($items, 'id');

        $invoices = $this->invoice->getByIds($args['invoice_ids']);

        foreach ($invoices as $newInvoice) {
            if ($newInvoice->group_id !== $args['group_id'] ||
                $newInvoice->status !== $this->invoice->getStatusInited()) {
                throw new AuthorizationError('Unauthorized');
            }

            $tableIds = array_merge($tableIds, $newInvoice->table_ids ?? []);
            $userIds = array_unique(array_merge($userIds, $newInvoice->user_ids ?? []));
            $totalDiscount += $this->resolveDiscount(
                $newInvoice->discount ?? 0,
                $newInvoice->discount_type ?? 1
            );

            foreach ($newInvoice->goods as $goods) {
                if (!in_array($goods['id'], $itemIds)) {
                    $items[] = $goods;
                    $itemIds[] = $goods['id'];
                    break;
                }

                // Gộp các orders item và số lượng các items
                $items = collect($items)->map(function ($item) use ($goods) {
                    if ($item['id'] === $goods['id']) {
                        $item['count'] += $goods['count'];
                        $item['orders'] = array_merge($item['orders'], $goods['orders']);

                        // Sắp xếp lại theo thức tự thời gian
                        $item['orders'] = collect($item['orders'])
                                            ->sortBy('created_at')->values()->all();
                    }
                    return $item;
                })->all();
            }
        }

        $this->invoice->updateInstance($invoice, [
            'user_ids'      => $userIds,
            'table_ids'     => $tableIds,
            'discount'      => $totalDiscount,
            'discount_type' => $this->invoice->getDiscountTypeFixed(),
            'goods'         => $items
        ]);

        $this->invoice->destroy($args['invoice_ids']);
        $this->table->massUpdate($tableIds, [
            'invoice_id' => $invoice->id
        ]);

        $invoice = $this->invoice->getById($args['invoice_id']);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,            
            InvoiceUpdated::TYPE_47,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? '',
            $args['invoice_ids']
        ))->toOthers();

        return $invoice;
    }

    /**
     * Lấy giá trị giảm giá
     *
     * @param  int    $discount
     * @param  int    $discountType
     * @return int
     */
    private function resolveDiscount(?float $discount = 0, ?float $discountType = 1): float
    {
        if ($discountType && $discountType === $this->invoice->getDiscountTypeFixed()) {
            return $discount;
        }
        return $discount * $discountType / 100;
    }
}
