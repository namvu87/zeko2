<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\TableContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\GraphQL\Traits\Authorize;
use App\Events\Restaurant\InvoiceUpdated;

class MergeTables extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.update';

    protected $attributes = [
        'name' => 'SaleMergeTables',
        'description' => 'Gộp nhiều đơn vào đơn hiện tại và trả lại bàn ở những hoá đơn được gộp'
    ];

    private $invoice;
    private $table;

    public function __construct(InvoiceContract $invoice, TableContract $table)
    {
        $this->invoice = $invoice;
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Id hoá đơn hiện tại'
            ],
            'invoice_ids' => [
                'type' => Type::listOf(Type::string()),
                'description' => 'Id các hoá đơn được gộp vào'
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);
        $invoice = $this->invoice->getById($args['invoice_id']);

        // Kiểm tra trạng thái hoá đơn và xác thực dữ liệu
        if ($invoice->group_id !== $args['group_id'] ||
            $invoice->status !== $this->invoice->getStatusInited()) {
            throw new AuthorizationError('Unauthorized');
        }

        // Giảm giá của hoá đơn hiện tại
        $totalDiscount = $this->resolveDiscount($invoice->discount ?? 0, $invoice->discount_type ?? 0);

        // User của hoá đơn hiện tại
        $userIds = $invoice->user_ids ?? [];

        // Bàn được trả ra
        $availableTables = [];

        $items = $invoice->goods;
        $itemIds = array_column($items, 'id');

        // Các hoá đơn được gộp vào
        $invoices = $this->invoice->getByIds($args['invoice_ids']);

        foreach ($invoices as $newInvoice) {
            // Kiểm tra trạng thái hoá đơn và xác thực dữ liệu
            if ($newInvoice->group_id !== $args['group_id'] ||
                $newInvoice->status !== $this->invoice->getStatusInited()) {
                throw new AuthorizationError('Unauthorized');
            }

            $availableTables = array_merge($availableTables, $newInvoice->table_ids ?? []);
            $userIds = array_unique(array_merge($userIds, $newInvoice->user_ids ?? []));

            // Tính tổng giá trị discount. Discount của hoá đơn sau cùng sẽ bằng tổng
            // giá trị các discount của từng hoá đơn
            $totalDiscount += $this->resolveDiscount(
                $newInvoice->discount ?? 0,
                $newInvoice->discount_type ?? 0
            );

            foreach ($newInvoice->goods as $goods) {
                if (!in_array($goods['id'], $itemIds)) {
                    $items[] = $goods;
                    $itemIds[] = $goods['id'];
                    break;
                }

                // Gộp các orders item và số lượng các items
                $items = collect($items)->map(function ($item) use ($goods) {
                    if ($item['id'] === $goods['id']) {
                        $item['count'] += $goods['count'];
                        $item['orders'] = array_merge($item['orders'], $goods['orders']);

                        // Sắp xếp lại theo thức tự thời gian
                        $item['orders'] = collect($item['orders'])
                                            ->sortBy('created_at')->values()->all();
                    }
                    return $item;
                })->all();
            }
        }

        $invoice = $this->invoice->save($invoice, [
            'user_ids'      => $userIds,
            'discount'      => $totalDiscount,
            'discount_type' => $this->invoice->getDiscountTypeFixed(),
            'goods'         => $items
        ]);

        // Xoá mềm các hoá đơn được gộp
        $this->invoice->destroy($args['invoice_ids']);

        // Trả lại trạng thái bàn
        $this->table->massUpdate($availableTables, [
            'status'     => $this->table->getAvailableType(),
            'invoice_id' => ''
        ]);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_46,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? '',
            $args['invoice_ids']
        ))->toOthers();

        return $invoice;
    }

    /**
     * Lấy giá trị giảm giá
     *
     * @param  int    $discount
     * @param  int    $discountType
     * @return int
     */
    private function resolveDiscount(?float $discount = 0, ?float $discountType = 1): float
    {
        if ($discountType && $discountType === $this->invoice->getDiscountTypeFixed()) {
            return $discount;
        }
        return $discount * $discountType / 100;
    }
}
