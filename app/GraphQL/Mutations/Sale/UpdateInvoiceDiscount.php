<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\Authorize;
use App\Events\Restaurant\InvoiceUpdated;

class UpdateInvoiceDiscount extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.update';
    private $invoice;

    protected $attributes = [
        'name' => 'SaleUpdateInvoiceDiscount',
        'description' => 'Cập nhật khuyến mại cho hoá đơn'
    ];

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'   => 'required|string',
            'invoice_id' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'discount' => [
                'type' => Type::nonNull(Type::float())
            ],
            'discount_type' => [
                'type' => Type::nonNull(Type::int())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->group_id !== $args['group_id'] || $invoice->status === $this->invoice->getStatusPurchased()) {
            throw new AuthorizationError('Unauthorized');
        }
        $this->invoice->updateInstance($invoice, [
            'discount'      => $args['discount'],
            'discount_type' => $args['discount_type']
        ]);
        $invoice = $this->invoice->getById($args['invoice_id']);
        
        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_50,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();
        
        return $invoice;
    }
}
