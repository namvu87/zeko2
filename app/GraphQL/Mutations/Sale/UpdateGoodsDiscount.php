<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use Closure;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\Authorize;
use App\Events\Restaurant\InvoiceUpdated;

class UpdateGoodsDiscount extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.update';

    protected $attributes = [
        'name' => 'SaleUpdateGoodsDiscount',
        'description' => 'Cập nhật discount cho goods trong invoice'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function args(): array
    {
        return [
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'discount' => [
                'type' => Type::nonNull(Type::float())
            ],
            'discount_type' => [
                'type' => Type::nonNull(Type::int())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $invoice = $this->invoice->getById($args['invoice_id']);
        if ($invoice->group_id !== $args['group_id'] && $invoice->status !== $this->invoice->getStatusInited()) {
            throw new AuthorizationError('Unauthorized');
        }

        $this->invoice->updateOperator(
            $args['invoice_id'],
            ['goods.id' => $args['goods_id']],
            [
                "goods.$.discount" => $args['discount'],
                "goods.$.discount_type" => $args['discount_type'],
            ]
        );
        $invoice = $this->invoice->getById($args['invoice_id']);
        
        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_50,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();

        return $invoice;
    }
}
