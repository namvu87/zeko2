<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;

class AsyncGoodsItemsStatus extends Mutation
{
    protected $attributes = [
        'name' => 'SaleAsyncGoodsItemsStatus',
        'description' => 'Cập nhật trạng thái của items khi có 1 goods thay đổi trạng thái'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('invoice'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'is_disable' => [
                'type' => Type::nonNull(Type::boolean())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->invoice->asyncGoodsItemsStatus(
            $args['group_id'],
            $args['goods_id'],
            $args['is_disable']
        );

        return $this->invoice->getInitedInvoicesHasGoodId($args['group_id'], $args['goods_id']);
    }
}
