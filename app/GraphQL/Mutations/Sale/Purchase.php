<?php

namespace App\GraphQL\Mutations\Sale;

use Carbon\Carbon;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\TableContract;
use App\GraphQL\Traits\Authorize;
use App\Events\Restaurant\InvoiceDestroyed;
use App\GraphQL\Traits\CalculatePrice;
use Rebing\GraphQL\Error\AuthorizationError;

class Purchase extends Mutation
{
    use Authorize, CalculatePrice;

    const PERMISSION     = ".invoice.checkout";
    const SUCCESS_STATUS = 200;
    const ERROR_STATUS   = 400;

    protected $attributes = [
        'name' => 'PurchaseInvoice'
    ];

    private $invoice;
    private $table;

    public function __construct(InvoiceContract $invoice, TableContract $table)
    {
        $this->invoice = $invoice;
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'   => 'required|string',
            'invoice_id' => 'required|string',
        ];
    }

    public function args(): array
    {
        return [
            'group_id'     => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id'   => [
                'type' => Type::nonNull(Type::string())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $invoice = $this->invoice->getById($args['invoice_id']);

        if (
            !$invoice || $invoice->group_id !== $args['group_id'] ||
            $invoice->status !== $this->invoice->getStatusInited()
        ) {
            throw new AuthorizationError('Unauthorized');
        }

        list($goods, $total, $totalOrigin) = $this->resolveGoodsField($invoice->goods);

        $discount = $this->resolveDiscount($total, $invoice);

        $this->invoice->update($args['invoice_id'], [
            'goods'          => $goods,
            'total_origin'   => $totalOrigin,
            'total_discount' => $discount,
            'total'          => $total - $discount,
            'recipient_id'   => $context->id,
            'payment_method' => config('const.payment_method.cash'),
            'paid_at'        => Carbon::now(),
            'status'         => $this->invoice->getStatusPurchased(),
        ]);

        $this->table->massUpdate($invoice->table_ids ?? [], [
            'status'     => $this->table->getAvailableType(),
            'invoice_id' => ''
        ]);

        broadcast(new InvoiceDestroyed(
            $invoice,
            $context,
            InvoiceDestroyed::TYPE_51,
            $args['except_token'] ?? ''
        ))->toOthers();

        return $invoice;
    }

    /**
     * Nhóm các good lại
     *
     * @param  array $goods
     *
     * @return array
     */
    private function resolveGoodsField(array $goods): array
    {
        $total = 0;       // Tổng giá trị của các items trong hoá đơn
        $totalOrigin = 0; // Tổng giá nhập của các itém trong hoá đơn

        foreach ($goods as &$item) {
            // $this->checkValidItem($item);
            unset($item['orders']);

            // Xác định giá trị giảm giá
            $discount = $this->getDiscountGoods($item);
            $item['total_discount'] = $discount * $item['count'];

            // Tổng giá trị cần thanh toán của item
            $item['total'] = $item['count'] * ($item['price'] - $discount);

            // Tổng giá nhập của item
            $item['total_origin'] = $item['count'] * $item['price_origin'];

            $total += $item['total'];
            $totalOrigin += $item['total_origin'];
        }

        return [$goods, $total, $totalOrigin];
    }

    /**
     * Xác đinh giá trị thực của hoá đơn
     *
     * @param  int     $total
     * @param  Invoice $invoice
     *
     * @return int
     */
    private function resolveTotal(int $total, $invoice): int
    {
        if (empty($invoice->discount)) {
            return $total;
        }
        if ($invoice->discount_type === $this->invoice->getDiscountTypeFixed()) {
            return $total - $invoice->discount;
        }
        return $total - ($invoice->discount * $total) / 100;
    }

    /**
     * Xác định giảm giá của hoá đơn
     *
     * @param integer            $total
     * @param App\Models\Invoice $invoice
     *
     * @return integer
     */
    private function resolveDiscount(int $total, $invoice): int
    {
        if (empty($invoice->discount)) {
            return 0;
        }
        if ($invoice->discount_type === $this->invoice->getDiscountTypeFixed()) {
            return $invoice->discount;
        }
        return ($invoice->discount * $total) / 100;
    }

    /**
     * Kiểm tra xem tất cả các orders item đã được giao hay chưa
     *
     * @param array $item
     *
     * @return void
     */
    private function checkValidItem(array $item)
    {
        foreach ($item['orders'] as $orderItem) {
            if ($orderItem['status'] !== $this->invoice->getDeliveredStatus()) {
                throw new AuthorizationError('Unauthorized');
            }
        }
    }
}
