<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\Authorize;
use App\Events\Restaurant\InvoiceUpdated;

class DeleteGoodsItem extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.update';

    protected $attributes = [
        'name' => 'SaleDeleteGoodsItem',
        'description' => 'Xoá goods items (chỉ goods có trạng thái đã đặt)'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'   => 'required|string',
            'invoice_id' => 'required|string',
            'goods_id'   => 'required|string',
            'created_at' => 'required|string',
            'count'      => 'required|integer'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods_id' => [
                'type' => Type::string()
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Thời gian khởi tạo của orders item'
            ],
            'count' => [
                'type' => Type::int(),
                'description' => 'Số lượng của orders item'
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    /**
     * Nếu mảng goods hiện tại có nhiều hơn 1 orders item thì xoá orders item đó,
     * nếu chỉ mảng goods chỉ có 1 item thì cả goods item
     */
    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $invoice = $this->invoice->getById($args['invoice_id']);
        
        if ($invoice->group_id !== $args['group_id'] ||
            $invoice->status !== $this->invoice->getStatusInited()
        ) {
            throw new AuthorizationError('Unauthorized');
        }

        $goods = collect($invoice->goods)->where('id', $args['goods_id'])->first();
                        
        if (count($goods['orders']) > 1) {
            $this->invoice->removeGoodsOrdersItem(
                $args['invoice_id'],
                $args['goods_id'],
                $args['created_at'],
                $args['count']
            );
        } else {
            $this->invoice->removeGoodsItem(
                $args['invoice_id'],
                $args['goods_id']
            );
        }

        $invoice = $this->invoice->getById($args['invoice_id']);
        
        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_49,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''            
        ))->toOthers();

        return $invoice;
    }
}
