<?php
namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\GoodContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use App\GraphQL\Traits\AddGoodsInvoice;
use App\Events\Restaurant\InvoiceUpdated;
use Carbon\Carbon;

class AdditionGood extends Mutation
{
    use Authorize, AddGoodsInvoice;

    const PERMISSION = ".invoice.update";

    protected $attributes = [
        'name' => 'SaleAddGoodInvoice',
        'description' => 'Đặt thêm món'
    ];

    private $invoice;
    private $good;

    public function __construct(InvoiceContract $invoice, GoodContract $good)
    {
        $this->invoice = $invoice;
        $this->good = $good;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'   => 'required|string',
            'invoice_id' => 'required|string',
            'goods'      => 'required|array'
        ];
    }

    public function args(): array
    {
        return [
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods' => [
                'type' => Type::listOf(GraphQL::type('InvoiceGoodInput'))
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    /**
     * Tạo hoá đơn
     * Kiểm tra xem hoá đơn này có thuộc về người dùng hay không,
     * hoặc người dùng phải là người có quyền bán hàng trong nhóm
     *
     * @param  $root
     * @param  array $args
     * @return App\Models\Invoice
     */
    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);
        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->group_id !== $args['group_id'] ||
            $invoice->status === $this->invoice->getStatusPurchased()) {
                throw new AuthorizationError('Unauthorized');
        }

        
        list($goods, $notificationItems) = $this->resolveGoodsField(
            $args['goods'], $invoice->goods, $args['group_id']
        );

        $notifications = $invoice->notifications ?? [];
        $notifications[] = [
            'from_type' => InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            'type' => InvoiceUpdated::TYPE_42,
            'created_at' => Carbon::now()->toDateTimeString(),
            'creator' => [
                'id' => $context->id,
                'fullname' => $context->fullname,
                'avatar' => $context->avatars ? $context->avatars['x2'] : null
            ],
            'goods' => $notificationItems,
            'is_execute' => false
        ];

        $invoice = $this->invoice->save($invoice, [
            'request_status' => $this->invoice->getAdditionFoodRequestStatus(),
            'goods'          => $goods,
            'notifications'  => $notifications
        ]);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_42,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();

        return $invoice;
    }
}