<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\Restaurant\InvoiceUpdated;

class UpdateItemCount extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.update';

    private $invoice;

    protected $attributes = [
        'name' => 'SaleUpdateItemCount',
        'description' => 'Cập nhật số lượng cho goods (chỉ cho phép khi trạng thái goods là `đã đặt`)'
    ];

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods_count' => [
                'type' => Type::nonNull(Type::int())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->group_id !== $args['group_id'] && $invoice->status !== $this->invoice->getStatusInited()) {
            throw new AuthorizationError('Unauthorized');
        }

        $booked = $this->invoice->getGoodBookedStatus();
        $this->invoice->updateOperator(
            $args['invoice_id'],
            ['goods.id' => $args['goods_id']],
            ["goods.$.$booked.count" => $args['goods_count']]
        );

        $invoice = $this->invoice->getById($args['invoice_id']);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_44,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();
        
        return $invoice;
    }
}
