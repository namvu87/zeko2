<?php
namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\Restaurant\RequestAccepted;

class AcceptRequest extends Mutation
{
    use Authorize;

    const PERMISSION = ".invoice.update";
    const SUCCESS_STATUS = 200;

    protected $attributes = [
        'name' => 'AcceptInvoiceRequest'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'notification_index' => [
                'type' => Type::nonNull(Type::int())
            ],
            'except_token' => [
                'type' => Type::string()
            ],
            'from_type' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Xác định xem yêu cầu từ user hay từ nhà bếp'
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $index = $args['notification_index'];
        $this->invoice->massUpdate([$args['invoice_id']], [
            "notifications.{$index}.is_execute" => true
        ]);

        $invoice = $this->invoice->getById($args['invoice_id']);
        
        broadcast(new RequestAccepted(
            $invoice,
            $context,
            $args['from_type'],
            $args['except_token'] ?? ''
        ))->toOthers();

        return $invoice;
    }
}