<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\TableContract;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\Restaurant\InvoiceUpdated;

class ChangeTable extends Mutation
{
    use Authorize;

    const PERMISSION = ".invoice.update";

    protected $attributes = [
        'name' => 'SaleChangeTable',
        'description' => 'Thay đổi bàn cho hoá đơn'
    ];

    private $invoice;
    private $table;

    public function __construct(InvoiceContract $invoice, TableContract $table)
    {
        $this->invoice = $invoice;
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'    => 'required|string',
            'invoice_id'  => 'required|string',
            'table_ids'   => 'required|array',
            'table_ids.*' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'table_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);
        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->group_id !== $args['group_id'] || $invoice->status === $this->invoice->getStatusPurchased()) {
            throw new AuthorizationError('Unauthorized');
        }

        $availableTables = array_filter($invoice->table_ids, function ($tableId) use ($args) {
            return !in_array($tableId, $args['table_ids']);
        });

        $this->invoice->updateInstance($invoice, [
            'table_ids' => $args['table_ids']
        ]);

        $this->table->massUpdate($args['table_ids'], [
            'status' => $this->table->getActingType(),
            'invoice_id' => $invoice->id
        ]);
        $this->table->massUpdate($availableTables, [
            'status' => $this->table->getAvailableType(),
            'invoice_id' => ''
        ]);

        $invoice = $this->invoice->getById($args['invoice_id']);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_45,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();
        
        return $invoice;
    }
}
