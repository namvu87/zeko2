<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use Carbon\Carbon;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Contracts\Restaurant\ReturnedInvoiceContract;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\CalculatePrice;

class PurchaseReturnInvoice extends Mutation
{
    use Authorize, CalculatePrice;

    const PERMISSION = '.returned-invoice.create';

    protected $attributes = [
        'name'        => 'PurchaseReturnInvoice',
        'description' => 'Thanh toán hoá đơn trả hàng'
    ];

    private $invoice;
    private $returnedInvoice;

    public function __construct(InvoiceContract $invoice, ReturnedInvoiceContract $returnedInvoice)
    {
        $this->returnedInvoice = $returnedInvoice;
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods' => [
                'type' => Type::listOf(GraphQL::type('InvoiceGoodInput'))
            ],
            'note' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $invoice = $this->invoice->getById($args['invoice_id']);

        if (empty($invoice) || $invoice->group_id !== $args['group_id']) {
            throw new AuthorizationError('Unauthorized');
        }

        list($returnInvoiceItems, $invoiceItems, $total) = $this->resolveGoodsField(
            $args['goods'],
            $invoice
        );

        $this->returnedInvoice->create([
            'group_id'   => $args['group_id'],
            'code'       => $this->returnedInvoice->generateCode($args['group_id']),
            'invoice_id' => $args['invoice_id'],
            'status'     => $this->returnedInvoice->getStatusPaid(),
            'creator_id' => $context->id,
            'total'      => $total,
            'note'       => $args['note'] ?? '',
            'goods'      => $returnInvoiceItems
        ]);

        $this->invoice->massUpdate([$invoice->id], $invoiceItems);

        return ['code' => 200];
    }

    /**
     * Tạo trường hàng hoá chuẩn dữ liệu
     *
     * @param  array $goods
     *
     * @return array
     */
    private function resolveGoodsField(array $goods, $invoice): array
    {
        $invoiceItems = [];        // Mảng cập nhật cho invoice hiện tại
        $returnInvoiceItems = [];  // Mảng dữ liệu tạo hoá đơn trả hàng
        $total = 0;                // Tổng giá trị hoá đơn trả hàng

        $invoiceCollect = collect($invoice->goods);
        $itemIds = $invoiceCollect->pluck('id')->all();

        foreach ($goods as $item) {
            if (!in_array($item['id'], $itemIds)) {
                throw new AuthorizationError('Unauthorized');
            }

            // Lấy item khớp trong invoice hiện tại
            $goodsItem = $invoiceCollect->firstWhere('id', $item['id']);

            // Gía bán (sau khi đã trừ giảm gía)
            $price = $this->getSalePriceGoods($goodsItem);

            $returnInvoiceItems[] = [
                'id'    => $item['id'],
                'code'  => $goodsItem['code'],
                'name'  => $goodsItem['name'],
                'image' => $goodsItem['image'],
                'unit'  => $goodsItem['unit'],
                'price' => $price,
                'count' => $item['count']
            ];

            $total += $item['count'] * (int) $price;

            $index = $invoiceCollect->search(function ($goods) use ($item) {
                return $goods['id'] === $item['id'];
            });
            // Tính toán số lượng item còn lại
            $count = $goodsItem['count'] - $item['count'];
            if ($count < 0) {
                throw new AuthorizationError('Unauthorized');
            }
            $invoiceItems["goods.{$index}.count"] = $count;

            // Tổng số lượng đã trả lại của item này
            $invoiceItems["goods.{$index}.count_returned"] =
                ($goodsItem['count_returned'] ?? 0) + $item['count'];

            // Tổng giá trị item trả lại
            $invoiceItems["goods.{$index}.total_returned"] =
                ($goodsItem['total_returned'] ?? 0) + $item['count'] * (int) $price;
        }

        $invoiceItems['total'] = $invoice->total - $total;
        $invoiceItems['total_returned'] = $total;
        $invoiceItems['paid_at'] = Carbon::now();

        return [$returnInvoiceItems, $invoiceItems, $total];
    }
}
