<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\TableContract;
use App\GraphQL\Traits\Authorize;
use App\Events\Restaurant\InvoiceDestroyed;

class DeleteInvoice extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.delete';

    private $invoice;
    private $table;

    protected $attributes = [
        'name' => 'SaleDeleteInvoice',
        'description' => 'Huỷ đơn'
    ];

    public function __construct(InvoiceContract $invoice, TableContract $table)
    {
        $this->invoice = $invoice;
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('response');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);
        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->group_id !== $args['group_id'] ||
            $invoice->status === $this->invoice->getStatusPurchased()) {
            throw new AuthorizationError('Unauthorized');
        }
        $this->invoice->deleteInstance($invoice);
        $this->table->available($invoice->id);

        broadcast(new InvoiceDestroyed(
            $invoice,
            $context,
            InvoiceDestroyed::TYPE_52,
            $args['except_token'] ?? ''
        ))->toOthers();

        return [
            'code' => 200
        ];
    }
}
