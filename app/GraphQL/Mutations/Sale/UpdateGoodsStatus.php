<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\Restaurant\InvoiceContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\GraphQL\Traits\Authorize;
use Illuminate\Validation\Rule;
use App\Events\Restaurant\InvoiceUpdated;

class UpdateGoodsStatus extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.update';

    protected $attributes = [
        'name' => 'UpdateGoodsStatus',
        'description' => 'Cập nhật trạng thái món ăn trong hoá đơn'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id' => 'required|string',
            'invoice_id' => 'required|string',
            'target_status' => [
                'required', 'string', 'in:' . implode(',', $this->invoice->getGoodStatus()),
                Rule::notIn([$this->invoice->getGoodBookedStatus()])
            ],
            'good_items' => 'required|array',
            'good_items.*.item_index' => 'required|integer',
            'good_items.*.order_item_index' => 'required|integer'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'good_items' => [
                'type' => Type::listOf(GraphQL::type('InvoiceItemStatusInput'))
            ],
            'target_status' => [
                'type' => Type::nonNull(Type::string())
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);
        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->group_id !== $args['group_id'] ||
            $invoice->status !== $this->invoice->getStatusInited()) {
            throw new AuthorizationError('Unauthorized');
        }

        $data = [];
        foreach ($args['good_items'] as $item) {
            $itemIndex = $item['item_index'];
            $orderItemIndex = $item['order_item_index'];
            $data["goods.{$itemIndex}.orders.{$orderItemIndex}.status"] = $args['target_status'];
        }
        
        $this->invoice->massUpdate([$args['invoice_id']], $data);

        $invoice = $this->invoice->getById($args['invoice_id']);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,
            InvoiceUpdated::TYPE_43,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();

        return $invoice;
    }
}
