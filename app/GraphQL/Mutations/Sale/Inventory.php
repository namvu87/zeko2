<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Carbon\Carbon;
use App\GraphQL\Traits\Authorize;
use App\Contracts\Restaurant\InvoiceContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Events\Restaurant\InvoiceUpdated;

class Inventory extends Mutation
{
    use Authorize;

    const PERMISSION = '.invoice.update';
    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    protected $attributes = [
        'name' => 'SaleInventory',
        'description' => 'Kiểm kê hàng hoá trước khi thanh toán'
    ];

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'               => 'required|string',
            'invoice_id'             => 'required|string',
            'goods_items'            => 'required|array',
            'goods_items.*.goods_id' => 'required|string',
            'goods_items.*.count'    => 'required|int'
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'goods_items' => [
                'type' => Type::listOf(GraphQL::type('GoodsItemsInventory'))
            ],
            'except_token' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $this->customAuthorize($args['group_id'], $context);

        $invoice = $this->invoice->getById($args['invoice_id']);

        if ($invoice->group_id !== $args['group_id'] && 
            $invoice->status !== $this->invoice->getStatusInited()
        ) {
            throw new AuthorizationError('Unauthorized');
        }

        $data = [];
        foreach ($invoice->goods as $goods) {
            foreach ($args['goods_items'] as $item) {
                if ($goods['id'] === $item['goods_id']) {
                    $goods['count'] -= $item['count'];
                    $goods['orders'][] = [
                        'status' => $this->invoice->getReturnedGoodsStatus(),
                        'count' => - $item['count'],
                        'created_at' => Carbon::now()->toDateTimeString()
                    ];
                }
            }
            $data[] = $goods;
        }

        $invoice = $this->invoice->save($invoice, [
            'goods' => $data
        ]);

        broadcast(new InvoiceUpdated(
            $invoice,
            $context,            
            InvoiceUpdated::TYPE_44,
            InvoiceUpdated::FROM_EMPLOYEE_TYPE,
            $args['except_token'] ?? ''
        ))->toOthers();
        
        return $invoice;
    }
}
