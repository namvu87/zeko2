<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Requirement;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\RequirementContract;
use App\Contracts\NotificationContract;
use App\Contracts\TimekeepingContract;
use App\Contracts\ShiftContract;
use App\Contracts\GroupContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\GraphQL\Traits\Timekeeping\Requirement;
use App\Interfaces\Notifiable;

class ApproveTakeLeave extends Mutation implements Notifiable
{
    use Requirement;

    protected $attributes = [
        'name' => 'Requirement/ApproveTakeLeave',
        'description' => 'Đồng ý xin nghỉ phép'
    ];

    private $requirement;
    private $notification;
    private $timekeeping;
    private $shift;
    private $group;
    
    public function __construct(
        RequirementContract $requirement,
        NotificationContract $notification,
        TimekeepingContract $timekeeping,
        ShiftContract $shift,
        GroupContract $group
    )
    {
        $this->requirement = $requirement;
        $this->notification = $notification;
        $this->timekeeping = $timekeeping;
        $this->shift = $shift;
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('requirement');
    }

    public function rules(array $args = []): array
    {
        $rules = [
            'content' => 'nullable|string',
            'checkin_1' => 'required|date_format:H:i',
            'checkout_1' => 'required|date_format:H:i|after:checkin_1',
        ];
        if ($args['shift_type'] === $this->shift->getMultiType()) {
            $rules['checkin_2'] = 'required|date_format:H:i';
            $rules['checkout_2'] = 'required|date_format:H:i|after:checkin_1';
        }
        return $rules;
    }

    public function args(): array
    {
        return [
            'requirement_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'content' => [
                'type' => Type::string()
            ],
            'shift_type' => [
                'type' => Type::nonNull(Type::int())
            ],
            'checkin_1' => [
                'type' => Type::string()
            ],
            'checkout_1' => [
                'type' => Type::string()
            ],
            'checkin_2' => [
                'type' => Type::string()
            ],
            'checkout_2' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $requirement = $this->requirement->getById($args['requirement_id']);
        $shift = $this->shift->getByIdsAndGroup(
            $requirement->creator->shift_ids ?? [], $args['group_id']
        );
        $group = $this->group->getById($args['group_id']);

        if (empty($requirement) ||
            $requirement->group_id !== $args['group_id'] ||
            !is_null($requirement->is_accept)) {
            throw new AuthorizationError('Unauthorized');
        }

        // Nếu không có ca làm việc thì từ chối
        $shift = $this->shift->getByIdsAndGroup(
            $requirement->creator->shift_ids ?? [], $args['group_id']
        );
        if (empty($shift)) {
            $this->notification->create([
                'user_id' => $requirement->creator_id,
                'creator' => [
                    'id' => $context->id,
                    'fullname' => $context->fullname,
                    'avatars' => $context->avatars
                ],
                'group' => [
                    'id' => $group->id,
                    'name' => $group->name
                ],
                'requirement_id' => $args['requirement_id'],
                'type' => self::TYPE_20
            ]);
            return $this->requirement->save($requirement, [
                'res_content' => $args['content'],
                'is_accept' => false
            ]);
        }

        $timekeeping = $this->createTimekeeping(
            $args, $requirement, $shift, $this->timekeeping->typeAddition()
        );

        $this->notification->create([
            'user_id' => $requirement->creator_id,
            'creator' => [
                'id' => $context->id,
                'fullname' => $context->fullname,
                'avatars' => $context->avatars
            ],
            'group' => [
                'id' => $group->id,
                'name' => $group->name
            ],
            'timekeeping_id' => $timekeeping->id,
            'requirement_id' => $args['requirement_id'],
            'type' => self::TYPE_19
        ]);
        
        return $this->requirement->save($requirement, [
            'timekeeping_id' => $timekeeping->id,
            'res_content' => $args['content'],
            'is_accept' => true
        ]);
    }
}
