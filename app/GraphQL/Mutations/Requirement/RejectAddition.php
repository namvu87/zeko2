<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Requirement;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\RequirementContract;
use App\Contracts\NotificationContract;
use App\Contracts\GroupContract;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Interfaces\Notifiable;

class RejectAddition extends Mutation implements Notifiable
{
    protected $attributes = [
        'name' => 'Requirement/RejectAddition',
        'description' => 'Từ chối chấm công bổ sung'
    ];

    private $requirement;
    private $notification;
    private $group;
    
    public function __construct(
        RequirementContract $requirement,
        NotificationContract $notification,
        GroupContract $group
    )
    {
        $this->requirement = $requirement;
        $this->notification = $notification;
        $this->group = $group;
    }

    public function type(): Type
    {
        return GraphQL::type('requirement');
    }

    public function rules(array $args = []): array
    {
        return [
            'content' => 'required|string'
        ];
    }

    public function args(): array
    {
        return [
            'requirement_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'content' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $requirement = $this->requirement->getById($args['requirement_id']);
        $group = $this->group->getById($args['group_id']);

        if (empty($requirement) ||
            $requirement->group_id !== $args['group_id'] ||
            !is_null($requirement->is_accept)) {
            throw new AuthorizationError('Unauthorized');
        }

        $this->notification->create([
            'user_id' => $requirement->creator_id,
            'creator' => [
                'id' => $context->id,
                'fullname' => $context->fullname,
                'avatars' => $context->avatars
            ],
            'group' => [
                'id' => $group->id,
                'name' => $group->name
            ],
            'requirement_id' => $args['requirement_id'],
            'type' => self::TYPE_18
        ]);

        return $this->requirement->save($requirement, [
            'res_content' => $args['content'],
            'is_accept' => false
        ]);
    }
}
