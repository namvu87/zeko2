<?php

namespace App\GraphQL\Mutations\Requirement;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Contracts\RequirementContract;

class JoinGroup extends Mutation
{
    protected $attributes = [
        'name' => 'JoinGroupRequirement'
    ];

    private $requirement;

    public function __construct(RequirementContract $requirement)
    {
        $this->requirement = $requirement;
    }

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function rules(array $args = []): array
    {
        return [
            'group_id'   => 'required|string',
        ];
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        try {
            $this->requirement->create([
                'creator_id' => $context->id,
                'group_id'   => $args['group_id'],
                'type'       => $this->requirement->getJoinGroupType()
            ]);
        } catch (\Exception $e) {
            return null;
        }

        $context->push('request_group_ids', $args['group_id'], true);

        return $context;
    }
}