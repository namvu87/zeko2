<?php
namespace App\GraphQL\Pagination;

use Illuminate\Pagination\LengthAwarePaginator;
use GraphQL\Type\Definition\Type as GraphQLType;

class CustomPaginationFields
{
    public static function getPaginationFields()
    {
        return [
            'last_page' => [
                'type'        => GraphQLType::nonNull(GraphQLType::int()),
                'description' => 'Tổng số trang',
                'resolve'     => function (LengthAwarePaginator $data) {
                    return $data->lastPage();
                },
                'selectable'  => false
            ]
        ];
    }
}