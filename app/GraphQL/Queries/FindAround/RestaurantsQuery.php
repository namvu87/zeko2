<?php
namespace App\GraphQL\Queries\FindAround;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Searchable;

class RestaurantsQuery extends Query
{
    const FIRST_PAGE = 1;
    const FIND_AROUND_TYPE = 0;
    const FIND_NEWEST_TYPE = 1;

    protected $attributes = [
        'name' => 'RestaurantsQuery'
    ];

    private $search;

    public function __construct(Searchable $search)
    {
        $this->search = $search;
    }

    public function type(): Type
    {
        return GraphQL::paginate('restaurant');
    }

    public function args(): array
    {
        return [
            'page' => [
                'type' => Type::int()
            ],
            'longitude' => [
                'type' => Type::float()
            ],
            'latitude' => [
                'type' => Type::float()
            ],
            'province' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $page = $args['page'] ?? self::FIRST_PAGE;

        if ($args['type'] === self::FIND_AROUND_TYPE) {
            return $this->search->findAround($args, $page);
        }
        return $this->search->newest($args, $page);
    }
}