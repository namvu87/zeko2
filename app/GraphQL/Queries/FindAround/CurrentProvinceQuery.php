<?php
namespace App\GraphQL\Queries\FindAround;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Searchable;

class CurrentProvinceQuery extends Query
{
    protected $attributes = [
        'name' => 'CurrentProvinceQuery'
    ];

    private $search;

    public function __construct(Searchable $search)
    {
        $this->search = $search;
    }

    public function type(): Type
    {
        return GraphQL::type('province');
    }

    public function args(): array
    {
        return [
            'longitude' => [
                'type' => Type::float()
            ],
            'latitude' => [
                'type' => Type::float()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->search->getCurrentProvince($args['longitude'], $args['latitude']);
    }
}