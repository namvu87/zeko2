<?php
namespace App\GraphQL\Queries\FindAround;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Searchable;

class RestaurantImagesQuery extends Query
{
    const FIRST_PAGE = 1;

    protected $attributes = [
        'name' => 'RestaurantImagesQuery'
    ];

    private $search;

    public function __construct(Searchable $search)
    {
        $this->search = $search;
    }

    public function type(): Type
    {
        return GraphQL::paginate('restaurant_image');
    }

    public function args(): array
    {
        return [
            'page' => [
                'type' => Type::int()
            ],
            'restaurant_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $page = $args['page'] ?? self::FIRST_PAGE;

        return $this->search->getImagesByRestaurantId($args['restaurant_id'], $page);
    }
}