<?php
namespace App\GraphQL\Queries\FindAround;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Searchable;

class ProvincesQuery extends Query
{
    protected $attributes = [
        'name' => 'ProvincesQuery'
    ];

    private $search;

    public function __construct(Searchable $search)
    {
        $this->search = $search;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('province'));
    }

    public function resolve($root, $args)
    {
        return $this->search->getProvinces();
    }
}