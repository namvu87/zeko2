<?php
namespace App\GraphQL\Queries\FindAround;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Searchable;

class RestaurantQuery extends Query
{
    protected $attributes = [
        'name' => 'RestaurantQuery'
    ];

    private $search;

    public function __construct(Searchable $search)
    {
        $this->search = $search;
    }

    public function type(): Type
    {
        return GraphQL::type('restaurant');
    }

    public function args(): array
    {
        return [
            'restaurant_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->search->getRestaurantById($args['restaurant_id']);
    }
}