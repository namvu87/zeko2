<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\ShiftContract;
use App\Contracts\UserContract;

class ShiftsQuery extends Query
{
    protected $attributes = [
        'name' => 'ShiftsQuery'
    ];

    private $shift;
    private $user;

    public function __construct(ShiftContract $shift, UserContract $user)
    {
        $this->shift = $shift;
        $this->user = $user;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('shift'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::string()
            ],
            'user_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['group_id'])) {
            return $this->shift->getByGroupId($args['group_id']);
        }

        if (isset($args['user_id'])) {
            $user = $this->user->getById($args['user_id']);
            return $this->shift->getByIds($user->shift_ids ?? []);
        }
        return null;
    }
}