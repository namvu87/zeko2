<?php

namespace App\GraphQL\Queries;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\InvoiceContract;

class InvoiceQuery extends Query
{
    protected $attributes = [
        'name' => 'InvoiceQuery',
        'description' => 'Lấy chi tiết 1 hoá đơn bán hàng'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function args(): array
    {
        return [
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $selections = $fields->getSelect();
        $relations = $fields->getRelations();

        return $this->invoice->getById($args['invoice_id'], $relations, $selections);
    }
}
