<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\GoodContract;

class SaleMenuQuery extends Query
{
    protected $attributes = [
        'name' => 'SaleMenuQuery',
        'description' => 'Lấy danh sách thực đơn trong màn hình POS'
    ];

    private $goods;

    public function __construct(GoodContract $goods)
    {
        $this->goods = $goods;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('good'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        return $this->goods->getGoodsForSale($args['group_id']);
    }
}
