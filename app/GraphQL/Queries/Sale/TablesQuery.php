<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Sale;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\TableContract;

class TablesQuery extends Query
{
    protected $attributes = [
        'name' => 'SaleTablesQuery',
        'description' => 'Lấy tất cả danh sách bàn'
    ];

    private $table;

    public function __construct(TableContract $table)
    {
        $this->table = $table;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('table'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'place_ids' => [
                'type' => Type::listOf(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $conditions = [
            'group_id'   => $args['group_id']
        ];

        if (isset($args['place_ids']) && !empty($args['place_ids'])) {
            return $this->table->getByPlaceIds($args['place_ids']);
        }

        return $this->table->getAll($conditions, $relations);
    }
}
