<?php
namespace App\GraphQL\Queries\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\TableContract;

class InvoicesQuery extends Query
{
    private $invoice;
    private $table;

    protected $attributes = [
        'name' => 'SaleInvoicesQuery'
    ];

    public function __construct(InvoiceContract $invoice, TableContract $table)
    {
        $this->invoice = $invoice;
        $this->table = $table;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('invoice'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'place_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'is_empty_table' => [
                'type' => Type::boolean(),
                'description' => 'Chỉ định lấy hoá đơn mang về'
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $args['status'] = $this->invoice->getStatusInited();

        if (isset($args['is_empty_table'])) {
            return $this->invoice->getInvoicesIsEmptyTable($args['group_id']);
        }
        if (!empty($args['place_ids'])) {
            $tableIds = $this->table->getByPlaceIds($args['place_ids'] ?? [])->modelKeys();
            return $this->invoice->getInitedInvoicesByTableIds($args['group_id'], $tableIds);
        }

        return $this->invoice->getInvoicesByStatus($args)->sortByDesc('updated_at');
    }
}