<?php
namespace App\GraphQL\Queries\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\InvoiceContract;
// use App\GraphQL\Traits\Authorize;
use Rebing\GraphQL\Error\AuthorizationError;

class InvoiceQuery extends Query
{
    // use Authorize;

    const PERMISSION = '.manage-sale.sale';

    protected $attributes = [
        'name' => 'InvoiceQuery'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::type('invoice');
    }

    public function args(): array
    {
        return [
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        // $this->customAuthorize($args['group_id'], $context);
        $invoice = $this->invoice->getById($args['invoice_id']);

        // if ($invoice->group_id !== $args['group_id'] ||
        //     $invoice->status !== $this->invoice->getStatusInited()) {
        //     throw new AuthorizationError('Unauthorized');
        // }
        return $invoice;
    }
}