<?php
namespace App\GraphQL\Queries\Sale;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\PlaceContract;
use App\Contracts\Restaurant\TableContract;

class PlacesQuery extends Query
{
    protected $attributes = [
        'name' => 'SalePlacesQuery'
    ];

    private $place;
    private $table;

    public function __construct(PlaceContract $place, TableContract $table)
    {
        $this->place = $place;
        $this->table = $table;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('place'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::string()
            ],
            'place_ids' => [
                'type' => Type::listOf(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $get)
    {
        $relations = [
            'tables' => function ($query) {
                $query->where('status', '!=', $this->table->getInactiveType());
            }
        ];

        return $this->place->getSalePlaces($args['group_id'], $args['place_ids'] ?? [], $relations);
    }
}