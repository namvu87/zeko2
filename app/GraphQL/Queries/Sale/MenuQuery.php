<?php
namespace App\GraphQL\Queries\Sale;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\GoodContract;

class MenuQuery extends Query
{
    const FIRST_PAGE = 1;

    protected $attributes = [
        'name' => 'MenuQuery'
    ];

    private $good;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    public function type(): Type
    {
        return GraphQL::paginate('good');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'page' => [
                'type' => Type::int()
            ],
            'group_menu_id' => [
                'type' => Type::string()
            ],
            'keyword' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $page = $args['page'] ?? self::FIRST_PAGE;

        return $this->good->getMenu($args, $page);
    }
}