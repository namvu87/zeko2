<?php
namespace App\GraphQL\Queries;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\RequirementContract;

class RequirementsQuery extends Query
{
    const FIRST_PAGE = 1;
    
    protected $attributes = [
        'name' => 'RequirementsQuery'
    ];

    private $requirement;

    public function __construct(RequirementContract $requirement)
    {
        $this->requirement = $requirement;
    }

    public function type(): Type
    {
        return GraphQL::paginate('requirement');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();
        $page = $args['page'] ?? self::FIRST_PAGE;

        return $this->requirement->paginateByGroupId($args['group_id'], $page, $relations);
    }
}