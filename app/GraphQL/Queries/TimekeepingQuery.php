<?php
namespace App\GraphQL\Queries;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\TimekeepingContract;

class TimekeepingQuery extends Query
{
    protected $attributes = [
        'name' => 'TimekeepingQuery'
    ];

    private $timekeeping;

    public function __construct(TimekeepingContract $timekeeping)
    {
        $this->timekeeping = $timekeeping;
    }

    public function type(): Type
    {
        return GraphQL::type('timekeeping');
    }

    public function args(): array
    {
        return [
            'timekeeping_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        return $this->timekeeping->getById($args['timekeeping_id'], $relations);
    }
}