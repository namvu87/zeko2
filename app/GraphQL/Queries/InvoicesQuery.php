<?php

namespace App\GraphQL\Queries;

use Closure;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\Restaurant\InvoiceContract;
use Carbon\Carbon;

class InvoicesQuery extends Query
{
    const FISRT_PAGE = 1;

    protected $attributes = [
        'name' => 'InvoicesQuery'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::paginate('invoice');
    }

    public function args(): array
    {
        return [
            'page' => [
                'type' => Type::int()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'code' => [
                'type' => Type::string()
            ],
            'status' => [
                'type' => Type::int()
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $page = $args['page'] ?? self::FISRT_PAGE;

        $args['start_date'] = !empty($args['start_date']) ? Carbon::parse($args['start_date'])->startOfDay() : null;
        $args['end_date'] = !empty($args['end_date']) ? Carbon::parse($args['end_date'])->endOfDay() : null;

        return $this->invoice->paginateByCondition($args, $page, $relations);
    }
}
