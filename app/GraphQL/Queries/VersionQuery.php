<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class VersionQuery extends Query
{
    protected $attributes = [
        'name' => 'VersionQuery'
    ];

    public function type(): Type
    {
        return GraphQL::type('version');
    }

    public function resolve($root, $args)
    {
        return [
            'ios' => config('mobile.version.ios'),
            'android' => config('mobile.version.android')
        ];
    }
}