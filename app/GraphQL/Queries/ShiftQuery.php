<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\ShiftContract;

class ShiftQuery extends Query
{
    protected $attributes = [
        'name' => 'ShiftQuery'
    ];

    private $shift;

    public function __construct(ShiftContract $shift)
    {
        $this->shift = $shift;
    }

    public function type(): Type
    {
        return GraphQL::type('shift');
    }

    public function args(): array
    {
        return [
            'shift_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    /**
     * Kiểm tra xem ca làm việc có thuộc về group chi định hay không
     *
     * @param  App\Models\Shift $root
     * @param  array $args
     * @return App\Models\Shift
     */
    public function resolve($root, $args, $context)
    {
        return $this->shift->getById($args['shift_id']);
    }
}