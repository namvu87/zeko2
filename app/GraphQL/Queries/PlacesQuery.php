<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\PlaceContract;
use Arr;

class PlacesQuery extends Query
{
    const RELATIONS = ['tables'];

    protected $attributes = [
        'name' => 'PlacesQuery'
    ];

    private $place;

    public function __construct(PlaceContract $place)
    {
        $this->place = $place;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('place'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $fileds, $info)
    {
        $info = $info->getFieldSelection($depth = 3);
        $relations = Arr::only($info, self::RELATIONS);
        $relations = array_keys($relations);

        return $this->place->getByGroupId($args['group_id'], $relations)->sortByDesc('created_at');
    }
}