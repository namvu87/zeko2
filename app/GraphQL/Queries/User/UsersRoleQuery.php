<?php
namespace App\GraphQL\Queries\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Users\Searchable;

class UsersRoleQuery extends Query
{
    const FIRST_PAGE = 1;

    protected $attributes = [
        'name' => 'UsersRoleQuery'
    ];

    private $user;

    public function __construct(Searchable $user)
    {
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::paginate('user');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'page' => [
                'type' => Type::int()
            ],
            'keyword' => [
                'type' => Type::string()
            ],
            'out_role' => [
                'type' => Type::boolean(),
                'description' => 'Nếu out_role được chỉ định thì lấy những user chưa được phân vai trò trong nhóm'
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $page = $args['page'] ?? self::FIRST_PAGE;

        if (isset($args['out_role'])) {
            return $this->user->getOutRole($args['keyword'] ?? '', $args['group_id'], $page);
        }

        return null;
    }
}