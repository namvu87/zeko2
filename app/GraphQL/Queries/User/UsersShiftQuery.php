<?php
namespace App\GraphQL\Queries\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Users\Searchable;

class UsersShiftQuery extends Query
{
    protected $attributes = [
        'name' => 'UsersShiftQuery'
    ];
    const FIRST_PAGE = 1;
    private $search;

    public function __construct(Searchable $search)
    {
        $this->search = $search;
    }

    public function type(): Type
    {
        return GraphQL::paginate('user');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'shift_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'keyword' => [
                'type' => Type::string()
            ],
            'outside' => [
                'type' => Type::boolean(),
                'description' => 'Lấy ngoài shift hoặc trong shift'
            ],
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $page = $args['page'] ?? self::FIRST_PAGE;

        if (isset($args['outside'])) {
            return $this->search->getOutShiftId($args['keyword'] ?? '', $args['group_id'], $args['shift_id'], $page);
        }
        return $this->search->getByShiftId($args['keyword'] ?? '', $args['shift_id'], $page);
    }
}