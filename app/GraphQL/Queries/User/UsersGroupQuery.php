<?php
namespace App\GraphQL\Queries\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Users\Searchable;

class UsersGroupQuery extends Query
{
    const FIRST_PAGE = 1;

    protected $attributes = [
        'name' => 'UsersGroupQuery'
    ];

    private $user;

    public function __construct(Searchable $user)
    {
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::paginate('user');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'outside' => [
                'type' => Type::boolean(),
                'description' => 'Chỉ định lấy user ngoài group, Nếu không chỉ định, mặc định lấy trong group'
            ],
            'keyword' => [
                'type' => Type::string(),
                'description' => 'Từ khoá tìm kiếm'
            ],
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $page = $args['page'] ?? self::FIRST_PAGE;

        if (isset($args['outside'])) {
            return $this->user->getOutGroup($args['keyword'] ?? '', $args['group_id'], $page);
        }

        return $this->user->getByGroupId($args['keyword'] ?? '', $args['group_id'], $page);
    }
}