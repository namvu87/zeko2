<?php
namespace App\GraphQL\Queries\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\UserContract;

class UsersQuery extends Query
{
    protected $attributes = [
        'name' => 'UsersQuery'
    ];

    private $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'email' => [
                'type' => Type::string()
            ],
            'phone_number' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return $this->user->getById($args['id']);
        }
        if (isset($args['email'])) {
            return $this->user->findForPassport($args['email']);
        }
        if (isset($args['phone_number'])) {
            return $this->user->findForPassport($args['phone_number']);
        }
        return [];
    }
}