<?php

namespace App\GraphQL\Queries\User;

use App\Contracts\UserContract;
use Graphql;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class UserQuery extends Query
{
    protected $attributes = [
        'name' => 'UserQuery',
    ];

    private $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    public function type(): Type
    {
        return Graphql::type('user');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'email' => [
                'type' => Type::string()
            ],
            'phone_number' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return $this->user->getById($args['id']);
        }
        if (isset($args['email'])) {
            return $this->user->findForPassport($args['email']);
        }
        if (isset($args['phone_number'])) {
            return $this->user->findForPassport($args['phone_number']);
        }
        return null;
    }
}
