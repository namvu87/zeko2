<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use Rebing\GraphQL\Support\Facades\GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\ReturnedInvoiceContract;

class ReturnedInvoiceQuery extends Query
{
    protected $attributes = [
        'name' => 'returnedInvoice',
        'description' => 'Lấy chi tiết hoá đơn trả hàng'
    ];

    public function type(): Type
    {
        return GraphQL::type('returned_invoice');
    }

    public function args(): array
    {
        return [
            'returned_invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $selections = $fields->getSelect();
        $reltions = $fields->getRelations();

        $returnedInvoice = app()->make(ReturnedInvoiceContract::class);

        return $returnedInvoice->getById($args['returned_invoice_id'], $reltions, $selections);
    }
}
