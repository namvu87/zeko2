<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\PermissionContract;

class PermissionsQuery extends Query
{
    protected $attributes = [
        'name' => 'PermissionsQuery'
    ];

    private $permission;

    public function __construct(PermissionContract $permission)
    {
        $this->permission = $permission;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('permission'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::string()
            ],
            'role_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['group_id'])) {
            return $this->permission->getByGroupId($args['group_id']);
        }
        if (isset($args['role_id'])) {
            return $this->permission->getByRoleId($args['role_id']);
        }
        return null;
    }
}