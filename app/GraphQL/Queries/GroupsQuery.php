<?php

namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\GroupContract;
use App\Contracts\UserContract;

class GroupsQuery extends Query
{
    protected $attributes = [
        'name' => 'GroupsQuery',
        'description' => 'Lấy danh sách các nhóm của 1 user hoặc của user đang đăng nhập'
    ];

    private $group;
    private $user;

    public function __construct(GroupContract $group, UserContract $user)
    {
        $this->group = $group;
        $this->user = $user;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('group'));
    }

    public function args(): array
    {
        return [
            'user_id' => [
                'type' => Type::String()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        if (isset($args['user_id'])) {
            $user = $this->user->getById($args['user_id']);
        } else {
            $user = $context;
        }

        return $this->group->getByIds($user->group_ids ?? []);
    }
}
