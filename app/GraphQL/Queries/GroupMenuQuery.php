<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\GroupMenuContract;

class GroupMenuQuery extends Query
{
    protected $attributes = [
        'name' => 'groupMenuQuery'
    ];

    private $menu;

    public function __construct(GroupMenuContract $menu)
    {
        $this->menu = $menu;
    }

    public function type(): Type
    {
        return GraphQL::type('group_menu');
    }

    public function args(): array
    {
        return [
            'group_menu_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        return $this->menu->getById($args['group_menu_id']);
    }
}
