<?php
namespace App\GraphQL\Queries;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\Restaurant\TableContract;

class TablesQuery extends Query
{
    const RELATIONS = ['place', 'group'];
    const FISRT_PAGE = 1;

    protected $attributes = [
        'name' => 'TablesQuery'
    ];

    private $table;

    public function __construct(TableContract $table)
    {
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::paginate('table');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'status' => [
                'type' => Type::int()
            ],
            'place_id' => [
                'type' => Type::string()
            ],
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $page = $args['page'] ?? self::FISRT_PAGE;

        return $this->table->getByCondition($args, $relations, $page);
    }
}