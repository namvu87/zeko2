<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Error\AuthorizationError;
use App\Contracts\RequirementContract;

class RequirementQuery extends Query
{
    protected $attributes = [
        'name' => 'RequirementQuery',
        'description' => 'Get requirement by id'
    ];

    private $requirement;

    public function __construct(RequirementContract $requirement)
    {
        $this->requirement = $requirement;
    }

    public function type(): Type
    {
        return GraphQL::type('requirement');
    }

    public function args(): array
    {
        return [
            'requirement_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $selections = $fields->getSelect();
        $relations = $fields->getRelations();

        $requirement = $this->requirement->getById(
            $args['requirement_id'], $relations, $selections
        );

        if ($requirement->creator_id !== $context->id) {
            throw new AuthorizationError('Unauthorized');
        }
        return $requirement;
    }
}
