<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\PlaceContract;

class PlaceQuery extends Query
{
    protected $attributes = [
        'name' => 'PlaceQuery'
    ];

    private $place;

    public function __construct(PlaceContract $place)
    {
        $this->place = $place;
    }

    public function type(): Type
    {
        return GraphQL::type('place');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $fileds, $info)
    {
		return $this->place->getById($args['id']);
    }
}