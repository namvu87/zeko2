<?php
namespace App\GraphQL\Queries;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\UserContract;
use App\Contracts\TimekeepingContract;
use App\GraphQL\Traits\Authorize;

class TimekeepingsQuery extends Query
{
    use Authorize;
    
    const FIRST_PAGE = 1;
    const PERMISSION = ".timekeeping.list";

    protected $attributes = [
        'name' => 'TimekeepingsQuery'
    ];

    private $timekeeping;
    private $user;

    public function __construct(UserContract $user, TimekeepingContract $timekeeping)
    {
        $this->user = $user;
        $this->timekeeping = $timekeeping;
    }

    public function type(): Type
    {
        return GraphQL::paginate('timekeeping');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ],
            'user_id' => [
                'type' => Type::string()
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Trang dữ liệu sẽ lấy, mặc định là trang đầu tiên $page = 1'
            ]
        ];
    }

    /**
     * Phân trang danh sách bảng chấm công
     *
     * @param  App\Models\Timekeeping $root
     * @param  array $args
     * @return App\Models\Timekeeping
     */
    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $this->customAuthorize($args['group_id'], $context);

        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $page = $args['page'] ?? self::FIRST_PAGE;

        return $this->timekeeping->getByConditionGraphQL($args, $page, $relations);
    }
}