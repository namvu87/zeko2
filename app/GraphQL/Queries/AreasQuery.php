<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\AreaContract;

class AreasQuery extends Query
{
    protected $attributes = [
        'name' => 'AreasQuery'
    ];

    private $area;

    public function __construct(AreaContract $area)
    {
        $this->area = $area;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('area'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'all' => [
                'type' => Type::boolean()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['name'])) {
            return $this->area->getAreaByKeyword($args['name']);
        }
        if (isset($args['all']) && $args['all']) {
            return $this->area->getAll()->sortBy('name');
        }
        return null;
    }
}