<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\ReturnedInvoiceContract;
use Carbon\Carbon;

class ReturnedInvoicesQuery extends Query
{
    protected $attributes = [
        'name' => 'ReturnedInvoicesQuery',
        'description' => 'Lấy danh sách hoá đơn trả hàng'
    ];

    public function type(): Type
    {
        return GraphQL::paginate('returned_invoice');
    }

    public function args(): array
    {
        return [
            'page' => [
                'type' => Type::int()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'code' => [
                'type' => Type::string()
            ],
            'status' => [
                'type' => Type::int()
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $selections = $fields->getSelect();
        $reltions = $fields->getRelations();
        $page = $args['page'] ?? 1;

        $args['start_date'] = isset($args['start_date']) ?
            Carbon::parse($args['start_date'])->startOfDay() : null;
        $args['end_date'] = isset($args['end_date']) ?
            Carbon::parse($args['end_date'])->endOfDay() : null;

        $instance = app()->make(ReturnedInvoiceContract::class);

        return $instance->paginateByCondition($args, $page, $reltions, $selections);
    }
}
