<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\CommuneContract;

class CommunesQuery extends Query
{
    protected $attributes = [
        'name' => 'CommunesQuery'
    ];

    private $commune;

    public function __construct(CommuneContract $commune)
    {
        $this->commune = $commune;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('commune'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'area_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['area_id'])) {
            return $this->commune->getAllByAreaId($args['area_id']);
        }
        if (isset($args['name'])) {
            return $this->commune->getByKeyword($args['name']);
        }
        return [];
    }
}