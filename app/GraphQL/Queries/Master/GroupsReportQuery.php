<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Master;

use Closure;
use GraphQL;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Query;
use App\Contracts\GroupContract;
use Carbon\Carbon;

class GroupsReportQuery extends Query
{
    protected $attributes = [
        'name' => 'master/GroupsReport',
        'description' => 'Lấy các groups theo thời gian khởi tạo kèm các thông tin liên quan'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('group'));
    }

    public function args(): array
    {
        return [
            'month' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        $startDate = Carbon::createFromFormat('Y-m', $args['month'])->startOfMonth();
        $endDate = Carbon::createFromFormat('Y-m', $args['month'])->endOfMonth();

        return $this->group->groupsStatistic($startDate, $endDate);
    }
}
