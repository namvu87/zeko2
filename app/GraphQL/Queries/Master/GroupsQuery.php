<?php

namespace App\GraphQL\Queries\Master;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\GroupContract;
use App\Contracts\UserContract;

class GroupsQuery extends Query
{
    protected $attributes = [
        'name'        => 'GroupsQuery',
        'description' => 'Lấy danh sách các đăng ký zeko'
    ];

    private $group;
    private $user;

    public function __construct(GroupContract $group, UserContract $user)
    {
        $this->group = $group;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::paginate('group');
    }

    public function args(): array
    {
        return [
            'care_status' => [
                'type' => Type::int()
            ],
            'status' => [
                'type' => Type::boolean()
            ],
            'customer' => [
                'type' => Type::string()
            ],
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $userId = null;
        if (isset($args['customer'])) {
            $user = $this->user->findForPassport($args['customer']);
            $userId = $user ? $user->id : '';
        }
        return $this->group->getGroups($args['care_status'], $args['status'], $args['page'], $userId);
    }
}
