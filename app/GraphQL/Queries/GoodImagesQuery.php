<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Contracts\GoodImageContract;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL;

class GoodImagesQuery extends Query
{
    protected $attributes = [
        'name' => 'goodImages',
        'description' => 'A query'
    ];

    private $image;

    public function __construct(GoodImageContract $image)
    {
        $this->image = $image;
    }

    public function type(): Type
    {
        return GraphQL::paginate('good_image');
    }

    public function args(): array
    {
        return [
            'name' => [
                'type' => Type::string()
            ],
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->image->getLists($args['name'] ?? '', $args['page'] ?? 1);
    }
}
