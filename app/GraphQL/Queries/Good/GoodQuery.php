<?php
namespace App\GraphQL\Queries\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\GoodContract;

class GoodQuery extends Query
{
    protected $attributes = [
        'name' => 'GoodQuery'
    ];

    private $good;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    public function type(): Type
    {
        return GraphQL::type('good');
    }

    public function args(): array
    {
        return [
            'good_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->good->getById($args['good_id']);
    }
}