<?php
namespace App\GraphQL\Queries\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\GoodContract;
use Arr;

class GoodsQuery extends Query
{
    const FIRST_PAGE = 1;
    const IMPORTED_TYPE = 1;
    const SALE_TYPE = 2;
    const RELATIONS = ['group', 'parent', 'childs', 'groupMenus'];

    protected $attributes = [
        'name' => 'GoodsQuery'
    ];

    private $good;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    public function type(): Type
    {
        return GraphQL::paginate('good');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'page' => [
                'type' => Type::int()
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'Tên hoặc mã hàng hoá'
            ],
            'status' => [
                'type' => Type::int()
            ],
            'group_menu_id' => [
                'type' => Type::string()
            ],
            'inventory' => [
                'type' => Type::int()
            ],
            'type' => [
                'type' => Type::int(),
                'description' => '1: Hàng nhập, 2: Hàng chế biến'
            ],
            'is_sale' => [
                'type' => Type::boolean(),
                'description' => 'true: Hàng bán (bao gồm cả type = 1 và type = 2)'
            ]
        ];
    }

    public function resolve($root, $args, $fields, $info)
    {
        $info = $info->getFieldSelection($depth = 3);
        $relations = Arr::only($info['data'], self::RELATIONS);
        $relations = array_keys($relations);

        $page = $args['page'] ?? self::FIRST_PAGE;

        return $this->good->getByCondition($args, $relations, $page);
    }
}