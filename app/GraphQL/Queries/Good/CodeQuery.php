<?php
namespace App\GraphQL\Queries\Good;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\GoodContract;

class CodeQuery extends Query
{
    protected $attributes = [
        'name' => 'CodeQuery'
    ];

    private $goods;

    public function __construct(GoodContract $goods)
    {
        $this->goods = $goods;
    }

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->goods->generateCode($args['group_id']);
    }
}