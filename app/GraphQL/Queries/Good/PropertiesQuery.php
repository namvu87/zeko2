<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Good;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\GroupContract;

class PropertiesQuery extends Query
{
    protected $attributes = [
        'name' => 'GoodsProperties',
        'description' => 'Lấy các thuộc tính cho hàng hoá trong nhóm'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return Type::listOf(Type::string());
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $group = $this->group->getById($args['group_id']);
        return $group->goods_properties ?? [];
    }
}
