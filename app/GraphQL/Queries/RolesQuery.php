<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\RoleContract;

class RolesQuery extends Query
{
    protected $attributes = [
        'name' => 'RolesQuery'
    ];

    private $role;

    public function __construct(RoleContract $role)
    {
        $this->role = $role;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('role'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return $this->role->getById($args['id']);
        }
        if (isset($args['group_id'])) {
            return $this->role->getByGroupId($args['group_id']);
        }
        return [];
    }
}