<?php
namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\GroupMenuContract;

class GroupMenusQuery extends Query
{
    protected $attributes = [
        'name' => 'GroupMenusQuery'
    ];

    private $menu;

    public function __construct(GroupMenuContract $menu)
    {
        $this->menu = $menu;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('group_menu'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'is_parent' => [
                'type' => Type::boolean(),
                'description' => 'Chỉ định lấy group menu theo dạng phân cấp'
            ]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['is_parent'])) {
            return $this->menu->getParentGroupMenusByGroupId($args['group_id']);
        }
        return $this->menu->getByGroupId($args['group_id']);
    }
}