<?php

namespace App\GraphQL\Queries\Search;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Users\Searchable;

class UsersQuery extends Query
{
    const FIRST_PAGE = 1;
    
    protected $attributes = [
        'name' => 'SearchUsersQuery'
    ];

    private $user;

    public function __construct(Searchable $user)
    {
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::paginate('user');
    }

    public function args(): array
    {
        return [
            'page' => [
                'type' => Type::int()
            ],
            'keyword' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->user->getByKeyword($args['keyword'] ?? '', $args['page'] ?? self::FIRST_PAGE);
    }
}