<?php

namespace App\GraphQL\Queries\Search;

use Graphql;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\GroupContract;

class GroupsQuery extends Query
{
    protected $attributes = [
        'name' => 'SearchGroupsQuery'
    ];

    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function type(): Type
    {
        return Type::listOf(Graphql::type('group'));
    }

    public function args(): array
    {
        return [
            'keyword' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->group->getByKeyword($args['keyword'] ?? '');
    }
}