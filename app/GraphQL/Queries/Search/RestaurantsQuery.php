<?php

namespace App\GraphQL\Queries\Search;

use Graphql;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Searchable;

class RestaurantsQuery extends Query
{
    protected $attributes = [
        'name' => 'SearchRestaurantsQuery'
    ];

    private $search;

    public function __construct(Searchable $search)
    {
        $this->search = $search;
    }

    public function type(): Type
    {
        return GraphQL::paginate('restaurant');
    }

    public function args(): array
    {
        return [
            'keyword' => [
                'type' => Type::string()
            ],
            'province' => [
                'type' => Type::string()
            ],
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->search->getRestaurantsByKeyword($args['keyword'] ?? '', $args['province'], $args['page']);
    }
}