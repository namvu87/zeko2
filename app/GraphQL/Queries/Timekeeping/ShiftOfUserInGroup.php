<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Timekeeping;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\ShiftContract;
use App\Contracts\UserContract;

class ShiftOfUserInGroup extends Query
{
    protected $attributes = [
        'name' => 'ShiftOfUserInGroup',
        'description' => 'Xác định ca làm việc của 1 user trong group chỉ định'
    ];

    private $shift;
    private $user;

    public function __construct(ShiftContract $shift, UserContract $user)
    {
        $this->shift = $shift;
        $this->user = $user;
    }

    public function type(): Type
    {
        return GraphQL::type('shift');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'user_id' => [
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function resolve($root, $args, $context)
    {
        $user = $this->user->getById($args['user_id']);

        return $this->shift->getByIdsAndGroup($user->shift_ids ?? [], $args['group_id']);
    }
}
