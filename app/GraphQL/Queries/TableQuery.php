<?php
namespace App\GraphQL\Queries;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\Restaurant\TableContract;

class TableQuery extends Query
{
    const FISRT_PAGE = 1;

    protected $attributes = [
        'name' => 'TableQuery'
    ];

    private $table;

    public function __construct(TableContract $table)
    {
        $this->table = $table;
    }

    public function type(): Type
    {
        return GraphQL::type('table');
    }

    public function args(): array
    {
        return [
            'table_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = array_keys($fields->getRelations());

        $data = $this->table->getById($args['table_id'], $relations);
        return $data;
    }
}