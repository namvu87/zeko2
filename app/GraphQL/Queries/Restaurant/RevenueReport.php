<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Restaurant;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\RestaurantReport;

class RevenueReport extends Query
{
    use RestaurantReport;

    protected $attributes = [
        'name' => 'RestaurantRevenueReport',
        'description' => 'Báo cáo doanh thu bán hàng theo bảng biểu'
    ];

    private $invoice;
    private $timeOperator;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
        $this->timeOperator = [
            'date' => '$created_at',
            'timezone' => config('app.timezone')
        ];
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('restaurant_sale_report'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        list($startDate, $endDate) = $this->resolveDate($args);
        $period = $this->resolvePeriod($startDate, $endDate);

        list($groupIdOperator, $sort) = $this->resolveGroupIdOperator($period);

        $matching = $this->matchPurchasedInvoice($args['group_id'], $startDate, $endDate);
        $groupOperator = $this->resolveGroupOperator($groupIdOperator);

        return $this->invoice->aggregate([
            $matching,
            $groupOperator,
            $sort
        ]);
    }

    /**
     * Xác định toán tử $group
     *
     * @param array $groupIdOperator
     * @return array
     */
    private function resolveGroupOperator(array $groupIdOperator): array
    {
        return [
            '$group' => [
                '_id' => $groupIdOperator,
                'total' => [
                    '$sum' => '$total'
                ],
                'total_returned' => [
                    '$sum' => '$total_returned'
                ]
            ]
        ];
    }
}

