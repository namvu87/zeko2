<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Restaurant;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\RestaurantReport;

class RevenueChart extends Query
{
    use RestaurantReport;
    
    const REPORT_BY_HOUR = 1;
    const REPORT_BY_DAY_OF_WEEK = 2;
    const REPORT_BY_DATE = 3;

    protected $attributes = [
        'name' => 'RestaurantRevenueChart',
        'description' => 'Báo cáo doanh thu bán hàng theo đồ thị'
    ];

    private $invoice;
    private $timeOperator;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
        $this->timeOperator = [
            'date' => '$created_at',
            'timezone' => config('app.timezone')
        ];
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('descartes'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => Type::int(),
                'description' => 'Cách thức báo cáo: 1-theo giờ, 2-theo ngày, 3-theo thứ
                    (Chỉ khả dụng khi khoảng thời gian dưới 31 ngày, nếu lớn hơn 31 ngày mặc định là báo cáo theo tháng)'
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        list($startDate, $endDate) = $this->resolveDate($args);
        $period = $this->resolvePeriod($startDate, $endDate);

        list($groupIdOperator, $sort) = $this->resolveGroupIdOperator(
            $period, $args['type'] ?? self::REPORT_BY_HOUR
        );
        $matching = $this->matchPurchasedInvoice($args['group_id'], $startDate, $endDate);
        $groupOperator = $this->resolveGroupOperator($groupIdOperator);

        $result = $this->invoice->aggregate([
            $matching, $groupOperator, $sort
        ]);
        
        return $this->normalizeData($result, $period, $args['type'] ?? self::REPORT_BY_HOUR);
    }

    private function resolveAggregation($groupId, $startDate, $endDate, $groupOperator, $sort)
    {
        return [
            $this->matchPurchasedInvoice($groupId, $startDate, $endDate),
            $groupOperator,
            $sort
        ];
    }

    /**
     * Xác định đầu ra cấu trúc dữ liệu (_id trong toán tử $group)
     */
    private function resolveGroupIdOperator($period, $type)
    {
        $groupIdOperator = null;
        $sort = [];

        // Nếu khoảng thời gian lớn hơn 365 ngày thì xem theo năm
        if ($period > 365) {
            $groupIdOperator = [
                'year' => [
                    '$year' => $this->timeOperator
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.year' => 1
                ]
            ];
            return [$groupIdOperator, $sort];
        }

        // Nếu khoảng thời gian lớn hơn 31 ngày thì mặc định là xem báo cáo theo tháng
        if ($period > 31) {
            $groupIdOperator = [
                'month' => [
                    '$month' => $this->timeOperator
                ],
                'year' => [
                    '$year' => $this->timeOperator
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.month' => 1,
                    '_id.year' => 1
                ]
            ];
            return [$groupIdOperator, $sort];
        }

        // Nếu khoảng thời gian trong 1 tháng thì xem theo các tuỳ chọn
        switch ($type) {
            case self::REPORT_BY_DAY_OF_WEEK:
                $groupIdOperator = [
                    'day_of_week' => [
                        '$dayOfWeek' => $this->timeOperator
                    ]
                ];
                $sort = [
                    '$sort' => [
                        '_id.day_of_week' => 1
                    ]
                ];
                break;
            case self::REPORT_BY_DATE:
                $groupIdOperator = [
                    'day' => [
                        '$dayOfMonth' => $this->timeOperator
                    ]
                ];
                $sort = [
                    '$sort' => [
                        '_id.day' => 1
                    ]
                ];
                break;
            default: // self::REPORT_BY_HOUR
                $groupIdOperator = [
                    'hour' => [
                        '$hour' => $this->timeOperator
                    ]
                ];
                $sort = [
                    '$sort' => [
                        '_id.hour' => 1
                    ]
                ];
                break;
        }
        return [$groupIdOperator, $sort];
    }

    /**
     * Xác định toán tử $group
     *
     * @param array $groupIdOperator
     * @return array
     */
    private function resolveGroupOperator(array $groupIdOperator): array
    {
        return [
            '$group' => [
                '_id' => $groupIdOperator,
                'revenue' => [
                    '$sum' => '$total'
                ]
            ]
        ];
    }

    /**
     * Chuyển định danh dữ liệu về hệ toạ độ Decart
     *
     * @param array|object $data
     * @param integer $period
     * @param int $type
     * @return void
     */
    private function normalizeData($data, int $period, int $type)
    {
        $targetData = [];
        foreach ($data as $item) {
            if ($period > 365) {
                $targetData[] = [
                    'y' => $item->revenue,
                    'x' => $item->_id->year
                ];
                continue;
            }
            if ($period > 31) {
                $targetData[] = [
                    'y' => $item->revenue,
                    'x' => $item->_id->month . '-' . $item->_id->year
                ];
                continue;
            }

            switch ($type) {
                case self::REPORT_BY_DAY_OF_WEEK:
                    $targetData[] = [
                        'y' => $item->revenue,
                        'x' => __("datetime.mongo_week." . $item->_id->day_of_week)
                    ];
                    break;
                case self::REPORT_BY_DATE:
                    $targetData[] = [
                        'y' => $item->revenue,
                        'x' => $item->_id->day
                    ];
                    break;
                default: // self::REPORT_BY_HOUR
                    $targetData[] = [
                        'y' => $item->revenue,
                        'x' => $item->_id->hour . ':00'
                    ];
                    break;
            }
        }
        return $targetData;
    }
}

