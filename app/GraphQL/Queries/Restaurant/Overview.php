<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Restaurant;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\ReportContract;
use App\Contracts\Restaurant\ReturnedInvoiceContract;
use Carbon\Carbon;

class Overview extends Query
{
    protected $attributes = [
        'name' => 'RestaurantOverview',
        'description' => 'Báo cáo số liệu sơ bộ trong ngày'
    ];

    private $report;
    private $invoice;
    private $returnedInvoice;

    public function __construct(
        ReportContract $report,
        InvoiceContract $invoice,
        ReturnedInvoiceContract $returnedInvoice
    )
    {
        $this->report = $report;
        $this->invoice = $invoice;
        $this->returnedInvoice = $returnedInvoice;
    }

    public function type(): Type
    {
        return GraphQL::type('restaurant_overview');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        $startDate = isset($args['start_date']) ? 
                        Carbon::parse($args['start_date'])->startOfDay() :
                        Carbon::now()->startOfDay();
        $endDate = isset($args['end_date']) ? 
                        Carbon::parse($args['end_date'])->endOfDay() :
                        Carbon::now()->endOfDay();

        $result = $this->invoice->getTotalRevenueByDateRange($args['group_id'], $startDate, $endDate);

        return [
            'inited_invoice' => $this->invoice->getCountByStatus(
                $args['group_id'],
                $startDate,
                $endDate,
                $this->invoice->getStatusInited()
            ),
            'returned_invoice' => $this->returnedInvoice->getCountByCondition(
                $args['group_id'],
                $startDate,
                $endDate
            ),
            'purchased_invoice' => count($result) > 0 ? $result[0]['purchased_invoice'] : 0,
            'revenue' => count($result) > 0 ? $result[0]['revenue'] : 0
        ];
    }
}
