<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Restaurant;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Carbon\Carbon;
use App\Contracts\Restaurant\InvoiceContract;

class TopSellingGoods extends Query
{
    const REVENUE_TYPE = 1;
    const AMOUNT_TYPE = 2;

    protected $attributes = [
        'name' => 'TopSellingGoods',
        'description' => 'Top những mặt hàng bán chạy'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('top_selling_goods'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        list($startDate, $endDate) = $this->resolvePeriodDate($args);

        return $this->invoice->getTopSellingGoods($args['group_id'], $startDate, $endDate);
    }

    /**
     * Xác định khoảng thời gian
     */
    private function resolvePeriodDate($args)
    {
        $startDate = isset($args['start_date']) ? 
                                Carbon::parse($args['start_date'])->startOfDay() :
                                Carbon::now()->startOfDay();
        $endDate = isset($args['end_date']) ? 
                                Carbon::parse($args['end_date'])->endOfDay() :
                                Carbon::now()->endOfDay();
        return [$startDate, $endDate];
    }
}
