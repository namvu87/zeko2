<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Restaurant;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\InvoiceContract;
use App\GraphQL\Traits\RestaurantReport;
use Carbon\Carbon;

class GoodsReportQuery extends Query
{
    use RestaurantReport;

    protected $attributes = [
        'name' => 'restaurant_goods_report',
        'description' => 'Báo cáo bán hàng theo hàng hoá'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('goods_report'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        list($startDate, $endDate) = $this->resolvePeriodDate($args);
        $matching = $this->matchPurchasedInvoice($args['group_id'], $startDate, $endDate);
        $groupOperator = $this->resolveGroupOperator();

        return $this->invoice->aggregate([
            ['$unwind' => '$goods'],
            $matching,
            $groupOperator,
            ['$sort' => ['count' => -1]]
        ]);
    }

    /**
     * Xác định khoảng thời gian
     */
    private function resolvePeriodDate($args)
    {
        $startDate = isset($args['start_date']) ? 
                                Carbon::parse($args['start_date'])->startOfDay() :
                                Carbon::now()->startOfDay();
        $endDate = isset($args['end_date']) ? 
                                Carbon::parse($args['end_date'])->endOfDay() :
                                Carbon::now()->endOfDay();
        return [$startDate, $endDate];
    }

    /**
     * Xác định toán tử $group cho biểu thức tổng hợp aggregate
     *
     * @return array
     */
    private function resolveGroupOperator(): array
    {
        return [
            '$group' => [
                '_id' => '$goods.id',
                'name' => [
                    '$first' => '$goods.name'
                ],
                'code' => [
                    '$first' => '$goods.code'
                ],
                'image' => [
                    '$first' => '$goods.image'
                ],
                'unit' => [
                    '$first' => '$goods.unit'
                ],
                'total' => [
                    '$sum' => '$goods.total'
                ],
                'total_origin' => [
                    '$sum' => '$goods.total_origin'
                ],
                'total_returned' => [
                    '$sum' => '$goods.total_returned'
                ],
                'total_discount' => [
                    '$sum' => '$goods.total_discount'
                ],
                'count' => [
                    '$sum' => '$goods.count'
                ],
                'count_returned' => [
                    '$sum' => '$goods.count_returned'
                ]
            ]
        ];
    }
}
