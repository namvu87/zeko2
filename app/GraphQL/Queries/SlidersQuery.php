<?php

namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\SliderContract;

class SlidersQuery extends Query
{

    protected $attributes = [
        'name' => 'SlidersQuery'
    ];

    private $slider;

    public function __construct(SliderContract $slider)
    {
        $this->slider = $slider;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('slider'));
    }

    public function resolve($root, $args)
    {
        return $this->slider->getCampaign();
    }
}