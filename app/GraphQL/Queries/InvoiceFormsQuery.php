<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\Restaurant\InvoiceFormContract;

class InvoiceFormsQuery extends Query
{
    protected $attributes = [
        'name' => 'InvoiceFormQuery',
        'description' => 'Lấy mẫu in của 1 nhóm theo loại'
    ];

    private $invoiceForm;

    public function __construct(InvoiceFormContract $invoiceForm)
    {
        $this->invoiceForm = $invoiceForm;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('invoice_form'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'type' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args, $context)
    {
        return $this->invoiceForm->getByType($args['group_id'], $args['type']);
    }
}
