<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Me;

use GraphQL;
use Closure;
use App\Contracts\ServiceFeeContract;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Maklad\Permission\Models\Role;
use Rebing\GraphQL\Support\Query;
use Carbon\Carbon;

class ServiceFeesQuery extends Query
{
    protected $attributes = [
        'name'        => 'ServiceFees',
        'description' => 'Lấy ra hoá đơn thanh toán hệ thống của 1 user'
    ];

    private $fee;

    public function __construct(ServiceFeeContract $fee)
    {
        $this->fee = $fee;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('service_fee'));
    }

    public function rules(array $args = []): array
    {
        return [
            'month' => 'nullable|string|date_format:Y-m'
        ];
    }

    public function args(): array
    {
        return [
            'month' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $roles = Role::whereIn('_id', $context->role_ids)
            ->where(function ($query) {
                return $query->where('name', 'like', '%.owner')
                    ->orWhere('name', 'like', '%.admin');
            })->get();

        return $this->fee->getByGroupIds(
            $roles->pluck('group_id')->toArray(),
            $relations,
            $args['month'] ?? Carbon::now()->subMonth()->format('Y-m')
        );
    }
}
