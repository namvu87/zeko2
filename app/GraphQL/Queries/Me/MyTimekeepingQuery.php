<?php
namespace App\GraphQL\Queries\Me;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\TimekeepingContract;
use Rebing\GraphQL\Error\AuthorizationError;

class MyTimekeepingQuery extends Query
{
    const FIRST_PAGE = 1;

    protected $attributes = [
        'name' => 'MyTimekeepingQuery',
        'description' => 'Lấy 1 dữ liệu chấm công của người dùng hiện tại'
    ];

    private $timekeeping;

    public function __construct(TimekeepingContract $timekeeping)
    {
        $this->timekeeping = $timekeeping;
    }

    public function type(): Type
    {
        return GraphQL::type('timekeeping');
    }

    public function args(): array
    {
        return [
            'date' => [
                'type' => Type::string()
            ],
            'timekeeping_id' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        if (isset($args['timekeeping_id'])) {
            $timekeeping = $this->timekeeping->getById($args['timekeeping_id'], $relations);

            if ($timekeeping->user_id !== $context->id) {
                throw new AuthorizationError('Unauthorized');
            }
            return $timekeeping;
        }

        return $this->timekeeping->getMyTimekeepingByDate(
            $context->id, $args['group_id'], $args['date']
        );
    }
}