<?php
namespace App\GraphQL\Queries\Me;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\UserContract;
use App\Contracts\RoleContract;
use App\Contracts\PermissionContract;

class PermissionsGroupQuery extends Query
{
    protected $attributes = [
        'name' => 'PermissionsInGroupQuery'
    ];

    private $user;
    private $role;
    private $permission;

    public function __construct(UserContract $user, RoleContract $role, PermissionContract $permission)
    {
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
    }

    public function authorize(array $args): bool
    {
        return auth('api')->check();
    }

    public function type(): Type
    {
        return GraphQL::type('array');
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Danh sách các quyền của user trong group'
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $user = $this->user->getUser();
        $role = $user->roles->where('group_id', $args['group_id'])->first();
        $permissionName = $this->permission
                               ->getByRoleId($role->id)
                               ->pluck('name')
                               ->map(function ($name) use ($args) {
                                   return str_replace($args['group_id'] . '.', '', $name);
                               });

        return [
            'data' => $permissionName
        ];
    }
}