<?php

namespace App\GraphQL\Queries\Me;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\RequirementContract;

class CountRequirementQuery extends Query
{
    protected $attributes = [
        'name' => 'CountRequirementQuery'
    ];

    private $requirement;

    public function __construct(RequirementContract $requirement)
    {
        $this->requirement = $requirement;
    }

    public function type(): Type
    {
        return Type::int();
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        return $this->requirement->count([
            'group_id' => $args['group_id'],
            'is_accept' => null
        ]);
    }
}