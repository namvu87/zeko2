<?php
namespace App\GraphQL\Queries\Me;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Contracts\RequirementContract;

class RequirementsQuery extends Query
{
    protected $attributes = [
        'name' => 'MyRequirementsQuery'
    ];

    private $requirement;

    public function __construct(RequirementContract $requirement)
    {
        $this->requirement = $requirement;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('requirement'));
    }

    public function args(): array
    {
        return [
            'group_id' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $condition = [
            'group_id'   => $args['group_id'],
            'creator_id' => $context->id
        ];

        return $this->requirement->getAll($condition, $relations);
    }
}