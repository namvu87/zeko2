<?php
namespace App\GraphQL\Queries\Me;

use Closure;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\TimekeepingContract;

class MyTimekeepingsQuery extends Query
{
    const FIRST_PAGE = 1;

    protected $attributes = [
        'name' => 'MyTimekeepingsQuery',
        'description' => 'Danh sách chấm công của người dùng hiện tại'
    ];

    private $timekeeping;

    public function __construct(TimekeepingContract $timekeeping)
    {
        $this->timekeeping = $timekeeping;
    }

    public function type(): Type
    {
        return GraphQL::paginate('timekeeping');
    }

    public function args(): array
    {
        return [
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ],
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    /**
     * Phân trang danh sách bảng chấm công của người dùng hiện tại
     *
     * @param  App\Models\Timekeeping $root
     * @param  array $args
     * @return App\Models\Timekeeping
     */
    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $page = $args['page'] ?? self::FIRST_PAGE;

        return $this->timekeeping->getMyTimekeepings($context->id, $args, $relations);
    }
}