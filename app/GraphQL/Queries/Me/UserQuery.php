<?php
namespace App\GraphQL\Queries\Me;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class UserQuery extends Query
{
    protected $attributes = [
        'name' => 'MeQuery'
    ];

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function args(): array
    {
        return [];
    }

    public function resolve($root, $args, $context)
    {
        return $context;
    }
}