<?php
namespace App\GraphQL\Queries\Me;

use Closure;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\Restaurant\InvoiceContract;
use Carbon\Carbon;

class MyInvoicesQuery extends Query
{
    const FISRT_PAGE = 1;

    protected $attributes = [
        'name' => 'MyInvoicesQuery'
    ];

    private $invoice;

    public function __construct(InvoiceContract $invoice)
    {
        $this->invoice = $invoice;
    }

    public function type(): Type
    {
        return GraphQL::paginate('invoice');
    }

    public function args(): array
    {
        return [
            'page' => [
                'type' => Type::int()
            ],
            'code' => [
                'type' => Type::string()
            ],
            'start_date' => [
                'type' => Type::string()
            ],
            'end_date' => [
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();

        $page = $args['page'] ?? self::FISRT_PAGE;

        return $this->invoice->paginateByUserId($context->id, $args, $relations);
    }
}