<?php

namespace App\GraphQL\Queries;

use GraphQL;
use Closure;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Query;
use App\Contracts\NotificationContract;

class NotificationsQuery extends Query
{
    const FIRST_PAGE = 1;
    private $notification;

    protected $attributes = [
        'name' => 'MyNotificationsQuery'
    ];

    public function __construct(NotificationContract $notification)
    {
        $this->notification = $notification;
    }

    public function type(): Type
    {
        return GraphQL::paginate('notification');
    }

    public function args(): array
    {
        return [
            'page' => [
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $relations = $fields->getRelations();
        
        $page = $args['page'] ?? self::FIRST_PAGE;
        
        return $this->notification->paginateByUserId($page, $context->id, $relations);
    }
}