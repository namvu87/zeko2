<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class InvoiceGoodItemType extends GraphQLType
{
    protected $attributes = [
        'name' => 'invoice_good_item'
    ];

    public function fields() :array
    {
        return [
            'is_disable' => [
                'type' => Type::boolean(),
                'description' => 'Xác định món này còn bán hay ngừng'
            ],
            'status' => [
                'type' => Type::int()
            ],
            'order_number' => [
                'type' => Type::int(),
                'description' => 'Số lượng đã đặt'
            ],
            'created_at' => [
                'type' => Type::string()
            ]
        ];
    }
}