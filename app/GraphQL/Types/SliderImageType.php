<?php

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class SliderImageType extends GraphQLType
{
    protected $attributes = [
        'name' => 'slider_image',
    ];

    public function fields(): array
    {
        return [
            'origin' => [
                'type' => Type::string()
            ],
            'x1' => [
                'type' => Type::string()
            ],
            'x2' => [
                'type' => Type::string()
            ],
            'x3' => [
                'type' => Type::string()
            ],
            'x4' => [
                'type' => Type::string()
            ]
        ];
    }
}