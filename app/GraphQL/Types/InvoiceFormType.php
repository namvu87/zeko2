<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\InvoiceForm;
use GraphQL\Type\Definition\Type;

class InvoiceFormType extends GraphQLType
{
    protected $attributes = [
        'name' => 'invoice_form',
        'model' => InvoiceForm::class
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => Type::int()
            ],
            'content' => [
                'type' => Type::string()
            ],
            'user_created_id' => [
                'type' => Type::string()
            ],
            'size' => [
                'type' => Type::string()
            ]
        ];
    }
}
