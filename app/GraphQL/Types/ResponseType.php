<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ResponseType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'response',
    ];

    public function fields() :array
    {
        return [
            'code' => [
                'type' => Type::int(),
                'name' => 'code'

            ],
            'message' => [
                'type' => Type::string(),
                'name' => 'message'
            ]
        ];
    }
}