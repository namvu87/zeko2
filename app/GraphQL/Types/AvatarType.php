<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AvatarType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'avatar',
    ];

    public function fields() :array
    {
        return [
            'x1' => [
                'type' => Type::string(),
                'name' => 'x1'

            ],
            'x2' => [
                'type' => Type::string(),
                'name' => 'x2'
            ],
            'x3' => [
                'type' => Type::string(),
                'name' => 'x3'
            ]
        ];
    }
}