<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Models\ServiceFee;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL;

class ServiceFeeType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'service_fee',
        'model'       => ServiceFee::class,
        'description' => 'Phí dịch vụ'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group' => [
                'type' => GraphQL::type('group')
            ],
            'package' => [
                'type' => Type::int()
            ],
            'amount' => [
                'type' => Type::int()
            ],
            'aggregated_month' => [
                'type' => Type::string()
            ],
            'invoices_count' => [
                'type' => Type::int()
            ],
            'revenue' => [
                'type' => Type::int()
            ],
            'users_count' => [
                'type' => Type::int()
            ],
            'status' => [
                'type' => Type::int()
            ],
            'transaction_id' => [
                'type' => Type::string()
            ],
            'transaction' => [
                'type' => GraphQL::type('service_fee_transaction')
            ]
        ];
    }
}
