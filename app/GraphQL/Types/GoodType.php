<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Good;

class GoodType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'good',
        'model' => Good::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'code' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => Type::int()
            ],
            'price' => [
                'type' => Type::int()
            ],
            'price_origin' => [
                'type' => Type::float()
            ],
            'unit' => [
                'type' => Type::string()
            ],
            'discount' => [
                'type' => Type::float()
            ],
            'discount_type' => [
                'type' => Type::int()
            ],
            'is_sale' => [
                'type' => Type::boolean()
            ],
            'inventory_number' => [
                'type' => Type::float()
            ],
            'inventory_min' => [
                'type' => Type::int(),
            ],
            'inventory_max' => [
                'type' => Type::int()
            ],
            'description' => [
                'type' => Type::string()
            ],
            'group_menu_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'groupMenus' => [
                'type' => Type::listOf(GraphQL::type('group_menu')),
            ],
            'group' => [
                'type' => GraphQL::type('group')
            ],
            'properties' => [
                'type' => Type::listOf(GraphQL::type('good_property')),
                'is_relation' => false
            ],
            'images' => [
                'type' => Type::listOf(Type::string())
            ],
            'status' => [
                'type' => Type::int()
            ],
            'parent_id' => [
                'type' => Type::string()
            ],
            'childs' => [
                'type' => Type::listOf(GraphQL::type('good')),
                'description' => 'Các biến thể cùng loại (đơn vị quy đổi khác)'
            ],
            'conversion_value' => [
                'type' => Type::float(),
                'description' => 'Giá trị quy đổi của biến thể so với phiên bản gốc'
            ],
            'ingredients' => [
                'type' => Type::listOf(GraphQL::type('good_ingredient')),
                'is_relation' => false
            ]
        ];
    }

    /**
     * @param  App\Models\Good $root
     * @param  array $args
     * @return Array
     */
    protected function resolveImagesField($root, $args) {
        return $root->images ?? [];
    }
}