<?php
namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class VersionType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'version',
        'description' => 'Lấy phiên bản hiện tại khả dụng của ứng dụng'
    ];

    public function fields() :array
    {
        return [
            'ios' => [
                'type' => Type::int()
            ],
            'android' => [
                'type' => Type::int()
            ]
        ];
    }
}