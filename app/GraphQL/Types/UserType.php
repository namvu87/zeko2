<?php

namespace App\GraphQL\Types;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\User;
use App\Contracts\RoleContract;
use App\Contracts\GroupContract;
use App\Contracts\UserContract;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'user',
        'model' => User::class,
    ];

    private $group;
    private $role;
    private $id;

    public function __construct(GroupContract $group, RoleContract $role, UserContract $user)
    {
        $this->group = $group;
        $this->role = $role;
        $this->id = $user->id();
    }

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'email' => [
                'type' => Type::string()
            ],
            'phone_number' => [
                'type' => Type::string()
            ],
            'first_name' => [
                'type' => Type::string()
            ],
            'last_name' => [
                'type' => Type::string()
            ],
            'fullname' => [
                'type' => Type::string(),
                'is_relation' => false
            ],
            'area' => [
                'type' => GraphQL::type('area'),
                'is_relation' => false
            ],
            'commune' => [
                'type' => GraphQL::type('commune'),
                'is_relation' => false
            ],
            'gender' => [
                'type' => Type::int()
            ],
            'address' => [
                'type' => Type::string()
            ],
            'birthday' => [
                'type' => Type::string()
            ],
            'verified' => [
                'type' => Type::boolean(),
                'is_relation' => false,
                'description' => 'Xác định user đã xác minh email hay chưa'
            ],
            'phone_verified' => [
                'type' => Type::boolean(),
                'is_relation' => false,
                'description' => 'Xác định user đã xác minh SĐT hay chưa'
            ],
            'uuid_devices' => [
                'type' => Type::listOf(Type::string())
            ],
            'avatars' => [
                'type' => GraphQL::type('avatar'),
                'is_relation' => false
            ],
            'cover_image' => [
                'type' => Type::string()
            ],
            'group_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'shift_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'request_group_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'roles' => [
                'type' => Type::listOf(GraphQL::type('role')),
                'is_relation' => false
            ],
            'is_master' => [
                'type' => Type::boolean(),
                'is_relation' => false
            ],
            'role_group' => [
                'type' => GraphQL::type('role'),
                'is_relation' => false,
                'description' => 'Vai trò của user trong nhóm được chỉ định',
                'args' => [
                    'group_id' => Type::string()
                ]
            ],
            'groups_owner' => [
                'type' => Type::listOf(GraphQL::type('group')),
                'description' => 'Danh sách nhóm mà người dùng khởi tạo',
                'is_relation' => false
            ],
            'groups' => [
                'type' => Type::listOf(GraphQL::type('group')),
                'description' => 'Danh sách nhóm mà user là thành viên',
                'is_relation' => false
            ],
            'groups_management' => [
                'type' => Type::listOf(GraphQL::type('group')),
                'description' => 'Danh sách nhóm mà user được phân vai trò nhất định',
                'is_relation' => false
            ]
        ];
    }

    /**
     * Kiểm tra xem user này có phải master hay ko
     *
     * @param App\Models\User $root
     * @param array $args
     * @return void
     */
    protected function resolveIsMasterField($root, $args): bool
    {
        return in_array($root->email, config('master.email'));
    }

    /**
     * Lấy danh sách các vai trò
     *
     * @param  App\Models\User $root
     * @param  array $args
     * @return Maklad\Permission\Models\Role
     */
    protected function resolveRolesField($root, $args)
    {
        if ($root->id === $this->id) {
            return $this->role->getByIds($root->role_ids ?? []);
        }
        return [];
    }

    /**
     * Lấy vai trò của user trong 1 nhóm được chỉ định
     *
     * @param  App\Models\User $root
     * @param  array $args
     * @return Maklad\Permission\Models\Role
     */
    protected function resolveRoleGroupField($root, $args)
    {
        return $root->roles->where('group_id', $args['group_id'])->first();
    }

    /**
     * Lấy danh sách các nhóm mà user là thành viên
     *
     * @param  App\Models\User $root
     * @param  array $args
     * @return App\Models\Group
     */
    protected function resolveGroupsField($root, $args)
    {
        if ($root->id === $this->id) {
            return $this->group->getByIds($root->group_ids ?? []);
        }
        return [];
    }

    /**
     * Lấy danh sách các nhóm mà user đã khởi tạo
     *
     * @param  App\Models\User $root
     * @param  array $args
     * @return App\Models\Group
     */
    protected function resolveGroupsOwnerField($root, $args)
    {
        return $root->ownerGroups;
    }

    /**
     * Lấy danh sách các nhóm mà user được phân vai trò
     *
     * @param  App\Models\User $root
     * @param  array $args
     * @return App\Models\Group
     */
    protected function resolveGroupsManagementField($root, $args)
    {
        if ($root->id === $this->id) {
            $roles = $this->role->getByIds($root->role_ids ?? []);
            return $this->group->getByIds($roles->pluck('group_id')->toArray());
        }
        return [];
    }

    /**
     * @param  App\Models\User $root
     * @param  array $args
     * @return array
     */
    protected function resolveGroupIdsField($root, $args): array
    {
        return $root->group_ids ?? [];
    }

    /**
     * @param  App\Models\User $root
     * @param  array $args
     * @return array
     */
    protected function resolveShiftIdsField($root, $args): array
    {
        return $root->shift_ids ?? [];
    }

    /**
     * @param  App\Models\User $root
     * @param  array $args
     * @return array
     */
    protected function resolveRequestGroupIdsField($root, $args): array
    {
        return $root->request_group_ids ?? [];
    }

    /**
     * Xác thực user đã xác minh email hay chưa
     *
     * @param \App\Models\User $root
     * @param array $args
     * @return boolean
     */
    protected function resolveVerifiedField($root, $args): bool
    {
        return !empty($root->email_verified_at);
    }

    /**
     * Xac thuc email da xac minh SDT
     *
     * @param \App\Models\User $root
     * @param array $args
     * @return boolean
     */
    protected function resolvePhoneVerifiedField($root, $args): bool
    {
        return !empty($root->phone_number_verified_at);
    }
}
