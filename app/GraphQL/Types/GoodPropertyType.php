<?php
namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class GoodPropertyType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'good_property',
        'description' => 'Thuộc tính sản phẩm'
    ];

    public function fields() :array
    {
        return [
            'name' => [
                'type' => Type::string()
            ],
            'value' => [
                'type' => Type::string()
            ]
        ];
    }
}