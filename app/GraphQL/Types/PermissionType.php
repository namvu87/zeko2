<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Maklad\Permission\Models\Permission;

class PermissionType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'permission',
        'model' => Permission::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'label' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'guard_name' => [
                'type' => Type::string()
            ],
            'roles' => [
                'type' => Type::listOf(GraphQL::type('role'))
            ]
        ];
    }

    protected function resolveNameField($root, $args)
    {
        return str_replace($root->group_id . '.', '', $root->name);
    }
}