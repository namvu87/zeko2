<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class LocationType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'location',
        'description' => 'Location object of Nativescript'
    ];

    public function fields() :array
    {
        return [
            'latitude' => [
                'type' => Type::float(),
                'rules' => ['required']
            ],
            'longitude' => [
                'type' => Type::float(),
                'rules' => ['required']
            ],
            'altitude' => [
                'type' => Type::float()
            ],
            'horizontalAccuracy' => [
                'type' => Type::float()
            ],
            'verticalAccuracy' => [
                'type' => Type::float()
            ],
            'speed' => [
                'type' => Type::float()
            ],
            'direction' => [
                'type' => Type::float()
            ]
        ];
    }
}