<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Requirement;

class RequirementType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'requirement',
        'model' => Requirement::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'creator_id' => [
                'type' => Type::string()
            ],
            'creator' => [
                'type' => GraphQL::type('user')
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'group' => [
                'type' => GraphQL::type('group')
            ],
            'type' => [
                'type' => Type::int()
            ],
            'timekeeping_id' => [
                'type' => Type::string()
            ],
            'timekeeping' => [
                'type' => GraphQL::type('timekeeping')
            ],
            'shift_id' => [
                'type' => Type::string()
            ],
            'is_accept' => [
                'type' => Type::boolean()
            ],
            'date' => [
                'type' => Type::string()
            ],
            'content' => [
                'type' => Type::string()
            ],
            'res_content' => [
                'type' => Type::string(),
                'description' => 'Nội dung trả lời'
            ],
            'notification' => [
                'type' => GraphQL::type('notification')
            ]
        ];
    }
}