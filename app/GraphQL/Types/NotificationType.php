<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Notification;

class NotificationType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'notification',
        'model' => Notification::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'creator' => [
                'type' => GraphQL::type('user'),
                'is_relation' => false
            ],
            'invoice' => [
                'type' => GraphQL::type('invoice'),
                'is_relation' => false
            ],
            'group' => [
                'type' => GraphQL::type('group'),
                'is_relation' => false
            ],
            'shift' => [
                'type' => GraphQL::type('shift'),
                'is_relation' => false
            ],
            'type' => [
                'type' => Type::int()
            ],
            'timekeeping_id' => [
                'type' => Type::string()
            ],
            'timekeeping' => [
                'type' => GraphQL::type('timekeeping')
            ],
            'view' => [
                'type' => Type::boolean()
            ],
            'date' => [
                'type' => Type::string()
            ],
            'requirement_id' => [
                'type' => Type::string()
            ],
            'requirement' => [
                'type' => GraphQL::type('requirement')
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'updated_at' => [
                'type' => Type::string()
            ]
        ];
    }
}