<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Commune;

class CommuneType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'commune',
        'model' => Commune::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'area_id' => [
                'type' => Type::string()
            ]
        ];
    }
}