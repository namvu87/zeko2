<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class InvoiceGoodType extends GraphQLType
{
    protected $attributes = [
        'name' => 'invoice_good'
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'code' => [
                'type' => Type::string()
            ],
            'count' => [
                'type' => Type::int()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'image' => [
                'type' => Type::string()
            ],
            'price' => [
                'type' => Type::int()
            ],
            'price_origin' => [
                'type' => Type::int()
            ],
            'unit' => [
                'type' => Type::string()
            ],
            'discount' => [
                'type' => Type::int()
            ],
            'discount_type' => [
                'type' => Type::int()
            ],
            'goods_order' => [
                'type' => Type::listOf(GraphQL::type('invoice_good_item'))
            ],
            'created_at' => [
                'type' => Type::string()
            ],
        ];
    }
}