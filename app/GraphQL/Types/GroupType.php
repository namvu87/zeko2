<?php

namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Contracts\UserContract;
use App\Contracts\RoleContract;
use App\Contracts\PermissionContract;
use App\Models\Group;

class GroupType extends GraphQLType
{
    const FIRST_PAGE = 1;

    protected $attributes = [
        'name'  => 'group',
        'model' => Group::class,
    ];

    private $user;
    private $role;
    private $permission;

    public function __construct(
        UserContract $user,
        RoleContract $role,
        PermissionContract $permission
    ) {
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
    }

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'area' => [
                'type' => GraphQL::type('area'),
                'is_relation' => false
            ],
            'commune' => [
                'type' => GraphQL::type('commune'),
                'is_relation' => false
            ],
            'address' => [
                'type' => Type::string()
            ],
            'service_type' => [
                'type' => Type::int()
            ],
            'owner_id' => [
                'type' => Type::int()
            ],
            'owner' => [
                'type' => GraphQL::type('user')
            ],
            'avatars' => [
                'type' => GraphQL::type('avatar'),
                'is_relation' => false
            ],
            'cover_image' => [
                'type' => Type::string()
            ],
            'is_active' => [
                'type' => Type::boolean()
            ],
            'care_status' => [
                'type' => GraphQL::type('scalar')
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'goods_properties' => [
                'type' => Type::listOf(Type::string()),
                'description' => 'Các thuộc tính hàng hoá của group'
            ],
            'goods_units' => [
                'type' => Type::listOf(Type::string()),
                'description' => 'Các đơn vị tính trong nhóm'
            ],
            'users' => [
                'type' => Type::listOf(GraphQL::type('user')),
                'description' => 'Danh sách nhân viên trong group',
                'is_relation' => false
            ],
            'users_count' => [
                'type' => Type::int(),
                'description' => 'Số lượng nhân viên trong group'
            ],
            'roles' => [
                'type' => Type::listOf(GraphQL::type('role')),
                'description' => 'Danh sách các vai trò trong nhóm',
                'is_relation' => false
            ],
            'role_current_user' => [
                'type' => GraphQL::type('role'),
                'description' => 'Vai trò của user hiện tại với nhóm',
                'is_relation' => false
            ],
            'users_role' => [
                'type' => Type::listOf(GraphQL::type('user')),
                'description' => 'Danh sách những user được phân vai trò trong nhóm',
                'is_relation' => false
            ],
            'users_out_role' => [
                'type' => Type::listOf(GraphQL::type('user')),
                'description' => 'Danh sách những user chưa được phân vai trò trong nhóm',
                'is_relation' => false
            ],
            'permissions' => [
                'type' => Type::listOf(GraphQL::type('permission')),
                'is_relation' => false,
                'description' => 'Danh sách những quyền (permissions) của nhóm'
            ],
            'permissions_user' => [
                'type' => Type::listOf(Type::string()),
                'description' => 'Danh sách quyền của người dùng hiện tại với nhóm chỉ định'
            ],
            'goods_count' => [
                'type' => Type::int(),
                'description' => 'Đếm số lượng hàng hoá đã tạo trong nhóm'
            ],
            'invoices_count' => [
                'type' => Type::int(),
                'description' => 'Đếm số lượng hoá đơn đã tạo trong nhóm'
            ],
            'timekeepings_count' => [
                'type' => Type::int(),
                'description' => 'Đếm số lượng chấm công đã tạo trong nhóm'
            ]
        ];
    }

    protected function resolveUnitsField($root, $args)
    {
        return $root->units ?? [];
    }

    /**
     * @param  App\Models\Group $root
     * @param  array $args
     * @return App\Models\User
     */
    protected function resolveUsersField($root, $args)
    {
        return $this->user->paginateByGroupId($root->id);
    }

    /**
     * @param  App\Models\Group $root
     * @param  array $args
     * @return int
     */
    protected function resolveUsersCountField($root, $args)
    {
        return $this->user->getCountByGroupId($root->id);
    }

    /**
     * @param  App\Models\Group $root
     * @param  array $args
     * @return Maklad\Permission\Models\Role
     */
    protected function resolveRolesField($root, $args)
    {
        return $this->role->getByGroupId($root->id);
    }

    /**
     * @param  App\Models\Group $root
     * @param  array $args
     * @return App\Models\User
     */
    protected function resolveUsersRoleField($root, $args)
    {
        $roles = $this->role->getByGroupId($root->id);
        $roleIds = $roles->pluck('id')->toArray();
        return $this->user->getByRoleIds($roleIds);
    }

    /**
     *
     * @param App\Models\Group $root
     * @param array $args
     * @return Maklad\Permission\Models\Role
     */
    protected function resolveRoleCurrentUserField($root, $args)
    {
        $user = $this->user->getUser();
        return $user->roles->where('group_id', $root->id)->first();
    }

    /**
     * @param  App\Models\Group $root
     * @param  array $args
     * @return App\Models\User
     */
    protected function resolveUsersOutRoleField($root, $args)
    {
        $page = $args['page'] ?? self::FIRST_PAGE;

        return $this->user->getOutRole($root->id, $page);
    }

    protected function resolvePermissionsField($root, $args)
    {
        return $this->permission->getByGroupId($root->id);
    }

    protected function resolvePermissionsUserField($root, $args)
    {
        $user = $this->user->getUser();
        $role = $user->roles->where('group_id', $root->id)->first();

        if ($role) {
            return $this->permission
                ->getByRoleId($role->id)
                ->pluck('name')
                ->map(function ($name) use ($root) {
                    return str_replace($root->id . '.', '', $name);
                });
        }
        return null;
    }

    protected function resolveGoodsCountField($root, $args)
    {
        if ($root->service_type === Group::RESTAURANT_SERVICE) {
            return $root->goods->count();
        }
        return 0;
    }

    protected function resolveInvoicesCountField($root, $args)
    {
        if ($root->service_type === Group::RESTAURANT_SERVICE) {
            return $root->invoices->count();
        }
        return 0;
    }

    protected function resolveTimekeepingsCountField($root, $args)
    {
        if ($root->service_type === Group::TIMEKEEPING_SERVICE) {
            return $root->timekeepings->count();
        }
        return 0;
    }
}
