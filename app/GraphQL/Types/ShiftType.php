<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Shift;

class ShiftType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'shift',
        'model' => Shift::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'group' => [
                'type' => GraphQL::type('group')
            ],
            'type' => [
                'type' => Type::int()
            ],
            'allow_time' => [
                'type' => Type::int()
            ],
            'schedules' => [
                'type' => Type::listOf(Type::string())
            ],
            'times' => [
                'type' => Type::listOf(GraphQL::type('shift_time')),
                'description' => 'Lưu thời gian ca làm việc',
                'is_relation' => false
            ]
        ];
    }
}