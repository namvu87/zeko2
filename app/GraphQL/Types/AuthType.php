<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AuthType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'auth',
        'description' => 'Dữ liệu trả về sau khi xác thực (đăng nhập, đăng ký)'
    ];

    public function fields() :array
    {
        return [
            'access_token' => [
                'type' => Type::nonNull(Type::string())
            ],
            'user' => [
                'type' => GraphQL::type('user')
            ]
        ];
    }
}