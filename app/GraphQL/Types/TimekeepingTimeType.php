<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TimekeepingTimeType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'timekeeping_time',
        'description' => 'Timekeeping time type'
    ];

    public function fields() :array
    {
        return [
            'checkin' => [
                'type' => GraphQL::type('timekeeping_time_item'),
                'is_relation' => false,
                'description' => 'Mảng dữ liệu chấm công vào ca'
            ],
            'checkout' => [
                'type' => GraphQL::type('timekeeping_time_item'),
                'is_relation' => false,
                'description' => 'Mảng dữ liệu chấm công ra về'
            ]
        ];
    }
}