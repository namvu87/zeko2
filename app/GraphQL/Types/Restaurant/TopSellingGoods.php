<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Restaurant;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TopSellingGoods extends GraphQLType
{
    protected $attributes = [
        'name' => 'top_selling_goods',
        'description' => 'Top hàng hoá bán chạy'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'image' => [
                'type' => Type::string()
            ],
            'unit' => [
                'type' => Type::string()
            ],
            'total' => [
                'type' => Type::float(),
                'description' => 'Doanh thu hàng hoá'
            ],
            'count' => [
                'type' => Type::int(),
                'description' => 'Số lượng hàng hoá'
            ]
        ];
    }
}
