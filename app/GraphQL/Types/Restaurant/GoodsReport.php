<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Restaurant;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class GoodsReport extends GraphQLType
{
    protected $attributes = [
        'name' => 'goods_report',
        'description' => 'Báo cáo bán hàng theo hàng hoá'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'image' => [
                'type' => Type::string()
            ],
            'unit' => [
                'type' => Type::string()
            ],
            'total' => [
                'type' => Type::float(),
                'description' => 'Doanh thu hàng hoá'
            ],
            'total_origin' => [
                'type' => Type::float(),
                'description' => 'Tổng giá gốc hàng hoá'
            ],
            'total_discount' => [
                'type' => Type::float(),
                'description' => 'Tổng giá trị giảm giá của hàng hoá'
            ],
            'total_returned' => [
                'type' => Type::float(),
                'description' => 'Tổng giá trị của hàng hoá đã trả lại'
            ],
            'count' => [
                'type' => Type::int(),
                'description' => 'Số lượng hàng hoá'
            ],
            'count_returned' => [
                'type' => Type::int(),
                'description' => 'Số lượng hàng hoá đã trả lại'
            ]
        ];
    }
}
