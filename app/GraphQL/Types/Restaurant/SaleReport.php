<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Restaurant;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class SaleReport extends GraphQLType
{
    protected $attributes = [
        'name' => 'restaurant_sale_report',
        'description' => 'Loại dữ liệu thống kê bán hàng'
    ];

    public function fields(): array
    {
        return [
            'time' => [
                'type' => GraphQL::type('date_time_object')
            ],
            'total' => [
                'type' => Type::float()
            ],
            'discount' => [
                'type' => Type::float()
            ],
            'total_returned' => [
                'type' => Type::float()
            ],
            'total_origin' => [
                'type' => Type::float()
            ]
        ];
    }

    protected function resolveTimeField($root, $args)
    {
        return $root->_id;
    }
}
