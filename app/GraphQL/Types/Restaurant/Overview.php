<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Restaurant;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Overview extends GraphQLType
{
    protected $attributes = [
        'name' => 'restaurant_overview',
        'description' => 'Báo cáo số liệu sơ bộ trong ngày'
    ];

    public function fields(): array
    {
        return [
            'inited_invoice' => [
                'type' => Type::int(),
                'description' => 'Số lượng hoá đơn đang hoạt động'
            ],
            'purchased_invoice' => [
                'type' => Type::int(),
                'description' => 'Số lượng hoá đơn đã thanh toán'
            ],
            'returned_invoice' => [
                'type' => Type::int(),
                'description' => 'Số lượng hoá đơn trả hàng'
            ],
            'revenue' => [
                'type' => Type::float(),
                'description' => 'Doanh thu trong ngày'
            ]
        ];
    }
}
