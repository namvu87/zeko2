<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Timekeeping;

class TimekeepingType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'timekeeping',
        'model' => Timekeeping::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'group' => [
                'type' => GraphQL::type('group')
            ],
            'date' => [
                'type' => Type::string()
            ],
            'times' => [
                'type' => Type::listOf(GraphQL::type('timekeeping_time')),
                'is_relation' => false,
                'description' => 'Mảng các mảng checkin, checkout'
            ],
            'user_id' => [
                'type' => Type::string()
            ],
            'user' => [
                'type' => GraphQL::type('user')
            ],
            'shift_type' => [
                'type' => Type::int()
            ],
            'shift_name' => [
                'type' => Type::string()
            ],
            'target_time' => [
                'type' => Type::listOf(GraphQL::type('shift_time')),
                'is_relation' => false
            ],
            'is_addition' => [
                'type' => Type::boolean()
            ],
            'is_switch' => [
                'type' => Type::boolean()
            ],
            'is_take_leave' => [
                'type' => Type::boolean()
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'updated_at' => [
                'type' => Type::string()
            ]
        ];
    }
}