<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class GoodImageType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'good_image',
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id'   => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'urls' => [
                'type' => Type::listOf(Type::string())
            ]
        ];
    }
}
