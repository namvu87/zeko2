<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Maklad\Permission\Models\Role;

class RoleType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'role',
        'model' => Role::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'permission_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'guard_name' => [
                'type' => Type::string()
            ],
            'permissions' => [
                'type' => Type::listOf(GraphQL::type('permission')),
                'description' => 'Danh sách các permissions của role'
            ]
        ];
    }

    protected function resolveNameField($root, $args)
    {
        return str_replace($root->group_id . '.', '', $root->name);
    }

    protected function resolvePermissionIdsField($root, $args)
    {
        return $root->permission_ids ?? [];
    }
}