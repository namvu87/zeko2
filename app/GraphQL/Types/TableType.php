<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Table;

class TableType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'table',
        'model' => Table::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::string()
            ],
            'count_seat' => [
                'type' => Type::int()
            ],
            'place_id' => [
                'type' => Type::string()
            ],
            'place' => [
                'type' => GraphQL::type('place')
            ],
            'status' => [
                'type' => Type::int()
            ],
            'sort' => [
                'type' => Type::int()
            ],
            'invoice_id' => [
                'type' => Type::string()
            ],
            'invoice' => [
                'type' => GraphQL::type('invoice')
            ]
        ];
    }
}