<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use Rebing\GraphQL\Support\Facades\GraphQL;
use App\Models\ReturnedInvoice;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ReturnedInvoiceType extends GraphQLType
{
    protected $attributes = [
        'name' => 'returned_invoice',
        'model' => ReturnedInvoice::class,
        'description' => 'Hoá đơn trả hàng'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'type' => Type::nonNull(Type::string())
            ],
            'invoice_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'status' => [
                'type' => Type::int()
            ],
            'total' => [
                'type' => Type::int()
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'note' => [
                'type' => Type::string()
            ],
            'goods' => [
                'type' => GraphQL::type('scalar')
            ],
            'creator_id' => [
                'type' => Type::string()
            ],
            'creator' => [
                'type' => GraphQL::type('user')
            ],
            'invoice' => [
                'type' => GraphQL::type('invoice')
            ]
        ];
    }
}
