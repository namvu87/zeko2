<?php
namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ShiftTimeType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'shift_time',
        'description' => 'Shift time type'
    ];

    public function fields() :array
    {
        return [
            'checkin' => [
                'type' => Type::string()
            ],
            'checkout' => [
                'type' => Type::string()
            ]
        ];
    }
}