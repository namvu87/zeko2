<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Report;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Descartes extends GraphQLType
{
    protected $attributes = [
        'name' => 'descartes',
        'description' => 'Hệ toạ độ descartes cho dữ liệu thống kê'
    ];

    public function fields(): array
    {
        return [
            'x' => [
                'type' => Type::string()
            ],
            'y' => [
                'type' => Type::string()
            ]
        ];
    }
}
