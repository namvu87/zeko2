<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Report;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class DateTimeObject extends GraphQLType
{
    protected $attributes = [
        'name' => 'date_time_object',
        'description' => 'Kiểu dữ liệu datetime trong toán tử $group _id của mongodb'
    ];

    public function fields(): array
    {
        return [
            'year' => [
                'type' => Type::int()
            ],
            'month' => [
                'type' => Type::string()
            ],
            'day_of_week' => [
                'type' => Type::string()
            ],
            'day' => [
                'type' => Type::string(),
                'description' => 'day of month'
            ],
            'hour' => [
                'type' => Type::string()
            ]
        ];
    }

    protected function resolveMonthField($root, $args)
    {
        if (empty($root->month)) {
            return null;
        }
        return $root->month > 9 ? $root->month : '0' . $root->month;
    }

    protected function resolveDayOfWeekField($root, $args)
    {
        if (empty($root->day_of_week)) {
            return null;
        }
        return __("datetime.mongo_week." . $root->day_of_week);
    }

    protected function resolveDayField($root, $args)
    {
        if (empty($root->day)) {
            return null;
        }
        return $root->day > 9 ? $root->day : '0' . $root->day;
    }

    protected function resolveHourField($root, $args)
    {
        if (empty($root->hour)) {
            return null;
        }
        return $root->hour > 9 ? $root->hour . ':00' : '0' . $root->hour . ':00';
    }
}
