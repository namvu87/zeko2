<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Place;

class PlaceType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'place',
        'model' => Place::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'tables' => [
                'type' => Type::listOf(GraphQL::type('table'))
            ]
        ];
    }
}