<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use App\Models\ServiceFeeTransaction;

class ServiceFeeTransactionType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'service_fee_transaction',
        'model'       => ServiceFeeTransaction::class,
        'description' => 'Transaction của service fee tương ứng'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'status' => [
                'type' => Type::int()
            ],
            'amount' => [
                'type' => Type::int()
            ],
            'user_id' => [
                'type' => Type::string()
            ],
            'user' => [
                'type' => GraphQL::type('user')
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'updated_at' => [
                'type' => Type::string()
            ]
        ];
    }
}
