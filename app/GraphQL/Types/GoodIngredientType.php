<?php
namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class GoodIngredientType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'good_ingredient',
        'description' => 'Thành phần sản phẩm'
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'code' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'unit' => [
                'type'  => Type::string()
            ],
            'price_origin' => [
                'type'  => Type::int()
            ],
            'count' => [
                'type'  => Type::float()
            ]
        ];
    }
}