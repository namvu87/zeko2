<?php

namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Slider;

class SliderType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'slider',
        'model' => Slider::class
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'title' => [
                'type' => Type::string()
            ],
            'photo_url' => [
                'type' => GraphQL::type('slider_image'),
                'is_relation' => false
            ],
            'link' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => Type::int()
            ],
            'status' => [
                'type' => Type::int()
            ]
        ];
    }
}