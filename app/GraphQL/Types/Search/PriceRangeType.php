<?php
namespace App\GraphQL\Types\Search;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class PriceRangeType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'price_range',
    ];

    public function fields() :array
    {
        return [
            'min' => [
                'type' => Type::int(),

            ],
            'max' => [
                'type' => Type::int(),
            ]
        ];
    }
}