<?php
namespace App\GraphQL\Types\Search;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Province;

class ProvinceType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'province',
        'model' => Province::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'location' => [
                'type' => GraphQL::type('geo_json'),
                'is_relation' => false
            ]
        ];
    }
}