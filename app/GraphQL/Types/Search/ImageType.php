<?php
namespace App\GraphQL\Types\Search;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ImageType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'image',
    ];

    public function fields() :array
    {
        return [
            'thumb' => [
                'type' => Type::string(),

            ],
            'origin' => [
                'type' => Type::string(),
            ]
        ];
    }
}