<?php
namespace App\GraphQL\Types\Search;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class GeoJsonType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'geo_json'
    ];

    public function fields() :array
    {
        return [
            'type' => [
                'type' => Type::string()
            ],
            'coordinates' => [
                'type' => Type::listOf(Type::float())
            ]
        ];
    }
}