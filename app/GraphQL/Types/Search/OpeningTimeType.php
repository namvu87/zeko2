<?php
namespace App\GraphQL\Types\Search;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class OpeningTimeType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'opening_time',
    ];

    public function fields() :array
    {
        return [
            'start' => [
                'type' => Type::string(),

            ],
            'end' => [
                'type' => Type::string(),
            ]
        ];
    }
}