<?php
namespace App\GraphQL\Types\Search;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Restaurant;

class RestaurantType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'restaurant',
        'model' => Restaurant::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'avatar' => [
                'type' => GraphQL::type('image'),
                'is_relation' => false
            ],
            'images' => [
                'type' => Type::listOf(GraphQL::type('restaurant_image'))
            ],
            'province' => [
                'type' => Type::string()
            ],
            'district' => [
                'type' => Type::string()
            ],
            'categories' => [
                'type' => Type::listOf(Type::string())
            ],
            'cuisines' => [
                'type' => Type::listOf(Type::string())
            ],
            'address' => [
                'type' => Type::string()
            ],
            'opening_times' => [
                'type' => GraphQL::type('opening_time'),
                'is_relation' => false
            ],
            'price_range' => [
                'type' => GraphQL::type('price_range'),
                'is_relation' => false,
            ],
            'type' => [
                'type' => Type::int()
            ],
            'location' => [
                'type' => GraphQL::type('geo_json'),
                'is_relation' => false
            ],
            'distance' => [
                'type' => Type::string()
            ]
        ];
    }
}