<?php
namespace App\GraphQL\Types\Search;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\RestaurantImage;

class RestaurantImageType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'restaurant_image',
        'model' => RestaurantImage::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'thumb' => [
                'type' => Type::string(),

            ],
            'origin' => [
                'type' => Type::string(),
            ],
            'restaurant_id' => [
                'type' => Type::string()
            ]
        ];
    }
}