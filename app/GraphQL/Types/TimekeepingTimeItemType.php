<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TimekeepingTimeItemType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'timekeeping_time_item',
        'description' => 'Timekeeping time item type'
    ];

    public function fields() :array
    {
        return [
            'time' => [
                'type' => Type::string()
            ],
            'location' => [
                'type' => GraphQL::type('location'),
                'is_relation' => false,
                'description' => 'Mảng lưu toạ độ, tốc độ, độ chính xác...'
            ],
            'address' => [
                'type' => Type::string()
            ],
            'photo_url' => [
                'type' => Type::string()
            ]
        ];
    }
}