<?php
namespace App\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\GroupMenu;

class GroupMenuType extends GraphQLType
{
    protected $attributes = [
        'name'  => 'group_menu',
        'model' => GroupMenu::class,
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'parent_id' => [
                'type' => Type::string()
            ],
            'level' => [
                'type' => Type::int()
            ],
            'parent' => [
                'type' => GraphQL::type('group_menu')
            ],
            'childs' => [
                'type' => Type::listOf(GraphQL::type('group_menu'))
            ],
            'goods_ids' => [
                'type' => Type::listOf(Type::string())
            ]
        ];
    }
}