<?php

namespace App\GraphQL\Types;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Contracts\UserContract;
use App\Contracts\Restaurant\TableContract;
use App\Models\Invoice;

class InvoiceType extends GraphQLType
{
    protected $attributes = [
        'name' => 'invoice',
        'model' => Invoice::class
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'type' => Type::string()
            ],
            'recipient_id' => [
                'type' => Type::string(),
                'description' => 'Người nhận thanh toán'
            ],
            'recipient' => [
                'type' => GraphQL::type('user')
            ],
            'group_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'group' => [
                'type' => GraphQL::type('group')
            ],
            'user_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'table_ids' => [
                'type' => Type::listOf(Type::string())
            ],
            'tables' => [
                'type' => Type::listOf(GraphQL::type('table')),
                'is_relation' => false,
                'selectable' => false
            ],
            'goods' => [
                'type' => GraphQL::type('scalar')
            ],
            'total' => [
                'type' => Type::float()
            ],
            'total_origin' => [
                'type' => Type::float()
            ],
            'discount' => [
                'type' => Type::float()
            ],
            'discount_type' => [
                'type' => Type::int(),
                'description' => '1: VND, 2: %'
            ],
            'total_discount' => [
                'type' => Type::float()
            ],
            'note' => [
                'type' => Type::string()
            ],
            'conversation' => [
                'type' => GraphQL::type('scalar')
            ],
            'vat' => [
                'type' => Type::int()
            ],
            'vat_type' => [
                'type' => Type::int(),
                'description' => '1: VND, 2: 5'
            ],
            'status' => [
                'type' => Type::int(),
                'description' => '1: inited, 2: purchased'
            ],
            'request_status' => [
                'type' => Type::int(),
                'description' => '0: none request, 1: create request, 2: add food request, 3: purchase request'
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'updated_at' => [
                'type' => Type::string()
            ],
            'users' => [
                'type' => Type::listOf(GraphQL::type('user')),
                'is_relation' => false,
                'selectable' => false
            ],
            'notifications' => [
                'type' => GraphQL::type('scalar'),
                'resolve' => function ($root, $args) {
                    return $root->notifications ?? [];
                },
                'selectable' => false
            ],
            'returned_invoices' => [
                'type' => Type::listOf(GraphQL::type('returned_invoice')),
                'description' => 'Danh sách các hoá đơn trả hàng',
                'resolve' => function ($root, $args) {
                    return $root->returnedInvoices ?? [];
                },
                'selectable' => false
            ]
        ];
    }

    /**
     * Danh sách user trong 1 hoá đơn
     *
     * @param  Invoice $root
     * @param  array $args
     * @return User|null
     */
    protected function resolveUsersField($root, $args)
    {
        $user = app()->make(UserContract::class);
        return $user->getByIds($root->user_ids ?? []);
    }

    /**
     * Danh sách bàn trong hoá đơn
     *
     * @param Invoice $root
     * @param array $args
     * @return Table|null
     */
    protected function resolveTablesField($root, $args)
    {
        $table = app()->make(TableContract::class);
        return $table->getByIds($root->table_ids ?? []);
    }
}
