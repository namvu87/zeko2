<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Invoice;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class NotificationItemType extends GraphQLType
{
    protected $attributes = [
        'name' => 'InvoiceNotificationItem',
        'description' => 'Các thông báo của invoice'
    ];

    public function fields(): array
    {
        return [
            'from_type' => [
                'type' => Type::int()
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => Type::int()
            ],
            'creator' => [
                'type' => GraphQL::type('InvoiceNotificationCreator')
            ],
            'is_execute' => [
                'type' => Type::boolean(),
                'description' => 'Đã được xác nhận hay chưa'
            ],
            'goods' => [
                'type' => Type::listOf(GraphQL::type('InvoiceNotificationGoods'))
            ]
        ];
    }
}
