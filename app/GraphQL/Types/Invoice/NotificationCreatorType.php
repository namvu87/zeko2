<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Invoice;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class NotificationCreatorType extends GraphQLType
{
    protected $attributes = [
        'name' => 'InvoiceNotificationCreator'
    ];

    public function fields(): array
    {
        return [
            'fullname' => [
                'type' => Type::string()
            ],
            'avatar' => [
                'type' => Type::string()
            ],
            'id' => [
                'type' => Type::string()
            ]
        ];
    }
}
