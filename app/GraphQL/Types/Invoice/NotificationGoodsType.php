<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Invoice;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class NotificationGoodsType extends GraphQLType
{
    protected $attributes = [
        'name' => 'InvoiceNotificationGoods',
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'unit' => [
                'type' => Type::string()
            ],
            'count' => [
                'type' => Type::int()
            ]
        ];
    }
}
