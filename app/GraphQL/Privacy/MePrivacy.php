<?php
namespace App\GraphQL\Privacy;

use Rebing\GraphQL\Support\Privacy;
use App\Contracts\UserContract;

class MePrivacy extends Privacy
{
    private $id;

    public function __construct(UserContract $user)
    {
        $this->id = $user->id();
    }

    public function validate(array $args)
    {
        return $args['id'] === $this->id;
    }
}