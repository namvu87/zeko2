<?php
namespace App\GraphQL\Traits\Timekeeping;

use App\Models\Requirement as RequirementModel;
use App\Models\Shift;
use App\Models\Timekeeping;
use App\Models\Group;
use App\Models\User;

trait Requirement
{
    /**
     * Tạo dữ liệu chấm công từ requirement
     *
     * @param array $args
     * @param RequirementModel $requirement
     * @param Shift $shift
     * @return Timekeeping
     */
    private function createTimekeeping(
        array $args, RequirementModel $requirement, Shift $shift, int $indirectType
    ): Timekeeping
    {
        $data = [
            'group_id'      => $args['group_id'],
            'date'          => $requirement->date,
            'user_id'       => $requirement->creator_id,
            'shift_name'    => $shift->name,
            'target_time'   => $shift->times,
            'shift_type'    => $shift->type,
            'times' => [
                [
                    'checkin' => [
                        'time' => $args['checkin_1'],
                        'type' => $indirectType
                    ],
                    'checkout' => [
                        'time' => $args['checkout_1'],
                        'type' => $indirectType
                    ]
                ]
            ]
        ];

        if ($shift->type === Shift::MULTIPLE_TIME_TYPE) {
            $data['times'][] = [
                'checkin' => [
                    'time' => $args['checkin_2'] ?? null,
                    'type' => $indirectType
                ],
                'checkout' => [
                    'time' => $args['checkout_2'] ?? null,
                    'type' => $indirectType
                    ]
            ];
        }
        return $this->timekeeping->create($data);
    }

    private function updateTimekeeping(Timekeeping $timekeeping, array $args, int $indirectType): Timekeeping
    {
        $times = $timekeeping->times;
        if (empty($times[0]['checkin'])) {
            $times[0]['checkin'] = [
                'time' => $args['checkin_1'],
                'type' => $indirectType
            ];
        }
        if (empty($times[0]['checkout'])) {
            $times[0]['checkout'] = [
                'time' => $args['checkout_1'],
                'type' => $indirectType
            ];
        }
        if ($timekeeping->shift_type === Shift::MULTIPLE_TIME_TYPE) {
            if (empty($times[1]['checkin'])) {
                $times[1]['checkin'] = [
                    'time' => $args['checkin_2'],
                    'type' => $indirectType
                ];
            }
            if (empty($times[1]['checkout'])) {
                $times[1]['checkout'] = [
                    'time' => $args['checkout_2'],
                    'type' => $indirectType
                ];
            }
        }
        return $this->timekeeping->save($timekeeping, [
            'times' => $times
        ]);
    }
}