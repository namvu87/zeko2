<?php

namespace App\GraphQL\Traits;

use App\Models\Invoice;

trait CalculatePrice
{
    /**
     * Xác định giá trị thực được bán của goods item
     *
     * @param array $item
     * @return void
     */
    private function getSalePriceGoods(array $item)
    {
        return $item['price'] - $this->getDiscountGoods($item);
    }

    /**
     * Xác định giá trị giảm giá của Goods item
     *
     * @param array $item
     * @return void
     */
    private function getDiscountGoods(array $item)
    {
        if (!$item['discount']) {
            return 0;
        }
        if ($item['discount_type'] === Invoice::DISCOUNT_TYPE_FIXED) {
            return $item['discount'];
        }
        return $item['discount'] * $item['price'] / 100;
    }
}