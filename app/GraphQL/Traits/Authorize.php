<?php

namespace App\GraphQL\Traits;

use Rebing\GraphQL\Error\AuthorizationError;

trait Authorize
{
     /**
     * Xác thực dữ liệu  tuỳ chỉnh
     *
     * @param string          $groupId
     * @param App\Models\User $context
     * @return void
     */
    public function customAuthorize($groupId, $context)
    {
        $permission = $groupId . self::PERMISSION;
        if (!$context->can($permission)) {
            throw new AuthorizationError('Unauthorize');
        }
    }
}