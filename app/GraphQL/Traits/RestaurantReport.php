<?php

namespace App\GraphQL\Traits;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use MongoDB\BSON\UTCDateTime;
use App\Models\Invoice;

trait RestaurantReport
{
    /**
     * Xác định mốc thời gian cho báo cáo
     *
     * @param array $args
     * @return array
     */
    protected function resolveDate(array $args): array
    {
        $startDate = isset($args['start_date']) ? 
                                Carbon::parse($args['start_date'])->startOfDay() :
                                Carbon::now()->startOfDay();
        $endDate = isset($args['end_date']) ? 
                                Carbon::parse($args['end_date'])->endOfDay() :
                                Carbon::now()->endOfDay();
        return [$startDate, $endDate];
    }

    /**
     * Xác định khoảng thời gian
     *
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return int
     */
    protected function resolvePeriod(Carbon $startDate, Carbon $endDate): int
    {
        return CarbonPeriod::create($startDate, $endDate)->count(); 
    }

    /**
     * So khớp dữ liệu thống kê
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    protected function matchPurchasedInvoice(string $groupId, Carbon $startDate, Carbon $endDate): array
    {
        return [
            '$match' => [
                'group_id' => $groupId,
                'status' => Invoice::STATUS_PURCHASED,
                'created_at' => [
                    '$gte' => new UTCDateTime($startDate),
                    '$lte' => new UTCDateTime($endDate)
                ]
            ]
        ];
    }

    /**
     * Xác định đầu ra cấu trúc dữ liệu (_id trong toán tử $group)
     */
    protected function resolveGroupIdOperator($period) :array
    {
        $groupIdOperator = null;
        $sort = [];

        // Nếu khoảng thời gian trong 1 ngày thì xem theo giờ
        if ($period === 1) {
            $groupIdOperator = [
                'hour' => [
                    '$hour' => $this->timeOperator
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.hour' => -1
                ]
            ];
            return [$groupIdOperator, $sort];
        }

        // Nếu khoảng thời gian lớn hơn 1 thì xem báo cáo theo ngày
        if ($period <= 31) {
            $groupIdOperator = [
                'day' => [
                    '$dayOfMonth' => $this->timeOperator
                ],
                'month' => [
                    '$month' => $this->timeOperator
                ],
                'year' => [
                    '$year' => $this->timeOperator
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.day' => -1,
                    '_id.month' => -1,
                    '_id.year' => -1
                ]
            ];
            return [$groupIdOperator, $sort];
        }

        // Nếu khoảng thời gian lớn hơn 31 ngày thì mặc định là xem báo cáo theo tháng
        if ($period <= 365) {
            $groupIdOperator = [
                'month' => [
                    '$month' => $this->timeOperator
                ],
                'year' => [
                    '$year' => $this->timeOperator
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.month' => -1,
                    '_id.year' => -1
                ]
            ];
            return [$groupIdOperator, $sort];
        }

        // Nếu khoảng thời gian lớn hơn 365 ngày thì xem theo năm
        if ($period > 365) {
            $groupIdOperator = [
                'year' => [
                    '$year' => $this->timeOperator
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.year' => -1
                ]
            ];
        }

        return [$groupIdOperator, $sort];
    }

    /**
     * Chuyển dữ liệu về dạng phù hợp
     *
     * @param array $data
     * @param integer $period
     * @return array|object
     */
    private function normalizeData($data, int $period)
    {
        foreach ($data as &$item) {
            if ($period === 1) {
                $item['time'] = $item->_id->hour . ':00';
                unset($item->_id);
                continue;
            }
            if ($period <= 31) {
                $item['time'] = ($item->_id->day > 9 ? $item->_id->day : '0' . $item->_id->day)
                    . '-' . 
                    ($item->_id->month > 9 ? $item->_id->month : '0' . $item->_id->month)
                    . '-' . $item->_id->year;
                unset($item->_id);
                continue;
            }
            if ($period <= 365) {
                $item['time'] = ($item->_id->month > 9 ? $item->_id->month : '0' . $item->_id->month)
                                . '-' . $item->_id->year;
                unset($item->_id);
                continue;
            }
            if ($period > 365) {
                $item['time'] = $item->_id->year;
                unset($item->_id);
                continue;
            }
            
        }

        return $data;
    }
}