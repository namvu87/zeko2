<?php
namespace App\GraphQL\Traits;

trait ShiftRequest
{
    /**
     * Định nghĩa các quy tắc xác thực dữ liệu (validation)
     *
     * @param  array $args
     *
     * @return aray
     */
    public function rules(array $args = []): array
    {
        $rules = [
            'group_id'     => 'required|string',
            'start_time_1' => 'required|date_format:H:i',
            'end_time_1'   => 'required|date_format:H:i|after:start_time_1',
            'name'         => 'required|string',
            'allow_time'   => 'numeric|nullable',
            'schedules.*'  => 'string|in:' . implode(',', $this->shift->getSchedules()),
            'type'         => 'required:in:' . implode(',', $this->shift->getTypes())
        ];
        if ($args['type'] === $this->shift->getMultiType()) {
            $rules['start_time_2'] = 'required|date_format:H:i|after:end_time_1';
            $rules['end_time_2']   = 'required|date_format:H:i|after:start_time_2';
        }
        return $rules;
    }

    /**
     * Prepare data for create and update
     *
     * @param  array $args
     *
     * @return array
     */
    private function prepareData($args)
    {
        $data = [
            'name'          => $args['name'],
            'group_id'      => $args['group_id'],
            'type'          => $args['type'],
            'schedules'     => $args['schedules'],
            'allow_time'    => $args['allow_time'],
            'times' => [
                [
                    'checkin'   => $args['start_time_1'],
                    'checkout'  => $args['end_time_1']
                ],
            ]
        ];
        if ($args['type'] === $this->shift->getMultiType()) {
            $data['times'][] = [
                'checkin'   => $args['start_time_2'],
                'checkout'  => $args['end_time_2']
            ];
        }
        return $data;
    }
}
