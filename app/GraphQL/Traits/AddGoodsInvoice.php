<?php

namespace App\GraphQL\Traits;

use Carbon\Carbon;
use Rebing\GraphQL\Error\AuthorizationError;

trait AddGoodsInvoice
{
    /**
     * Tạo trường hàng hoá chuẩn dữ liệu
     *
     * @param  array $goods        Mảng mới, chứ goods cần đặt thêm
     * @param  array $currentGoods Mảng goods hiện tại
     * @return array
     */
    private function resolveGoodsField(array $newGoods, array $currentGoods, string $groupId) :array
    {        
        // Xử lý goods đã có trong hoá đơn
        $data = [];
        foreach ($currentGoods as $current) {
            foreach ($newGoods as $new) {
                if ($current['id'] === $new['id']) {
                    $current['orders'][] = [
                        'count' => (int) $new['count'],
                        'status' => $this->invoice->getGoodBookedStatus(),
                        'created_at' => Carbon::now()->toDateTimeString()
                    ];
                    $current['count'] += (int) $new['count'];
                    continue;
                }
            }
            $data[] = $current;
        }

        // xử lý goods đặt thêm chưa có trong hoá đơn
        $currentGoodsIds = collect($currentGoods)->pluck('id')->all();

        // Mảng các notification items thêm mới
        $notificationItems = [];
        
        foreach ($newGoods as $new) {
            $good = $this->good->getById($new['id']);
            if (empty($good) || $good->group_id !== $groupId) {
                throw new AuthorizationError('Unauthorized');
            }
            $notificationItems[] = [
                'id' => $good->id,
                'name' => $good->name,
                'unit' => $good->unit,
                'count' => (int) $new['count']
            ];
            if (!in_array($new['id'], $currentGoodsIds)) {
                $data[] = [
                    'id'            => $good->id,
                    'code'          => $good->code,
                    'name'          => $good->name,
                    'image'         => $good->images[0] ?? '',
                    'price'         => $good->price ?? 0,
                    'price_origin'  => $good->price_origin ?? 0,
                    'unit'          => $good->unit,
                    'discount'      => $good->discount ?? 0,
                    'discount_type' => $good->discount_type ?? 1,
                    'count'         => (int) $new['count'],
                    'orders'        => [
                        [
                            'count' => (int) $new['count'],
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'status' => $this->invoice->getGoodBookedStatus()
                        ]
                    ]
                ];
            }
        }
        return [$data, $notificationItems];
    }
}