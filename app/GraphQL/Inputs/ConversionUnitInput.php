<?php
namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ConversionUnitInput extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name'  => 'ConversionUnitInput',
        'description' => 'Các biến thể sản phẩm'
    ];

    public function fields() :array
    {
        return [
            'unit' => [
                'type'  => Type::string(),
                'rules' => ['required', 'string', 'max:255']
            ],
            'value' => [
                'type'  => Type::float(),
                'rules' => ['required', 'numeric']
            ],
            'price' => [
                'type'  => Type::int(),
                'rules' => ['required', 'int']
            ]
        ];
    }
}