<?php
namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class PayServiceFeeInput extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name'  => 'PayServiceFeeInput',
        'description' => 'Input client for Pay Service Fee'
    ];

    public function fields() :array
    {
        return [
            'service_fee_id' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'package' => [
                'type' => Type::int(),
                'description' => 'Gói thanh toán đã lựa chọn (áp dụng cho dịch vụ nhà hàng)',
                'rules' => ['nullable', 'in:1,2,3']
            ]
        ];
    }
}