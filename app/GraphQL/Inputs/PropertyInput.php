<?php
namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class PropertyInput extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name'  => 'PropertyInput',
        'description' => 'Thuộc tính sản phẩm'
    ];

    public function fields() :array
    {
        return [
            'name' => [
                'type' => Type::string(),
                'rules' => ['required', 'string', 'max:255']
            ],
            'value' => [
                'type' => Type::string(),
                'rules' => ['required', 'string', 'max:255']
            ]
        ];
    }
}