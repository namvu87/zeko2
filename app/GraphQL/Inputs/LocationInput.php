<?php
namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class LocationInput extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name'  => 'LocationInput',
        'description' => 'Input client for location'
    ];

    public function fields() :array
    {
        return [
            'latitude' => [
                'type' => Type::float(),
                'rules' => ['required']
            ],
            'longitude' => [
                'type' => Type::float(),
                'rules' => ['required']
            ],
            'altitude' => [
                'type' => Type::float()
            ],
            'horizontalAccuracy' => [
                'type' => Type::float()
            ],
            'verticalAccuracy' => [
                'type' => Type::float()
            ],
            'speed' => [
                'type' => Type::float()
            ],
            'direction' => [
                'type' => Type::float()
            ]
        ];
    }
}