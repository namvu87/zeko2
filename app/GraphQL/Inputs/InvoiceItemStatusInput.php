<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\InputType;

class InvoiceItemStatusInput extends InputType
{
    protected $attributes = [
        'name' => 'InvoiceItemStatusInput',
        'description' => 'Mảng các item cần cập nhật trạng thái trong hoá đơn',
    ];

    public function fields(): array
    {
        return [
            'item_index' => [
                'type' => Type::int(),
                'description' => 'index của mảng invoice.goods'
            ],
            'order_item_index' => [
                'type' => Type::int(),
                'description' => 'index của mảng invoice.goods.orders'
            ]
        ];
    }
}
