<?php
namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class InvoiceGoodInput extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name'  => 'InvoiceGoodInput',
        'description' => 'Input good for create invoice'
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'rules' => ['required', 'string', 'max:32']
            ],
            'count' => [
                'type' => Type::string(),
                'rules' => ['required', 'integer']
            ]
        ];
    }
}