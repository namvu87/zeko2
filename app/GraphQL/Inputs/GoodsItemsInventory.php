<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\InputType;

class GoodsItemsInventory extends InputType
{
    protected $attributes = [
        'name' => 'GoodsItemsInventory',
        'description' => 'Goods items được trả lại',
    ];

    public function fields(): array
    {
        return [
            'goods_id' => [
                'type' => Type::string()
            ],
            'count' => [
                'type' => Type::int(),
                'description' => 'Số lượng trả lại'
            ]
        ];
    }
}
