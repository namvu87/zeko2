<?php
namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CommuneInput extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name'  => 'CommuneInput',
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'rules' => ['required', 'string', 'max:255']
            ],
            'name' => [
                'type' => Type::string(),
                'rules' => ['required', 'exists:communes,name']
            ]
        ];
    }
}