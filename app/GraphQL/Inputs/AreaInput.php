<?php
namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AreaInput extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name'  => 'AreaInput',
        'description' => 'Input client for area'
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'rules' => ['required', 'string', 'max:255']
            ],
            'name' => [
                'type' => Type::string(),
                'rules' => ['required', 'exists:areas,name']
            ]
        ];
    }
}