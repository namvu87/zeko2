<?php
namespace App\GraphQL\Inputs;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class IngredientGoodInput extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name'  => 'IngredientGoodInput',
        'description' => 'Các thành phần của hàng hoá'
    ];

    public function fields() :array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'code' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => Type::string()
            ],
            'unit' => [
                'type'  => Type::string()
            ],
            'price_origin' => [
                'type'  => Type::int()
            ],
            'count' => [
                'type'  => Type::float()
            ]
        ];
    }
}