<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ServiceFee extends Model
{
    protected $collection = 'service_fees';
    const UNPAID_STATUS = 0;
    const INPROGRESS_STATUS = 1;
    const PAID_STATUS = 2;

    protected $fillable = [
        'group_id', 'aggregated_month', 'status',
        'invoices_count', 'revenue', 'users_count',
        'amount', 'package', 'transaction_id'
    ];

    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Models\ServiceFeeTransaction', 'transaction_id');
    }
}
