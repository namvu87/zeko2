<?php

namespace App\Models;

use DesignMyNight\Mongodb\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Auth\MustVerifyEmail as MustVerifyEmailTrait;
use Illuminate\Notifications\Notifiable;
use App\Notifications\VerifyEmail;
use Maklad\Permission\Traits\HasRoles;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements MustVerifyEmail
{
    use MustVerifyEmailTrait, Notifiable, HasRoles;

    const ACTIVE_STATUS = 1;
    const STOP_STATUS = 0;
    const PENDING_STATUS = 2;
    const MALE = 1;
    const FEMALE = 2;
    const OTHER = 3;
    const TIME_EXPIRES_CODE = 30; // life time reset code
    const TIME_RETRIES = 60; // min time for waiting over retries reset
    const RETRY_RESET = 3;

    protected $collection = 'users';

    protected $guard_name = 'api';

    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone_number', 'password', 'gender', 'birthday',
        'avatars', 'cover_image', 'firebase_tokens', 'expired_at', 'counter', 'reset_code',
        'status', 'shift_ids', 'invoice_ids', 'group_ids', 'uuid_devices',
        'area', 'commune', 'address', 'notify_count',
        'email_verified_at', 'social', 'request_group_ids',
        'phone_number_verify', 'phone_number_verified_at'
    ];

    protected $dates = ['created_at', 'updated_at', 'expired_at'];

    protected $hidden = ['password', 'remember_token'];

    protected $appends = ['fullname'];

    /**
     * Relationship user - timekeepings
     *
     * @return Jenssegers\Mongodb\Relations\HasMany
     */
    public function timeKeepings()
    {
        return $this->hasMany('App\Models\Timekeeping');
    }

    /**
     * get user instance from client data
     *
     * @param  string $identifier
     *
     * @return App\Models\User
     */
    public function findForPassport($identifier)
    {
        return $this->where('phone_number', $identifier)
            ->orWhere('email', $identifier)
            ->first();
    }

    /**
     * Relationship user-owner-group
     *
     * @return Jenssegers\Mongodb\Relations\HasMany
     */
    public function ownerGroups()
    {
        return $this->hasMany('App\Models\Group', 'owner_id');
    }

    /**
     * @return array
     */
    public function getShiftIdsAttribute(): array
    {
        return !empty($this->attributes['shift_ids']) ? $this->attributes['shift_ids'] : [];
    }

    /**
     * @return array
     */
    public function getGroupIdsAttribute(): array
    {
        return !empty($this->attributes['group_ids']) ? $this->attributes['group_ids'] : [];
    }

    /**
     *
     * @return string
     */
    public function getFullnameAttribute(): string
    {
        return ($this->attributes['first_name'] ?? '') . ' ' .
            ($this->attributes['last_name'] ?? '');
    }


    /**
     * [Customed] Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Resolve avatars
     *
     * @param  array | null $value
     * @return array
     */
    public function getAvatarsAttribute($value)
    {
        if (is_array($value)) {
            // Kiểm tra xem avatar có phải đường dẫn tuyệt đối hay không
            if (strpos($value['x1'], 'https://') !== false) {
                return $value;;
            }

            $avatars = [];
            foreach ($value as $key => $avatar) {
                $avatars[$key] = config('filesystems.disks.s3.url') . $avatar;
            }
            return $avatars;
        }
        return $value;
    }

    public function getCoverImageAttribute($value)
    {
        if ($value) {
            return config('filesystems.disks.s3.url') . $value;
        }
        return null;
    }

    /**
     * @param  Model $query
     * @param  string $text
     *
     * @return Collection instance
     */
    public function scopeFullText($query, string $text)
    {
        if ($text) {
            $query = $query->whereRaw([
                '$text' => [
                    '$search' => $text
                ]
            ]);
        }
        return $query;
    }
}
