<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * -- type
 * -- label
 * -- contact ['name', 'phone_number', 'zalo', 'skype', 'email']
 */
class Contact extends Eloquent
{
    const CUSTOMER_CARE_TYPE = 1; // Chăm sóc khách hàng
    const CONTACT_INFORMATION_TYPE = 2; // Thông tin liên hệ
    protected $collection = 'contacts';
    protected $fillable = ['type', 'name', 'address', 'phone_number', 'zalo', 'skype', 'fb', 'email', 'fax'];

    public $timestamps = false;
}
