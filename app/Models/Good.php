<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Good extends Model
{
    use SoftDeletes;

    const SYMBOL_CODE = 'P';
    const MAX_FILE_UPLOAD = 5;
    const MAX_SIZE_UPLOAD = 2000;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const GOOD_IMPORTED_TYPE = 1;
    const GOOD_PROCESSED_TYPE = 2;
    const GOOD_TYPES = [1, 2]; //1: Hàng hóa thường; 2: Hàng chế biến
    const GOODS_BOOKED = 1;
    const GOODS_PROCESSING = 2;
    const GOODS_DELIVERED = 3;

    protected $collection = 'goods';
    protected $fillable = [
        'code', 'group_id', 'name', 'group_menu_ids', 'price', 'price_origin',
        'unit', 'properties', 'inventory_number', 'inventory_max',
        'inventory_min', 'description', 'images', 'status', 'parent_id',
        'is_sale', 'type', 'ingredients', 'discount', 'discount_type',
        'conversion_value'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public function groupMenus()
    {
        return $this->belongsToMany('App\Models\GroupMenu', null, 'goods_ids', 'group_menu_ids');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Good', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\Good', 'parent_id');
    }

    public function setIngredientsAttribute($value)
    {
        if ($this->type == self::GOOD_PROCESSED_TYPE && count($value) > 0) {
            $priceOrigin = array_reduce($value, function($price, $item) {
                return $price + $item['count'] * $item['price_origin'];
            });
            $this->attributes['price_origin'] = $priceOrigin;
        }
        $this->attributes['ingredients'] = $value;
    }

    public function scopeName($query, string $name)
    {
        if ($name) $query = $query->where('name', 'like', '%' . $name . '%');
        return $query;
    }

    /**
     * @param  Model $query
     * @param  string $text
     *
     * @return Collection instance
     */
    public function scopeFullText($query, $text)
    {
        if ($text) {
            return $query->whereRaw([
                '$text' => [
                    '$search' => $text
                ]
            ]);
        }
    }

    public function scopeStatus($query, $status)
    {
        if ($status) {
            return $query->where('status', (int)$status);
        }
    }

    public function scopeGroupMenus($query, array $groupMenu)
    {
        if ($groupMenu) {
            return $query->where('group_menu_ids', 'all', $groupMenu);
        }
    }

    public function scopeGroupMenu($query, $groupMenuId)
    {
        if ($groupMenuId) {
            return $query->where('group_menu_ids', 'all', [$groupMenuId]);
        }
    }

    public function scopeInventory($query, $inventory)
    {
        if ($inventory) {
            switch ($inventory) {
                case 1: //Dưới định mức tồn
                    $query = $query->whereRaw([
                        '$expr' => [
                            '$gt' => ['$inventory_min', '$inventory_number']
                        ]
                    ])->whereNotNull('inventory_number');
                    break;
                case 2: //Trên định mức tồn
                    $query = $query->where('inventory_max', '>', 0)->whereRaw([
                        '$expr' => [
                            '$gt' => ['$inventory_number', '$inventory_max']
                        ]
                    ]);
                    break;
                case 3: //Còn hàng
                    $query = $query->where(function($queryNext) {
                        $queryNext->where('inventory_number', '>', 0)->orWhereNull('inventory_number');
                    });
                    break;
                case 4: //Hết hàng
                    $query = $query->where('inventory_number', 0);
                    break;
                default:
                    break;
            }
        }
        return $query;
    }

    public function scopeIsSale($query, $isSale)
    {
        if (!is_null($isSale)) {
            return $query->where('is_sale', (bool)$isSale);
        }
    }

    public function scopeType($query, $type)
    {
        if ($type) {
            return $query->where('type', (int)$type);
        }
    }
}
