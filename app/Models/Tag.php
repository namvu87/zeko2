<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;

    const STATUS_VISIBLE = 1;
    const STATUS_UNVISIBLE = 0;

    protected $collection = 'tags';
    protected $fillable = ['name', 'status', 'usage_count'];
    protected $dates = ['deleted_at'];

    public $timestamps = false;
}
