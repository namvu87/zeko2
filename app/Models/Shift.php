<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Shift extends Model
{
    use SoftDeletes;

    /**
     * Constant type single time (scan 2 time per day)
     */
    const SINGLE_TIME_TYPE = 1;

    /**
     * Constant type multiple time (scan 4 time per day)
     */
    const MULTIPLE_TIME_TYPE = 2;

    /**
     * Constant contain all shift's type
     */
    const TYPES = [1, 2];

    /**
     * Constant contain day of week
     */
    const SCHEDULES = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

    protected $collection = 'shifts';

    protected $fillable = [
        'group_id', 'name', 'schedules', 'type', 'times', 'allow_time'
    ];

    /**
     * @return Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    /**
     * @return Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }
}
