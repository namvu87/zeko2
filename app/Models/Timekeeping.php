<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Timekeeping extends Model
{
    use SoftDeletes;

    protected $collection = 'timekeepings';

    const IS_ADDITION = 1;
    const IS_SWITCH = 2;
    const IS_TAKE_LEAVE = 3;

    /**
     * @var shift_name Tên ca làm
     * @var shift_type Loại ca làm việc
     * @var target_time Thời gian cụ thể ca làm việc
     */
    protected $fillable = [
        'group_id', 'user_id', 'date', 'times', 'type', 'target_time', 'shift_name', 'shift_type', 'shift_allow_time'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Relation ship user - timekeepings
     *
     * @return \Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * Relation ship group - timekeepings
     *
     * @return \Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public function scopePeriodDate($query, $startDate, $endDate)
    {
        if ($startDate) {
            $query = $query->where('date', '>=', $startDate);
        }
        if ($endDate) {
            $query = $query->where('date', '<=', $endDate);
        }
        return $query;
    }

    public function scopeUser($query, $userId)
    {
        if (!empty($userId)) {
            return $query->where('user_id', $userId);
        }
    }

    public function scopeGroup($query, $groupId)
    {
        if (!empty($groupId)) {
            return $query->where('group_id', $groupId);
        }
    }
}
