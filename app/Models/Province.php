<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Province extends Model
{
    protected $collection = 'provinces';

    protected $fillable = ['name', 'location', 'districts'];

    public $timestamps = false;
}
