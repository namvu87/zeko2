<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class UserGuide extends Model
{
    use SoftDeletes;

    protected $collection = 'user_guides';

    protected $fillable = ['service_type', 'category', 'title', 'hashes', 'content', 'tags', 'sort', 'slug',
        'meta_description', 'meta_title', 'rate'
    ];

    protected $dates = ['deleted_at'];

    public function scopeTitle($query, $title)
    {
        if ($title) $query = $query->where('title', 'like', "%{$title}%");
        return $query;
    }
}
