<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ImportInvoice extends Model
{
    const STATUS_TEMPORARY = 1;
    const STATUS_FINISHED = 2;
    const SYMBOL_CODE = 'IP';

    protected $collection = 'import_invoices';
    protected $fillable = [
        'group_id', 'code', 'status', 'payment_method', 'paid_at', 'note', 'goods', 'recipient_id'
    ];

    protected $dates = ['paid_at'];

    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\Models\User', 'recipient_id');
    }

    public function scopeCreatedAt($query, $startDate, $endDate)
    {
        if (!empty($startDate)) {
            $query = $query->where('created_at', '>=', $startDate);
        }
        if (!empty($endDate)) {
            $query = $query->where('created_at', '<=', $endDate);
        }
        return $query;
    }

    public function scopeStatus($query, $status)
    {
        if (!empty($status)) {
            return $query->where('status', intval($status));
        }
    }
}
