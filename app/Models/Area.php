<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Area extends Model
{
    protected $collection = 'areas';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function communes()
    {
        return $this->hasMany('App\Models\Commune', 'area_id');
    }
}
