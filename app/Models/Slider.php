<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Slider extends Model
{
    const LIMIT = 2;
    const STATUS_VISIBLE  = 1;
    const STATUS_UNVISBLE = 0;

    const TYPE_SHOW_WEB    = 0;
    const TYPE_SHOW_MOBILE = 1;

    const SIZE_1X =  [320, 150];
    const SIZE_2X =  [480, 180];
    const SIZE_3X =  [720, 200];
    const SIZE_4X =  [480, 210];

    const MOBILE_SIZE_X1 = [530, 300];
    const MOBILE_SIZE_X2 = [712, 400];
    const MOBILE_SIZE_X3 = [890, 500];
    const MOBILE_SIZE_X4 = [1068, 600];

    protected $collection = 'sliders';
    protected $fillable = ['title', 'photo_url', 'link', 'type', 'status'];

    public $timestamps = false;
}
