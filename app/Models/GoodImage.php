<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class GoodImage extends Model
{
    protected $collection = 'good_images';

    public $fillable = ['name', 'urls'];

    public function scopeName($query, $name)
    {
        if ($name) $query = $query->where('name', 'like', "%{$name}%");
        return $query;
    }

    public function scopeFullText($query, $text)
    {
        if ($text) $query = $query->whereRaw([
            '$text' => [
                '$search' => $text
            ]
        ]);
        return $query;
    }
}
