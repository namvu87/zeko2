<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Events\NotificationCreated;

class Notification extends Model
{
    const TYPE_10 = 10; // Bạn đã được thêm vào danh sách nhân viên công ty
    const TYPE_11 = 11; // Bạn đã bị xoá khỏi danh sách nhân viên công ty
    const TYPE_12 = 12; // Yêu cầu tham gia công ty của bạn dã được chấn nhận
    const TYPE_13 = 13; // Yêu cầu tham gia công ty của bạn dã bị từ chối
    const TYPE_14 = 14; // Bạn đã được thêm vào ca làm việc
    const TYPE_15 = 15; // Bạn đã bị xoá khỏi danh sách ca làm việc
    const TYPE_16 = 16; // Ca làm việc của bạn đã được cập nhật
    const TYPE_17 = 17; // Yêu cầu chấm công bổ sung được chấp thuận
    const TYPE_18 = 18; // Yêu cầu chấm công bổ sung bị từ chối
    const TYPE_19 = 19; // Yêu cầu nghỉ phép của bạn được chấp thuận
    const TYPE_20 = 20; // Yêu cầu nghỉ phép của bạn bị từ chối
    const TYPE_21 = 21; // Yêu cầu chuyển ca làm việc của bạn đã được chấp nhân
    const TYPE_22 = 22; // Yêu cầu chuyển ca làm việc của bạn đã bị từ chối

    const VIEWED = true;
    const NO_VIEW = false; // default
    const IS_EXEC = true; // Thông báo đã được xử lý

    // Thông báo trong màn hình quản lý bán hàng
    const TYPE_40 = 40; // Đã cập nhật bàn của hóa đơn (hoặc kiểu hóa đơn)
    const TYPE_41 = 41; // Đã cập nhật ghi chú của hóa đơn
    const TYPE_42 = 42; // Đã gộp bàn vào hóa đơn 
    const TYPE_43 = 43; // Đã gộp hóa đơn khác vào hóa đơn hiện tại
    const TYPE_44 = 44; // Đã cập nhật discount của hóa đơn
    const TYPE_45 = 45; // Đã thanh toán

    const TYPE_60 = 60; // Đã gán quyền mới cho thành viên trong group
    const TYPE_61 = 61; // Đã thu hồi quyền cho thành viên trong group

    const NUMBER_SAVE = 50;

    protected $dispatchesEvents = [
        'created' => NotificationCreated::class
    ];

    protected $collection = 'notifications';

    protected $fillable = [
        'creator',          // id, fullname, avatars
        'group',            // id, name
        'invoice',          // id, code
        'shift',            // id, name
        'user_id',
        'user_ids',
        'type',
        'date',
        'role_name',        // Tên vai trò gán cho user
        'view',
        'content',
        'requirement_id',
        'timekeeping_id'
    ];

    public function setViewAttribute($value)
    {
        $this->attributes['view'] = (bool) $value;
    }

    // public function creator()
    // {
    //     return $this->belongsTo('App\Models\User', 'creator_id');
    // }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    // public function group()
    // {
    //     return $this->belongsTo('App\Models\Group', 'group_id');
    // }

    public function requirement()
    {
        return $this->belongsTo('App\Models\Requirement', 'requirement_id');
    }

    public function timekeeping()
    {
        return $this->belongsTo('App\Models\Timekeeping', 'timekeeping_id');
    }

    // public function shift()
    // {
    //     return $this->belongsTo('App\Models\Shift', 'shift_id');
    // }
}
