<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Commune extends Model
{
    protected $collection = 'communes';

    protected $fillable = ['name', 'area_id'];

    public $timestamps = false;

    public function area()
    {
        return $this->belongsTo('App\Models\Area', 'area_id');
    }
}
