<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class GroupMenu extends Model
{
    const LEVER_0 = 0;
    const LEVER_1 = 1;
    const LEVER_2 = 2;

    protected $collection = 'group_menus';

    protected $fillable = ['name', 'description', 'group_id', 'goods_ids', 'parent_id', 'level'];

    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\GroupMenu', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\GroupMenu', 'parent_id');
    }

    public function goods()
    {
        return $this->belongsToMany('App\Models\Good', null, 'group_menu_ids', 'goods_ids');
    }
}
