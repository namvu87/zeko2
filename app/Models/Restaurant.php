<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Restaurant extends Model
{
    protected $collection = 'restaurants';

    protected $fillable = [
        'name', 'address', 'province', 'opening_times',
        'price_range', 'location', 'district', 'cuisines',
        'categories', 'avatar'
    ];

    public $timestamps = false;

    /**
     * @return Jenssegers\Mongodb\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\RestaurantImage', 'restaurant_id');
    }

    /**
     * @param  Model $query
     * @param  string $text
     *
     * @return Collection instance
     */
    public function scopeFullText($query, string $text)
    {
        if ($text) {
            $query = $query->whereRaw([
                '$text' => [
                    '$search' => $text
                ]
            ]);
        }
        return $query;
    }
}
