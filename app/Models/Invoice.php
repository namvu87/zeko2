<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    const STATUS_INITED    = 1;
    const STATUS_PURCHASED = 2;
    const STATUS_DISABLE   = 3;

    const NONE_REQUEST            = 0;
    const CREATE_REQUEST_STATUS   = 1;
    const ADDITION_REQUEST_STATUS = 2;
    const PURCHASE_REQUEST        = 3;

    const GOOD_BOOKED_STATUS     = 'booked';
    const GOOD_PROCESSING_STATUS = 'processing';
    const GOOD_PROCESSED_STATUS  = 'processed';
    const GOOD_DELIVERED_STATUS  = 'delivered';
    const GOODS_RETURNED_STATUS  = 'returned';
    const GOOD_STATUS            = ['booked', 'processing', 'processed', 'delivered'];

    const SYMBOL_CODE = 'HD';

    const DISCOUNT_TYPE_FIXED   = 1;
    const DISCOUNT_TYPE_PERCENT = 2;
    const DISCOUNT_TYPES        = [1, 2];

    const STATUS_GOOD_BOOKED     = 1;
    const STATUS_GOOD_PROCESSING = 2;
    const STATUS_GOOD_DELIVERED  = 3;

    protected $collection = 'invoices';
    protected $fillable = [
        'recipient_id', 'code', 'group_id', 'user_ids', 'table_ids',
        'vat', 'type', 'status', 'goods', 'note', 'conversation',
        'discount', 'discount_type', 'total_discount',
        'total', 'total_origin', 'total_returned',
        'payment_method', 'paid_at', 'request_status', 'notifications'
    ];

    protected $dates = ['paid_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\Models\User', 'recipient_id');
    }

    public function returnedInvoices()
    {
        return $this->hasMany('App\Models\ReturnedInvoice');
    }

    /**
     * Get the tables that the invoices belongs to
     */
    public function tables()
    {
        return $this->hasMany('App\Models\Table', 'invoice_id');
    }

    public function scopeCreatedAt($query, $startDate, $endDate)
    {
        if (!empty($startDate)) {
            $query = $query->where('created_at', '>=', $startDate);
        }
        if (!empty($endDate)) {
            $query = $query->where('created_at', '<=', $endDate);
        }
        return $query;
    }

    /**
     * Scope a query to only include invoices of a given groupId
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null                           $groupId
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGroupId($query, $groupId)
    {
        if (!empty($groupId)) {
            return $query->where('group_id', $groupId);
        }
    }

    public function scopeStatus($query, $status)
    {
        if (!empty($status)) {
            return $query->where('status', intval($status));
        }
    }

    public function scopeCode($query, $code)
    {
        if (!empty($code)) {
            return $query->where('code', $code);
        }
    }

    /**
     * Gets the subtotal attribute.
     *
     * @return integer
     */
    // public function getSubtotalAttribute()
    // {
    //     if (empty($this->total)) {
    //         return 0;
    //     }
    //     $discount = $this->discount ?? 0;
    //     $discountPrice = 0;
    //     if ($this->discount_type === self::DISCOUNT_TYPE_FIXED) {
    //         $discountPrice = $discount;
    //     } else {
    //         $discountPrice = $this->total / (1 - ($discount / 100));
    //     }
    //     return $this->total + $discountPrice;
    // }
}
