<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class InvoiceForm extends Model
{
    /**
     * Constant type sale invoice
     */
    const TYPE_SALE_INVOICE = 1;

    /**
     * Constant type import invoice
     */
    const TYPE_IMPORT_INVOICE = 2;

    /**
     * Constant contain all invoice form's type
     */
    const TYPES = [1, 2, 3];

    /**
     * Constant contain all invoice form's size
     */
    const SIZES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    protected $collection = 'invoice_forms';
    protected $fillable =  ['group_id', 'name', 'type', 'content', 'user_created_id', 'size'];

    protected $dates = ['created_at', 'updated_at'];
}
