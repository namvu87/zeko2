<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ServiceFeeTransaction extends Model
{
    const INITED_STATUS = 0;
    const PAID_STATUS = 1;
    const CANCELED_STATUS = 2;

    protected $collection = 'service_fee_transactions';

    protected $fillable = [
        'status', 'service_fees', 'amount', 'user_id', 'payment_info'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
