<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Events\GroupCreated;
use App\Models\User;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Group extends Model
{
    //    use SoftDeletes;
    const ADVERTISIMENT_SERVICE = 0;
    const MASTER_SERVICE        = -1;
    const TIMEKEEPING_SERVICE   = 1;
    const RESTAURANT_SERVICE    = 2;
    const SERVICES              = [0, 1, 2];
    const NOT_CARE = 1;
    const CARING = 2;
    const CARED = 3;

    protected $collection = 'groups';

    protected $fillable = [
        'name', 'address', 'commune', 'area', 'owner_id', 'service_type',
        'cover_image', 'expired_at', 'goods_properties', 'goods_number',
        'invoice_number', 'goods_units', 'number_user', 'number_requirement',
        'acting_user_ids', 'is_active', 'care_status'
    ];

    protected $appends = ['number_user'];

    protected $dispatchesEvents = [
        'created' => GroupCreated::class
    ];

    /**
     * Relation ship group - user (owner's group)
     *
     * @return   \Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'owner_id');
    }

    /**
     * Realtion shift group - shifts
     *
     * @return \Jenssegers\Mongodb\Relations\HasMany
     */
    public function shifts()
    {
        return $this->hasMany('App\Models\Shift', 'group_id');
    }

    public function goods()
    {
        return $this->hasMany('App\Models\Good', 'group_id');
    }

    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice', 'group_id');
    }

    public function timekeepings()
    {
        return $this->hasMany('App\Models\Timekeeping', 'group_id');
    }

    public function getNumberUserAttribute()
    {
        return User::where('group_ids', 'all', [$this->_id])->count();
    }

    public function getCoverImageAttribute($value)
    {
        if ($value) {
            return config('filesystems.disks.s3.url') . $value;
        }
        return null;
    }

    public function setIsActiveAttribute($value)
    {
        $this->attributes['is_active'] = (bool) $value;
    }

    /**
     * @param  Model  $query
     * @param  string $text
     *
     * @return Collection instance
     */
    public function scopeFullText($query, string $text)
    {
        if ($text) {
            $query = $query->whereRaw([
                '$text' => [
                    '$search' => $text
                ]
            ]);
        }
        return $query;
    }

    public function scopeActive($query, $status)
    {
        if (!empty($status)) {
            return $query->where('is_active', (bool) $status);
        }
    }

    public function scopeCare($query, $care)
    {
        if ($care) {
            return $query->where('care_status.status', $care);
        }
    }
}
