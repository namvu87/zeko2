<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class RestaurantImage extends Model
{
    protected $collection = 'restaurant_images';

    protected $fillable = ['thumb', 'origin', 'restaurant_id'];

    public $timestamps = false;

    /**
     * @return Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant', 'restaurant_id');
    }
}
