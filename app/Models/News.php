<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    const STATUS_VISIBLE = 1;
    const STATUS_UNVISBLE = 0;
    const SIZE_1X = [320, 150];
    const SIZE_2X = [480, 180];
    const TYPE_1 = 1; // Bài viết thường
    const TYPE_2 = 2; // Giối thiệu về công ty
    const TYPE_3 = 3; // Trang chủ
    const TYPE_4 = 4; // Điều khoản sử dụng
    const TYPE_5 = 5; // Tuyển dụng
    const TYPE_6 = 6; // Chính sách công ty
    const TYPE_7 = 7; // Về công ty
    const TYPE_8 = 8; // Điều khoản
    const TYPE_9 = 9; // Phí dịch vụ

    protected $collection = 'news';
    protected $fillable = ['title', 'description', 'image', 'content', 'status', 'tags', 'sort', 'slug', 'type'];
    protected $dates = ['deleted_at'];
}
