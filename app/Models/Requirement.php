<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Requirement extends Model
{
    const ADDITION_TIMEKEEPING_TYPE = 1;
    const SWITCH_SHIFT_TYPE = 2;
    const TAKE_LEAVE_TYPE = 3;
    const JOIN_GROUP_TYPE = 4;

    protected $collection = 'requirements';

    protected $fillable = [
        'creator_id', 'group_id', 'type', 'timekeeping_id', 'shift_id',
        'date', 'content', 'res_content', 'is_accept'
    ];

    /**
     * @return Jenssegers\Mongodb\Relations\BelongsTo
     */

    public function setViewAttribute($value)
    {
        $this->attributes['view'] = (bool)$value;
    }

    public function setIsExecAttribute($value)
    {
        $this->attributes['is_exec'] = (bool)$value;
    }

    /**
     * @return Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'creator_id');
    }

    /**
     * @return Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    /**
     * @return Jenssegers\Mongodb\Relations\hasOne
     */
    public function notification()
    {
        return $this->hasOne('App\Models\Notification', 'requirement_id');
    }

    public function shift()
    {
        return $this->belongsTo('App\Models\Shift', 'shift_id');
    }

    public function timekeeping()
    {
        return $this->belongsTo('App\Models\Timekeeping', 'timekeeping_id');
    }
}
