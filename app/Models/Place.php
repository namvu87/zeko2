<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Place extends Model
{
    use SoftDeletes;

    protected $collection = 'places';

    protected $fillable = ['name', 'description', 'group_id'];

    public function tables()
    {
        return $this->hasMany('App\Models\Table', 'place_id');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public function scopeIds($query, $ids)
    {
        if (!empty($ids)) {
            return $query->whereIn('_id', $ids);
        }
    }
}
