<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Table extends Model
{
    use SoftDeletes;

    const STATUS_INACTIVE = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_ACTING = 2;
    const STATUS_BOOKING = 3;
    const STATUS = [0, 1, 2, 3];

    protected $collection = 'tables';

    protected $fillable = [
        'name', 'group_id', 'sort', 'status', 'place_id', 'count_seat', 'description', 'invoice_id'
    ];

    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place', 'place_id');
    }

    /**
     * Get the invoices that the tables belongs to
     */
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'invoice_id');
    }

    public function scopeName($query, $name)
    {
        if (!empty($name)) {
            return $query->where('name', 'like', '%' . $name . '%');
        }
    }

    public function scopePlace($query, $placeId)
    {
        if (!empty($placeId)) {
            return $query->where('place_id', $placeId);
        }
    }

    public function scopeStatus($query, $status)
    {
        if (is_numeric($status)) {
            return $query->where('status', intval($status));
        }
    }

    public function scopeStatusNotInactiveType($query)
    {
        return $query->where('status', '<>', self::STATUS_INACTIVE);
    }

    /**
     * @param  Model $query
     * @param  string $text
     * @return Collection instance
     */
    public function scopeFullText($query, $text)
    {
        if (!empty($text)) {
            return $query->whereRaw([
                    '$text' => [
                        '$search' => $text
                    ]
                ]);
        }
    }
}
