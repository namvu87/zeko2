<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ReturnedInvoice extends Model
{
    const STATUS_PAID = 1;
    const STATUS_DISABLE = 2;

    const SYMBOL_CODE = 'TH';

    protected $table = 'returned_invoices';

    protected $fillable = [
        'group_id', 'code', 'invoice_id', 'status', 'total', 'creator_id', 'note', 'goods', 'creator'
    ];

    protected $dates = ['created_at'];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'creator_id');
    }

    public function scopeGroupId($query, $groupId)
    {
        if (!empty($groupId)) {
            return $query->where('group_id', $groupId);
        }
    }

    public function scopeCreatedAt($query, $startDate, $endDate)
    {
        if (!empty($startDate)) {
            $query = $query->where('created_at', '>=', $startDate);
        }
        if (!empty($endDate)) {
            $query = $query->where('created_at', '<=', $endDate);
        }
        return $query;
    }

    public function scopeStatus($query, $status)
    {
        if (!empty($status)) {
            $query = $query->where('status', intval($status));
        }
        return $query;
    }

    public function scopeCode($query, $code)
    {
        if (!empty($code)) {
            $query->where('code', $code);
        }
        return $query;
    }
}
