<?php

namespace App\Interfaces;

interface Notifiable
{
    /**
     * Tạo từ phía người dùng
     */
    const FROM_USER_TYPE = 1;

    /**
     * Tạo từ phía nhân viên bán hàng
     */
    const FROM_EMPLOYEE_TYPE = 2;

    /**
     * Tạo từ phía nhà bếp
     */
    const FROM_KITCHEN = 3;

    /**
     * Tạo từ phía hệ thống
     */
    const FROM_SYSTEM = 4;

    const TYPE_10 = 10; // Được thêm vào nhóm
    const TYPE_11 = 11; // Bị xoá khỏi nhóm
    const TYPE_12 = 12; // Yêu cầu tham gia nhóm được chấp nhận
    const TYPE_13 = 13; // Yêu cầu thanh gia nhóm bị từ chối
    const TYPE_14 = 14; // Được thêm vào ca làm việc
    const TYPE_15 = 15; // Bị xoá khỏi ca làm việc
    const TYPE_16 = 16; // Ca làm việc được cập nhật
    const TYPE_17 = 17; // Yêu cầu chấm công bổ sung được chấp nhận
    const TYPE_18 = 18; // Yêu cầu chấm công bổ sung bị từ chối
    const TYPE_19 = 19; // Yêu cầu xin nghỉ phép được chấp nhận
    const TYPE_20 = 20; // Yêu cầu xin nghỉ phép bị từ chối
    const TYPE_21 = 21; // Yêu cầu chuyển ca được chấp nhận
    const TYPE_22 = 22; // Yêu cầu chuyển ca bị từ chối

    /**
     * Const notification BÁN HÀNG (màn hinh POS)
     */
    const MANAGE_SALE_TYPES = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53];
    const TYPE_40 = 40; // Hoá đơn được khởi tạo
    const TYPE_41 = 41; // Yêu cầu thanh toán
    const TYPE_42 = 42; // Yêu cầu thêm món
    const TYPE_43 = 43; // Thay đổi trạng thái món
    const TYPE_44 = 44; // Thay đổi số lượng món (kiểm kê)
    const TYPE_45 = 45; // Thay đổi danh sách bàn
    const TYPE_46 = 46; // Gộp bàn
    const TYPE_47 = 47; // Gộp đơn
    const TYPE_48 = 48; // Cập nhật ghi chú
    const TYPE_49 = 49; // Huỷ món
    const TYPE_50 = 50; // Cập nhật giảm giá
    const TYPE_51 = 51; // Thanh toán
    const TYPE_52 = 52; // Huỷ đơn
    const TYPE_53 = 53; // Xác nhận yêu cầu hoá đơn
    const TYPE_54 = 54; // Hoá đơn đã thêm thành viên tham gia
    const TYPE_55 = 55; // Đã chế biến xong

    /**
     * Permission, Roles
     */
    const TYPE_70 = 70; // Được gán vai trò trong nhóm
    const TYPE_71 = 71; // Bị thu hồi vai trò trong nhóm

    /**
     * Thông báo hệ thống
     */
    const TYPE_101 = 101; // Thông báo thanh toán phí dịch vụ
    const TYPE_102 = 102; // Thông báo dừng hoạt động nhóm
    const TYPE_103 = 103; // Đã thanh toán phí dịch vụ thành công
    const TYPE_104 = 104; // Thanh toán thất bại
}
