<?php

namespace App\Traits\Payment;

use Rebing\GraphQL\Error\AuthorizationError;
use App\Models\Group;
use App\Models\ServiceFee;

trait ServiceFeeHelper
{
    /**
     * Cập nhật trạng thái đang thanh toán cho serviceFee
     *
     * @param \App\Models\ServiceFeeTransaction $transaction
     * @return void
     */
    private function updateServiceFeeState($transaction)
    {
        foreach ($transaction->service_fees as $item) {
            $this->fee->update($item['service_fee_id'], [
                'status' => $this->fee->getInprogressState(),
                'transaction_id' => $transaction->id
            ]);
        }
    }

    /**
     * Xác định giá trị cần thanh toán
     *
     * @param array $serviceFees
     * @return int
     */
    private function getAmount(array $serviceFees): int
    {
        $amount = 0;

        foreach ($serviceFees as $item) {
            $serviceFee = $this->fee->getById($item['service_fee_id']);
            if ($serviceFee->status !== ServiceFee::UNPAID_STATUS) {
                throw new AuthorizationError($serviceFee->group->name . ' đã thanh toán hoặc đang trong tiến trình thanh toán');
            }
            if ($serviceFee->group->service_type === Group::TIMEKEEPING_SERVICE) {
                $amount += config('payment_package.timekeeping.fee');
                continue;
            }
            $amount += $this->calculateRestaurantServiceFee(
                $item['package'] ?? 1,
                $serviceFee
            );
        }
        return (int) $amount;
    }

    /**
     * Tính amount cho nhóm nhà hàng
     *
     * @param integer $package
     * @param ServiceFee $serviceFee
     * @return integer
     */
    private function calculateRestaurantServiceFee(int $package, ServiceFee $serviceFee): int
    {
        $amount = 0;
        switch ($package) {
            case 1:
                $amount = $serviceFee['invoices_count'] * $this->getValuePackge($package);
                break;
            case 2:
                $amount = $serviceFee['revenue'] * $this->getValuePackge($package);
                break;
            case 3:
                $amount = $this->getValuePackge($package);
                break;
            default:
                break;
        }
        return (int) $amount;
    }

    private function getValuePackge($package): float
    {
        return config("payment_package.restaurant.{$package}.fee");
    }
}
