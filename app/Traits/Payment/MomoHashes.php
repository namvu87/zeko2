<?php

namespace App\Traits\Payment;

use phpseclib\Crypt\RSA;

trait MomoHashes
{
    /**
     * Tạo chữ ký điện tử từ dữ liệu đầu vào và secret key MOMO
     *
     * @param array $data
     * @return string
     */
    private function generateSignature(array $data): string
    {
        $strData = '';
        foreach ($data as $key => $value) {
            $strData .= $key . '=' . $value . '&';
        }
        $strData = substr($strData, 0, -1);

        return hash_hmac('sha256', $strData, config('payment.momo.secret_key'));
    }

    /**
     * Encrypt RSA PKCS #8
     *
     * @param array $rawData
     * @return string
     */
    private function encryptRSA(array $rawData): string
    {
        $rawJson = json_encode($rawData, JSON_UNESCAPED_UNICODE);

        $rsa = new RSA();
        $rsa->loadKey(config('payment.momo.app_public_key'));
        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
        $result = $rsa->encrypt($rawJson);

        return base64_encode($result);
    }
}
