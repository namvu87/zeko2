<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait EsmsHelper
{
    protected function sendSms(string $phone, string $code)
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);

        return $client->request('POST', config('esms.url'), [
            'form_params' => [
                'ApiKey' => config('esms.api_key'),
                'SecretKey' => config('esms.secret_key'),
                'SmsType' => 2,
                // 'Content' => __('esms.content', ['code' => $code]),
                'Content' => $code,
                'Brandname' => 'Verify',
                'Phone' => $phone
            ]
        ]);
    }
}
