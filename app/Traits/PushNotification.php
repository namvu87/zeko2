<?php

namespace App\Traits;

trait PushNotification
{
    public function createDeviceGroup($notificationKeyName, $registrationIds)
    {
        $url = config('firebase.fcm_notification_url');
        $headers = [
            'Content-Type:application/json',
            'Authorization:key=' . config('firebase.legacy_server_key'),
            'project_id:290804472597'
        ];
        $payload = [
            'operation' => 'create',
            'notification_key_name' => $notificationKeyName,
            'registration_ids' => $registrationIds
        ];
        return $this->httpPostRequest($url, $headers, $payload);
    }

    public function sendSingleDevices($tokenDevice, $data, $title = '', $body = '')
    {
        $url = config('firebase.fcm_url');
        $headers = [
            'Content-Type:application/json',
            'Authorization:key=' . config('firebase.legacy_server_key')
        ];
        $payload = [
            'registration_ids' => $tokenDevice,
            'notification' => [
                'title' => $title,
                'body' => $body,
                'icon' => '/images/restaurant-icon.png'
            ],
            'data' => [
                'payload' => $data
            ]
        ];
        return $this->httpPostRequest($url, $headers, $payload);
    }

    public function sendDeviceGroup($notificationKey, $title, $body, $type, $data)
    {
        $url = config('firebase.fcm_url');
        $headers = [
            'Content-Type:application/json',
            'Authorization:key=' . config('firebase.legacy_server_key')
        ];
        $payload = [
            'to' => $notificationKey,
            'notification' => [
                'title' => $title,
                'body' => $body,
                'icon' => '/images/restaurant-icon.png'
            ],
            'data' => [
                'type' => $type,
                'payload' => $data
            ]
        ];
        return $this->httpPostRequest($url, $headers, $payload);
    }

    public function httpPostRequest($url, $headers, $payload = [], $data = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            \Log::error('Error push notification : ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
}