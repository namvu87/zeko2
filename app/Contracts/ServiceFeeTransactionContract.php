<?php

namespace App\Contracts;

use App\Contracts\BaseContract;

interface ServiceFeeTransactionContract extends BaseContract
{
    public function getInitedState(): int;

    public function getPaidState(): int;

    public function getCanceledState(): int;
}
