<?php

namespace App\Contracts\Restaurant;

use App\Contracts\BaseContract;

interface PlaceContract extends BaseContract
{
    /**
     * @param  array  $data
     * @return App\Models\Place | boolean
     */
    public function createPlace(array $data);

    /**
     * Get Places in group by groupdId
     * @param  string $groupId
     * @param  string $name
     * @return array
     */
    public function getByName(string $groupId, string $name);

    /**
     * Gets the places with tables.
     *
     * @param      string  $groupId   The group identifier
     * @param      array   $placeIds  The place identifiers
     */
    public function getPlacesWithCondition(string $groupId, array $placeIds);

    /**
     * Lấy danh sách khu vực trong màn hình bán hàng
     *
     * @param string $groupId
     * @param array  $placeIds
     * @param array  $relations
     * @return list of App\Models\Place
     */
    public function getSalePlaces(string $groupId, array $placeIds = [], array $relations = []);
}