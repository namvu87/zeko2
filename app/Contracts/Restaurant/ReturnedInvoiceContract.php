<?php

namespace App\Contracts\Restaurant;

use Carbon\Carbon;
use App\Contracts\BaseContract;

interface ReturnedInvoiceContract extends BaseContract
{
    public function getStatusPaid();

    public function getStatusDisable();

    /**
     * Lấy danh sách hóa đơn theo điều kiện
     *
     * @return App\Models\Invoice
     */
    public function filterByCondition(array $data);

    /**
     * Tạo ra mã code
     * Code is unique in group
     *
     * @return string
     */
    public function generateCode(string $groupId) :string;

    /**
     * Lấy danh sách hoá đơn trả hàng theo ID hoá đơn gốc
     *
     * @param string $invoiceId
     * @return void
     */
    public function getByInvoiceId(string $invoiceId);

    /**
     * Lấy số lượng hoá đơn theo khoảng ngày
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return integer
     */
    public function getCountByCondition(string $groupId, Carbon $startDate, Carbon $endDate): int;
}