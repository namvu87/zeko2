<?php

namespace App\Contracts\Restaurant;

use App\Contracts\BaseContract;
use Carbon\Carbon;

interface InvoiceContract extends BaseContract
{
    /**
     * Lấy trạng thái hóa đơn chưa thanh toán
     *
     * @return int
     */
    public function getStatusInited() :int;

    /**
     * Lấy trạng thái hóa đơn đã thanh toán
     *
     * @return int
     */
    public function getStatusPurchased() :int;

    /**
     * Lấy trạng thái hóa đơn lỗi bị hủy bỏ
     *
     * @return int
     */
    public function getStatusDisable() :int;

    /**
     * Lấy trạng thái món đã đặt
     *
     * @return string
     */
    public function getGoodBookedStatus() :string;

    /**
     * Lấy trạng thái món đang chế biến
     *
     * @return string
     */
    public function getGoodProcessingStatus() :string;

    /**
     * Lấy trạng thái món đã chế biến
     *
     * @return string
     */
    public function getGoodProcessedStatus() :string;

    /**
     * Lấy trạng thái món đã giao hàng
     *
     * @return string
     */
    public function getDeliveredStatus() :string;

    /**
     * Lấy trạng thái món đã trả lại
     *
     * @return string
     */
    public function getReturnedGoodsStatus(): string;

    /**
     * Lấy trạng thái không có yêu cầu
     *
     * @return int
     */
    public function getNoneRequestStatus(): int;

    /**
     * Lấy trạng thái yêu cầu tạo hoá đơn
     *
     * @return int
     */
    public function getCreateInvoiceRequestStatus() :int;

    /**
     * Lấy trạng thái yêu cầu thêm món
     *
     * @return int
     */
    public function getAdditionFoodRequestStatus() :int;

    /**
     * Lấy trạng thái yêu cầu thanh toán
     *
     * @return int
     */
    public function getPurchaseRequestStatus() :int;

    /**
     * Lấy loại discount theo phần trăm
     *
     * @return integer
     */
    public function getDiscountTypePercent() :int;

    /**
     * Lấy loại discount theo giá
     *
     * @return integer
     */
    public function getDiscountTypeFixed() :int;

    /**
     * Lấy tất cả các strạng thái của món ăn
     *
     * @return int
     */
    public function getGoodStatus() :array;

    /**
     * Lấy ra trạng thái không có request nào
     *
     * @return     integer  The status none request.
     */
    public function getStatusNoneRequest() :int;

    /**
     * Lấy ra trạng thái có request tạo hóa đơn nào
     *
     * @return     integer  The status create request.
     */
    public function getStatusCreateRequest() :int;

    /**
     * Lấy ra trạng thái có request thêm món ăn
     *
     * @return     integer  The status addition request.
     */
    public function getStatusAdditionRequest() :int;

    /**
     * Lấy ra trạng thái có request thanh toán
     *
     * @return     integer  The status purchase request.
     */
    public function getStatusPurchaseRequest() :int;

    /**
     * Mảng chứa các loại discount
     *
     * @return     array  The discount types.
     */
    public function getDiscountTypes() :array;

    /**
     * generate new invocie code
     * Code is unique in group
     *
     * @return string
     */
    public function generateCode(string $groupId) :string;

    /**
     * Lấy danh sách hóa đơn theo điều kiện
     *
     * @return App\Models\Invoice
     */
    public function getInvoiceByCondition(array $data);

    /**
     * Phân trang kết quả truy vấn theo điều kiện với groupId
     *
     * @param array $args
     * @param integer $page
     * @param array $relations
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateByCondition(array $args, $page = 1, array $relations = []);

    /**
     * Lấy danh sách hóa đơn theo trạng thái và phân trang kết quả
     *
     * @param      string   $groupId  The group identifier
     * @param      integer  $status   The status
     * @param      integer  $perPage  The per page
     * @param      integer  $page     The page
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function paginateByStatus(string $groupId, int $status, int $perPage, int $page);

    /**
     * Tìm kiếm danh sách hóa đơn với điều kiện đầu vào
     *
     * @param      array    $condition  The condition
     * @param      integer  $perPage    The per page
     * @param      integer  $page       The page
     */
    public function paginateSearchByCondition(array $condition, int $perPage, int $page);

    /**
     * Lấy thông tin hóa đơn trang chủ hôm nay
     *
     * @params string $groupId;
     * @return App\Models\Invoice
     */
    public function getStatisticToday(string $groupId);

    /**
     * Lấy danh sách hóa đơn trong khoang thoi gian
     *
     * @params string $groupId;
     * @params object $period;
     * @return App\Models\Invoice
     */
    public function getStatisticPeriodTime(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * get invoices by hour today
     *
     * @params array $data;
     * @return App\Models\Invoice
     */
    public function getReportSale(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * get invoices by hour today
     *
     * @params array $data;
     * @return App\Models\Invoice
     */
    public function getReportGoods(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * Gets all invoices.
     *
     * @param      string   $groupId  The group identifier
     * @param      integer  $amount   The amount
     */
    public function getListLatestInvoices(string $groupId, int $amount);

    /**
     * Gets the invoices by status.
     *
     * @param array $args The args
     */
    public function getInvoicesByStatus(array $args);

    /**
     * Checkout order
     *
     * @param array $data The data
     */
    public function getInvoicesByPeriodTime(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * Cập nhật lại số lượng hàng hóa trong hóa đơn
     *
     * @param string $invoiceId
     * @param array  $goods
     * @param bool   $restore
     */
    public function updateNumberGoodsInvoice(string $invoiceId, array $goods, bool $restore);

    /**
     * Lấy hoá đơn mang về 
     *
     * @param string $groupId
     * @return array
     */
    public function getInvoicesIsEmptyTable(string $groupId);

    /**
     * Lấy ra danh sách các hóa đơn chưa thanh toán chứa một trong số những tableIds
     *
     * @param string $groupId
     * @param array  $tableIds
     */
    public function getInitedInvoicesByTableIds(string $groupId, array $tableIds);

    /**
     * Lấy ra danh sách hóa đơn có goods chứa $goodId
     *
     * @param      string  $groupId  The group identifier
     * @param      string  $goodId   The good identifier
     */
    public function getInitedInvoicesHasGoodId(string $groupId, string $goodId);

    /**
     * Cập nhật goods_order của hóa đơn
     *
     * @param      string  $invoiceId   The invoice identifier
     * @param      string  $goodId      The good identifier
     * @param      array   $goodsOrder  The goods order
     */
    public function updateGoodsOrderInvoice(string $invoiceId, string $goodId, array $goodsOrder);

    /**
     * Get list of invoice by user's ID
     *
     * @param  string $userId
     * @param  array  $args
     * @param  array  $relations
     * @param  array  $selections
     * @return App\Models\Invoices
     */
    public function paginateByUserId(string $userId, array $args, array $relations = []);

    /**
     * Tổng hợp dữ liệu theo toán tử điều kiện truyền vào
     *
     * @param array $aggregation
     * @return Object
     */
    public function aggregate(array $aggregation);

    /**
     * Xoá 1 item trong mảng goods
     *
     * @param string  $invoiceId
     * @param string  $goodsId
     * @return void
     */
    public function removeGoodsItem(string $invoiceId, string $goodsId);

    /**
     * Xoá 1 item trong goods.orders
     *
     * @param string $invoiceId
     * @param string $goodsId
     * @param string $createdAt  Thời gian khởi tạo của orders item (goods.$.orders.$.created_at)
     * @param int    $count      Số lượng của orders item (goods.$.orders.$.count)
     * @return void
     */
    public function removeGoodsOrdersItem(string $invoiceId, string $goodsId,
                                          string $createdAt, int $count);

    /**
     * Cập nhật trạng thái món theo ID hoá đơn
     * (màn hình nhà bếp)
     *
     * @param string $invoiceId
     * @return void
     */
    public function updateProcessedInvoice(string $invoiceId);

    /**
     * Cập nhật trạng thái món theo goodsId, created_at
     *
     * @param string $invoiceId
     * @param string $goodsId
     * @param string $createdAt
     * @param string $targetStatus
     * @return void
     */
    public function updateProcessedOrderItem(string $invoiceId,
                                             string $goodsId,
                                             string $createdAt);

    /**
     * Cập nhật trạng thái tất cả các hoá đơn có chưa goodsId
     *
     * @param string $groupId
     * @param string $goodsId
     * @return void
     */
    public function updateProcessedByGoodsId(string $groupId, string $goodsId);

    /**
     * Lấy danh sách hóa đơn chưa thanh toán có ít nhất một món đúng trạng thái
     *
     * @param string $groupId
     * @param string $status
     *
     * @return mixed
     */
    public function getInvoiceByGoodsStatus(string $groupId, string $goodsId, string $status);
}