<?php

namespace App\Contracts\Restaurant;

use App\Contracts\BaseContract;
use Carbon\Carbon;

interface ReportContract
{
    /**
     * Lấy số lượng hoá đơn trong ngày của 1 nhóm, theo trạng thái hoá đơn
     *
     * @param array $args
     * @return void
     */
    public function getCountByStatus(string $groupId, Carbon $startDate, Carbon $endDate, int $status);

    /**
     * Lấy tổng doanh thu của group theo khoảng ngày
     * Đồng thời lấy số lượng hoá đơn đã thanh toán
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getTotalRevenueByDateRange(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * Báo cáo bán hàng (doanh số) theo khoảng thời gian
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @param array $groupIdOperator
     * @param array $sort
     * @return array
     */
    public function getRevenueReport(
        string $groupId, Carbon $startDate, Carbon $endDate, array $groupIdOperator, array $sort
    );

    /**
     * dữ liệu thống kê hóa đơn trong ngày
     *
     * @param string $groupId
     *
     * @return mixed
     */
    public function getStatisticToday(string $groupId);

    /**
     * Dữ liệu thống kê hàng hóa theo hóa đơn trong khoảng thời gian
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function statisticGoods(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * Dữ liệu thống kê doanh thu trong khoảng thời gian và sắp xếp giảm dần theo doanh thu
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function statisticRevenue(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * Dữ liệu báo cáo bán hàng
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function getReportSale(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * Dữ liệu báo cáo hàng hóa
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function getReportGoods(string $groupId, Carbon $startDate, Carbon $endDate);

    /**
     * Dữ liệu báo cáo nhập hàng
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function getReportGoodsImported(string $groupId, Carbon $startDate, Carbon $endDate);
}
