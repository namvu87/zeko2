<?php

namespace App\Contracts\Restaurant;

use App\Contracts\BaseContract;
use Carbon\Carbon;

interface ImportInvoiceContract extends BaseContract
{
    /**
     * Lấy trạng thái hóa đơn tạm thời
     *
     * @return int
     */
    public function getStatusTemporary();

    /**
     * Lấy trạng thái hóa đơn đã thanh toán
     *
     * @return int
     */
    public function getStatusFinished();


    /**
     * store new import invoice
     *
     * @return App\Models\Invoice
     */
    public function store(array $data);

    /**
     * get list import invoice by condition
     *
     * @return App\Models\Invoice
     */
    public function getImportInvoiceByCondition(array $data);

    /**
     * get data report
     *
     * @return array
     */
    public function getReportGoods(string $groupId, Carbon $startDate, Carbon $endDate);
}