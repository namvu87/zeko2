<?php

namespace App\Contracts\Restaurant;

use App\Contracts\BaseContract;
use Illuminate\Http\UploadedFile;

interface GoodContract extends BaseContract
{
    /**
     * Lấy trạng thái đang hoạt động
     *
     * @return int
     */
    public function getStatusActive(): int;

    /**
     * Lấy trạng thái ngừng hoạt động
     *
     * @return int
     */
    public function getStatusInactive(): int;

    /**
     * get symbol code
     *
     * @return string
     */
    public function getSymbolCode(): string;

    /**
     * Lấy giá trị loại hàng nhập
     *
     * @return int
     */
    public function getImportType(): int;

    /**
     * Lấy giá trị loại hàng chế biến
     *
     * @return int
     */
    public function getProcessType(): int;

    /**
     * get array goods type
     *
     * @return string
     */
    public function getGoodsType(): array;

    /**
     * get max file upload
     *
     * @return int
     */
    public function getMaxFileUpload(): int;

    /**
     * get max size upload
     *
     * @return int
     */
    public function getMaxSizeUpload(): int;

    /**
     * get goods by code
     *
     * @param  string $groupId
     * @param  string $code
     *
     * @return App\Models\Good
     */
    public function getByCode(string $groupId, string $code);

    /**
     * get normal goods by code
     *
     * @param  string $groupId
     * @param  string $code
     *
     * @return App\Models\Good
     */
    public function getNormalGoodsByCode(string $groupId, string $code);

    /**
     * generate new goods code
     *
     * @param  string $groupId
     *
     * @return string
     */
    public function generateCode(string $groupId): string;

    /**
     * get imported goods in group
     *
     * @param  string $groupId
     * @param  string $name
     *
     * @return App\Models\Good
     */
    public function getImportedGoods(string $groupId, string $name = '');

    /**
     * Gets all goods in group for sale.
     *
     * @param  string $groupId
     *
     * @return App\Models\Good
     */
    public function getGoodsForSale(string $groupId);

    /**
     * Lấy danh sách hàng hoá theo chủng loại
     *
     * @param  array $request
     *
     * @return App\Models\Good
     */
    public function getByCondition(array $request, array $relations = [], int $page = 1);

    /**
     * Danh sách hàng để bán
     *
     * @param  array $request
     * @param  integer $page
     *
     * @return App\Models\Good
     */
    public function getMenu(array $args, int $page = 1);

    /**
     * change status to active
     *
     * @param  string $goodId
     *
     * @return App\Models\Good
     */
    // public function enable(string $goodId);

    /**
     * change status to inactive
     *
     * @param  string $goodId
     *
     * @return App\Models\Good
     */
    // public function disable(string $goodId);

    /**
     * update Good
     *
     * @param  array $goodIds
     * @param  array $menuIds
     *
     * @return App\Models\Good
     */
    public function updateGoodsGroupMenus(array $goodIds, array $menuIds);

    /**
     * search limited goods by name
     *
     * @param  string $groupId
     * @param  string $name
     *
     * @return App\Models\Good
     */
    public function limitGoodsByName(string $groupId, string $name);

    /**
     * Upload image then return realtive path
     *
     * @param array|null $images
     * @param string $groupId
     *
     * @return array
     */
    public function saveImages($images, string $groupId): array;

    /**
     * Upload single image
     *
     * @param  UploadedFile $image
     * @param  string $groupId
     *
     * @return string
     */
    public function saveImage(UploadedFile $image, string $groupId, $suffix = 0): string;

    /**
     * Xoá 1 ảnh của good chỉ định
     *
     * @param  string $imageUrl
     *
     * @return boolean
     */
    public function removeImage(string $imageUrl): bool;

    /**
     * Gán hàng hoá để bán
     *
     * @param  array $goodsIds
     *
     * @return boolean
     */
    public function assignSale(array $goodsIds);

    /**
     * Lấy những hàng hoá có thành phần chỉ định
     *
     * @param  string $goodId
     *
     * @return App\Models\Good
     */
    public function getByIngredientId(string $goodId);

    /**
     * restore number goods after disable invoice
     *
     * @param  string $goodId
     *
     * @return App\Models\Good
     */
    public function restoreNumberFromGoodsInvoice(array $goods);

    /**
     * update sale many goods
     *
     * @param  string $groupId
     * @param  array $goodsIds
     *
     * @return App\Models\Good
     */
    public function updateIsSaleByIds(string $groupId, array $goodsIds);
}


