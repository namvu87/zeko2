<?php

namespace App\Contracts\Restaurant;

use App\Contracts\BaseContract;

interface InvoiceFormContract extends BaseContract
{
    /**
     * Get all types
     * 
     * @return array
     */
    public function getTypes();

    /**
     * Get all sizes
     * 
     * @return array
     */
    public function getSizes();

    /**
     * Get all invoice form.
     *
     * @param array  $data   The data
     */
    public function getAllInvoiceForm(array $data);

    /**
     * Get invoice form by type.
     *
     * @param string $groupId
     * @param string $type
     *
     * @return array
     */
    public function getByType(string $groupId, int $type);


}