<?php

namespace App\Contracts\Restaurant;

use App\Contracts\BaseContract;

interface TableContract extends BaseContract
{
    /**
     * Gets the inactive type.
     *
     * @return     integer  The inactive type.
     */
    public function getInactiveType() :int;

    /**
     * get Type avaliable table
     *
     * @return int
     */
    public function getAvailableType() :int;

    /**
     * get acting type
     *
     * @return int
     */
    public function getActingType() :int;

    /**
     * Lấy danh sách trạng thái bàn
     *
     * @return array
     */
    public function getStatusList() :array;

    /**
     * Lấy danh sách các bàn theo place ID
     *
     * @param  string $placeId
     * @return App\Models\Table
     */
    public function getByPlaceId(string $placeId);

    /**
     * Lấy danh sách bàn theo danh sách id của place
     *
     * @param      array  $placeIds  The place identifiers
     */
    public function getByPlaceIds(array $placeIds);

    /**
     * Search Tables in group
     * 
     * @param  array $request
     * @param  array $relations
     * @param  array $selecions
     * @param  int   $page
     * 
     * @return array
     */
    public function getByCondition(array $request, $relations = [], $page = 1);

    /**
     * Xoá danh sách bàn khi xoá place chứa các bàn đó
     *
     * @param  string $placeId
     * @return void
     */
    public function deleteByPlaceId(string $placeId);
}