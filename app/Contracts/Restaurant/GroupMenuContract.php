<?php

namespace App\Contracts\Restaurant;


use App\Contracts\BaseContract;

interface GroupMenuContract extends BaseContract
{
    /**
     * get level 0 of group menu
     *
     * @return int
     */
    public function getLevel0();

    /**
     * get level 1 of group menu
     *
     * @return int
     */
    public function getLevel1();

    /**
     * get level 2 of group menu
     *
     * @return int
     */
    public function getLevel2();

    /**
     * get group menus has level = 0 and it's child in group
     *
     * @param  string $groupId
     * @return App\Models\GroupMenu
     */
    public function getParentGroupMenusByGroupId(string $groupId);

    /**
     * delete groupMenu and it's childs
     *
     * @param  string $groupMenuId
     * @return App\Models\GroupMenu
     */
    public function deleteGroupMenuWithChilds(string $groupMenuId);

    /**
     * remove goodId from good_ids
     *
     * @param  GroupMenu $goodId
     * @param  string    $goodId
     * @return App\Models\GroupMenu
     */
    public function pullGoodId($groupMenu, string $goodId);

    /**
     * get group menu bu name
     *
     * @param  string $name
     * @param  string $groupId
     * @return App\Models\GroupMenu
     */
    public function getByName(string $groupId, string $name);

}