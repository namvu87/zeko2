<?php

namespace App\Contracts;

use Jenssegers\Mongodb\Eloquent\Model;

interface BaseContract
{
    /**
     * Get table name
     *
     * @return string
     */
    public function getKey(): string;

    /**
     * get all data in {Model} (can with relation)
     *
     * @param  array  $relations
     * @param  array  $condition
     * @return App\Models\{Model}
     */
    public function getAll(array $condition = [], array $relations = []);

    /**
     * Gets all data {Model} in group.
     *
     * @param      string  $groupId   The group identifier
     * @param      array   $relation  The relation
     * @return     App\Models\{Model}
     */
    public function getByGroupId(string $groupId, array $relations = []);

    /**
     * Lấy danh sách dữ liệu theo groupId kết hợp phân trang
     *
     * @param  string      $groupId
     * @param  int|integer $page
     * @param  array       $relation
     * @return Collection App\Models\{Model}
     */
    public function paginateByGroupId(string $groupId, int $page = 1, array $relation = []);

    /**
     * get data with pagination (can with relation) in {Model}
     * @param array $relation
     * @param array @selected
     * @return App\Models\{Model}
     */
    public function paginate($page = 1, array $relations = [], array $selections = []);

    /**
     * Get a record (can with relation) in {Model}
     *
     * @param  string $id
     * @return App\Models\{Model}
     */
    public function getById(string $id, array $relations = [], array $selections = []): ?Model;

    /**
     * Get all record has _id in array ids of {Model}
     *
     * @param  array  $ids
     * @return App\Models\{Model}
     */
    public function getByIds(array $ids, array $relations = [], array $selections = []);

    /**
     * Get data with pagination (can with relation, selected fields) by ids in {Model}
     *
     * @param array $ids
     * @param  int $perPage
     * @param  array $relation
     * @return App\Models\{Model}
     */
    public function paginateByIds(
        array $ids,
        $page = 1,
        array $relation = [],
        array $selected = []
    );

    /**
     * @param  int $limit
     * @return App\Models\{Model}
     */
    public function getLimit(int $limit);

    /**
     * create a record in {Model}
     * @param  array  $data
     * @return \App\Models\{Model}
     */
    public function create(array $data);

    /**
     * update record has id = $id in {Model}
     * @param  string $id
     * @param  array  $data
     * @return bool
     */
    public function update(string $id, array $data);

    /**
     * Update record and 
     * 
     * @param Model $model
     * @param array $data
     * @return App\Models\{Model}
     */
    public function save(Model $model, array $data);

    /**
     * Update via Model instance
     *
     * @param  Model $model
     * @param  array $data
     * @return bool
     */
    public function updateInstance(Model $model, array $data): bool;

    /**
     * Update without get instance
     *
     * @param array $ids
     * @param array $data
     * @return void
     */
    public function massUpdate(array $ids, array $data): bool;

    /**
     * remove 1 record has id = $id in {Model}
     * @param  string $id
     * @return int
     */
    public function delete(string $id);

    /**
     * Delete the document via instance itself
     *
     * @param Model $model
     * @return bool
     */
    public function deleteInstance(Model $model): bool;

    /**
     * remove records has id in $ids in {Model}
     * @param  array $ids
     * @return bool
     */
    public function destroy(array $ids);

    /**
     * Lấy số lượng bản ghi theo điều kiện
     *
     * @param  array  $condition
     * @return int
     */
    public function count(array $condition = []): int;
}
