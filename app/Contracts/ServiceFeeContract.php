<?php

namespace App\Contracts;

use App\Contracts\BaseContract;

interface ServiceFeeContract extends BaseContract
{
    public function getUnpaidState(): int;

    public function getInprogressState(): int;

    public function getPaidState(): int;

    /**
     * Lấy danh sách hóa đơn thanh toán theo tháng
     *
     * @param array      $groupIds
     * @param array      $relations
     * @param null|string $month
     *
     * @return mixed
     */
    public function getByGroupIds(array $groupIds, array $relations, string $month);
}
