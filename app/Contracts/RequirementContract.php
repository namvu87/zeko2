<?php

namespace App\Contracts;

interface RequirementContract extends BaseContract
{
    /**
     * Lấy loại yêu cầu chấm công bổ sung
     */
    public function getAdditionTimekeepingType();

    /**
     * Lấy loại yêu cầu chuyển ca làm việc
     */
    public function getWitchShiftType();

    /**
     * Lấy loại yêu cầu xin nghỉ phép
     */
    public function getTakeLeaveType();

    /**
     * Lấy loại yêu cầu tham gia nhóm
     */
    public function getJoinGroupType();
}