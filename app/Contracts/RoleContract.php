<?php

namespace App\Contracts;

use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;
use App\Contracts\BaseContract;

interface RoleContract extends BaseContract
{
    /**
     * Gán quyền cho 1 vai trò
     *
     * @param  Role       $role
     * @param  Permission $permission
     *
     * @return void
     */
    public function assign(Role $role, Permission $permission);

    /**
     * Thu hồi quyền của vai trò
     *
     * @param  Role       $role
     * @param  Permission $permission
     *
     * @return void
     */
    public function revoke(Role $role, Permission $permission);

    /**
     * Xác định vai trò của 1 user trong nhóm chỉ định
     *
     * @param string $userId
     * @param string $groupId
     *
     * @return Maklad\Permission\Models\Role
     */
    public function getRoleUserByGroupId(string $userId, string $groupId): ?Role;

    /**
     * Lấy vai trò bởi tên vai trò
     *
     * @param array $roleNames
     *
     * @return mixed
     */
    public function getByRoleNames(array $roleNames);
}
