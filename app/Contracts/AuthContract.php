<?php
namespace App\Contracts;

interface AuthContract
{
    /**
     * Khởi tạo đối tượng người dùng
     *
     * @param  array  $data
     * @return App\Models\User | null
     */
    public function regist(array $data);

    /**
     * Xác thực người dùng
     *
     * @param  array $data
     * @return App\Models\User | null
     */
    public function login(array $data);

    /**
     * Đăng xuất
     *
     * @param array $args
     * @return bool
     */
    public function logout(array $args);
}