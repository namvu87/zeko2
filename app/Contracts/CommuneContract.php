<?php
namespace App\Contracts;

use App\Contracts\BaseContract;

interface CommuneContract extends BaseContract
{
    /**
     * get all communes via area id
     * @return App\Models\Commune
     */
    public function getAllByAreaId(string $areaId);

    /**
     * get {LIMIT} areas via key word input
     * @param  string $keyword
     * @return App\Models\Area
     */
    public function getByKeyword(string $keyword);
}