<?php
namespace App\Contracts;

use Illuminate\Http\Request;

interface CaptchaContract
{
    /**
     * Generate captcha
     *
     * @return string
     */
    public function init();

    /**
     * Check valid captcha
     *
     * @return bool
     */
    public function checkApi(string $value, string $key, int $expires);
}