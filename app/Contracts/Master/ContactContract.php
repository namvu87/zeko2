<?php
namespace App\Contracts\Master;

use App\Contracts\BaseContract;

interface ContactContract extends BaseContract
{
    /**
     * Gets the customer care.
     */
    public function getCustomerCare();

    /**
     * Stores a customer care.
     *
     * @param      array  $data   The data customer care
     * @return     App\Models\Contact
     */
    public function storeCustomerCare(array $data);

    /**
     * Update customer care
     *
     * @param      array   $data            The data customer care
     * @param      string  $customerCareId  The customer care identifier
     */
    public function updateCustomerCare(array $data, string $customerCareId);

    /**
     * Gets the contact information.
     */
    public function getContactInformation();

    /**
     * Sets the contact information.
     *
     * @param      array  $data   The data contact
     */
    public function setContactInformation(array $data);

}