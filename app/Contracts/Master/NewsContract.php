<?php

namespace App\Contracts\Master;

use App\Contracts\BaseContract;

interface NewsContract extends BaseContract
{
    /**
     * Filter list News by status news
     *
     * @param   integer $statusNews The status news
     */
    public function filter(int $statusNews);

    /**
     * Create a record News model
     *
     * @param   Array $data
     * @return  App\Models\News
     */
    public function store(Array $data);

    /**
     * Update News model
     *
     * @param   Array $data
     * @param   string $id
     * @return  App\Models\News
     */
    public function updateModel(Array $data, string $id);

    /**
     * Restore News model
     *
     * @param      string $newsId The tag identifier
     * @return
     */
    public function restore(string $newsId);

    public function getUserGuide();

    public function getNewsByType(int $type);

    public function getAllNewNews();

    public function getNewsByTagId(string $tagId);

    public function getNewPosts();

    public function getOtherPostsBySlug(string $slug);

    public function getNewsById(string $newsId);

    public function getNewsByIdWithTags(string $newsId);

    public function getFirstNewsByType(int $type);

    public function getCareerNewsByTag(string $tag);

    public function getCareerNews();

    public function changeStatus(int $status, string $id);

    public function delete(string $id);

    public function refreshCache(string $newsId = null, $type = null);

    public function getTagsInNormalNews();

    public function getBySlug(string $slug);
}