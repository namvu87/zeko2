<?php

namespace App\Contracts\Master;

use App\Contracts\BaseContract;

interface SliderContract extends BaseContract
{
    /**
     * Create a record Slider model
     *
     * @param      Array $data
     * @return     App\Models\Slider
     */
    public function store(array $data);

    /**
     * Update Slider model
     *
     * @param      Array $data
     * @param      string $id
     */
    public function updateModel(array $data, string $id);

    /**
     * get all slider
     *
     */
    public function getSliders();

    public function getAllSliders();
}