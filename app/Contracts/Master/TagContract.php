<?php

namespace App\Contracts\Master;

use App\Contracts\BaseContract;
use Jenssegers\Mongodb\Eloquent\Model;

interface TagContract extends BaseContract
{

    public function statusVisiable();

    /**
     * Filter list Tag by status tag
     *
     * @param      integer $statusTag The status tag
     */
    public function filter(int $statusTag);

    /**
     * Create a record Tag model
     *
     * @param     Array $data
     * @return    App\Models\Tag
     */
    public function store(Array $data);

    /**
     * Delete Tag model
     *
     * @param     string $tagId The tag identifier
     * @return    boolean
     */
    public function delete(string $tagId);

    /**
     * Restore Tag model
     *
     * @param      string $tagId The tag identifier
     * @return
     */
    public function restore(string $tagId);

    /**
     * get top tags for news
     *
     * @return
     */
    public function getTopTagsByTags(array $tags);

    /**
     * get tag by name
     *
     * @param string $name
     * @return Model
     */
    public function getByName(string $name);
}