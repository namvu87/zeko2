<?php
/**
 * Created by PhpStorm.
 * User: zeko-1
 * Date: 18/04/2019
 * Time: 11:52
 */

namespace App\Contracts\Master;


use App\Contracts\BaseContract;

interface UserGuideContract extends BaseContract
{
    public function getAllGuide(string $status);

    /**
     * Restore guide
     *
     * @param  string$id
     * @return \Illuminate\Http\Response
     */
    public function restore(string $id);

    /**
     * get category by service
     *
     * @param  string $service
     * @return \Illuminate\Http\Response
     */
    public function getCategoryByService(string $service);

    /**
     * get user guide by slug
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function getBySlug(string $slug, string $service);

    /**
     * get user guide by category
     *
     * @param  int $category
     * @return \Illuminate\Http\Response
     */
    public function getByCategory(int $category, string $service);

    /**
     * get user guide by title
     *
     * @param  int $category
     * @return \Illuminate\Http\Response
     */
    public function searchByTitle(string $title);
}