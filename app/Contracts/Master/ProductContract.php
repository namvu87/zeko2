<?php

namespace App\Contracts\Master;

use App\Contracts\BaseContract;

interface ProductContract extends BaseContract
{
    /**
     * Gets the best selling products.
     */
    public function getBestSellingProducts();
}