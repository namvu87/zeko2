<?php

namespace App\Contracts;


use Carbon\Carbon;

interface DateTimeContract
{
    /**
     * get period month
     * @return \DateTime
     */
    public function getPeriodMonth(Carbon $startDate, Carbon $endDate): array;

    /**
     * get period time by type option
     * @return \DateTime
     */
    public function getPeriodDateByOption(string $option): array;

}