<?php

namespace App\Contracts;

use App\Contracts\BaseContract;
use Carbon\Carbon;

interface GroupContract extends BaseContract
{
    /**
     * @return int
     */
    public function getTimekeepingType();

    /**
     * @return int
     */
    public function getRestaurantType();

    /**
     * @return mixed
     */
    public function getServicesType();

    /**
     * Get group's owner by groupId
     *
     * @param string $groupId
     * @return App\Models\User
     */
    public function getOwner(string $groupId);

    /**
     * Lấy danh sách nhóm theo ID chủ sở hữu
     *
     * @param  string $ownerId
     * @return App\Models\Group
     */
    public function getByOwnerId(string $ownerId);

    /**
     * Gets the new group.
     */
    public function getNewGroups();

    /**
     * Lấy danh sách các nhóm mới theo trạng thái chăm sóc
     *
     * @param int     $careStatus
     * @param bool    $status
     * @param int     $page
     * @param string|null $ownerId
     *
     * @return mixed
     */
    public function getGroups(int $careStatus, bool $status, int $page, ?string $ownerId);

    /**
     * Tìm kiếm nhóm theo từ khoá
     *
     * @param  string $keyword
     * @return App\Models\Group
     */
    public function getByKeyword(string $keyword);

    /**
     * Thêm trạng thái đang bán hàng cho người dùng
     *
     * @param string $groupId
     * @param string $userId
     * @return boolean
     */
    public function onActingUser(string $groupId, string $userId): bool;

    /**
     * Xoá người dùng khỏi trạng thái bán hàng
     *
     * @param string $groupId
     * @param string $userId
     * @return boolean
     */
    public function offActingUser(string $groupId, string $userId): bool;

    /**
     * Lấy các nhóm đăng ký mới kèm thông tin
     * 
     * $startDate Carbon
     * $endDate Carbon
     */
    public function groupsStatistic(Carbon $startDate, Carbon $endDate);
}
