<?php

namespace App\Contracts;

use App\Contracts\BaseContract;

interface PermissionContract extends BaseContract
{
    /**
     * Lấy tất cả quyền theo ID của vai trò
     *
     * @param  string $roleId
     * @return Maklad\Permission\Models\Permission
     */
    public function getByRoleId(string $roleId);
}
