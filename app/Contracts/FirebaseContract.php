<?php

namespace App\Contracts;

interface FirebaseContract
{
    /**
     * Gửi thông báo đơn mục tiêu
     *
     * @param  array  $recipient
     * @return FirebaseService
     */
    public function to(array $recipient);

    /**
     * Gửi thông báo theo chủ đề
     *
     * @param  string $topic
     * @return FirebaseService
     */
    public function toTopic(string $topic);

    /**
     * Payload of notify
     *
     * @param  array  $data
     * @return FirebaseService
     */
    public function data(array $data = []);

    /**
     * @param  array  $notification
     * @return FirebaseService
     */
    public function notification(array $notification = []);

    /**
     * Execute send notification
     *
     * @return string
     */
    public function send();
}