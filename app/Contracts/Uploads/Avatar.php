<?php
namespace App\Contracts\Uploads;

use Illuminate\Http\UploadedFile;

interface Avatar
{
    /**
     * Generate unique filename to store S3
     * @return string
     */
    public function generateFilename($userId, int $size) :string;

    /**
     * Resize image
     * @param  Illuminate\Http\UploadedFile $file
     * @param  integer $size
     * @return Intervention\Image\Image encoded
     */
    public function resize(UploadedFile $file, int $size);

    /**
     * Function entry
     *
     * @param UploadedFile $file
     * @return array
     */
    public function upload(UploadedFile $file) :array;

    /**
     * Execute upload to driver
     *
     * @param string $filename
     * @param $source
     * @return string
     */
    public function store(string $filename, $source) :string;

    /**
     * Delete old avatar
     * 
     * @param  string $path
     * @return void
     */
    public function remove(array $urls);
}