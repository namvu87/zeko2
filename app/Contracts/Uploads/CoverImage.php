<?php
namespace App\Contracts\Uploads;

use Illuminate\Http\UploadedFile;

interface CoverImage
{
    /**
     * Generate unique filename to store S3
     * @return string
     */
    public function generateFilename($userId) :string;

    /**
     * Resize image
     * @param  Illuminate\Http\UploadedFile $file
     * @param  integer $size
     * @return Intervention\Image\Image encoded
     */
    public function resize(UploadedFile $file);

    /**
     * Function entry
     *
     * @param UploadedFile $file
     * @return array
     */
    public function upload(UploadedFile $file) :string;

    /**
     * Execute upload to driver
     *
     * @param string $filename
     * @param $source
     * @return string
     */
    public function store(string $filename, $source) :string;

    /**
     * Delete old cover image
     * 
     * @param  string $path
     * @return void
     */
    public function remove(string $urls);
}