<?php
namespace App\Contracts\Users;

use App\Models\User;

interface Firebase
{
    /**
     * @param  string $token
     * @param  App\Models\User   $user
     * @return void
     */
    public function push(User $user, string $token);

    /**
     * @param  string $token
     * @param  User   $user
     * @return void
     */
    public function pull(User $user, string $token);

    /**
     * Thêm mã uuid vào user
     *
     * @param  User   $user
     * @param  string $uuid
     * @return void
     */
    public function pushUuid(User $user, string $uuid);
}