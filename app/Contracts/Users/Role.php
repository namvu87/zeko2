<?php
namespace App\Contracts\Users;

use App\Models\User;
use Maklad\Permission\Models\Role as RoleModel;

interface Role
{
    /**
     * Gán quyền của group cho User
     *
     * @param  App\Models\User $user
     * @param  Role $role
     * @return void
     */
    public function assign(User $user, RoleModel $role);

    /**
     * Thu hồi quyền của User trong 1 group
     *
     * @param  App\Models\User $user
     * @param  Role $role
     * @return void
     */
    public function revoke(User $user, RoleModel $role);
}