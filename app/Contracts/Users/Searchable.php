<?php
namespace App\Contracts\Users;

interface Searchable
{
    /**
     * Tìm kiếm user theo từ khoá
     *
     * @param  string      $keyword
     * @param  int|integer $page
     * @return App\Models\User
     */
    public function getByKeyword(string $keyword, int $page = 1);

    /**
     * Get users list of group
     *
     * @param  string $keyword
     * @param  string $groupId
     * @return App\Models\User
     */
    public function getByGroupId(string $keyword, string $groupId, int $page = 1);

    /**
     * Lấy danh sách user không nằm trong group theo keyword
     * Phân trang dữ liệu
     *
     * @param  string $keyword
     * @param  string $groupId
     * @param  int    $page
     * @return App\Modes\User
     */
    public function getOutGroup(string $keyword = '', string $groupId, int $page = 1);

    /**
     * Get users list of shift
     *
     * @param  string $keyword
     * @param  string $shiftId
     * @return App\Models\User
     */
    public function getByShiftId(string $keyword, string $shiftId, int $page = 1);

    /**
     * Get list users in groups but not in shift
     *
     * @param  string $groupId
     *
     * @return App\Models\User
     */
    public function getNotShiftInGroup(string $keyword, string $groupId, array $shiftIds, int $page = 1);

    /**
     * Get users list in group and without shift
     *
     * @param  string $keyword
     * @param  string $groupId
     * @param  string $shiftId
     * @return App\Models\User
     */
    public function getOutShiftId(string $keyword, string $groupId, string $shiftId, int $page = 1);

    /**
     * Lấy tất cả User trong group mà chưa có vai trò theo điều kiện
     *
     * @param  string $keyword
     * @param  string $groupId
     * @return App\Models\User
     */
    public function getOutRole(string $keyword, string $groupId);
}