<?php

namespace App\Contracts;

interface Searchable
{
    /**
     * Lấy danh sách các provinces
     *
     * @return App\Models\Province
     */
    public function getProvinces();

    /**
     * Xác định thành phố hiện tại của user
     *
     * @param  float  $longitude
     * @param  float  $latitude
     * @return App\Models\Province
     */
    public function getCurrentProvince(float $longitude, float $latitude);

    /**
     * Tìm kiếm nhà hàng theo vị trí user
     *
     * @param  float  $longitude
     * @param  float  $latitude
     * @return Restaurant
     */
    public function findAround(array $args, int $page = 1);

    /**
     * Lấy danh sách nhà hàng theo thứ tự mới nhất của một province
     *
     * @param  array       $args
     * @param  int|integer $page
     * @return App\Models\Restaurant
     */
    public function newest(array $args, int $page = 1);

     /**
     * Lấy chi tiết 1 nhà hàng
     *
     * @param  string $restaurantId
     * @return App\Models\Restaurant
     */
    public function getRestaurantById(string $restaurantId);

    /**
     * Lấy danh sách ảnh của nhà hàng
     *
     * @param  string      $restaurantId
     * @param  int|integer $page
     * @return App\Models\RestaurantImage
     */
    public function getImagesByRestaurantId(string $restaurantId, int $page = 1);

    /**
     * Tìm kiếm nhà hàng theo từ khoá (tên, địa chỉ ...)
     *
     * @param  string $keyword
     * @param  string $province
     * @return App\Models\Restaurant
     */
    public function getRestaurantsByKeyword(string $keyword, string $province, int $page = 1);
}