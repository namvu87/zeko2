<?php
namespace App\Contracts;

interface ImageContract
{
    /**
     * Set image source
     * @param $imagePath
     */
    public function setImage($imagePath);

    /**
     * Get image's path source
     * @return string
     */
    public function getImage();

    /**
     * Set image's rate
     * @param float $rate
     */
    public function setRate(float $rate);

    /**
     * Get image's rate
     * @return float
     */
    public function getRate();

    /**
     * Set image sizes
     * @param int $width
     * @param int|null $height
     */
    public function setSize(int $width, $height = null);

    /**
     * Get image's sizes
     * @return array
     */
    public function getSize(): array;

    /**
     * Set folder dest path to save
     * @param string $path
     */
    public function setDestPath(string $destPath);

    /**
     * Get folder dest path to save
     * @return string
     */
    public function getDestPath();

    /**
     * Set image coordinate
     * @param int $xCoord
     * @param int $yCoord
     */
    public function setCoordinate(int $xCoord, int $yCoord);

    /**
     * Get image coordinate
     * @return array
     */
    public function getCoordinate(): array;

    /**
     * Set fit position image
     * @param int $position
     */
    public function setFitPosition(int $position);

    /**
     * Get image fit position
     * @return string
     */
    public function getFitPosition();

    /**
     * Set filename new image
     * @param string $filename
     */
    public function setFilename(string $filename);

    /**
     * @return get filename new image
     */
    public function getFilename();

    /**
     * @param  integer $quality
     * @return Intervention\Image\Image
     */
    public function resize(int $quality = 100);

    /**
     * @param  integer $quality
     * @return Intervention\Image\Image
     */
    public function crop(int $quality = 100);

    /**
     * @param  integer $quality
     * @return Intervention\Image\Image
     */
    public function fit(int $quality = 100);

    /**
     * @param  string $mode
     * @param  integer $quality
     * @return string
     */
    public function save(string $mode = 'fit', int $quality = 100);

    /**
     * Save image base on Storage
     * @param  integer $quality
     * @param  string $mode
     * @return image instance
     */
    public function saveInDisk(string $mode = 'resize', int $quality = 100);

    /**
     * Remove the image
     *
     * @return void
     */
    public function delete();
}