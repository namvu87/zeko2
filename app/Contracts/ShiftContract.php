<?php

namespace App\Contracts;

use App\Contracts\BaseContract;

interface ShiftContract extends BaseContract
{
    /**
     * Get single type in model (2 scan per day)
     *
     * @return string
     */
    public function getSingleType();

    /**
     * Get multiple type in model (4 scan per day)
     *
     * @return string
     */
    public function getMultiType();

    /**
     * Get all types
     *
     * @return array
     */
    public function getTypes();

    /**
     * Get all schedules
     *
     * @return array
     */
    public function getSchedules();

    /**
     * @param array $shiftIds
     * @param string $groupId
     *
     * @return mixed
     */
    public function getByIdsAndGroup(array $shiftIds, string $groupId);
}