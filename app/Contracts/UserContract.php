<?php

namespace App\Contracts;

use App\Contracts\BaseContract;

interface UserContract extends BaseContract
{
    /**
     * get current user
     *
     * @return \App\Models\User
     */
    public function getUser();

    /**
     * Get current user's id
     *
     * @return string
     */
    public function id();

    /**
     * Lấy thông tin người dùng theo email hoặc số điện thoại
     *
     * @param string $identifier
     *
     * @return \App\Models\User
     */
    public function findForPassport(string $identifier);

    /**
     * Get all emplyees in group specified
     *
     * @param  string $groupId
     *
     * @return \App\Models\User
     */
    public function getByGroupIdNotPaginate(string $groupId);

    /**
     * Lấy số lượng nhân viên trong group
     *
     * @param  string $groupId
     *
     * @return int
     */
    public function getCountByGroupId(string $groupId);

    /**
     * Lấy danh sách user không có trong group
     *
     * @param  string $groupId
     * @param  int $page
     *
     * @return \App\Models\User
     */
    public function getOutGroup(string $groupId, int $page = 1);

    /** Get user with timekeeping via groupId
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     *
     * @return
     */
    public function getByGroupIdWithTimekeeping(string $groupId, string $startDate, string $endDate);

    /**
     * Assign user to group's emplyee list
     *
     * @param string $userId
     * @param string $groupId
     */
    public function addUserToGroup(string $userId, string $groupId);

    /**
     * Remove user from group
     *
     * @param  string $userId
     * @param  string $groupId
     */
    public function removeUserFromGroup(string $userId, string $groupId);

    /**
     * Get list employee permissioned
     * @param string $groupId
     * @return
     */
    public function getByRoleIds(array $roleIds);

    /**
     * Get list user in shift
     *
     * @param  string $shiftId
     *
     * @return \App\Models\User
     */
    public function getByShiftId(string $shiftId, int $page = 1);

    /**
     * Remove user from shift
     *
     * @param  string $userId
     * @param  string $shiftId
     */
    public function removeUserFromShift(string $userId, string $shiftId);

    /**
     * Remove users from shift
     *
     * @param  string $shiftId
     */
    public function removeUsersFromShift(string $shiftId);

    /**
     * Remove user from shift
     *
     * @param  string $userId
     * @param  string $shiftId
     */
    public function removeShift(string $shiftId);

    /**
     * Get list users in groups but not in shift
     *
     * @param  string $groupId
     *
     * @return \App\Models\User
     */
    public function getNotShiftInGroup(string $groupId, array $shiftIds, int $page = 1);

    /**
     * Get list users in groups but not in shift
     *
     * @param  string $shiftId
     * @param  string $groupId
     *
     * @return \App\Models\User
     */
    public function getOutShiftId(string $groupId, string $shiftId, int $page = 1);

    /**
     * add user to shift
     *
     * @param  string $userId
     * @param  string $shiftId
     *
     * @return array
     */
    public function addUserToShift(string $userId, string $shiftId);

    /**
     * Get a user instance via firebase token
     *
     * @param  string $token
     *
     * @return \App\Models\User
     */
    public function getByFirebaseToken(string $token);

    /**
     * Lấy hữu hạn những User trong group mà chưa có vai trò
     *
     * @param  string $keyword
     * @param  string $groupId
     *
     * @return \App\Models\User
     */
    public function getOutRole(string $groupId, $page = 1);

    /**
     * Lấy user theo mã UUID theo máy
     * UUID là mã máy sinh ra duy nhất theo ứng dụng, và sẽ thay đổi khi cài lại ứng dụng
     *
     * @param  string $uuid
     *
     * @return \App\Models\User
     */
    public function getByUuid($uuid);

    /**
     * Lấy firebase token của nhóm user
     *
     * @param  array $userIds
     *
     * @return array
     */
    public function getFirebaseTokensByIds(array $userIds): array;
}
