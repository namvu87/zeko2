<?php
namespace App\Contracts;

use App\Contracts\BaseContract;

interface AreaContract extends BaseContract
{
    /**
     * @param  string $PDF_set_info_keywords()
     * @return App\Models\Area
     */
    public function getAreaByKeyword(string $keyword);
}