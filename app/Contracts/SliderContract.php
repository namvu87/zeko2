<?php

namespace App\Contracts;

interface SliderContract
{
    /**
     * @return App\Models\Slider
     */
    public function getCampaign();
}