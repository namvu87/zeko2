<?php

namespace App\Contracts;

use Illuminate\Http\Request;
use App\Contracts\BaseContract;

interface TimekeepingContract extends BaseContract
{
    public function typeAddition();

    public function typeSwitch();

    public function typeTakeLeave();

    /**
     * Get timekeeping by condition
     *
     * @param array $condition
     *
     * @return
     */
    public function getByCondition(array $condition);

    /**
     * Get timekeeping by condition
     *
     * @param array $condition
     *
     * @return
     */
    public function getByConditionGraphQL(array $condition, $page = 1, $relations = []);

    /**
     * Get timekeeping by userId and groupId
     *
     * @param array $condition
     *
     * @return
     */
    public function getByUserIdInGroup(string $userId, string $groupId, string $startDate, string $endDate);

    /**
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     *
     * @return mixed
     */
    public function groupByUserIdInGroup(string $groupId, string $startDate, string $endDate);

    /**
     * Lấy dữ liệu chấm công của người dùng hiện tại trong ngày hôm nay
     *
     * @param  string $userId
     * @param  string $groupId
     *
     * @return App\Models\Timekeeping|null
     */
    public function getTimekeepingToday(string $userId, string $groupId);

    /**
     * Lấy danh sách chấm công của người dùng hiện tại
     *
     * @param  string $userId
     * @param  array $args
     * @param  array $relations
     * @param  array $selections
     *
     * @return list of App\Models\Timekeeping
     */
    public function getMyTimekeepings(string $userId, array $args, $relations = []);

    /**
     * Lấy 1 kết quả chấm công theo ngày của người dùng hiện tại
     *
     * @param  string $userId
     * @param  string $groupId
     * @param  string $date
     *
     * @return App\Models\Timekeeping
     */
    public function getMyTimekeepingByDate(string $userId, string $groupId, string $date);
}