<?php

namespace App\Contracts;


interface GoodImageContract extends BaseContract
{
    /**
     * Lấy danh sách ảnh hàng hóa/ thực đơn
     *
     * @param string $name
     * @param int    $page
     *
     * @return mixed
     */
    public function getLists(string $name, int $page);

    /**
     * get good image folder by name
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getByName(string $name);
}
