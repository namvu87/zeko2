<?php
namespace App\Contracts;

use App\Contracts\BaseContract;
use App\Models\Notification;

interface NotificationContract extends BaseContract
{
	/**
	 * Gets the data.
	 *
	 * @param      \App\Models\Notification  $notification  The notification
	 */
	public function getData(Notification $notification);

	/**
	 * Gets the user notifications.
	 *
	 * @param      array  $data   The data
	 */
	public function getUserNotifications(array $data);

	/**
	 * Gets the count notification by user identifier.
	 *
	 * @param string  $userId
	 */
	public function getCountNotificationByUserId(string $userId) :int;

	/**
     * Lấy thông báo của người dùng theo trang
     *
     * @param integer $page
     * @param string $userId
     * @param array $relations
     * @return Pagination
     */
    public function paginateByUserId(int $page = 1, string $userId, array $relations = []);

	/**
     * Xoá thông báo khi hoá đơn được thanh toán, hoặc huỷ
     *
     * @param string $invoiceId
     * @return boolean
     */
    public function removeByInvoiceId(string $invoiceId): bool;
}