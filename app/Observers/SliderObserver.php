<?php

namespace App\Observers;

use App\Contracts\Master\SliderContract;
use Illuminate\Support\Facades\Cache;
use App\Models\Slider;
use Illuminate\Support\Facades\File;

class SliderObserver
{
    private $slider;

    public function __construct(SliderContract $slider)
    {
        $this->slider = $slider;
        if (Cache::has('sliders')) Cache::forget('sliders');
    }

    public function created()
    {

    }

    public function updated()
    {

    }

    public function deleted(Slider $slider)
    {
        $images = $slider->photo_url;
        foreach ($images as $image) {
            if (File::exists($image)) {
                File::delete($image);
            }
        }
    }

    public function __destruct()
    {
        $this->slider->getSliders();
    }
}
