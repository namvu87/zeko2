<?php

namespace App\Observers;

use App\Contracts\Master\TagContract;
use App\Models\News;

class NewsObserver
{
    private $tag;

    public function __construct(TagContract $tag)
    {
        $this->tag = $tag;
    }

    public function created(News $news)
    {
        if ($news->tags) {
            foreach ($news->tags as $tag) {
                $tagModel = $this->tag->getByName($tag);
                if (!$tagModel) {
                    $this->tag->create([
                        'name' => $tag,
                        'usage_count' => 0,
                        'status' => $this->tag->statusVisiable(),
                    ]);
                } else {
                    $tagModel->update([
                        'usage_count' => $tagModel->usage_count + 1
                    ]);
                }
            }
        }
    }
}
