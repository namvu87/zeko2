<?php

namespace App\Observers;

use App\Contracts\UserContract;
use App\Models\Notification;
use App\Models\Shift;

class ShiftObserver
{
    private $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    public function updated(Shift $shift)
    {
        $users = $this->user->getByShiftId($shift->id);
        foreach ($users as $user) {
            Notification::create([
                'user_assign_id' => auth('api')->id(),
                'shift_id' => $shift->id,
                'user_id' => $user->id,
                'group_id' => $shift->group_id,
                'type' => Notification::TYPE_16,
            ]);
        }
    }

    public function deleted(Shift $shift)
    {
        $this->user->removeUsersFromShift($shift->id);
    }
}
