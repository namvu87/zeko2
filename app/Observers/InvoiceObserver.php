<?php

namespace App\Observers;

use App\Models\Invoice;
use App\Contracts\GroupContract;
use App\Contracts\Restaurant\TableContract;

class InvoiceObserver
{
    private $group;
    private $table;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(GroupContract $group, TableContract $table)
    {
        $this->group = $group;
        $this->table = $table;
    }

    /**
     * Handle the invoice "created" event.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return void
     */
    public function created(Invoice $invoice)
    {
        // Cập nhật trạng thái các bàn
        $tableIds = $invoice->table_ids ?? [];
        if (!empty($tableIds)) {
            $this->table->massUpdate($tableIds, [
                'status'     => $this->table->getActingType(),
                'invoice_id' => $invoice->id
            ]);
        }
        $group = $this->group->getById($invoice->group_id);
        $group->increment('invoice_number');
    }

    /**
     * Handle the invoice "updated" event.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return void
     */
    public function updated(Invoice $invoice)
    {
        //
    }

    /**
     * Handle the invoice "deleted" event.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return void
     */
    public function deleted(Invoice $invoice)
    {
        // $this->table->available($invoice->id);
    }

    /**
     * Handle the invoice "restored" event.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return void
     */
    public function restored(Invoice $invoice)
    {
        //
    }

    /**
     * Handle the invoice "force deleted" event.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return void
     */
    public function forceDeleted(Invoice $invoice)
    {
        //
    }
}
