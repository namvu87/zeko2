<?php

namespace App\Observers;

use App\Models\Good;
use App\Contracts\GroupContract;
use App\Contracts\Restaurant\GroupMenuContract;
use App\Contracts\Restaurant\GoodContract;

class GoodObserver
{
    private $group;
    private $menu;
    private $good;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(GroupContract $group, GroupMenuContract $menu, GoodContract $good)
    {
        $this->group = $group;
        $this->menu = $menu;
        $this->good = $good;
    }

    /**
     * Handle the good "created" event.
     *
     * @param  \App\Models\Good  $good
     * @return void
     */
    public function created(Good $good)
    {
        $group = $this->group->getById($good->group_id);
        $group->increment('goods_number');

        foreach ($good->group_menu_ids as $id) {
            $groupMenu = $this->menu->getById($id);
            $groupMenu->push('goods_ids', $good->id, true);
        }
    }

    /**
     * Handle the good "updated" event.
     *
     * @param  \App\Models\Good  $good
     * @return void
     */
    public function updated(Good $good)
    {
        //
    }

    /**
     * Handle the good "deleted" event.
     *
     * @param  \App\Models\Good  $good
     * @return void
     */
    public function deleted(Good $good)
    {
        foreach ($good->groupMenus as $groupMenu) {
            $this->menu->pullGoodId($groupMenu, $good->id);
        }

        if (empty($good->parent_id)) {
            // Xoá những hàng hoá con
            foreach ($good->childs as $child) {
                $this->good->delete($child->id);
            }

            // Xoá ảnh trên S3 của hàng hoá này
            foreach ((array) $good->images as $image) {
                $this->good->removeImage($image);
            }
        }
    }

    /**
     * Handle the good "restored" event.
     *
     * @param  \App\Models\Good  $good
     * @return void
     */
    public function restored(Good $good)
    {
        //
    }

    /**
     * Handle the good "force deleted" event.
     *
     * @param  \App\Models\Good  $good
     * @return void
     */
    public function forceDeleted(Good $good)
    {
        //
    }
}
