<?php

namespace App\Observers;

use App\Events\RequirementCreated;
use App\Models\Requirement;
use Illuminate\Support\Facades\Log;

class RequirementObserver
{
    public function created(Requirement $requirement)
    {
        $requirement->group()->increment('number_requirement');
        event(new RequirementCreated($requirement));
    }
}
