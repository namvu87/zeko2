<?php

namespace App\Mail;

use App\Models\Group;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DisableGroup extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $group;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Group $group)
    {
        $this->group = $group;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.disable_group')
            ->subject(__('mail.subject.disable_group'))
            ->with(['group' => $this->group]);
    }
}
