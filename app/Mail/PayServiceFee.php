<?php

namespace App\Mail;

use App\Models\ServiceFee;
use App\Models\Group;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PayServiceFee extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $fee;
    private $group;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceFee $fee, Group $group)
    {
        $this->fee = $fee;
        $this->group = $group;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.payment_periodic')
            ->subject(__('mail.subject.pay_service_fee'))
            ->with([
                'fee'   => $this->fee,
                'group' => $this->group
            ]);
    }
}
