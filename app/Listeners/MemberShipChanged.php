<?php

namespace App\Listeners;

use App\Events\MemberShipChanged as MemberShipChangedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contracts\NotificationContract;
use App\Contracts\GroupContract;

class MemberShipChanged implements ShouldQueue
{
    private $notification;
    private $group;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationContract $notification, GroupContract $group)
    {
        $this->notification = $notification;
        $this->group = $group;
    }

    /**
     * Handle the event.
     *
     * @param  MemberShipChangedEvent  $event
     * @return void
     */
    public function handle(MemberShipChangedEvent $event)
    {
        $group = $this->group->getById($event->groupId);

        $this->notification->create([
            'creator' => [
                'id'       => $event->creator->id,
                'fullname' => $event->creator->fullname,
                'avatars'  => $event->creator->avatars
            ],
            'group' => [
                'id'   => $group->id,
                'name' => $group->name
            ],
            'user_id'    => $event->userId,
            'type'       => $event->type
        ]);
    }
}
