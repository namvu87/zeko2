<?php

namespace App\Listeners;

use App\Traits\PushNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\RequirementCreated as RequirementCreatedEvent;

class RequirementCreated implements ShouldQueue
{
    use PushNotification;

    private $title;
    private $body;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationCreated $event
     *
     * @return void
     */
    public function handle(RequirementCreatedEvent $event)
    {
        $notification = $event->requirement;
        $tokenDevice = $notification->user->firebase_token ?? [];
        $this->title = $notification->group->name;
        $this->body = __("notification.messages.{notify.type}", [
            'user_assign' => $notification->creator->last_name . ' ' . $notification->creator->first_name,
            'group' => $notification->group->name
        ]);
        $this->sendSingleDevices(
            $tokenDevice,
            $notification,
            $this->title,
            $this->body
        );
    }
}
