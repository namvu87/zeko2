<?php

namespace App\Listeners;

use App\Events\GroupCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;
use App\Models\Group;

class GenerateGroupRoles implements ShouldQueue
{
    /**
     * user's role default, who registed the group
     */
    const TIMEKEEPING_SERVICE = 'timekeeping_service';
    const RESTAURANT_SERVICE = 'restaurant_service';

    /**
     * Handle the event.
     *
     * @param  GroupCreated  $event
     * @return void
     */
    public function handle(GroupCreated $event)
    {
        $serviceType = $this->convertServiceTypeToText($event->group->service_type);

        $roles = config("role.{$serviceType}.roles");
        unset($roles['owner']);
        
        foreach ($roles as $roleName => $permissions) {
            $role = $this->createRoles($roleName, $event->group);
            foreach ($permissions as $permission) {
                $role->givePermissionTo($event->group->id . $permission);
            }
        }
    }

    /**
     * Generate a role and assign every permisson for owner role
     *
     * @param  string $role
     * @param  Group  $group
     * @return Maklad\Permission\Models\Role
     */
    private function createRoles(string $role, Group $group): Role
    {
        return Role::create([
            'name'     => $group->id . '.' . $role,
            'group_id' => $group->id
        ]);
    }

    /**
     * @param  int $type
     * @return string
     */
    private function convertServiceTypeToText(int $type) :string {
        if ($type === Group::TIMEKEEPING_SERVICE) {
            return self::TIMEKEEPING_SERVICE;
        }
        return self::RESTAURANT_SERVICE;
    }
}
