<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\NotificationCreated as NotificationCreatedEvent;
use App\Contracts\UserContract;
use App\Contracts\FirebaseContract;
use App\Interfaces\Notifiable;

class NotificationCreated implements ShouldQueue, Notifiable
{
    private $user;
    private $firebase;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserContract $user, FirebaseContract $firebase)
    {
        $this->user = $user;
        $this->firebase = $firebase;
    }

    /**
     * Handle the event.
     *
     * @param  NotificationCreatedEvent $event
     *
     * @return void
     */
    public function handle(NotificationCreatedEvent $event)
    {
        if (in_array($event->notification->type, self::MANAGE_SALE_TYPES)) {
            return;
        }

        $notification = $event->notification;

        $userIds = [$notification->user_id] ?? $notification->user_ids ?? [];
        $tokens = $this->user->getFirebaseTokensByIds($userIds);

        $this->firebase
            ->to($tokens)
            ->data([
                'notification' => $notification,
                'type'         => $notification->type
            ])
            ->notification([
                'title' => 'ZEKO',
                'body' => __("notification.messages_original.{$notification->type}", [
                    'creator' => $notification->creator['fullname'] ?? null,
                    'group'   => $notification->group['name'] ?? null,
                    'shift'   => $notification->shift['name'] ?? null,
                    'invoice' => $notification->invoice['code'] ?? null
                ])
            ])
            ->send();
    }
}
