<?php

namespace App\Listeners;

use App\Contracts\NotificationContract;
use App\Events\SystemNotify as SystemNotifyEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class SystemNotify implements ShouldQueue
{
    private $notification;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationContract $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  SystemNotify  $event
     * @return void
     */
    public function handle(SystemNotifyEvent $event)
    {
        $this->notification->create([
            'group' => [
                'id'   => $event->group->id,
                'name' => $event->group->name
            ],
            'month'    => date('Y-m'),
            'user_ids' => $event->userIds,
            'type'     => $event->type
        ]);
    }
}
