<?php

namespace App\Listeners;

use App\Events\GroupCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;
use App\Models\Group;

class GenerateGroupPermissions
{
    /**
     * user's role default, who registed the group
     */
    const TIMEKEEPING_SERVICE = 'timekeeping_service';
    const RESTAURANT_SERVICE = 'restaurant_service';

    /**
     * Handle the event.
     * create default permissions, roles of group
     *
     * @param  GroupCreated  $event
     * @return void
     */
    public function handle(GroupCreated $event)
    {
        $serviceType = $this->convertServiceTypeToText($event->group->service_type);

        $permissions = config("role.{$serviceType}.permissions");
        foreach ($permissions as $label => $actions) {
            foreach ($actions as $action) {
                $this->createPermission($label, $action, $event->group);
            }
        }

        $role = Role::create([
            'name'     => $event->group->id . '.owner',
            'group_id' => $event->group->id
        ]);
        $event->group->owner->assignRole($role);
        $permissions = config("role.{$serviceType}.roles.owner");
        foreach ($permissions as $permission) {
            $role->givePermissionTo($event->group->id . $permission);
        }
    }

    /**
     * Generate a permission
     *
     * @param  string $label
     * @param  string $permission
     * @param  Group  $group
     * @return Maklad\Permission\Models\Permission
     */
    private function createPermission(string $label, string $permission, Group $group)
    {
        return Permission::create([
            'name'     => $group->id . '.' . $label . '.' . $permission,
            'group_id' => $group->id,
            'label'    => $label
        ]);
    }

    /**
     * @param  int $type
     * @return string
     */
    private function convertServiceTypeToText(int $type) :string {
        if ($type === Group::TIMEKEEPING_SERVICE) {
            return self::TIMEKEEPING_SERVICE;
        }
        return self::RESTAURANT_SERVICE;
    }
}
