<?php

namespace App\Listeners;

use App\Events\UserRoleChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRoleChanged
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRoleChanged  $event
     * @return void
     */
    public function handle(UserRoleChanged $event)
    {
        //
    }
}
