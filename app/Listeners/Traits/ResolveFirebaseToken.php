<?php

namespace App\Listeners\Traits;

trait ResolveFirebaseToken
{
     /**
     * Lấy token của những user cần gửi thông báo
     *
     * @param $event
     * @return array
     */
    private function getTokens($event): array
    {
        $actingUserIds = $this->group->getById($event->invoice->group_id)->acting_user_ids ?? [];

        $userIds = array_merge($actingUserIds, $event->invoice->user_ids ?? []);
        
        $tokens = $this->user->getFirebaseTokensByIds($userIds);

        return array_diff($tokens, [$event->exceptToken]);
    }
}