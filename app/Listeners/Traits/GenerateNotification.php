<?php
namespace App\Listeners\Traits;

trait GenerateNotification
{
    /**
     * Tạo thông báo cho phía người dùng
     *
     * @param $event
     * @return void
     */
    private function createNotification($event, int $type, array $userIds)
    {
        $this->notification->create([
            'invoice' => [
                'id' => $event->invoice->id,
                'code' => $event->invoice->code
            ],
            'user_ids'     => $userIds,
            'type'         => $type,
            'view'         => false,
            'creator'      => [
                'id'       => $event->creator->id,
                'fullname' => $event->creator->fullname,
                'avatars'  => $event->creator->avatars
            ]
        ]);
    }
}