<?php

namespace App\Listeners\Restaurant\ManageSale;

use App\Events\Restaurant\ManageSale\PurchaseInvoiceCheckouted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contracts\Restaurant\GoodContract;
use App\Events\Restaurant\GoodRunningOut;
use App\Models\Good as GoodModel;

class CalculatingInventoryNumber
{
    private $good;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    /**
     * Handle the event.
     *
     * @param  PurchaseInvoiceCheckouted  $event
     * @return void
     */
    public function handle(PurchaseInvoiceCheckouted $event)
    {
        $goods = $event->invoice->goods;
        foreach ($goods as $good) {
            $goodModel = $this->good->getById($good['id']);
            if ($goodModel->type === $this->good->getImportType()) { // Hàng nhập
                $this->calculatingInventoryNumber($goodModel, $good['count']);
            } else if ($goodModel->type === $this->good->getProcessType()) { // Hàng chế biến
                if (count($goodModel->ingredients) > 0) {
                    foreach ($goodModel->ingredients as $goodIngredients) {
                        $goodIngredientsModel = $this->good->getById($goodIngredients['id']);
                        $this->calculatingInventoryNumber($goodIngredientsModel, $good['count']);
                    }
                }
            }
        }
    }

    /**
     * Trừ đi số lượng đã sử dụng vào tồn kho của hàng hóa và lưu vào DB
     *
     * @param      GoodModel  $good       The good
     * @param      integer    $countUsed  The count used
     */
    private function calculatingInventoryNumber(GoodModel $good, int $countUsed)
    {
        if ($good->inventory_number) {
            $good->decrement('inventory_number', $countUsed);
        } else {
            $good->inventory_number = -$countUsed;
        }

        $good->save();

        // Thông báo nếu hàng sắp hết
        if (($good->inventory_min !== 0)  && ($good->inventory_number <= $good->inventory_min)) {
            event(new GoodRunningOut($good));
        }
    }
}
