<?php

namespace App\Listeners\Restaurant;

use App\Events\Restaurant\GoodRunningOut;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyGoodRunningOut
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GoodRunningOut  $event
     * @return void
     */
    public function handle(GoodRunningOut $event)
    {
        //
    }
}
