<?php

namespace App\Listeners\Restaurant;

use App\Events\Restaurant\RequestAccepted as RequestAcceptedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Interfaces\Notifiable;
use App\Contracts\UserContract;
use App\Contracts\GroupContract;
use App\Contracts\FirebaseContract;
use App\Contracts\NotificationContract;
use App\Listeners\Traits\GenerateNotification;
use App\Listeners\Traits\ResolveFirebaseToken;

class RequestAccepted implements ShouldQueue, Notifiable
{
    use GenerateNotification, ResolveFirebaseToken;

    private $user;
    private $group;
    private $firebase;
    private $notification;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        UserContract $user,
        GroupContract $group,
        FirebaseContract $firebase,
        NotificationContract $notification
    )
    {
        $this->user = $user;
        $this->group = $group;
        $this->firebase = $firebase;
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  RequestAcceptedEvent  $event
     * @return void
     */
    public function handle(RequestAcceptedEvent $event)
    {
        // Không gửi thông báo firebase nếu yêu cầu được xác nhận được gửi từ nhà bếp
        if ($event->fromType === self::FROM_KITCHEN) {
            return;
        }

        $type = self::TYPE_53;

        // if (count($event->invoice->user_ids ?? []) > 0) {
        //     $this->createNotification($event, $type, $event->invoice->user_ids);
        // }

        $tokens = $this->getTokens($event);

        $this->firebase
            ->to($tokens)
            ->data([
                'invoice_id'   => $event->invoice->id,
                'invoice_code' => $event->invoice->code,
                'table_ids'    => $event->invoice->table_ids ?? [],
                'creator'      => $event->creator->fullname,
                'type'         => $type
            ])
            ->notification([
                'title' => __('manage_sale.notify.invoice_updated_title', [
                    'code' => $event->invoice->code
                ]),
                'body' => __("manage_sale.notify.{$type}")
            ])
            ->send();
    }
}
