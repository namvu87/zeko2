<?php

namespace App\Listeners\Restaurant;

use App\Events\Restaurant\InvoiceMemberAdded as InvoiceMemberAddedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contracts\NotificationContract;
use App\Interfaces\Notifiable;

class InvoiceMemberAdded implements ShouldQueue, Notifiable
{
    private $notification;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationContract $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceMemberAddedEvent  $event
     * @return void
     */
    public function handle(InvoiceMemberAddedEvent $event)
    {
        $this->notification->create([
            'creator' => [
                'id' => $event->creator->id,
                'fullname' => $event->creator->fullname,
                'avatars' => $event->creator->avatars 
            ],
            'invoice' => [
                'id' => $event->invoice->id,
                'code' => $event->invoice->code
            ],
            'user_ids' => $event->invoice->user_ids,
            'type'     => self::TYPE_54
        ]);
    }
}
