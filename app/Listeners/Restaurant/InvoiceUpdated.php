<?php

namespace App\Listeners\Restaurant;

use App\Events\Restaurant\InvoiceUpdated as InvoiceUpdatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Interfaces\Notifiable;
use App\Contracts\UserContract;
use App\Contracts\GroupContract;
use App\Contracts\FirebaseContract;
use App\Contracts\NotificationContract;
use App\Listeners\Traits\GenerateNotification;
use App\Listeners\Traits\ResolveFirebaseToken;

class InvoiceUpdated implements ShouldQueue, Notifiable
{
    use GenerateNotification, ResolveFirebaseToken;

    private $group;
    private $user;
    private $firebase;
    private $notification;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        UserContract $user,
        GroupContract $group,
        FirebaseContract $firebase,
        NotificationContract $notification
    )
    {
        $this->user = $user;
        $this->group = $group;
        $this->firebase = $firebase;
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceUpdatedEvent  $event
     * @return void
     */
    public function handle(InvoiceUpdatedEvent $event)
    {
        // if (count($event->invoice->user_ids ?? []) > 0) {
        //     $userIds = $event->invoice->user_ids;
        //     if ($event->fromType === self::FROM_USER_TYPE) {
        //         $userIds = array_diff($userIds, [$event->creator->id]);
        //     }
        //     $this->createNotification($event, $event->type, $userIds);
        // }
        
        $tokens = $this->getTokens($event);     
        $this->firebase
            ->to($tokens)
            ->data([
                'invoice_id'   => $event->invoice->id,
                'invoice_code' => $event->invoice->code,
                'table_ids'    => $event->invoice->table_ids ?? [],
                'creator'      => $event->creator->fullname,
                'type'         => $event->type,
                'from_type'    => $event->fromType
            ])
            ->notification([
                'title' => __('manage_sale.notify.invoice_updated_title', [
                    'code' => $event->invoice->code
                ]),
                'body' => __("manage_sale.notify.{$event->type}")
            ])
            ->send();
    }
}
