<?php

namespace App\Listeners\Restaurant;

use App\Events\Restaurant\ConversationUpdated as ConversationUpdatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Listeners\Traits\ResolveFirebaseToken;
use App\Contracts\FirebaseContract;
use App\Contracts\UserContract;

class ConversationUpdated implements ShouldQueue
{
    use ResolveFirebaseToken;

    private $firebase;
    private $user;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(FirebaseContract $firebase, UserContract $user)
    {
        $this->firebase = $firebase;
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  ConversationUpdated  $event
     * @return void
     */
    public function handle(ConversationUpdatedEvent $event)
    {
        $tokens = $this->user->getFirebaseTokensByIds($event->userIds);
        $tokens = array_diff($tokens, [$event->exceptToken]);

        $this->firebase
            ->to($tokens)
            ->data([
                'payload' => $event->payload
            ])
            ->notification([
                'title' => $event->payload['creator']['fullname'],
                'body' => $event->payload['content'],
                'image' => $event->payload['creator']['avatar']
            ])
            ->send();
    }
}
