<?php

namespace App\Listeners\Restaurant;

use App\Events\Restaurant\InvoiceCreated as InvoiceCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contracts\UserContract;
use App\Contracts\FirebaseContract;
use App\Contracts\GroupContract;
use App\Interfaces\Notifiable;

class InvoiceCreated implements ShouldQueue, Notifiable
{
    private $user;
    private $firebase;
    private $group;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        UserContract $user,
        FirebaseContract $firebase,
        GroupContract $group
    )
    {
        $this->user = $user;
        $this->firebase = $firebase;
        $this->group = $group;
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceCreatedEvent  $event
     * @return void
     */
    public function handle(InvoiceCreatedEvent $event)
    {
        $userIds = $this->group->getById($event->invoice->group_id)->acting_user_ids ?? [];
        $tokens = $this->user->getFirebaseTokensByIds($userIds);

        if ($event->fromType === self::FROM_EMPLOYEE_TYPE) {
            $tokens = array_diff($tokens, [$event->exceptToken]);
        }

        $type = self::TYPE_40;
        
        $this->firebase
            ->to($tokens)
            ->data([
                'invoice_id' => $event->invoice->id,
                'type'       => $type,
                'from_type'  => $event->fromType
            ])
            ->notification([
                'title' => __('manage_sale.notify.invoice_created_title'),
                'body' => __("manage_sale.notify.{$type}", [
                    'code' => $event->invoice->code
                ]),
            ])
            ->send();
    }
}
