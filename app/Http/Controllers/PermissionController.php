<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\UserContract;
use App\Contracts\RoleContract;
use App\Contracts\PermissionContract;
use App\Contracts\Users\Searchable;
use App\Contracts\Users\Role as UserRoleable;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Permission as PermissionResource;
use App\Http\Resources\Role as RoleResource;
use App\Models\Notification;

class PermissionController extends Controller
{
    private $user;
    private $role;
    private $search;
    private $userRole;
    private $permission;

    public function __construct(
        UserContract $user,
        RoleContract $role,
        Searchable $search,
        UserRoleable $userRole,
        PermissionContract $permission
    )
    {
        $this->user       = $user;
        $this->role       = $role;
        $this->search     = $search;
        $this->userRole   = $userRole;
        $this->permission = $permission;
    }

    /**
     * Lấy danh sách tất cả users được phân quyền trong 1 nhóm.
     * Lấy danh sách tất cả các roles trong 1 nhóm.
     *
     * @param  Request $request
     * @return json
     */
    public function roles(Request $request)
    {
        $roles = $this->role->getByGroupId($request->group_id);

        $roleIds = $roles->pluck('id')->toArray();
        $users = $this->user->getByRoleIds($roleIds);

        return response()->json([
            'roles' => RoleResource::collection($roles),
            'users' => UserResource::collection($users)
        ]);
    }

    /**
     * Lấy danh sách tất cả các roles cùng quan hệ permission trong 1 nhóm.
     * Lấy danh sách tất cả các permission trong 1 nhóm.
     *
     * @param  Request $request
     * @return json
     */
    public function rolesPermissions(Request $request)
    {
        $roles = $this->role->getByGroupId($request->group_id);
        $permissions = $this->permission->getByGroupId($request->group_id);

        return response()->json([
            'roles' => RoleResource::collection($roles),
            'permissions' => PermissionResource::collection($permissions)->groupBy('label')
        ]);
    }

    /**
     * Gán vai trò (Role) cho 1 user
     * Đảm bảo 1 user chỉ có 1 vai trò trong 1 nhóm, vai trò gưi lên thuộc về
     * nhóm chỉ định, và vai trò không thể là owner
     *
     * @param  Request $request
     * @return json
     */
    public function assignRole(Request $request)
    {
        $request->validate([
            'user_id' => 'required|string',
            'role_id' => 'required|string'
        ]);

        $role = $this->role->getById($request->role_id);
        $user = $this->user->getById($request->user_id);

        // Kiểm tra role thuộc về group
        $this->authorize('belongsToGroup', [$role, $request->group_id]);
        // Kiểm tra role được chỉ định không phải owner
        $this->authorize('outOwner', $role);

        // Xoá vai trò hiện tại
        $roles = $user->roles->where('group_id', $request->group_id);
        foreach ($roles as $roleRemoved) {
            // Kiểm tra role hiện tại không phải owner mới được xoá
            $this->authorize('outOwner', $roleRemoved);
            $user->removeRole($roleRemoved);
        }

        $this->userRole->assign($user, $role);

        Notification::create([
            'creator_id' => auth('api')->id(),
            'user_id' => $request->user_id,
            'group_id' => $request->group_id,
            'type' => Notification::TYPE_60,
            'role_name' => str_replace($request->group_id . '.', '', $role->name),
        ]);

        // Cập nhật lại giá trị role cho thằng User 
        $user = $this->user->getById($request->user_id);
        return response()->json([
            'message' => __('word.updated'),
            'user' => new UserResource($user)
        ]);
    }

    /**
     * Thu hồi 1 vai trò (Role) của 1 user
     * Đảm bảo vai trò gửi lên thuộc về group chỉ định
     * Vai trò không được là owner
     *
     * @param  Request $request
     * @return json
     */
    public function revokeRole(Request $request)
    {
        $request->validate([
            'user_id' => 'required|string',
            'role_id' => 'required|string'
        ]);

        $role = $this->role->getById($request->role_id);
        $user = $this->user->getById($request->user_id);

        $this->authorize('belongsToGroup', [$role, $request->group_id]);
        $this->authorize('outOwner', $role);

        $this->userRole->revoke($user, $role);

        Notification::create([
            'creator_id' => auth('api')->id(),
            'user_id' => $user->_id,
            'group_id' => $request->group_id,
            'type' => Notification::TYPE_61,
            'role_name' => str_replace($request->group_id . '.', '', $role->name),
        ]);

        return response()->json([
            'message' => __('company.permission.delete_user_success')
        ]);
    }

    public function updateRolePermissions(Request $request)
    {
        $request->validate([
            'role_id' => 'required|string',
            'permission_ids' => 'nullable|array',
            'permission_ids.*' => 'string'
        ]);

        $permissions = $this->permission->getByIds($request->permission_ids);
        $role = $this->role->getById($request->role_id);

        $this->authorize('belongsToGroup', [$role, $request->group_id]);
        $this->authorize('outOwner', $role);

        $this->assignPermissionToRole($role, $permissions, $request->group_id);
        $this->removePermissionFromRole($role, $request->permission_ids);

        return response()->json([
            'message' => __('company.permission.update_role_permissions_success')
        ]);
    }

    /**
     * Kiểm tra có nhưng permission nào trong mảng gửi lên từ client không nằm trong
     * quyền của vai trò này thì thêm quyền đó vào vai trò
     *
     * @param  Maklad\Permission\Models\Role $role
     * @param  array  $permissions
     * @return void
     */
    private function assignPermissionToRole($role, $permissions, $groupId)
    {
        foreach ($permissions as $permission) {
            $this->authorize('belongsToGroup', [$permission, $groupId]);

            if (!in_array($permission->id, (array) $role->permission_ids)) {
                $this->role->assign($role, $permission);
            }
        }
    }

    /**
     * Kiểm tra có những permission nào không nằm trong mảng gửi từ client
     * thì xoá quyền đó khỏi vai trò chỉ định
     *
     * @param  Maklad\Permission\Models\Role $role
     * @param  array $permissionIds
     * @return void
     */
    private function removePermissionFromRole($role, array $permissionIds)
    {
        foreach ($role->permissions as $permission) {
            if (!in_array($permission->id, $permissionIds)) {
                $this->role->revoke($role, $permission);
            }
        }
    }
}
