<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\ServiceFeeContract;
use App\Contracts\ServiceFeeTransactionContract;
use App\Contracts\NotificationContract;
use App\Traits\Payment\MomoHashes;
use GuzzleHttp\Client;
use App\Interfaces\Notifiable;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\App;

class AppMomoController extends Controller implements Notifiable
{
    use MomoHashes;

    const MOMO_CAPTURE = 'capture';
    const MOMO_REVERT_AUTHORIZE = 'revertAuthorize';

    private $fee;
    private $transaction;
    private $notification;

    public function __construct(
        ServiceFeeContract $fee,
        ServiceFeeTransactionContract $transaction,
        NotificationContract $notification
    ) {
        $this->fee = $fee;
        $this->transaction = $transaction;
        $this->notification = $notification;
    }

    /**
     * Xử lý dữ liệu trả về từ momo khi thanh toán trên APP
     *
     * @param Request $request
     * @return void
     */
    public function callback(Request $request)
    {
        $hashed = $this->hashData($request);
        // Kiểm tra chữ ký điện tử
        if (!hash_equals($request->signature, $hashed)) {
            throw new AuthorizationException('Chữ ký điện tử không hợp lệ');
        }

        $transaction = $this->transaction->getById($request->partnerRefId);

        // Giao dịch thành công
        if ($request->status === 0) {
            // Commit giao dịch
            try {
                $response = $this->confirm($request, self::MOMO_CAPTURE);
                if ($response['status'] === 0) {
                    $this->updateData($transaction, $response);
                }
            } catch (\Exception $e) {
                if (App::environment() !== 'testing') {
                    throw new AuthorizationException('Không thể xác nhận thanh toán');
                }
            }

            // Response current request
            $responseData = [
                'amount'       => $request->amount,
                'message'      => 'Giao dịch thành công',
                'momoTransId'  => $request->momoTransId,
                'partnerRefId' => $request->partnerRefId,
                'status'       => 0
            ];
            $responseData['signature'] = $this->generateSignature($responseData);
            return response()->json($responseData);
        }

        // Nếu không thành công
        try {
            $this->confirm($request, self::MOMO_REVERT_AUTHORIZE);
        } catch (\Exception $e) {
            if (App::environment() !== 'testing') {
                throw new AuthorizationException('Không thể xác nhận thanh toán');
            }
        }
        $this->updateFailedData($transaction, $request);

        $responseData = [
            'amount'       => $request->amount,
            'message'      => 'Thanh toán không hoàn tất',
            'momoTransId'  => $request->momoTransId,
            'partnerRefId' => $request->partnerRefId,
            'status'       => $this->transaction->getCanceledState()
        ];
        $responseData['signature'] = $this->generateSignature($responseData);
        return response()->json($responseData);
    }

    /**
     * Tạo chữ ký điện tử từ MOMO trả về theo chuẩn của MOMO
     *
     * @param Request $request
     * @return string
     */
    private function hashData(Request $request): string
    {
        $rawData = [
            'accessKey'      => $request->accessKey,
            'amount'         => $request->amount,
            'message'        => $request->message,
            'momoTransId'    => $request->momoTransId,
            'partnerCode'    => $request->partnerCode,
            'partnerRefId'   => $request->partnerRefId,
            'partnerTransId' => $request->partnerTransId,
            'responseTime'   => $request->responseTime,
            'status'         => $request->status,
            'storeId'        => $request->storeId,
            'transType'      => $request->transType
        ];
        return $this->generateSignature($rawData);
    }

    /**
     * Xác nhận giao dịch (commit hoặc rollback)
     *
     * @param Request $request
     * @param string $requestType
     * @return void
     */
    private function confirm(Request $request, string $requestType): array
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
        $payload = [
            'partnerCode' => $request->partnerCode,
            'partnerRefId' => $request->partnerRefId,
            'requestType' => $requestType,
            'requestId' => $request->momoTransId,
            'momoTransId' => $request->momoTransId
        ];
        $payload['signature'] = $this->generateSignature($payload);
        if ($requestType === self::MOMO_REVERT_AUTHORIZE) {
            $payload['description'] = 'Thông tin giao dịch không đúng';
        }

        $response = $client->post(config('payment.momo.app_confirm_url'), [
            'body' => json_encode($payload)
        ]);
        return json_decode((string) $response->getBody(), true);
    }

    /**
     * Cập nhật dữ liệu nếu thanh toán thành công
     *
     * @param \App\Models\ServiceFeeTransaction $transaction
     * @return void
     */
    private function updateData($transaction, array $response)
    {
        $this->notification->create([
            'month'   => date('Y-m'),
            'user_id' => $transaction->user_id,
            'type'    => self::TYPE_103
        ]);
        $transaction = $this->transaction->save($transaction, [
            'status' => $this->transaction->getPaidState(),
            'payment_info' => [
                'momo' => [
                    'status'         => $response['status'],
                    'message'        => $response['message'],
                    'pay_type'       => 'app',
                    'transaction_id' => $response['data']['momoTransId'],
                    'amount'         => $response['data']['amount']
                ]
            ]
        ]);
        foreach ($transaction->service_fees as $item) {
            $this->fee->update($item['service_fee_id'], [
                'transaction_id' => $transaction->id,
                'package' => $item['package'] ?? null,
                'status' => $this->fee->getPaidState(),
                'amount' => $response['data']['amount']
            ]);
        }
    }

    /**
     * Cập nhật dữ liệu khi giao dịch thất bại
     *
     * @param \App\Models\ServiceFeeTransaction $transaction
     * @param array $request
     * @return void
     */
    private function updateFailedData($transaction, $request)
    {
        $this->notification->create([
            'month'   => date('Y-m'),
            'user_id' => $transaction->user_id,
            'type'    => self::TYPE_104
        ]);
        $transaction = $this->transaction->save($transaction, [
            'status' => $this->transaction->getCanceledState(),
            'payment_info' => [
                'momo' => [
                    'status'         => $request->status,
                    'pay_type'       => 'app',
                    'message'        => $request->message,
                    'response_time'  => $request->responseTime,
                    'amount'         => $request->amount,
                    'transaction_id' => $request->momoTransId,
                ]
            ]
        ]);
        foreach ($transaction->service_fees as $item) {
            $this->fee->update($item['service_fee_id'], [
                'transaction_id' => null,
                'status' => $this->fee->getUnpaidState()
            ]);
        }
    }
}
