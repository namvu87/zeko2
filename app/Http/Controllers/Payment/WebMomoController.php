<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contracts\ServiceFeeContract;
use App\Contracts\ServiceFeeTransactionContract;
use App\Contracts\NotificationContract;
use App\Traits\Payment\MomoHashes;
use App\Interfaces\Notifiable;
use Illuminate\Auth\Access\AuthorizationException;

class WebMomoController extends Controller implements Notifiable
{
    use MomoHashes;

    const SUSSCESS_ERROR_CODE = 0;

    private $fee;
    private $transaction;
    private $notification;

    public function __construct(
        ServiceFeeContract $fee,
        ServiceFeeTransactionContract $transaction,
        NotificationContract $notification
    ) {
        $this->fee = $fee;
        $this->transaction = $transaction;
        $this->notification = $notification;
    }

    /**
     * Nhận thông báo ngầm định từ MOMO cho kết quả thanh toán qua phương thức IPN
     *
     * @param Request $request
     * @return void
     */
    public function callback(Request $request)
    {
        $hashed = $this->hash($request);

        // Kiểm tra chữ ký điện tử
        if (!hash_equals($request->signature, $hashed)) {
            throw new AuthorizationException('Chữ ký điện tử không hợp lệ');
        }

        $transaction = $this->transaction->getById($request->orderId);

        // Thanh toán thành công
        if ($request->errorCode == 0) {
            $this->updateSuccessData($transaction, $request);
            $resData = $this->generateResponse(
                $request,
                'Thanh toán thành công',
                self::SUSSCESS_ERROR_CODE
            );
            return response()->json($resData, 200, [
                'Content-Type' => 'application/json',
                'charset' => 'UTF-8'
            ]);
        }

        // Thanh toán thất bại
        $this->updateFailedData($transaction, $request);
        $resData = $this->generateResponse(
            $request,
            'Thanh toán không hoàn tất',
            $request->errorCode
        );
        return response()->json($resData, 200, [
            'Content-Type' => 'application/json',
            'charset' => 'UTF-8'
        ]);
    }

    /**
     * Tạo dữ liệu trả về cho MOMO
     *
     * @param Request $request
     * @return array
     */
    private function generateResponse(Request $request, $message, $errorCode): array
    {
        $data = [
            'partnerCode' => $request->partnerCode,
            'accessKey' => $request->partnerCode,
            'requestId' => $request->requestId,
            'orderId' => $request->orderId,
            'errorCode' => $errorCode,
            'message' => $message,
            'responseTime' => date('Y-m-d H:i:s'),
            'extraData' => $request->extraData,
        ];
        $data['signature'] = $this->generateSignature($data);
        return $data;
    }

    /**
     * Cập nhật dữ liệu nếu thanh toán thành công
     *
     * @param \App\Models\ServiceFeeTransaction $transaction
     * @param Request $request
     * @return void
     */
    private function updateSuccessData($transaction, Request $request)
    {
        $this->notification->create([
            'month'   => date('Y-m'),
            'user_id' => $transaction->user_id,
            'type'    => self::TYPE_103
        ]);
        $transaction = $this->transaction->save($transaction, [
            'status' => $this->transaction->getPaidState(),
            'payment_info' => [
                'momo' => [
                    'error_code'     => $request->errorCode,
                    'message'        => $request->message,
                    'local_message'  => $request->localMessage,
                    'pay_type'       => $request->payType,
                    'transaction_id' => $request->transId,
                    'amount'         => $request->amount,
                    'response_time'  => $request->responseTime,
                ]
            ]
        ]);
        foreach ($transaction->service_fees as $item) {
            $this->fee->update($item['service_fee_id'], [
                'transaction_id' => $transaction->id,
                'package' => $item['package'] ?? null,
                'status' => $this->fee->getPaidState(),
                'amount' => $request->amount
            ]);
        }
    }

    /**
     * Cập nhật dữ liệu khi giao dịch thất bại
     *
     * @param \App\Models\ServiceFeeTransaction $transaction
     * @param array $request
     * @return void
     */
    private function updateFailedData($transaction, $request)
    {
        $this->notification->create([
            'month'   => date('Y-m'),
            'user_id' => $transaction->user_id,
            'type'    => self::TYPE_104
        ]);
        $transaction = $this->transaction->save($transaction, [
            'status' => $this->transaction->getCanceledState(),
            'payment_info' => [
                'momo' => [
                    'error_code'     => $request->errorCode,
                    'message'        => $request->message,
                    'local_message'  => $request->localMessage,
                    'response_time'  => $request->responseTime,
                    'amount'         => $request->amount,
                    'transaction_id' => $request->transId,
                    'pay_type'       => $request->payType,
                ]
            ]
        ]);
        foreach ($transaction->service_fees as $item) {
            $this->fee->update($item['service_fee_id'], [
                'transaction_id' => null,
                'status' => $this->fee->getUnpaidState()
            ]);
        }
    }

    /**
     * generate signature dữ liệu trả về từ momo
     *
     * @param Request $request
     * @return string
     */
    private function hash(Request $request): string
    {
        return $this->generateSignature([
            'partnerCode'  => $request->partnerCode,
            'accessKey'    => $request->accessKey,
            'requestId'    => $request->requestId,
            'amount'       => $request->amount,
            'orderId'      => $request->orderId,
            'orderInfo'    => $request->orderInfo,
            'orderType'    => $request->orderType,
            'transId'      => $request->transId,
            'message'      => $request->message,
            'localMessage' => $request->localMessage,
            'responseTime' => $request->responseTime,
            'errorCode'    => $request->errorCode,
            'payType'      => $request->payType,
            'extraData'    => $request->extraData,
        ]);
    }
}
