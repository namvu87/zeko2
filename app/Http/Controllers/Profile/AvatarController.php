<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Profile\ProfileController;
use Illuminate\Support\Facades\Validator;
use App\Contracts\UserContract;
use App\Contracts\Uploads\Avatar as AvatarUpload;

class AvatarController extends ProfileController
{
    private $avatar;

    public function __construct(UserContract $user, AvatarUpload $avatar)
    {
        parent::__construct($user);
        $this->avatar = $avatar;
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'avatar' => 'required|mimes:jpeg,png,jpg|max:5120'
        ]);
    }

    public function prepare(Request $request)
    {
        return [
            'avatars' => $this->avatar->upload($request->avatar)
        ];
    }
}
