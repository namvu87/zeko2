<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Profile\ProfileController;
use Illuminate\Support\Facades\Validator;
use App\Contracts\CaptchaContract;
use App\Contracts\UserContract;
use App\Rules\CaptchaRule;
use App\Rules\PasswordMatched;

class PasswordController extends ProfileController
{
    private $captcha;

    public function __construct(CaptchaContract $captcha, UserContract $user)
    {
        parent::__construct($user);
        $this->captcha = $captcha;
    }

    /**
     * @param  array  $data
     * @return \Illuminate\Validation\Validation
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'old_password' => [
                'present',
                new PasswordMatched
            ],
            'password' => 'required|string|min:8|confirmed',
            'key'      => 'required|string',
            'expires'  => 'required|numeric',
            'code'     => [
                'required',
                'string',
                new CaptchaRule($data['key'], $data['expires'], $this->captcha)
            ]
        ]);
    }

    /**
     * @param  Request $request
     * @return array
     */
    protected function prepare($request)
    {
        return [
            'password' => bcrypt($request->password)
        ];
    }
}
