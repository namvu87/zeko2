<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Profile\ProfileController;
use App\Contracts\Uploads\CoverImage as CoverImageUpload;
use Illuminate\Support\Facades\Validator;
use App\Contracts\UserContract;

class CoverImageController extends ProfileController
{
    private $coverImage;
    
    public function __construct(UserContract $user, CoverImageUpload $coverImage)
    {
        parent::__construct($user);
        $this->coverImage = $coverImage;
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'avatar' => 'required|mimes:jpeg,png,jpg|max:8192'
        ]);
    }

    public function prepare(Request $request)
    {
        return [
            'cover_image' => $this->coverImage->upload($request->avatar)
        ];
    }
}
