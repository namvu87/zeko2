<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Profile\ProfileController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OverviewController extends ProfileController
{
    /**
     * Validatate request update overview user profile
     *
     * @param  array  $data
     * @return \Illuminate\Support\Facades\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:64',
            'last_name'  => 'required|string|max:64',
            'birthday'   => 'required|date_format:d-m-Y',
            'gender'     => 'required|numeric|in:1,2',
            'area'       => 'required|array',
            'commune'    => 'required|array',
            'area.*'     => 'required|string|max:128',
            'commune.*'  => 'required|string|max:128',
            'address'    => 'required|string|max:256',
            'email'      => [
                'required', 'email',
                Rule::unique('users')->ignore(auth('api')->id(), '_id')
            ],
            'phone_number' => [
                'required', 'numeric',
                Rule::unique('users')->ignore(auth('api')->id(), '_id')
            ]
        ]);
    }
}
