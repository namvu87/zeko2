<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\UserContract;
use App\Http\Resources\UserBase as UserResource;

abstract class ProfileController extends Controller
{
    protected $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    /**
     * Execute update date
     *
     * @param  Request $request
     * @return json
     */
    public function update(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->user->getUser();

        $this->user->update($user->id, $this->prepare($request));

        return response()->json([
            'message' => __('common.update_success'),
            'user' => new UserResource($user->refresh())
        ]);
    }

    /**
     * Validate custom
     *
     * @param  array  $data
     * @return Validator
     */
    abstract protected function validator(array $data);

    /**
     * @param  Request $request
     * @return array
     */
    protected function prepare(Request $request)
    {
        return $request->all();
    }
}
