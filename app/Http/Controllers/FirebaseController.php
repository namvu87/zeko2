<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\Users\Firebase;

class FirebaseController extends Controller
{
    const SUCCESS_STATUS = 1;

    private $firebase;

    public function __construct(Firebase $firebase)
    {
        $this->firebase = $firebase;
    }

    /**
     * Addition firebase token to current user (when token initialize or when login)
     *
     * @param  Request $request
     * @return json
     */
    public function push(Request $request)
    {
        $request->validate(['firebase_token' => 'required|string|min:32']);

        $this->firebase->push($request->user(), $request->firebase_token);

        return response()->json([
            'code' => self::SUCCESS_STATUS
        ]);
    }
}
