<?php

namespace App\Http\Controllers;

use App\Contracts\GroupContract;
use App\Contracts\NotificationContract;
use App\Contracts\RequirementContract;
use App\Contracts\ShiftContract;
use App\Contracts\TimekeepingContract;
use App\Contracts\UserContract;
use App\Models\Notification;
use App\Models\Requirement;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RequirementController extends Controller
{
    private $requirement;
    private $group;
    private $user;
    private $notification;
    private $shift;
    private $timekeeping;

    public function __construct(RequirementContract $requirement, GroupContract $group, UserContract $user, NotificationContract $notification, ShiftContract $shift, TimekeepingContract $timekeeping)
    {
        $this->requirement = $requirement;
        $this->group = $group;
        $this->user = $user;
        $this->notification = $notification;
        $this->shift = $shift;
        $this->timekeeping = $timekeeping;
    }

    public function index(Request $request)
    {
        return response()->json([
            'requirements' => $this->requirement->paginateByGroupId($request->group_id, $request->page ?? 1, ['creator:first_name,last_name,avatars'])
        ]);
    }

    public function active($id)
    {
        $this->requirement->update($id, ['is_view' => true]);
    }

    public function reset(Request $request)
    {
        $this->group->update($request->group_id, ['number_requirement' => 0]);
        return;
    }

    public function detail($id)
    {
        $requirement = $this->requirement->getById($id, ['creator:first_name,last_name,phone_number,avatars', 'timekeeping']);
        return response()->json([
            'requirement' => $requirement
        ]);
    }

    public function getRequirementTimekeeping(Request $request, $id)
    {
        $requirement = $this->requirement->getById($id, [
            'creator:first_name,last_name,phone_number,avatars,shift_ids', 'timekeeping', 'notification', 'shift'
        ]);
        $shift = $this->shift->getByIdsAndGroup($requirement->creator->shift_ids, $requirement->group_id);
        if (!$shift && empty($requirement->timekeeping_id)) {
            $shifts = $this->shift->getByGroupId($requirement->group_id);
        }
        return response()->json([
            'requirement' => $requirement,
            'shift' => $shift,
            'shifts' => $shifts ?? []
        ]);
    }

    public function joinGroup(Request $request, $id)
    {
        $requirement = $this->checkBelongsToGroup($id, $request->group_id);
        if (!$requirement->is_exec && $requirement->type == $this->requirement->getJoinGroupType()) {
            $request->validate([
                'status' => 'required|boolean'
            ]);
            $notify = [
                'creator_id' => auth('api')->id(),
                'user_id' => $requirement->creator_id,
                'group_id' => $request->group_id,
                'requirement_id' => $id
            ];
            if ($request->status) {
                $this->user->addUserToGroup($requirement->creator_id, $request->group_id);
                $notify['type'] = Notification::TYPE_12;
            } else {
                $notify['type'] = Notification::TYPE_13;
            }
            $requirement->update(['is_exec' => true]);
            $this->notification->create($notify);
            return response()->json([
                'message' => __('word.success')
            ]);
        }
    }

    public function acceptAdditionalTimekeeping(Request $request, $id)
    {
        $requirement = $this->checkBelongsToGroup($id, $request->group_id);
        if (!$requirement->is_exec && $requirement->type == $this->requirement->getAdditionTimekeepingType()) {
            $shift = $this->shift->getByIdsAndGroup($requirement->creator->shift_ids, $requirement->group_id);
            if (!$shift && empty($requirement->timekeeping_id)) {
                $request->validate(['shift_id' => 'required']);
                $shift = $this->checkShiftBelongsToGroup($request->shift_id, $request->group_id);
            }
            $this->validateRequest($request, $shift->type);
            if (empty($requirement->timekeeping_id)) {
                $data = $this->prepareData($request, $requirement, $shift, $this->timekeeping->typeAddition());
                $timekeeping = $this->timekeeping->create($data);
            } else {
                $timekeeping = $this->timekeeping->getById($requirement->timekeeping_id);
                $times = [
                    [
                        'checkin' => $timekeeping->time[0]['checkin'] ?? ['time' => $request['checkin1']],
                        'checkout' => $timekeeping->time[0]['checkout'] ?? ['time' => $request['checkout1']]
                    ]
                ];
                // Trường hợp công ty 1 ngày chấm 4 lần
                if (!empty($request['checkin2']) && !empty($request['checkout2'])) {
                    $times[] = [
                        'checkin' => $timekeeping->time[1]['checkin'] ?? ['time' => $request['checkin2']],
                        'checkout' => $timekeeping->time[1]['checkout'] ?? ['time' => $request['checkout2']]
                    ];
                }
                $timekeeping->update([
                    'type' => $this->timekeeping->typeAddition(),
                    'times' => $times
                ]);
            }
            $requirement->update([
                'is_exec' => true,
                'timekeeping_id' => $timekeeping->id
            ]);
            $this->notification->create([
                'creator_id' => auth('api')->id(),
                'user_id' => $requirement->creator_id,
                'group_id' => $requirement->group_id,
                'requirement_id' => $id,
                'content' => $request->content,
                'type' => Notification::TYPE_17
            ]);
            return response()->json([
                'message' => __('word.success'),
            ]);
        }
    }

    public function denyAdditionalTimekeeping(Request $request, $id)
    {
        $requirement = $this->checkBelongsToGroup($id, $request->group_id);
        if (!$requirement->is_exec && $requirement->type == $this->requirement->getAdditionTimekeepingType()) {
            $request->validate([
                'content' => 'required'
            ]);
            $requirement->update(['is_exec' => true]);
            $this->notification->create([
                'creator_id' => auth('api')->id(),
                'user_id' => $requirement->creator_id,
                'group_id' => $requirement->group_id,
                'requirement_id' => $id,
                'content' => $request->content,
                'type' => Notification::TYPE_18
            ]);
            return response()->json([
                'message' => __('word.success')
            ]);
        }
    }

    public function acceptTakeLeave(Request $request, $id)
    {
        $requirement = $this->checkBelongsToGroup($id, $request->group_id);
        if (!$requirement->is_exec && $requirement->type == $this->requirement->getTakeLeaveType()) {
            $shift = $this->shift->getByIdsAndGroup($requirement->creator->shift_ids, $requirement->group_id);
            if (!$shift) {
                $request->validate(['shift_id' => 'required']);
                $shift = $this->checkShiftBelongsToGroup($request->shift_id, $request->group_id);
            }
            $this->validateRequest($request, $shift->type);
            $data = $this->prepareData($request, $requirement, $shift, $this->timekeeping->typeTakeLeave());
            $timekeeping = $this->timekeeping->create($data);
            $requirement->update([
                'is_exec' => true,
                'timekeeping_id' => $timekeeping->id
            ]);
            $this->notification->create([
                'creator_id' => auth('api')->id(),
                'user_id' => $requirement->creator_id,
                'group_id' => $requirement->group_id,
                'requirement_id' => $id,
                'content' => $request->content,
                'type' => Notification::TYPE_19
            ]);
            return response()->json([
                'message' => __('word.success')
            ]);
        }
    }

    public function denyTakeLeave(Request $request, $id)
    {
        $requirement = $this->checkBelongsToGroup($id, $request->group_id);
        if (!$requirement->is_exec && $requirement->type == $this->requirement->getTakeLeaveType()) {
            $request->validate([
                'content' => 'required'
            ]);
            $requirement->update(['is_exec' => true]);
            $this->notification->create([
                'creator_id' => auth('api')->id(),
                'user_id' => $requirement->creator_id,
                'group_id' => $request->group_id,
                'requirement_id' => $id,
                'content' => $request->content,
                'type' => Notification::TYPE_20
            ]);
            return response()->json([
                'message' => __('word.success')
            ]);
        }
    }

    public function acceptSwitchShift(Request $request, $id)
    {
        $requirement = $this->checkBelongsToGroup($id, $request->group_id);
        if (!$requirement->is_exec && $requirement->type == $this->requirement->getWitchShiftType()) {
            $request->validate([
                'content' => 'required',
            ]);
            $shift = $this->shift->getById($requirement->shift_id);
            $this->timekeeping->update($requirement->timekeeping_id, [
                'type' => $this->timekeeping->typeSwitch(),
                'shift_name' => $shift->name,
                'shift_type' => $shift->type,
                'target_time' => $shift->times,
                'shift_allow_time' => $shift->allow_time
            ]);
            $requirement->update(['is_exec' => true]);
            $this->notification->create([
                'creator_id' => auth('api')->id(),
                'user_id' => $requirement->creator_id,
                'group_id' => $requirement->group_id,
                'requirement_id' => $id,
                'content' => $request->content,
                'type' => Notification::TYPE_21
            ]);
            return response()->json([
                'message' => __('word.success')
            ]);
        }
    }

    public function denySwitchShift(Request $request, $id)
    {
        $requirement = $this->checkBelongsToGroup($id, $request->group_id);
        if (!$requirement->is_exec && $requirement->type == $this->requirement->getWitchShiftType()) {
            $request->validate([
                'content' => 'required',
            ]);
            $requirement->update(['is_exec' => true]);
            $this->notification->create([
                'creator_id' => auth('api')->id(),
                'user_id' => $requirement->creator_id,
                'group_id' => $requirement->group_id,
                'requirement_id' => $id,
                'content' => $request->content,
                'type' => Notification::TYPE_22
            ]);
            return response()->json([
                'message' => __('word.success')
            ]);
        }
    }

    public function checkBelongsToGroup(string $requirementId, string $groupId)
    {
        $requirement = $this->requirement->getById($requirementId, ['shift', 'creator:shift_ids']);
        $this->authorize('belongstoGroup', [$requirement, $groupId]);
        return $requirement;
    }

    public function checkShiftBelongsToGroup(string $shiftId, string $groupId)
    {
        $shift = $this->shift->getById($shiftId);
        $this->authorize('belongstoGroup', [$shift, $groupId]);
        return $shift;
    }

    private function validateRequest($request, $type)
    {
        $rules = [
            'checkin1' => 'required|string|date_format:H:i',
            'checkout1' => 'required|string|date_format:H:i|after:checkin1',
            'content' => 'required',
        ];
        if ($type == $this->shift->getMultiType()) {
            $rules['checkin2'] = 'string|required|date_format:H:i|after:checkout1';
            $rules['checkout2'] = 'string|required|date_format:H:i|after:checkin2';
        }
        $request->validate($rules);
    }

    private function prepareData($request, $requirement, $shift, int $type)
    {
        $data = [
            'user_id' => $requirement->creator_id,
            'group_id' => $requirement->group_id,
            'date' => $requirement->date,
            'type' => $type,
            'times' => [
                [
                    'checkin' => [
                        'time' => $request['checkin1']
                    ],
                    'checkout' => [
                        'time' => $request['checkout1']
                    ]
                ]
            ],
            'shift_name' => $shift->name,
            'shift_type' => $shift->type,
            'target_time' => $shift->times,
            'shift_allow_time' => $shift->allow_time
        ];
        // Trường hợp công ty 1 ngày chấm 4 lần
        if (!empty($request['checkin2']) && !empty($request['checkout2'])) {
            $data['times'][] = [
                'checkin' => [
                    'time' => $request['checkin2']
                ],
                'checkout' => [
                    'time' => $request['checkout2']
                ]
            ];
        }
        return $data;
    }
}
