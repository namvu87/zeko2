<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\CaptchaContract;

class CaptchaController extends Controller
{
    private $captcha;

    public function __construct(CaptchaContract $captcha)
    {
        $this->captcha = $captcha;
    }

    /**
     * @return
     */
    public function getCaptcha()
    {
        return $this->captcha->init();
    }
}
