<?php

namespace App\Http\Controllers\LandingPage;

use App\Contracts\Master\NewsContract;
use App\Contracts\Master\TagContract;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    private $news;
    private $tag;

    public function __construct(NewsContract $news, TagContract $tag)
    {
        $this->news = $news;
        $this->tag = $tag;
    }

    public function list($tagName = null)
    {
        if ($tagName) {
            $tag = $this->tag->getByName($tagName);
            if ($tag) {
                $newses = $this->news->getNewsByTagId($tag->id);
            } else {
                abort(404);
            }
        } else {
            $newses = $this->news->getAllNewNews();
        }
        return view('landing_pages.news.index', [
            'newses' => $newses,
            'desc' => __('tag.news.desc'),
            'title' => __('tag.news.title'),
        ]);
    }

    public function show($slug)
    {
        $news = $this->news->getBySlug($slug);
        if (!$news) {
            abort(404);
        }
        $newNewses = $this->news->getOtherPostsBySlug($slug);
        return view('landing_pages.news.show', [
            'news' => $news,
            'newNewses' => $newNewses
        ]);
    }
}