<?php

namespace App\Http\Controllers\LandingPage;

use App\Contracts\Master\ContactContract;
use App\Contracts\Master\NewsContract;
use App\Contracts\Master\SliderContract;
use App\Contracts\Master\TagContract;
use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Support\Facades\App;

class CommonController extends Controller
{
    private $slider;
    private $news;
    private $contact;
    private $tag;

    public function __construct(SliderContract $slider, NewsContract $news, ContactContract $contact, TagContract $tag)
    {
        $this->slider = $slider;
        $this->news = $news;
        $this->contact = $contact;
        $this->tag = $tag;
    }

    public function landingPage()
    {
        $sliders = $this->slider->getSliders();

        // Bài viết giới thiệu về phần mềm chấm công Zeko
        $newsIntroduction = $this->news->getUserGuide();

        // Bài viết thêm vào trang chủ của Zeko
        $newsAddHomePage = $this->news->getNewsByType(News::TYPE_4);;

        // Bài viết thường
        $newNewsList = $this->news->getNewPosts();
        return view('landing_pages.home', [
            'sliders' => $sliders,
            'newsIntroduction' => $newsIntroduction,
            'newsAddHomePage' => $newsAddHomePage,
            'newNewsList' => $newNewsList
        ]);
    }

    public function career($tag = null)
    {
        if ($tag) $careerNewses = $this->news->getCareerNewsByTag($tag);
        $careerNewses = $this->news->getCareerNews();

        return view('landing_pages.common.career', [
            'careerNewses' => $careerNewses,
            'title' => __('tag.career.title'),
            'desc' => __('tag.career.desc'),
        ]);
    }

    public function contact()
    {
        $contact = $this->contact->getContactInformation();
        $listCustomerCare = $this->contact->getCustomerCare();
        return view('landing_pages.common.contact', [
            'title' => __('tag.contact.title'),
            'desc' => __('tag.contact.desc'),
            'contact' => $contact ?? null,
            'listCustomerCare' => $listCustomerCare ?? null
        ]);
    }

    public function policyCompany()
    {
        $content = $this->news->getFirstNewsByType(News::TYPE_6);
        return view('landing_pages.common.web', [
            'content' => $content,
            'title' => __('tag.policy.title'),
            'desc' => __('tag.policy.desc')
        ]);
    }

    public function about()
    {
        $content = $this->news->getFirstNewsByType(News::TYPE_7);
        return view('landing_pages.common.web', [
            'content' => $content,
            'title' => __('tag.about.title'),
            'desc' => __('tag.about.desc')
        ]);
    }

    public function rules()
    {
        $content = $this->news->getFirstNewsByType(News::TYPE_8);
        return view('landing_pages.common.web', [
            'content' => $content,
            'title' => __('tag.rules.title'),
            'desc' => __('tag.rules.desc')
        ]);
    }

    public function costs()
    {
        $content = $this->news->getFirstNewsByType(News::TYPE_9);
        return view('landing_pages.common.web', [
            'content' => $content,
            'title' => __('tag.costs.title'),
            'desc' => __('tag.costs.desc')
        ]);
    }

    public function changeLanguage($language)
    {
        session(['website_language' => $language]);
        App::setLocale($language);
        return redirect()->back();
    }

    public function apiChangeLanguage($domain, $language)
    {
        session(['manage_language' => $language]);
        App::setLocale($language);
        return response()->json([
            'status' => config('response.success')
        ]);
    }
}
