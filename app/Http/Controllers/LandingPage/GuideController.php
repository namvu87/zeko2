<?php

namespace App\Http\Controllers\LandingPage;

use App\Contracts\Master\UserGuideContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GuideController extends Controller
{
    private $guide;

    public function __construct(UserGuideContract $guide)
    {
        $this->guide = $guide;
    }

    public function timekeeping()
    {
        $categories = $this->guide->getCategoryByService('1');
        return view('landing_pages.guides.index', ['categories' => $categories, 'service' => '1']);
    }

    public function restaurant()
    {
        $categories = $this->guide->getCategoryByService('2');
        return view('landing_pages.guides.index', ['categories' => $categories, 'service' => '2']);
    }

    public function detailGuideTimekeeping($slug)
    {
        $categories = $this->guide->getCategoryByService('1');
        $guide = $this->guide->getBySlug($slug, '1');
        if (!$guide) abort(404);
        $guides = $this->guide->getByCategory($guide->category, '1');
        return view('landing_pages.guides.detail', [
            'categories' => $categories,
            'guide' => $guide,
            'guides' => $guides,
            'service' => '1'
        ]);
    }

    public function detailGuideRestaurant($slug)
    {
        $categories = $this->guide->getCategoryByService('2');
        $guide = $this->guide->getBySlug($slug, '2');
        if (!$guide) abort(404);
        $guides = $this->guide->getByCategory($guide->category, '2');
        return view('landing_pages.guides.detail', [
            'categories' => $categories,
            'guide' => $guide,
            'guides' => $guides,
            'service' => '2'
        ]);
    }

    public function search(Request $request)
    {
        $guides = $this->guide->searchByTitle($request->title ?? '');
        return view('landing_pages.guides.search', [
           'guides' => $guides
        ]);
    }
}
