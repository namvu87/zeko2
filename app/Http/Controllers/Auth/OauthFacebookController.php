<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\OauthController;
use Socialite;
use Carbon\Carbon;

class OauthFacebookController extends OauthController
{
    const SCOPES = ['public_profile', 'email'];
    const FIELDS = ['first_name', 'last_name', 'email', 'middle_name'];
    const DRIVER = 'facebook';

    /**
     * @return Illuminate\Http\RedirectResponse
     */
    public function redirect()
    {
        return Socialite::driver(self::DRIVER)
            ->fields(self::FIELDS)
            ->scopes(self::SCOPES)
            ->redirect();
    }

    /**
     * @return array
     */
    public function handle(): array
    {
        $response = Socialite::driver(self::DRIVER)
            ->fields(self::FIELDS)
            ->stateless()
            ->user();
        if (empty($response->email)) {
            throw new \Exception(__('validation.required', ['attribute' => 'email']));
        }
        return [
            'email' => $response->email,
            'first_name' => ($response->user['middle_name'] ?? '') . ' ' .
                ($response->user['first_name'] ?? ''),
            'last_name' => $response->user['last_name'] ?? '',
            'avatars'     => [
                'x1' => "https://graph.facebook.com/{$response->id}/picture?width=32&height=32",
                'x2' => "https://graph.facebook.com/{$response->id}/picture?width=160&height=160",
                'x3' => "https://graph.facebook.com/{$response->id}/picture?width=576&height=576"
            ],
            'social' => [
                'facebook' => [
                    'id' => $response->id,
                    'access_token' => $response->token
                ]
            ],
            'email_verified_at' => Carbon::now()->toDateTimeString()
        ];
    }
}
