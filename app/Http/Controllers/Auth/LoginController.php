<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\UserBase as UserResource;
use App\Contracts\Users\Firebase;
use DB;

class LoginController extends Controller
{
    private $firebase;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Firebase $firebase)
    {
        $this->middleware('guest')->except('logout');
        $this->firebase = $firebase;
    }

    /**
     * @param  Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        $payload = [
            'grant_type' => 'password',
            'client_id' => config('passport.password_grant.client_id'),
            'client_secret' => config('passport.password_grant.secret'),
            'username' => request('username'),
            'password' => request('password'),
            'member_groups' => true
        ];

        $loginRequest = Request::create(config('app.url') . '/oauth/token', 'POST', $payload);
        $response = app()->handle($loginRequest);

        if ($response->getStatusCode() !== 200) {
            return response()->json([
                'message' => trans('login.message.auth_fail')
            ], 401);
        }

        $data = json_decode($response->getContent());
        $user = (new User)->findForPassport(request('username'));
        return response()->json([
            'access_token' => $data->access_token,
            'user' => new UserResource($user)
        ]);
    }

    /**
     * Đăng xuất người dùng
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $accessToken = auth('api')->user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);
        $accessToken->revoke();

        return response()->json([
            'message' => 'ok'
        ]);
    }
}
