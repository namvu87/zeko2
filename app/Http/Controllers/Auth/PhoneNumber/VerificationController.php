<?php

namespace App\Http\Controllers\Auth\PhoneNumber;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Traits\EsmsHelper;

class VerificationController extends Controller
{
    use EsmsHelper;

    public function __construct()
    {
        $this->middleware('throttle:5,60');
    }

    public function init(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'phone_number' => [
                'nullable', 'numeric',
                Rule::unique('users')->ignore($user->id, '_id')
            ]
        ]);

        if (empty($user->phone_number) && !$request->phone_number) {
            return response()->json([
                'message' => __('esms.update_your_phone_number')
            ], 403);
        }
        $phoneNumber = $user->phone_number ?? $request->phone_number;

        $code = rand(100000, 999999);
        try {
            $response = $this->sendSms($phoneNumber, $code);
            $response = json_decode($response->getBody(), true);

            if ($response['CodeResult'] != 100) {
                throw new \Exception(__('esms.error_message'));
            }

            $user->phone_number_verify = [
                'phone_number' => $request->phone_number,
                'code' => $code,
                'expired_at' => Carbon::now()->toDateTimeString()
            ];
            $user->save();
            return response()->json([
                'message' => __('esms.verify_phone_number')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => __('esms.error_message')
            ], 500);
        }
    }
}
