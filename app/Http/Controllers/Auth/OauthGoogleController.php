<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\OauthController;
use Socialite;
use Carbon\Carbon;

class OauthGoogleController extends OauthController
{
    const DRIVER = 'google';

    /**
     * @return Illuminate\Http\RedirectResponse
     */
    public function redirect()
    {
        return Socialite::driver(self::DRIVER)->redirect();
    }

    /**
     * @return array
     */
    public function handle(): array
    {
        $response = Socialite::driver(self::DRIVER)->stateless()->user();

        if (empty($response->email)) {
            throw new \Exception(__('validation.required', ['attribute' => 'email']));
        }

        return [
            'email' => $response->email,
            'first_name' => $response->user['given_name'] ?? '',
            'last_name' => $response->user['family_name'] ?? '',
            'avatars'     => [
                'x1' => $response->avatar . '?sz=32',
                'x2' => $response->avatar . '?sz=160',
                'x3' => $response->avatar . '?sz=576',
            ],
            'social' => [
                'google' => [
                    'id' => $response->id,
                    'access_token' => $response->token
                ]
            ],
            'email_verified_at' => Carbon::now()->toDateTimeString()
        ];
    }
}
