<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;
use App\Contracts\UserContract;

class VerificationController extends Controller
{
    // use VerifiesEmails;

    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserContract $user)
    {
        $this->middleware('auth:api')->except('verify');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
        $this->user = $user;
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {
        $user = $this->user->getById($request->id);

        if ($request->route('id') != $user->getKey()) {
            return response()->json([
                'message' => __('mail.verify_email.fail_message')
            ], 403);
        }

        if ($user->hasVerifiedEmail()) {
            return response()->json([
                'message' => __('mail.verify_email.verified')
            ], 400);
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        return response()->json([
            'message' => __('mail.verify_email.success_message')
        ]);
    }

    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json([
                'message' => __('mail.verify_email.verified')
            ], 400);
        }

        $request->user()->sendEmailVerificationNotification();

        return response()->json([
            'message' => __('mail.verify_email.sent')
        ]);
    }
}
