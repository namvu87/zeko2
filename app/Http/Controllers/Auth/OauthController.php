<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\UserContract;
use App\Contracts\AuthContract;
use App\Http\Resources\UserBase as UserResource;
use Exception;

abstract class OauthController extends Controller
{
    private $user;
    private $auth;

    public function __construct(UserContract $user, AuthContract $auth)
    {
        $this->user = $user;
        $this->auth = $auth;
    }

    /**
     * @return Illuminate\Http\RedirectResponse
     */
    abstract protected function redirect();

    /**
     * @return \Illuminate\Http\Response
     */
    abstract protected function handle() :array;

    /**
     * Xác thực cho người dùng, trả về access token và thông tin user
     *
     * @return \Illuminate\Http\Response
     */
    protected function callback()
    {
        try {
            $data = $this->handle();
        } catch (Exception $e) {
            abort(403);
        }

        $user = $this->user->findForPassport($data['email']);
        if (empty($user)) {
            $user = $this->auth->regist($data);
        }

        return view('auth.oauth', [
            'token' => $user->createToken('access token')->accessToken,
            'user' => new UserResource($user)
        ]);
    }
}
