<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Area as AreaResource;
use App\Http\Resources\Commune as CommuneResource;
use App\Contracts\AreaContract;
use App\Contracts\CommuneContract;

class CommonController extends Controller
{
    private $area;
    private $commune;

    public function __construct(AreaContract $area, CommuneContract $commune)
    {
        $this->area = $area;
        $this->commune = $commune;
    }

    /**
     * get area by keyword
     *
     * @param  Request $request
     *
     * @return App\Models\Area
     */
    public function getAreas()
    {
        return response()->json([
            'areas' => AreaResource::collection($this->area->getAll())
        ]);
    }

    /**
     * get all areas and communes by area id
     *
     * @param $areaId
     */
    public function getAreasAndCommuneByAreaById($areaId)
    {
        return response()->json([
            'areas' => AreaResource::collection($this->area->getAll()),
            'communes' => CommuneResource::collection($this->commune->getAllByAreaId($areaId))
        ]);
    }

    /**
     * get commune via relationship
     *
     * @param  string $areaId
     *
     * @return App\Models\Commune
     */
    public function getCommunes(Request $request, $areaId)
    {
        return response()->json([
            'communes' => CommuneResource::collection($this->commune->getAllByAreaId($areaId))
        ]);
    }
}
