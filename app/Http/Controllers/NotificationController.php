<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\NotificationContract;
use App\Http\Resources\NotificationCollection;
use App\Models\Notification;

class NotificationController extends Controller
{
    private $notification;

    public function __construct(NotificationContract $notification)
    {
        $this->notification = $notification;
    }

    public function getUserNotifications(Request $request)
    {
        return response()->json([
            'notifications' => new NotificationCollection($this->notification->getUserNotifications($request->all()))
        ]);
    }

    public function active(Request $request, $id)
    {
        $this->notification->update($id, ['view' => true]);
    }

    public function reset()
    {
        auth('api')->user()->update([
            'notify_count' => 0
        ]);
        return;
    }

    public function detail($id)
    {
        $notification = $this->notification->getById($id, ['creator:firstname_last_name', 'shift', 'requirement.timekeeping']);
        return response()->json([
            'notification' => $notification
        ]);
    }
}