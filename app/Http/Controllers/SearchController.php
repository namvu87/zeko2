<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\Searchable;
use App\Http\Resources\RestaurantCollection;

class SearchController extends Controller
{
    private $search;

    public function __construct(Searchable $search)
    {
        $this->search = $search;
    }

    /**
     * Tìm kiếm nhà hàng theo vị trí người dùng
     *
     * @param  Request $request
     * @return JSON
     */
    public function findAround(Request $request)
    {
        $restaurant = $this->search->findAround($request->longitude, $request->latitude, $request->page);
        return new RestaurantCollection($restaurant);
    }
}
