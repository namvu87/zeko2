<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\UserContract;
use App\Contracts\GroupContract;
use App\Contracts\ShiftContract;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Group as GroupResource;
use App\Http\Resources\Shift as ShiftResource;

class UserController extends Controller
{
    private $user;
    private $group;
    private $shift;

    public function __construct(UserContract $user, GroupContract $group, ShiftContract $shift)
    {
        $this->user  = $user;
        $this->group = $group;
        $this->shift = $shift;
    }

    /**
     * Get current user
     *
     * @param  Request $request
     * @return json App\Models\User
     */
    public function getUser(Request $request)
    {
        return response()->json([
            'user' => new UserResource($this->user->getUser())
        ]);
    }

    /**
     * Get all group of current user
     *
     * @param  Request $request
     * @return json App\Models\Group
     */
    public function groupsList(Request $request)
    {
        $user = $this->user->getUser();

        return response()->json([
            'groups' => GroupResource::collection($this->group->getByIds($user->group_ids))
        ]);
    }

    public function shiftsList(Request $request)
    {
        $user = $this->user->getUser();

        return response()->json([
            'shifts' => ShiftResource::collection($this->shift->getByIds($user->shift_ids))
        ]);
    }
}
