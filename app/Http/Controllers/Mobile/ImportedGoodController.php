<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\Restaurant\GoodImportedTrait;
use App\Http\Controllers\Traits\Restaurant\MobileGoodTrait;
use App\Contracts\Restaurant\GoodContract;

class ImportedGoodController extends Controller
{
    use MobileGoodTrait, GoodImportedTrait;

    private $good;
    const SUCCESS_STATUS = 200;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    /**
     * Cooking request fot normally
     *
     * @param  Request $request
     * @return array
     */
    private function prepareRequest(Request $request)
    {
        $properties = [];
        $conversionUnits = [];

        foreach (json_decode($request->properties) as $property) {
            array_push($properties, (array) $property);
        }
        foreach (json_decode($request->conversion_units) as $conversion) {
            array_push($conversionUnits, (array) $conversion);
        }

        $request['conversion_units'] = $conversionUnits;
        $request['properties'] = $properties;
        $request['group_menu_ids'] = json_decode($request->group_menu_ids);

        return $request;
    }
}
