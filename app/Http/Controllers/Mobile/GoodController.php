<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\Restaurant\GoodImportedTrait;
use App\Contracts\Restaurant\GoodContract;
use Illuminate\Support\Facades\Validator;

class GoodController extends Controller
{
    use GoodImportedTrait;

    private $good;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    /**
     * update single good image
     *
     * @param  Request $request
     * @return array
     */
    public function updateImage(Request $request)
    {
        $this->validate($request, [
            'group_id' => 'required|string',
            'good_id'  => 'required|string',
            'image'    => 'required|mimes:jpeg,png,jpg|max:5120'
        ]);
        $good = $this->good->getById($request->good_id);
        $this->authorize('belongsToGroup', [$good, $request->group_id]);

        $imagePath = $this->good->saveImage($request->image, $request->group_id);
        $images = $good->images ?? [];
        $images[] = $imagePath;

        $this->good->update($request->good_id, [
            'images' => $images
        ]);
        return ['image_url' => $imagePath];
    }
}


