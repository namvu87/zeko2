<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\Restaurant\GoodProcessedTrait;
use App\Http\Controllers\Traits\Restaurant\MobileGoodTrait;
use App\Contracts\Restaurant\GoodContract;

class ProcessedGoodController extends Controller
{
    use MobileGoodTrait, GoodProcessedTrait;

    private $good;
    const SUCCESS_STATUS = 200;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }

    /**
     * Cooking request fot normally
     *
     * @param  Request $request
     * @return array
     */
    private function prepareRequest(Request $request)
    {
        $properties = [];
        $conversionUnits = [];
        $ingredients = [];

        foreach (json_decode($request->properties) as $property) {
            array_push($properties, (array) $property);
        }
        foreach (json_decode($request->conversion_units) as $conversion) {
            array_push($conversionUnits, (array) $conversion);
        }
        foreach (json_decode($request->ingredients) as $ingredient) {
            array_push($ingredients, (array) $ingredient);
        }

        $request['conversion_units'] = $conversionUnits;
        $request['properties'] = $properties;
        $request['ingredients'] = $ingredients;
        $request['group_menu_ids'] = json_decode($request->group_menu_ids);

        return $request;
    }
}
