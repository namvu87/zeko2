<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\GroupContract;
use App\Contracts\UserContract;
use App\Contracts\ImageContract;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Group as GroupResource;
use Arr;

class GroupController extends Controller
{
    const SIZE = [
        'width' => 768,
        'height' => 512
    ];
    const VISIBILITY = 'public';

    private $group;
    private $user;
    private $image;

    public function __construct(GroupContract $group, UserContract $user, ImageContract $image)
    {
        $this->group = $group;
        $this->user = $user;
        $this->image = $image;
    }

    /**
     * upload group's cover picture
     *
     * @param Request $request
     * @return json
     */
    public function updateCoverPicture(Request $request)
    {
        $this->validate($request, [
            'group_id' => 'required|string',
            'image' => 'required|mimes:jpeg,png,jpg|max:8192'
        ]);
        $group = $this->group->getById($request->group_id);
        $oldImage = $group->cover_image;

        $filename = "group_cover_images/{$request->group_id}_" . time() . ".png";
        $source = (string) $this->image
                                ->setImage($request->image)
                                ->setSize(self::SIZE['width'], self::SIZE['height'])
                                ->resize();
        Storage::put($filename, $source, self::VISIBILITY);

        $group = $this->group->save($group, [
            'cover_image' => $filename
        ]);
        Storage::delete($oldImage);
        
        return response()->json([
            'group' => new GroupResource($group)
        ]);
    }

    /**
     * [Deprecated]
     * initialize a group
     * 
     * @param  Request $request
     * @return json
     */
    public function create(Request $request)
    {
        $request->validate($this->rules());

        $data = [
            'name' => $request->name,
            'area' => $request->area,
            'commune' => $request->commune,
            'address' => $request->address,
            'service_type' => $request->service_type,
            'owner_id' => auth('api')->id(),
        ];

        $data['owner_id'] = auth('api')->id();

        $group = $this->group->create($data);
        $this->user->addUserToGroup($this->user->id(), $group->id);

        return response()->json([
            'group' => $group,
            'permissions' => $this->getUserPermissions($group->id),
            'message' => __('common.init_success')
        ]);
    }

    public function deleteProperty(Request $request)
    {
        $group = $this->group->getById($request->group_id);
        $group->pull('goods_properties', $request->property);
        return response()->json([
            'message' => __('restaurant.good.update_goods_property_success')
        ]);
    }

    /**
     * [Deprecated]
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        $request->validate(Arr::except($this->rules(), ['service_type']));

        $this->group->update($request->group_id, $request->only('name', 'area', 'commune', 'address'));

        return response()->json([
            'message' => __('common.update_group_success')
        ]);
    }

    /**
     * [Deprecated]
     *
     * @param Request $request
     * @return void
     */
    public function updateUnits(Request $request)
    {
        $request->validate([
            'units' => 'required|array',
            'units.*' => 'string'
        ]);
        $this->group->update($request->group_id, [
            'units' => $request->units
        ]);
        return response()->json([
            'message' => __('word.success')
        ]);
    }

    /**
     * [Deprecated]
     * Get permission in group of current user
     *
     * @param  Request $request
     * @return json
     */
    public function change(Request $request)
    {
        return response()->json([
            'permissions' => $this->getUserPermissions($request->group_id)
        ]);
    }

    /**
     * [Deprecated]
     * rules of validation create, update group
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'service_type' => 'required|numeric|in:' . implode(',', $this->group->getServicesType()),
            'area' => 'required|array',
            'area.*' => 'required|string|max:128',
            'area.name' => 'exists:areas,name',
            'commune' => 'required|array',
            'commune.*' => 'required|string|max:128',
            'commune.name' => 'exists:communes,name',
            'address' => 'required|string|max:255'
        ];
    }

    /**
     * [Deprecated]
     * Gets the user permissions.
     *
     * @param  string  $groupId  The group identifier
     *
     * @return array  The user permissions.
     */
    public function getUserPermissions(string $groupId)
    {
        $user = $this->user->getUser();
        $permissions = $user->getPermissionsViaRoles()->where('group_id', $groupId);
        return $permissions->pluck('name');
    }
}
