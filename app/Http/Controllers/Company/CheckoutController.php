<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Contracts\UserContract;
use App\Contracts\ShiftContract;
use App\Contracts\TimekeepingContract;
use Exception;
use App\Http\Controllers\Company\AbstractCreateTimekeepingController;

class CheckoutController extends AbstractCreateTimekeepingController
{
    const VISIBILITY_UPLOAD = 'public';
    const UNAUTHORIZED_STATUS = 403;
    const SUCCESS_STATUS = 200;

    public function __construct(UserContract $user, ShiftContract $shift,
                                TimekeepingContract $timekeeping) {
        parent::__construct($user, $shift, $timekeeping);
    }

    /**
     * @param  array $data
     * @param  array $time
     * @param  App\Models\Shift $shift
     * @param  App\Models\TimeKeeping $timekeeping
     * @return App\Models\TimeKeeping
     */
    public function create(array $data, array $time, $shift, $timekeeping)
    {
        if (strtotime($time['time']) <= strtotime($shift->times[0]['checkin'])) {
            throw new Exception(__('time_keeping.over_time_checkout'));
        }
        if (empty($timekeeping)) {
            return $this->handleCheckoutCreate($data, $time, $shift);
        } else {
            return $this->handleCheckoutUpdate($time, $shift, $timekeeping);
        }
    }

    /**
     * @param  array $data
     * @param  array $time
     * @param  App\Models\Shift $shift
     * @return App\Models\TimeKeeping
     */
    private function handleCheckoutCreate($data, $time, $shift)
    {
        if ($shift->type == $this->shift->getSingleType()) {
            $data['times'][0]['checkout'] = $time;
        } else {
            if (strtotime($time['time']) <= strtotime($shift->times[1]['checkin'])) {
                $data['times'][0]['checkout'] = $time;
            } else {
                $data['times'][1]['checkout'] = $time;
            }
        }
        return $this->timekeeping->create($data);
    }

    /**
     * @param  array $time
     * @param  array $shift
     * @param  App\Models\TimeKeeping $timekeeping
     * @return App\Models\TimeKeeping
     */
    private function handleCheckoutUpdate($time, $shift, $timekeeping)
    {
        $data = $timekeeping->times;
        if ($shift->type == $this->shift->getSingleType()) {
            if (!empty($data[0]['checkout'])) {
                throw new Exception(__('time_keeping.already_checkout'));
            }
            $data[0]['checkout'] = $time;
        } else {
            if (strtotime($time['time']) <= strtotime($shift->times[1]['checkin'])) {
                if (!empty($data[0]['checkout'])) {
                    throw new Exception(__('time_keeping.already_checkout'));
                }
                $data[0]['checkout'] = $time;
            } else {
                if (!empty($data[1]['checkout'])) {
                    throw new Exception(__('time_keeping.already_checkout'));
                }
                $data[1]['checkout'] = $time;
            }
        }
        $timekeeping->times = $data;
        $timekeeping->save();
        return $timekeeping;
    }
}
