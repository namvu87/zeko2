<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\TimekeepingContract;
use App\Contracts\UserContract;
use App\Contracts\ShiftContract;
use Illuminate\Support\Facades\Storage;
use Illuminate\Auth\Access\AuthorizationException;

abstract class AbstractCreateTimekeepingController extends Controller
{
    const UNAUTHORIZED_STATUS = 403;
    const SUCCESS_STATUS = 200;
    const VISIBILITY_UPLOAD = 'public';

    protected $timekeeping;
    protected $user;
    protected $shift;

    public function __construct(
        UserContract $user,
        ShiftContract $shift,
        TimekeepingContract $timekeeping
    ) {
        $this->user = $user;
        $this->shift = $shift;
        $this->timekeeping = $timekeeping;
    }

    /**
     * Chấm công
     * @param  Request $request
     * @return json
     */
    public function timekeeping(Request $request)
    {
        $this->validate($request, [
            'group_id'  => 'required|string',
            'location'  => 'required',
            'image'     => 'nullable|mimes:jpeg,png,jpg|max:2048'
        ]);

        $user = $this->user->getUser();
        $this->authorize('belongsToGroup', [$user, $request->group_id]);

        $shifts = $this->shift->getByIds($user->shift_ids);

        $shift = $shifts->where('group_id', $request->group_id)->first();
        if (!$shift) {
            throw new AuthorizationException(__('company.shift.not_exists_shift'));
        }
        $timekeeping = $this->timekeeping->getTimekeepingToday($user->id, $request->group_id);
        $currentTime = date('H:i');
        $data = [
            'date'     => date('Y-m-d'),
            'group_id' => $request->group_id,
            'user_id'  => $user->id,
            'shift_name' => $shift->name,
            'shift_type' => $shift->type,
            'target_time' => $shift->times
        ];

        if (!empty($request->image)) {
            $photoUrl = $this->saveImage($request->group_id, $user->id, $request->image);
        }

        $time = [
            'time'     => $currentTime,
            'location' => json_decode($request->location),
            'photo_url' => $photoUrl ?? null
        ];

        try {
            $this->create($data, $time, $shift, $timekeeping);
        } catch (\Exception $e) {
            if (!empty($request->image)) {
                $this->removeImage($photoUrl);
            }
            return response()->json([
                'code' => self::UNAUTHORIZED_STATUS,
                'message' => $e->getMessage()
            ]);
        }
        return response()->json([
            'code' => self::SUCCESS_STATUS
        ]);
    }

    /**
     * @param  array  $data
     * @param  string $time
     * @param  App\Models\Shift $shift
     * @param  App\Models\Timekeeping $timekeeping
     * @return App\Models\Timekeeping
     */
    abstract protected function create(array $data, array $time, $shift, $timekeeping);

    private function saveImage($groupId, $userId, $image)
    {
        $filename = "timekeepings/{$groupId}/{$userId}_" . time() . ".png";
        Storage::put($filename, file_get_contents($image), self::VISIBILITY_UPLOAD);
        return $filename;
    }

    private function removeImage($filename)
    {
        Storage::delete($filename);
    }
}
