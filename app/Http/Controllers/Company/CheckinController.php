<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Company\AbstractCreateTimekeepingController;
use Illuminate\Support\Facades\Storage;
use App\Contracts\UserContract;
use App\Contracts\ShiftContract;
use App\Contracts\TimekeepingContract;
use Exception;

class CheckinController extends AbstractCreateTimekeepingController
{
    const VISIBILITY_UPLOAD = 'public';
    const UNAUTHORIZED_STATUS = 403;
    const SUCCESS_STATUS = 200;

    public function __construct(
        UserContract $user,
        ShiftContract $shift,
        TimekeepingContract $timekeeping
    ) {
        parent::__construct($user, $shift, $timekeeping);
    }

    /**
     * @param  array $data
     * @param  array $time
     * @param  App\Models\Shift $shift
     * @param  App\Models\TimeKeeping $timeKeeping
     * 
     * @return App\Models\TimeKeeping
     */
    public function create(array $data, array $time, $shift, $timekeeping)
    {
        if ($shift->type === $this->shift->getSingleType()) {
            if (strtotime($time['time']) >= strtotime($shift->times[0]['checkout'])) {
                throw new Exception(__('time_keeping.over_time_checkin'));
            }
        } else {
            if (strtotime($time['time']) >= strtotime($shift->times[1]['checkout'])) {
                throw new Exception(__('time_keeping.over_time_checkin'));
            }
        }

        if (empty($timekeeping)) {
            return $this->handleCheckinCreate($data, $time, $shift);
        } else {
            return $this->handleCheckinUpdate($time, $shift, $timekeeping);
        }
    }

    /**
     * @param  array $data
     * @param  array $time
     * @param  App\Models\Shift $shift
     * @return App\Models\TimeKeeping
     */
    private function handleCheckinCreate($data, $time, $shift)
    {
        if ($shift->type == $this->shift->getSingleType()) {
            $data['times'][0]['checkin'] = $time;
        } else {
            if (strtotime($time['times']) <= strtotime($shift->times[0]['checkout'])) {
                $data['times'][0]['checkin'] = $time;
            } else {
                $data['times'][1]['checkin'] = $time;
            }
        }
        return $this->timekeeping->create($data);
    }

    /**
     * @param  array $time
     * @param  array $shift
     * @param  App\Models\TimeKeeping $timekeeping
     * @return App\Models\TimeKeeping
     */
    private function handleCheckinUpdate($time, $shift, $timekeeping)
    {
        $data = $timekeeping->times;
        if ($shift->type === $this->shift->getSingleType()) {
            if (!empty($data[0]['checkin'])) {
                throw new Exception(__('time_keeping.already_checkin'));
            }
            $data[0]['checkin'] = $time;
        } else {
            if (strtotime($time['time']) <= strtotime($shift->times[0]['checkout'])) {
                if (!empty($data[0]['checkin'])) {
                    throw new Exception(__('time_keeping.already_checkin'));
                }
                $data[0]['checkin'] = $time;
            } else {
                if (!empty($data[1]['checkin'])) {
                    throw new Exception(__('time_keeping.already_checkin'));
                }
                $data[1]['checkin'] = $time;
            }
        }
        $timekeeping->times = $data;
        $timekeeping->save();
        return $timekeeping;
    }
}
