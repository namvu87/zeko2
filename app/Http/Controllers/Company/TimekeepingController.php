<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\TimekeepingContract;
use App\Contracts\UserContract;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Timekeeping as TimekeepingResource;
use App\Http\Resources\TimekeepingCollection;
use Carbon\CarbonPeriod;
use Carbon\Carbon;

class TimekeepingController extends Controller
{
    const MAX_DATE_REPORT = 62;

    protected $timekeeping;
    protected $user;

    public function __construct(UserContract $user, TimekeepingContract $timekeeping)
    {
        $this->user = $user;
        $this->timekeeping = $timekeeping;
    }

    /**
     * Danh sách dữ liệu chấm công
     *
     * @param  Request $request
     * @return json
     */
    public function list(Request $request)
    {
        $timekeepings = $this->timekeeping->getByCondition($request->all());

        return response()->json([
            'timekeepings' => new TimekeepingCollection($timekeepings)
        ]);
    }

    /**
     * Hiển thị thông tin chi tiết về 1 dữ liệu chấm công
     *
     * @param  Request $request
     * @return json
     */
    public function detail(Request $request, $id)
    {
        $timekeeping = $this->timekeeping->getById($id);
        $this->authorize('belongsToGroup', [$timekeeping, $request->group_id]);

        return response()->json([
            'timekeeping' => new TimekeepingResource($timekeeping)
        ]);
    }


    public function report(Request $request)
    {
        list($startDate, $endDate) = $this->getTime($request);

        $period = CarbonPeriod::create($startDate, $endDate);

        if ($period->count() > self::MAX_DATE_REPORT) {
            return response()->json([
                'message' => __('time_keeping.over_max_date_report')
            ], 402);
        }

//        $users = $this->user->getByGroupIdWithTimekeeping($request->group_id, $startDate, $endDate);
        $timekeepings = $this->timekeeping->groupByUserIdInGroup($request->group_id, $startDate, $endDate);

        return response()->json([
            'timekeepings' => $timekeepings,
            'period' => $period->toArray()
        ]);
    }

    public function reportDetail(Request $request, string $userId)
    {
        $user = $this->user->getById($userId);
        $this->authorize('belongsToGroup', [$user, $request->group_id]);

        list($startDate, $endDate) = $this->getTime($request);

        $period = CarbonPeriod::create($startDate, $endDate);
        if ($period->count() > self::MAX_DATE_REPORT) {
            return response()->json([
                'message' => __('time_keeping.over_max_date_report')
            ], 402);
        }

        $timeKeepings = $this->timekeeping->getByUserIdInGroup($user->id, $request->group_id, $startDate, $endDate);

        return response()->json([
            'user' => new UserResource($user),
            'period' => $period->toArray(),
            'time_keepings' => $timeKeepings
        ]);
    }

    /**
     * Lấy startDate và endDate từ request
     *
     * @param  Request $request
     * @return array
     */
    private function getTime(Request $request) :array
    {
        if (empty($request->month_year)) {
            return $this->convertDate($request);
        }
        return $this->convertMonthYear($request);
    }

    /**
     * Lấy startDate và endDate trong trường hợp người dùng chọn mốc thời gian
     *
     * @param  Request $request
     * @return array
     */
    private function convertDate(Request $request) :array
    {
        $this->validate($request, [
            'start_date' => 'nullable|date_format:Y-m-d|before:now',
            'end_date' => 'nullable|date_format:Y-m-d|after:start_date',
        ]);

        $startDate = !empty($request->start_date) ? $request->start_date : (new Carbon('first day of this month'))->format('Y-m-d');
        $endDate = !empty($request->end_date) ? $request->end_date : (new Carbon('last day of this month'))->format('Y-m-d');

        return [$startDate, $endDate];
    }

    /**
     * Lấy startDate và endDate trong trường hợp người dùng chọn khoảng thời gian
     *
     * @param  Request $request
     * @return array
     */
    private function convertMonthYear(Request $request) :array  {
        $this->validate($request, [
            'month_year' => 'nullable|date_format:Y-m'
        ]);

        $time = Carbon::createFromFormat('Y-m', $request->month_year);
        $month = config('datetime.month')[$time->month];
        $year = $time->year;

        $startDate = (new Carbon("first day of {$month} {$year}"))->format('Y-m-d');
        $endDate = (new Carbon("last day of {$month} {$year}"))->format('Y-m-d');

        return [$startDate, $endDate];
    }
}
