<?php

namespace App\Http\Controllers\Company;

use App\Contracts\NotificationContract;
use App\Contracts\RoleContract;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\GroupContract;
use App\Contracts\UserContract;
use App\Contracts\Users\Searchable;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;
use App\Contracts\Users\Role as UserRoleable;

class UserController extends Controller
{
    private $group;
    private $user;
    private $search;
    private $role;
    private $userRole;
    private $notification;

    public function __construct(UserContract $user, GroupContract $group, Searchable $search, RoleContract $role, UserRoleable $userRole, NotificationContract $notification)
    {
        $this->user = $user;
        $this->group = $group;
        $this->search = $search;
        $this->role = $role;
        $this->userRole = $userRole;
        $this->notification = $notification;
    }

    /**
     * @return App\Models\User
     */
    public function list(Request $request)
    {
        return response()->json([
            'users' => new UserCollection($this->user->paginateByGroupId($request->group_id, $request->page))
        ]);
    }

    /**
     * Get user in group by keyword
     *
     * @param  Request $request
     *
     * @return App\Models\User
     */
    public function search(Request $request)
    {
        $users = $this->search->getByGroupId($request->keyword ?? '', $request->group_id, $request->page);
        return response()->json([
            'users' => new UserCollection($users)
        ]);
    }

    /**
     * Lấy tất cả user trong group mà chưa có vai trò theo keyword
     *
     * @param  Request $request
     *
     * @return json App\Models\User
     */
    public function roleOut(Request $request)
    {
        $users = $this->search->getOutRole($request->keyword, $request->group_id);

        return response()->json([
            'users' => UserResource::collection($users)
        ]);
    }


    /**
     * Get an user via phone number to add employee list
     *
     * @param  Request $request
     *
     * @return \App\Models\User
     */
    public function getByPhoneNumber(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|numeric'
        ]);
        return response()->json([
            'user' => new UserResource($this->user->findForPassport($request->phone))
        ]);
    }

    /**
     * @param  string $userId
     *
     * @return App\Models\User
     */
    public function show(string $userId)
    {
        return response()->json([
            'user' => new UserResource($this->user->getById($userId))
        ]);
    }

    /**
     * Push user to group
     *
     * @param Request $request
     *
     * @return json
     */
    public function add(Request $request)
    {
        $request->validate($this->rules());

        $this->user->addUserToGroup($request->user_id, $request->group_id);
        $this->notification->create([
            'creator_id' => auth('api')->id(),
            'user_id' => $request->user_id,
            'group_id' => $request->group_id,
            'type' => Notification::TYPE_10,
        ]);
        return response()->json([
            'message' => __('company.user.add_user_success')
        ]);
    }

    /**
     * Remove user from group
     *
     * @param  Request $request
     *
     * @return json
     */
    public function pull(Request $request)
    {
        $request->validate($this->rules());
        $this->checkIsNotOwner($request->user_id, $request->group_id);
                $this->user->removeUserFromGroup($request->user_id, $request->group_id);
        $user = $this->user->getById($request->user_id);
        $role = $this->role->getRoleUserByGroupId($request->user_id, $request->group_id);
        if ($role) {
            $this->userRole->revoke($user, $role);
        }
        $this->notification->create([
            'creator_id' => auth('api')->id(),
            'user_id' => $request->user_id,
            'group_id' => $request->group_id,
            'type' => Notification::TYPE_11,
        ]);
        return response()->json([
            'message' => __('company.user.delete_user_success')
        ]);
    }

    /**
     * rules for validation add & pull methods
     *
     * @param  array $request
     *
     * @return array
     */
    private function rules()
    {
        return [
            'user_id' => 'required|string'
        ];
    }

    private function checkIsNotOwner(string $userId, string $groupId)
    {
        $group = $this->group->getById($groupId);
        $this->authorize('isNotOwner', [$group, $userId]);
    }
}
