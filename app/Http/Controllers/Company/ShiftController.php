<?php

namespace App\Http\Controllers\Company;

use App\Contracts\NotificationContract;
use App\Http\Requests\ShiftRequest;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\UserContract;
use App\Contracts\ShiftContract;
use App\Contracts\Users\Searchable;
use App\Http\Resources\Shift as ShiftResource;
use App\Http\Resources\UserCollection;

class ShiftController extends Controller
{
    private $shift;
    private $user;
    private $search;
    private $notification;

    public function __construct(ShiftContract $shift, UserContract $user, Searchable $search, NotificationContract $notification)
    {
        $this->shift = $shift;
        $this->user = $user;
        $this->search = $search;
        $this->notification = $notification;
    }

    /**
     * Get Group's shift list
     *
     * @return json
     */
    public function list(Request $request)
    {
        $shifts = $this->shift->getByGroupId($request->group_id);

        return response()->json([
            'shifts' => ShiftResource::collection($shifts)
        ]);
    }

    /**
     * Create a shift
     *
     * @param  ShiftRequest $request
     *
     * @return json
     */
    public function store(ShiftRequest $request)
    {
        $data = $this->prepareData($request);
        $this->shift->create($data);

        return response()->json([
            'message' => __('shifts.infoShift.success')
        ]);
    }

    /**
     * Get shift info
     *
     * @param  string $id
     *
     * @return json
     */
    public function edit(Request $request, string $id)
    {
        $this->checkShiftBelongsToGroup($id, $request->group_id);
        $shift = $this->shift->getById($id);

        return response()->json([
            'shift' => new ShiftResource($shift)
        ]);
    }

    /**
     * @param  ShiftRequest $request
     * @param  string $id
     *
     * @return [type]
     */
    public function update(ShiftRequest $request, string $id)
    {
        $shift = $this->checkShiftBelongsToGroup($id, $request->group_id);

        $data = $this->prepareData($request);
        $shift->update($data);

        return response()->json([
            'message' => __('shifts.infoShift.edit_success')
        ]);
    }

    public function delete(Request $request, string $id)
    {
        $this->checkShiftBelongsToGroup($id, $request->group_id);

        $this->shift->delete($id);

        return response()->json([
            'message' => __('shifts.infoShift.delete_success')
        ]);
    }

    /**
     * Prepare data for create and update
     *
     * @param  ShiftRequest $request
     *
     * @return array
     */
    private function prepareData(ShiftRequest $request)
    {
        return [
            'group_id' => $request->group_id,
            'name' => $request->name,
            'type' => $request->type,
            'schedules' => $request->schedules,
            'allow_time' => $request->allow_time,
            'times' => [
                [
                    'checkin' => $request->start_time_1,
                    'checkout' => $request->end_time_1
                ],
                [
                    'checkin' => $request->start_time_2,
                    'checkout' => $request->end_time_2
                ]
            ]
        ];
    }

    /**
     * Get employee list in shift
     *
     * @param  Request $request
     * @param  string $shiftId
     *
     * @return mixed
     */
    public function listUsers(Request $request, string $shiftId)
    {
        return response()->json([
            'users' => new UserCollection($this->user->getByShiftId($shiftId, $request->page)),
            'shift' => new ShiftResource($this->shift->getById($shiftId))
        ]);
    }

    /**
     * Get user list n shift by keyword
     *
     * @param  Request $request
     * @param  string $shiftId
     *
     * @return json App\Modes\User
     */
    public function userSearch(Request $request, string $shiftId)
    {
        $users = $this->search->getByShiftId($request->keyword, $shiftId, $request->page);

        return response()->json([
            'users' => new UserCollection($users)
        ]);
    }

    /**
     * Get user in group and without shift
     *
     * @param  Request $request
     * @param  string $shiftId
     *
     * @return json mixed
     */
    public function userAdd(Request $request, string $shiftId)
    {
        $shiftIds = $this->shift->getByGroupId($request->group_id)->pluck('_id')->toArray();
        $users = $this->user->getNotShiftInGroup($request->group_id, $shiftIds, $request->page);
        return response()->json([
            'users' => new UserCollection($users),
            'shift' => new ShiftResource($this->shift->getById($shiftId))
        ]);
    }

    /**
     * Get user in group and out shift
     *
     * @param  Request $request
     * @param  string $shiftId
     *
     * @return json App\Models\User
     */
    public function searchUsersWithoutShift(Request $request, string $shiftId)
    {
        $shiftIds = $this->shift->getByGroupId($request->group_id)->pluck('_id')->toArray();
        $users = $this->search->getNotShiftInGroup($request->keyword ?? '', $request->group_id, $shiftIds, $request->page);

        return response()->json([
            'users' => new UserCollection($users)
        ]);
    }

    /**
     * Assign user to shift's employee list
     *
     * @param  Request $request
     * @param  string $shiftId
     *
     * @return json
     */
    public function userStore(Request $request, string $shiftId)
    {
        $shift = $this->checkShiftBelongsToGroup($shiftId, $request->group_id);
        $user = $this->checkUserBelongsToGroup($request->user_id, $request->group_id);

        if (!$this->checkExistsShiftOfUserInGroup($user, $request->group_id)) {

            $this->user->addUserToShift($request->user_id, $shiftId);
            $this->notification->create([
                'creator_id' => auth('api')->id(),
                'shift_id' => $shiftId,
                'user_id' => $request->user_id,
                'group_id' => $shift->group_id,
                'type' => Notification::TYPE_14,
            ]);
            return response()->json([
                'message' => __('company.shift.add_user_success')
            ]);
        }
    }

    /**
     * Assign more users to shift's employee list
     *
     * @param  Request $request
     * @param  string $shiftId
     *
     * @return json
     */
    public function usersStore(Request $request, string $shiftId)
    {
        $shift = $this->checkShiftBelongsToGroup($shiftId, $request->group_id);

        $success = 0;
        foreach ($request->user_ids as $userId) {
            $user = $this->checkUserBelongsToGroup($userId, $request->group_id);

            if (!$this->checkExistsShiftOfUserInGroup($user, $request->group_id)) {
                $this->user->addUserToShift($userId, $shiftId);
                $this->notification->create([
                    'creator_id' => auth('api')->id(),
                    'shift_id' => $shiftId,
                    'user_id' => $userId,
                    'group_id' => $shift->group_id,
                    'type' => Notification::TYPE_14,
                ]);
                $success++;
            }
        }

        return response()->json([
            'message' => __('word.success')
        ]);
    }

    /**
     * Remove user from shift's employee list
     *
     * @param  Request $request
     * @param  string $shiftId
     *
     * @return json mixed string
     */
    public function userDelete(Request $request, string $shiftId)
    {
        $this->checkShiftBelongsToGroup($shiftId, $request->group_id);
        $this->checkUserBelongsToGroup($request->user_id, $request->group_id);

        $this->user->removeUserFromShift($request->user_id, $shiftId);
        $this->notification->create([
            'creator_name' => [
                'first_name' => auth('api')->user()->first_name,
                'last_name' => auth('api')->user()->last_name,
            ],
            'shift_id' => $shiftId,
            'user_id' => $request->user_id,
            'group_id' => $request->group_id,
            'type' => Notification::TYPE_15,
        ]);
        return response()->json([
            'message' => __('company.shift.delete_user_success')
        ]);
    }

    /**
     * Kiểm tra xem user có nằm trong group hay không
     *
     * @param  tring $userId
     * @param  string $groupId
     *
     * @return void
     */
    private function checkUserBelongsToGroup(string $userId, string $groupId)
    {
        $user = $this->user->getById($userId);
        $this->authorize('belongsToGroup', [$user, $groupId]);
        return $user;
    }

    /**
     * Kiểm tra xem ca làm việc có thuộc về group hay không
     *
     * @param  string $shiftId
     * @param  string $groupId
     *
     * @return void
     */
    private function checkShiftBelongsToGroup(string $shiftId, string $groupId)
    {
        $shift = $this->shift->getById($shiftId);
        $this->authorize('belongsToGroup', [$shift, $groupId]);
        return $shift;
    }

    /**
     * Kiểm tra xem người dùng đã có ca làm việc trong nhóm hay chưa
     *
     * @param  App\Models\User $user
     * @param  string $groupId
     * @return bool
     */
    private function checkExistsShiftOfUserInGroup($user, $groupId)
    {
        $count = $this->shift
                       ->getByIds($user->shift_ids ?? [])
                       ->where('group_id', $groupId)
                       ->count();
        if ($count > 0) {
            return true;
        }
        return false;
    }
}
