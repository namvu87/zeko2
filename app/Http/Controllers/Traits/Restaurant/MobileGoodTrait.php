<?php

namespace App\Http\Controllers\Traits\Restaurant;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait MobileGoodTrait
{
    /**
     * Tạo hàng hoá nhập
     *
     * @param  Request $request
     * @return App\Models\Good
     */
    public function store(Request $request)
    {
        $request = $this->prepareRequest($request);
        $validator = Validator::make($request->all(), $this->rules($request));

        if ($validator->fails()) {
            return [
                'status' => 422,
                'data' => $validator->errors()
            ];
        }

        $data = $this->prepareData($request);
        $good = $this->good->create($data);

        // Các biến thể khác
        foreach ($request->conversion_units as $conversionUnit) {
            array_set($data, 'parent_id', $good->id);
            array_set($data, 'unit', $conversionUnit['unit']);
            array_set($data, 'price', $conversionUnit['price']);
            array_set($data, 'conversion_value', $conversionUnit['value']);
            array_set($data, 'code', $this->good->generateCode($request->group_id));
            $this->good->create($data);
        }

        return [
            'status' => self::SUCCESS_STATUS,
            'data' => $good
        ];
    }
}