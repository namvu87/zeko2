<?php

namespace App\Http\Controllers\Traits\Restaurant;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

trait GoodProcessedTrait
{
    /**
     * Rules validate store
     *
     * @param  array  $request
     * @return array
     */
    private function rules(Request $request) :array
    {
        $rules = [
            'name' => 'required|string|max:255',
            'code' => [
                'required', 'string',
                Rule::unique('goods', 'code')->where(function($query) use ($request) {
                    return $query->where('group_id', $request->group_id);
                })
            ],
            'group_menu_ids' => 'required|array',
            'group_menu_ids.*' => [
                'required', 'string',
                Rule::exists('group_menus', '_id')->where(function($query) use ($request) {
                    return $query->where('group_id', $request->group_id);
                })
            ],
            'price'                      => 'required|integer|min:0',
            'unit'                       => 'required',
            'discount'                   => 'nullable|integer|min:0',
            'discount_type'              => 'nullable|in:1,2',
            'properties'                 => 'nullable|array',
            'properties.*.value'         => 'required',
            'properties.*.name'          => 'required|distinct',
            'inventory_min'              => 'nullable|integer|min:0|lte:inventory_max',
            'inventory_max'              => 'nullable|integer|min:0',
            'images'                     => 'array|max:' . $this->good->getMaxFileUpload(),
            'images.*'                   => 'image|max:' . $this->good->getMaxSizeUpload(),
            'system_images'              => 'nullable|array',
            'system_images.*'            => 'string',
            'is_sale'                    => 'nullable|boolean',
            'conversion_units'           => 'nullable|array',
            'conversion_units.*.unit'    => 'required|string|distinct',
            'conversion_units.*.value'   => 'required|numeric',
            'conversion_units.*.price'   => 'required|integer',
            'description'                => 'nullable|string',
            'ingredients'                => 'nullable|array',
            'ingredients.*.id'           => 'string|distinct',
            'ingredients.*.count'        => 'numeric|min:0.01',
            'ingredients.*.unit'         => 'string',
            'ingredients.*.name'         => 'string',
            'ingredients.*.price_origin' => 'int',
            'ingredients.*.code'         => [
                'distinct',
                Rule::exists('goods', 'code')->where(function($query) use ($request) {
                    $query->where('group_id', $request->group_id)
                          ->where('type', $this->good->getImportType());
                })
            ]
        ];
        if (count($request->ingredients) == 0) $rules['price_origin'] = 'required|integer|min:0';
        return $rules;
    }

    /**
     * Cooking request fot normally
     *
     * @param  Request $request
     * @return array
     */
    private function prepareRequest(Request $request)
    {
        $properties = [];
        $conversionUnits = [];
        $ingredients = [];

        foreach (json_decode($request->properties) as $property) {
            array_push($properties, (array) $property);
        }
        foreach (json_decode($request->conversion_units) as $conversion) {
            array_push($conversionUnits, (array) $conversion);
        }
        foreach (json_decode($request->ingredients) as $ingredient) {
            array_push($ingredients, (array) $ingredient);
        }

        $request['conversion_units'] = $conversionUnits;
        $request['properties'] = $properties;
        $request['ingredients'] = $ingredients;

        return $request;
    }

    /**
     * Tạo dữ liệu chuẩn cho store
     *
     * @param  Request $request
     * @return array
     */
    private function prepareData(Request $request) :array
    {
        $images = $this->good->saveImages($request->images ?? [], $request->group_id) ?? null;
        $images = array_merge($images ?? [], $request->system_images ?? []);
        $data = [
            'group_id'       => $request->group_id,
            'code'           => $request->code,
            'name'           => $request->name,
            'price'          => (int) $request->price,
            'unit'           => $request->unit,
            'discount'       => (int) $request->discount,
            'discount_type'  => (int) $request->discount_type,
            'inventory_min'  => (int) $request->inventory_min,
            'inventory_max'  => (int) $request->inventory_max,
            'description'    => $request->description,
            'group_menu_ids' => $request->group_menu_ids,
            'properties'     => (array) $request->properties,
            'type'           => $this->good->getProcessType(),
            'is_sale'        => true,
            'status'         => $this->good->getStatusActive(),
            'ingredients'    => (array) $request->ingredients,
            'images'         => count($images) > 0 ? $images : null
        ];
        if (count($data['ingredients']) == 0) $data['price_origin'] = (int) $request->price_origin;
        return $data;
    }
}
