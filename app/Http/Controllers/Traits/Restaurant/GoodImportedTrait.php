<?php

namespace App\Http\Controllers\Traits\Restaurant;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

trait GoodImportedTrait
{
    /**
     * Rules validate store
     *
     * @param  array  $request
     * @return array
     */
    private function rules(Request $request) :array
    {
        return [
            'name' => 'required|string|max:255',
            'code' => [
                'required', 'string',
                Rule::unique('goods', 'code')->where(function($query) use ($request) {
                    return $query->where('group_id', $request->group_id);
                })
            ],
            'group_menu_ids' => 'required|array',
            'group_menu_ids.*' => [
                'required', 'string',
                Rule::exists('group_menus', '_id')->where(function($query) use ($request) {
                    return $query->where('group_id', $request->group_id);
                })
            ],
            'price'                      => 'required|integer|min:0',
            'unit'                       => 'required',
            'discount'                   => 'nullable|integer|min:0',
            'discount_type'              => 'nullable|in:1,2',
            'properties'                 => 'nullable|array',
            'properties.*.value'         => 'required',
            'properties.*.name'          => 'required|distinct',
            'price_origin'               => 'nullable|integer|min:0',
            'inventory_min'              => 'nullable|integer|min:0|lte:inventory_max',
            'inventory_max'              => 'nullable|integer|min:0',
            'images'                     => 'array|max:' . $this->good->getMaxFileUpload(),
            'images.*'                   => 'image|max:' . $this->good->getMaxSizeUpload(),
            'is_sale'                    => 'nullable|boolean',
            'conversion_units'           => 'nullable|array',
            'conversion_units.*.unit'    => 'required|string|distinct',
            'conversion_units.*.value'   => 'required|numeric',
            'conversion_units.*.price'   => 'required|integer',
            'description'                => 'nullable|string',
            'system_images'              => 'nullable|array',
            'system_images.*'            => 'string',
        ];
    }

    /**
     * Cooking request fot normally
     *
     * @param  Request $request
     * @return array
     */
    private function prepareRequest(Request $request)
    {
        $properties = [];
        $conversionUnits = [];

        foreach (json_decode($request->properties) as $property) {
            array_push($properties, (array) $property);
        }
        foreach (json_decode($request->conversion_units) as $conversion) {
            array_push($conversionUnits, (array) $conversion);
        }

        $request['conversion_units'] = $conversionUnits;
        $request['properties'] = $properties;

        return $request;
    }

    /**
     * Tạo dữ liệu chuẩn cho store
     *
     * @param  Request $request
     * @return array
     */
    private function prepareData(Request $request) :array
    {
        $images = $this->good->saveImages($request->images ?? [], $request->group_id);
        $images = array_merge($images ?? [], $request->system_images ?? []);
        return [
            'group_id'       => $request->group_id,
            'code'           => $request->code,
            'name'           => $request->name,
            'price'          => (int) $request->price,
            'unit'           => $request->unit,
            'is_sale'        => (boolean) $request->is_sale,
            'discount'       => (int) $request->discount,
            'discount_type'  => (int) $request->discount_type,
            'inventory_min'  => (int) $request->inventory_min,
            'inventory_max'  => (int) $request->inventory_max,
            'description'    => $request->description,
            'group_menu_ids' => $request->group_menu_ids,
            'properties'     => (array) $request->properties,
            'type'           => $this->good->getImportType(),
            'status'         => $this->good->getStatusActive(),
            'images'         => count($images) > 0 ? $images : null,
            'price_origin'   => (int) $request->price_origin,
        ];
    }
}
