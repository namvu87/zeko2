<?php

namespace App\Http\Controllers\Traits\Restaurant;

use Illuminate\Http\Request;
use Rebing\GraphQL\Error\AuthorizationError;

trait GoodTrait
{
    /**
     * Khởi tạo hàng hoá
     *
     * @param  Request $request
     * @return json
     */
    public function store(Request $request)
    {
        if ((count($request->images ?? []) + count($request->system_images ?? [])) >
            $this->good->getMaxFileUpload())
            return new AuthorizationError('Over Max image');
        $request = $this->prepareRequest($request);
        $this->validate($request, $this->rules($request));

        $data = $this->prepareData($request);
        $good = $this->good->create($data);

        // Các biến thể khác
        foreach ($request->conversion_units as $conversionUnit) {
            array_set($data, 'parent_id', $good->id);
            array_set($data, 'unit', $conversionUnit['unit']);
            array_set($data, 'price', $conversionUnit['price']);
            array_set($data, 'conversion_value', $conversionUnit['value']);
            array_set($data, 'code', $this->good->generateCode($request->group_id));
            $this->good->create($data);
        }

        return response()->json([
            'code' => self::SUCCESS_STATUS,
            'good' => $good
        ]);
    }
}
