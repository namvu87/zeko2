<?php

namespace App\Http\Controllers;

use App\Contracts\NotificationContract;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\GroupMenuContract;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Contracts\UserContract;
use Illuminate\Validation\Rule;

class ImportController extends Controller
{
    const FAIL_STATUS = 0;
    const SUCCESS_STATUS = 1;
    private $user;
    private $notification;

    public function __construct(UserContract $user, NotificationContract $notification)
    {
        $this->user = $user;
        $this->notification = $notification;
    }

    public function importUserIntoGroup(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'bail|required|string|max:64',
            'phone_number' => 'bail|required_without:email|digits:10|exists:users,phone_number',
            'email' => 'bail|required_without:phone_number|email|max:255|exists:users,email'
        ]);

        $user = $this->user->findForPassport($request->phone_number ?? $request->email);

        $code = self::FAIL_STATUS;
        $message = __('import_export.user_already_added');

        if ($this->user->addUserToGroup($user->id, $request->group_id)) {
            $this->notification->create([
                'user_assign_id' => auth('api')->id(),
                'user_id' => $request->user_id,
                'group_id' => $request->group_id,
                'type' => Notification::TYPE_10,
            ]);
            $code = self::SUCCESS_STATUS;
            $message = __('word.success');
        }

        return response()->json([
            'message' => $message,
            'code' => $code
        ]);
    }
}
