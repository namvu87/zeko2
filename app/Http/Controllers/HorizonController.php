<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class HorizonController extends Controller
{
    use ThrottlesLogins;

    public function home()
    {
        if (!auth('web')->check()) {
            return redirect('/horizon-login');
        }
        return view('home');
    }

    public function showLoginForm()
    {
        if (auth('web')->check()) {
            return redirect('/horizon-home');
        }
        return view('auth.login');
    }

    public function login(Request $request)
    {
        if (auth('web')->check()) {
            return redirect('/horizon-home');
        }

        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string|min:8'
        ]);

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        
        $credentials = $request->only('email', 'password');

        if (in_array($request->email, config('master.email')) &&
            Auth::guard('web')->attempt($credentials, $request->remember)) {
            return redirect('/horizon-home');
        }

        $this->incrementLoginAttempts($request);
        
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    public function logout(Request $request)
    {
        if (!auth('web')->check()) {
            return redirect('/horizon-home');
        }

        auth('web')->logout();

        $request->session()->invalidate();

        return redirect('/horizon-login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    private function username()
    {
        return 'email';
    }
}
