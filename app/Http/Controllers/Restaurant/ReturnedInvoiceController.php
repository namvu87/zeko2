<?php

namespace App\Http\Controllers\Restaurant;

use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\ReturnedInvoiceContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReturnedInvoiceController extends Controller
{
    private $invoice;
    private $returnedInvoice;

    public function __construct(ReturnedInvoiceContract $invoice, InvoiceContract $invoiceContract)
    {
        $this->invoice = $invoiceContract;
        $this->returnedInvoice = $invoice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'invoices' => $this->returnedInvoice->filterByCondition($request->all())
        ]);
    }

    public function detail($invoiceId)
    {
        return response()->json([
            'invoice' => $this->returnedInvoice->getById($invoiceId, ['invoice:code', 'creator:first_name,last_name'])
        ]);
    }

    public function disable(Request $request, $invoiceId)
    {
        $invoice = $this->checkInvoiceBelongsToGroup($invoiceId, $request->group_id);
        if ($invoice->status != $this->returnedInvoice->getStatusDisable()) {
            $invoice->update([
                'status' => $this->returnedInvoice->getStatusDisable()
            ]);
            $this->invoice->updateNumberGoodsInvoice($invoice->invoice_id, $invoice->goods);
        }
        return response()->json([
            'message' => __('word.success')
        ]);
    }

    private function checkInvoiceBelongsToGroup(string $invoiceId, string $groupId)
    {
        $invoice = $this->returnedInvoice->getById($invoiceId);
        $this->authorize('belongsToGroup', [$invoice, $groupId]);
        return $invoice;
    }
}
