<?php

namespace App\Http\Controllers\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Restaurant\InvoiceFormContract;
use Illuminate\Validation\Rule;

class InvoiceFormController extends Controller
{
    private $invoiceForm;

    public function __construct(InvoiceFormContract $invoiceForm)
    {
        $this->invoiceForm = $invoiceForm;
    }

    public function list(Request $request)
    {
        return response()->json([
            'invoice_forms' => $this->invoiceForm->getAllInvoiceForm($request->all())
        ]);
    }

    public function listByType(Request $request)
    {
        $request->validate(['type' => 'required|in:' . implode(',', $this->invoiceForm->getTypes())]);
        return response()->json([
            'invoice_forms' => $this->invoiceForm->getByType($request->group_id, $request->type)
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => [
                'required', 'string',
                Rule::unique('invoice_forms', 'name')
                    ->where('group_id', $request->group_id)
            ],
            'type' => 'required|in:' . implode(',', $this->invoiceForm->getTypes()),
            'content' => 'required|string',
            'size' => 'required|in:' . implode(',', $this->invoiceForm->getSizes())
        ]);
        $newForm = $this->invoiceForm->create([
            'group_id' => $request->group_id,
            'name' => $request->name,
            'type' => $request->type,
            'content' => $request->content,
            'size' => $request->size,
            'user_created_id' => auth('api')->id()
        ]);

        return response()->json([
            'invoice_form' => $newForm,
            'message' => __('invoice_form.create_success')
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->checkInvoiceFormBelongsToGroup($id, $request->group_id);
        $request->validate([
            'name' => [
                'required', 'string',
                Rule::unique('invoice_forms', 'name')
                    ->where('group_id', $request->group_id)
                    ->ignore($id, '_id')
            ],
            'type' => 'required|in:' . implode(',', $this->invoiceForm->getTypes()),
            'content' => 'required|string',
            'size' => 'required|in:' . implode(',', $this->invoiceForm->getSizes())
        ]);

        $this->invoiceForm->update($id, [
            'name' => $request->name,
            'content' => $request->content,
            'size' => $request->size,
        ]);

        return response()->json([
            'message' => __('invoice_form.update_success')
        ]);
    }

    public function delete(Request $request, $invoiceFormId)
    {
        $this->checkInvoiceFormBelongsToGroup($invoiceFormId, $request->group_id);
        $this->invoiceForm->delete($invoiceFormId);
        return response()->json([
            'message' => __('invoice_form.delete_success')
        ]);
    }

    /**
     * Kiểm tra invoice form có thuộc về group không
     *
     * @param      String $invoiceFormId The invoice form identifier
     * @param      String $groupId The group identifier
     */
    private function checkInvoiceFormBelongsToGroup(string $invoiceFormId, string $groupId)
    {
        $invoiceForm = $this->invoiceForm->getById($invoiceFormId);
        $this->authorize('belongsToGroup', [$invoiceForm, $groupId]);
    }
}
