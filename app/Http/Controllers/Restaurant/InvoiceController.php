<?php

namespace App\Http\Controllers\Restaurant;

use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\TableContract;
use App\Contracts\UserContract;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $invoice;
    private $table;
    private $user;
    private $good;

    public function __construct(InvoiceContract $invoiceContract, TableContract $tableContract, UserContract $userContract, GoodContract $good)
    {
        $this->invoice = $invoiceContract;
        $this->table = $tableContract;
        $this->user = $userContract;
        $this->good = $good;
    }

    public function index(Request $request)
    {
        $invoices = $this->invoice->getInvoiceByCondition($request->all());
        return response()->json([
            'invoices' => $invoices
        ]);
    }

    public function listInTime(Request $request)
    {
        $request->validate([
            'start' => 'required_with:end|date_format:d-m-Y',
            'end' => 'required_with:start|date_format:d-m-Y',
            'hour' => 'nullable|date_format:d-m-Y H',
            'date' => 'nullable|date_format:d-m-Y'
        ]);
        if ($request->has('hour')) {
            $start = Carbon::createFromFormat('d-m-Y H', $request->hour);
            $end = Carbon::createFromFormat('d-m-Y H:i:s', $request->hour . ':59:59');
        } elseif ($request->has('date')) {
            $start = new Carbon(Carbon::createFromFormat('d-m-Y H:i:s', $request->date . '00:00:00'));
            $end = new Carbon(Carbon::createFromFormat('d-m-Y H:i:s', $request->date . '23:59:59'));
        } else {
            $start = new Carbon(Carbon::createFromFormat('d-m-Y H:i:s', $request->start . '00:00:00'));
            $end = new Carbon(Carbon::createFromFormat('d-m-Y H:i:s', $request->end . '23:59:59'));
        }
        return response()->json([
            'invoices' => $this->invoice->getInvoicesByPeriodTime($request->group_id, $start, $end)->load('recipient:first_name,last_name')
        ]);
    }

    public function detail(Request $request, $invoiceId)
    {
        $invoice = $this->invoice->getById($invoiceId, ['returnedInvoices.creator:first_name,last_name', 'recipient:first_name,last_name']);
        return response()->json([
            'tables' => $this->table->getByIds($invoice->table_ids ?? []),
            'users' => $this->user->getByIds($invoice->user_ids ?? []),
            'invoice' => $invoice
        ]);
    }

    public function disable(Request $request, $invoiceId)
    {
        $invoice = $this->checkInvoiceBelongsToGroup($invoiceId, $request->group_id);
        $returnedInvoice = $invoice->returnedInvoices->count();
        if ($returnedInvoice) return response()->json([
            'message' => __('restaurant.invoice.not_delete_has_returned_invoice')
        ], 422);
        if ($invoice->status != $this->invoice->getStatusDisable()) {
            $invoice->update([
                'status' => $this->invoice->getStatusDisable()
            ]);
            $this->good->restoreNumberFromGoodsInvoice($invoice->goods);
        }
        return response()->json([
            'message' => __('word.success')
        ]);
    }

    private function checkInvoiceBelongsToGroup(string $invoiceId, string $groupId)
    {
        $invoice = $this->invoice->getById($invoiceId);
        $this->authorize('belongsToGroup', [$invoice, $groupId]);
        return $invoice;
    }
}
