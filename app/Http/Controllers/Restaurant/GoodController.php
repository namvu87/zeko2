<?php

namespace App\Http\Controllers\Restaurant;

use App\Contracts\GroupContract;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\GroupMenuContract;
use App\Http\Controllers\Controller;
use App\Rules\EqualRule;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class GoodController extends Controller
{
    const SUCCESS_STATUS = 200;
    private $good;
    private $group;
    private $menu;

    public function __construct(GoodContract $good, GroupContract $groupContract, GroupMenuContract $groupMenuContract)
    {
        $this->good = $good;
        $this->group = $groupContract;
        $this->menu = $groupMenuContract;
    }

    public function listImported(Request $request)
    {
        return response()->json([
            'goods' => $this->good->getImportedGoods($request->group_id, $request->name ?? '')
        ]);
    }

    public function importGoods(Request $request)
    {
        if (!$request->code) {
            $request['code'] = $this->good->generateCode($request->group_id);
        }
        $rules = [
            'name' => 'required|string|max:255',
            'code' => [
                'required', 'string', 'size:8',
                Rule::unique('goods', 'code')->where('group_id', $request->group_id)
            ],
            'group_menu_name' => 'required',
            'price' => 'required|integer|min:0',
            'price_origin' => 'integer|min:0',
            'unit' => 'required',
            'is_sale' => 'required|boolean',
            'inventory_min' => 'nullable|integer|min:0',
            'inventory_max' => 'nullable|integer|min:0',
        ];
        $code = config('const.status.fail');
        $message = __('import_export.error_response');
        $request->validate($rules);
        $key = array_search($request->unit, __('config.goods.units'));
        if ($key) $request['unit'] = $key;
        $group = $this->group->getById($request->group_id)->push('units', $key, true);
        try {
            $nameMenu = explode(',', $request->group_menu_name);
            $arr = array();
            foreach ($nameMenu as $key => $name) {
                $groupMenu = $this->menu->getByName($request->group_id, $name);
                if ($groupMenu) {
                    array_push($arr, $groupMenu->id);
                }
                $request['group_menu_ids'] = $arr;
            }
            $request['status'] = $this->good->getStatusActive();
            $request['is_sale'] = (bool) $request->is_sale;
            $request['inventory_number'] = 0;
            $request['type'] = $this->good->getGoodsType()[0];
            $this->good->create($request->all());
            $code = config('const.status.success');
            $message = __('word.success');
        } catch (\Exception $exception) {
        }
        return response()->json([
            'message' => $message,
            'code' => $code
        ]);
    }

    public function detail(Request $request, $goodId)
    {
        $this->checkGoodBelongsToGroup($goodId, $request->group_id);
        $good = $this->good->getById($goodId, ['groupMenus']);
        return response()->json([
            'menus' => $this->menu->getByGroupId($request->group_id),
            'good' => $good,
        ]);
    }

    /**
     * update goods images
     *
     * @param  Request $request
     *
     * @return json
     */
    public function updateImage(Request $request)
    {
        $good = $this->good->getById($request->good_id);
        $countImage = count($good->images ?? []);
        $this->validate($request, [
            'good_id' => 'required|string',
            'images' => 'required|array|max:' . ($this->good->getMaxFileUpload() - $countImage),
            'images.*' => 'required|mimes:jpeg,png,jpg|max:' . $this->good->getMaxSizeUpload()
        ]);

        $images = $this->good->saveImages($request->images ?? [], $request->group_id) ?? null;

        $this->good->update($request->good_id, [
            'images' => array_merge($good->images ?? [], $images)
        ]);

        return response()->json([
            'images' => $images
        ]);
    }

    public function getGoodsToImportInvoice(Request $request)
    {
        return response()->json([
            'goods' => $this->good->limitGoodsByName($request->group_id, $request->name)
        ]);
    }

    public function checkFile(Request $request)
    {
        $rules = [];
        $messages = [];
        $attributes = [];
        $goods = [];
        foreach ($request->rows as $key => $row) {
            $rules["rows.{$key}.code"] = [
                'required',
                Rule::exists('goods', 'code')
                    ->where(function ($query) use ($request) {
                        return $query->where('group_id', $request->group_id)
                            ->where('type', $this->good->getGoodsType()[0])
                            ->where('status', $this->good->getStatusActive());
                    })
            ];
            $attributes["rows.{$key}.code"] = __('word.row') . ' ' . ($key + 1) . ': ' . __('restaurant.good.code');
            $good = $this->good->getNormalGoodsByCode($request->group_id, $row['code'] ?? '');
            if ($good) {
                $row['unit'] = $good->unit;
                if (!$row['name']) $row['name'] = $good->name;
                if (($row['price'] === null)) {
                    $row['price'] = $good->price_origin;
                }
                if (!$row['discount_type']) $row['discount_type'] = 1;
                $rules["rows.{$key}.name"] = [
                    'nullable',
                    new EqualRule($good->name)
                ];
                $rules["rows.{$key}.price"] = 'nullable|numeric|min:0';
                $rules["rows.{$key}.count"] = 'integer|min:0';
                if ($row['discount_type'] == 1) {
                    $rules["rows.{$key}.discount"] = "numeric|min:0|lte:rows.{$key}.price";
                } else {
                    $rules["rows.{$key}.discount"] = 'numeric|min:0|max:100';
                }
                $rules["rows.{$key}.discount_type"] = 'in:1,2';
                $attributes["rows.{$key}.name"] = __('word.row') . ' ' . ($key + 1) . ': ' . __('restaurant.good.name');
                $attributes["rows.{$key}.price_origin"] = __('word.row') . ' ' . ($key + 1) . ': ' . __('restaurant.good.price_import');
                $attributes["rows.{$key}.count"] = __('word.row') . ' ' . ($key + 1) . ': ' . __('word.count');
                $attributes["rows.{$key}.discount"] = __('word.row') . ' ' . ($key + 1) . ': ' . __('word.discount');
                $attributes["rows.{$key}.discount_type"] = __('word.row') . ' ' . ($key + 1) . ': ' . __('restaurant.good.discount_type');
            }
            $messages["rows.{$key}.code.exists"] =
                __('word.row') . ' ' . ($key + 1) . ': ' . __('word.exist_goods_code_file');
            array_push($goods, $row);
        };
        $this->validate($request, $rules, $messages, $attributes);
        return response()->json([
            'goods' => $goods
        ]);
    }

    public function addMenus(Request $request)
    {
        $this->good->updateIsSaleByIds($request->group_id, $request->goods_sale);
        return response()->json([
            'message' => __('restaurant.good.add_menus_success')
        ]);
    }

    private function checkGroupMenuBelongsToGroup(string $groupMenuId, string $groupId)
    {
        $menu = $this->menu->getById($groupMenuId);
        $this->authorize('belongsToGroup', [$menu, $groupId]);
    }

    private function checkGoodBelongsToGroup(string $goodId, string $groupId)
    {
        $good = $this->good->getById($goodId);
        $this->authorize('belongsToGroup', [$good, $groupId]);
    }
}
