<?php

namespace App\Http\Controllers\Restaurant;

use App\Contracts\Restaurant\ReportContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\RestaurantReportRequest;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $report;
    private $startDate;
    private $endDate;

    public function __construct(ReportContract $report, RestaurantReportRequest $request)
    {
        $this->startDate = Carbon::createFromFormat('d-m-Y H:i:s', $request->start . '00:00:00');
        $this->endDate = Carbon::createFromFormat('d-m-Y H:i:s', $request->end . '23:59:59');
        $this->report = $report;
    }

    public function sale(RestaurantReportRequest $request)
    {
        $this->validateOvertime();
        return response()->json([
            'datas' => $this->report->getReportSale($request->group_id, $this->startDate, $this->endDate)
        ]);
    }

    public function goods(RestaurantReportRequest $request)
    {
        $this->validateOvertime();
        return response()->json([
            'datas' => $this->report->getReportGoods($request->group_id, $this->startDate, $this->endDate)
        ]);
    }

    public function goodsImported(RestaurantReportRequest $request)
    {
        $this->validateOvertime();
        return response()->json([
            'datas' => $this->report->getReportGoodsImported($request->group_id, $this->startDate, $this->endDate)
        ]);
    }

    private function validateOvertime()
    {
        if (CarbonPeriod::create($this->startDate, $this->endDate)->count() > 366) {
            return response()->json([
                'errors' => ['overtime' => __('word.less_1_year')]
            ], 422);
        }
    }
}
