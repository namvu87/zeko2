<?php

namespace App\Http\Controllers\Restaurant;

use App\Contracts\Restaurant\GroupMenuContract;
use App\Http\Controllers\Controller;
use App\Models\GroupMenu;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class GroupMenuController extends Controller
{
    private $menu;

    public function __construct(GroupMenuContract $menu)
    {
        $this->menu = $menu;
    }

    public function list(Request $request)
    {
        return response()->json([
            'menus' => $this->menu->getByGroupId($request->group_id)
        ]);
    }

    public function create(Request $request)
    {
        return response()->json([
            'parent_group_menus' => $this->menu->getParentGroupMenusByGroupId($request->group_id)
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'required', 'string', 'max:255',
                Rule::unique('group_menus', 'name')->where(function ($query) use ($request) {
                    $query->where('group_id', $request->group_id);
                })
            ],
            'description' => 'nullable|string|max:1024',
            'parent_id' => [
                'nullable', 'string',
                Rule::exists('group_menus', '_id')->where(function ($query) use ($request) {
                    $query->where('group_id', $request->group_id);
                })
            ]

        ]);

        $this->menu->create([
            'group_id' => $request->group_id,
            'name' => $request->name,
            'description' => $request->description,
            'level' => $this->getLevel($request->parent_id),
            'parent_id' => $request->parent_id ?? null
        ]);

        return response()->json([
            'message' => __('restaurant.group_menu.create_success')
        ]);
    }

    public function edit(Request $request, $groupMenuId)
    {
        $menu = $this->menu->getById($groupMenuId);
        $this->authorize('belongsToGroup', [$menu, $request->group_id]);

        return response()->json([
            'group_menu' => $menu
        ]);
    }

    public function update(Request $request, $groupMenuId)
    {
        $this->checkGroupMenuBelongsToGroup($groupMenuId, $request->group_id);
        $this->validate($request, [
            'name' => [
                'required', 'max:255',
                Rule::unique('group_menus', 'name')->where(function ($query) use ($request) {
                    return $query->where('group_id', $request->group_id);
                })->ignore($groupMenuId, '_id')
            ],
            'description' => 'nullable|string|max:1024'
        ]);
        $this->menu->update($groupMenuId, [
            'name' => $request->name,
            'description' => $request->description
        ]);
        return response()->json([
            'message' => __('restaurant.group_menu.update_success')
        ]);
    }

    public function delete(Request $request, $groupMenuId)
    {
        $this->checkGroupMenuBelongsToGroup($groupMenuId, $request->group_id);
        $this->menu->deleteGroupMenuWithChilds($groupMenuId);
        return response()->json([
            'message' => __('restaurant.group_menu.delete_success')
        ]);
    }

    /**
     * @param  string $parentId
     * @return Int
     */
    private function getLevel($parentId)
    {
        if (!empty($parentId)) {
            return $this->menu->getById($parentId)->level + 1;
        }
        return $this->menu->getLevel0();
    }

    /**
     * Kiểm tra xem groupMenu có thuộc về group chỉ định hay không
     * @param  string $groupMenuId
     * @param  string $groupId
     * @return void
     */
    private function checkGroupMenuBelongsToGroup(string $groupMenuId, string $groupId)
    {
        $menu = $this->menu->getById($groupMenuId);
        $this->authorize('belongsToGroup', [$menu, $groupId]);
    }
}
