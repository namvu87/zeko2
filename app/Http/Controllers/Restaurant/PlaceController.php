<?php

namespace App\Http\Controllers\Restaurant;

use App\Contracts\Restaurant\PlaceContract;
use App\Contracts\Restaurant\TableContract;
use App\Http\Resources\Place as PlaceResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PlaceController extends Controller
{

    private $place;
    private $table;

    public function __construct(PlaceContract $place, TableContract $table)
    {
        $this->place = $place;
        $this->table = $table;
    }

    public function list(Request $request)
    {
        $places = $this->place->getByGroupId($request->group_id);
        return response()->json([
            'places' => PlaceResource::collection($places)
        ]);
    }

    /**
     * Tạo khu vực
     * Kiểm tra xem khu vực có là duy nhất trong nhóm hay không (without trashed)
     * Nếu đã tồn tại và đã trashed thì restore
     *
     * @param  Request $request
     *
     * @return App\Models\Place
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'required', 'string', 'max:128',
                Rule::unique('places')->where(function($query) use ($request) {
                    return $query->where('group_id', $request->group_id)->whereNull('deleted_at');
                })
            ],
            'group_id' => 'required|string',
            'description' => 'nullable|string'
        ]);

        $this->place->createPlace($request->all());
        return response()->json([
            'message' => __('restaurant.place.create_success')
        ]);
    }

    public function import(Request $request)
    {
        $request->validate([
            'name' => [
                'required', 'string', 'max:128',
                Rule::unique('places')->where(function($query) use ($request) {
                    return $query->where('group_id', $request->group_id)->whereNull('deleted_at');
                })
            ]
        ]);
        $code = config('const.status.fail');
        $message = __('import_export.error_response');
        try {
            $this->place->createPlace($request->all());
            $code = config('const.status.success');
            $message = __('word.success');
        } catch (\Exception $exception) {
            dd($exception);
        }
        return response()->json([
            'message' => $message,
            'code' => $code
        ]);
    }

    public function edit(Request $request, $placeId)
    {
        $this->checkPlaceBelongsToGroup($placeId, $request->group_id);
        $place = $this->place->getById($placeId);

        return response()->json([
            'place' => new PlaceResource($place)
        ]);
    }

    /**
     * Cập nhật thông tin khu vực
     * Kiểm tra name phải là suy nhất trong group (tính cả trong trashed)
     *
     * @param  Request $request
     * @param  string $placeId
     *
     * @return json
     */
    public function update(Request $request, string $placeId)
    {
        $this->validate($request, [
            'name' => [
                'required', 'string', 'max:255',
                Rule::unique('places')->where(function($query) use ($request, $placeId) {
                    return $query->where('group_id', $request->group_id)
                        ->where('_id', '!=', $placeId);
                })
            ],
            'description' => 'nullable|string'
        ]);
        $this->checkPlaceBelongsToGroup($placeId, $request->group_id);
        $this->place->update($placeId, [
            'name' => $request->name,
            'description' => $request->description
        ]);
        return response()->json([
            'message' => __('restaurant.place.update_success')
        ]);
    }

    public function delete(Request $request, $placeId)
    {
        $this->checkPlaceBelongsToGroup($placeId, $request->group_id);

        $this->table->deleteByPlaceId($placeId);
        $this->place->delete($placeId);

        return response()->json([
            'message' => __('restaurant.place.delete_success')
        ]);
    }

    private function checkPlaceBelongsToGroup(string $placeId, string $groupId)
    {
        $place = $this->place->getById($placeId);
        $this->authorize('belongsToGroup', [$place, $groupId]);
    }
}
