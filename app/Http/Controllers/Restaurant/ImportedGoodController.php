<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\Restaurant\GoodTrait;
use App\Http\Controllers\Traits\Restaurant\GoodImportedTrait;
use App\Contracts\Restaurant\GoodContract;

class ImportedGoodController extends Controller
{
    use GoodTrait, GoodImportedTrait;

    const SUCCESS_STATUS = 200;
    private $good;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }
}
