<?php

namespace App\Http\Controllers\Restaurant;

use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\ImportInvoiceContract;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ImportInvoiceController extends Controller
{
    private $invoice;
    private $good;

    public function __construct(ImportInvoiceContract $invoice, GoodContract $good)
    {
        $this->invoice = $invoice;
        $this->good = $good;
    }

    public function list(Request $request)
    {
        $invoices = $this->invoice->getImportInvoiceByCondition($request->all());
        return response()->json([
            'invoices' => $invoices
        ]);
    }

    public function store(Request $request)
    {
        $attributes = [];
        $rules = [
            'note' => 'nullable|string|max:255',
            'goods' => 'required|array',
            'goods.*.count' => 'required|integer|min:1',
            'goods.*.name' => 'required',
            'goods.*.price' => 'required|integer|min:0',
            'goods.*.discount_type' => 'required|in:1,2',
            'goods.*.code' => [
                'required', 'distinct',
                Rule::exists('goods', 'code')
                    ->where(function($query) use ($request) {
                        return $query->where('group_id', $request->group_id)
                            ->where('type', $this->good->getGoodsType()[0])
                            ->where('status', $this->good->getStatusActive());
                    })
            ],
            'code' => [
                'nullable', 'string', 'size:8',
                Rule::unique('import_invoices', 'code')
                    ->where('group_id', $request->group_id),
            ],
            'payment_method' => 'required|integer|in:1,2'
        ];
        foreach ($request->goods as $key => $good) {
            $rules["goods.{$key}.discount"] = "required|numeric|min:0|lte:goods.{$key}.price";
            $attributes["goods.{$key}.discount"] = __('restaurant.good.discount');
        }
        $this->validate($request, $rules, [], $attributes);
        $goods = $this->invoice->store($request->all());
        if ($request->status == $this->invoice->getStatusFinished()) {
            foreach ($goods as $key => $good) {
                $obj = $this->good->getById($good->id);
                $obj->update([
                    'inventory_number' => $obj->inventory_number + $good->count
                ]);
            }
        }
        return response()->json([
            'message' => __('word.success')
        ]);
    }

    public function pay(Request $request, $id)
    {
        $invoice = $this->checkInvoiceBelongsToGroup($id, $request->group_id);
        if ($invoice->status == $this->invoice->getStatusTemporary()) {
            $invoice->update([
                'status' => $this->invoice->getStatusFinished(),
                'paid_at' => Carbon::now()
            ]);
            foreach ($invoice->goods as $key => $good) {
                $obj = $this->good->getById($good['id']);
                $obj->update([
                    'inventory_number' => $obj->inventory_number + $good['count']
                ]);
            }
        }
        return response()->json([
            'message' => __('word.success')
        ]);
    }

    private function checkInvoiceBelongsToGroup(string $invoiceId, string $groupId)
    {
        $invoice = $this->invoice->getById($invoiceId);
        $this->authorize('belongsToGroup', [$invoice, $groupId]);
        return $invoice;
    }
}
