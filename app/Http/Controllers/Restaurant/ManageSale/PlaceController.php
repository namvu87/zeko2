<?php

namespace App\Http\Controllers\Restaurant\ManageSale;

use App\Contracts\Restaurant\PlaceContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Place as PlaceResource;

class PlaceController extends Controller {
	private $place;

	public function __construct(PlaceContract $place) {
		$this->place = $place;
	}

	public function getPlaces(Request $request) {
		return response()->json([
			'places' => PlaceResource::collection($this->place->getAll(['group_id' => $request->group_id]))
		]);
	}

	public function getPlacesWithCondition(Request $request) {
		$request->validate([
			'place_ids' => 'nullable|array'
		]);
		if (empty($request->place_ids)) {
			$placeIds = [];
		} else {
			$placeIds = $request->place_ids;
		}
		return response()->json([
			'places' => PlaceResource::collection($this->place->getPlacesWithCondition($request->group_id, $placeIds))
		]);
	}
}
