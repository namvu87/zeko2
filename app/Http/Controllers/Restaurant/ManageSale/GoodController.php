<?php

namespace App\Http\Controllers\Restaurant\ManageSale;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\InvoiceContract;

class GoodController extends Controller
{
    private $good;

    private $invoice;

    public function __construct(GoodContract $good, InvoiceContract $invoice) 
    {
        $this->good = $good;
        $this->invoice = $invoice;
    }

    public function setStatus(Request $request) 
    {
        $this->good->update($request->good_id, ['status' => $request->good_status]);
    }
}
