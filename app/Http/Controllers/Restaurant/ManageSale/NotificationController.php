<?php

namespace App\Http\Controllers\Restaurant\ManageSale;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\UserContract;
use App\Contracts\Restaurant\InvoiceContract;
use App\Jobs\Restaurant\AcceptCreateInvoice;

class NotificationController extends Controller
{
    const SUCCESS_STATUS = 1;

    private $invoice;
    private $user;

    public function __construct(InvoiceContract $invoice, UserContract $user)
    {
        $this->invoice = $invoice;
        $this->user = $user;
    }

    /**
     * Thông báo đã chấp nhận yêu cầu của user
     *
     * @param  Request $request
     * @return json
     */
    public function accept(Request $request)
    {
        $this->validate($request, [
            'group_id'   => 'required|string',
            'invoice_id' => 'required|string'
        ]);
        $invoice = $this->invoice->getById($request->invoice_id);

        $this->authorize('belongsToGroup', [$invoice, $request->group_id]);
        $this->authorize('requestable', $invoice);

        $recipients = $this->user->getFirebaseTokensByIds($invoice->user_ids ?? []);

        $notification = [];
        switch($invoice->request_status) {
            case $this->invoice->getCreateInvoiceRequestStatus():
                $notification = [
                    'title' => __('manage_sale.client_notify.accepted_create_invoice_title'),
                    'body' => __('manage_sale.client_notify.accepted_create_invoice_body', ['code' => $invoice->code])
                ];
                break;
            case $this->invoice->getAdditionFoodRequestStatus():
                $notification = [
                    'title' => __('manage_sale.client_notify.accepted_addition_food_invoice_title'),
                    'body' => __('manage_sale.client_notify.accepted_addition_food_invoice_body', ['code' => $invoice->code])
                ];
                break;
            case $this->invoice->getPurchaseRequestStatus():
                $notification = [
                    'title' => __('manage_sale.client_notify.accepted_purchase_invoice_title'),
                    'body' => __('manage_sale.client_notify.accepted_purchase_invoice_body', ['code' => $invoice->code])
                ];
                break;
            default: 
                $notification = [
                    'title' => __('manage_sale.client_notify.accepted_create_invoice_title'),
                    'body' => __('manage_sale.client_notify.accepted_create_invoice_body', ['code' => $invoice->code])
                ];
        }

        AcceptCreateInvoice::dispatch($recipients, $notification);

        $this->invoice->update($invoice->id, [
            'request_status' => $this->invoice->getNoneRequestStatus()
        ]);

        return response()->json([
            'status' => self::SUCCESS_STATUS
        ]);
    }
}
