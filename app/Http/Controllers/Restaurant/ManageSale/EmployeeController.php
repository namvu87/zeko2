<?php

namespace App\Http\Controllers\Restaurant\ManageSale;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\UserContract;
use App\Http\Resources\User as UserResource;

class EmployeeController extends Controller
{
	public $employee;

    public function __construct(UserContract $employee)
    {
    	$this->employee = $employee;
    }

    public function getEmployees(Request $request)
    {
    	return response()->json([
    		'employees' => UserResource::collection($this->employee->getByGroupIdNotPaginate($request->group_id))
    	]);
    }
}
