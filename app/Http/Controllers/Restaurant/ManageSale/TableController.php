<?php

namespace App\Http\Controllers\Restaurant\ManageSale;

use App\Contracts\Restaurant\TableContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Table as TableResource;

class TableController extends Controller {
	private $table;

	public function __construct(TableContract $table) {
		$this->table = $table;
	}

	public function getAll(Request $request) {
		return response()->json([
			'tables' => TableResource::collection($this->table->getByGroupId($request->group_id, ['place']))
		]);
	}

	public function updateTableStatus(Request $request) {
		try {
			$this->table->update($request->_id, [
				'status' => $request->status,
			]);
		} catch (Exception $e) {
			//
		}
	}
}
