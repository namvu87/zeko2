<?php

namespace App\Http\Controllers\Restaurant\ManageSale;

use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\GroupMenuContract;
use App\Http\Controllers\Controller;
use App\Http\Resources\GroupMenus as GroupMenusResource;
use App\Http\Resources\Good as GoodResource;
use Illuminate\Http\Request;

class GeneralController extends Controller {
	private $menu;
	private $good;

	public function __construct(GroupMenuContract $menu, GoodContract $good) {
		$this->menu = $menu;
		$this->good = $good;
	}

	public function getGroupMenusAndGoods(Request $request) {
		return response()->json([
			'groupMenus' => GroupMenusResource::collection($this->menu->getByGroupId($request->group_id)),
			'goods' => GoodResource::collection($this->good->getGoodsForSale($request->group_id)),
		]);
	}
}
