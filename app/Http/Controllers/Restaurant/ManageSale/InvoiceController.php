<?php

namespace App\Http\Controllers\Restaurant\ManageSale;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\ReturnedInvoiceContract;
use App\Contracts\Restaurant\TableContract;
use App\Contracts\Restaurant\GoodContract;
use App\Http\Controllers\Controller;
use App\Http\Resources\Invoice as InvoiceResource;
use App\Http\Resources\InvoiceCollection;
use App\Http\Resources\Table as TableResource;
use App\Models\Invoice;
use App\Events\Restaurant\Manage\InvoiceCreated as ManageInvoiceCreated;
use App\Events\Restaurant\Manage\InvoiceUpdated as ManageInvoiceUpdated;
use App\Events\Restaurant\ManageSale\PurchaseInvoiceCheckouted;
use App\Jobs\Restaurant\NotifyInvoiceUpdatedGoods;
use App\Jobs\Restaurant\NotifyInvoiceChangedTables;
use App\Jobs\Restaurant\NotifyInvoiceUpdatedNote;
use App\Jobs\Restaurant\NotifyInvoiceCombinedTables;
use App\Jobs\Restaurant\NotifyInvoiceCombinedInvoices;
use App\Jobs\Restaurant\NotifyInvoiceUpdatedDiscount;
use App\Jobs\Restaurant\NotifyInvoicePurchased;
use App\Jobs\Restaurant\NotifyInvoiceChangedStatus;

class InvoiceController extends Controller
{
	private $invoice;
	private $returnedInvoice;
	private $table;
	private $good;

	public function __construct(InvoiceContract $invoice, ReturnedInvoiceContract $returnedInvoice, TableContract $table, GoodContract $good)
	{
		$this->invoice = $invoice;
		$this->returnedInvoice = $returnedInvoice;
		$this->table = $table;
		$this->good = $good;
	}

	/**
	 * Lấy danh sách 500 hóa đơn mới nhất trong trang lịch sử
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function getListInvoices(Request $request)
	{
		$invoices = $this->invoice->getListLatestInvoices($request->group_id, 500);
		$currentInvoice = $invoices->first();
		$invoices = $invoices->map(function ($item) {
			$invoice = $item->only(['_id', 'code', 'status', 'recipient', 'total', 'created_at']);
			$invoice['recipient'] = $item->recipient->only(['first_name', 'last_name']);
			return $invoice;
		});
		$invoices = $invoices->groupBy(function ($item) {
			return Carbon::parse($item['created_at'])->format('d-m-Y');
		});
		return response()->json([
			'invoices' => $invoices,
			'current_invoice' => $currentInvoice
		]);
	}

	/**
	 * Lấy thông tin hóa đơn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function getInvoice(Request $request)
	{
		$invoice = $this->invoice->getById($request->id);
		return response()->json([
			'invoice' => $invoice->load('recipient')
		]);
	}

	/**
	 * Lấy danh sách các hóa đơn đang được khởi tạo và danh sách bàn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function getInvoicesAndTables(Request $request)
	{
		if (!empty($request->place_ids)) {
			$tableIds = $this->table->getByPlaceIds($request->place_ids)->modelKeys();
			$invoices = $this->invoice->getInitedInvoicesByTableIds($request->group_id, $tableIds);
		} else {
			$invoices = $this->invoice->getInvoicesByStatus($request->all());
		}
		return response()->json([
			'invoices' => InvoiceResource::collection($invoices),
			'tables' => TableResource::collection($this->table->getByGroupId($request->group_id, ['place']))
		]);
	}


	/**
	 * Nhân viên tạo hóa đơn mua hàng
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function createPurchaseInvoice(Request $request)
	{
		$this->authorize('create', [\App\Models\Invoice::class, $request->group_id]);

		$request->validate([
			'table_ids' => 'present|array',
			'table_ids.*' => [
				'required', 'string',
				Rule::exists('tables', '_id')->where(function ($query) use ($request) {
					$query->where([
						['group_id', $request->group_id],
						['status', $this->table->getAvailableType()]
					]);
				})
			],
			'goods' => 'present|array',
			'goods.*.id' => [
				'required', 'string',
				Rule::exists('goods', '_id')->where(function ($query) use ($request) {
					$query->where('group_id', $request->group_id);
				})
			],
			'discount' => 'required|numeric',
			'discount_type' => [
				'required', 'numeric',
				Rule::in($this->invoice->getDiscountTypes())
			]
		]);

		// Thiết lập dữ liệu cho goods
		$invoiceItems = [];
		foreach ($request->goods as $item) {
			$itemModel = $this->good->getById($item['id']);
			$itemChanged = $itemModel->only(['id', 'code', 'name', 'price', 'price_origin', 'unit']);
			$itemChanged['discount'] = $item['discount'];
			$itemChanged['discount_type'] = $item['discount_type'];
			$itemChanged['image'] = $itemModel->images[0] ?? '';
			$itemChanged['goods_order'] = [];
			foreach ($item['goods_order'] as $itemOrder) {
				$itemOrderChanged = [];
				$itemOrderChanged['is_disable'] = $itemOrder['is_disable'] ?? false;
				$itemOrderChanged['order_number'] = $itemOrder['order_number'];
				$itemOrderChanged['status'] = $itemOrder['status'];
				$itemOrderChanged['created_at'] = $itemOrder['created_at'] ?? Carbon::now()->toDateTimeString();
				$itemChanged['goods_order'][] = $itemOrderChanged;
			}
			$invoiceItems[] = $itemChanged;
		}

		$invoice = $this->invoice->create([
			'group_id' => $request->group_id,
			'table_ids' => $request->table_ids,
			'code' => $this->invoice->generateCode($request->group_id),
			'status' => $this->invoice->getStatusInited(),
			'user_ids' => [],
			'goods' => $invoiceItems,
			'discount' => $request->discount,
			'discount_type' => $request->discount_type,
			'request_status' => $this->invoice->getStatusNoneRequest()
		]);

		return response()->json([
			'invoice' => new InvoiceResource($invoice)
		]);
	}

	/**
	 * Api test người dùng tạo hóa đơn mua hàng
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function userCreatePurchaseInvoice(Request $request)
	{
		$request->validate([
			'table_ids' => 'present|array',
			'table_ids.*' => [
				'required', 'string',
				Rule::exists('tables', '_id')->where(function ($query) use ($request) {
					$query->where([
						['group_id', $request->group_id],
						['status', $this->table->getAvailableType()]
					]);
				})
			]
		]);

		$invoice = $this->invoice->create([
			'group_id' => $request->group_id,
			'table_ids' => $request->table_ids,
			'code' => $this->invoice->generateCode($request->group_id),
			'status' => $this->invoice->getStatusInited(),
			'user_ids' => $request->user_ids,
			'goods' => [],
			'request_status' => $this->invoice->getStatusCreateRequest()
		]);

		broadcast(new ManageInvoiceCreated($invoice));

		return response()->json([
			'id' => $invoice->_id,
			'code' => $invoice->code
		]);
	}


	/**
	 * Lấy ra dữ liệu good trong hóa đơn từ dữ liệu người dùng gửi lên
	 * kết hợp với dữ liệu good trong DB
	 *
	 * @param      array  $item   The item
	 */
	private function getInvoiceItem(array $item)
	{
		$itemModel = $this->good->getById($item['id']);
		$itemChanged = $itemModel->only(['id', 'code', 'name', 'price', 'price_origin', 'unit']);
		$itemChanged['image'] = $itemModel->images[0] ?? '';
		if (!empty($item['count_return'])) {
			$itemChanged['count_return'] = $item['count_return'];
		}
		$itemChanged['discount'] = $item['discount'];
		$itemChanged['discount_type'] = $item['discount_type'];
		$itemChanged['goods_order'] = [];
		foreach ($item['goods_order'] as $itemOrder) {
			$itemOrderChanged = [
				'is_disable' => $itemOrder['is_disable'] ?? false,
				'order_number' => $itemOrder['order_number'],
				'status' => $itemOrder['status'],
				'created_at' => $itemOrder['created_at'] ?? Carbon::now()->toDateTimeString()
			];
			$itemChanged['goods_order'][] = $itemOrderChanged;
		}
		return $itemChanged;
	}

	/**
	 * Cập nhật món ăn của hóa đơn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function updateGoods(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$request->validate([
			'id' => [
				'required', 'string',
				Rule::exists('invoices', '_id')->where(function ($query) use ($request) {
					$query->where('group_id', $request->group_id);
				})
			],
			'goods' => 'present|array',
			'goods.*.id' => [
				'required', 'string',
				Rule::exists('goods', '_id')->where(function ($query) use ($request) {
					$query->where('group_id', $request->group_id);
				})
			],
			'send_notify' => 'required|boolean'
		]);

		$invoiceItems = []; // Chứa danh sách các items sau khi merge
		$invoiceItemsChanged = []; // Chứa danh sách items thay đổi
		$oldInvoiceItems = $this->invoice->getById($request->id)->goods;
		$oldInvoiceItemIds = array_map(function($item) {
			return $item['id'];
		}, $oldInvoiceItems);
		$isEmptyInvoiceItems = empty($oldInvoiceItems);

		/**
		 * Từ dữ liệu người dùng gửi lên kết hợp với dữ liệu trong DB để đưa ra dữ 
		 * liệu goods hoàn chỉnh
		 */
		foreach ($request->goods as $item) {
			$itemChanged = $this->getInvoiceItem($item);

			// Goods trong hóa đơn đang trống
			if ($isEmptyInvoiceItems) {
				$invoiceItems[] = $itemChanged;
				continue;
			};

			// Goods mới chưa có id trong hóa đơn
			if (!in_array($item['id'], $oldInvoiceItemIds)) {
				$invoiceItems[] = $itemChanged;
			} else {
				$invoiceItemsChanged[] = $itemChanged;
			}
		}

		// Merge dữ liệu người dùng thay đổi với dữ liệu ở trên server
		if (!$isEmptyInvoiceItems) {
			foreach ($oldInvoiceItems as $item) {
				$itemChanged = collect($invoiceItemsChanged)->first(function ($itemChanged) use ($item) {
					return $itemChanged['id'] === $item['id'];
				});

				if ($itemChanged) {
					if (!empty($itemChanged['count_return'])) {
						$item['count_return'] = $itemChanged['count_return'];
					}
					$item['discount'] = $itemChanged['discount'];
					$item['discount_type'] = $itemChanged['discount_type'];
					$oldItemsInvoice = $item['goods_order'];

					// Thêm goods_order mới vào
					$itemsInvoice = collect($itemChanged['goods_order'])->filter(function ($itemOrderChanged) use ($oldItemsInvoice) {
						return collect($oldItemsInvoice)->every(function ($itemOrder) use ($itemOrderChanged) {
							return $itemOrder['status'] !== $itemOrderChanged['status'];
						});
					})->toArray();

					// Cập nhật goods_order đang tồn tại
					foreach ($item['goods_order'] as $itemOrder) {
						$itemOrderChanged = collect($itemChanged['goods_order'])->first(function ($itemOrderChanged) use ($itemOrder) {
							return $itemOrderChanged['status'] === $itemOrder['status'];
						});
						if ($itemOrderChanged) {
							$itemsInvoice[] = $itemOrderChanged;
						} else {
							$itemsInvoice[] = $itemOrder;
						}
					}
					$item['goods_order'] = $itemsInvoice;
				}
				$invoiceItems[] = $item;
			}
		}

		$invoice = $this->invoice->getById($request->id);
		$invoice->goods = $invoiceItems;
		$invoice->save();

		if (!empty($invoice->user_ids) && $request->send_notify) {
			NotifyInvoiceUpdatedGoods::dispatch($invoice);
		}

		return response()->json([
			'invoice' => new InvoiceResource($invoice)
		]);
	}

	/**
	 * Cập nhật trạng thái của các món ăn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function updateStatusGoods(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$request->validate([
			'id' => [
				'required', 'string',
				Rule::exists('invoices', '_id')->where(function ($query) use ($request) {
					$query->where('group_id', $request->group_id);
				})
			],
			'items_updated' => 'present|array',
			'items_updated.*.id' => [
				'required', 'string',
				Rule::exists('goods', '_id')->where(function ($query) use ($request) {
					$query->where('group_id', $request->group_id);
				})
			],
			'status' => [
				'required', 'numeric',
				Rule::in($this->invoice->getGoodStatus())
			]
		]);

		$invoice = $this->invoice->getById($request->id);
		$invoiceItems = $invoice->goods;
		$itemsUpdated = [];

		foreach ($invoiceItems as $item) {
			$itemsOrderUpdated = array_filter($request->items_updated, function ($itemUpdated) use ($item) {
				return $itemUpdated['id'] === $item['id'];
			});

			// Good trong Invoice không có thay đổi
			if (!$itemsOrderUpdated) {
				$itemsUpdated[] = $item;
				continue;
			}

			// Mảng status của những good_order thay đổi
			$itemStatus = array_map(function ($itemOrderUpdated) {
				return $itemOrderUpdated['status'];
			}, $itemsOrderUpdated);

			$item['goods_order'] = collect($item['goods_order'])->map(function ($itemOrder) use ($itemStatus, $request) {
				if (in_array($itemOrder['status'], $itemStatus)) {
					$itemOrder['status'] = $request->status;
				}
				return $itemOrder;
			})->toArray();

			$itemsUpdated[] = $item;
		}

		$invoice->goods = $this->mergeItemsSameStatus($itemsUpdated);
		$invoice->save();

		if (!empty($invoice->user_ids)) {
			NotifyInvoiceChangedStatus::dispatch($invoice);
		}
	}

	/**
	 * Xóa món ăn trong hóa đơn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function deleteItem(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$request->validate($this->validateInvoiceId($request->group_id));

		$invoice = $this->invoice->getById($request->id);
		$items = $invoice->goods;

		$iItem = -1;
		foreach ($items as $index => $item) {
			if ($item['id'] === $request->item_id) {
				$iItem = $index;
				break;
			}
		}
		if ($iItem === -1) return;
		$itemsOrder = $items[$iItem]['goods_order'] ?? [];
		$iItemOrder = -1;
		foreach ($itemsOrder as $index => $itemOrder) {
			if ($itemOrder['status'] === $this->invoice->getGoodBookedStatus()) {
				$iItemOrder = $index;
				break;
			}
		}
		if ($iItemOrder === -1) return;
		array_splice($itemsOrder, $iItemOrder, 1);
		$items[$iItem]['goods_order'] = $itemsOrder;
		$invoice->goods = $items;

		$invoice->save();

		if (!empty($invoice->user_ids)) {
			NotifyInvoiceDeletedGoods::dispatch($invoice);
		}
	}

	/**
	 * Xóa hóa đơn
	 *
	 * @param  mixed $request
	 * @param  mixed $invoiceId
	 *
	 * @return void
	 */
	public function deleteInvoice(Request $request, String $invoiceId)
	{
		$this->checkInvoiceCanDelete($request->group_id);

		$invoice = $this->invoice->getById($invoiceId);
		if ($invoice && $invoice->status === $this->invoice->getStatusPurchased()) {
			return response()->json(['error' => ''], 422);
		}

		$invoice->delete();
		$this->table->massUpdate($invoice->table_ids ?? [], [
			'status'     => $this->table->getAvailableType(),
			'invoice_id' => ''
		]);
		return response()->json([
			'message' => 'Success'
		]);
	}

	/**
	 * Thay đổi trạng thái món ăn trong danh sách các hóa đơn có chứa món ăn đó
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function changeStatusGood(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$invoices = $this->invoice->getInitedInvoicesHasGoodId($request->group_id, $request->good_id);

		$invoicesUpdate = [];

		$goodBooked = $this->invoice->getGoodBookedStatus();
		$goodActive = $this->good->getStatusActive();
		$goodInactive = $this->good->getStatusInactive();

		foreach ($invoices as $invoice) {
			$items = $invoice->goods;

			$item = Arr::first($items, function ($item) use ($request) {
				return $item['id'] === $request->good_id;
			});

			if ($item) {
				$itemsOrder = $item['goods_order'];
				$itemsOrderChanged = [];
				$update = false;

				foreach ($itemsOrder as $itemOrder) {
					if ($itemOrder['status'] === $goodBooked) {
						if (empty($itemOrder['is_disable']) && $request->good_status === $goodInactive) {
							$itemOrder['is_disable'] = true;
							$update = true;
						} elseif ($itemOrder['is_disable'] === true && $request->good_status === $goodActive) {
							$itemOrder['is_disable'] = false;
							$update = true;
						}
					}
					$itemsOrderChanged[] = $itemOrder;
				}

				// Chỉ cập nhật những hóa đơn nào thay đổi
				if ($update) {
					$invoicesUpdate[] = [
						'id' => $invoice->_id,
						'goods_order' => $itemsOrderChanged
					];
				}
			}
		}

		foreach ($invoicesUpdate as $invoiceUpdate) {
			$this->invoice->updateGoodsOrderInvoice($invoiceUpdate['id'], $request->good_id, $invoiceUpdate['goods_order']);
		}
	}

	/**
	 * Thay đổi bàn cho hóa đơn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function setInvoiceTables(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$request->validate($this->validateInvoiceId($request->group_id));

		$invoice = $this->invoice->getById($request->id);

		$actingTables = $request->table_ids;
		$availableTables = array_filter($invoice->table_ids, function ($tableId) use ($actingTables) {
			return !in_array($tableId, $actingTables);
		});

		$invoice->table_ids = $request->table_ids;
		$invoice->save();

		$this->table->massUpdate($actingTables, [
			'status' => $this->table->getActingType(),
			'invoice_id' => $invoice->id
		]);
		$this->table->massUpdate($availableTables, [
			'status' => $this->table->getAvailableType(),
			'invoice_id' => ''
		]);

		if ($invoice && !empty($invoice->user_ids)) {
			NotifyInvoiceChangedTables::dispatch($invoice);
		}
	}

	/**
	 * Thiết lập lưu ý cho hóa đơn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function setNote(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$request->validate($this->validateInvoiceId($request->group_id));

		$invoice = $this->invoice->getById($request->id);
		$invoice->note = $request->note;
		$invoice->save();

		if (!empty($invoice->user_ids)) {
			NotifyInvoiceUpdatedNote::dispatch($invoice);
		}
	}

	/**
	 * Thiết lập giảm giá cho hóa đơn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function setDiscount(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$request->validate([
			'id' => [
				'required', 'string',
				Rule::exists('invoices', '_id')->where(function ($query) use ($request) {
					$query->where('group_id', $request->group_id);
				})
			],
			'discount' => 'required|numeric',
			'discount_type' => [
				'required', 'numeric',
				Rule::in($this->invoice->getDiscountTypes())
			]
		]);

		$invoice = $this->invoice->getById($request->id);
		$invoice->discount = $request->discount;
		$invoice->discount_type = $request->discount_type;
		$invoice->save();

		if (!empty($invoice->user_ids)) {
			NotifyInvoiceUpdatedDiscount::dispatch($invoice);
		}
	}

	/**
	 * Gộp bàn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function combineTables(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$request->validate([
			'table_ids' => 'required|array',
			'table_ids.*' => [
				'required', 'string',
				Rule::exists('tables', '_id')->where(function ($query) use ($request) {
					$query->where([
						['group_id', $request->group_id],
						['status', $this->table->getActingType()]
					]);
				})
			],
			'current_invoice_id' => [
				'required', 'string',
				Rule::exists('invoices', '_id')->where(function ($query) use ($request) {
					$query->where([
						['group_id', $request->group_id],
						['status', $this->invoice->getStatusInited()]
					]);
				})
			]
		]);

		$currentInvoice = $this->invoice->getById($request->current_invoice_id);

		$itemsCurrentInvoice = $this->mergeItemsSameStatus($currentInvoice->goods);

		$invoiceIds = array_column($this->invoice->getInitedInvoicesByTableIds($request->group_id, $request->table_ids)->toArray(), '_id');

		$invoicesCombined = $this->invoice->getByIds($invoiceIds);

		list($itemsCurrentInvoice, $userIds, $tableIds, $totalDiscount) = $this->mergeInvoices($invoicesCombined, $itemsCurrentInvoice);

		// Danh sách chứa tất cả id user được gộp vào
		$userIds = array_merge($currentInvoice->user_ids, $userIds);
		
		// Tổng giá trị tiền discount của các hóa đơn gộp vào và hóa đơn hiện tại
		$totalDiscount += $this->priceDiscountInvoice($currentInvoice);

		$currentInvoice->user_ids = $userIds;
		$currentInvoice->goods = $itemsCurrentInvoice;
		$currentInvoice->note = $request->note_content;
		$currentInvoice->discount = $totalDiscount;
		$currentInvoice->discount_type = $this->invoice->getDiscountTypeFixed();

		$currentInvoice->save();

		$this->table->massUpdate($tableIds, [
			'status'     => $this->table->getAvailableType(),
			'invoice_id' => ''
		]);
		$this->invoice->massUpdate($invoiceIds, [
			'status' => $this->invoice->getStatusDisable()
		]);

		if (!empty($userIds)) {
			NotifyInvoiceCombinedTables::dispatch($currentInvoice);
		}

		return response()->json([
			'current_invoice' => new InvoiceResource($currentInvoice),
			'table_ids' => $tableIds,
			'invoice_ids' => $invoiceIds
		]);
	}

	/**
	 * Gộp hóa đơn
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function combineInvoices(Request $request)
	{
		$this->checkInvoiceCanUpdate($request->group_id);

		$request->validate([
			'invoice_ids' => 'required|array',
			'invoice_ids.*' => [
				'required', 'string',
				Rule::exists('invoices', '_id')->where(function ($query) use ($request) {
					$query->where([
						['group_id', $request->group_id],
						['status', $this->invoice->getStatusInited()]
					]);
				})
			],
			'current_invoice_id' => [
				'required', 'string',
				Rule::exists('invoices', '_id')->where(function ($query) use ($request) {
					$query->where([
						['group_id', $request->group_id],
						['status', $this->invoice->getStatusInited()]
					]);
				})
			]
		]);

		$currentInvoice = $this->invoice->getById($request->current_invoice_id);

		$itemsCurrentInvoice = $this->mergeItemsSameStatus($currentInvoice->goods);

		$invoicesCombined = $this->invoice->getByIds($request->invoice_ids);

		list($itemsCurrentInvoice, $userIds, $tableIds, $totalDiscount) = $this->mergeInvoices($invoicesCombined, $itemsCurrentInvoice);

		// Danh sách chứa tất cả id user được gộp vào
		$userIds = array_merge($currentInvoice->user_ids, $userIds);
		// Danh sách chứa tất cả id table sau khi được gộp
		$tableIds = array_merge($currentInvoice->table_ids, $tableIds);
		// Tổng giá trị tiền discount của các hóa đơn gộp vào và hóa đơn hiện tại
		$totalDiscount += $this->priceDiscountInvoice($currentInvoice);

		$currentInvoice->user_ids = $userIds;
		$currentInvoice->table_ids = $tableIds;
		$currentInvoice->goods = $itemsCurrentInvoice;
		$currentInvoice->note = $request->note_content;
		$currentInvoice->discount = $totalDiscount;
		$currentInvoice->discount_type = $this->invoice->getDiscountTypeFixed();

		$currentInvoice->save();

		$this->invoice->massUpdate($request->invoice_ids, [
			'status' => $this->invoice->getStatusDisable()
		]);

		if (!empty($userIds)) {
			NotifyInvoiceCombinedInvoices::dispatch($currentInvoice);
		}

		return response()->json([
			'current_invoice' => new InvoiceResource($currentInvoice),
			'invoice_ids' => $request->invoice_ids
		]);
	}

	/**
	 * Thanh toán hóa đơn mua hàng
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function checkoutPurchaseInvoice(Request $request)
	{
		$this->checkInvoiceCanCheckout($request->group_id);

		$request->validate($this->validateInvoiceId($request->group_id));

		$invoice = $this->invoice->getById($request->id);

		$items = $invoice->goods;
		$itemsInvoice = [];
		$subtotalInvoice = 0;
		$totalInvoice = 0;

		foreach ($items as $item) {
			$count = collect($item['goods_order'])->sum('order_number');
			if (!empty($item['count_return'])) {
				$count -= $item['count_return'];
			}
			$discountItemData = [
				'price' => $item['price'],
				'discount' => $item['discount'] ?? 0,
				'discount_type' => $item['discount_type'] ?? $this->invoice->getDiscountTypeFixed(),
				'count' => $count
			];
			$subtotalInvoice += $this->priceItemAfterDiscount($discountItemData);
			$itemsInvoice[] = collect($item)->reject(function ($value, $key) {
				return $key === 'goods_order';
			})
				->put('count', $count)
				->toArray();
		}

		$discountInvoiceData = [
			'price' => $subtotalInvoice,
			'discount' => $invoice->discount ?? 0,
			'discount_type' => $invoice->discount_type ?? $this->invoice->getDiscountTypeFixed()
		];

		$discountInvoice = $this->priceDiscount($discountInvoiceData);

		$invoice->recipient_id = auth('api')->user()->_id;
		$invoice->status = $this->invoice->getStatusPurchased();
		$invoice->goods = $itemsInvoice;
		$invoice->total = $subtotalInvoice - $discountInvoice;
		$invoice->save();

		// Tính toán lại số lượng hàng tồn kho
		event(new PurchaseInvoiceCheckouted($invoice));

		if (!empty($invoice->user_ids)) {
			NotifyInvoicePurchased::dispatch($invoice);
		}

		$this->table->massUpdate($invoice->table_ids ?? [], [
			'status'     => $this->table->getAvailableType(),
			'invoice_id' => ''
		]);
	}

	/**
	 * Api test yêu cầu đặt món của khách hàng
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function userRequireOrderGoods(Request $request)
	{
		$invoice = $this->invoice->getById($request->invoice_id);

		if (!in_array($request->user_id, $invoice->user_ids)) {
			// return;
		}

		$request->validate([
			'goods' => 'required|array',
			'goods.*.id' => [
				'required', 'string',
				Rule::exists('goods', '_id')->where(function ($query) use ($request) {
					$query->where('group_id', $request->group_id);
				})
			]
		]);

		$invoice->request_status = $this->invoice->getStatusAdditionRequest();
		$invoice->goods = $request->goods;
		$invoice->save();

		broadcast(new ManageInvoiceUpdated($invoice));
	}

	/**
	 * Api test yêu cầu thanh toán của khách hàng
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function userRequireCheckoutPurchaseInvoice(Request $request)
	{
		$invoice = $this->invoice->getById($request->invoice_id);

		if (!in_array($request->user_id, $invoice->user_ids)) {
			// return;
		}

		$invoice->request_status = $this->invoice->getStatusPurchaseRequest();
		$invoice->save();

		broadcast(new ManageInvoiceUpdated($invoice));
	}

	/**
	 * Lấy ra danh sách các hóa đơn bán hàng
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function getPurchasedInvoices(Request $request)
	{
		return response()->json([
			'invoices' => new InvoiceCollection(
				$this->invoice->paginateByStatus(
					$request->group_id,
					$this->invoice->getStatusPurchased(),
					10,
					$request->page
				)
			)
		]);
	}

	/**
	 * Thanh toán hóa đơn trả hàng
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function checkoutReturnInvoice(Request $request)
	{
		$this->authorize('create', [\App\Models\ReturnedInvoice::class, $request->group_id]);

		$request->validate([
			'invoice_id' => [
				'required',
				Rule::exists('invoices', '_id')->where(function ($query) {
					$query->where('status', $this->invoice->getStatusPurchased());
				})
			],
			'creator_id' => 'required|exists:users,_id',
			'goods' => 'required|array',
			'total' => 'required|numeric'
		]);

		$data = $request->all();
		$data['status'] = $this->returnedInvoice->getStatusPaid();
		$data['code'] = $this->returnedInvoice->generateCode($request->group_id);

		$returnedInvoice = $this->returnedInvoice->create($data);

		return response()->json([
			'code' => $returnedInvoice->code
		]);
	}

	/**
	 * Tìm kiếm hóa đơn mua hàng
	 *
	 * @param  mixed $request
	 *
	 * @return void
	 */
	public function searchPurchasedInvoices(Request $request)
	{
		$condition = [
			'group_id' 			=> $request->group_id,
			'status'			=> $this->invoice->getStatusPurchased(),
			'invoice_code'		=> $request->invoice_code
		];
		$condition['start_date'] = !empty($request->start_date) ? Carbon::createFromFormat('d-m-Y H:i:s', $request->start_date . '00:00:00') : null;
		$condition['end_date'] = !empty($request->end_date) ? Carbon::createFromFormat('d-m-Y H:i:s', $request->end_date . '23:59:59') : null;
		return response()->json([
			'invoices' => new InvoiceCollection($this->invoice->paginateSearchByCondition($condition, 10, $request->page))
		]);
	}

	/**
	 * Gộp những món ăn được đặt có cùng trạng thái
	 *
	 * @param  mixed $items
	 *
	 * @return void
	 */
	private function mergeItemsSameStatus($items)
	{
		$itemsInvoice = [];
		foreach ($items as $index => $item) {
			$itemsInvoice[$index] = $item;
			$itemsOrder = [];
			foreach ($item['goods_order'] as $itemOrder) {
				if (!isset($itemOrder['created_at'])) {
					$itemOrder['created_at'] = Carbon::now()->toDateTimeString();
				}
				$iItemOrder = array_search($itemOrder['status'], array_column($itemsOrder, 'status'));
				if ($iItemOrder !== false) {
					$itemsOrder[$iItemOrder]['order_number'] += $itemOrder['order_number'];

					// Thiết lập thời gian tạo món ăn là sớm nhất có thể
					$first = Carbon::parse($itemsOrder[$iItemOrder]['created_at']);
					$second = Carbon::parse($itemOrder['created_at']);
					if ($first->lessThan($second)) {
						$itemsOrder[$iItemOrder]['created_at'] = $itemOrder['created_at'];
					}
				} else {
					$itemsOrder[] = $itemOrder;
				}
			}
			$itemsInvoice[$index]['goods_order'] = $itemsOrder;
		}
		return $itemsInvoice;
	}

	/**
	 * Gộp hóa đơn
	 *
	 * @param  mixed $invoicesCombined
	 * @param  mixed $itemsCurrentInvoice
	 *
	 * @return void
	 */
	private function mergeInvoices($invoicesCombined, $itemsCurrentInvoice)
	{
		// Danh sách chứa tất cả id user được gộp vào
		$userIds = [];
		// Danh sách chứa tất cả id table được gộp vào
		$tableIds = [];
		// Tổng giá trị tiền discount của các hóa đơn gộp vào và hóa đơn hiện tại
		$totalDiscount = 0;

		foreach ($invoicesCombined as $invoice) {
			$tableIds = array_merge($tableIds, $invoice->table_ids);
			if (!empty($invoice->user_ids)) {
				$userIds = array_merge($userIds, $invoice->user_ids);
			}
			$totalDiscount += $this->priceDiscountInvoice($invoice);
			foreach ($invoice->goods as $item) {
				$iItemCurrentInvoice = array_search($item['code'], array_column($itemsCurrentInvoice, 'code'));
				if ($iItemCurrentInvoice !== false) {
					$itemCurrentInvoice = $itemsCurrentInvoice[$iItemCurrentInvoice];

					// Gộp phần count_return
					if (!empty($item['count_return'])) {
						if (!empty($itemCurrentInvoice['count_return'])) {
							$itemCurrentInvoice['count_return'] += $item['count_return'];
						} else {
							$itemCurrentInvoice['count_return'] = $item['count_return'];
						}
					}

					// Lấy discount của món được giảm giá nhiều hơn
					if ($this->priceDiscountMore($item, $itemCurrentInvoice)) {
						$itemCurrentInvoice['discount'] = $item['discount'];
						$itemCurrentInvoice['discount_type'] = $item['discount_type'];
					}

					// Merge order_number của những item có cùng status
					foreach ($item['goods_order'] as $itemOrder) {
						$iItemOrder = array_search($itemOrder['status'], array_column($itemCurrentInvoice['goods_order'], 'status'));
						if ($iItemOrder !== false) {
							$itemCurrentInvoice['goods_order'][$iItemOrder]['order_number'] += $itemOrder['order_number'];
						} else {
							$itemCurrentInvoice['goods_order'][] = $itemOrder;
						}
					}

					$itemsCurrentInvoice[$iItemCurrentInvoice] = $itemCurrentInvoice;
				} else {
					$itemsCurrentInvoice[] = $item;
				}
			}
		}

		return [$itemsCurrentInvoice, $userIds, $tableIds, $totalDiscount];
	}

	/**
	 * Lấy ra dữ liệu discount từ invoice hoặc good để xử lý
	 *
	 * @param  mixed $object
	 *
	 * @return void
	 */
	private function getDiscountData($object)
	{
		return [
			'discount' => $object['discount'] ?? 0,
			'discount_type' => $object['discount_type'] ?? $this->invoice->getDiscountTypeFixed(),
			'price' => $object['price']
		];
	}

	/**
	 * Tính toán giá giảm của invoice và cũng có thể của good
	 * 
	 * $data = [
	 * 		'price' => '',
	 * 		'discount' => '',
	 * 		'discount_type' => ''
	 * ];
	 *
	 * @param      integer  $data   The data
	 *
	 * @return     integer  ( description_of_the_return_value )
	 */


	/**
	 * Tính toán giá giảm của invoice và cũng có thể của good
	 * $data = [
	 * 		'price' => '',
	 * 		'discount' => '',
	 * 		'discount_type' => ''
	 * ];
	 *
	 * @param  mixed $data
	 *
	 * @return void
	 */
	private function priceDiscount($data)
	{
		return $data['discount_type'] === $this->invoice->getDiscountTypeFixed() ? (int)$data['discount'] : ((float)($data['discount'] / 100)) * $data['price'];
	}

	/**
	 * $itemA giảm giá nhiều hơn $itemB không?
	 *
	 * @param  mixed $itemA
	 * @param  mixed $itemB
	 *
	 * @return void
	 */
	private function priceDiscountMore($itemA, $itemB)
	{
		$itemAPrice = $this->priceDiscount($this->getDiscountData($itemA));
		$itemBPrice = $this->priceDiscount($this->getDiscountData($itemB));
		return $itemAPrice > $itemBPrice;
	}

	/**
	 * Giá của item sau khi trừ discount
	 *
	 * @param  mixed $data
	 *
	 * @return void
	 */
	private function priceItemAfterDiscount($data)
	{
		if ($data['discount_type'] === $this->invoice->getDiscountTypeFixed()) {
			return ($data['price'] - (int)$data['discount']) * $data['count'];
		} else {
			return ($data['price'] * (1 - (float)($data['discount'] / 100))) * $data['count'];
		}
	}

	/**
	 * Tính subtotal của hóa đơn
	 *
	 * @param  mixed $invoice
	 *
	 * @return void
	 */
	private function calcSubtotalInvoice(Invoice $invoice)
	{
		$subtotal = 0;
		$items = $invoice->goods ?? [];
		foreach ($items as $item) {
			$countItem = 0;
			foreach ($item['goods_order'] as $itemOrder) {
				$countItem += $itemOrder['order_number'];
			}
			if (!empty($item['count_return'])) {
				$countItem -= $item['count_return'];
			}
			$data = [
				'price' => $item['price'],
				'discount' => $item['discount'] ?? 0,
				'discount_type' => $item['discount_type'] ?? $this->invoice->getDiscountTypeFixed(),
				'count' => $countItem
			];
			$subtotal += $this->priceItemAfterDiscount($data);
		}
		return $subtotal;
	}

	/**
	 * Tính giá discount của hóa đơn
	 *
	 * @param  mixed $invoice
	 *
	 * @return void
	 */
	private function priceDiscountInvoice(Invoice $invoice)
	{
		$subtotal = $this->calcSubtotalInvoice($invoice);
		$data = [
			'price' => $subtotal,
			'discount' => $invoice->discount ?? 0,
			'discount_type' => $invoice->discount_type ?? $this->invoice->getDiscountTypeFixed()
		];
		return $this->priceDiscount($data);
	}

	/**
	 * Validate trường id (của invoice) của $request
	 *
	 * @param  mixed $groupId
	 *
	 * @return void
	 */
	private function validateInvoiceId($groupId)
	{
		return [
			'id' => [
				'required', 'string',
				Rule::exists('invoices', '_id')->where(function ($query) use ($groupId) {
					$query->where('group_id', $groupId);
				})
			]
		];
	}

	private function checkInvoiceCanUpdate(string $groupId)
	{
		$this->authorize('update', [\App\Models\Invoice::class, $groupId]);
	}

	private function checkInvoiceCanDelete(string $groupId)
	{
		$this->authorize('delete', [\App\Models\Invoice::class, $groupId]);
	}

	private function checkInvoiceCanCheckout(string $groupId)
	{
		$this->authorize('checkout', [\App\Models\Invoice::class, $groupId]);
	}
}
