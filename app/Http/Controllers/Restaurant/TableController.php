<?php

namespace App\Http\Controllers\Restaurant;

use App\Contracts\Restaurant\PlaceContract;
use App\Contracts\Restaurant\TableContract;
use App\Http\Controllers\Controller;
use App\Http\Resources\Table as TableResource;
use App\Http\Resources\Place as PlaceResource;
use App\Http\Resources\TableCollection;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TableController extends Controller
{
    private $table;
    private $place;

    public function __construct(TableContract $table, PlaceContract $place)
    {
        $this->table = $table;
        $this->place = $place;
    }

    public function list(Request $request)
    {
        $tables = $this->table->paginateByGroupId($request->group_id, $request->page, ['place:name']);
        $places = $this->place->getByGroupId($request->group_id);

        return response()->json([
            'tables' => new TableCollection($tables),
            'places' => PlaceResource::collection($places),
        ]);
    }

    public function search(Request $request)
    {
        $tables = $this->table->getByCondition($request->all(), ['place'], $request->page);

        return response()->json([
            'tables' => new TableCollection($tables)
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('tables', 'name')->where(function($query) use ($request) {
                    return $query->where('place_id', $request->place_id)->whereNull('deleted_at');
                })
            ],
            'sort' => 'nullable|numeric|min:0',
            'count_seat' => 'required|numeric|min:0',
            'description' => 'nullable|string|max:1024',
            'place_id'   => [
                'required', 'string',
                Rule::exists('places', '_id')->where(function ($query) use ($request) {
                    return $query->where('group_id', $request->group_id)->whereNull('deleted_at');
                })
            ]
        ]);
        
        $request['status'] = $this->table->getAvailableType();

        $this->table->createTable($request->all());

        return response()->json([
            'message' => __('restaurant.table.create_success')
        ]);
    }

    public function import(Request $request)
    {
        $request->validate([
            'sort' => 'nullable|numeric|min:0',
            'count_seat' => 'required|numeric|min:0',
            'place_name' => [
                'required', 'string',
                Rule::exists('places', 'name')
                    ->where('group_id', $request->group_id)
                    ->whereNull('deleted_at')
            ]
        ]);
        $place = $this->place->getByName($request->group_id, $request->place_name);
        if ($place) {
            $request->validate([
                'name' => [
                    'required',
                    Rule::unique('tables', 'name')->where(function($query) use ($place) {
                        return $query->where('place_id', $place->id)->whereNull('deleted_at');
                    })
                ],
            ]);
        }
        $code = config('const.status.fail');
        $message = __('import_export.error_response');
        $request['status'] = $this->table->getAvailableType();
        try {
            $this->table->createTable($request->all());
            $code = config('const.status.success');
            $message = __('word.success');
        } catch (\Exception $exception) {
        }
        return response()->json([
            'message' => $message,
            'code' => $code
        ]);
    }

    public function edit(Request $request, $tableId)
    {
        $this->checkTableBelongsToGroup($tableId, $request->group_id);
        $table = $this->table->getById($tableId, ['place']);
        $places = $this->place->getByGroupId($request->group_id);

        return response()->json([
            'table' => new TableResource($table),
            'places' => PlaceResource::collection($places)
        ]);
    }

    public function update(Request $request, $tableId)
    {
        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('tables', 'name')->ignore($tableId, '_id')->where(function($query) use ($request) {
                    return $query->where('place_id', $request->place_id);
                })
            ],
            'place_id'   => [
                'required', 'string',
                Rule::exists('places', '_id')->where(function ($query) use ($request) {
                    $query->where('group_id', $request->group_id);
                })
            ],
            'sort' => 'nullable|numeric|min:0',
            'count_seat' => 'required|numeric|min:0',
            'description' => 'nullable|string',
        ]);

        $this->checkTableBelongsToGroup($tableId, $request->group_id);

        $this->table->update($tableId, $request->all());

        return response()->json([
            'message' => __('restaurant.table.update_success')
        ]);
    }

    public function delete(Request $request, $tableId)
    {
        $this->checkTableBelongsToGroup($tableId, $request->group_id);
        $this->table->delete($tableId);
        return response()->json([
            'message' => __('restaurant.table.delete_success')
        ]);
    }

    private function checkTableBelongsToGroup(string $tableId, string $groupId)
    {
        $table = $this->table->getById($tableId);
        $this->authorize('belongsToGroup', [$table, $groupId]);
    }

    private function checkPlaceBelongsToGroup(string $placeId, string $groupId)
    {
        $place = $this->place->getById($placeId);
        $this->authorize('belongsToGroup', [$place, $groupId]);
    }
}
