<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\Restaurant\GoodTrait;
use App\Http\Controllers\Traits\Restaurant\GoodProcessedTrait;
use App\Contracts\Restaurant\GoodContract;

class ProcessedGoodController extends Controller
{
    use GoodTrait, GoodProcessedTrait;

    const SUCCESS_STATUS = 200;
    private $good;

    public function __construct(GoodContract $good)
    {
        $this->good = $good;
    }
}
