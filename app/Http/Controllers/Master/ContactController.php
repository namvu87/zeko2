<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Master\ContactContract;
use App\Http\Resources\ContactCollection;

class ContactController extends Controller
{
    private $contact;

    public function __construct(ContactContract $contact)
    {
        $this->contact = $contact;
    }

    public function listCustomerCare()
    {
        return response()->json([
                'customerCareList' => $this->contact->getCustomerCare()
            ]);
    }

    public function storeCustomerCare(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required|email'
        ]);

        $this->contact->storeCustomerCare($request->only(['name', 'phone_number', 'skype', 'zalo', 'email']));
    
        return response()->json([
            'message' => __('master.customer_care_manager.create_customer_care_success')
        ]);
    }

    public function deleteCustomerCare(string $customerCareId)
    {
        $this->contact->delete($customerCareId);

        return response()->json([
            'message' => __('master.customer_care_manager.delete_customer_care_success')
        ]);
    }

    public function editCustomerCare(Request $request, string $customerCareId)
    {
        return response()->json([
                'customerCare' => $this->contact->getById($customerCareId)
            ]);
    }

    public function updateCustomerCare(Request $request, string $customerCareId)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required|email'
        ]);

        $this->contact->updateCustomerCare($request->only(['name', 'phone_number', 'skype', 'zalo', 'email']), $customerCareId);

        return response()->json([
            'message' => __('master.customer_care_manager.update_customer_care_success')
        ]);
    }

    public function getContactInformation(Request $request)
    {
        $contactInformation = $this->contact->getContactInformation();
        return response()->json([
            'contact_information' => $contactInformation
        ]);
    }

    public function setContactInformation(Request $request)
    {
        $this->validate($request, [
            'address' => 'required|string',
            'phone_number' => 'required|numeric',
            'fax' => 'nullable|string',
            'fb' => 'nullable|url',
        ]);

        $this->contact->setContactInformation($request->only(['address', 'phone_number', 'fax', 'fb']));

        return response()->json([
            'message' => __('master.contact.update_contact_success')
        ]);
    }
}
