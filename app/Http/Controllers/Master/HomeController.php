<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\GroupContract;
use App\Contracts\Master\ProductContract;

class HomeController extends Controller
{
    private $group;

    private $order;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function getData()
    {
        return response()->json([
            'totalGroup' => $this->group->getAll()->count(),
            'newGroups' => $this->group->getNewGroups()
        ]);
    }
}
