<?php

namespace App\Http\Controllers\Master;

use App\Contracts\Master\TagContract;
use App\Contracts\Master\UserGuideContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserGuideController extends Controller
{
    private $guide;
    private $tag;

    public function __construct(UserGuideContract $guide, TagContract $tag)
    {
        $this->guide = $guide;
        $this->tag = $tag;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        return response()->json([
            'guides' => $this->guide->getAllGuide($request->status)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'tags' => $this->tag->getAll()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required|unique:user_guides',
            'meta_title' => 'required',
            'meta_description' => 'required',
            'sort' => 'required|integer|min:0',
            'content' => 'required',
            'hashes' => 'nullable|array',
            'tags' => 'required|array',
            'service_type' => 'required|array',
            'service_type.*' => 'integer|in:' . join(array_keys(__('configuration.service')), ','),
            'category' => 'required|integer|in:' . join(array_keys(__('configuration.category')), ',')
        ]);
        $request['sort'] = (int)$request->sort;
        $request['category'] = (int)$request->category;
        $request['content'] = str_replace("<p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>", '', $request->content);
        $this->guide->create($request->all());
        foreach ($request->tags as $key => $tag) {
            if (!($this->tag->getByName($tag))) {
                $this->tag->create([
                    'name' => $tag,
                    'status' => $this->tag->statusVisiable()
                ]);
            }
        }
        return response()->json([
            'message' => __('word.success')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserGuide $userGuide
     *
     * @return \Illuminate\Http\Response
     */
    public function show(UserGuide $userGuide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserGuide $userGuide
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return response()->json([
            'tags' => $this->tag->getAll(),
            'guide' => $this->guide->getById($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\UserGuide $userGuide
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required|unique:user_guides,slug,' . $id . ',_id',
            'meta_title' => 'required',
            'meta_description' => 'required',
            'sort' => 'required|integer|min:0',
            'content' => 'required',
            'hashes' => 'nullable|array',
            'tags' => 'required|array',
            'service_type' => 'required|array',
            'service_type.*' => 'integer|in:' . join(array_keys(__('configuration.service')), ','),
            'category' => 'required|integer|in:' . join(array_keys(__('configuration.category')), ',')
        ]);
        $request['sort'] = (int)$request->sort;
        $request['category'] = (int)$request->category;
        $request['content'] = str_replace("<p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>", '', $request->content);
        $this->guide->update($id, $request->all());
        foreach ($request->tags as $key => $tag) {
            if (!($this->tag->getByName($tag))) {
                $this->tag->create([
                    'name' => $tag,
                    'status' => $this->tag->statusVisiable()
                ]);
            }
        }
        return response()->json([
            'message' => __('word.success')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserGuide $userGuide
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->guide->delete($id);
        return response()->json([
            'message' => __('word.success')
        ]);
    }

    /**
     * restore user guide
     *
     * @param  \App\UserGuide $userGuide
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->guide->restore($id);
        return response()->json([
            'message' => __('word.success')
        ]);
    }
}
