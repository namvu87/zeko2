<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ImageManagerController extends Controller
{
    const SUCCESS_STATUS = 1;
    const ERROR_STATUS = 0;
    const UPLOAD_PATH = '/upload/editor/';

    public function list()
    {
        try {
            $path = public_path() . self::UPLOAD_PATH;
            $folders = array_diff(scandir($path), ['.', '..']);
            $data = [];
            foreach ($folders as $folder) {
                $files = array_diff(scandir($path . $folder), ['.', '..']);
                foreach ($files as $file) {
                    $data[] = [
                        'url' => self::UPLOAD_PATH . $folder . '/' . $file,
                        'thumb' => self::UPLOAD_PATH . $folder . '/' . $file,
                        'tag' => $folder
                    ];
                }
            }
            return json_encode($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => self::ERROR_STATUS,
                'error' => __('common.get_list_fail')
            ]);
        }
    }

    public function upload(Request $request)
    {
        $key = array_keys($request->all())[0];
        $folder = self::UPLOAD_PATH . str_replace('_', ' ', str_replace('image_', '', $key)) . '/';
        $path = public_path() . $folder;
        if (!file_exists($path)) {
            @mkdir($path, 0755, true);
        }
        $file = $request[$key];
        try {
            $filename = $file->getClientOriginalName() . '-' . time() . '.' . $file->getClientOriginalExtension();
            $file->move($path, $filename);
        } catch (\Exception $e) {
        }
        return response()->json([
            'link' => $folder . $filename
        ]);
    }

    public function delete(Request $request)
    {
        $this->validate($request, ['src' => 'required|string']);
        $url = public_path() . $request->src;
        try {
            File::delete($url);
        } catch (\Exception $e) {
            return response()->json([
                'status' => self::ERROR_STATUS,
                'message' => __('common.cant_remove_image')
            ]);
        }
        return response()->json([
            'status' => self::SUCCESS_STATUS,
            'message' => __('common.removed_image')
        ]);
    }
}
