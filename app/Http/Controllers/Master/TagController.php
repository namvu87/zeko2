<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Master\TagContract;
use App\Http\Resources\Tag as TagResource;
use App\Http\Resources\TagCollection;

class TagController extends Controller
{
    private $tag;

    public function __construct(TagContract $tag)
    {
        $this->tag = $tag;
    }

    public function list(Request $request, $statusTag = 1)
    {   
        return response()->json([
            'tags' => new TagCollection($this->tag->filter($statusTag))
        ]);
    }

    public function store(Request $request)
    {
        $this->tag->store($request->all());

        return response()->json([
            'message' => __('master.tag_manager.add_tag_success')
        ]);
    }

    public function edit(Request $request, string $tagId)
    {
        return response()->json([
            'name' => $this->tag->getById($tagId)->name
        ]);
    }

    public function update(Request $request, string $tagId)
    {
        $this->validate($request, [
            'name' => 'required|unique:tags|string'
        ]);

        $this->tag->update($tagId, ['name' => $request->name]);

        return response()->json([
            'message' => __('master.tag_manager.edit_tag_success')
        ]);
    }

    public function changeStatus(Request $request)
    {
        $this->tag->update($request->id, ['status' => $request->status]);
        return response()->json();
    }

    public function delete(string $tagId)
    {
        $result = $this->tag->delete($tagId);
        $response = [
            'is_success' => $result
        ];
        if ($result) {
            $response['message'] = __('master.tag_manager.delete_tag_success'); 
        } else {
            $response['message'] = __('master.tag_manager.error_tag_exists_news');
        }
        return response()->json($response);
    }

    public function restore(string $tagId)
    {
        $this->tag->restore($tagId);

        return response()->json([
            'message' => __('master.tag_manager.restore_tag_success')
        ]);
    }
}
