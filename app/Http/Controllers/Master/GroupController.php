<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\GroupContract;
use App\Http\Resources\GroupCollection;

class GroupController extends Controller
{
    private $group;

    public function __construct(GroupContract $group)
    {
        $this->group = $group;
    }

    public function list()
    {
        return response()->json([
            'groups' => $this->group->getGroups()
        ]);
    }

    public function edit(Request $request, string $groupId)
    {
        $group = $this->group->getById($groupId);
        return response()->json([
                'expired_at' => $group->expired_at
            ]);
    }

    public function update(Request $request, string $groupId)
    {
        $this->validate($request, [
            'expired_at' => 'required|date|after:now'
        ]);

        $this->group->update($groupId, ['expired_at' => $request->expired_at]);

        return response()->json([
            'message' => __('master.group_manager.set_expiration_date_success')
        ]);
    }
}
