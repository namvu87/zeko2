<?php

namespace App\Http\Controllers\Master;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Master\NewsContract;
use App\Contracts\Master\TagContract;
use App\Http\Resources\NewsCollection;
use App\Http\Resources\News as NewsResource;
use Illuminate\Validation\Rule;

class NewsController extends Controller
{
    private $news;

    private $tag;

    public function __construct(NewsContract $news, TagContract $tag)
    {
        $this->news = $news;
        $this->tag = $tag;
    }

    public function list(Request $request)
    {
        return response()->json([
            'newses' => new NewsCollection($this->news->filter($request->status))
        ]);
    }

    public function create(Request $request)
    {
        return response()->json([
            'tags' => $this->tag->getAll(),
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|string',
            'sort' => 'required|numeric',
            'type' => 'required|numeric|in:' . join(array_keys(__('configuration.news_type')), ','),
            'description' => 'required|string',
            'content' => 'required',
        ];
        if ($request->type == News::TYPE_1 || $request->type == News::TYPE_5) {
            $rules['slug'] = 'required|unique:news';
            $rules['image'] = 'required|image|max:2000';
            $request['tags'] = json_decode($request->tags);
            $rules['tags'] = 'required|array';
            $rules['tags.*'] = 'string';
        }
        $request['content'] = str_replace("<p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">
                Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">
                Froala Editor</a></p>", '', $request->contents);
        $request->validate($rules);
        $this->news->store($request->all());

        return response()->json([
            'message' => __('master.news_manager.add_news_success')
        ]);
    }

    public function edit(Request $request, string $newsId)
    {
        return response()->json([
            'news' => new NewsResource($this->news->getById($newsId)),
            'tags' => $this->tag->getAll(),
        ]);
    }

    public function update(Request $request, $newsId)
    {
        $news = $this->news->getById($newsId);
        $rules = [
            'title' => 'required|string',
            'sort' => 'required|numeric',
            'description' => 'required|string',
            'content' => 'required'
        ];
        if ($news->type == News::TYPE_1 || $news->type == News::TYPE_5) {
            $rules['slug'] = 'required|unique:news,slug,' . $newsId . ',_id';
            $request['tags'] = json_decode($request->tags);
            $rules['tags'] = 'required|array';
            $rules['tags.*'] = 'string';
            $rules['image'] = 'nullable|image|max:2000';
        }
        $request->validate($rules);
        $request['content'] = str_replace("<p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>", '', $request->contents);
        $this->news->updateModel($request->all(), $newsId);
        if ($request->type == News::TYPE_1 || $request->type == News::TYPE_5) {
            $arr1 = array_diff($news->tags, $request->tags);
            $arr2 = array_diff($request->tags, $news->tags);
            foreach ($arr1 as $tagName) {
                $tag = $this->tag->getByName($tagName);
                $tag->update(['usage_count' => $tag->usage_count - 1]);
            }
            foreach ($arr2 as $tagName) {
                $tag = $this->tag->getByName($tagName);
                if (!$tag) {
                    $this->tag->create([
                        'name' => $tagName,
                        'usage_count' => 0,
                        'status' => $this->tag->statusVisiable(),
                    ]);
                } else {
                    $tag->update([
                        'usage_count' => $tag->usage_count + 1
                    ]);
                }
            }
        }
        return response()->json([
            'message' => __('master.news_manager.edit_news_success')
        ]);
    }

    public function changeStatus(Request $request)
    {
        $this->news->update($request->id, ['status' => $request->status]);

        return response()->json();
    }

    public function delete(string $newsId)
    {
        $this->news->delete($newsId);

        return response()->json([
            'message' => __('master.news_manager.delete_news_success')
        ]);
    }

    public function restore(string $newsId)
    {
        $this->news->restore($newsId);

        return response()->json([
            'message' => __('master.news_manager.restore_tag_success')
        ]);
    }
}
