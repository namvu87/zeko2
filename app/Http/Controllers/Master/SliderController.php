<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Master\SliderContract;
use App\Http\Resources\Slider as SliderResource;
use App\Http\Resources\SliderCollection;
use Illuminate\Support\Facades\Cache;

class SliderController extends Controller
{
    private $slider;

    public function __construct(SliderContract $slider)
    {
        $this->slider = $slider;
    }

    public function list()
    {
        return response()->json([
            'sliders' => new SliderCollection($this->slider->getAllSliders())
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'link' => 'required|url',
            'type' => 'required|numeric',
            'image' => 'required|image'
        ]);

        $this->slider->store($request->all());

        return response()->json([
            'message' => __('master.slider_manager.add_slider_success')
        ]);
    }

    public function edit($sliderId)
    {
        return response()->json([
            'slider' => new SliderResource($this->slider->getById($sliderId))
        ]);
    }

    public function update(Request $request, string $sliderId)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'link' => 'required|url',
            'image' => 'nullable|mimes:jpg,png,jpeg'
        ]);
        $this->slider->updateModel($request->all(), $sliderId);
        return response()->json([
            'message' => __('master.slider_manager.edit_slider_success')
        ]);
    }

    public function changeStatus(Request $request, $id)
    {
        $this->slider->update($id, ['status' => intval($request->status)]);
        return;
    }

    public function delete($sliderId)
    {
        if (Cache::has('sliders')) Cache::forget('sliders');
        $this->slider->delete($sliderId);
        return response()->json([
            'message' => __('master.slider_manager.delete_slider_success')
        ]);
    }
}
