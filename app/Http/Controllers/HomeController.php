<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\Restaurant\ReportContract;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Contracts\GroupContract;
use App\Contracts\UserContract;

class HomeController extends Controller
{
    private $group;
    private $user;
    private $report;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GroupContract $group, UserContract $user, ReportContract $report)
    {
        $this->middleware('auth');

        $this->group = $group;
        $this->user = $user;
        $this->report = $report;

    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Get data home page Timekeeping
     *
     * @param \Illuminate\Http\Request $request The request
     */
    public function timekeeping(Request $request)
    {
        return response()->json([
            'group' => $this->group->getById($request->group_id, ['owner', 'shifts']),
            'user_count' => $this->user->getCountByGroupId($request->group_id)
        ]);
    }

    public function restaurant(Request $request)
    {
        $request->validate([
            'start' => 'required|date_format:d-m-Y',
            'end' => 'required|date_format:d-m-Y'
        ]);
        $startDate = Carbon::createFromFormat('d-m-Y H:i:s', $request->start . '00:00:00');
        $endDate = Carbon::createFromFormat('d-m-Y H:i:s', $request->end . '23:59:59');
        if (CarbonPeriod::create($startDate, $endDate)->count() <= 32) {
            $statisticGoods = $this->report->statisticGoods($request->group_id, $startDate, $endDate);
            $statisticRevenue = $this->report->statisticRevenue($request->group_id, $startDate, $endDate);
            return response()->json([
                'statisticRevenue' => $statisticRevenue,
                'statisticGoods' => $statisticGoods
            ]);
        }
        return;
    }

    public function restaurantToday(Request $request)
    {
        return response()->json([
            'statistic' => $this->report->getStatisticToday($request->group_id)
        ]);
    }
}
