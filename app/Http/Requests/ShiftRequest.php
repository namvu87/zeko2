<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Contracts\ShiftContract;

class ShiftRequest extends FormRequest
{
    private $shift;

    public function __construct(ShiftContract $shift)
    {
        $this->shift = $shift;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'start_time_1' => 'required|date_format:H:i',
            'end_time_1' => 'required|date_format:H:i|after:start_time_1',
            'name' => 'required|string',
            'allow_time' => 'numeric|nullable',
            'schedules.*' => 'string|in:' . implode(',', $this->shift->getSchedules()),
            'type' => 'required:in:' . implode(',', $this->shift->getTypes())
        ];
        if ($this->type === $this->shift->getMultiType()) {
            $rules['start_time_2'] = 'required|date_format:H:i|after:end_time_1';
            $rules['end_time_2'] = 'required|date_format:H:i|after:start_time_2';
        }
        return $rules;
    }
}
