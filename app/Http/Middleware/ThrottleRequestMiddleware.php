<?php

namespace App\Http\Middleware;

use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Http\Exceptions\ThrottleRequestsException;

class ThrottleRequestMiddleware extends ThrottleRequests
{
    /**
     * Create a 'too many attempts' exception.
     *
     * @param  string  $key
     * @param  int  $maxAttempts
     * @return \Illuminate\Http\Exceptions\ThrottleRequestsException
     */
    protected function buildException($key, $maxAttempts)
    {
        $retryAfter = $this->getTimeUntilNextRetry($key);

        $headers = $this->getHeaders(
            $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts, $retryAfter),
            $retryAfter
        );

        $timer = $retryAfter / 60 > 1 ? round($retryAfter / 60) . ' ' . __('word.minutes') : $retryAfter . ' ' . __('word.seconds');

        return new ThrottleRequestsException(
            __('auth.attemp_too_many', ['counter' => $timer]),
            null,
            $headers
        );
    }
}
