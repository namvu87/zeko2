<?php

namespace App\Http\Middleware;

use Closure;

class VerifyPhoneNumberMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user() || empty($request->user()->phone_number_verified_at)) {
            return response()->json([
                'message' => __('auth.verify_email_and_phone_number_is_required')
            ], 403);
        }

        return $next($request);
    }
}
