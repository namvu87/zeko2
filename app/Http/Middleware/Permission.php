<?php

namespace App\Http\Middleware;

use Closure;

class Permission
{
    /**
     * Segments are allowed
     * @var array
     */
    private $allow = [];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth('api')->user();
        $permission = $request->group_id . '.' . $request->segment(2) . '.' . $request->segment(3);

        if ($user->can($permission)) {
            return $next($request);
        }
        return response()->json([
            'message' => __('error.403')
        ], 403);
    }
}
