<?php

namespace App\Http\Middleware;

use Closure;

class LocaleManage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->header('X-Localization') ?? 'vi';
        if (!empty($locale)) {
            app()->setLocale($locale);
        }
        return $next($request);
    }
}
