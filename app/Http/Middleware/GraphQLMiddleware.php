<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class GraphQLMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('api')->check()) {
            return $next($request);
        }
        return response()->json([
            'message' => __('auth.unauthorized')
        ], 401);
    }

    public function terminate($request, $response)
    {
        // info(\DB::getQueryLog());

        // /* Currently used memory */
        // $mem_usage = memory_get_usage();

        // /* Peak memory usage */
        // $mem_peak = memory_get_peak_usage();

        // info('USAGE: ' . ($mem_usage / 1024 / 1024) . ' MB');
        // info('PEAK: ' . ($mem_peak / 1024 / 1024) . ' MB');
    }
}
