<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Requsest  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return response()->json([
                'message' => __('auth.unauthorized')
            ], 401);
        }
    }

    public function terminate($request, $response)
    {
        // info(\DB::getQueryLog());

        // /* Currently used memory */
        // $mem_usage = memory_get_usage();
        
        // /* Peak memory usage */
        // $mem_peak = memory_get_peak_usage();

        // info('USAGE: ' . ($mem_usage / 1024 / 1024) . ' MB');
        // info('PEAK: ' . ($mem_peak / 1024 / 1024) . ' MB');
    }
}
