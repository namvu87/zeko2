<?php
/**
 * Created by PhpStorm.
 * User: zeko-1
 * Date: 16/04/2019
 * Time: 09:34
 */

namespace App\Http\View\Composers;


use App\Contracts\Master\ContactContract;
use App\Contracts\Master\NewsContract;
use App\Contracts\Master\TagContract;
use Illuminate\View\View;

class FooterComposer
{
    protected $tag;
    protected $contact;
    protected $news;

    public function __construct(TagContract $tag, ContactContract $contact, NewsContract $news)
    {
        $this->tag = $tag;
        $this->contact = $contact;
        $this->news = $news;
    }

    public function compose(View $view)
    {
        $tags = $this->news->getTagsInNormalNews();
        $view->with('topTags', $this->tag->getTopTagsByTags($tags));
        $view->with('contact', $this->contact->getContactInformation());
    }
}