<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Contact extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id'    =>  $this->_id,
            'type'    =>  $this->type,
            'name'    =>  $this->name,
            'address'    =>  $this->address,
            'phone_number'    =>  $this->phone_number,
            'zalo'    =>  $this->zalo,
            'skype'    =>  $this->skype,
            'fb'    =>  $this->fb,
            'email'    =>  $this->email,
            'fax'    =>  $this->fax
        ];
    }
}
