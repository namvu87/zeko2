<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Invoice as InvoiceModel;

class Invoice extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id'             => $this->_id,
            'code'           => $this->code,
            'group_id'       => $this->group_id,
            'user_ids'       => $this->user_ids,
            'table_ids'      => $this->table_ids,
            'goods'          => $this->goods,
            'notifications'  => $this->notifications ?? [],
            'note'           => $this->note ?? "",
            'discount'       => $this->discount ?? 0,
            'discount_type'  => $this->discount_type ?? InvoiceModel::DISCOUNT_TYPE_FIXED,
            'status'         => $this->status,
            'request_status' => $this->request_status,
            'recipient'      => $this->when($this->whenLoaded('recipient'), $this->recipient),
            'total'          => $this->total,
            'payment_method' => $this->payment_method,
            'vat'            => $this->vat,
            'created_at'     => $this->created_at->toDateTimeString(),
            'updated_at'     => $this->updated_at->toDateTimeString()
        ];
    }
}
