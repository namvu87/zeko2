<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Role as RoleResource;
use App\Http\Resources\Permission as PermissionResource;
use App\Http\Resources\Group as GroupResource;
use App\Models\Group as GroupModel;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }

        $id = auth('api')->id();

        return [
            'id'                => $this->_id,
            'first_name'        => $this->first_name,
            'last_name'         => $this->last_name,
            'email'             => $this->email,
            'email_verified'    => ! is_null($this->email_verified_at),
            // 'is_master'         => $this->when(in_array($this->email, config('master.email')), function() {
            //         return true;
            //     }
            // ),
            'phone_number'      => $this->phone_number,
            'group_ids'         => $this->group_ids ?? [],
            'area'              => $this->area,
            'commune'           => $this->commune,
            'address'           => $this->address,
            'birthday'          => $this->birthday,
            'gender'            => $this->gender,
            'avatars'           => $this->avatars,
            'group_role'        => $this->when($request->group_role && $request->group_id,
                function() use ($request) {
                    $roles = $this->roles->where('group_id', $request->group_id)->first();
                    return new RoleResource($roles);
                }
            ),
            'group_permissions' => $this->when($request->group_permissions && $request->group_id,
                function() use ($request) {
                    $permissions = $this->permissions->where('group_id', $request->group_id);
                    return PermissionResource::collection($permissions);
                }
            ),
            'roles' => $this->when($request->roles && $id === $this->_id,
                function() {
                    return RoleResource::collection($this->roles);
                }
            ),
            'permissions' => $this->when($request->permissions && $id === $this->_id,
                function() {
                    return PermissionResource::collection($this->permissions);
            }),
            'owner_groups' => $this->when($request->owner_groups, function() use ($id) {
                if ($id === $this->_id) {
                    return GroupResource::collection($this->ownerGroups);
                }
                return [];
            }),
            'member_groups' => $this->when($request->member_groups, function() {
                $groups = GroupModel::whereIn('_id', $this->group_ids)->get();
                return GroupResource::collection($groups);
            }),
            'password_none' => $this->when(!isset($this->password) && isset($this->social), function () {
                return true;
            })
        ];
    }
}
