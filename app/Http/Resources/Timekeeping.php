<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class Timekeeping extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id' => $this->_id,
            'group_id' => $this->group_id,
            'user_id' => $this->user_id,
            'date' => $this->date,
            'times' => $this->times,
            'shift_name' => $this->shift_name,
            'shift_type' => $this->shift_type,
            'is_addition' => $this->is_addition,
            'is_switch' => $this->is_switch,
            'is_take_leave' => $this->is_take_leave,
            'target_time' => $this->target_time,
            'user' => new UserResource($this->whenLoaded('user'))
        ];
    }
}
