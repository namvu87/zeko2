<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Permission as PermissionResource;
use App\Http\Resources\GroupCollection;
use App\Models\Group as GroupModel;

class UserBase extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->_id,
            'first_name'        => $this->first_name,
            'last_name'         => $this->last_name,
            'email'             => $this->email,
            'email_verified'    => !is_null($this->email_verified_at),
            'is_master'         => $this->when(
                in_array($this->email, config('master.email')), function () {
                    return true;
                }
            ),
            'phone_number'      => $this->phone_number,
            'group_ids'         => $this->group_ids ?? [],
            'area'              => $this->area,
            'commune'           => $this->commune,
            'address'           => $this->address,
            'birthday'          => $this->birthday,
            'gender'            => $this->gender,
            'avatars'           => $this->avatars,
            'cover_image'       => $this->cover_image,
            'permissions'       => $this->when($request->permissions, function() {
                return $this->getPermissionsViaRoles()->pluck('name');
            }),
            'member_groups'     => $this->when($request->member_groups, function() {
                $groups = GroupModel::whereIn('_id', $this->group_ids)->get();
                return (new GroupCollection($groups))->setUser($this);
            })
        ];
    }
}
