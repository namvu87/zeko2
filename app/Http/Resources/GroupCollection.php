<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Group as GroupResource;

class GroupCollection extends ResourceCollection
{
    protected $user;

    public function setUser($user){
        $this->user = $user;
        return $this;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->each->setUser($this->user);
        return parent::toArray($request);
    }
}
