<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Tag extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id'            => $this->_id,
            'name'          => $this->name,
            'status'        => $this->title,
            'usage_count'   => $this->usage_count,
            'status'        => $this->status,
            'deleted_at'    => $this->when($this->deleted_at, function() {
                return $this->deleted_at;
            })
        ];
    }
}
