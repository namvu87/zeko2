<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Permission as PermissionResource;

class Role extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->_id,
            'name' => str_replace($request->group_id . '.', '', $this->name),
            'permission_ids' => (array) $this->permission_ids,
            'user_ids' => (array) $this->user_ids,
            'permissions'  => $this->when(
                $request->permissions, PermissionResource::collection($this->permissions)
            ),
        ];
    }
}
