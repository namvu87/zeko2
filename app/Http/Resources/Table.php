<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Place as PlaceResource;

class Table extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id'          => $this->_id,
            'name'        => $this->name,
            'group_id'    => $this->group_id,
            'sort'        => $this->sort,
            'status'      => $this->status,
            'count_seat'  => $this->count_seat,
            'description' => $this->description,
            'place_id'    => $this->place_id,
            'place'       => new PlaceResource($this->whenLoaded('place')),
            // 'place_name'  => $this->when($this->whenLoaded('place'), function() {
            //     return $this->place->name;
            // })
        ];
    }
}
