<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class Requirement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id'             => $this->_id,
            'creator_id'     => $this->creator_id,
            'date'           => $this->date,
            'type'           => $this->type,
            'timekeeping_id' => $this->timekeeping_id,
            'shift_id'       => $this->shift_id,
            'is_accept'      => $this->is_accept,
            'content'        => $this->content,
            'res_content'    => $this->res_content,
            'creator'        => $this->when(
                $this->whenLoaded('creator'),
                new UserResource($this->creator)
            ),
            'timekeeping'    => $this->when(
                $this->whenLoaded('timekeeping'),
                $this->timekeeping
            ),
            'shift'          => $this->when(
                $this->whenLoaded('shift'), $this->shift
            ),
        ];
    }
}
