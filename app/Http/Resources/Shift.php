<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Shift extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id' => $this->_id,
            'name' => $this->name,
            'group_id' => $this->group_id,
            'type' => $this->type,
            'times' => $this->times,
            'allow_time' => $this->allow_time,
            'schedules' => $this->schedules
        ];
    }
}
