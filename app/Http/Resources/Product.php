<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id' => $this->_id,
            'name' => $this->name,
            'type' => $this->type,
            'price_type' => $this->price_type,
            'price' => $this->price,
            'user_count' => $this->user_count,
            'code' => $this->code,
            'user_manage' => $this->user_manage,
            'shift_manage' => $this->shift_manage,
            'time_keeping_manage' => $this->time_keeping_manage,
            'statistic' => $this->statistic,
            'detail' => $this->detail,
            'description' => $this->description,
            'user_guide' => $this->user_guide,
            'key_word' => $this->key_word
        ];
    }
}
