<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Table as TableResource;

class Place extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id' => $this->_id,
            'name' => $this->name,
            'group_id' => $this->group_id,
            'description' => $this->description,
            'tables' => TableResource::collection($this->whenLoaded('tables'))
        ];
    }
}
