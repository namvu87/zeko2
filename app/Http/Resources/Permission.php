<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Permission extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->_id,
            'label' => $this->label,
            'name'  => str_replace($request->group_id . '.', '', $this->name),
            'role_ids' => $this->role_ids
        ];
    }
}
