<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupMenus extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }

        return [
            'id'        => $this->_id,
            'level'     => $this->level,
            'name'      => $this->name,
            'parent_id' => $this->parent_id,
            'childs'    => GroupMenus::collection($this->whenLoaded('childs'))
        ];
    }
}
