<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Slider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id'        => $this->_id,
            'title'     => $this->title,
            'photo_url' => $this->photo_url,
            'link'      => $this->link,
            'type'      => $this->type,
            'status'    => $this->status
        ];
    }
}
