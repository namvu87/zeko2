<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Restaurant extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id'            => $this->_id,
            'name'          => $this->name,
            'province'      => $this->province,
            'address'       => $this->address,
            'price_range'   => $this->price_range,
            'opening_times' => $this->opening_times,
            'images'        => $this->images,
            'location'      => $this->location
        ];
    }
}
