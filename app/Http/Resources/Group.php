<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Group extends JsonResource
{
    protected $user;

    public function setUser($user) 
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'area' => $this->area,
            'commune' => $this->commune,
            'address' => $this->address,
            'service_type' => $this->service_type,
            'owner_id' => $this->owner_id,
            'goods_properties' => $this->goods_properties,
            'units' => $this->units,
            'number_requirement' => $this->number_requirement,
            'role_name' => $this->when($this->user, function() {
                $roles = $this->user->roles->where('group_id', $this->id)->first();
                if ($roles) {
                    return str_replace($this->id . '.', '', $roles->name);
                } else {
                    return 'none';
                }
            }),
            'expired_at' => $this->expired_at,
            'cover_image' => $this->cover_image
        ];
    }
}
