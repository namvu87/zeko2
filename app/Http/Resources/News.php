<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class News extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id'            => $this->_id,
            'title'         => $this->title,
            'description'   => $this->description,
            'image'         => $this->image,
            'content'       => $this->content,
            'status'        => $this->status,
            'sort'          => $this->sort,
            'slug'          => $this->slug,
            'type'          => $this->type,
            'deleted_at'    => $this->when($this->deleted_at, function() {
                return $this->deleted_at;
            }),
            'tags'          => $this->when($this->tags, function() {
                return $this->tags;
            })
        ];
    }
}
