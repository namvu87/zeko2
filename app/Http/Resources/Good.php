<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Good extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return null;
        }
        return [
            'id' => $this->_id,
            'code' => $this->code,
            'group_id' => $this->group_id,
            'name' => $this->name,
            'group_menu_ids' => $this->group_menu_ids,
            'price' => $this->price,
            'price_origin' => $this->price_origin,
            'unit' => $this->unit,
            'properties' => $this->properties,
            'inventory_number' => $this->inventory_number,
            'inventory_max' => $this->inventory_max,
            'inventory_min' => $this->inventory_min,
            'description' => $this->description,
            'images' => $this->images ?? [],
            'status' => $this->status,
            'parent_id' => $this->parent_id,
            'is_sale' => $this->is_sale,
            'type' => $this->type,
            'ingredients' => $this->ingredients,
            'discount' => $this->discount,
            'discount_type' => $this->discount_type,
            'conversion_value' => $this->conversion_value
        ];
    }
}
