<?php

namespace App\Services;

use App\Contracts\ServiceFeeContract;
use App\Models\ServiceFee;

class ServiceFeeService extends BaseService implements ServiceFeeContract
{
    public function __construct(ServiceFee $model)
    {
        parent::__construct($model);
    }

    public function getUnpaidState(): int
    {
        return $this->model::UNPAID_STATUS;
    }

    public function getInprogressState(): int
    {
        return $this->model::INPROGRESS_STATUS;
    }

    public function getPaidState(): int
    {
        return $this->model::PAID_STATUS;
    }

    /**
     * Lấy danh sách hóa đơn thanh toán theo tháng
     *
     * @param array      $groupIds
     * @param array      $relations
     * @param null|string $month
     *
     * @return mixed
     */
    public function getByGroupIds(array $groupIds, array $relations, string $month)
    {
        return $this->model
            ->with($relations)
            ->whereIn('group_id', $groupIds)
            ->where('aggregated_month', $month)
            ->get();
    }
}
