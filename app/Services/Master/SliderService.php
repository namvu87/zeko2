<?php

namespace App\Services\Master;

use App\Services\BaseService;
use App\Contracts\Master\SliderContract;
use App\Models\Slider;
use App\Contracts\ImageContract;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class SliderService extends BaseService implements SliderContract
{
    const PATH_UPLOAD_FILE = 'upload/sliders/';
    const PER_PAGE = 10;

    private $image;

    public function __construct(Slider $model, ImageContract $image)
    {
        parent::__construct($model);
        $this->image = $image;
    }

    public function getSliders()
    {
        return Cache::rememberForever('sliders', function () {
            return $this->model->where('status', $this->model::STATUS_VISIBLE)
                               ->where(function($query) {
                                    $query->whereNull('type')
                                          ->orWhere('type', $this->model::TYPE_SHOW_WEB);
                               })
                               ->get();
        });
    }

    /**
     * Create a record Slider model
     *
     * @param      Array $data
     * @return     App\Models\Slider
     */
    public function store(array $data)
    {
        $photoUrl = $this->saveImage($data['image'], (int)$data['type']);

        return $this->model->create([
            'title' => $data['title'],
            'photo_url' => $photoUrl,
            'link' => $data['link'],
            'type' => (int)$data['type'],
            'status' => $this->model::STATUS_VISIBLE
        ]);
    }

    private function saveImage($file, $type)
    {
        $path = self::PATH_UPLOAD_FILE;
        if (!file_exists($path)) {
            @mkdir(public_path() . '/' . $path, 0755, true);
        }

        $ext = $file->extension();
        $filename = md5(time());

        $file->move($path, $filename . ".{$ext}");

        $images = ['origin' => $path . $filename . ".{$ext}"];

        if ($type === $this->model::TYPE_SHOW_WEB) {
            $x1 = $this->image->setImage($path . $filename . ".{$ext}")
                ->setSize($this->model::SIZE_1X[0], $this->model::SIZE_1X[1])
                ->setDestPath($path)
                ->setFilename($filename . "_x1.{$ext}")
                ->save('resize');
            $x2 = $this->image->setImage($path . $filename . ".{$ext}")
                ->setSize($this->model::SIZE_2X[0], $this->model::SIZE_2X[1])
                ->setDestPath($path)
                ->setFilename($filename . "_x2.{$ext}")
                ->save('resize');
            $x3 = $this->image->setImage($path . $filename . ".{$ext}")
                ->setSize($this->model::SIZE_3X[0], $this->model::SIZE_3X[1])
                ->setDestPath($path)
                ->setFilename($filename . "_x3.{$ext}")
                ->save('resize');
            $x4 = $this->image->setImage($path . $filename . ".{$ext}")
                ->setSize($this->model::SIZE_4X[0], $this->model::SIZE_4X[1])
                ->setDestPath($path)
                ->setFilename($filename . "_x4.{$ext}")
                ->save('resize');
            return array_merge($images, [
                'x1' => $x1,
                'x2' => $x2,
                'x3' => $x3,
                'x4' => $x4
            ]);
        }

        $x1 = $this->image->setImage($path . $filename . ".{$ext}")
            ->setSize($this->model::MOBILE_SIZE_X1[0], $this->model::MOBILE_SIZE_X1[1])
            ->setDestPath($path)
            ->setFilename($filename . "_mobile_x1.{$ext}")
            ->save('resize');
        $x2 = $this->image->setImage($path . $filename . ".{$ext}")
            ->setSize($this->model::MOBILE_SIZE_X2[0], $this->model::MOBILE_SIZE_X2[1])
            ->setDestPath($path)
            ->setFilename($filename . "_mobile_x2.{$ext}")
            ->save('resize');
        $x3 = $this->image->setImage($path . $filename . ".{$ext}")
            ->setSize($this->model::MOBILE_SIZE_X3[0], $this->model::MOBILE_SIZE_X3[1])
            ->setDestPath($path)
            ->setFilename($filename . "_mobile_x3.{$ext}")
            ->save('resize');
        $x4 = $this->image->setImage($path . $filename . ".{$ext}")
            ->setSize($this->model::MOBILE_SIZE_X4[0], $this->model::MOBILE_SIZE_X4[1])
            ->setDestPath($path)
            ->setFilename($filename . "_mobile_x4.{$ext}")
            ->save('resize');
        return array_merge($images, [
                'x1' => $x1,
                'x2' => $x2,
                'x3' => $x3,
                'x4' => $x4
            ]);
    }

    /**
     * Update Slider model
     *
     * @param      Array $data
     * @param      string $id
     */
    public function updateModel(array $data, string $id)
    {
        $request = [
            'title' => $data['title'],
            'link' => $data['link'],
        ];
        if (!empty($data['image'])) {
            $slider = $this->getById($id);
            if ($slider) {
                $images = $slider->photo_url;
                foreach ($images as $image) {
                    if (File::exists($image)) {
                        File::delete($image);
                    }
                }
                $request['photo_url'] = $this->saveImage($data['image'], $slider->type);
            }
        }
        $this->update($id, $request);
    }

    public function getAllSliders()
    {
        return $this->model->paginate(self::PER_PAGE);
    }
}
