<?php
namespace App\Services\Master;

use App\Services\BaseService;
use App\Contracts\Master\OrderContract;
use App\Models\Order;

class OrderService extends BaseService implements OrderContract
{
    public function __construct(Order $model)
    {
        parent::__construct($model);
    }
}
