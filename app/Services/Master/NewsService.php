<?php

namespace App\Services\Master;

use App\Services\BaseService;
use App\Contracts\Master\NewsContract;
use App\Models\News;
use App\Contracts\ImageContract;
use File;
use Illuminate\Support\Facades\Cache;

class NewsService extends BaseService implements NewsContract
{
    const PER_PAGE = 10;
    const PATH_UPLOAD_FILE = 'upload/newses/';
    const NUMBER_NEW_NEWS = 3;

    private $image;

    public function __construct(News $model, ImageContract $image)
    {
        parent::__construct($model);
        $this->image = $image;
    }

    /**
     * Filter list News by status news
     *
     * @param      integer $statusNews The status news
     */
    public function filter(int $statusNews)
    {
        if ($statusNews === $this->model::STATUS_VISIBLE) {
            return $this->model->paginate(self::PER_PAGE);
        }
        return $this->model->onlyTrashed()->paginate(self::PER_PAGE);
    }

    /**
     * Create a record News model
     *
     * @param      Array $data
     * @return     App\Models\News
     */
    public function store(array $data)
    {
        if (!empty($data['image'])) {
            $data['image'] = $this->saveImage($data['image']);
        }
        $data['type'] = (int)$data['type'];
        $data['sort'] = (int)$data['sort'];
        $data['status'] = News::STATUS_VISIBLE;
        $news = $this->create($data);
        $this->refreshCache(null, $data['type']);
        return $news;
    }

    /**
     * @param  string $id
     * @param  array $data
     * @return App\Models\News
     */
    public function update(string $id, array $data)
    {
        return $this->model::withTrashed()->find($id)->update($data);
        $this->refreshCache(null, $data['type']);
    }

    /**
     * Update News model
     *
     * @param      array $data
     * @param      string $id
     */
    public function updateModel(array $data, string $id)
    {
        $news = $this->getById($id);
        if (!empty($data['image'])) {
            if ($news->image) {
                foreach ($news->image as $photo_url) {
                    if (file_exists(public_path() . '/' . $photo_url)) {
                        File::delete(public_path() . '/' . $photo_url);
                    }
                }
            }
            $data['image'] = $this->saveImage($data['image']);
        }
        $data['sort'] = (int)$data['sort'];
        $news->update($data);

        $this->refreshCache(null, $news->type);

        return $news;
    }

    private function saveImage($file)
    {
        $path = self::PATH_UPLOAD_FILE;
        if (!file_exists($path)) {
            @mkdir(public_path() . '/' . $path, 0755, true);
        }

        $ext = $file->extension();
        $filename = md5(time());
        $file->move($path, $filename . ".{$ext}");

        $x1 = $this->image->setImage($path . $filename . ".{$ext}")
            ->setSize($this->model::SIZE_1X[0], $this->model::SIZE_1X[1])
            ->setDestPath($path)
            ->setFilename($filename . "_x1.{$ext}")
            ->save('resize');
        $x2 = $this->image->setImage($path . $filename . ".{$ext}")
            ->setSize($this->model::SIZE_2X[0], $this->model::SIZE_2X[1])
            ->setDestPath($path)
            ->setFilename($filename . "_x2.{$ext}")
            ->save('resize');

        return [
            "origin" => $path . $filename . ".{$ext}",
            "x1" => $x1,
            "x2" => $x2
        ];
    }

    /**
     * Restore News model
     *
     * @param      string $newsId The tag identifier
     * @return
     */
    public function restore(string $newsId)
    {
        $this->model::withTrashed()->where('_id', $newsId)->restore();
    }

    public function getUserGuide()
    {
        return Cache::rememberForever('news_home_page', function() {
            return $this->model->where('status', News::STATUS_VISIBLE)->first();
        });
    }

    public function getNewsByType(int $type)
    {
        return Cache::rememberForever("news_type_{$type}", function() use ($type) {
            return $this->model->where('status', News::STATUS_VISIBLE)->where('type', $type)->get();
        });
    }

    public function getAllNewNews()
    {
        return Cache::rememberForever("new_newses", function() {
            return $this->model->where('status', News::STATUS_VISIBLE)
                ->where('type', News::TYPE_1)->paginate(self::PER_PAGE);
        });
    }

    public function getNewsByTagId(string $tagId)
    {
        return $this->model->where('type', News::TYPE_1)->whereIn('tag_ids', [$tagId])->paginate(self::PER_PAGE);
    }

    public function getNewPosts()
    {
        return $this->model->where('type', News::TYPE_1)->orderBy('created_at', 'desc')->get()->take(self::NUMBER_NEW_NEWS);
    }

    public function getOtherPostsBySlug(string $slug)
    {
        return $this->model->where('type', News::TYPE_1)->where('slug', '<>', $slug)->orderBy('created_at', 'desc')->get()->take(self::NUMBER_NEW_NEWS);
    }

    public function getNewsById(string $newsId)
    {
        return Cache::rememberForever("news_{$newsId}", function() use ($newsId) {
            return $this->model->find($newsId);
        });
    }

    public function getNewsByIdWithTags(string $newsId)
    {
        return $this->model->with('tags')->find($newsId);
    }

    public function getFirstNewsByType(int $type)
    {
        return Cache::rememberForever("first_news_type_{$type}", function() use ($type) {
            return $this->model->where('status', News::STATUS_VISIBLE)->where('type', $type)->first();
        });
    }

    public function getCareerNewsByTag(string $tag)
    {
        return $this->model->where('type', News::TYPE_5)
            ->where('tags', 'all', [$tag])->paginate(self::PER_PAGE);
    }

    public function getCareerNews()
    {
        return $this->model->where('type', News::TYPE_5)
            ->where('status', News::STATUS_VISIBLE)->paginate(self::PER_PAGE);
    }

    public function changeStatus(int $status, string $id)
    {
        $news = $this->model->withTrashed()->find($id);
        $news->status = (int)$status;
        $news->save();

        $this->refreshCache($id, $news->type);
    }

    public function delete(string $id)
    {
        $news = $this->model->find($id);
        $news->delete();
        $this->refreshCache($id, $news->type);
    }

    public function refreshCache(string $newsId = null, $type = null)
    {
        if (!empty($newsId)) {
            if (Cache::has("news_{$newsId}")) {
                Cache::forget("news_{$newsId}");
            }
        }
        if (!empty($type)) {
            if (Cache::has("news_type_{$type}")) {
                Cache::forget("news_type_{$type}");
            }
            if (Cache::has("first_news_type_{$type}")) {
                Cache::forget("first_news_type_{$type}");
            }
        }
        if (Cache::has("newses")) {
            Cache::forget("newses");
        }
        if (Cache::has('new_newses')) {
            Cache::forget("new_newses");
        }
        if ($type == News::TYPE_1 && Cache::has('top_tags')) {
            Cache::forget('top_tags');
        }
    }

    public function getTagsInNormalNews()
    {
        return array_values(array_unique(array_flatten($this->model->where('type', News::TYPE_1)->pluck('tags')->toArray())));
    }

    public function getBySlug(string $slug)
    {
        return $this->model->where('slug', $slug)->first();
    }
}