<?php

namespace App\Services\Master;

use App\Contracts\Master\NewsContract;
use App\Models\News;
use App\Services\BaseService;
use App\Contracts\Master\TagContract;
use App\Models\Tag;
use Illuminate\Support\Facades\Cache;

class TagService extends BaseService implements TagContract
{
    const PER_PAGE = 10;
    const TOP_TAGS = 5;


    public function statusVisiable()
    {
        return News::STATUS_VISIBLE;
    }

    public function __construct(Tag $model)
    {
        parent::__construct($model);
    }

    /**
     * Filter list Tag by status tag
     *
     * @param      integer $statusTag The status tag
     */
    public function filter(int $statusTag)
    {
        if ($statusTag === $this->model::STATUS_VISIBLE) {
            return $this->model::paginate(self::PER_PAGE);
        }
        return $this->model::onlyTrashed()->paginate(self::PER_PAGE);
    }

    /**
     * Create a record Tag model
     *
     * @param      Array $data
     *
     * @return     App\Models\Tag
     */
    public function store(Array $data)
    {
        return $this->model::create([
            'name' => $data['name'],
            'usage_count' => 0,
            'status' => $this->model::STATUS_VISIBLE,
        ]);
    }

    /**
     * @param  string $id
     * @param  array $data
     *
     * @return App\Models\Tag
     */
    public function update(string $id, array $data)
    {
        return $this->model::withTrashed()->find($id)->update($data);
    }

    /**
     * Delete Tag model
     *
     * @param     string $tagId The tag identifier
     *
     * @return    boolean
     */
    public function delete(string $tagId)
    {
        $tagModel = $this->model::find($tagId);
        if (empty($tagModel->news_ids)) {
            $tagModel->delete();
            return true;
        }
        return false;
    }

    /**
     * Restore Tag model
     *
     * @param      string $tagId The tag identifier
     *
     * @return
     */
    public function restore(string $tagId)
    {
        $this->model::withTrashed()->where('_id', $tagId)->restore();
    }

    /**
     * Restore Tag model
     *
     * @param      string $tagId The tag identifier
     *
     * @return
     */

    public function getTopTagsByTags(array $tags)
    {
        return Cache::rememberForever('top_tags', function() use (&$tags) {
            return $this->model->whereIn('name', $tags)->orderBy('usage_count', 'desc')
                ->take(self::TOP_TAGS)->get();
        });
    }

    /**
     * get tag by name
     *
     * @param string $name
     *
     * @return Model
     */
    public function getByName(string $name)
    {
        return $this->model->where('name', $name)->first();
    }
}