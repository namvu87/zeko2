<?php
/**
 * Created by PhpStorm.
 * User: zeko-1
 * Date: 18/04/2019
 * Time: 11:52
 */

namespace App\Services\Master;

use App\Contracts\Master\TagContract;
use App\Contracts\Master\UserGuideContract;
use App\Models\UserGuide;
use App\Services\BaseService;

class UserGuideService extends BaseService implements UserGuideContract
{
    const PER_PAGE = 10;

    private $tag;

    public function __construct(UserGuide $model, TagContract $tag)
    {
        parent::__construct($model);
        $this->tag = $tag;
    }

    public function getAllGuide(string $status)
    {
        if ($status) {
            return $this->model->paginate(self::PER_PAGE);
        }
        return $this->model->onlyTrashed()->paginate(self::PER_PAGE);
    }

    public function restore(string $id)
    {
        return $this->model->onlyTrashed()->where('_id', $id)->unset('deleted_at');
    }

    /**
     * get guide by service
     *
     * @param  int $service
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategoryByService(string $service)
    {
        $categories = __('configuration.category');
        $categoriesService = array();
        $guides = $this->model->where('service_type', 'all', [$service])
            ->orderBy('category')->orderBy('sort')->get()
            ->unique('category');
        foreach ($guides as $key => $guide) {
            $category = $categories[$guide->category];
            $category['slug'] = $guide['slug'];
            $categoriesService[$guide['category']] = $category;
        }
        return $categoriesService;
    }

    /**
     * get user guide by slug
     *
     * @param  string $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function getBySlug(string $slug, string $service)
    {
        return $this->model->where('service_type', 'all', [$service])->where('slug', $slug)->first();
    }

    /**
     * get user guide by category
     *
     * @param  int $category
     *
     * @return \Illuminate\Http\Response
     */
    public function getByCategory(int $category, string $service)
    {
        return $this->model->where('category', $category)
            ->where('service_type', 'all', [$service])
            ->orderBy('sort')->get();
    }

    /**
     * get user guide by title
     *
     * @param  int $category
     *
     * @return \Illuminate\Http\Response
     */
    public function searchByTitle(string $title)
    {
        return $this->model->title($title)->paginate(self::PER_PAGE);
    }

}