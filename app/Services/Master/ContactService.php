<?php

namespace App\Services\Master;

use App\Services\BaseService;
use App\Contracts\Master\ContactContract;
use App\Models\Contact;
use Illuminate\Support\Facades\Cache;

class ContactService extends BaseService implements ContactContract
{
    public function __construct(Contact $model)
    {
        parent::__construct($model);
    }

    /**
     * Gets the customer care.
     */
    public function getCustomerCare()
    {
        return Cache::rememberForever('customer_care', function() {
            return $this->model::where('type', $this->model::CUSTOMER_CARE_TYPE)->get();
        });
    }

    /**
     * Stores a customer care.
     *
     * @param      array $data The data
     */
    public function storeCustomerCare(array $data)
    {
        if (Cache::has('customer_care')) Cache::forget('customer_care');
        return $this->create([
            'type' => $this->model::CUSTOMER_CARE_TYPE,
            'name' => $data['name'],
            'phone_number' => $data['phone_number'],
            'zalo' => $data['zalo'],
            'skype' => $data['skype'],
            'email' => $data['email']
        ]);
    }


    /**
     * Update customer care
     *
     * @param      array $data The data
     * @param      string $customerCareId The customer care identifier
     */
    public function updateCustomerCare(array $data, string $customerCareId)
    {
        if (Cache::has('customer_care')) Cache::forget('customer_care');
        $this->update($customerCareId, $data);
    }

    /**
     * Gets the contact information.
     */
    public function getContactInformation()
    {
        return Cache::rememberForever('contact_information', function() {
            return $this->model::where('type', $this->model::CONTACT_INFORMATION_TYPE)->first();
        });
    }

    /**
     * Sets the contact information.
     *
     * @param      array $data The data contact
     *
     * @return     App\Models\Contact  ( description_of_the_return_value )
     */
    public function setContactInformation(array $data)
    {
        if (Cache::has('contact_information')) {
            Cache::forget('contact_information');
        }
        return $this->model::updateOrCreate([
            'type' => $this->model::CONTACT_INFORMATION_TYPE,
            'address' => $data['address'],
            'phone_number' => $data['phone_number'],
            'fax' => $data['fax'],
            'fb' => $data['fb']
        ]);
    }
}