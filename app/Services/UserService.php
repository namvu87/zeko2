<?php

namespace App\Services;

use App\Contracts\UserContract;
use App\Contracts\RoleContract;
use App\Services\BaseService;
use App\Models\User;

class UserService extends BaseService implements UserContract
{
    const PER_PAGE = 20;
    private $role;

    public function __construct(User $model, RoleContract $role)
    {
        parent::__construct($model);
        $this->role = $role;
    }

    /**
     * Get current User
     *
     * @return app\Models\User
     */
    public function getUser()
    {
        return auth('api')->user();
    }

    /**
     * Get current user's id
     *
     * @return string
     */
    public function id()
    {
        return auth('api')->id();
    }


    /**
     * Get all emplyees in group specified
     *
     * @param  string $groupId
     * @return App\Models\User
     */
    public function paginateByGroupId(string $groupId, int $page = 1, array $relation = [])
    {
        return $this->model
                    ->with($relation)
                    ->where('group_ids', 'all', [$groupId])
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Get all emplyees in group specified
     *
     * @param  string  $groupId
     * @return App\Models\User
     */
    public function getByGroupIdNotPaginate(string $groupId)
    {
        return $this->model
                    ->where('group_ids', 'all', [$groupId])
                    ->get();
    }

    /**
     * Lấy danh sách user không có trong group
     *
     * @param  string $groupId
     * @param  int    $page
     * @return App\Models\User
     */
    public function getOutGroup(string $groupId, int $page = 1)
    {
        return $this->model
                    ->whereNotIn('group_ids', [$groupId])
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy số lượng nhân viên trong group
     *
     * @param  string $groupId
     * @return int
     */
    public function getCountByGroupId(string $groupId)
    {
        return $this->model->where('group_ids', 'all', [$groupId])->count();
    }

    /**
     * Get user via phone number and email.
     *
     * @param  string $phone The phone
     * @param  string $email The email
     * @return App\Models\User
     */
    public function findForPassport(string $identifier)
    {
        return $this->model
                    ->where('phone_number', $identifier)
                    ->orWhere('email', $identifier)
                    ->first();
    }

     /**
      * Get user with timekeeping via groupId
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @return
     */
    public function getByGroupIdWithTimekeeping(string $groupId, string $startDate, string $endDate)
    {
        return $this->model->where('group_ids', 'all', [$groupId])
                ->with(['timeKeepings' => function($query) use ($startDate, $endDate, $groupId) {
                    $query->where('group_id', $groupId)
                        ->where('date', '>=', $startDate)
                        ->where('date', '<=', $endDate)
                        ->orderBy('date', 'ASC');
                }])
                ->orderBy('first_name', 'ASC')
                ->orderBy('last_name', 'ASC')
                ->get(['first_name', 'last_name']);
    }

    /**
     * Assign user to group's employee list
     *
     * @param string $userId
     * @return void
     */
    public function addUserToGroup(string $userId, string $groupId)
    {
        $user = $this->getById($userId);
        return $user->push('group_ids', $groupId, true);
    }

    /**
     * remove User from group's employee list
     *
     * @param  string $userId
     * @param  string $groupId
     * @return void
     */
    public function removeUserFromGroup(string $userId, string $groupId)
    {
        $user = $this->getById($userId);
        $user->pull('group_ids', $groupId);
    }

    /**
     * Get list employee permissioned
     * @param string $groupId
     * @return
     */
    public function getByRoleIds(array $roleIds)
    {
        return $this->model->whereIn('role_ids', $roleIds)->get();
    }

    /**
     * get list users in shift
     *
     * @param  string $shiftId
     * @return void
     */
    public function getByShiftId(string $shiftId, int $page = 1)
    {
        return $this->model
                    ->where('shift_ids', 'all', [$shiftId])
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Remove user from shift
     *
     * @param  string $userId
     * @param  string $shiftId
     */

    public function removeUserFromShift(string $userId, string $shiftId)
    {
        $user = $this->getById($userId);
        $user->pull('shift_ids', $shiftId);
    }

    /**
     * Remove users from shift
     *
     * @param  string $shiftId
     */
    public function removeUsersFromShift(string $shiftId)
    {
        $this->model->where('shift_ids', 'all', [$shiftId])->pull('shift_ids', $shiftId);
    }

    /**
     * Remove user from shift
     *
     * @param  string $userId
     * @param  string $shiftId
     */
    public function removeShift(string $shiftId)
    {
        $this->model->where('shift_ids', 'all', [$shiftId])->pull('shift_ids', $shiftId);
    }

    /**
     * Get list users in groups but not in shift
     *
     * @param  string $groupId
     *
     * @return App\Models\User
     */
    public function getNotShiftInGroup(string $groupId, array $shiftIds, int $page = 1)
    {
        return $this->model->where('group_ids', 'all', [$groupId])
            ->whereNotIn('shift_ids', $shiftIds)
            ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Get list users in groups but not in shift
     *
     * @param  string $shiftId
     * @param  string $groupId
     * @return App\Models\User
     */
    public function getOutShiftId(string $groupId, string $shiftId, int $page = 1)
    {
        return $this->model
                    ->where('group_ids', 'all', [$groupId])
                    ->whereNotIn('shift_ids', [$shiftId])
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * add user to shift
     *
     * @param  string $userId
     * @param  string $shiftId
     * @return array
     */
    public function addUserToShift(string $userId, string $shiftId)
    {
        $user = $this->getById($userId);
        $user->push('shift_ids', $shiftId, true);
    }

    /**
     * Get a user instance via firebase token
     *
     * @param  string $token
     * @return App\Models\User
     */
    public function getByFirebaseToken(string $token)
    {
        return $this->model->where('firebase_tokens', 'all', [$token])->first();
    }

    /**
     * Lấy hữu hạn những User trong group mà chưa có vai trò
     *
     * @param  string $keyword
     * @param  string $groupId
     * @return App\Models\Ủe
     */
    public function getOutRole(string $groupId, $page = 1)
    {
        $roles = $this->role->getByGroupId($groupId);

        return $this->model
                    ->where('group_ids', 'all', [$groupId])
                    ->whereNotIn('role_ids', $roles->pluck('id')->toArray())
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy user theo mã UUID theo máy
     * UUID là mã máy sinh ra duy nhất theo ứng dụng, và sẽ thay đổi khi cài lại ứng dụng
     *
     * @param  string $uuid
     * @return App\Models\User
     */
    public function getByUuid($uuid)
    {
        return $this->model
                    ->where('uuid_devices', 'all', [$uuid])
                    ->first();
    }

    /**
     * Lấy firebase token của nhóm user
     *
     * @param  array $userIds
     * @return [type]
     */
    public function getFirebaseTokensByIds(array $userIds) :array
    {
        $tokens = [];
        $users = $this->getByIds($userIds, [], ['_id', 'firebase_tokens']);
        foreach ($users as $user) {
            if (!empty($user->firebase_tokens)) {
                $tokens = array_merge($tokens, $user->firebase_tokens);
            }
        }
        return $tokens;
    }
}


