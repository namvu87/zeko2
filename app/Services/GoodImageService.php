<?php

namespace App\Services;


use App\Contracts\GoodImageContract;
use App\Contracts\ImageContract;
use App\Models\GoodImage;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class GoodImageService extends BaseService implements GoodImageContract
{
    const PER_PAGE = 24;
    const SIZE     = [480, 480];
    const PREFIX = 'system/goods/';
    const VISIBILITY = 'public';

    private $image;

    public function __construct(GoodImage $model)
    {
        parent::__construct($model);
        $this->image = new ImageManager(['driver' => 'gd']);
    }

    /**
     * Lấy danh sách ảnh hàng hóa/ thực đơn
     *
     * @param string $name
     * @param int    $page
     *
     * @return mixed
     */
    public function getLists(string $name, int $page)
    {
        return $this->model->name($name)->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * get good image folder by name
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getByName(string $name)
    {
        return $this->model->where('name', $name)->first();
    }

    /**
     * Upload images then return realtive path
     *
     * @param array|null $images
     * @param string     $groupId
     *
     * @return array
     */
    public function saveImages($images): array
    {
        $targets = [];
        foreach ($images as $index => $image) {
            $targets[] = $this->saveImage($image, $index);
        }
        return $targets;
    }

    /**
     * Upload single image
     *
     * @param  UploadedFile $image
     * @param  string       $groupId
     *
     * @return string
     */
    public function saveImage(UploadedFile $image, $index): string
    {
        $filename = self::PREFIX . time() . "_{$index}.jpg";

        list($width, $height) = self::SIZE;
        $source = $this->handle($image, $width, $height);

        $this->upload($filename, $source);

        return $filename;
    }

    /**
     * Resize image
     *
     * @param UploadedFile $image
     * @param integer      $width
     * @param integer      $height
     *
     * @return string
     */
    private function handle(UploadedFile $image, int $width, int $height): string
    {
        return $this->image
            ->make($image)
            ->resize($width, $height)
            ->insert('images/watermark.png', 'center')
            ->encode('jpg');
    }

    /**
     * @param  string               $filename
     * @param  Illuminate\Http\File $source
     *
     * @return [type]
     */
    private function upload(string $filename, $source): string
    {
        return Storage::put($filename, $source, self::VISIBILITY);
    }

    /**
     * Xoá 1 ảnh của good chỉ định
     *
     * @param  string $imageUrl
     *
     * @return boolean
     */
    public function removeImage(string $imageUrl): bool
    {
        return Storage::delete($imageUrl);
    }
}
