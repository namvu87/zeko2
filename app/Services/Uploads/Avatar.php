<?php
namespace App\Services\Uploads;

use App\Contracts\ImageContract;
use App\Contracts\Uploads\Avatar as AvatarContract;
use App\Contracts\UserContract;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class Avatar implements AvatarContract
{
    const SIZE = [
        'x1' => 32,
        'x2' => 160,
        'x3' => 576
    ];
    const VISIBILITY = 'public';

    private $image;
    private $user;

    public function __construct(ImageContract $image, UserContract $user)
    {
        $this->image = $image;
        $this->user = $user;
    }

    public function upload(UploadedFile $file) :array
    {
        $avatars = [];
        $user = $this->user->getUser();

        foreach (self::SIZE as $key => $size) {
            $filename = $this->generateFilename($user->id, $size);

            $source = $this->resize($file, $size);

            $this->store($filename, $source);

            $avatars[$key] = $filename;
        }
        $this->remove($user->getOriginal('avatars'));

        return $avatars;
    }

    public function resize(UploadedFile $file, int $size)
    {
        return (string) $this->image->setImage($file)->setSize($size, $size)->resize();
    }

    /**
     * @param  string $filename
     * @param  Illuminate\Http\File $source
     * @return [type]
     */
    public function store(string $filename, $source) :string
    {
        return Storage::put($filename, $source, self::VISIBILITY);
    }

    /**
     * Generate Avatar image name
     *
     * @param  string $userId
     * @param  int    $size
     * @return string
     */
    public function generateFilename($userId, int $size) :string
    {
        return "avatars/p{$size}x{$size}/{$userId}_" . time() . "_{$size}x{$size}.jpg";
    }

    /**
     * Delete old avatar
     *
     * @param  array|string  $urls
     * @return void
     */
    public function remove($urls)
    {
        Storage::delete($urls);
    }
}