<?php

namespace App\Services\Uploads;

use App\Contracts\ImageContract;
use App\Contracts\Uploads\CoverImage as CoverImageContract;
use App\Contracts\UserContract;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class CoverImage implements CoverImageContract
{
    const SIZE = [
        'width' => 768,
        'height' => 512
    ];
    const VISIBILITY = 'public';

    private $image;
    private $user;

    public function __construct(ImageContract $image, UserContract $user)
    {
        $this->image = $image;
        $this->user = $user;
    }

    public function upload(UploadedFile $file) :string
    {
        $user = $this->user->getUser();

        $filename = $this->generateFilename($user->id);
        $source = $this->resize($file);
        $this->store($filename, $source);

        $this->remove($user->getOriginal('cover_image'));

        return $filename;
    }

    public function resize(UploadedFile $file)
    {
        return (string) $this->image
                            ->setImage($file)
                            ->setSize(self::SIZE['width'], self::SIZE['height'])
                            ->resize();
    }

    /**
     * @param  string $filename
     * @param  Illuminate\Http\File $source
     * @return
     */
    public function store(string $filename, $source) :string
    {
        return Storage::put($filename, $source, self::VISIBILITY);
    }

    /**
     * Generate Avatar image name
     *
     * @param  string $userId
     * @param  int    $size
     * @return string
     */
    public function generateFilename($userId) :string
    {
        return "cover_images/{$userId}_" . time() . ".png";
    }

    /**
     * Delete old avatar
     *
     * @param  array|string  $urls
     * @return void
     */
    public function remove($urls)
    {
        Storage::delete($urls);
    }
}