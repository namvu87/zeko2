<?php

namespace App\Services;

use App\Contracts\FirebaseContract;
use GuzzleHttp\Client;
use App\Contracts\UserContract;
use App\Contracts\Users\Firebase as UserFirebaseContract;

class FirebaseService implements FirebaseContract
{
    const SOUND_DEFAULT = 'kingkong.mp3';
    const ANDROID_CHANNEL_ID = 'zeko_notify';
    private $endPoint;
    private $recipient;
    private $topic;
    private $data;
    private $notification;
    private $client;
    private $userContract;
    private $firebase;

    public function __construct(UserContract $userContract, UserFirebaseContract $firebase)
    {
        $this->endPoint = config('firebase.fcm_url');
        $this->client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key=' . config('firebase.legacy_server_key')
            ]
        ]);
        $this->userContract = $userContract;
        $this->firebase = $firebase;
    }

    /**
     * Gửi thông báo đơn mục tiêu
     *
     * @param  array  $recipient
     * @return FirebaseService
     */
    public function to(array $recipient)
    {
        $this->recipient = array_values($recipient);
        return $this;
    }

    /**
     * Gửi thông báo theo chủ đề
     *
     * @param  string $topic
     * @return FirebaseService
     */
    public function toTopic(string $topic)
    {
        $this->topic = $topic;
        return $this;
    }

    /**
     * Payload of notify
     *
     * @param  array  $data
     * @return FirebaseService
     */
    public function data(array $data = [])
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param  array  $notification
     * @return FirebaseService
     */
    public function notification(array $notification = [])
    {
        $this->notification = $notification;
        $this->notification['color'] = '#27a1f2';
        $this->notification['sound'] = self::SOUND_DEFAULT;
        $this->notification['android_channel_id'] = self::ANDROID_CHANNEL_ID;
        return $this;
    }

    /**
     * Execute send notification
     * Remove firebase token rejected
     *
     * @return string
     */
    public function send()
    {
        $payload = [
            'content-available' => true,
            'priority'          => 'high',
            'data'              => $this->data,
            'notification'      => $this->notification
        ];

        if ($this->topic) {
            $payload['to'] = '/topics/' . $this->topic;
        } else {
            if (!$this->recipient) {
                return null;
            }
            $payload['registration_ids'] = $this->recipient;
        }

        $response = $this->client->post($this->endPoint, [
            'body' => json_encode($payload)
        ]);
        $response = json_decode($response->getBody());

        if ($response) {
            $index = 0;
            foreach ($response->results as $value) {
                // $value có dạng { error: 'InvalidRegistration' } hoặc { 'message_id': Number } 
                if (!empty($value->error) &&
                    ($value->error === 'NotRegistered' || $value->error === 'InvalidRegistration' ||
                    $value->error === 'MismatchSenderId')
                ) {
                    $user = $this->userContract->getByFirebaseToken($this->recipient[$index]);
                    if ($user) {
                        $this->firebase->pull($user, $this->recipient[$index]);
                    }
                }
                $index ++;
            }
        }

        return $response;
    }
}