<?php

namespace App\Services\Restaurant;

use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\ImportInvoiceContract;
use App\Models\ImportInvoice;
use App\Services\BaseService;
use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model;

class ImportInvoiceService extends BaseService implements ImportInvoiceContract
{
    const PER_PAGE = 10;

    private $good;

    public function __construct(ImportInvoice $model, GoodContract $good)
    {
        parent::__construct($model);
        $this->good = $good;
    }

    /**
     * Lấy trạng thái hóa đơn tạm thời
     *
     * @return int
     */
    public function getStatusTemporary()
    {
        return $this->model::STATUS_TEMPORARY;
    }

    /**
     * Lấy trạng thái hóa đơn đã thanh toán
     *
     * @return int
     */
    public function getStatusFinished()
    {
        return $this->model::STATUS_FINISHED;
    }

    /**
     * Lấy đầu Mã hóa đơn
     *
     * @return int
     */
    public function getSymbolCode()
    {
        return $this->model::SYMBOL_CODE;
    }

    /**
     * Lấy trạng thái hóa đơn đã thanh toán
     *
     * @return int
     */
    public function getByCode($code)
    {
        return $this->model->where('code', $code)->first();
    }

    /**
     * generate new Code import invoice
     *
     * @return string
     */
    private function generateCode(string $groupId)
    {
        $newNumber = $this->model->where('group_id', $groupId)->count();
        do {
            $newNumber = $newNumber + 1;
            $code = $this->getSymbolCode() . str_pad($newNumber, 6, '0', STR_PAD_LEFT);
        } while ($this->getByCode($code) !== null);
        return $code;
    }

    /**
     * store new import invoice
     *
     * @return App\Models\Invoice
     */
    public function store(array $data)
    {
        $arr = array();
        foreach ($data['goods'] as $key => $good) {
            $obj = new \stdClass();
            if (empty($good['id'])) {
                $good['id'] = $this->good->getNormalGoodsByCode($data['group_id'], $good['code'])->id;
            }
            $obj->id = $good['id'];
            $obj->code = $good['code'];
            $obj->name = $good['name'];
            $obj->price = $good['price'];
            $obj->count = $good['count'];
            $obj->unit = $good['unit'];
            $obj->discount = $good['discount'];
            $obj->discount_type = $good['discount_type'];
            array_push($arr, $obj);
        }
        $invoice = $this->create([
            'group_id' => $data['group_id'],
            'code' => $data['code'] ?? $this->generateCode($data['group_id']),
            'recipient_id' => auth('api')->user()->id,
            'goods' => $arr,
            'note' => $data['note'],
            'payment_method' => $data['payment_method'],
            'paid_at' => Carbon::now(),
            'status' => $data['status']
        ]);
        return $invoice->goods;
    }

    /**
     * get list import invoice by condition
     *
     * @return App\Models\Invoice
     */
    public function getImportInvoiceByCondition(array $data)
    {
        return $this->model->with('recipient')
            ->where('group_id', $data['group_id'])
            ->createdAt(
                !empty($data['start']) ? new Carbon($data['start']) : null,
                !empty($data['end']) ? new Carbon(Carbon::createFromFormat('d-m-Y H:i:s', $data['end'] . '23:59:59')) : null
            )
            ->status($data['status'] ?? null)
            ->paginate(self::PER_PAGE);
    }

    /**
     * get data report
     *
     * @return array
     */
    public function getReportGoods(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        $goods = array();
        $this->model
            ->where('group_id', $groupId)
            ->createdAt($startDate, $endDate)
            ->status(self::getStatusFinished())
            ->chunk(1000, function($invoices) use (&$goods) {
                foreach ($invoices as $invoice) {
                    $columns = array_column($goods, 'id');
                    foreach ($invoice->goods as $good) {
                        if (in_array($good['id'], $columns)) {
                            $index = array_search($good['id'], $columns);
                            $goods[$index]['count'] += $good['count'];
                            $goods[$index]['total'] += ($good['price'] * $good['count'] - $this->getDiscountGoods($good));
                            $goods[$index]['total_discount'] += $this->getDiscountGoods($good);
                        } else {
                            $good['total_discount'] = $this->getDiscountGoods($good);
                            $good['total'] = $good['price'] * $good['count'] - $this->getDiscountGoods($good);
                            array_push($goods, $good);
                        }
                    }
                };
            });
        usort($goods, function($a, $b) {
            return $b['total'] - $a['total'];
        });
        return $goods;
    }

    private function getDiscountGoods($good)
    {
        $priceDiscount = $good['discount'] ?? 0;
        if ($good['discount'] && $good['discount_type'] == 2) {
            $priceDiscount = $good['price'] * $good['discount'] / 100;
        }
        return intval($priceDiscount * $good['count']);
    }
}
