<?php

namespace App\Services\Restaurant;

use App\Contracts\GroupContract;
use App\Contracts\ImageContract;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\Restaurant\GroupMenuContract;
use App\Models\Good;
use App\Services\BaseService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class GoodService extends BaseService implements GoodContract
{
    const SIZE = 480;
    const PER_PAGE = 25;
    const VISIBILITY = 'public';
    const LIMIT = 10;

    protected $image;
    protected $group;
    protected $menu;

    public function __construct(
        Good $good,
        ImageContract $image,
        GroupContract $group,
        GroupMenuContract $groupMenuContract
    ) {
        parent::__construct($good);

        $this->image = $image;
        $this->group = $group;
        $this->menu = $groupMenuContract;
    }

    /**
     * Lấy trạng thái đang hoạt động
     *
     * @return int
     */
    public function getStatusActive(): int
    {
        return $this->model::STATUS_ACTIVE;
    }

    /**
     * Lấy trạng thái ngừng hoạt động
     *
     * @return int
     */
    public function getStatusInactive(): int
    {
        return $this->model::STATUS_INACTIVE;
    }

    /**
     * get symbol code
     *
     * @return string
     */
    public function getSymbolCode(): string
    {
        return $this->model::SYMBOL_CODE;
    }

    /**
     * Lấy giá trị loại hàng nhập
     *
     * @return int
     */
    public function getImportType(): int
    {
        return $this->model::GOOD_IMPORTED_TYPE;
    }

    /**
     * Lấy giá trị loại hàng chế biến
     *
     * @return int
     */
    public function getProcessType(): int
    {
        return $this->model::GOOD_PROCESSED_TYPE;
    }

    /**
     * get array goods type
     *
     * @return string
     */
    public function getGoodsType(): array
    {
        return $this->model::GOOD_TYPES;
    }


    /**
     * get max file upload
     *
     * @return int
     */
    public function getMaxFileUpload(): int
    {
        return $this->model::MAX_FILE_UPLOAD;
    }

    /**
     * get max size upload
     *
     * @return int
     */
    public function getMaxSizeUpload(): int
    {
        return $this->model::MAX_SIZE_UPLOAD;
    }

    /**
     * get goods via code
     *
     * @param  string $code
     *
     * @return App\Models\Good
     */
    public function getByCode(string $groupId, string $code)
    {
        return $this->model->withTrashed()
            ->where('group_id', $groupId)
            ->where('code', $code)
            ->first();
    }

    /**
     * get normal goods by code
     *
     * @param  string $groupId
     * @param  string $code
     *
     * @return App\Models\Good
     */
    public function getNormalGoodsByCode(string $groupId, string $code)
    {
        return $this->model->where('group_id', $groupId)
            ->where('code', $code)
            ->where('status', self::getStatusActive())
            ->where('type', self::getGoodsType()[0])
            ->first();
    }

    /**
     * generate new goods code
     *
     * @return string
     */
    public function generateCode(string $groupId): string
    {
        $group = $this->group->getById($groupId);
        $count = (int) $group->goods_number ?? 0;
        do {
            $code = $this->getSymbolCode();
            $count = $count + 1;
            $num = '00000000' . $count;
            $code = $code . substr($num, -7);
        } while ($this->getByCode($groupId, $code) !== null);
        return $code;
    }

    /**
     * get imported goods in group
     *
     * @param  string $groupId
     *
     * @return App\Models\Good
     */
    public function getImportedGoods(string $groupId, string $name = '')
    {
        return $this->model
            ->where('group_id', $groupId)
            ->type($this->model::GOOD_IMPORTED_TYPE)
            ->status(self::getStatusActive())
            ->name($name)
            ->get();
    }

    /**
     * Gets all goods in group for sale.
     *
     * @param  string $groupId
     *
     * @return App\Models\Good
     */
    public function getGoodsForSale(string $groupId)
    {
        return $this->model
            ->where('group_id', $groupId)
            ->isSale(true)
            ->get();
    }

    /**
     * Lấy danh sách hàng hoá theo chủng loại
     *
     * @param  array $request
     *
     * @return App\Models\Good
     */
    public function getByCondition(array $request, array $relations = [], int $page = 1)
    {
        return $this->model
            ->with($relations)
            ->where('group_id', $request['group_id'])
            ->whereNull('parent_id')
            ->fullText($request['name'] ?? null)
            ->status($request['status'] ?? null)
            ->inventory($request['inventory'] ?? null)
            ->groupMenu($request['group_menu_id'] ?? null)
            ->type($request['type'] ?? null)
            ->isSale($request['is_sale'] ?? null)
            ->orderBy('updated_at', 'DESC')
            ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Danh sách hàng để bán
     *
     * @param  array $request
     * @param  integer $page
     *
     * @return App\Models\Good
     */
    public function getMenu(array $args, int $page = 1)
    {
        return $this->model
            ->with('childs')
            ->where('group_id', $args['group_id'])
            ->whereNull('parent_id')
            ->where('is_sale', true)
            ->status($this->getStatusActive())
            ->groupMenu($args['group_menu_id'] ?? null)
            ->fulltext($args['keyword'] ?? '')
            ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    // /**
    //  * change status to active
    //  *
    //  * @param  string $goodId
    //  *
    //  * @return App\Models\Good
    //  */
    // public function enable(string $goodId)
    // {
    //     $this->update($goodId, [
    //         'status' => $this->getStatusActive()
    //     ]);
    // }

    // /**
    //  * change status to inactive
    //  *
    //  * @param  string $goodId
    //  *
    //  * @return App\Models\Good
    //  */
    // public function disable(string $goodId)
    // {
    //     $good = $this->getById($goodId, ['childs']);
    //     foreach ($good->childs as $child) {
    //         $this->update($child->id, [
    //             'status' => $this->getStatusInactive()
    //         ]);
    //     }
    //     $this->update($goodId, [
    //         'status' => $this->getStatusInactive()
    //     ]);
    // }

    /**
     * update Good
     *
     * @param  array $goodIds
     * @param  array $menuIds
     *
     * @return App\Models\Good
     */
    public function updateGoodsGroupMenus(array $goodIds, array $menuIds)
    {
        foreach ($goodIds as $goodId) {
            $good = $this->getById($goodId);
            $arr1 = array_diff($good->group_menu_ids, $menuIds);
            $arr2 = array_diff($menuIds, $good->group_menu_ids);
            $good->update(['group_menu_ids' => $menuIds]);
            foreach ($arr1 as $groupMenu) {
                $this->menu->getById($groupMenu)->pull('goods_ids', $goodId);
            }
            foreach ($arr2 as $groupMenu) {
                $this->menu->getById($groupMenu)->push('goods_ids', $goodId, true);
            }
        }
    }

    /**
     * search limited goods by name
     *
     * @param  string $goodId
     * @param  string $name
     *
     * @return App\Models\Good
     */
    public function limitGoodsByName(string $groupId, string $name)
    {
        return $this->model
            ->where('group_id', $groupId)
            ->name($name)
            ->take(self::LIMIT)->get();
    }

    /**
     * Upload images then return realtive path
     *
     * @param array|null $images
     * @param string $groupId
     *
     * @return array
     */
    public function saveImages($images, string $groupId): array
    {
        $targets = [];
        foreach ($images as $index => $image) {
            $targets[] = $this->saveImage($image, $groupId, $index);
        }
        return $targets;
    }

    /**
     * Upload single image
     *
     * @param  UploadedFile $image
     * @param  string $groupId
     *
     * @return string
     */
    public function saveImage(UploadedFile $image, string $groupId, $suffix = 0): string
    {
        $filename = $this->generateFilename($groupId, $suffix);

        list($width, $height) = $this->getImageSize($image);
        $source = $this->resize($image, $width, $height);

        $this->upload($filename, $source);

        return $filename;
    }

    /**
     * Generate image name
     *
     * @param  string $groupId
     * @param  string $suffix
     *
     * @return string
     */
    private function generateFilename($groupId, $suffix): string
    {
        return "goods/{$groupId}/" . time() . "_{$suffix}.jpg";
    }

    /**
     * get size image
     *
     * @param $image
     *
     * @return array
     */
    private function getImageSize($image): array
    {
        list($width, $height) = getimagesize($image);
        $rate = $width / $height;
        return $rate >= 1 ? [(int) ($rate * self::SIZE), self::SIZE] : [self::SIZE, (int) (self::SIZE / $rate)];
    }

    /**
     * Resize image
     *
     * @param UploadedFile $image
     * @param integer $width
     * @param integer $height
     *
     * @return string
     */
    private function resize(UploadedFile $image, int $width = null, int $height = null): string
    {
        return (string) $this->image
            ->setImage($image)
            ->setSize($width, $height)
            ->resize();
    }

    /**
     * @param  string $filename
     * @param  Illuminate\Http\File $source
     *
     * @return [type]
     */
    private function upload(string $filename, $source): string
    {
        return Storage::put($filename, $source, self::VISIBILITY);
    }

    /**
     * Xoá 1 ảnh của good chỉ định
     *
     * @param  string $imageUrl
     *
     * @return boolean
     */
    public function removeImage(string $imageUrl): bool
    {
        if (Str::substr($imageUrl, 0, 7) !== 'system/') return Storage::delete($imageUrl);
        return false;
    }

    /**
     * Gán hàng hoá để bán
     *
     * @param  array $goodsIds
     *
     * @return boolean
     */
    public function assignSale(array $goodsIds)
    {
        $this->model->whereIn('_id', $goodsIds)->update(['is_sale' => true]);
    }

    /**
     * Lấy những hàng hoá có thành phần chỉ định
     *
     * @param  string $goodId
     *
     * @return App\Models\Good
     */
    public function getByIngredientId(string $goodId)
    {
        return $this->model->whereRaw([
            'ingredients' => [
                '$elemMatch' => [
                    'id' => $goodId
                ]
            ]
        ])->get();
    }

    /**
     * restore number goods after disable invoice
     *
     * @param  string $goodId
     *
     * @return App\Models\Good
     */
    public function restoreNumberFromGoodsInvoice(array $goods)
    {
        foreach ($goods as $key => $good) {
            $model = $this->getById($good['id']);
            if ($model) {
                if ($model->type == $this->getImportType()) {
                    $model->update(['inventory_number' => $model->inventory_number + $good['count']]);
                } elseif ($model->type == $this->getProcessType() && !empty($model->ingredients)) {
                    foreach ($model->ingredients as $ingredient) {
                        $ingrre = $this->getById($ingredient['id']);
                        $ingrre->update(['inventory_number' => $ingrre->inventory_number + $ingredient['count']]);
                    }
                }
            }
        }
    }

    /**
     * update sale many goods
     *
     * @param  string $groupId
     * @param  array $goodsIds
     *
     * @return App\Models\Good
     */
    public function updateIsSaleByIds(string $groupId, array $goodsIds)
    {
        $this->model->where('group_id', $groupId)
            ->type($this->model::GOOD_IMPORTED_TYPE)
            ->status(self::getStatusActive())
            ->whereIn('_id', $goodsIds)
            ->update(['is_sale' => true]);
        $this->model->where('group_id', $groupId)
            ->type($this->model::GOOD_IMPORTED_TYPE)
            ->status(self::getStatusActive())
            ->whereNotIn('_id', $goodsIds)
            ->update(['is_sale' => false]);
        return;
    }

    /**
     * Lấy số lượng hàng hóa chứa ảnh này
     *
     * @param string $url
     */
    public function countByUrlImage(string $url)
    {
        return $this->model->where('images', 'all', [$url])->count();
    }

    /**
     * Lấy số lượng hàng hóa chứa ảnh này
     *
     * @param array $urls
     */
    public function countByUrlImages(array $urls)
    {
        return $this->model->whereIn('images', $urls)->count();
    }
}
