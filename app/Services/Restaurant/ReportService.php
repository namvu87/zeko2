<?php

namespace App\Services\Restaurant;

use App\Contracts\Restaurant\ReportContract;
use App\Models\ImportInvoice;
use App\Models\Invoice;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use MongoDB\BSON\UTCDateTime;
use App\Contracts\Restaurant\InvoiceContract;

class ReportService implements ReportContract
{
    private $model;

    public function __construct(Invoice $model)
    {
        $this->model = $model;
    }

    /**
     * Lấy số lượng hoá đơn trong ngày của 1 nhóm, theo trạng thái hoá đơn
     *
     * @param array $args
     * @return void
     */
    public function getCountByStatus(string $groupId, Carbon $startDate, Carbon $endDate, int $status)
    {
        return $this->model
                    ->groupId($groupId)
                    ->status($status)
                    ->createdAt($startDate, $endDate)
                    ->count();
    }

    /**
     * Lấy tổng doanh thu của group theo khoảng ngày
     * Đồng thời lấy số lượng hoá đơn đã thanh toán
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getTotalRevenueByDateRange(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return $this->model->raw(function($collection) use ($groupId, $startDate, $endDate) {
            return $collection->aggregate([
                $this->matchPurchasedInvoice($groupId, $startDate, $endDate),
                [
                    '$group' => [
                        '_id' => null,
                        'revenue' => ['$sum' => '$total'],
                        'purchased_invoice' => ['$sum' => 1]
                    ]
                ]
            ]);
        });
    }

    /**
     * Báo cáo bán hàng (doanh số) theo khoảng thời gian
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @param array $groupIdOperator
     * @param array $sort
     * @return array
     */
    public function getRevenueReport(
        string $groupId, Carbon $startDate, Carbon $endDate, array $groupIdOperator, array $sort
    )
    {
        return $this->model->raw(function($collection)
            use ($groupId, $startDate, $endDate, $groupIdOperator, $sort) {

            return $collection->aggregate([
                $this->matchPurchasedInvoice($groupId, $startDate, $endDate),
                [
                    '$group' => [
                        '_id' => $groupIdOperator,
                        'total' => [
                            '$sum' => '$total'
                        ],
                        'total_discount' => [
                            '$sum' => '$total_discount'
                        ],
                        'total_origin' => [
                            '$sum' => '$total_origin'
                        ],
                        'total_returned' => [
                            '$sum' => '$total_returned'
                        ]
                    ]
                ],
                $sort
            ]);
        });
    }

    /**
     * So khớp dữ liệu thống kê
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function matchPurchasedInvoice(string $groupId, Carbon $startDate, Carbon $endDate): array
    {
        return [
            '$match' => [
                'group_id' => $groupId,
                'status' => $this->model::STATUS_PURCHASED,
                'paid_at' => [
                    '$gte' => new UTCDateTime($startDate),
                    '$lte' => new UTCDateTime($endDate)
                ]
            ]
        ];
    }

    private function totalGoodsPrice()
    {
        return [
            '$sum' => [
                '$let' => [
                    'vars' => [
                        'totalPriceGoods' => [
                            '$multiply' => ['$goods.count', '$goods.price']
                        ],
                        'discountedGoods' => [
                            '$cond' => [
                                'if' => ['$eq' => [['$ifNull' => ['$goods.discount_type', 0]], 2]],
                                'then' => [
                                    '$multiply' => [
                                        '$goods.count',
                                        '$goods.price',
                                        ['$divide' => ['$goods.discount', 100]]
                                    ]
                                ],
                                'else' => ['$ifNull' => ['$goods.discount', 0]]
                            ]
                        ]
                    ],
                    'in' => [
                        '$subtract' => ['$$totalPriceGoods', '$$discountedGoods']
                    ]
                ],
            ]
        ];
    }

    private function addSubtotalGoods()
    {
        return [
            '$addFields' => [
                'goods.subtotal' => [
                    '$multiply' => ['$goods.count', '$goods.price']
                ]
            ]
        ];
    }

    private function addDiscountedGoods()
    {
        return [
            '$addFields' => [
                'goods.discounted' => [
                    '$cond' => [
                        'if' => ['$eq' => [['$ifNull' => ['$goods.discount_type', 0]], 2]],
                        'then' => [
                            '$multiply' => [
                                '$goods.count',
                                '$goods.price',
                                ['$divide' => ['$goods.discount', 100]]
                            ]
                        ],
                        'else' => ['$ifNull' => ['$goods.discount', 0]]
                    ]
                ]
            ]
        ];
    }

    private function addSubtotal()
    {
        return [
            '$addFields' => [
                'subtotal' => $this->totalGoodsPrice()
            ]
        ];
    }

    private function totalOriginGoods()
    {
        return [
            '$sum' => [
                '$multiply' => ['$goods.count', '$goods.price_origin']
            ]
        ];
    }

    private function addTotalOrigin()
    {
        return [
            '$addFields' => [
                'total_origin' => $this->totalOriginGoods()
            ]
        ];
    }

    private function addDiscounted()
    {
        return [
            '$addFields' => [
                'discounted' => [
                    '$cond' => [
                        'if' => ['$eq' => [['$ifNull' => ['$discount_type', 0]], 2]],
                        'then' => [
                            '$multiply' => [
                                '$subtotal',
                                ['$divide' => ['$discount', 100]]
                            ]
                        ],
                        'else' => ['$ifNull' => ['$discount', 0]]
                    ]
                ]
            ]
        ];
    }

    private function setPeriodTime(Carbon $startDate, Carbon $endDate)
    {
        $period = CarbonPeriod::create($startDate, $endDate)->count();

        $timezone = [
            'date' => '$paid_at',
            'timezone' => config('app.timezone')
        ];

        $idGroup = null;
        $sort = [];
        if ($period == 1) {
            $idGroup = [
                'hour' => [
                    '$hour' => $timezone
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.hour' => 1
                ]
            ];
        } elseif ($period < 32) {
            $idGroup = [
                'month' => [
                    '$month' => $timezone
                ],
                'year' => [
                    '$year' => $timezone
                ],
                'day' => [
                    '$dayOfMonth' => $timezone
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.year' => 1,
                    '_id.month' => 1,
                    '_id.day' => 1
                ]
            ];
        } else {
            $idGroup = [
                'month' => [
                    '$month' => $timezone
                ],
                'year' => [
                    '$year' => $timezone
                ]
            ];
            $sort = [
                '$sort' => [
                    '_id.year' => 1,
                    '_id.month' => 1
                ]
            ];
        }
        return [$idGroup, $sort];
    }

    private function matchInvoice(string $groupId, Carbon $startDate, Carbon $endDate = null)
    {
        $match = [
            '$match' => [
                'group_id' => $groupId,
                'status' => Invoice::STATUS_PURCHASED,
                'paid_at' => [
                    '$gte' => new UTCDateTime($startDate)
                ]
            ]
        ];
        if ($endDate) {
            $match['$match']['paid_at']['$lte'] = new UTCDateTime($endDate);
        }

        return $match;
    }

    /**
     * dữ liệu thống kê hóa đơn trong ngày
     *
     * @param string $groupId
     *
     * @return mixed
     */
    public function getStatisticToday(string $groupId)
    {
        $date = Carbon::now()->startOfDay();
        $inited = Invoice::where('group_id', $groupId)
            ->where('paid_at', '>', $date)
            ->status(Invoice::STATUS_INITED)->count();
        $purchasedTotal = Invoice::where('group_id', $groupId)
            ->where('paid_at', '>', $date)
            ->status(Invoice::STATUS_PURCHASED)->count();
        $purchased = Invoice::raw(function($collection) use ($groupId, $date) {
            return $collection->aggregate([
                $this->matchInvoice($groupId, $date),
                ['$unwind' => '$goods'],
                $this->addSubtotal(),
                $this->addDiscounted(),
                [
                    '$group' => [
                        '_id' => null,
                        'purchasedMoney' => [
                            '$sum' => [
                                '$subtract' => ['$subtotal', '$discounted'],
                            ]
                        ]
                    ]
                ],
                [
                    '$project' => [
                        '_id' => 0,
                    ]
                ]
            ]);
        });
        if ($purchased->count()) {
            $purchased[0]->initedTotal = $inited;
            $purchased[0]->purchasedTotal = $purchasedTotal;
            return $purchased[0];
        }
        return (object)[
            'purchasedMoney' => 0,
            'initedTotal' => $inited,
            'purchasedTotal' => $purchasedTotal
        ];
    }

    /**
     * Dữ liệu thống kê hàng hóa theo hóa đơn trong khoảng thời gian
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function statisticGoods(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return Invoice::raw(function($collection) use ($groupId, $startDate, $endDate) {
            return $collection->aggregate([
                $this->matchInvoice($groupId, $startDate, $endDate),
                ['$unwind' => '$goods'],
                [
                    '$group' => [
                        '_id' => '$goods.id',
                        'name' => [
                            '$first' => '$goods.name'
                        ],
                        'revenue' => $this->totalGoodsPrice(),
                        'count' => [
                            '$sum' => '$goods.count'
                        ]
                    ]
                ],
                [
                    '$sort' => [
                        'revenue' => -1
                    ]
                ]
            ]);
        });
    }

    /**
     * Dữ liệu thống kê doanh thu trong khoảng thời gian
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function statisticRevenue(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        list($idGroup, $sort) = $this->setPeriodTime($startDate, $endDate);
        $result = Invoice::raw(function($collection) use ($groupId, $startDate, $endDate, $idGroup, $sort) {
            return $collection->aggregate([
                $this->matchInvoice($groupId, $startDate, $endDate),
                // ['$unwind' => '$goods'],
                // $this->addSubtotal(),
                // $this->addDiscounted(),
                [
                    '$group' => [
                        '_id' => $idGroup,
                        'revenue' => [
                            '$sum' => '$total'
                        ]
                    ]
                ],
                $sort
            ]);
        });

        return $result;
    }

    /**
     * Dữ liệu báo cáo bán hàng
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function getReportSale(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        list($idGroup, $sort) = $this->setPeriodTime($startDate, $endDate);
        return Invoice::raw(function($collection) use ($groupId, $startDate, $endDate, $idGroup, $sort) {
            return $collection->aggregate([
                $this->matchInvoice($groupId, $startDate, $endDate),
                ['$unwind' => '$goods'],
                $this->addTotalOrigin(),
                $this->addSubtotal(),
                $this->addDiscounted(),
                [
                    '$group' => [
                        '_id' => $idGroup,
                        'total' => [
                            '$sum' => [
                                '$subtract' => ['$subtotal', '$discounted']
                            ]
                        ],
                        'discounted' => [
                            '$sum' => '$discounted'
                        ],
                        'total_origin' => [
                            '$sum' => '$total_origin'
                        ]
                    ]
                ],
                $sort
            ]);
        });
    }

    /**
     * Dữ liệu báo cáo hàng hóa
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function getReportGoods(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return Invoice::raw(function($collection) use ($groupId, $startDate, $endDate) {
            return $collection->aggregate([
                $this->matchInvoice($groupId, $startDate, $endDate),
                ['$unwind' => '$goods'],
                $this->addDiscountedGoods(),
                $this->addSubtotalGoods(),
                [
                    '$group' => [
                        '_id' => '$goods.id',
                        'name' => [
                            '$first' => '$goods.name'
                        ],
                        'code' => [
                            '$first' => '$goods.code'
                        ],
                        'total' => [
                            '$sum' => [
                                '$subtract' => ['$goods.subtotal', '$goods.discounted']
                            ]
                        ],
                        'discounted' => [
                            '$sum' => '$goods.discounted'
                        ],
                        'total_origin' => [
                            '$sum' => [
                                '$multiply' => ['$goods.count', '$goods.price_origin']
                            ]
                        ],
                        'count' => [
                            '$sum' => '$goods.count'
                        ]
                    ]
                ],
                [
                    '$sort' => [
                        'total' => -1
                    ]
                ]
            ]);
        });
    }

    /**
     * Dữ liệu báo cáo nhập hàng
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return mixed
     */
    public function getReportGoodsImported(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return ImportInvoice::raw(function($collection) use ($groupId, $startDate, $endDate) {
            return $collection->aggregate([
                $this->matchInvoice($groupId, $startDate, $endDate),
                ['$unwind' => '$goods'],
                $this->addSubtotalGoods(),
                $this->addDiscountedGoods(),
                [
                    '$group' => [
                        '_id' => '$goods.id',
                        'name' => [
                            '$first' => '$goods.name'
                        ],
                        'code' => [
                            '$first' => '$goods.code'
                        ],
                        'total' => [
                            '$sum' => [
                                '$subtract' => ['$goods.subtotal', '$goods.discounted']
                            ]
                        ],
                        'discounted' => [
                            '$sum' => '$goods.discounted'
                        ],
                        'count' => [
                            '$sum' => '$goods.count'
                        ]
                    ]
                ],
                [
                    '$sort' => [
                        'total' => -1
                    ]
                ]
            ]);
        });
    }

    /**
     * Lấy số lượng hoá đơn đã thanh toán
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getQuantityInvoice(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return $this->model->raw(function($collection) use ($groupId, $startDate, $endDate) {
            return $collection->aggregate([
                $this->matchPurchasedInvoice($groupId, $startDate, $endDate),
                [
                    '$group' => [
                        '_id' => null,
                        'purchased_invoice' => ['$sum' => 1]
                    ]
                ]
            ]);
        });
    }

    /**
     * Lấy tổng doanh thu của group theo khoảng ngày
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getRevenueInvoice(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return $this->model->raw(function($collection) use ($groupId, $startDate, $endDate) {
            return $collection->aggregate([
                $this->matchPurchasedInvoice($groupId, $startDate, $endDate),
                [
                    '$group' => [
                        '_id' => null,
                        'revenue' => ['$sum' => '$total'],
                    ]
                ]
            ]);
        });
    }
}
