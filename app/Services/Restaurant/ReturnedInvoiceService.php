<?php

namespace App\Services\Restaurant;

use App\Contracts\Restaurant\ReturnedInvoiceContract;
use App\Models\ReturnedInvoice;
use App\Services\BaseService;
use Carbon\Carbon;

class ReturnedInvoiceService extends BaseService implements ReturnedInvoiceContract
{
    const PER_PAGE = 15;

    public function __construct(ReturnedInvoice $model)
    {
        parent::__construct($model);
    }

    public function getStatusPaid()
    {
        return $this->model::STATUS_PAID;
    }

    public function getStatusDisable()
    {
        return $this->model::STATUS_DISABLE;
    }

    /**
     * get symbol code
     *
     * @return string
     */
    private function getSymbolCode(): string
    {
        return $this->model::SYMBOL_CODE;
    }

    /**
     * Lấy invoice qua code
     *
     * @param  string $code
     *
     * @return App\Models\Good
     */
    private function getByCode(string $groupId, string $code)
    {
        return $this->model
            ->where('group_id', $groupId)
            ->where('code', $code)
            ->first();
    }

    /**
     * Lấy danh sách hóa đơn theo điều kiện
     *
     * @return App\Models\Invoice
     */
    public function filterByCondition(array $data)
    {
        return $this->model->with('creator:first_name,last_name')
            ->where('group_id', $data['group_id'])
            ->createdAt(
                !empty($data['start']) ? new Carbon($data['start']) : null,
                !empty($data['end']) ? new Carbon(Carbon::createFromFormat('d-m-Y H:i:s', $data['end'] . '23:59:59')) : null
            )
            ->status($data['status'] ?? null)
            ->code($data['code'] ?? null)
            ->paginate(self::PER_PAGE);
    }

    /**
     * Tạo ra mã code
     * Code is unique in group
     *
     * @return string
     */
    public function generateCode(string $groupId): string
    {
        $count = $this->model->count();
        do {
            $code = $this->getSymbolCode();
            $count = $count + 1;
            $code = $code . str_pad((string) $count, 6, '0', STR_PAD_LEFT);
        } while ($this->getByCode($groupId, $code) !== null);
        return $code;
    }

    /**
     * Lấy danh sách hoá đơn trả hàng theo ID hoá đơn gốc
     *
     * @param string $invoiceId
     * @return void
     */
    public function getByInvoiceId(string $invoiceId)
    {
        return $this->model->where('invoice_id', $invoiceId)->get();
    }

    /**
     * Phân trang theo điều kiện truyền vào
     *
     * @param array $data
     * @param integer $page
     * @param array $relations
     * @param array $selections
     * @return void
     */
    public function paginateByCondition(
        array $data,
        int $page,
        array $relations = [],
        array $selections = []
    ) {
        return $this->model
            ->with($relations)
            ->where('group_id', $data['group_id'])
            ->createdAt($data['start_date'], $data['end_date'])
            ->status($data['status'] ?? null)
            ->code($data['code'] ?? null)
            ->select($selections)
            ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy số lượng hoá đơn theo khoảng ngày
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return integer
     */
    public function getCountByCondition(string $groupId, Carbon $startDate, Carbon $endDate): int
    {
        return $this->model
            ->groupId($groupId)
            ->createdAt($startDate, $endDate)
            ->count();
    }
}
