<?php

namespace App\Services\Restaurant;

use App\Contracts\Restaurant\InvoiceContract;
use App\Contracts\Restaurant\GoodContract;
use App\Contracts\GroupContract;
use App\Models\Invoice;
use App\Services\BaseService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectID;

class InvoiceService extends BaseService implements InvoiceContract
{
    const PER_PAGE = 10;
    const TOP_SELLING_GOODS = 10;

    private $group;

    private $good;

    public function __construct(Invoice $invoice, GroupContract $group, GoodContract $good)
    {
        parent::__construct($invoice);
        $this->group = $group;
        $this->good = $good;
    }

    /**
     * Lấy trạng thái hóa đơn chưa thanh toán
     *
     * @return array
     */
    public function getStatusInited(): int
    {
        return $this->model::STATUS_INITED;
    }

    /**
     * Lấy trạng thái hóa đơn đã thanh toán
     *
     * @return array
     */
    public function getStatusPurchased(): int
    {
        return $this->model::STATUS_PURCHASED;
    }

    /**
     * Lấy trạng thái hóa đơn lỗi bị hủy bỏ
     *
     * @return array
     */
    public function getStatusDisable(): int
    {
        return $this->model::STATUS_DISABLE;
    }

    /**
     * Lấy trạng thái món đã đặt
     *
     * @return string
     */
    public function getGoodBookedStatus(): string
    {
        return $this->model::GOOD_BOOKED_STATUS;
    }

    /**
     * Lấy trạng thái món đang chế biến
     *
     * @return string
     */
    public function getGoodProcessingStatus(): string
    {
        return $this->model::GOOD_PROCESSING_STATUS;
    }

    /**
     * Lấy trạng thái món đã chế biến
     *
     * @return string
     */
    public function getGoodProcessedStatus(): string
    {
        return $this->model::GOOD_PROCESSED_STATUS;
    }

    /**
     * Lấy trạng thái món đã giao hàng
     *
     * @return string
     */
    public function getDeliveredStatus(): string
    {
        return $this->model::GOOD_DELIVERED_STATUS;
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getReturnedGoodsStatus(): string
    {
        return $this->model::GOODS_RETURNED_STATUS;
    }

    /**
     * Lấy trạng thái không có yêu cầu
     *
     * @return int
     */
    public function getNoneRequestStatus(): int
    {
        return $this->model::NONE_REQUEST;
    }

    /**
     * Lấy trạng thái yêu cầu tạo hoá đơn
     *
     * @return int
     */
    public function getCreateInvoiceRequestStatus(): int
    {
        return $this->model::CREATE_REQUEST_STATUS;
    }

    /**
     * Lấy trạng thái yêu cầu thêm món
     *
     * @return int
     */
    public function getAdditionFoodRequestStatus(): int
    {
        return $this->model::ADDITION_REQUEST_STATUS;
    }

    /**
     * Lấy trạng thái yêu cầu thanh toán
     *
     * @return int
     */
    public function getPurchaseRequestStatus(): int
    {
        return $this->model::PURCHASE_REQUEST;
    }

    /**
     * get symbol code
     *
     * @return string
     */
    private function getSymbolCode(): string
    {
        return $this->model::SYMBOL_CODE;
    }

    /**
     * Lấy loại discount theo phần trăm
     *
     * @return     integer  The discount type percent.
     */
    public function getDiscountTypePercent(): int
    {
        return $this->model::DISCOUNT_TYPE_PERCENT;
    }

    /**
     * Lấy loại discount theo giá
     *
     * @return     integer  The discount type percent.
     */
    public function getDiscountTypeFixed(): int
    {
        return $this->model::DISCOUNT_TYPE_FIXED;
    }

    /**
     * Lấy tất cả các strạng thái của món ăn
     *
     * @return int
     */
    public function getGoodStatus(): array
    {
        return [
            $this->model::GOOD_BOOKED_STATUS,
            $this->model::GOOD_PROCESSING_STATUS,
            $this->model::GOOD_DELIVERED_STATUS
        ];
    }

    /**
     * Mảng chứa các loại discount
     *
     * @return     array  The discount types.
     */
    public function getDiscountTypes(): array
    {
        return $this->model::DISCOUNT_TYPES;
    }

    /**
     * Lấy ra trạng thái không có request nào
     *
     * @return     integer  The status none request.
     */
    public function getStatusNoneRequest(): int
    {
        return $this->model::NONE_REQUEST;
    }

    /**
     * Lấy ra trạng thái có request tạo hóa đơn nào
     *
     * @return     integer  The status create request.
     */
    public function getStatusCreateRequest(): int
    {
        return $this->model::CREATE_REQUEST_STATUS;
    }

    /**
     * Lấy ra trạng thái có request thêm món ăn
     *
     * @return     integer  The status addition request.
     */
    public function getStatusAdditionRequest(): int
    {
        return $this->model::ADDITION_REQUEST_STATUS;
    }

    /**
     * Lấy ra trạng thái có request thanh toán
     *
     * @return     integer  The status purchase request.
     */
    public function getStatusPurchaseRequest(): int
    {
        return $this->model::PURCHASE_REQUEST;
    }

    /**
     * get goods via code
     *
     * @param  string $code
     *
     * @return App\Models\Good
     */
    private function getByCode(string $groupId, string $code)
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->where('code', $code)
                    ->withTrashed()
                    ->first();
    }

    /**
     * generate new invocie code
     * Code is unique in group
     *
     * @return string
     */
    public function generateCode(string $groupId): string
    {
        $group = $this->group->getById($groupId);
        $count = (int)$group->invoice_number ?? 0;
        do {
            $code = $this->getSymbolCode();
            $count = $count + 1;
            $code = $code . str_pad((string)$count, 8, '0', STR_PAD_LEFT);
        } while ($this->getByCode($groupId, $code) !== null);
        return $code;
    }

    /**
     * Lấy danh sách hóa đơn theo điều kiện
     *
     * @return App\Models\Invoice
     */
    public function getInvoiceByCondition(array $data)
    {
        $startDate = !empty($data['start']) ? new Carbon($data['start']) : null;
        $endDate = !empty($data['end']) ?
                    new Carbon(Carbon::createFromFormat('d-m-Y H:i:s', $data['end'] . '23:59:59')) : null;
        return $this->model
                    ->with('recipient:first_name,last_name')
                    ->where('group_id', $data['group_id'])
                    ->createdAt($startDate, $endDate)
                    ->status($data['status'] ?? null)
                    ->code($data['code'] ?? null)
                    ->orderBy('created_at', 'desc')
                    ->paginate(self::PER_PAGE);
    }

    /**
     * Phân trang kết quả truy vấn theo điều kiện với groupId
     *
     * @param array $args
     * @param integer $page
     * @param array $relations
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateByCondition(array $args, $page = 1, array $relations = [])
    {
        return $this->model
                    ->with($relations)
                    ->groupId($args['group_id'] ?? null)
                    ->createdAt($args['start_date'], $args['end_date'])
                    ->status($args['status'] ?? null)
                    ->code($args['code'] ?? null)
                    ->orderBy('code', 'DESC')
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy danh sách hóa đơn theo trạng thái và phân trang kết quả
     *
     * @param  string $groupId
     * @param  integer $status
     * @param  integer $perPage
     * @param  integer $page
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateByStatus(string $groupId, int $status, int $perPage = self::PER_PAGE, int $page = 1)
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->status($status)
                    ->with('recipient:first_name,last_name')
                    ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Tìm kiếm danh sách hóa đơn với điều kiện đầu vào
     *
     * @param      array $condition The condition
     * @param      integer $perPage The per page
     * @param      integer $page The page
     */
    public function paginateSearchByCondition(array $condition, int $perPage = self::PER_PAGE, int $page = 1)
    {
        return $this->model
                    ->where('group_id', $condition['group_id'])
                    ->code($condition['invoice_code'] ?? null)
                    ->createdAt($condition['start_date'], $condition['end_date'])
                    ->status($condition['status'] ?? null)
                    ->with('recipient:first_name,last_name')
                    ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Gets the invoices by status.
     *
     * @param array $args
     */
    public function getInvoicesByStatus(array $args)
    {
        return $this->model
                    ->where('group_id', $args['group_id'])
                    ->status($args['status'])
                    ->get();
    }

    /**
     * Gets all invoices.
     *
     * @param      string   $groupId  The group identifier
     * @param      integer  $amount   The amount
     */
    public function getListLatestInvoices(string $groupId, int $amount)
    {
        return $this->model
                    ->where([
                        ['group_id', $groupId],
                        ['status', '=', $this->getStatusPurchased()]
                    ])
                    ->with('recipient:first_name,last_name')
                    ->orderBy('created_at', 'DESC')
                    ->take($amount)
                    ->get();
    }

    /**
     * Lấy danh sách hóa đơn trong khoang thoi gian
     *
     * @param string $groupId;
     * @param object $period;
     *
     * @return App\Models\Invoice
     */
    public function getStatisticPeriodTime(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        $revenue = array();
        $goods = array();
        $invoices = self::groupInvoicesByPeriodTime($groupId, $startDate, $endDate);
        foreach ($invoices as $key => $invoice) {
            $revenue[] = array((string)$key, $invoice->sum('total'));
            foreach ($invoice as $inv) {
                $columns = array_column($goods, 'id');
                foreach ($inv->goods as $good) {
                    if (in_array($good['id'], $columns)) {
                        $index = array_search($good['id'], $columns);
                        $goods[$index]['count'] += $good['count'];
                        $goods[$index]['total'] += $good['total'];
                    } else {
                        $goods[] = $good;
                    }
                }
            };
        };
        usort($goods, function($a, $b) {
            return $b['count'] - $a['count'];
        });
        $bestCount = array_slice($goods, 0, 10);
        usort($goods, function($a, $b) {
            return $b['total'] - $a['total'];
        });
        $bestRevenue = array_slice($goods, 0, 10);
        return [$revenue, $bestCount, $bestRevenue];
    }

    /**
     * Lấy thông tin hóa đơn trang chủ hôm nay
     *
     * @param string $groupId;
     *
     * @return App\Models\Invoice
     */
    public function getStatisticToday(string $groupId)
    {
        $obj = new \stdClass();
        $purchasedInvoices = $this->model->where('group_id', $groupId)
            ->createdAt(new Carbon(date('y-m-d')), new Carbon(date('y-m-d 23:59:59')))
            ->status(self::getStatusPurchased())->get();
        $obj->purchasedTotal = $purchasedInvoices->count();
        $obj->purchasedMoney = $purchasedInvoices->sum('total');
        $obj->initedTotal = $this->model->where('group_id', $groupId)
            ->createdAt(new Carbon(date('y-m-d')), new Carbon(date('y-m-d 23:59:59')))
            ->status(self::getStatusInited())->get()->count();
        return $obj;
    }

    /**
     * get invoices by hour today
     *
     * @param string;
     *
     * @return array
     */
    public function getReportSale(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        $array = array();
        $invoices = self::groupInvoicesByPeriodTime($groupId, $startDate, $endDate);
        $invoices->each(function($invoice, $key) use (&$array) {
            $obj = new \stdClass();
            $obj->time = (string)$key;
            $obj->subtotal = $invoice->sum('subtotal');
            $obj->total = $invoice->sum('total');
            $obj->total_origin = 0;
            $invoice->each(function($inv) use (&$obj) {
                $obj->total_origin += collect($inv->goods)->reduce(function($total_origin, $good) {
                    return $total_origin + ($good['count'] * $good['price_origin']);
                }, 0);
            });
            $array[] = $obj;
        });
        return $array;
    }

    public function getReportGoods(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        $goods = array();
        $this->model
            ->where('group_id', $groupId)
            ->createdAt($startDate, $endDate)
            ->status(self::getStatusPurchased())
            ->chunk(1000, function($invoices) use (&$goods) {
                foreach ($invoices as $invoice) {
                    $columns = array_column($goods, 'id');
                    foreach ($invoice->goods as $good) {
                        if (in_array($good['id'], $columns)) {
                            $index = array_search($good['id'], $columns);
                            $goods[$index]['count'] += $good['count'];
                            $goods[$index]['total'] += $good['total'];
                            $goods[$index]['total_discount'] += $this->getDiscountGoods($good);
                            $goods[$index]['total_origin'] += $good['price_origin'] * $good['count'];
                        } else {
                            $good['total_discount'] = $this->getDiscountGoods($good);
                            $goods[] = $good;
                        }
                    }
                };
            });
        usort($goods, function($a, $b) {
            return $b['total'] - $a['total'];
        });
        return $goods;
    }

    private function getDiscountGoods($good)
    {
        $priceDiscount = 0;
        if (!empty($good['discount']) && !empty($good['discount_type']) && $good['discount_type'] == 2) {
            $priceDiscount = $good['price'] * $good['discount'] / 100;
        }
        return intval($priceDiscount * $good['count']);
    }

    private function groupInvoicesByPeriodTime($groupId, Carbon $startDate, Carbon $endDate)
    {
        $invoices = $this->getInvoicesByPeriodTime($groupId, $startDate, $endDate);
        $count = CarbonPeriod::create($startDate, $endDate)->count();
        if ($count == 1) {
            $groupInvoices = $invoices->groupBy(function($invoice) {
                return Carbon::parse($invoice->created_at)->format('H:00');
            });
        } elseif ($count < 32) {
            if ($startDate->format('m') == $endDate->format('m')) {
                $groupInvoices = $invoices->groupBy(function($invoice) {
                    return Carbon::parse($invoice->created_at)->format('d');
                });
            } else {
                $groupInvoices = $invoices->groupBy(function($invoice) {
                    return Carbon::parse($invoice->created_at)->format('d-m');
                });
            }
        } else {
            $groupInvoices = $invoices->groupBy(function($invoice) {
                return Carbon::parse($invoice->created_at)->format('m-Y');
            });
        }
        return $groupInvoices;
    }

    public function getInvoicesByPeriodTime(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->createdAt($startDate, $endDate)
                    ->status(self::getStatusPurchased())
                    ->get();
    }

    /**
     * Cập nhật lại số lượng hàng hóa trong hóa đơn
     *
     * @param string $invoiceId
     * @param array $goods
     * @param bool $restore
     */
    public function updateNumberGoodsInvoice(string $invoiceId, array $goods, bool $restore = true)
    {
        $invoice = $this->getById($invoiceId);
        $invoice->goods = $this->calculateCountGoods($invoice, $goods, $restore);
        list($good, $subtotal, $total) = $this->getTotalInInvoice($invoice);
        $invoice->update(['goods' => $good, 'subtotal' => $subtotal, 'total' => $total]);
    }

    private function calculateCountGoods($invoice, $goods, $restore)
    {
        $newGoods = array();
        if ($restore) {
            foreach ($invoice->goods as $good) {
                foreach ($goods as $go) {
                    if ($good['id'] == $go['id']) {
                        $good['count'] += $go['count'];
                        break;
                    }
                }
                $newGoods[] = $good;
            }
        } else {
            foreach ($invoice->goods as $good) {
                foreach ($goods as $go) {
                    if ($good['id'] == $go['id']) {
                        $good['count'] -= $go['count'];
                        break;
                    }
                }
                if ((int)$good['count'] > 0) {
                    $newGoods[] = $good;
                }
            }
        }
        return $newGoods;
    }

    // Tính toán lại total của invoice
    private function getTotalInInvoice($invoice)
    {
        $arr = array();
        $subtotal = 0;
        foreach ($invoice->goods as $good) {
            $price = $good['price'] * $good['count'];
            if (!empty($good['discount'])) {
                if ($good['discount_type'] == $this->getDiscountTypePercent()) {
                    $good['total'] = intval($price * (1 - $good['discount'] / 100));
                } else {
                    $good['total'] = $price - $good['discount'];
                }
            } else {
                $good['total'] = $price;
            }
            $good['total_origin'] = $good['price_origin'] * $good['count'];
            $subtotal += $good['total'];
            $arr[] = $good;
        }
        if (!empty($invoice->discount)) {
            if ($invoice->discount_type == $this->getDiscountTypeFixed()) {
                $total = $subtotal - $invoice->discount;
            } else {
                $total = intval($subtotal * (1 - $invoice->discount / 100));
            }
        } else {
            $total = $subtotal;
        }
        return [$arr, $subtotal, $total];
    }

    
    /**
     * Disable những goods item có status là booked khi hết hàng
     *
     * @param string $groupId
     * @param string $goodsId
     * @param boolean $status
     * @return void
     */
    public function asyncGoodsItemsStatus(string $groupId, string $goodsId, bool $status)
    {
        return $this->model->raw(function($query) use ($groupId, $goodsId, $status) {
            return $query->updateMany([
                'group_id' => $groupId,
                'status' => $this->getStatusInited(),
                'goods.id' => $goodsId
            ], [
                '$set' => [
                    "goods.$[i].orders.$[j].is_disable" => $status
                ]
            ], [
                'arrayFilters' => [
                    ['i.id' => $goodsId],
                    ['j.status' => $this->getGoodBookedStatus()]
                ]
            ]);
        });
    }

    /**
     * Lấy hoá đơn mang về 
     *
     * @param string $groupId
     * @return array
     */
    public function getInvoicesIsEmptyTable(string $groupId)
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->where('status', $this->getStatusInited())
                    ->where('table_ids', [])
                    ->get();
    }

    /**
     * Lấy ra danh sách các hóa đơn chưa thanh toán chứa một trong số những tableIds
     *
     * @param string $groupId
     * @param array  $tableIds
     */
    public function getInitedInvoicesByTableIds(string $groupId, array $tableIds)
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->where('status', $this->getStatusInited())
                    ->whereIn('table_ids', $tableIds)
                    ->get();
    }

    /**
     * Lấy ra danh sách hóa đơn có goods chứa $goodId
     *
     * @param string $groupId
     * @param string $goodId
     */
    public function getInitedInvoicesHasGoodId(string $groupId, string $goodsId)
    {
        return $this->model
                    ->where([
                        'group_id' => $groupId,
                        'status'   => $this->getStatusInited(),
                        'goods.id' => $goodsId
                    ])
                    ->get();
    }

    /**
     * Cập nhật goods_order của hóa đơn
     *
     * @param      string $invoiceId The invoice identifier
     * @param      string $goodId The good identifier
     * @param      array $goodsOrder The goods order
     */
    public function updateGoodsOrderInvoice(string $invoiceId, string $goodId, array $goodsOrder)
    {
        return $this->model
                    ->where([
                        ['_id', $invoiceId],
                        ['goods.id', $goodId]
                    ])
                    ->update([
                        'goods.$.goods_order' => $goodsOrder
                    ]);
    }

    /**
     * Thêm goods item
     * Những goods item mới chưa có trong invoice
     *
     * @param string $invoiceId
     * @param array  $data
     * @return bool
     */
    public function addGoodsItems(string $invoiceId, array $data): bool
    {
        return $this->model
                    ->where('_id', $invoiceId)
                    ->push('goods', $data, true);
    }

    /**
     * Get list of invoice by user's ID
     *
     * @param  string      $userId
     * @param  int|integer $page
     * @param  array       $relations
     * @param  array       $selections
     * @return App\Models\Invoices
     */
    public function paginateByUserId(string $userId, array $args, array $relations = [])
    {
        $startDate = !empty($args['start_date']) ? Carbon::parse($args['start_date'])->startOfDay() : '';
        $endDate = !empty($args['end_date']) ? Carbon::parse($args['end_date'])->endOfDay() : '';

        return $this->model
                    ->with($relations)
                    ->where('user_ids', 'all', [$userId])
                    ->createdAt($startDate, $endDate)
                    ->orderBy('created_at', 'DESC')
                    ->paginate(self::PER_PAGE, ['*'], 'page', $args['page']);
    }

    /**
     * Lấy số lượng hoá đơn trong ngày của 1 nhóm, theo trạng thái hoá đơn
     *
     * @param array $args
     * @return void
     */
    public function getCountByStatus(string $groupId, Carbon $startDate, Carbon $endDate, int $status)
    {
        return $this->model
                    ->groupId($groupId)
                    ->status($status)
                    ->createdAt($startDate, $endDate)
                    ->count();
    }

    /**
     * Lấy tổng doanh thu của group theo khoảng ngày
     * Đồng thời lấy số lượng hoá đơn đã thanh toán
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getTotalRevenueByDateRange(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return $this->model->raw(function($collection) use ($groupId, $startDate, $endDate) {
            return $collection->aggregate([
                [
                    '$match' => [
                        'group_id' => $groupId,
                        'status' => $this->model::STATUS_PURCHASED,
                        'created_at' => [
                            '$gte' => new UTCDateTime($startDate),
                            '$lte' => new UTCDateTime($endDate)
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => null,
                        'revenue' => ['$sum' => '$total'],
                        'purchased_invoice' => ['$sum' => 1]
                    ]
                ]
            ]);
        });
    }

    /**
     * Báo cáo bán hàng (doanh số) theo khoảng thời gian
     *
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     * @param array $groupIdOperator
     * @param array $sort
     * @return array
     */
    public function getRevenueReport(
        string $groupId, Carbon $startDate, Carbon $endDate, array $groupIdOperator, array $sort
    )
    {
        return $this->model->raw(function($collection)
            use ($groupId, $startDate, $endDate, $groupIdOperator, $sort) {

            return $collection->aggregate([
                [
                    '$match' => [
                        'group_id' => $groupId,
                        'status' => $this->model::STATUS_PURCHASED,
                        'created_at' => [
                            '$gte' => new UTCDateTime($startDate),
                            '$lte' => new UTCDateTime($endDate)
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => $groupIdOperator,
                        'revenue' => [
                            '$sum' => '$total'
                        ]
                    ]
                ],
                $sort
            ]);
        });
    }

    /**
     * Lấy top hàng hoá bán chạy theo doanh thu
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return object
     */
    public function getTopSellingGoods(string $groupId, Carbon $startDate, Carbon $endDate)
    {
        return $this->model->raw(function ($collection) use ($groupId, $startDate, $endDate) {
            return $collection->aggregate([
                [
                    '$unwind' => '$goods'
                ],
                [
                    '$match' => [
                        'group_id' => $groupId,
                        'status' => $this->model::STATUS_PURCHASED,
                        'created_at' => [
                            '$gte' => new UTCDateTime($startDate),
                            '$lte' => new UTCDateTime($endDate)
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$goods.id',
                        'name' => [
                            '$first' => '$goods.name'
                        ],
                        'image' => [
                            '$first' => '$goods.image'
                        ],
                        'unit' => [
                            '$first' => '$goods.unit'
                        ],
                        'total' => [
                            '$sum' => '$goods.total'
                        ],
                        'count' => [
                            '$sum' => '$goods.count'
                        ]
                    ]
                ],
                [
                    '$sort' => ['count' => -1]
                ],
                [
                    '$limit' => self::TOP_SELLING_GOODS
                ]
            ]);
        });
    }

    /**
     * Lấy 
     *
     * @param string $groupId
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param array $groupIdOperator
     * @param array $sort
     * @return void
     */
    public function getProfitReport(
        string $groupId, Carbon $startDate, Carbon $endDate, array $groupIdOperator, array $sort
    )
    {
        return $this->model->raw(function($collection)
            use ($groupId, $startDate, $endDate, $groupIdOperator, $sort) {

            return $collection->aggregate([
                [
                    '$match' => [
                        'group_id' => $groupId,
                        'status' => $this->model::STATUS_PURCHASED,
                        'created_at' => [
                            '$gte' => new UTCDateTime($startDate),
                            '$lte' => new UTCDateTime($endDate)
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => $groupIdOperator,
                        'revenue' => [
                            '$sum' => '$total'
                        ],
                        'total_origin' => [
                            '$sum' => '$total_origin'
                        ]
                    ]
                ],
                $sort
            ]);
        });
    }

    /**
     * Tổng hợp dữ liệu theo toán tử điều kiện truyền vào
     *
     * @param array $aggregation
     * @return Object
     */
    public function aggregate(array $aggregation)
    {
        return $this->model->raw(function($collection) use ($aggregation) {
            return $collection->aggregate($aggregation);
        });
    }

    /**
     * Xoá 1 item trong mảng goods
     *
     * @param string  $invoiceId
     * @param string  $goodsId
     * @return void
     */
    public function removeGoodsItem(string $invoiceId, string $goodsId)
    {
        $this->model
            ->raw()
            ->findOneAndUpdate([
                '_id' => new ObjectID($invoiceId),
            ], [
                '$pull' => [
                    "goods" => [
                        "id" => $goodsId
                    ]
                ]
            ]);
    }
    
    /**
     * Xoá 1 item trong goods.orders
     *
     * @param string $invoiceId
     * @param string $goodsId
     * @param string $createdAt  Thời gian khởi tạo của orders item (goods.$.orders.$.created_at)
     * @param int    $count      Số lượng của orders item (goods.$.orders.$.count)
     * @return void
     */
    public function removeGoodsOrdersItem(string $invoiceId, string $goodsId,
                                          string $createdAt, int $count)
    {
        $this->model
            ->raw()
            ->findOneAndUpdate([
                '_id' => new ObjectID($invoiceId),
                'goods.id' => $goodsId
            ], [
                '$inc' => [
                    "goods.$.count" => -$count
                ],
                '$pull' => [
                    "goods.$.orders" => [
                        'created_at' => $createdAt,
                        'status'     => $this->getGoodBookedStatus()
                    ]
                ]
            ]);
    }

    /**
     * Cập nhật trạng thái món theo ID hoá đơn (màn hình nhà bếp)
     * === KHÔNG DÙNG NỮA ===
     *
     * @param string $invoiceId
     * @return void
     */
    public function updateProcessedInvoice(string $invoiceId)
    {
        $this->model
            ->raw()
            ->findOneAndUpdate([
                '_id' => new ObjectID($invoiceId)
            ], [
                '$set' => [
                    "goods.$[].orders.$[index].status" => $this->getGoodProcessedStatus()
                ]
            ], [
                'arrayFilters' => [
                    ['index.status' => $this->getGoodBookedStatus()]
                ]
            ]);
    }

    /**
     * Cập nhật trạng thái món theo goodsId, created_at
     * === KHÔNG DÙNG NỮA ===
     * 
     * @param string $invoiceId
     * @param string $goodsId
     * @param string $createdAt
     * @param string $targetStatus
     * @return void
     */
    public function updateProcessedOrderItem(string $invoiceId,
                                             string $goodsId,
                                             string $createdAt)
    {
        $this->model
            ->raw()
            ->updateOne([
                '_id' => new ObjectID($invoiceId)
            ], [
                '$set' => [
                    "goods.$[i].orders.$[j].status" => $this->getGoodProcessedStatus()
                ]
            ], [
                'arrayFilters' => [
                    ['i.id' => $goodsId],
                    ['j.created_at' => $createdAt]
                ]
            ]);
    }

    /**
     * Cập nhật trạng thái tất cả các hoá đơn có chưa goodsId
     *
     * @param string $groupId
     * @param string $goodsId
     * @return void
     */
    public function updateProcessedByGoodsId(string $groupId, string $goodsId)
    {
        $this->model
            ->raw()
            ->updateMany([
                'group_id' => $groupId,
                'status' => $this->getStatusInited(),
            ], [
                '$set' => [
                    "goods.$[i].orders.$[j].status" => $this->getGoodProcessedStatus()
                ]
            ], [
                'arrayFilters' => [
                    ['i.id' => $goodsId],
                    ['j.status' => $this->getGoodBookedStatus()]
                ]
            ]);
    }

    /**
     * Lấy danh sách hóa đơn chưa thanh toán có ít nhất một món đúng trạng thái
     * === Deprecated ===
     * 
     * @param string $groupId
     * @param string $goodsId,
     * @param string $status
     *
     * @return mixed
     */
    public function getInvoiceByGoodsStatus(string $groupId, string $goodsId, string $status)
    {
        return $this->model
            ->where('group_id', $groupId)
            ->status($this->getStatusInited())
            ->whereRaw([
                'goods' => [
                    '$elemMatch' => [
                        'id' => $goodsId,
                        'orders' => [
                            '$elemMatch' => [
                                'status' => $status
                            ]
                        ]
                    ]
                ]
            ])
            ->get();
    }
}