<?php

namespace App\Services\Restaurant;

use App\Services\BaseService;
use App\Contracts\Restaurant\InvoiceFormContract;
use App\Models\InvoiceForm;

class InvoiceFormService extends BaseService implements InvoiceFormContract
{
    public function __construct(InvoiceForm $model)
    {
        parent::__construct($model);
    }

    /**
     * @return App\Models\InvoiceForm::TYPES
     */
    public function getTypes()
    {
        return $this->model::TYPES;
    }

    /**
     * @return App\Models\InvoiceForm::SIZES
     */
    public function getSizes()
    {
        return $this->model::SIZES;
    }

    /**
     * Get all invoice form.
     *
     * @param array $data The data
     */
    public function getAllInvoiceForm(array $data)
    {
        return $this->model->where('group_id', $data['group_id'])
            ->orderBy('type', 'ASC')
            ->get()
            ->groupBy('type');
    }

    /**
     * Get invoice form by type.
     *
     * @param array
     */
    public function getByType(string $groupId, int $type)
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->where('type', $type)
                    ->get();
    }
}