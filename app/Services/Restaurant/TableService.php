<?php

namespace App\Services\Restaurant;

use App\Contracts\Restaurant\TableContract;
use App\Models\Table;
use App\Services\BaseService;

class TableService extends BaseService implements TableContract
{
    const PER_PAGE = 20;

    public function __construct(Table $model)
    {
        parent::__construct($model);
    }

    /**
     * Gets the inactive type.
     *
     * @return     integer  The inactive type.
     */
    public function getInactiveType(): int
    {
        return $this->model::STATUS_INACTIVE;
    }

    /**
     * get type avaliable table
     *
     * @return int
     */
    public function getAvailableType(): int
    {
        return $this->model::STATUS_AVAILABLE;
    }

    /**
     * Get the acting type.
     *
     * @return int
     */
    public function getActingType(): int
    {
        return $this->model::STATUS_ACTING;
    }

    /**
     * Lấy danh sách trạng thái bàn
     *
     * @return array
     */
    public function getStatusList(): array
    {
        return $this->model::STATUS;
    }

    /**
     * Lấy danh sách các bàn theo place ID
     *
     * @param  string $placeId
     * @return App\Models\Table
     */
    public function getByPlaceId(string $placeId)
    {
        return $this->model->where('place_id', $placeId)->get();
    }

    /**
     * Lấy danh sách bàn theo danh sách id của place
     *
     * @param array  $placeIds
     */
    public function getByPlaceIds(array $placeIds)
    {
        return $this->model->whereIn('place_id', $placeIds)->get();
    }

    /**
     * Search Tables in group
     * @param  string $groupId
     * @return array
     */
    public function getByCondition(array $request, $relations = [], $page = 1)
    {
        return $this->model
            ->with($relations)
            ->where('group_id', $request['group_id'])
            ->fullText($request['name'] ?? '')
            ->place($request['place_id'] ?? null)
            ->status($request['status'] ?? null)
            ->orderBy('updated_at', 'DESC')
            ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy 1 bàn theo tên và group ID (xét cả trong trash)
     *
     * @param  string $groupId
     * @param  string $name
     * @return App\Models\Table
     */
    private function getByName(string $groupId, string $placeId, string $name)
    {
        return $this->model
            ->onlyTrashed()
            ->where('group_id', $groupId)
            ->where('place_id', $placeId)
            ->where('name', $name)
            ->first();
    }

    /**
     * Create new or restore table if table was deleted
     *
     * @param  array $data
     * @return object
     */
    public function createTable(array $data)
    {
        $table = $this->getByName($data['group_id'], $data['place_id'], $data['name']);
        if ($table) {
            $table->restore();
            return $table->refresh();
        }
        return $this->create($data);
    }

    /**
     * Xoá danh sách bàn khi xoá place chứa các bàn đó
     *
     * @param  string $placeId
     * @return void
     */
    public function deleteByPlaceId(string $placeId)
    {
        $this->model->where('place_id', $placeId)->delete();
    }

    /**
     * Giải phóng bàn
     *
     * @param  string $invoiceId
     * @return void
     */
    public function available(string $invoiceId)
    {
        $this->model->where('invoice_id', $invoiceId)
            ->update([
                'status' => $this->getAvailableType(),
                'invoice_id' => ''
            ]);
    }
}
