<?php

namespace App\Services\Restaurant;

use App\Contracts\Restaurant\PlaceContract;
use App\Contracts\Restaurant\TableContract;
use App\Models\Place;
use App\Services\BaseService;

class PlaceService extends BaseService implements PlaceContract
{
    private $table;

    public function __construct(Place $place, TableContract $table)
    {
        parent::__construct($place);
        $this->table = $table;
    }

    /**
     * Lấy 1 khu vực (xét cả trong trash) theo name
     *
     * @param  string $groupId
     * @param  string $name
     * @return App\Models\Place
     */
    private function getTrashedByName(string $groupId, string $name)
    {
        return $this->model
            ->onlyTrashed()
            ->where('group_id', $groupId)
            ->where('name', $name)
            ->first();
    }

    /**
     * @param  array  $data
     * @return App\Models\Place | boolean
     */
    public function createPlace(array $data)
    {
        $place = $this->getTrashedByName($data['group_id'], $data['name']);

        if ($place) {
            $place->restore();
            return $place;
        }
        return $this->create($data);
    }

    /**
     * Get Places in group by groupdId
     * @param  string $groupId
     * @param  string $name
     * @return array
     */
    public function getByName(string $groupId, string $name)
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->where('name', $name)
                    ->first();
    }

    /**
     * Lấy danh sách khu vực trong màn hình bán hàng
     *
     * @param string $groupId
     * @param array  $placeIds
     * @param array  $relations
     * @return list of App\Models\Place
     */
    public function getSalePlaces(string $groupId, array $placeIds = [], array $relations = [])
    {
        return $this->model
                    ->with($relations)
                    ->where('group_id', $groupId)
                    ->ids($placeIds)
                    ->get();
    }


    /**
     * Gets the places with tables.
     *
     * @param      string  $groupId   The group identifier
     * @param      array   $placeIds  The place identifiers
     *
     * @return     <type>  The places with tables.
     */
    public function getPlacesWithCondition(string $groupId, array $placeIds)
    {
        $query = $this->model->where('group_id', $groupId);
        if (!empty($placeIds)) {
            $query = $query->whereIn('_id', $placeIds);
        }
        return $query->with(['tables' => function($query) {
                        $query->statusNotInactiveType()
                              ->with('place:_id,name');
                     }])->get();
    }
}