<?php

namespace App\Services\Restaurant;


use App\Contracts\Restaurant\GroupMenuContract;
use App\Models\GroupMenu;
use App\Services\BaseService;

class GroupMenuService extends BaseService implements GroupMenuContract
{
    public function __construct(GroupMenu $menu)
    {
        parent::__construct($menu);
    }

    /**
     * get level 0 of group menu

     * @return int
     */
    public function getLevel0()
    {
        return $this->model::LEVER_0;
    }

    /**
     * get level 1 of group menu
     *
     * @return int
     */
    public function getLevel1()
    {
        return $this->model::LEVER_1;
    }

    /**
     * get level 2 of group menu
     *
     * @return int
     */
    public function getLevel2()
    {
        return $this->model::LEVER_2;
    }

    /**
     * get all group menus in group
     *
     * @param  string $groupId
     * @return App\Models\GroupMenu
     */
    public function getByGroupId(string $groupId, array $relation = [])
    {
        return $this->model
                    ->with('childs.childs')
                    ->where([
                        ['group_id', $groupId],
                        ['level', $this->getLevel0()]
                    ])
                    ->get();
    }

    /**
     * get group menus has level = 0 and it's child in group
     *
     * @param  string $groupId
     * @return App\Models\GroupMenu
     */
    public function getParentGroupMenusByGroupId(string $groupId)
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->where('level', $this->model::LEVER_0)
                    ->with('childs')
                    ->get();
    }

    /**
     * remove goodId from good_ids
     *
     * @param  GroupMenu $groupMenu
     * @param  string $goodId
     * @return App\Models\GroupMenu
     */
    public function pullGoodId($groupMenu, string $goodId)
    {
        $groupMenu->pull('goods_ids', $goodId);
    }

    public function store($request, $parentRestaurantId)
    {
        if ($request['parent_id']) {
            $groupMenu = GroupMenuModel::create([
                'name' => $request['name'],
                'description' => $request['description'],
                'parent_id' => $request['parent_id'],
                'level' => $request['parent_level'] + 1,
            ]);
        } else {
            $groupMenu = GroupMenuModel::create([
                'name' => $request['name'],
                'description' => $request['description'],
                'parent_id' => $request['parent_id'],
                'level' => 0,
                'restaurant_id' => $parentRestaurantId
            ]);
        }

        return $groupMenu;
    }

    /**
     * delete groupMenu and it's childs
     *
     * @param  string $groupMenuId
     * @return bool
     */
    public function deleteGroupMenuWithChilds(string $groupMenuId)
    {
        $menu = $this->getById($groupMenuId);
        $ids = [$groupMenuId];
        if (empty($menu->goods_ids)) {
            switch ($menu->level) {
                case 1:
                    foreach ($menu->childs as $menuChild) {
                        if (!empty($menuChild->goods_ids)) {
                            return false;
                        }
                        $ids[] = $menuChild->id;
                    }
                    break;
                case 0:
                    foreach ($menu->childs as $menuChild) {
                        if (!empty($menuChild->goods_ids)) {
                            return false;
                        }
                        $ids[] = $menuChild->id;

                        foreach ($menuChild->childs as $child) {
                            if (!empty($menuChild->goods_ids)) {
                                return false;
                            }
                            $ids[] = $child->id;
                        }
                    }
                    break;
            }
            $this->destroy($ids);
            return true;
        }
        return false;
    }

    /**
     * get group menu bu name
     *
     * @param  string $goodId
     * @return App\Models\GroupMenu
     */
    public function getByName(string $groupId, string $name)
    {
        return $this->model->where('group_id', $groupId)->where('name', $name)->first();
    }

    /**
     * @param  App\Models\GroupMenu $menu
     * @return boolean
     */
    public function hasChilds($menu): bool
    {
        foreach ($menu->childs as $item) {
            if (!empty($item->goods_ids)) {
                return false;
            }
        }
        return true;
    }

}