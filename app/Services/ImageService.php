<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use App\Contracts\ImageContract;

class ImageService implements ImageContract
{
    /**
     * @var Intervention\Image\ImageManager
     */
    public $image;

    /**
     * Path from filesystem, URL of an image, binary data, base64 data, PHP source type GD,
     * Imagick instance, Intervention\Image\Image instance ....
     */
    public $path;
    public $destPath;

    public $thumbRate;
    public $fitPosition;
    public $thumbWidth;
    public $thumbHeight;
    public $filename;
    public $xCoordinate;
    public $yCoordinate;

    public function __construct()
    {
        $this->image = new ImageManager(['driver' => 'gd']);
        $this->thumbRate = 0.75;
        $this->fitPosition = 'center';
    }

    /**
     * Set image source
     * @param $imagePath
     */
    public function setImage($imagePath)
    {
        $this->path = $imagePath;
        return $this;
    }

    /**
     * Get image's path source
     * @return string
     */
    public function getImage()
    {
        return $this->path;
    }

    /**
     * Set image's rate
     * @param float $rate
     */
    public function setRate(float $rate)
    {
        $this->thumbRate = $rate;
    }

    /**
     * Get image's rate
     * @return float
     */
    public function getRate()
    {
        return $this->thumbRate;
    }

    /**
     * Set image sizes
     * @param int $width
     * @param int|null $height
     */
    public function setSize(int $width, $height = null)
    {
        $this->thumbWidth = $width;
        $this->thumbHeight = $height;
        if (is_null($height)) {
            $this->thumbHeight = $this->thumbWidth * $this->thumbRate;
        }
        return $this;

    }

    /**
     * Get image's sizes
     * @return array
     */
    public function getSize(): array
    {
        return [$this->thumbWidth, $this->thumbHeight];
    }

    /**
     * Set folder dest path to save
     * @param string $path
     */
    public function setDestPath(string $destPath)
    {
        $this->destPath = $destPath;
        return $this;
    }

    /**
     * Get folder dest path to save
     * @return string
     */
    public function getDestPath()
    {
        return $this->destPath;
    }

    /**
     * Set image coordinate
     * @param int $xCoord
     * @param int $yCoord
     */
    public function setCoordinate(int $xCoord, int $yCoord)
    {
        $this->xCoordinate = $xCoord;
        $this->yCoordinate = $xCoord;
        return $this;
    }

    /**
     * Get image coordinate
     * @return array
     */
    public function getCoordinate(): array
    {
        return [$this->xCoordinate, $this->xCoordinate];
    }

    /**
     * Set fit position image
     * @param int $position
     */
    public function setFitPosition(int $position)
    {
        $this->fitPosition = $position;
        return $this;
    }

    /**
     * Get image fit position
     * @return string
     */
    public function getFitPosition()
    {
        return $this->fitPosition;
    }

    /**
     * Set filename new image
     * @param string $filename
     */
    public function setFilename(string $filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return get filename new image
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param  integer $quality
     * @return Intervention\Image\Image
     */
    public function resize(int $quality = 100)
    {
        return $this->image
            ->make($this->path)
            ->resize($this->thumbWidth, $this->thumbHeight)
            ->encode('jpg', $quality);
    }

    /**
     * @param  integer $quality
     * @return Intervention\Image\Image
     */
    public function crop(int $quality = 100)
    {
        return $this->image
            ->make($this->path)
            ->crop($this->thumbWidth, $this->thumbHeight, $this->xCoordinate, $this->yCoordinate)
            ->encode('jpg', $quality);
    }

    /**
     * @param  integer $quality
     * @return Intervention\Image\Image
     */
    public function fit(int $quality = 100)
    {
        return $this->image
            ->make($this->path)
            ->fit($this->thumbWidth, $this->thumbHeight, null, $this->fitPosition)
            ->encode('jpg', $quality);
    }

    /**
     * @param  string $mode
     * @param  integer $quality
     * @return string
     */
    public function save(string $mode = 'fit', int $quality = 100)
    {
        $filename = pathinfo($this->path, PATHINFO_BASENAME);
        if ($this->filename) {
            $filename = $this->filename;
        }
        $destPath = sprintf('%s/%s', trim($this->destPath, '/'), $filename);
        $thumbImage = $this->image->make($this->path);
        switch ($mode) {
            case 'resize':
                $thumbImage->resize($this->thumbWidth, $this->thumbHeight);
                break;
            case 'crop':
                $thumbImage->crop($this->thumbWidth, $this->thumbHeight, $this->xCoordinate, $this->yCoordinate);
                break;
            default:
                $thumbImage->fit($this->thumbWidth, $this->thumbHeight, null, $this->fitPosition);
                break;
        }
        try {
            $thumbImage->save($destPath, $quality);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return false;
        }
        return $destPath;
    }

    /**
     * Save image base on Storage
     * @param  integer $quality
     * @param  string $mode
     * @return image instance
     */
    public function saveInDisk(string $mode = 'resize', int $quality = 100)
    {
        $thumbImage = $this->image->make($this->path);
        switch ($mode) {
            case 'resize':
                $thumbImage->resize($this->thumbWidth, $this->thumbHeight);
                break;
            case 'crop':
                $thumbImage->crop($this->thumbWidth, $this->thumbHeight, $this->xCoordinate, $this->yCoordinate);
                break;
            default:
                $thumbImage->fit($this->thumbWidth, $this->thumbHeight, null, $this->fitPosition);
                break;
        }
        try {
            Storage::disk()->put($this->destPath, $thumbImage->encode('jpg', $quality), 'public');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return false;
        }
        return $this->destPath;
    }

    public function delete()
    {
        if (Storage::exists($this->path)) {
            Storage::delete($this->path);
        }
    }
}
