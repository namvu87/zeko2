<?php
namespace App\Services;

use App\Contracts\AreaContract;
use App\Services\BaseService;
use App\Models\Area;

class AreaService extends BaseService implements AreaContract
{
    const LIMIT = 50;

    public function __construct(Area $model)
    {
        parent::__construct($model);
    }

    /**
     * get {LIMIT} areas via key word input
     * @param  string $keyword
     * @return App\Models\Area
     */
    public function getAreaByKeyword(string $keyword)
    {
        return $this->model->where('name', 'like', "%{$keyword}%")
                           ->limit(self::LIMIT)
                           ->get();
    }
}