<?php
namespace App\Services;

use App\Services\BaseService;
use App\Contracts\CommuneContract;
use App\Models\Commune;

class CommuneService extends BaseService implements CommuneContract
{
    const LIMIT = 50;

    public function __construct(Commune $model)
    {
        parent::__construct($model);
    }

    /**
     * get Commune via relationship area_id
     * @param  string $areaId
     * @return App\Models\Commune
     */
    public function getAllByAreaId(string $areaId)
    {
        return $this->model->where('area_id', $areaId)->get();
    }

    /**
     * get {LIMIT} areas via key word input
     * @param  string $keyword
     * @return App\Models\Area
     */
    public function getByKeyword(string $keyword)
    {
        return $this->model->where('name', 'like', "%{$keyword}%")
                           ->limit(self::LIMIT)
                           ->get();
    }
}