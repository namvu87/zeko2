<?php
namespace App\Services;

use App\Services\BaseService;
use App\Contracts\RoleContract;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;

class RoleService extends BaseService implements RoleContract
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    /**
     * Gán 1 permission tới 1 role
     *
     * @param  Role       $role
     * @param  Permission $permission
     * @return void
     */
    public function assign(Role $role, Permission $permission)
    {
        $role->givePermissionTo($permission);
    }

    /**
     * Thu hồi 1 quyền của 1 vai trò
     *
     * @param  Role       $role
     * @param  Permission $permission
     * @return void
     */
    public function revoke(Role $role, Permission $permission)
    {
        $role->revokePermissionTo($permission);
    }

    /**
     * Xác định vai trò của 1 user trong nhóm chỉ định
     *
     * @param string $userId
     * @param string $groupId
     * @return Maklad\Permission\Models\Role
     */
    public function getRoleUserByGroupId(string $userId, string $groupId): ?Role
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->where('user_ids', 'all', [$userId])
                    ->first();
    }

    /**
     * Lấy vai trò bởi tên vai trò
     *
     * @param array $roleName
     *
     * @return mixed
     */
    public function getByRoleNames(array $roleNames)
    {
        return $this->model->whereIn('name', $roleNames)->get();
    }
}
