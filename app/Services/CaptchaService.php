<?php
namespace App\Services;
use Illuminate\Config\Repository;
use Illuminate\Hashing\BcryptHasher as Hasher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;
use Illuminate\Session\Store as Session;
use App\Contracts\CaptchaContract;
use Mews\Captcha\Captcha;

class CaptchaService extends Captcha implements CaptchaContract
{
    const EXPIRES = 60; // seconds
    private $expires;

    public function __construct(
        Filesystem $files,
        Repository $config,
        ImageManager $imageManager,
        Session $session,
        Hasher $hasher,
        Str $str
    )
    {
        parent::__construct($files, $config, $imageManager, $session, $hasher, $str);
        $this->expires = time() + self::EXPIRES;
    }

    /**
     * [Override]
     * Generate captcha text
     * For API with vuejs
     *
     * @return string
     */
    public function generate()
    {
        $characters = str_split($this->characters);

        $bag = '';
        for($i = 0; $i < $this->length; $i++)
        {
            $bag .= $characters[rand(0, count($characters) - 1)];
        }

        $bag = $this->sensitive ? $bag : $this->str->lower($bag);

        $hash = $this->hasher->make($bag . $this->expires);

        return [
            'value'     => $bag,
            'sensitive' => $this->sensitive,
            'key'       => $hash
        ];
    }

    /**
     * Initialize captcha datas
     *
     * @return array
     */
    public function init()
    {
        $captcha = $this->create('default', true);
        $captcha['expires'] = $this->expires;
        return $captcha;
    }

    /**
     * Check captcha valid
     *
     * @param  string $value
     * @return bool
     */
    public function checkApi($value, $key, $expires)
    {
        if ($expires + self::EXPIRES < time()) {
            return false;
        }
        return $this->hasher->check($value . $expires, $key);
    }
}