<?php
/**
 * Created by PhpStorm.
 * User: zeko-1
 * Date: 26/02/2019
 * Time: 16:54
 */

namespace App\Services;


use App\Contracts\DateTimeContract;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Log;

class DateTimeService implements DateTimeContract
{
    /**
     * get period time by type option
     * @return \DateTime
     */
    public function getPeriodMonth(Carbon $startDate, Carbon $endDate): array
    {
        $date = (new Carbon($startDate . '+1 month'))->modify('first day of this month');
        $array = array([$startDate, $date]);
        do {
            if ($date->format('Y-m') == $endDate->format('Y-m')) {
                array_push($array, [$date, new Carbon($endDate . '+1 day')]);
            } else {
                array_push($array, [$date, new Carbon($date . '+1 month')]);
            }
            $date = new Carbon($date . '+1 month');
        } while ($date <= $endDate);
        return $array;
    }

    public function getPeriodDateByOption(string $option): array
    {
        $startDate = new Carbon(date('Y-m-d 00:00:00')); //today
        $endDate = new Carbon(date('Y-m-d 00:00:00')); //today
        switch ($option) {
            case 'yesterday': // yesterday
                $startDate = new Carbon($startDate . '-1 day');
                $endDate = new Carbon($endDate . '-1 day');
                break;
            case 'this_week': // this week
                $startDate = new Carbon('monday this week');
                break;
            case 'last_week': // this week
                $startDate = new Carbon('monday last week');
                $endDate = new Carbon('sunday last week');
                break;
            case 'last_seven_days': // 7 days ago
                $startDate = new Carbon($startDate . '-6 day');
                break;
            case 'this_month': // this month
                $startDate = new Carbon('first day of this month');
                break;
            case 'last_month': // last month
                $startDate = new Carbon('first day of last month');
                $endDate = new Carbon('last day of last month');
                break;
            case 'last_thirty_days': // last month
                $startDate = new Carbon($startDate . '-29 day');
                break;
            case 'this_quarter': // this quarter month
                $startDate = self::getFirstDayOfQuarterByDate($startDate);
                break;
            case 'last_quarter': // last quarter
                $startDate = self::getFirstDayOfQuarterByDate(new Carbon($startDate . '-3 month'));
                $endDate = self::getLastDayOfQuarterByDate(new Carbon($endDate . '-3 month'));
                break;
            case 'this_year': // this year
                $startDate = new Carbon('first day of january' . $startDate->format('Y'));
                break;
            case 'last_year': // last year
                $startDate = new Carbon('first day of january' . ($startDate->format('Y') - 1));
                $endDate = new Carbon('last day of december' . ($endDate->format('Y') - 1));
                break;
            default:
                break;
        }
        return [$startDate, $endDate];
    }

    private function getFirstDayOfQuarterByDate(Carbon $date): Carbon
    {
        $month = $date->format('m');
        $year = $date->format('Y');
        if ($month <= 3) {
            $date = new Carbon('first day of january ' . $year);
        } elseif ($month > 3 && $month < 7) {
            $date = new Carbon('first day of april ' . $year);
        } elseif ($month > 6 && $month < 10) {
            $date = new Carbon('first day of july ' . $year);
        } elseif ($month > 9) {
            $date = new Carbon('first day of october ' . $year);
        }
        return $date;
    }

    private function getLastDayOfQuarterByDate(Carbon $date): Carbon
    {
        $month = $date->format('m');
        $year = $date->format('Y');
        if ($month <= 3) {
            $date = new Carbon('last day of march ' . $year);
        } elseif ($month > 3 && $month < 7) {
            $date = new Carbon('last day of june ' . $year);
        } elseif ($month > 6 && $month < 10) {
            $date = new Carbon('last day of september ' . $year);
        } elseif ($month > 9) {
            $date = new Carbon('last day of december ' . $year);
        }
        return $date;
    }

}