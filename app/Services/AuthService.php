<?php
namespace App\Services;

use App\Contracts\AuthContract;
use Illuminate\Http\Request;
use App\Contracts\UserContract;
use App\Contracts\Users\Firebase;

class AuthService implements AuthContract
{
    const GRANT_TYPE = 'password';

    private $user;
    private $firebase;

    public function __construct(UserContract $user, Firebase $firebase)
    {
        $this->user = $user;
        $this->firebase = $firebase;
    }

    /**
     * Khởi tạo đối tượng người dùng
     *
     * @param  array  $data
     * @return App\Models\User | null
     */
    public function regist(array $data)
    {
        return $this->user->create($data);
    }

    /**
     * Gửi dữ liệu xác thực tới máy chủ passport và trả về kết quả
     *
     * @param array $data
     * @return object
     */
    public function login(array $data)
    {
        $request = $this->generateAuthRequest($data);

        return app()->handle($request);
    }

    /**
     * @param  array  $args
     * @return void
     */
    public function logout(array $args)
    {
        $user = $this->user->getUser();
        $accessToken = $user->token();
        $accessToken->revoke();

        if (!empty($args['firebase_token'])) {
            $this->firebase->pull($user, $args['firebase_token']);
        }
    }

    /**
     * Lấy endpoit xác thực passport
     *
     * @return  string
     */
    private function getAuthLink()
    {
        return config('app.url') . '/oauth/token';
    }

    /**
     * Tạo request xác thực passport
     *
     * @param   array  $data
     *
     * @return  Illiminate\Http\Request
     */
    private function generateAuthRequest(array $data)
    {
        $payload = $this->generatePayloadRequest($data);

        return Request::create($this->getAuthLink(), 'POST', $payload);
    }

    /**
     * Tạo mảng xác thực passport
     *
     * @param   array  $data
     *
     * @return  array
     */
    private function generatePayloadRequest(array $data)
    {
        return [
            'grant_type' => self::GRANT_TYPE,
            'client_id' => config('passport.password_grant.client_id'),
            'client_secret' => config('passport.password_grant.secret'),
            'username' => $data['username'],
            'password' => $data['password']
        ];
    }
}
