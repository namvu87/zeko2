<?php

namespace App\Services;

use App\Contracts\PermissionContract;
use App\Services\BaseService;
use Maklad\Permission\Models\Permission;

class PermissionService extends BaseService implements PermissionContract
{
    public function __construct(Permission $model)
    {
        parent::__construct($model);
    }

    /**
     * @param  string $roleId
     * @return Maklad\Permission\Models\Permission
     */
    public function getByRoleId(string $roleId)
    {
        return $this->model->where('role_ids', 'all', [$roleId])->get();
    }
}
