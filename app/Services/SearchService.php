<?php
namespace App\Services;

use App\Contracts\Searchable;
use App\Models\Province;
use App\Models\Restaurant;
use App\Models\RestaurantImage;

class SearchService implements Searchable
{
    const PER_PAGE = 18;
    const IMAGES_PER_PAGE = 36;

    private $restaurant;
    private $province;
    private $image;

    public function __construct(Restaurant $restaurant, Province $province, RestaurantImage $image)
    {
        $this->restaurant = $restaurant;
        $this->province = $province;
        $this->image = $image;
    }

    /**
     * Lấy danh sách các provinces
     *
     * @return App\Models\Province
     */
    public function getProvinces()
    {
        return $this->province->select(['name', '_id'])->get();
    }

    /**
     * Xác định thành phố hiện tại của user
     *
     * @param  float  $longitude
     * @param  float  $latitude
     * @return App\Models\Province
     */
    public function getCurrentProvince(float $longitude, float $latitude)
    {
        return $this->province
                    ->where('location', 'geoIntersects', [
                        '$geometry' => [
                            'type' => 'Point',
                            'coordinates' => [$longitude, $latitude]
                        ]
                    ])->first();
    }

    /**
     * Tìm kiếm nhà hàng theo vị trí user
     *
     * @param  float  $longitude
     * @param  float  $latitude
     * @return App\Mdoels\Restaurant
     */
    public function findAround(array $args, int $page = 1)
    {
        return $this->restaurant
                    ->where('province', $args['province'])
                    ->where('location', 'near', [
                        '$geometry' => [
                            'type' => 'Point',
                            'coordinates' => [
                                $args['longitude'], $args['latitude']
                            ]
                        ]
                    ])
                    ->simplePaginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy danh sách nhà hàng theo thứ tự mới nhất của một province
     *
     * @param  array       $args
     * @param  int|integer $page
     * @return App\Models\Restaurant
     */
    public function newest(array $args, int $page = 1)
    {
        return $this->restaurant
                    ->where('province', $args['province'])
                    ->orderBy('created_at', 'DESC')
                    ->simplePaginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy chi tiết 1 nhà hàng
     *
     * @param  string $restaurantId
     * @return App\Models\Restaurant
     */
    public function getRestaurantById(string $restaurantId)
    {
        return $this->restaurant->find($restaurantId);
    }

    /**
     * Lấy danh sách ảnh của nhà hàng
     *
     * @param  string      $restaurantId
     * @param  int|integer $page
     * @return App\Models\RestaurantImage
     */
    public function getImagesByRestaurantId(string $restaurantId, int $page = 1)
    {
        return $this->image
                    ->where('restaurant_id', $restaurantId)
                    ->paginate(self::IMAGES_PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Tìm kiếm nhà hàng theo từ khoá (tên, địa chỉ ...)
     *
     * @param  string $keyword
     * @param  string $province
     * @return App\Models\Restaurant
     */
    public function getRestaurantsByKeyword(string $keyword, string $province, int $page = 1)
    {
        return $this->restaurant
                    ->where('province', $province)
                    ->fullText($keyword)
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }
}