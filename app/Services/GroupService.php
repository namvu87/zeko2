<?php

namespace App\Services;

use App\Services\BaseService;
use App\Contracts\GroupContract;
use App\Models\Group;
use Carbon\Carbon;

class GroupService extends BaseService implements GroupContract
{
    const COUNT_NEW_GROUP = 10;
    const PER_PAGE = 20;
    const LIMIT = 20;

    public function __construct(Group $model)
    {
        parent::__construct($model);
    }

    /**
     * @return int int App\Models\Group::TIMEKEEPING_SRVICE
     */
    public function getTimekeepingType()
    {
        return $this->model::TIMEKEEPING_SERVICE;
    }

    /**
     * @return int App\Models\Group::RESTAURANT_SERVICE
     */
    public function getRestaurantType()
    {
        return $this->model::RESTAURANT_SERVICE;
    }

    /**
     * get service type in Group
     * @return mixed
     */
    public function getServicesType()
    {
        return $this->model::SERVICES;
    }

    /**
     * Get group's owner by groupId
     *
     * @param string $groupId
     * @return App\Models\User
     */
    public function getOwner(string $groupId)
    {
        return $this->model::find($groupId)->owner;
    }

    /**
     * Gets the new group.
     */
    public function getNewGroups()
    {
        return $this->model::latest()
            ->limit(self::COUNT_NEW_GROUP)
            ->get();
    }

    /**
     * Lấy danh sách nhóm theo ID chủ sở hữu
     *
     * @param  string $ownerId
     * @return App\Models\Group
     */
    public function getByOwnerId(string $ownerId)
    {
        return $this->model->where('owner_id', $ownerId)->get();
    }

    /**
     * Lấy danh sách các nhóm mới theo trạng thái chăm sóc
     *
     * @param int    $careStatus
     * @param bool   $status
     * @param int    $page
     * @param string|null $ownerId
     *
     * @return mixed
     */
    public function getGroups(int $careStatus, bool $status, int $page, ?string $ownerId)
    {
        $query = $this->model;
        if (!empty($ownerId)) {
            $query = $query->where('owner_id', $ownerId);
        }
        return $query->care($careStatus ?? null)
            ->active($status)
            ->with('owner.ownerGroups')
            ->orderByDesc('created_at')
            ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Tìm kiếm nhóm theo từ khoá
     *
     * @param  string $keyword
     * @return App\Models\Group
     */
    public function getByKeyword(string $keyword)
    {
        return $this->model->fullText($keyword)->limit(self::LIMIT)->get();
    }

    /**
     * Thêm trạng thái đang bán hàng cho người dùng
     *
     * @param string $groupId
     * @param string $userId
     * @return void
     */
    public function onActingUser(string $groupId, string $userId): bool
    {
        return $this->model
            ->where('_id', $groupId)
            ->push('acting_user_ids', $userId, true);
    }

    /**
     * Xoá người dùng khỏi trạng thái bán hàng
     *
     * @param string $groupId
     * @param string $userId
     * @return boolean
     */
    public function offActingUser(string $groupId, string $userId): bool
    {
        return $this->model
            ->where('_id', $groupId)
            ->pull('acting_user_ids', $userId);
    }

    /**
     * Lấy các nhóm mới được khởi tạo kèm các thông tin liên quan
     */
    public function groupsStatistic(Carbon $startDate, Carbon $endDate)
    {
        return $this->model
            ->whereBetween('created_at', [$startDate, $endDate])
            ->with([
                'owner', 'goods',
                'invoices' => function ($query) use ($startDate, $endDate) {
                    return $query->whereBetween('created_at', [$startDate, $endDate]);
                },
                'timekeepings' => function ($query) use ($startDate, $endDate) {
                    return $query->whereBetween('created_at', [$startDate, $endDate]);
                },
            ])
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
