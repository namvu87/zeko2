<?php

namespace App\Services;

use App\Contracts\RequirementContract;
use App\Models\Requirement;

class RequirementService extends BaseService implements RequirementContract
{
    const PER_PAGE = 20;

    public function __construct(Requirement $model)
    {
        parent::__construct($model);
    }

    /**
     * @return App\Models\Requirement::ADDITION_TIMEKEEPING_TYPE
     */
    public function getAdditionTimekeepingType()
    {
        return $this->model::ADDITION_TIMEKEEPING_TYPE;
    }

    /**
     * @return App\Models\Requirement::SWITCH_SHIFT_TYPE
     */
    public function getWitchShiftType()
    {
        return $this->model::SWITCH_SHIFT_TYPE;
    }

    /**
     * @return App\Models\Requirement::TAKE_LEAVE_TYPE
     */
    public function getTakeLeaveType()
    {
        return $this->model::TAKE_LEAVE_TYPE;
    }

    /**
     * Lấy loại yêu cầu tham gia nhóm
     */
    public function getJoinGroupType()
    {
        return $this->model::JOIN_GROUP_TYPE;
    }

    public function getUserNotifications(array $data)
    {
        return $this->model::where('user_id', auth('api')->user()->_id)
            ->with(['creator:first_name,last_name,avatars', 'group:name'])
            ->orderBy('created_at', 'DESC')
            ->paginate(self::PER_PAGE);
    }

    /**
     * Gets the count notification by user identifier.
     *
     * @param      string $userId The user identifier
     */
    public function getCountNotificationByUserId(string $userId)
    {
        return $this->model::where('user_id', $userId)->count();
    }
}