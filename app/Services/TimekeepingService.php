<?php

namespace App\Services;

use App\Contracts\TimekeepingContract;
use App\Contracts\UserContract;
use App\Models\Timekeeping;

class TimekeepingService extends BaseService implements TimekeepingContract
{
    const PER_PAGE = 30;

    private $user;

    public function __construct(Timekeeping $model, UserContract $user)
    {
        parent::__construct($model);
        $this->user = $user;
    }

    public function typeAddition()
    {
        return $this->model::IS_ADDITION;
    }

    public function typeSwitch()
    {
        return $this->model::IS_SWITCH;
    }

    public function typeTakeLeave()
    {
        return $this->model::IS_TAKE_LEAVE;
    }

    /**
     * @param string $groupId
     * @param string $startDate
     * @param string $endDate
     *
     * @return mixed
     */
    public function groupByUserIdInGroup(string $groupId, string $startDate, string $endDate)
    {
        return $this->model->with('user:first_name,last_name')
            ->where('group_id', $groupId)
            ->where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->orderBy('date')
            ->get()
            ->sortBy(function($timekeeping) {
                return $timekeeping->user->first_name;
            })
            ->sortBy(function($timekeeping) {
                return $timekeeping->user->last_name;
            })
            ->groupBy('user_id')
            ->values()->all();
    }

    /**
     * Get timekeeping by userId and groupId
     *
     * @param array $condition
     *
     * @return
     */
    public function getByUserIdInGroup(string $userId, string $groupId, string $startDate, string $endDate)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('group_id', $groupId)
            ->where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->get();
    }

    /**
     * Get timekeeping by condition
     *
     * @param array $condition
     *
     * @return
     */
    public function getByCondition(array $condition)
    {
        $userId = null;
        $startDate = !empty($condition['start_date']) ?
                      date('Y-m-d', strtotime($condition['start_date'])) : null;
        $endDate = !empty($condition['end_date']) ?
                    date('Y-m-d', strtotime($condition['end_date'])) : null;

        if (!empty($condition['phone_number'])) {
            $userId = $this->user->findForPassport($condition['phone_number'])->id;
        }

        return $this->model
            ->periodDate($startDate, $endDate)
            ->orderBy('date', 'desc')
            ->user($userId)
            ->group($condition['group_id'])
            ->paginate(self::PER_PAGE);
    }

    /**
     * Get timekeeping by condition
     *
     * @param array $condition
     *
     * @return
     */
    public function getByConditionGraphQL(array $condition, $page = 1, $relations = [])
    {
        $userId = null;
        $startDate = !empty($condition['start_date']) ? date('Y-m-d', strtotime($condition['start_date'])) : null;
        $endDate = !empty($condition['end_date']) ? date('Y-m-d', strtotime($condition['end_date'])) : null;

        return $this->model
            ->with($relations)
            ->periodDate($startDate, $endDate)
            ->group($condition['group_id'] ?? null)
            ->user($condition['user_id'] ?? null)
            ->orderBy('date', 'DESC')
            ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy dữ liệu chấm công của người dùng hiện tại trong ngày hôm nay
     *
     * @param  string $userId
     * @param  string $groupId
     *
     * @return App\Models\Timekeeping|null
     */
    public function getTimekeepingToday(string $userId, string $groupId)
    {
        return $this->model
            ->where('date', date('Y-m-d'))
            ->where('user_id', $userId)
            ->where('group_id', $groupId)
            ->first();
    }

    /**
     * Lấy danh sách chấm công của người dùng hiện tại
     *
     * @param  string $userId
     * @param  array $args
     * @param  array $relations
     * @param  array $selections
     *
     * @return list of App\Models\Timekeeping
     */
    public function getMyTimekeepings(string $userId, array $args, $relations = [])
    {
        return $this->model
                    ->with($relations)
                    ->where('user_id', $userId)
                    ->periodDate($args['start_date'] ?? null, $args['end_date'] ?? null)
                    ->orderBy('date', 'DESC')
                    ->paginate(self::PER_PAGE, ['*'], 'page', $args['page'] ?? 1);
    }

    /**
     * Lấy 1 kết quả chấm công theo ngày của người dùng hiện tại
     *
     * @param  string $userId
     * @param  string $groupId
     * @param  string $date
     *
     * @return \App\Models\Timekeeping
     */
    public function getMyTimekeepingByDate(string $userId, string $groupId, string $date)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('date', $date)
            ->where('group_id', $groupId)
            ->first();
    }
}