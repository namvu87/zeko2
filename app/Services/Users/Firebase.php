<?php
namespace App\Services\Users;

use App\Contracts\UserContract;
use App\Contracts\Users\Firebase as FirebaseContract;
use App\Models\User;

class Firebase implements FirebaseContract
{
    private $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    /**
     * Add firebase token of current user
     *
     * @param  string $token
     * @return void
     */
    public function push(User $user, string $token)
    {
        $user->push('firebase_tokens', $token, true);
    }

    /**
     * Pull firebase token of current user
     *
     * @param  string $token
     * @return void
     */
    public function pull(User $user, string $token)
    {
        $user->pull('firebase_tokens', $token);
    }

    /**
     * Thêm mã uuid vào user
     *
     * @param  User   $user
     * @param  string $uuid
     * @return void
     */
    public function pushUuid(User $user, string $uuid)
    {
        $user->push('uuid_devices', $uuid, true);
    }
}