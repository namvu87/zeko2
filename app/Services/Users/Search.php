<?php
namespace App\Services\Users;

use App\Contracts\Users\Searchable;
use App\Models\User;
use App\Contracts\RoleContract;

class Search implements Searchable
{
    const PER_PAGE = 25;
    private $model;
    private $role;

    public function __construct(User $model, RoleContract $role)
    {
        $this->model = $model;
        $this->role = $role;
    }

    /**
     * Tìm kiếm user theo từ khoá
     *
     * @param  string      $keyword
     * @param  int|integer $page
     * @return App\Models\User
     */
    public function getByKeyword(string $keyword, int $page = 1)
    {
        return $this->model
                    ->fullText($keyword)
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Get users list of group
     *
     * @param  string $keyword
     * @param  string $groupId
     * @return App\Models\User
     */
    public function getByGroupId(string $keyword, string $groupId, int $page = 1)
    {
        return $this->model
                    ->where('group_ids', 'all', [$groupId])
                    ->fullText($keyword)
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy danh sách user không nằm trong group theo keyword
     * Phân trang dữ liệu
     *
     * @param  string $keyword
     * @param  string $groupId
     * @param  int    $page
     * @return App\Modes\User
     */
    public function getOutGroup(string $keyword = '', string $groupId, int $page = 1)
    {
        return $this->model
                    ->whereNotIn('group_ids', [$groupId])
                    ->fullText($keyword)
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Get users list of shift
     *
     * @param  string $keyword
     * @param  string $shiftId
     * @return App\Models\User
     */
    public function getByShiftId(string $keyword, string $shiftId, int $page = 1)
    {
        return $this->model
                    ->where('shift_ids', 'all', [$shiftId])
                    ->fullText($keyword)
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Get list users in groups but not in shift
     *
     * @param  string $groupId
     *
     * @return App\Models\User
     */
    public function getNotShiftInGroup(string $keyword, string $groupId, array $shiftIds, int $page = 1)
    {
        return $this->model->where('group_ids', 'all', [$groupId])
            ->whereNotIn('shift_ids', $shiftIds)
            ->fullText($keyword)
            ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Get users list in group and without shift
     *
     * @param  string $keyword
     * @param  string $groupId
     * @param  string $shiftId
     * @return App\Models\User
     */
    public function getOutShiftId(string $keyword, string $groupId, string $shiftId, int $page = 1)
    {
        return $this->model
                    ->where('group_ids', 'all', [$groupId])
                    ->whereNotIn('shift_ids', [$shiftId])
                    ->fullText($keyword)
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Lấy những User trong group mà chưa có vai trò theo keyword
     *
     * @param  string $keyword
     * @param  string $groupId
     * @return App\Models\User
     */
    public function getOutRole(string $keyword, string $groupId, $page = 1)
    {
        $roles = $this->role->getByGroupId($groupId);

        return $this->model
                    ->where('group_ids', 'all', [$groupId])
                    ->whereNotIn('role_ids', $roles->pluck('id')->toArray())
                    ->fullText($keyword)
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }
}