<?php
namespace App\Services\Users;

use App\Contracts\Users\Role as UserRoleContract;
use App\Contracts\UserContract;
use App\Models\User;
use Maklad\Permission\Models\Role as RoleModel;

class Role implements UserRoleContract
{
    private $user;

    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    /**
     * Gán quyền của group cho User
     *
     * @param  App\Models\User $user
     * @param  Maklad\Permission\Models\Role $role
     * @return void
     */
    public function assign(User $user, RoleModel $role)
    {
        $user->assignRole($role);
    }

    /**
     * Thu hồi quyền của User trong 1 group
     *
     * @param  App\Models\User $user
     * @param  Maklad\Permission\Models\Role $role
     * @return void
     */
    public function revoke(User $user, RoleModel $role)
    {
        $user->removeRole($role);
    }
}