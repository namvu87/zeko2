<?php
namespace App\Services;

use App\Contracts\PurchaseContract;
use App\Models\Product as ProductModel;

class PurchaseService implements PurchaseContract
{
    private $baokimUrl;
    private $merchantId;
    private $securePass;
    private $business;
    private $urlSuccess;
    private $urlCancel;
    private $urlDetail;

    public function __construct()
    {
        $this->baokimUrl = config('payment.baokim.url');
        $this->merchantId = config('payment.baokim.merchant_id');
        $this->securePass = config('payment.baokim.pass');
        $this->business = config('payment.baokim.business');
        $this->urlSuccess = route('purchase.success');
        $this->urlCancel = route('purchase.cancel');
    }

    /**
     * Create url payment baokim
     * @param  instamm Eloquent $product
     * @param  array $request
     * @return string
     */
    public function generateRequestUrl($product, $request, $orderId)
    {
        $params = [
            'merchant_id'       =>  strval($this->merchantId),
            'order_id'          =>  strval($orderId),
            'business'          =>  strval($this->business),
            'total_amount'      =>  strval($this->getProductPrice($product, $request)),
            'order_description' =>  strval(__('payment.description', ['name' => $product->name])),
            'url_success'       =>  strtolower($this->urlSuccess),
            'url_cancel'        =>  strtolower($this->urlCancel),
            'url_detail'        =>  strtolower(route('product.show', $product->id)),
            'payer_name' => $request['customer_name'],
            'payer_email' => $request['customer_email'],
            'payer_phone_no' => $request['customer_phone']
        ];
        ksort($params);
        $params['checksum'] = hash_hmac('SHA1', implode('', $params), $this->securePass);
        return $this->baokimUrl . http_build_query($params);
    }

    /**
     * Handle result from baokim
     * @param  [array]   $request
     * @return void
     */
    public function callback($request)
    {
        if ($this->verifyResponseUrl($request)) {
            return Order::updateOrder($request);
        }
        throw new \Exception(__('payment.invalid_information'));
    }

    /**
     * Hàm thực hiện xác minh tính chính xác thông tin trả về từ BaoKim.vn
     * @param $request chứa tham số trả về trên url
     * @return true nếu thông tin là chính xác, false nếu thông tin không chính xác
     */
    private function verifyResponseUrl($request)
    {
        $checksum = strtolower($request['checksum']);
        unset($request['checksum']);
        ksort($request);
        $verifyChecksum = hash_hmac('SHA1', implode('', $request), $this->securePass);
        if ($verifyChecksum === $checksum) {
            return true;
        }
        return false;
    }

    public function validateGetInput($request, $productPriceType)
    {
        $message = '';
        if (empty($request['month_count']) || (int) $request['month_count'] < 1) {
            $message = __('payment.validate.month_count_invalid');
        }
        if ($productPriceType == ProductModel::PRICE_TYPE_12) {
            if (empty($request['user_count']) || (int)$request['user_count'] < 1) {
                $message .= __('payment.validate.user_count_invalid');
            }
        }
        return $message;
    }

    public function getProductPrice($product, $request)
    {
        if ($product->price_type == ProductModel::PRICE_TYPE_12) {
            return (int) ceil((int)$request['user_count'] / $product->user_count) * $product->price * (int)$request['month_count'];
        }
        return (int) $product->price * (int)$request['month_count'];
    }
}