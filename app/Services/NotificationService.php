<?php
namespace App\Services;

use App\Contracts\NotificationContract;
use App\Services\BaseService;
use App\Models\Notification;
use App\Interfaces\Notifiable;

class NotificationService extends BaseService implements NotificationContract, Notifiable
{
    const PER_PAGE = 20;

    public function __construct(Notification $model)
    {
        parent::__construct($model);
    }

    /**
     * Gets the data.
     *
     * @param      \App\Models\Notification  $notification  The notification
     */
    public function getData(Notification $notification)
    {
        $data = [];
        $type = $notification->type;
        switch ($type) {
            case $this->model::TYPE_60:
            case $this->model::TYPE_61:
                $data = $this->getDataGroupRole($notification);
                break;
        }
        return $data;
    }

    public function getDataGroupRole(Notification $notification)
    {
        $data = $notification->toArray();
        $data['user_assigned'] = $notification->userAssigned;
        $data['group'] = $notification->group;
        return $data;
    }

    /**
     * Gets the user notifications.
     *
     * @param array  $data
     */
    public function getUserNotifications(array $data)
    {
        return $this->model::where('user_id', auth('api')->user()->_id)
                            ->with(['creator:first_name,last_name,avatars', 'group:name'])
                            ->orderBy('created_at', 'DESC')
                            ->paginate(self::PER_PAGE);
    }

    /**
     * Gets the count notification by user identifier.
     *
     * @param string  $userId
     */
    public function getCountNotificationByUserId(string $userId): int
    {
        return $this->model::where('user_id', $userId)->count();
    }

    /**
     * Lấy thông báo của người dùng theo trang
     *
     * @param integer $page
     * @param string $userId
     * @param array $relations
     * @return Pagination
     */
    public function paginateByUserId(int $page = 1, string $userId, array $relations = [])
    {
        return $this->model
                    ->with($relations)
                    ->where('user_id', $userId)
                    ->orWhere('user_ids', 'all', [$userId])
                    ->orderBy('created_at', 'DESC')
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Xoá thông báo khi hoá đơn được thanh toán, hoặc huỷ
     *
     * @param string $invoiceId
     * @return boolean
     */
    public function removeByInvoiceId(string $invoiceId): bool
    {
        return $this->model->where('invoice.id', $invoiceId)->delete();
    }
}