<?php

namespace App\Services;

use App\Services\BaseService;
use App\Contracts\ServiceFeeTransactionContract;
use App\Models\ServiceFeeTransaction;

class ServiceFeeTransactionService extends BaseService implements ServiceFeeTransactionContract
{
    public function __construct(ServiceFeeTransaction $model)
    {
        parent::__construct($model);
    }

    public function getInitedState(): int
    {
        return $this->model::INITED_STATUS;
    }

    public function getPaidState(): int
    {
        return $this->model::PAID_STATUS;
    }

    public function getCanceledState(): int
    {
        return $this->model::CANCELED_STATUS;
    }
}
