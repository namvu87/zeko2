<?php

namespace App\Services;

use App\Contracts\SliderContract;
use App\Models\Slider;

class SliderService implements SliderContract
{
    private $slider;

    public function __construct(Slider $slider)
    {
        $this->slider = $slider;
    }

    /**
     * Lấy slider cho mobile
     *
     * @return App\Models\Slider
     */
    public function getCampaign()
    {
        return $this->slider
                    ->where('type', $this->slider::TYPE_SHOW_MOBILE)
                    ->limit(5)
                    ->orderBy('created_at', 'DESC')
                    ->get();
    }
}