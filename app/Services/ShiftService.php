<?php

namespace App\Services;

use App\Contracts\ShiftContract;
use App\Services\BaseService;
use App\Models\Shift;

class ShiftService extends BaseService implements ShiftContract
{
    public function __construct(Shift $model)
    {
        parent::__construct($model);
    }

    /**
     * @return App\Models\Shift::SINGLE_TIME_TYPE
     */
    public function getSingleType()
    {
        return $this->model::SINGLE_TIME_TYPE;
    }

    /**
     * @return App\Models\Shift::MULTIPLE_TIME_TYPE
     */
    public function getMultiType()
    {
        return $this->model::MULTIPLE_TIME_TYPE;
    }

    /**
     * @return App\Models\Shift::TYPES
     */
    public function getTypes()
    {
        return $this->model::TYPES;
    }

    /**
     * @return App\Models\Shifts::SCHEDULES
     */
    public function getSchedules()
    {
        return $this->model::SCHEDULES;
    }

    /**
     * @param string $userId
     * @param string $groupId
     *
     * @return mixed
     */
    public function getByIdsAndGroup(array $shiftIds, string $groupId)
    {
        return $this->model->whereIn('_id', $shiftIds)->where('group_id', $groupId)->first();
    }
}