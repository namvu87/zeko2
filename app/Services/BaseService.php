<?php

namespace App\Services;

use App\Contracts\BaseContract;
use Jenssegers\Mongodb\Eloquent\Model;

class BaseService implements BaseContract
{
    const PER_PAGE = 20;

    /**
     * define {Model} to usage
     * @var Jenssegers\Mongodb\Eloquent\Model
     */
    protected $model;

    /**
     * Specify model will to use
     *
     * @param Jenssegers\Mongodb\Eloquent\Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get table name
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->model->getTable();
    }

    /**
     * Get every data satisfy conditions
     * 
     * @param array $relations
     * @param  array  $condition
     * @return App\Models\{Model}
     */
    public function getAll(array $conditions = [], array $relations = [])
    {
        return $this->model
                    ->with($relations)
                    ->where($conditions)
                    ->get();
    }

    /**
     * Gets all data {Model} in group.
     *
     * @param  string $groupId
     * @param  array $relations
     * @return App\Models\{Model}
     */
    public function getByGroupId(string $groupId, array $relations = [])
    {
        return $this->model
                    ->with($relations)
                    ->where('group_id', $groupId)
                    ->get();
    }

    /**
     * Lấy danh sách dữ liệu theo groupId kết hợp phân trang
     *
     * @param  string      $groupId
     * @param  int|integer $page
     * @param  array       $relation
     * @return Collection App\Models\{Model}
     */
    public function paginateByGroupId(string $groupId, int $page = 1, array $relations = [])
    {
        return $this->model
                    ->where('group_id', $groupId)
                    ->with($relations)
                    ->orderBy('created_at', 'DESC')
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Get data with pagination (can with relation, selected fields) in {Model}
     *
     * @param  array $relation
     * @param  array $selected
     * @return App\Models\{Model}
     */
    public function paginate($page = 1, array $relations = [], array $selections = [])
    {
        return $this->model
                    ->with($relations)
                    ->paginate(self::PER_PAGE, $selections, 'page', $page);
    }

    /**
     * Get data with limit in {Model}
     *
     * @param  int $limit
     * @return App\Models\{Model}
     */
    public function getLimit(int $limit)
    {
        return $this->model->limit($limit)->get();
    }

    /**
     * Get a Model instance via id
     * 
     * @param  string $id
     * @param array $relation
     * @return App\Models\{Model}
     */
    public function getById(string $id, array $relations = [], array $selections = []): ?Model
    {
        return $this->model
                    ->where('_id', $id)
                    ->with($relations)
                    ->select($selections)
                    ->first();
    }

    /**
     * Get all instance Model in array ids
     *
     * @param  array $ids
     * @return App\Models\{Model}
     */
    public function getByIds(array $ids, array $relations = [], array $selections = [])
    {   
        return $this->model
                    ->with($relations)
                    ->select($selections)
                    ->whereIn('_id', $ids)
                    ->get();
    }

    /**
     * Get data with pagination (can with relation, selected fields) by ids in {Model}
     *
     * @param array $ids
     * @param  int $perPage
     * @param  array $relation
     * @return App\Models\{Model}
     */
    public function paginateByIds(array $ids, $page = 1, array $relation = [], array $selected = []
    ) {
        return $this->model
                    ->whereIn('_id', $ids)
                    ->with($relation)
                    ->select($selected)
                    ->paginate(self::PER_PAGE, ['*'], 'page', $page);
    }

    /**
     * Create a new record
     * 
     * @param  array $data
     * @return App\Models\{Model}
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update a record via ID
     * 
     * @param  string $id
     * @param  array $data
     * @return bool
     */
    public function update(string $id, array $data)
    {
        return $this->model->find($id)->update($data);
    }

    /**
     * Update Model instance and return itself after updated
     * 
     * @param Model $model
     * @param array $data
     * @return App\Models\{Model}
     */
    public function save(Model $model, array $data)
    {
        foreach ($data as $attr => $value) {
            $model->setAttribute($attr, $value);
        }
        $model->save();
        return $model;
    }

    /**
     * Update via Model instance
     *
     * @param  Model $model
     * @param  array $data
     * @return bool
     */
    public function updateInstance(Model $model, array $data): bool
    {
        return $model->update($data);
    }

    /**
     * Update without get instance
     *
     * @param array $ids
     * @param array $data
     * @return void
     */
    public function massUpdate(array $ids, array $data): bool
    {
        return $this->model->whereIn('_id', $ids)->update($data);
    }

    /**
     * Update embedded array with $ operator
     *
     * @param  string $id
     * @param  array  $operator Toán tử $ chỉ định vị trí sẽ update (matching $ in biến $data)
     * @param  array  $data
     * @return bool
     */
    public function updateOperator(string $id, array $operator, array $data): bool
    {
        return $this->model
                    ->where('_id', $id)
                    ->where($operator)
                    ->update($data);
    }

    /**
     * Update {Model} if exists $id
     * Insert new {Model} if not exist $id
     *
     * @param  string $id
     * @param  array $data
     * @return App\Models\{Model}
     */
    public function upsert(string $id, array $data)
    {
        return $this->model
                    ->where('_id', $id)
                    ->update($data, ['upsert' => true]);
    }

    /**
     * Delete a record via ID
     * 
     * @param  string $id
     * @return int
     */
    public function delete(string $id)
    {
        return $this->model->find($id)->delete();
    }

    /**
     * Delete the document via instance itself
     *
     * @param Model $model
     * @return void
     */
    public function deleteInstance(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * Remove records has id in $ids in {Model}
     * 
     * @param  array $ids
     * @return bool
     */
    public function destroy(array $ids)
    {
        return $this->model->destroy($ids);
    }

    /**
     * Lấy số lượng bản ghi theo điều kiện
     *
     * @param  array  $condition
     * @return int
     */
    public function count(array $condition = []): int
    {
        return $this->model->where($condition)->count();
    }
}
