<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Timekeeping extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'timekeeping';
    }
}