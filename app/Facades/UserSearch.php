<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class UserSearch extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'userSearch';
    }
}