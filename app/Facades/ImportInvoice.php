<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ImportInvoice extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'importInvoice';
    }
}