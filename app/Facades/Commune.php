<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Commune extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'commune';
    }
}