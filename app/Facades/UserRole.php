<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class UserRole extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'userRole';
    }
}