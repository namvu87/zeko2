<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Goods extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'goods';
    }
}