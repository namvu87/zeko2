<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class GroupMenu extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'groupMenu';
    }
}