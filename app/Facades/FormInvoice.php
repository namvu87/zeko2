<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class FormInvoice extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'formInvoice';
    }
}