<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Place extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'place';
    }
}