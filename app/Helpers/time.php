<?php
use Carbon\Carbon;

function convertTimekepping($checkin, $checkout, $checkin2 = null, $checkout2 = null)
{
    $checkin = Carbon::createFromFormat('H:i', $checkin);
    $checkout = Carbon::createFromFormat('H:i', $checkout);
    $minutes = $checkout->diffInMinutes($checkin);
    if (!empty($checkin2) && !empty($checkout2)) {
        $checkin2 = Carbon::createFromFormat('H:i', $checkin2);
        $checkout2 = Carbon::createFromFormat('H:i', $checkout2);
        $minutes2 = $checkout2->diffInMinutes($checkin2);
        $minutes = $minutes + $minutes2;
    }
    $hour = round($minutes / 60);
    return $hour > 0 ? $hour . 'h' . ($minutes % 60) : ($minutes % 60);
}

function getDiffInMinutesTimeKeeping($from, $to)
{
    $from = Carbon::createFromFormat('H:i', $from);
    $to = Carbon::createFromFormat('H:i', $to);
    return $to->diffInMinutes($from);
}

function convertMinutesToHour($minutes)
{
    $hour = round($minutes / 60);
    $minutes = $minutes % 60;
    return $hour > 0 ? $hour . 'h' . ($minutes % 60) : ($minutes % 60);
}

function convertPrice($value)
{
    return number_format((int)$value, 0, '', '.');
}