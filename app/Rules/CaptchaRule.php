<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Contracts\CaptchaContract;

class CaptchaRule implements Rule
{
    private $key;
    private $expires;
    private $captcha;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($key, $expires, CaptchaContract $captcha)
    {
        $this->key = $key;
        $this->expires = $expires;
        $this->captcha = $captcha;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->captcha->checkApi($value, $this->key, $this->expires);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.captcha');
    }
}
