<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EqualRule implements Rule
{
    private $compare;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($compare)
    {
        $this->compare = $compare;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->compare == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.equal');
    }
}
