<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Notification;
use App\Contracts\FirebaseContract;
use App\Contracts\UserContract;
use App\Http\Resources\Notification as NotificationResource;

class NotifyGroupRemoved implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $notification;
    private $groupName;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Notification $notification, string $groupName)
    {
        $this->notification = $notification;
        $this->groupName = $groupName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(FirebaseContract $firebase, UserContract $user)
    {
        $tokens = $user->getById($this->notification->user_id)->firebase_token ?? [];        

        $firebase->to($tokens)
                 ->data([
                    'notification' => new NotificationResource($this->notification),
                    'type' => TYPE_11
                 ])
                 ->notification([
                    'title' => __($this->groupName),
                    'body' => __('notification.messages.11')
                 ])
                 ->send();
    }
}
