<?php

namespace App\Jobs\Restaurant;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Invoice;
use App\Contracts\UserContract;
use App\Contracts\FirebaseContract;
use App\Http\Resources\Invoice as InvoiceResource;

class NotifyJoinedInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $invoice;
    private $exceptId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, string $exceptId)
    {
        $this->invoice = $invoice;
        $this->exceptId = $exceptId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(FirebaseContract $firebase, UserContract $user)
    {
        $userIds = array_diff($this->invoice->user_ids ?? [], [$this->exceptId]);
        $tokens = $user->getFirebaseTokensByIds($userIds);
        $currentUser = $user->getById($this->exceptId);

        $firebase->to($tokens)
                 ->data([
                    'invoice' => new InvoiceResource($this->invoice),
                    'type' => 3
                 ])
                 ->notification([
                    'title' => __('manage_sale.client_notify.new_member_joined_invoice_title'),
                    'body' => __('manage_sale.client_notify.new_member_joined_invoice_body', [
                        'user' => $currentUser->first_name . ' ' . $currentUser->last_name,
                        'code' => $this->invoice->code
                    ])
                 ])
                 ->send();
    }
}
