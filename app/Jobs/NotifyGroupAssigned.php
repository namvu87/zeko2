<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Notification;
use App\Contracts\FirebaseContract;
use App\Contracts\UserContract;
use App\Http\Resources\Notification as NotificationResource;

class NotifyGroupAssigned implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $notification;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(FirebaseContract $firebase, UserContract $user, GroupContract $group)
    {
        $tokens = $user->getById($this->notification->user_id)->firebase_token ?? [];        
        $groupName = $group->getById($this->notification->group_id)->name;

        $firebase->to($tokens)
                 ->data([
                    'notification' => new NotificationResource($this->notification),
                    'type' => Notification::TYPE_10
                 ])
                 ->notification([
                    'title' => __($groupName),
                    'body' => __('notification.messages.11')
                 ])
                 ->send();
    }
}
