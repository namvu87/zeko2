<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Restaurant;

class RestaurantAvatarGenerateCommand extends Command
{
    private $restaurant;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'avatar:generate {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Câp nhật avatar lấy thêm sau khi đã tạo dữ liệu';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Restaurant $restaurant)
    {
        parent::__construct();
        $this->restaurant = $restaurant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('file');
        $path = storage_path() . "/json/foody/{$filename}.json";

        ini_set('memory_limit', '-1');
        $data = json_decode(file_get_contents($path), true);

        $bar = $this->output->createProgressBar(count($data));
        $bar->start();
        $success = 0;
        $invalid = 0;

        foreach ($data as $item) {
            try {
                $restaurants = $this->restaurant->where('name', $item['name'])->get();
                foreach ($restaurants as $restaurant) {
                    $restaurant->avatar = [
                        'thumb' => $item['avatar']['thumb'],
                        'origin' => $item['avatar']['origin'],
                    ];
                    $restaurant->save();
                    $success ++;
                }
                $invalid ++;
            } catch (\Exception $e) {
                $invalid ++;
            }
            $bar->advance();
        }

        $bar->finish();
        $this->info("Generated {$success} the restaurant!");
        $this->info("Cannot generated {$invalid} the restaurant!");
    }
}
