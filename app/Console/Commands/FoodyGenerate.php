<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Restaurant;
use App\Models\RestaurantImage;

class FoodyGenerate extends Command
{
    private $restaurant;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'foody:generate {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate foody data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Restaurant $restaurant)
    {
        parent::__construct();
        $this->restaurant = $restaurant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('file');
        $path = storage_path() . "/json/foody/{$filename}.json";

        ini_set('memory_limit', '-1');
        $data = json_decode(file_get_contents($path), true);

        $bar = $this->output->createProgressBar(count($data));
        $bar->start();
        $success = 0;
        $invalid = 0;

        foreach ($data as $item) {
            try {
                $restaurant = $this->restaurant->create([
                    'name'          => $item['name'],
                    'district'      => $item['district'],
                    'province'      => $item['province'],
                    'address'       => $item['address'],
                    'opening_times' => [
                        'start' => $item['opening_times']['start'],
                        'end'   => $item['opening_times']['end']
                    ],
                    'price_range'   => [
                        'min' => $item['price_range']['min'],
                        'max' => $item['price_range']['max'],
                    ],
                    'avatar' => $item['images'][0] ?? [],
                    'cuisines' => $item['cuisines'],
                    'categories' => $item['categories'],
                    'location' => [
                        'type' => 'Point',
                        'coordinates' => [
                            $item['longitude'], $item['latitude']
                        ]
                    ],
                ]);
                foreach ($item['images'] as $image) {
                    RestaurantImage::create([
                        'thumb' => $image['thumb'],
                        'origin' => $image['origin'],
                        'restaurant_id' => $restaurant->id
                    ]);
                }
                $success ++;
            } catch (\Exception $e) {
                $invalid ++;
            }
            $bar->advance();
        }

        $bar->finish();
        $this->info("Generated {$success} the restaurant!");
        $this->info("Cannot generated {$invalid} the restaurant!");
    }
}
