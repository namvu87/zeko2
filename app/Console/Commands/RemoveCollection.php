<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RemoveCollection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:collection {collection*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all record in collection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $argument = $this->argument();
        $collections = $argument['collection'];
        foreach ($collections as $collection) {
            \DB::connection('mongodb')->collection($collection)->truncate();
        }
        $this->info('Đã xóa thành công!');
    }
}
