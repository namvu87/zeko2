<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Area;
use App\Models\Commune;

class AreaGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'area:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate all areas of Viet Nam';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $datas = config('area');
        $areaCount = 0;
        $communeCount = 0;
        foreach ($datas as $pkey => $province) {
            foreach ($province as $dkey => $district) {
                $area = Area::create([
                    'name' => $pkey . ' - ' . $dkey
                ]);
                $areaCount ++;
                foreach ($district as $commune) {
                    Commune::create([
                        'name' => $commune,
                        'area_id' => $area->id
                    ]);
                    $communeCount ++;
                }
            }
        }

        $this->info("Đã khởi tạo thành công {$areaCount} dữ liệu khu vực (tỉnh-huyện)");
        $this->info("Đã khởi tạo thành công {$communeCount} dữ liệu phường xã");
    }
}
