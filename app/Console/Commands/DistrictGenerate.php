<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Province;

class DistrictGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'district:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $path = storage_path() . "/json/district.json";
        $data = json_decode(file_get_contents($path), true);

        $bar = $this->output->createProgressBar(count($data));
        $bar->start();

        $success = 0;
        $error = 0;

        foreach ($data as $index => $item) {
            $province = Province::where('name', $item['NAME_1'])->first();
            if ($province) {
                $province->push('districts', $item['NAME_2'], true);
                $success ++;
            } else {
                $error ++;
            }

            $bar->advance();
        }
        $bar->finish();
        $this->info("Generated {$success} / {$index} the restaurant!");
        $this->info("Failed {$error} / {$index} the restaurant!");
    }
}
