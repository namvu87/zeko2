<?php

namespace App\Console\Commands;

use App\Contracts\UserContract;
use App\Events\SystemNotify;
use App\Mail\DisableGroup;
use App\Models\ServiceFee;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CheckOutDatePaymentFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'serviceFee:check-out-of-date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kiểm tra những nhóm chưa thanh toán phí sử dụng';

    private $user;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserContract $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ServiceFee::with('group')
            ->where('aggregated_month', Carbon::now()->subMonth()->format('Y-m'))
            ->paid(false)
            ->chunk(100, function ($fees) {
                foreach ($fees as $fee) {
                    $user = $this->user->getById($fee->group->owner_id);
                    $fee->group->update([
                        'is_active' => false
                    ]);

                    Mail::to($user->email)->send(new DisableGroup($fee->group));
                    event(new SystemNotify(
                        $fee->group,
                        [$user->id],
                        SystemNotify::TYPE_102
                    ));
                }
            });
    }
}
