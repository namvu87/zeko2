<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Province;

class PolygonCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'province:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $path = storage_path() . "/json/province.json";
        $data = json_decode(file_get_contents($path), true);

        $bar = $this->output->createProgressBar(count($data));
        $bar->start();

        foreach ($data as $index => $item) {
            try {
                Province::create([
                    'name' => $item['NAME_1'],
                    'location' => $item['json_geometry']
                ]);
                $bar->advance();
            } catch (\Exception $e) {
                info($item['ADM1_NAME']);
            }
        }
        $bar->finish();
        $this->info("Generated {$index} the restaurant!");
    }
}
