<?php

namespace App\Console\Commands;

use App\Contracts\RoleContract;
use App\Contracts\UserContract;
use App\Events\SystemNotify;
use App\Mail\PayServiceFee;
use App\Models\ServiceFee;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class RemindPayServiceFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'serviceFee:remind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Nhắc nhở thanh toán phí dịch vụ';

    private $user;
    private $role;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserContract $user, RoleContract $role)
    {
        parent::__construct();
        
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ServiceFee::with('group')
            ->where('aggregated_month', Carbon::now()->subMonth()->format('Y-m'))
            ->paid(false)
            ->chunk(100, function ($fees) {
                foreach ($fees as $fee) {
                    $roles = $this->role->getByRoleNames([
                        "{$fee->group->id}.owner",
                        "{$fee->group->id}.admin"
                    ]);
                    $users = $this->user->getByRoleIds($roles->pluck('_id')->toArray());

                    Mail::to($users)->send(new PayServiceFee($fee, $fee->group));

                    event(new SystemNotify(
                        $fee->group,
                        $users->pluck('_id')->toArray(),
                        SystemNotify::TYPE_101
                    ));
                }
            });
    }
}
