<?php

namespace App\Console\Commands;

use App\Contracts\Restaurant\ReportContract;
use App\Contracts\RoleContract;
use App\Contracts\ServiceFeeContract;
use App\Contracts\UserContract;
use App\Events\SystemNotify;
use App\Mail\PayServiceFee;
use App\Models\Group;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CalculationServiceFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'serviceFee:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tính chi phí dịch vụ hàng tháng của người dùng';

    private $report;
    private $serviceFee;
    private $user;
    private $role;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ReportContract $report,
        ServiceFeeContract $serviceFeeContract,
        UserContract $userContract,
        RoleContract $role
    ) {
        parent::__construct();

        $this->report = $report;
        $this->serviceFee = $serviceFeeContract;
        $this->user = $userContract;
        $this->role = $role;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->firstOfMonth();
        $maxDay = config('payment_package.max_used_day');
        $end = Carbon::now()->lastOfMonth()->subMonth()->add('1day')->sub('1second');

        Group::where('is_active', '<>', false)
            ->chunk(100, function ($groups) use ($date, $end, $maxDay) {
                foreach ($groups as $group) {
                    if ($group->created_at->diffInDays($date) <= $maxDay) {
                        continue;
                    }

                    $data = $this->prepareData($group, $end);
                    $serviceFee = $this->serviceFee->create($data);

                    $roles = $this->role->getByRoleNames(["{$group->id}.owner", "{$group->id}.admin"]);
                    $users = $this->user->getByRoleIds($roles->pluck('_id')->toArray());
                    Mail::to($users)->send(new PayServiceFee($serviceFee, $group));

                    event(new SystemNotify(
                        $group,
                        $users->pluck('_id')->toArray(),
                        SystemNotify::TYPE_101
                    ));
                }
            });
    }

    private function prepareData(Group $group, Carbon $end): array
    {
        $data = [
            'group_id'         => $group->id,
            'aggregated_month' => Carbon::now()->subMonth()->format('Y-m'),
            'status'           => $this->serviceFee->getUnpaidState()
        ];
        switch ($group->service_type) {
            case Group::TIMEKEEPING_SERVICE:
                $data['users_count'] = $group->number_user;
                break;
            case Group::RESTAURANT_SERVICE:
                $data = $this->prepareRestaurantService($data, $group, $end);
                break;
            default:
                break;
        }
        return $data;
    }

    private function prepareRestaurantService(array $data, Group $group, Carbon $end): array
    {
        $start = Carbon::now()->firstOfMonth()->subMonth();
        $statistic = $this->report->getTotalRevenueByDateRange($group->id, $start, $end);

        $data['invoices_count'] = 0;
        $data['revenue'] = 0;

        if (count($statistic)) {
            $data['invoices_count'] = $statistic[0]->purchased_invoice;
            $data['revenue'] = $statistic[0]->revenue;
        }
        return $data;
    }
}
