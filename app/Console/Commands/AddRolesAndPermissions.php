<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Group;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;
use Maklad\Permission\Exceptions\PermissionDoesNotExist;

class AddRolesAndPermissions extends Command
{
    /**
     * user's role default, who registed the group
     */
    const ROLE_DEFAULT = 'owner';
    const TIMEKEEPING_SERVICE = 'timekeeping_service';
    const RESTAURANT_SERVICE = 'restaurant_service';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:roles-permissions {group}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add new roles and new permissions for users in a group';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groupId = $this->argument('group');
        try {
            $group = Group::findOrFail($groupId);
            $serviceType = $this->convertServiceTypeToText($group->service_type);

            $permissionsName = config("role.permissions.{$serviceType}");
            $permissionsName['permissions'] = config('role.permissions.permissions');
            $permissionsName['group'] = config('role.permissions.group');

            $owner = $group->owner;
            $ownerRole = $groupId . '.' . self::ROLE_DEFAULT;


            if ($owner->hasRole($ownerRole)) {
                $role = Role::findByName($ownerRole);
                foreach ($permissionsName as $label => $permissions) {
                    foreach ($permissions as $permission) {
                        $permissionName = $groupId . '.' . $label . '.' . $permission;
                        try {
                            $role->hasPermissionTo($permissionName);
                        } catch (PermissionDoesNotExist $e) {
                            $permissionModel = Permission::create([
                                'name'     => $permissionName,
                                'group_id' => $groupId,
                                'label'    => $label
                            ]);
                            $role->givePermissionTo($permissionModel);
                        }
                    }
                }
            }
            $this->info('Accomplished!');
        } catch (ModelNotFoundException $e) {
            $this->info('Group not found!');
        }
    }

    /**
     * @param  int $type
     * @return string
     */
    private function convertServiceTypeToText(int $type) :string {
        if ($type === Group::TIMEKEEPING_SERVICE) {
            return self::TIMEKEEPING_SERVICE;
        }
        return self::RESTAURANT_SERVICE;
    }
}
