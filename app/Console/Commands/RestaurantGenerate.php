<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Restaurant;

class RestaurantGenerate extends Command
{
    private $restaurant;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restaurant:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate restaurant data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Restaurant $restaurant)
    {
        parent::__construct();
        $this->restaurant = $restaurant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = storage_path() . "/json/restaurants.json";
        $data = json_decode(file_get_contents($path), true);

        $bar = $this->output->createProgressBar(count($data));
        $bar->start();

        foreach ($data as $index => $item) {
            $this->restaurant->create([
                'name'          => $item['name'],
                'province'      => $item['province'],
                'address'       => $item['address'],
                'opening_times' => $item['opening_times'],
                'price_range'   => $item['price_range'],
                'images'        => $item['images'],
                'location' => [
                    'type' => 'Point',
                    'coordinates' => [
                        $item['longitude'], $item['latitude']
                    ]
                ]
            ]);
            $bar->advance();
        }

        $bar->finish();
        $this->info("Generated {$index} the restaurant!");
    }
}
