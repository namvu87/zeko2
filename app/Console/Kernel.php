<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\RemoveCollection::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $email = config('mail.system_feedback_email');
        $schedule->command('serviceFee:calculate')
            ->monthly()
            ->runInBackground()
            ->emailOutputOnFailure($email);
        for ($i = 2; $i <= config('payment_package.out_of_date'); $i++) {
            $schedule->command('serviceFee:remind')
                ->monthlyOn($i, '09:00')
                ->runInBackground()
                ->emailOutputOnFailure($email);
        }
        $schedule->command('serviceFee:check-out-of-date')
            ->monthlyOn(config('payment_package.out_of_date') + 1, '00:00')
            ->runInBackground()
            ->emailOutputOnFailure($email);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
