<?php

namespace App\Policies;

use App\Models\User;
use App\Models\InvoiceForm;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvoiceFormPolicy
{
    use HandlesAuthorization;

    /**
     * Kiểm tra invoice form có thuộc về group không
     *
     * @param      \App\Models\User         $user         The user
     * @param      \App\Models\InvoiceForm  $invoiceForm  The invoice form
     * @param      String                   $groupId      The group identifier
     *
     * @return     bool
     */
    public function belongsToGroup(User $user, InvoiceForm $invoiceForm, String $groupId)
    {
        return $invoiceForm->group_id === $groupId;
    }
}
