<?php

namespace App\Policies;

use App\Models\GroupMenu;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupMenuPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function belongsToGroup(User $user, GroupMenu $groupMenu, $groupId)
    {
        // $menu = $groupMenu->parent->parent ?? $groupMenu->parent ?? $groupMenu;
        return $groupMenu->group_id === $groupId;
    }
}
