<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Kiểm tra user có thuộc về group hay không
     *
     * @param  User   $user
     * @param  Shift  $shift
     * @param  string $groupId
     * @return bool
     */
    public function belongsToGroup(User $currentUser, User $user, string $groupId)
    {
        if (!in_array($groupId, $user->group_ids)) {
            $this->deny(__('auth.you_are_not_in_group'));
        }
        return true;
    }
}
