<?php

namespace App\Policies;

use App\Models\Requirement;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RequirementPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function belongstoGroup(User $user, Requirement $requirement, string $groupId)
    {
        return $requirement->group_id == $groupId;
    }
}
