<?php

namespace App\Policies;

use App\Models\ReturnedInvoice;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReturnedInvoicePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function belongsToGroup(User $user, ReturnedInvoice $invoice, string $groupId)
    {
        return $invoice->group_id === $groupId;
    }

    /**
     * Xác định hóa đơn trả hàng có thể được tạo không
     *
     * @param      \App\Models\User  $user     The user
     * @param      string            $groupId  The group identifier
     *
     * @return     <type>            ( description_of_the_return_value )
     */
    public function create(User $user, string $groupId)
    {
        return $user->hasPermissionTo($groupId . '.returned-invoice.store');
    }
}
