<?php

namespace App\Policies;

use App\Models\Good;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GoodPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function belongsToGroup(User $user, Good $good, $groupId)
    {
        $goodParent = $groupMenu->parent ?? $good;
        return $goodParent->group_id === $groupId;
    }
}
