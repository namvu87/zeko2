<?php

namespace App\Policies;

use App\Models\User;
use Maklad\Permission\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    const OWNER_ROLE_RULES = '/owner/';

    /**
     * Kiểm tra role gửikhôngthuộc về group đó hay không
     *
     * @param  User   $user
     * @param  Role   $role
     * @param  string $groupId
     * @return bool
     */
    public function belongsToGroup(User $user, Role $role, string $groupId)
    {
        return $role->group_id === $groupId;
    }

    /**
     * Kiểm tra vai trò (role) có phải là owner không
     * Không phải owner trả về TRUE
     * Là owner trả về FALSE
     *
     * @param  User   $user
     * @param  Role   $role
     * @return bool
     */
    public function outOwner(?User $user, Role $role) {
        return !preg_match(self::OWNER_ROLE_RULES, $role->name);
    }
}
