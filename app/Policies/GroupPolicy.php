<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Group;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    public function viewTimeKeeping(User $user, Group $group)
    {
        return in_array($group->id, $user->group_ids);
    }

    public function isNotOwner(User $user, Group $group, string $userId)
    {
        return $group->owner_id !== $userId;
    }
}
