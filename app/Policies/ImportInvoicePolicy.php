<?php

namespace App\Policies;

use App\Models\ImportInvoice;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ImportInvoicePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function belongsToGroup(User $user, ImportInvoice $invoice, string $groupId)
    {
        return $invoice->group_id === $groupId;
    }
}
