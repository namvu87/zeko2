<?php

namespace App\Policies;

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvoicePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function belongsToGroup(User $user, Invoice $invoice, string $groupId)
    {
        return $invoice->group_id === $groupId;
    }

    /**
     * Xác định xem người dùng có quyền khởi tạo hóa đơn không
     *
     * @param      \App\Models\User     $user     The user
     * @param      \App\Models\Invoice  $invoice  The invoice
     */
    public function create(User $user, string $groupId)
    {
        return $user->hasPermissionTo($groupId . '.invoice.store');
    }

    /**
     * Xác định hóa đơn có thể được cập nhật bởi người dùng không
     *
     * @param      \App\Models\User     $user     The user
     * @param      string               $groupId  The group identifier
     */
    public function update(User $user, string $groupId)
    {
        return $user->hasPermissionTo($groupId . '.invoice.update');
    }

    /**
     * Xác định hóa đơn có thể được xóa bởi người dùng không
     *
     * @param      \App\Models\User     $user     The user
     * @param      \App\Models\Invoice  $invoice  The invoice
     * @param      string               $groupId  The group identifier
     */
    public function delete(User $user, string $groupId)
    {
        return $user->hasPermissionTo($groupId . '.invoice.delete');
    }

    /**
     * Xác định hóa đơn có thể được xác nhận thanh toán bởi người dùng không
     *
     *
     * @param      \App\Models\User  $user     The user
     * @param      string            $groupId  The group identifier
     *
     * @return     <type>            ( description_of_the_return_value )
     */
    public function checkout(User $user, string $groupId)
    {
        return $user->hasPermissionTo($groupId . '.invoice.checkout');
    }

    /**
     * Kiểm tra xem hoá đơn có đang tồn tại request_status hay ko
     *
     * @param  User    $user
     * @param  Invoice $invoice
     * @return bool
     */
    public function requestable(User $user, Invoice $invoice)
    {
        return $invoice->request_status !== Invoice::NONE_REQUEST;
    }
}
