<?php

namespace App\Policies;

use App\Models\User;
use Maklad\Permission\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Kiểm tra xem permission có thuộc về group hay không
     *
     * @param  User       $user
     * @param  Permission $permission
     * @param  string     $groupId
     * @return bool
     */
    public function belongsToGroup(User $user, Permission $permission, string $groupId)
    {
        return $permission->group_id === $groupId;
    }
}
