<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Shift;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShiftPolicy
{
    use HandlesAuthorization;

    /**
     * Kiểm tra ca làm việc có thuộc về group hay không
     *
     * @param  User   $user
     * @param  Shift  $shift
     * @param  string $groupId
     * @return bool
     */
    public function belongsToGroup(User $user, Shift $shift, string $groupId)
    {
        return $shift->group_id === $groupId;
    }
}
