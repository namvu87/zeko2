<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Timekeeping;
use Illuminate\Auth\Access\HandlesAuthorization;

class TimekeepingPolicy
{
    use HandlesAuthorization;

    /**
     * Kiểm tra timekeeping có thực sự thuộc về group hay không
     *
     * @param  User        $user
     * @param  Timekeeping $timekeeping
     * @param  string      $groupId
     * @return bool
     */
    public function belongsToGroup(User $user, Timekeeping $timekeeping, string $groupId)
    {
        return $timekeeping->group_id === $groupId;
    }
}
