<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\RequirementService;
use App\Models\Requirement;

class RequirementServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('requirement', function() {
            return new RequirementService(new Requirement);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
