<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Restaurant\GroupMenuService;
use App\Models\GroupMenu;

class GroupMenuServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('groupMenu', function() {
            return new GroupMenuService(new GroupMenu);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
