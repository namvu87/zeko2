<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Restaurant\TableService;
use App\Models\Table;

class TableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('table', function() {
            return new TableService(new Table);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
