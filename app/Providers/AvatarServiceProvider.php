<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Uploads\Avatar;

class AvatarServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('avatar', function($app) {
            return new Avatar($app['image'], $app['user']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
