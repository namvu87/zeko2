<?php

namespace App\Providers;

use Laravel\Passport\Passport;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;

use App\Models\Group;
use App\Models\GroupMenu;
use App\Models\Good;
use App\Models\Place;
use App\Models\Table;
use App\Models\Invoice;
use App\Models\ImportInvoice;
use App\Models\InvoiceForm;
use App\Models\Shift;
use App\Models\User;
use App\Models\Timekeeping;
use App\Models\ReturnedInvoice;
use App\Models\Requirement;

use App\Policies\RolePolicy;
use App\Policies\PermissionPolicy;

use App\Policies\GroupMenuPolicy;
use App\Policies\GoodPolicy;
use App\Policies\PlacePolicy;
use App\Policies\TablePolicy;
use App\Policies\InvoicePolicy;
use App\Policies\ImportInvoicePolicy;
use App\Policies\InvoiceFormPolicy;
use App\Policies\ShiftPolicy;
use App\Policies\UserPolicy;
use App\Policies\TimekeepingPolicy;
use App\Policies\ReturnedInvoicePolicy;
use App\Policies\GroupPolicy;
use App\Policies\RequirementPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Role::class => RolePolicy::class,
        Permission::class => PermissionPolicy::class,
        Shift::class => ShiftPolicy::class,
        User::class => UserPolicy::class,
        Timekeeping::class => TimekeepingPolicy::class,
        GroupMenu::class => GroupMenuPolicy::class,
        Good::class => GoodPolicy::class,
        Place::class => PlacePolicy::class,
        Table::class => TablePolicy::class,
        Invoice::class => InvoicePolicy::class,
        ImportInvoice::class => ImportInvoicePolicy::class,
        InvoiceForm::class => InvoiceFormPolicy::class,
        ReturnedInvoice::class => ReturnedInvoicePolicy::class,
        Group::class => GroupPolicy::class,
        Requirement::class => RequirementPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::personalAccessClientId(config('passport.personal_access.client_id'));
    }
}
