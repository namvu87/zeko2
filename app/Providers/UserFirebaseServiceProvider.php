<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Users\Firebase;

class UserFirebaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('userFirebase', function($app) {
            return new Firebase($app['user']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
