<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Users\Role;

class UserRoleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('userRole', function($app) {
            return new Role($app['user']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
