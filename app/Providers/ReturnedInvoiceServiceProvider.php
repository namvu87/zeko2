<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Restaurant\ReturnedInvoiceService;
use App\Models\ReturnedInvoice;

class ReturnedInvoiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('returnedInvoice', function() {
            return new ReturnedInvoiceService(new ReturnedInvoice);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
