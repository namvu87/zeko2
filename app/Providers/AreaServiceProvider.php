<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\AreaService;
use App\Models\Area;

class AreaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('area', function() {
            return new AreaService(new Area);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
