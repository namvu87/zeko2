<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ShiftService;
use App\Models\Shift;

class ShiftServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('shift', function() {
            return new ShiftService(new Shift);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
