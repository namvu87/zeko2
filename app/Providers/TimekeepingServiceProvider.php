<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\TimekeepingService;
use App\Models\Timekeeping;

class TimekeepingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('timekeeping', function($app) {
            return new TimekeepingService(new Timekeeping, $app['user']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
