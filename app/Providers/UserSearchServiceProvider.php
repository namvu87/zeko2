<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Users\Search;
use App\Models\User;

class UserSearchServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('userSearch', function($app) {
            return new Search(new User, $app['role']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
