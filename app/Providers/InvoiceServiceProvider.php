<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Restaurant\InvoiceService;
use App\Models\Invoice;

class InvoiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('invoice', function($app) {
            return new InvoiceService(
                new Invoice,
                $app['group'],
                $app['goods']
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
