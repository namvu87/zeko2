<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Restaurant\PlaceService;
use App\Models\Place;

class PlaceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('place', function($app) {
            return new PlaceService(new Place, $app['table']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
