<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CommuneService;
use App\Models\Commune;

class CommuneServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('commune', function() {
            return new CommuneService(new Commune);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
