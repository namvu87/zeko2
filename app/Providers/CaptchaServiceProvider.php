<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CaptchaService;
use Illuminate\Config\Repository;
use Illuminate\Hashing\BcryptHasher as Hasher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;
use Illuminate\Session\Store as Session;

class CaptchaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('captcha', function() {
            return new CaptchaService(
                new Filesystem,
                new Repository,
                new ImageManager,
                new Session,
                new Hasher,
                new Str
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
