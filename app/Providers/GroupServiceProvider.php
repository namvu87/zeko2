<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\GroupService;
use App\Models\Group;

class GroupServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('group', function() {
            return new GroupService(new Group);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
