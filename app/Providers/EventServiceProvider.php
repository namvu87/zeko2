<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\GroupCreated' => [
            'App\Listeners\GenerateGroupPermissions',
            'App\Listeners\GenerateGroupRoles'
        ],
        // 'App\Events\Restaurant\ManageSale\PurchaseInvoiceCheckouted' => [
            // 'App\Listeners\Restaurant\ManageSale\CalculatingInventoryNumber'
        // ],
        'App\Events\Restaurant\GoodRunningOut' => [
            'App\Listeners\Restaurant\NotifyGoodRunningOut'
        ],
        'App\Events\NotificationCreated' => [
            'App\Listeners\NotificationCreated'
        ],
        'App\Events\Restaurant\InvoiceCreated' => [
            'App\Listeners\Restaurant\InvoiceCreated'
        ],
        'App\Events\Restaurant\InvoiceUpdated' => [
            'App\Listeners\Restaurant\InvoiceUpdated'
        ],
        'App\Events\Restaurant\InvoiceDestroyed' => [
            'App\Listeners\Restaurant\InvoiceDestroyed'
        ],
        'App\Events\Restaurant\RequestAccepted' => [
            'App\Listeners\Restaurant\RequestAccepted'
        ],
        'App\Events\Restaurant\InvoiceMemberAdded' => [
            'App\Listeners\Restaurant\InvoiceMemberAdded'
        ],
        'App\Events\Restaurant\ConversationUpdated' => [
            'App\Listeners\Restaurant\ConversationUpdated'
        ],
        'App\Events\MemberShipChanged' => [
            'App\Listeners\MemberShipChanged'
        ],
        'App\Events\ShiftChanged' => [
            'App\Listeners\ShiftChanged'
        ],
        'App\Events\SystemNotify' => [
            'App\Listeners\SystemNotify'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
