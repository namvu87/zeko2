<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SearchService;
use App\Models\Province;
use App\Models\Restaurant;
use App\Models\RestaurantImage;

class SearchServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('search', function() {
            return new SearchService(
                new Restaurant,
                new Province,
                new RestaurantImage
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
