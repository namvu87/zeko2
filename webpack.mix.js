const mix = require('laravel-mix')
// const webpack = require('webpack')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    output: {
        publicPath: '/',
        chunkFilename: 'js/chunks/[name].js'
    },
    module: {
        rules: [
            {
                test: /\.(graphql|gql)$/,
                loader: 'graphql-tag/loader'
            }
        ]
    },
    resolve: {
        alias: {
            '@js': __dirname + '/resources/js',
            '@apollo': __dirname + '/resources/js/apollo',
            '@components': __dirname + '/resources/js/components'
        }
    },
    // plugins: [
    // new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    // ]
})

mix.js('resources/js/app.js', 'public/js')
    .version()

// .sass('resources/sass/app.scss', 'public/css')
// .sass('resources/sass/restaurants/manage-sale.scss', 'public/css/restaurants/manage-sale.css')
// .sass('resources/sass/guide.scss', 'public/css')

if (mix.inProduction()) {
    mix.version()
}