function url(path) {
    return window.Laravel.app_url + '/api/' + path
}

export const LOGIN = url('login')
export const LOGOUT = url('logout')
export const REGISTER = url('register')
export const VERIFY_EMAIL = url('email/verify')
export const RESEND_VERIFY_EMAIL = url('email/resend')
export const FORGET_PASSWORD = url('forget-password')
export const GET_USER_INFO = url('user')
export const UPDATE_FIREBASE_TOKEN = url('firebase/push')
export const GET_CAPTCHA = url('captcha')
export const CHANGE_CURRENT_COMPANY = url('company/change')
export const INIT_VERIFY_PHONE_NUMBER = url('phone-number/verification/init')

export const PROFILE = url('user')

export const GET_PROFILE_COMPANY = url('company/profile')
export const UPDATE_PROFILE_COMPANY = url('company/profile/update')
export const CHANGE_PASSWORD_COMPANY = url('company/profile/change-password')

export const USER_UPDATE_OVERVIEW = url('user/profile/update/overview')
export const USER_UPDATE_ACCOUNT = url('user/profile/update/account')
export const USER_UPDATE_AVATAR = url('user/profile/update/avatar')

export const GET_NOTIFICATION = url('notification/list')
export const GET_NOTIFICATION_BY_ID = url('notification/detail')
export const ACTIVE_NOTIFICATION = url('notification/active')
export const RESET_NOTIFICATION = url('notification/reset')

export const GET_REQUIREMENT = url('requirement/list')
export const GET_REQUIREMENT_BY_ID = url('requirement/detail')
export const ACTIVE_REQUIREMENT = url('requirement/handle/active')
export const RESET_REQUIREMENT = url('requirement/handle/reset')
export const HANDLE_JOIN_GROUP = url('requirement/handle/join-group')
export const HANDLE_TIME_KEEPING_ADDITION = url('requirement/handle/timekeeping-addition')
export const HANDLE_TAKE_LEAVE = url('requirement/handle/take-leave')
export const HANDLE_SWITCH_SHIFT = url('requirement/handle/switch-shift')

export const GET_REQUIREMENT_TIMEKEEPING = url('requirement/timekeeping')

export const GET_HOME_TIMEKEEPING = url('home/timekeeping')
export const GET_HOME_RESTAURANT = url('home/restaurant')
export const GET_DATA_HOME_RESTAURANT_TODAY = url('home/restaurant-today')

export const GET_HOME_COMPANY_PARENT = url('home-company-parent')
export const GET_HOME_COMPANY_BRANCH = url('home-company-branch')
export const GET_BRANCH_COMPANIES = url('company/branch')
export const ENABLE_COMPANY = url('company/branch/enable')
export const DISABLE_COMPANY = url('company/branch/disable')
// export const STORE_BRANCH_COMPANY              = url('company/branch/store');
// export const GET_COMPANY_EDITED                = url('company/branch/edit');
// export const UPDATE_COMPANY                    = url('company/branch/update');

export const GET_LIST_USERS = url('employee/list')
export const SEARCH_USERS_IN_GROUP = url('employee/search')
export const PULL_USER_FROM_GROUP = url('employee/pull')
export const GET_USER_BY_ID = url('employee/detail')
export const GET_USER_BY_PHONE_NUMBER = url('employee/search-by-phone')
export const ADD_USER_TO_GROUP = url('employee/add')

export const GET_ROLES_AND_USER_BY_GROUP_ID = url('permissions/roles')
export const UPDATE_ROLE_PERMISSIONS = url('permissions/update-role-permissions')
export const GET_ROLES_AND_PERMISSION_BY_GROUP_ID = url('permissions/roles-permissions')

export const ASSIGN_ROLE = url('permissions/roles-assign')
export const REVOKE_ROLE = url('permissions/roles-revoke')
export const GET_USER_IN_GROUP_OUT_ROLE = url('employee/search-role-out')

export const GET_SHIFT_LIST = url('shift/list')
export const STORE_SHIFT = url('shift/store')
export const EDIT_SHIFT = url('shift/edit')
export const UPDATE_SHIFT = url('shift/update')
export const DELETE_SHIFT = url('shift/delete')
export const USER_LIST_SHIFT = url('shift/user-list')
export const USER_SEARCH_SHIFT = url('shift/user-search')
export const DELETE_USER_SHIFT = url('shift/user-delete')
export const USER_ADD_SHIFT = url('shift/user-add')
export const USER_ADD_SEARCH_SHIFT = url('shift/user-add-search')
export const USERS_ADD_SHIFT = url('shift/user-add/many')
export const USER_GROUPS_LIST = url('user/groups-list')
export const USER_SHIFTS_LIST = url('user/shifts-list')

export const LIST_TIME_KEEPING = url('timekeeping/list')
export const DETAIL_TIME_KEEPING = url('timekeeping/detail')
export const REPORT_TIME_KEEPING = url('report/timekeeping-group')
export const REPORT_TIME_KEEPING_EMPLOYEE = url('report/timekeeping-employee')

//Restaurant
export const LIST_PLACE = url('place/list')
export const STORE_PLACE = url('place/store')
export const EDIT_PLACE = url('place/edit')
export const UPDATE_PLACE = url('place/update')
export const DELETE_PLACE = url('place/delete')

export const LIST_TABLE = url('table/list')
export const STORE_TABLE = url('table/store')
export const EDIT_TABLE = url('table/edit')
export const UPDATE_TABLE = url('table/update')
export const DELETE_TABLE = url('table/delete')
export const SEARCH_TABLE = url('table/search')

export const LIST_INVOICE = url('invoice/list')
export const DETAIL_INVOICE = url('invoice/detail')
export const DISABLE_INVOICE = url('invoice/disable')

export const LIST_RETURNED_INVOICE = url('returned-invoice/list')
export const DETAIL_RETURNED_INVOICE = url('returned-invoice/detail')
export const DISABLE_RETURNED_INVOICE = url('returned-invoice/disable')

export const REPORT_END_DAY = url('report/end-day')
export const REPORT_SALE = url('report/sale')
export const REPORT_GOODS = url('report/goods')

export const LIST_MENU = url('group-menus/list')
export const CREATE_MENU = url('group-menus/create')
export const STORE_MENU = url('group-menus/store')
export const EDIT_MENU = url('group-menus/edit')
export const UPDATE_MENU = url('group-menus/update')
export const DELETE_MENU = url('group-menus/delete')
export const SELECT_MENU = url('group-menus/select')

export const LIST_GOODS = url('goods/list')
export const LIST_IMPORTED_GOODS = url('goods/list/imported')
export const UPDATE_LIST_MENU = url('goods/update/add-menus')
export const UPDATE_GOODS_IMAGES = url('goods/update/images')
export const STORE_IMPORTED_GOODS = url('goods/create/imported-good')
export const STORE_PROCESSED_GOODS = url('goods/create/processed-good')
export const DETAIL_GOODS = url('goods/detail')
export const UPDATE_GOODS = url('goods/update')
export const ENABLE_GOODS = url('goods/update/enable')
export const DISABLE_GOODS = url('goods/update/disable')
export const DELETE_GOODS = url('goods/delete')
export const DELETE_IMAGE_GOODS = url('goods/update/delete-image')
export const SEARCH_GOODS_TO_IMPORT = url('goods/search-import-invoice')
export const INACTIVE_GOODS = url('goods/update/inactive')
export const CHECK_INFO_GOODS_UPLOAD = url('goods/check-file')

export const STORE_IMPORT_INVOICE = url('imported-invoice/create')
export const LIST_IMPORT_INVOICE = url('imported-invoice/list')
export const PAY_IMPORT_INVOICE = url('imported-invoice/pay')

//Master
export const GET_HOME_MASTER = url('home-master')

export const GET_SLIDER_LIST = url('master/slider')
export const GET_SLIDER_CREATE = url('master/slider/create')
export const STORE_SLIDER = url('master/slider/store')
export const GET_SLIDER_EDIT = url('master/slider/edit')
export const UPDATE_SLIDER = url('master/slider/update')
export const DELETE_SLIDER = url('master/slider/delete')
export const GET_SLIDER_CHANGE_STATUS = url('master/slider/change-status')

export const GET_TAG_LIST = url('master/tag')
export const GET_TAG_CREATE = url('master/tag/create')
export const STORE_TAG = url('master/tag/store')
export const GET_TAG_EDIT = url('master/tag/edit')
export const UPDATE_TAG = url('master/tag/update')
export const DELETE_TAG = url('master/tag/delete')
export const RESTORE_TAG = url('master/tag/restore')
export const GET_TAG_CHANGE_STATUS = url('master/tag/change-status')

export const GET_NEWS_LIST = url('master/news/list')
export const GET_NEWS_CREATE = url('master/news/create')
export const STORE_NEWS = url('master/news/store')
export const GET_NEWS_EDIT = url('master/news/edit')
export const UPDATE_NEWS = url('master/news/update')
export const DELETE_NEWS = url('master/news/delete')
export const RESTORE_NEWS = url('master/news/restore')
export const CHANGE_STATUS_NEWS = url('master/news/change-status')

export const GET_CUSTOMER_CARE_LIST = url('master/customer-care/list')
export const GET_CUSTOMER_CARE_CREATE = url('master/customer-care/create')
export const STORE_CUSTOMER_CARE = url('master/customer-care/store')
export const GET_CUSTOMER_CARE_EDIT = url('master/customer-care/edit')
export const UPDATE_CUSTOMER_CARE = url('master/customer-care/update')
export const DELETE_CUSTOMER_CARE = url('master/customer-care/delete')

export const GET_CONTACT_INFORMATION = url('master/contact')
export const STORE_CONTACT_INFORMATION = url('master/contact/store')

export const LIST_PRODUCT_MASTER = url('master/product')
export const CREATE_PRODUCT_MASTER = url('master/product/create')
export const STORE_PRODUCT_MASTER = url('master/product/store')
export const EDIT_PRODUCT_MASTER = url('master/product/edit')
export const UPDATE_PRODUCT_MASTER = url('master/product/update')
export const DELETE_PRODUCT_MASTER = url('master/product/delete')

export const GET_COMPANY_LIST_MASTER = url('master/company')
export const GET_COMPANY_EDIT_MASTER = url('master/company/edit')
export const UPDATE_COMPANY_MASTER = url('master/company/update')

export const CHANGE_LANGUAGE = url('change-language')

export const GET_AREAS = url('areas')
export const GET_COMMUNE_BY_AREA_ID = url('communes')

export const CREATE_GROUP = url('group/create')
export const DELETE_PROPERTY = url('group/update/delete-property')
export const UPDATE_GROUP = url('group/update')

// Invoice Form
export const LIST_INVOICE_FROM = url('form-invoice/list')
export const STORE_INVOICE_FORM = url('form-invoice/create')
export const UPDATE_INVOICE_FORM = url('form-invoice/update')
export const DELETE_INVOICE_FORM = url('form-invoice/delete')
export const LIST_INVOICE_FORM_BY_TYPE = url('form-invoice/list/type')

// Manage Sale
export const LIST_PLACES = url('manage-sale/sale/places')
export const LIST_PLACES_WITH_TABLES = url('manage-sale/sale/places-with-tables')
export const LIST_GROUP_MENUS_AND_GOODS = url('manage-sale/sale/group-menus-and-goods')
export const LIST_INVOICE_AND_TABLES = url('manage-sale/sale/invoices-and-tables')
export const LIST_EMPLOYEES = url('manage-sale/sale/employees')
export const SET_GOOD_STATUS = url('manage-sale/sale/good/set-status')
export const LIST_PURCHASED_INVOICES = url('manage-sale/sale/invoice/get-purchased')
export const SEARCH_PURCHASED_INVOICES = url('manage-sale/sale/invoice/search-purchased')
export const SET_INVOICE_TABLES = url('manage-sale/sale/invoice/set-invoice-tables')
export const SET_INVOICE_NOTE = url('manage-sale/sale/invoice/set-note')
export const SET_INVOICE_DISCOUNT = url('manage-sale/sale/invoice/set-discount')
export const CREATE_PURCHASE_INVOICE = url('manage-sale/sale/invoice/create-purchase-invoice')
export const CHANGE_STATUS_GOOD_INVOICES = url('manage-sale/sale/invoice/change-status-good')
export const COMBINE_TABLES = url('manage-sale/sale/invoice/combine-tables')
export const COMBINE_INVOICES = url('manage-sale/sale/invoice/combine-invoices')
export const CHECKOUT_PURCHASE_INVOICE = url('manage-sale/sale/invoice/checkout-purchase-invoice')
export const CHECKOUT_RETURN_INVOICE = url('manage-sale/sale/invoice/checkout-return-invoice')
export const UPDATE_GOODS_INVOICE = url('manage-sale/sale/invoice/update-goods')
export const UPDATE_STATUS_GOODS = url('manage-sale/sale/invoice/update-status-goods')
export const DELETE_INVOICE = url('manage-sale/sale/invoice/delete')
export const DELETE_ITEM_INVOICE = url('manage-sale/sale/invoice/delete-item')

export const ACCEPT_REQUEST_CUSTOMER = url('manage-sale/sale/notify/accept')
// Order History
export const LIST_TABLES = url('manage-sale/sale/tables/all')
export const LIST_INVOICES = url('manage-sale/sale/invoices/all')
export const GET_INVOICE = url('manage-sale/sale/invoice/get')

export const UPDATE_UNITS = url('group/update/units')
