export default {
    data() {
        return {
            config: {
                language: this.$i18n.locale,
                height: 'calc(100vh - 250px)',
                toolbarGroups: [
                    { name: 'document', groups: ['mode', 'document'] },
                    { name: 'tools' },
                    '/',
                    { name: 'clipboard', groups: ['clipboard', 'undo'] },
                    { name: 'insert' },
                    '/',
                    { name: 'others' },
                    '/',
                    {
                        name: ['basicstyles', 'paragraph'],
                        groups: ['basicstyles', 'list', 'indent', 'blocks', 'align'],
                    },
                    '/',
                    { name: 'styles' },
                    { name: 'colors' }
                ],
                entities_latin: false,
                disallowedContent: 'table[cellspacing,cellpadding]',
            },
        }
    }
}