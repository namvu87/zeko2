import {RESTAURANT_SERVICE, TIMEKEEPING_SERVICE} from "../constants";

export default {
    methods: {
        getAddress(group) {
            let address = []
            address.push(group.address, group.commune.name, group.area.name)
            return address.join(', ')
        },
        nameService(service) {
            switch (service) {
                case TIMEKEEPING_SERVICE:
                    return this.$t('word.time_keeping')
                case RESTAURANT_SERVICE:
                    return this.$t('word.restaurant')
                default:
                    return ''
            }
        },
    }
}