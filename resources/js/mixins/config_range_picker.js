export default {
    data() {
        return {
            ranges: false,
            maxDate: '',
            locale: {
                applyLabel: this.$t('word.apply'),
                cancelLabel: this.$t('word.cancel'),
                daysOfWeek: this.$t('config.day_of_week'),
                monthNames: this.$t('config.month_names'),
                firstDay: 1
            },
        }
    }
}