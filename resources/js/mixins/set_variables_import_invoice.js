import { formatPrice } from '../helpers/handle_function.js'
import { INVOICE_FORM_TYPE_IMPORT_INVOICE, DISCOUNT_TYPE_FIXED } from '../constants.js'
import { convertToDateTime } from './../helpers/handle_date_time.js'
import handleUnit from './../mixins/handle_unit.js'

export default {
    mixins: [handleUnit],
    methods: {
        getValueInvoice(invoice) {
            let data = { Goods: [] }
            let outTable = Object.keys(this.$t(`config.invoice_form.variables.${INVOICE_FORM_TYPE_IMPORT_INVOICE}.out_table`))
            let inTable = Object.keys(this.$t(`config.invoice_form.variables.${INVOICE_FORM_TYPE_IMPORT_INVOICE}.in_table`))
            data[outTable[0]] = invoice.code
            data[outTable[1]] = invoice.recipient.last_name + ' ' + invoice.recipient.first_name
            data[outTable[2]] = convertToDateTime(invoice.created_at)
            data[outTable[3]] = formatPrice(invoice.total)
            data[outTable[4]] = invoice.note
            for (let good of invoice.goods) {
                let goods = {}
                goods[inTable[0]] = good.code
                goods[inTable[1]] = good.name
                goods[inTable[2]] = formatPrice(good.count) + this.getUnit(good.unit)
                goods[inTable[3]] = formatPrice(good.price)
                goods[inTable[4]] = formatPrice(good.discount) + (good.discount_type == DISCOUNT_TYPE_FIXED ? 'VND' : '%')
                goods[inTable[5]] = formatPrice(this.getMoney(good))
                data.Goods.push(goods)
            }
            return data
        },
        getMoney(good) {
            if (good.discount_type == DISCOUNT_TYPE_FIXED) {
                return good.count * (good.price - good.discount)
            } else {
                return good.count * good.price * (1 - good.discount / 100)
            }
        }
    }
}