import { UPLOAD_IMAGE_EDITOR, GET_IMAGE_EDITOR, DELETE_IMAGE_EDITOR } from '../api_master'
import token from './../helpers/token'

export default {
    data() {
        return {
            config: {
                height: 300,
                language: this.$i18n.locale,
                charCounterMax: 15000,
                requestHeaders: {
                    Authorization: 'Bearer ' + token.getItem(),
                    'X-Requested-With': 'XMLHttpRequest',
                    'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
                },
                imageUploadParam: 'image',
                imageUploadURL: UPLOAD_IMAGE_EDITOR,
                imageMaxSize: 3 * 1024 * 1024,
                imageManagerPreloader: '/images/loading.gif',
                imageManagerLoadURL: GET_IMAGE_EDITOR,
                imageManagerDeleteURL: DELETE_IMAGE_EDITOR,
                imageManagerDeleteMethod: 'DELETE',
                toolbarButtons: [
                    'fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript',
                    'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle',
                    'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL',
                    'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo',
                    'embedly', 'insertFile', 'insertTable', '|', 'emoticons',
                    'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|',
                    'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo']
            }
        }
    }
}