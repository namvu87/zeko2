export default {
    data() {
        return {
            menus: [],
        }
    },
    methods: {
        handleGroupMenus(menus) {
            this.menus = []
            for (let menu of menus) {
                this.menus.push(menu)
                let childMenus = menu.childs
                for (let child1 of childMenus) {
                    this.menus.push(child1)
                    let child2Menus = child1.childs
                    for (let child2 of child2Menus) {
                        this.menus.push(child2)
                    }
                }
            }
        }
    }
}