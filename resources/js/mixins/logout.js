import LOGOUT_MUTATION from './../apollo/mutation/auth/logout.gql'

export default {
    methods: {
        logoutExec() {
            this.$apollo.mutate({
                mutation: LOGOUT_MUTATION,
                variables: {
                    firebaseToken: this.$store.getters.firebaseToken
                }
            }).finally(() => {
                this.$apollo.provider.clients.default.resetStore()
                this.$store.dispatch('removeCurrentUser').then(() => {
                    this.$router.push('/login').then(() => {
                        this.$store.dispatch('removeCurrentGroup')
                        this.$store.dispatch('resetTotalGroups')
                    })
                })
            })
        }
    }
}