export default {
    data() {
        return {
            images: [],
            imagesPreview: [],
            limitImage: 5
        }
    },
    computed: {
        maxUploaded() {
            return this.limitImage
        }
    },
    methods: {
        async processImage(e) {
            let files = e.target.files || e.dataTransfer.files
            for (let file of files) {
                await this.createImage(file)
            }
        },
        async createImage(file) {
            let reader = new FileReader()
            reader.onload = (e) => {
                let result = e.target.result
                if (result.slice(0, 10) == 'data:image' &&
                    (this.limitImage === 0 || this.maxUploaded > 0)) {
                    this.images.push(file)
                    this.imagesPreview.push(result)
                }
            }
            reader.readAsDataURL(file)
        },
    }
}
