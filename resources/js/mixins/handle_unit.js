export default {
    computed: {
        units() {
            return this.$t('config.goods.units')
        }
    },
    methods: {
        getUnit(unit) {
            return (this.units[unit] || unit).toLowerCase()
        }
    }
}