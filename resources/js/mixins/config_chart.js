export default {
    data() {
        return {
            configChart: {
                vAxis: {
                    format: 'short',
                    minValue: 0,
                    baseline: 0,
                    textStyle: {
                        fontSize: 12,
                        color: '#4c5356'
                    }
                },
                hAxis: {
                    baseline: 0,
                    minValue: 0,
                    textPosition: 'out',
                    textStyle: {
                        fontSize: 12,
                        color: '#4c5356'
                    }
                },
                tooltip: {
                    textStyle: { color: '#27a1f2' },
                    showColorCode: true,
                },
                animation: {
                    startup: true,
                    duration: 1000,
                    easing: 'out',
                    alwaysOutside: true
                },
                chartArea: {
                    top: 30,
                    bottom: 20
                },
                height: 450,
                legend: {
                    position: 'top',
                    alignment: 'center',
                    maxLines: 4,
                    textStyle: {
                        fontSize: 13,
                        color: '#27a1f2'
                    }
                },
            }
        }
    }
}