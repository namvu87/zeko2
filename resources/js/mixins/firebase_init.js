import UPDATE_FIREBASE_TOKEN from './../apollo/mutation/user/update_firebase_token.gql'
const firebase = require('firebase/app')
require('firebase/messaging')

export default {
    created() {
        Notification.requestPermission().then(permission => {
            if (permission !== 'granted') {
                this.$awn.warning('Yêu cầu quyền thông báo để nhận thông báo thời gian thực từ hệ thống')
                return
            }
            const config = {
                apiKey: 'AIzaSyDCX9_Mb8rlxNg3XYpXUWaO1ZO4qChrKdk',
                authDomain: 'zeko-production.firebaseapp.com',
                databaseURL: 'https://zeko-production.firebaseio.com',
                projectId: 'zeko-production',
                storageBucket: 'zeko-production.appspot.com',
                messagingSenderId: '431317895221',
                appId: '1:431317895221:web:3036537b83994515'
            }
            firebase.initializeApp(config)
    
            const messaging = firebase.messaging()
            messaging.usePublicVapidKey(
                'BK997VQ_YrPUN15DpsJ4jcwj8prFPOqLtqIFhliy4HjZFfgpXu_gYgZa5Khr81Vafev1V8LKC0uvSvO62N5SUC0'
            )
    
            // Request permission Firebase FCM
            messaging.getToken().then(currentToken => {
                console.log(currentToken)
                if (currentToken) {
                    this.updateFirebaseToken(currentToken)
                } else {
                    this.$awn.warning('Không thể truy xuất mã truy cập hợp lệ')            
                }
            })
    
            // Token refresh
            messaging.onTokenRefresh(() => {
                messaging.getToken().then(refreshToken => {
                    console.log(refreshToken)
                    this.updateFirebaseToken(refreshToken)
                }).catch(() => {
                    this.$awn.warning('Không thể làm mới mã truy cập')
                })
            })
    
            messaging.onMessage(payload => {
                this.handleNotification(payload)
            })
        })
    },
    methods: {
        updateFirebaseToken(token) {
            this.$apollo.mutate({
                mutation: UPDATE_FIREBASE_TOKEN,
                variables: {
                    firebaseToken: token
                }
            }).then(() => {
                this.$store.dispatch('setCurrentFirebase', token)
            })
        },
        handleNotification(payload) {
            console.log(payload)
        }
    }
}