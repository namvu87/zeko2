import { formatPrice } from './../helpers/handle_function.js'
import { INVOICE_FORM_TYPE_RETURNED_INVOICE } from './../constants.js'
import { convertToDateTime } from './../helpers/handle_date_time.js'

export default {
    methods: {
        getValueInvoice(invoice) {
            let data = {}
            let config = this.$t(`config.invoice_form.variables.${INVOICE_FORM_TYPE_RETURNED_INVOICE}`)
            let outTable = Object.keys(config.out_table)
            let inTable = Object.keys(config.in_table)
            data[config.loop] = []
            data[outTable[0]] = invoice.code
            data[outTable[1]] = invoice.invoice ? invoice.invoice.code : ''
            data[outTable[2]] = this.$store.getters.groupName
            data[outTable[3]] = this.$store.getters.groupAddress
            data[outTable[4]] = convertToDateTime(Date.now())
            data[outTable[5]] = invoice.creator_name
            data[outTable[6]] = formatPrice(invoice.total)
            data[outTable[7]] = invoice.note || ''
            for (let good of invoice.goods) {
                let goods = {}
                goods[inTable[0]] = good.code
                goods[inTable[1]] = good.name
                goods[inTable[2]] = formatPrice(good.count)
                goods[inTable[3]] = formatPrice(good.price)
                goods[inTable[4]] = formatPrice(good.price * good.count)
                data[config.loop].push(goods)
            }
            return data
        },
    }
}