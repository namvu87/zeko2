export default {
    data() {
        return {
            file: null,
            imagesPreview: [],
            extensions: ['xlsx', 'csv']
        }
    },
    methods: {
        processFiles(e) {
            let files = e.target.files || e.dataTransfer.files
            if (this.extensions.includes(files[0].name.split('.')
                .pop())) {
                this.file = files[0]
            } else {
                this.$awn.alert(this.$t('word.note_upload_excel'))
            }
        },
    }
}