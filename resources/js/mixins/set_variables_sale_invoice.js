import { formatPrice } from './../helpers/handle_function.js'
import { INVOICE_FORM_TYPE_SALE_INVOICE, DISCOUNT_TYPE_FIXED, DISCOUNT_TYPE_PERCENT } from './../constants'
import { convertToDateTime } from './../helpers/handle_date_time.js'

export default {
    methods: {
        getValueInvoice(invoice) {
            let data = {}
            let config = this.$t(`config.invoice_form.variables.${INVOICE_FORM_TYPE_SALE_INVOICE}`)
            let outTable = Object.keys(config.out_table)
            let inTable = Object.keys(config.in_table)
            data[config.loop] = []
            data[outTable[0]] = invoice.code
            data[outTable[1]] = this.$store.getters.groupName
            data[outTable[2]] = this.$store.getters.groupAddress
            data[outTable[3]] = convertToDateTime(Date.now())
            data[outTable[4]] = this.tablesName
            data[outTable[5]] = invoice.recipient_name
            data[outTable[6]] = formatPrice(invoice.subtotal)
            data[outTable[7]] = formatPrice(this.getDiscount(invoice))
            data[outTable[8]] = formatPrice(invoice.total)
            data[outTable[9]] = invoice.note || ''
            for (let good of invoice.goods) {
                let goods = {}
                let priceDiscount = this.getPriceGoodsDiscount(good)
                goods[inTable[0]] = good.code
                goods[inTable[1]] = good.name
                goods[inTable[2]] = formatPrice(good.count)
                goods[inTable[3]] = formatPrice(good.price)
                goods[inTable[4]] = formatPrice(priceDiscount) + (good.discount_type == DISCOUNT_TYPE_PERCENT ? '%' : '')
                goods[inTable[5]] = formatPrice(parseInt(good.price - priceDiscount))
                goods[inTable[6]] = formatPrice((parseInt((good.price - priceDiscount) * good.count)))
                data[config.loop].push(goods)
            }
            return data
        },
        getDiscount(invoice) {
            if (invoice.discount) {
                if (invoice.discount_type == DISCOUNT_TYPE_FIXED) {
                    return invoice.discount
                } else {
                    return invoice.discount + '%'
                }
            }
            return 0
        },
        getPriceGoodsDiscount(good) {
            if (good.discount && good.discount_type == DISCOUNT_TYPE_PERCENT) {
                return good.price * good.discount / 100
            }
            return good.discount || 0
        }
    }
}