require('./bootstrap')
window.Vue = require('vue')
import i18n from './i18n'
import store from './store'
import router from './router'
window.bus = new Vue()
import apolloProvider from './apollo'
import Initialized from './initialized'
Vue.use(Initialized)

import VueAWN from 'vue-awesome-notifications'
require('vue-awesome-notifications/dist/styles/style.css')
Vue.use(VueAWN, {
    labels: {
        success: '',
        warning: '',
        info: '',
        alert: ''
    },
    icons: {
        success: 'check',
        warning: 'exclamation-triangle',
        info: 'info-circle',
        alert: 'exclamation-triangle'
    },
    position: 'bottom-left'
})
import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
Vue.use(Viewer)

import vClickOutside from 'v-click-outside'
Vue.use(vClickOutside)

Vue.config.devtools = true
Vue.config.productionTip = false
Vue.config.warnHandler = function () {
    return
}

Vue.prototype.$baseUrl = function (link) {
    return window.Laravel.app_url + '/' + link
}
Vue.prototype.$avatar = function (url, size = 'x1') {
    if (url) {
        return process.env.MIX_AWS_URL + url[size]
    }
    return window.Laravel.app_url + `/images/avatar_${size}.png`
}
Vue.prototype.$coverImage = function (url) {
    if (url) {
        return window.Laravel.app_url + '/' + url['x1']
    }
    let index = Math.floor((Math.random() * 5) + 1)
    return window.Laravel.app_url + `/images/heading-bg/slide${index}.jpg`
}
Vue.prototype.$urlImage = process.env.MIX_AWS_URL
Vue.prototype.$asset = function (url) {
    return process.env.MIX_AWS_URL + url
}
Vue.prototype.$getFullName = (firstName, lastName) => {
    if (i18n.locale === 'vi') {
        return lastName + ' ' + firstName
    }
    return firstName + ' ' + lastName
}

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

import Clients from './components/passport/Clients.vue'
import AuthorizedClients from './components/passport/AuthorizedClients.vue'
import PersonalAccessTokens from './components/passport/PersonalAccessTokens.vue'

Vue.component('passport-clients', Clients)
Vue.component('passport-authorized-clients', AuthorizedClients)
Vue.component('passport-personal-access-tokens', PersonalAccessTokens)

require('froala-editor/js/froala_editor.pkgd.min')
require('froala-editor/js/languages/vi')
require('froala-editor/css/froala_editor.pkgd.min.css')
require('froala-editor/css/froala_style.min.css')

import VueHtml2Canvas from 'vue-html2canvas'
Vue.use(VueHtml2Canvas)

import Avatar from 'vue-avatar'
Vue.component('avatar', Avatar)

import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala)

import fullscreen from 'vue-fullscreen'
Vue.use(fullscreen)

import VSelect from 'vue-select'
Vue.component('v-select', VSelect)

import MultiSelect from 'vue-multiselect'
Vue.component('multiselect', MultiSelect)

import App from './components/app.vue'

/**
 * Custom directive `check` to checking permission
 */
Vue.directive('check', {
    inserted(el, binding, vnode) {
        let permissions = binding.value.permission.split(',')
        let keepEl = false
        for (let permission of permissions) {
            if (store.getters.permissions &&
                store.getters.permissions.includes(permission)) {
                keepEl = true
                break
            }
        }
        if (!keepEl) {
            vnode.elm.parentElement.removeChild(vnode.elm)
        }
    }
})
Vue.directive('check-role', {
    inserted(el, binding, vnode) {
        let checkedRoles = binding.value.role.split(',')
        if (!store.getters.currentRole ||
            !checkedRoles.includes(store.getters.currentRole)) {
            vnode.elm.parentElement.removeChild(vnode.elm)
        }
    }
})

new Vue({
    created() {
        window.axios.interceptors.response.use(null, error => {
            let statusCode = error.response.status

            if (statusCode === 401 && this.$route.name !== 'login') {
                this.$store.dispatch('removeCurrentUser').finally(() => {
                    this.$store.dispatch('removeCurrentGroup')
                    this.$router.push('/login')
                })
            }
            return Promise.reject(error)
        })
    },
    i18n,
    router,
    store,
    apolloProvider,
    render: h => h(App)
}).$mount('#app')
