import * as mutation from './../mutation'
export default {
    state: {
        error: false,
        success: false,
        warning: false,
        info: false,
        message: null
    },
    mutations: {
        [mutation.SHOW_ERROR_TOAST] (state, payload) {
            state.error = true
            state.message = payload.message
        },
        [mutation.HIDDEN_ERROR_TOAST] (state) {
            state.error = false
            state.message = null
        },
        [mutation.SHOW_WARNING_TOAST] (state, payload) {
            state.warning = true
            state.message = payload.message
        },
        [mutation.HIDDEN_WARNING_TOAST] (state) {
            state.warning = false
            state.message = null
        },
        [mutation.SHOW_SUCCESS_TOAST] (state, payload) {
            state.success = true
            state.message = payload.message
        },
        [mutation.HIDDEN_SUCCESS_TOAST] (state) {
            state.success = false
            state.message = null
        },
        [mutation.SHOW_INFO_TOAST] (state, payload) {
            state.info = true
            state.message = payload.message
        },
        [mutation.HIDDEN_INFO_TOAST] (state) {
            state.info = false
            state.message = null
        }
    },
    actions: {
        showSuccess: ({commit}, message) => {
            commit(mutation.SHOW_SUCCESS_TOAST, {message: message})
            setTimeout(function() {
                commit(mutation.HIDDEN_SUCCESS_TOAST)
            }, 5000)
        },
        showWarning: ({commit}, message) => {
            commit(mutation.SHOW_WARNING_TOAST, {message: message})
            setTimeout(function() {
                commit(mutation.HIDDEN_WARNING_TOAST)
            }, 5000)
        },
        showError: ({commit}, message) => {
            commit(mutation.SHOW_ERROR_TOAST, {message: message})
            setTimeout(function() {
                commit(mutation.HIDDEN_ERROR_TOAST)
            }, 5000)
        },
        showInfo: ({commit}, message) => {
            commit(mutation.SHOW_INFO_TOAST, {message: message})
            setTimeout(function() {
                commit(mutation.HIDDEN_INFO_TOAST)
            }, 5000)
        },
    }
}