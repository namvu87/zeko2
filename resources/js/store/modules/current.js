import {
    SET_CURRENT_GROUP,
    SET_CURRENT_SERVICE,
    SET_CURRENT_FIREBASE_TOKEN,
    UPDATE_REQUIREMENT,
    SET_TOTAL_GROUPS,
    SET_CURRENT_PERMISSIONS
} from './../mutation.js'
import { MASTER_SERVICE } from '../../constants.js'
import currentGroup from './../../helpers/current_group.js'

export default {
    state: {
        group: {},
        service: null,
        totalGroups: [],
        firebaseToken: '',
        permissions: []
    },
    mutations: {
        [SET_CURRENT_GROUP] (state, payload) {
            state.group = Object.assign({}, payload)
        },
        [SET_CURRENT_SERVICE] (state, payload) {
            state.service = payload
        },
        [SET_CURRENT_FIREBASE_TOKEN] (state, payload) {
            state.firebaseToken = payload
        },
        [UPDATE_REQUIREMENT] (state, count) {
            state.group.number_requirement = count
        },
        [SET_TOTAL_GROUPS] (state, groups) {
            state.totalGroups = Array.from(groups)
        },
        [SET_CURRENT_PERMISSIONS] (state, permissions) {
            state.permissions = permissions
        }
    },
    getters: {
        group: state => {
            return state.group || null
        },
        groupName: state => {
            return state.group.name || ''
        },
        groupAddress: state => {
            if (state.group) {
                const address = state.group.address || ''
                const commune = (typeof state.group.commune !== 'undefined') ? state.group.commune.name : ''
                const area = (typeof state.group.area !== 'undefined') ? state.group.area.name : ''
                const position = [address, commune, area]
                if (address || commune || area) {
                    return position.toString()
                }
            }
        },
        groupId: state => {
            return state.group.id || ''
        },
        firebaseToken: state => {
            return state.firebaseToken
        },
        requirements: state => {
            return state.group.number_requirement || 0
        },
        serviceType: state => {
            return parseInt(state.service) || null
        },
        currentRole: state => {
            return state.group.role_current_user ? state.group.role_current_user.name : ''
        },
        permissions: state => {
            return state.permissions
        },
    },
    actions: {
        setCurrentGroup: ({ commit }, data) => {
            commit(SET_CURRENT_GROUP, data)
            commit(SET_CURRENT_SERVICE, data.service_type)
        },
        removeCurrentGroup: ({ commit }) => {
            currentGroup.removeCurrentGroup()
            commit(SET_CURRENT_GROUP, {})
            commit(SET_CURRENT_SERVICE, null)
        },
        setCurrentFirebase: ({ commit }, token) => {
            commit(SET_CURRENT_FIREBASE_TOKEN, token)
        },
        updateRequirement: ({ commit, state }, count) => {
            commit(UPDATE_REQUIREMENT, count)
            currentGroup.setCurrentGroup(state.group)
        },
        setMasterGroup: ({ commit }) => {
            const masterGroup = { service_type: MASTER_SERVICE }
            currentGroup.setCurrentGroup(masterGroup)
            commit(SET_CURRENT_GROUP, masterGroup)
            commit(SET_CURRENT_SERVICE, MASTER_SERVICE)
        },
        setTotalGroups: ({ commit }, groups) => {
            commit(SET_TOTAL_GROUPS, groups)
        },
        resetTotalGroups: ({ commit }) => {
            commit(SET_TOTAL_GROUPS, [])
            currentGroup.removeTotalGroups()
        },
        setCurrentPermissions: ({ commit }, permissions) => {
            commit(SET_CURRENT_PERMISSIONS, permissions)
        }
    }
}