import { SET_INTERNET_STATUS } from './../mutation.js'
export default {
    state: {
        status: true
    },
    mutations: {
        [SET_INTERNET_STATUS] (state, payload) {
            state.status = payload
        }
    },
    getters: {
        internet: state => {
            return state.internet
        }
    },
    actions: {
        setInternet: ({commit}, status) => {
            commit(SET_INTERNET_STATUS, status)
        }
    }
}