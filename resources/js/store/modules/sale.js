import {
    SET_CONFIG,
    SET_TABLES,
    UPDATE_STATUS_TABLE,
    SET_CURRENT_LEFT_SCREEN,
    IS_LOADING_LEFT_SCREEN,
    SET_DISPLAY_ORDER,
    IS_CHECKOUT,
    SET_INVOICES
} from './../mutation.js'
import { TABLE_STATUS_AVAILABLE } from './../../constants.js'
export default {
    state: {
        config: {
            auto_next_screen: true,
            place_number: 4,
            places_manage: [],
            print_invoice: true,
            notify_sound: true,
        },
        users: [
            {
                _id: 'KH0001',
                name: 'Trần Thị Liên'
            },
            {
                _id: 'KH0002',
                name: 'Nguyễn Thị Dung'
            }
        ],
        // Chứa danh sách tất cả các bàn của nhà hàng
        tables: [],
        invoices: [],
        currentLeftScreen: 'restaurant-screen',
        isLoadingLeftScreen: true,
        isShowOrder: true,
        isCheckout: false
    },
    getters: {
        users: state => {
            return state.users
        },
        tables: state => {
            return state.tables
        },
        invoices: state => {
            return state.invoices
        },
        currentLeftScreen: state => {
            return state.currentLeftScreen
        },
        isLoadingLeftScreen: state => {
            return state.isLoadingLeftScreen
        },
        isShowOrder: state => {
            return state.isShowOrder
        },
        isCheckout: state => {
            return state.isCheckout
        },
        config: state => {
            return state.config
        },
        autoNextScreen: state => {
            return state.config.auto_next_screen
        },
        placeNumber: state => {
            return state.config.place_number
        },
        placesManage: state => {
            return state.config.places_manage
        },
        printInvoice: state => {
            return state.config.print_invoice
        },
        notifySound: state => {
            return state.config.notify_sound
        }
    },
    mutations: {
        [SET_CONFIG] (state, payload) {
            state.config = JSON.parse(JSON.stringify(payload))
        },
        [SET_TABLES] (state, payload) {
            state.tables = JSON.parse(JSON.stringify(payload))
        },
        [SET_INVOICES] (state, invoices) {
            state.invoices = invoices
        },
        [UPDATE_STATUS_TABLE] (state, payload) {
            const iTable = state.tables.findIndex(table => table.id === payload.id)
            if (iTable !== -1) {
                state.tables[iTable].status = payload.status
            }
        },
        [SET_CURRENT_LEFT_SCREEN] (state, payload) {
            state.currentLeftScreen = payload
        },
        [SET_DISPLAY_ORDER] (state, payload) {
            state.isShowOrder = payload
        },
        [IS_LOADING_LEFT_SCREEN] (state, payload) {
            state.isLoadingLeftScreen = payload
        },
        [IS_CHECKOUT] (state, payload) {
            state.isCheckout = payload
            if (payload) {
                state.currentLeftScreen = 'invoice-screen'
            } else {
                state.currentLeftScreen = 'restaurant-screen'
            }
        }
    },
    actions: {
        setConfig: ({commit}, data) => {
            commit(SET_CONFIG, data)
        },
        setTables: ({commit}, data) => {
            commit(SET_TABLES, data)
        },
        setInvoices: ({ commit }, invoices) => {
            commit(SET_INVOICES, invoices)
        },
        setEmptyTables: ({commit}, tableIds) => {
            tableIds.forEach(tableId => {
                commit(UPDATE_STATUS_TABLE, {
                    id: tableId,
                    status: TABLE_STATUS_AVAILABLE
                })
            })
        },
        updateTableStatus: ({commit}, data) => {
            commit(UPDATE_STATUS_TABLE, data)
        },
        updateTablesStatus: ({commit}, data) => {
            const tables = data.tables.map(table => {
                return {
                    id: table.table_id,
                    status: data.status
                }
            })
            tables.forEach(table => {
                commit(UPDATE_STATUS_TABLE, table)
            })
        },
        setCurrentLeftScreen: ({commit}, data) => {
            commit(SET_CURRENT_LEFT_SCREEN, data)
        },
        setDisplayOrder: ({commit}, data) => {
            commit(SET_DISPLAY_ORDER, data)
        },
        isLoadingLeftScreen: ({commit}, data) => {
            commit(IS_LOADING_LEFT_SCREEN, data)
        },
        isCheckout: ({commit}, data) => {
            commit(IS_CHECKOUT, data)
        }
    }
}