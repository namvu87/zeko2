import {
    SET_USER_INFO,
    SET_AUTHENTICATED,
    SET_VERIFIED,
    UPDATE_AVATARS,
    LOGOUT,
    ADD_NEW_GROUP,
    UPDATE_GROUP,
    UPDATE_NOTIFICATION,
    INCREMENT_REQUIREMENT
} from './../mutation'
import token from './../../helpers/token'
import user from './../../helpers/user'
import axios from 'axios'

export default {
    state: {
        authenticated: false,
        user: {}
    },
    mutations: {
        [SET_AUTHENTICATED](state) {
            state.authenticated = true
        },
        [SET_VERIFIED](state, payload) {
            state.user.email_verified = payload
        },
        [SET_USER_INFO](state, payload) {
            state.user = payload
        },
        [UPDATE_AVATARS](state, payload) {
            state.user.avatars = payload
        },
        [ADD_NEW_GROUP](state, payload) {
            if (state.user) {
                if (Array.isArray(state.user.member_groups)) {
                    state.user.member_groups.push(payload)
                } else {
                    state.user.member_groups = [payload]
                }
            }
        },
        [UPDATE_GROUP](state, payload) {
            if (state.user) {
                let index = state.user.member_groups.findIndex(obj => obj.id == payload.id)
                state.user.member_groups[index] = payload
            }
        },
        [UPDATE_NOTIFICATION](state, count) {
            state.user.notify_count = count
        },
        [INCREMENT_REQUIREMENT](state, groupId) {
            if (state.user) {
                state.user.member_groups.map(obj => {
                    if (obj.id == groupId) {
                        obj.number_requirement += 1
                    }
                    return obj
                })
            }
        },
        [LOGOUT](state) {
            state.authenticated = false
            state.user = {}
        }
    },
    getters: {
        authenticated: state => {
            return state.authenticated
        },
        verified: state => {
            return state.user.verified !== undefined ? state.user.verified : true
        },
        phoneVerified: state => {
            return state.user.phone_verified !== undefined ? state.user.phone_verified : true
        },
        id: state => {
            return state.user.id
        },
        avatar: state => {
            return state.user.avatars || {}
        },
        address: state => {
            return state.user.address
        },
        firstname: state => {
            return state.user.first_name
        },
        lastname: state => {
            return state.user.last_name
        },
        fullname: state => {
            return Vue.prototype.$getFullName(state.user.first_name, state.user.last_name)
        },
        email: state => {
            return state.user.email
        },
        phoneNumber: state => {
            return state.user.phone_number
        },
        groups: state => {
            return state.user.member_groups || []
        },
        notifications: state => {
            return state.user.notify_count || 0
        },
        isMaster: state => {
            return state.user.is_master
        }
    },
    actions: {
        setAuth: ({ commit }, data) => {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.access_token
            Echo.connector.options.auth.headers['Authorization'] = 'Bearer ' + data.access_token

            token.setItem(data.access_token)
            user.setItem(data.user)
            commit(SET_USER_INFO, data.user)
            commit(SET_AUTHENTICATED)
        },
        setVerified: ({ commit, state }, data) => {
            commit(SET_VERIFIED, data)
            user.setItem(state.user)
        },
        setUserInfo: async ({ commit }, userData) => {
            commit(SET_USER_INFO, userData)
            user.setItem(userData)
            commit(SET_AUTHENTICATED)
        },
        updateAvatar: ({ commit }, avatars) => {
            commit(UPDATE_AVATARS, avatars)
            var userInfo = user.getItem()
            userInfo.avatars = avatars
            user.setItem(userInfo)
        },
        addNewGroup: ({ commit, state }, data) => {
            commit(ADD_NEW_GROUP, data)
            user.setItem(state.user)
        },
        updateGroup: ({ commit, state }, data) => {
            commit(UPDATE_GROUP, data)
            user.setItem(state.user)
        },
        removeCurrentUser: ({ commit }) => {
            token.removeItem()
            user.removeItem()
            commit(LOGOUT)
        },
        updateNotification: ({ commit, state }, count) => {
            commit(UPDATE_NOTIFICATION, count)
            user.setItem(state.user)
        },
        incrementRequirement: ({ commit, state }, groupId) => {
            commit(INCREMENT_REQUIREMENT, groupId)
            user.setItem(state.user)
        }
    }
}