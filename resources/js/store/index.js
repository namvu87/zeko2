import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import auth from './modules/auth'
import current from './modules/current'
import internet from './modules/internet.js'

export default new Vuex.Store({
    modules: {
        auth,
        current,
        internet
    },
    strict: process.env.NODE_ENV !== 'production'
})