// Toast
export const SHOW_ERROR_TOAST            = 'SHOW_ERROR_TOAST'
export const HIDDEN_ERROR_TOAST          = 'HIDDEN_ERROR_TOAST'
export const SHOW_WARNING_TOAST          = 'SHOW_WARNING_TOAST'
export const HIDDEN_WARNING_TOAST        = 'HIDDEN_WARNING_TOAST'
export const SHOW_SUCCESS_TOAST          = 'SHOW_SUCCESS_TOAST'
export const HIDDEN_SUCCESS_TOAST        = 'HIDDEN_SUCCESS_TOAST'
export const SHOW_INFO_TOAST             = 'SHOW_INFO_TOAST'
export const HIDDEN_INFO_TOAST           = 'HIDDEN_INFO_TOAST'
export const SHOW_MODAL                  = 'SHOW_MODAL'

// Auth
export const SET_AUTHENTICATED           = 'SET_AUTHENTICATED'
export const SET_USER_INFO               = 'SET_USER_INFO'
export const SET_VERIFIED                = 'SET_VERIFIED'
export const UPDATE_AVATARS              = 'UPDATE_AVATARS'
export const LOGOUT                      = 'LOGOUT'
export const SET_CURRENT_PERMISSIONS     = 'SET_CURRENT_PERMISSIONS'
export const UPDATE_NOTIFICATION         = 'UPDATE_NOTIFICATION'
export const UPDATE_REQUIREMENT          = 'UPDATE_REQUIREMENT'
export const INCREMENT_REQUIREMENT       = 'INCREMENT_REQUIREMENT'

//Init
export const SET_CURRENT_GROUP           = 'SET_CURRENT_GROUP'
export const SET_CURRENT_SERVICE         = 'SET_CURRENT_SERVICE'
export const SET_CURRENT_FIREBASE_TOKEN  = 'SET_CURRENT_FIREBASE_TOKEN'
export const ADD_NEW_GROUP               = 'ADD_NEW_GROUP'
export const SET_TOTAL_GROUPS            = 'SET_TOTAL_GROUPS'

// Sale
export const SET_CONFIG                  = 'SET_CONFIG'
export const SET_CONFIG_AUTO_NEXT_SCREEN = 'SET_CONFIG_AUTO_NEXT_SCREEN'
export const SET_CONFIG_PRINT_INVOICE    = 'SET_CONFIG_PRINT_INVOICE'
export const SET_TABLES                  = 'SET_TABLES'
export const SET_INVOICES                = 'SET_INVOICES'
export const SET_CURRENT_LEFT_SCREEN     = 'SET_CURRENT_LEFT_SCREEN'
export const IS_LOADING_LEFT_SCREEN      = 'IS_LOADING_LEFT_SCREEN'
export const SET_DISPLAY_ORDER           = 'SET_DISPLAY_ORDER'
export const IS_CHECKOUT                 = 'IS_CHECKOUT'
export const ADD_COMPONENT_LOADED        = 'ADD_COMPONENT_LOADED'
export const UPDATE_STATUS_TABLE         = 'UPDATE_STATUS_TABLE'
export const SET_NOTE_CONTENT            = 'SET_NOTE_CONTENT'

export const UPDATE_GROUP                = 'UPDATE_GROUP'
export const UPDATE_UNITS                = 'UPDATE_UNITS'

export const SET_INTERNET_STATUS         = 'SET_INTERNET_STATUS'
