module.exports = {
    root: true,
    env: {
        node: true,
        es6: true
    },
    'extends': [
        'plugin:vue/recommended',
    ],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'vue/html-quotes': ['error', 'double'],
        'quotes': ['error', 'single'],
        'vue/html-closing-bracket-newline': ['error', {
            'singleline': 'never',
            'multiline': 'never'
        }],
        'vue/max-attributes-per-line': ['error', {
            'singleline': 4,
            'multiline': {
              'max': 3,
              'allowFirstLine': true
            }
        }],
        'vue/html-self-closing': ['error', {
            'html': {
                'void': 'never',
                'normal': 'never',
                'component': 'never'
            },
            'svg': 'never',
            'math': 'never'
        }],
        'vue/singleline-html-element-content-newline': 'off',
        'vue/multiline-html-element-content-newline': 'off',
        'vue/no-v-html': 'off',
        'no-unused-vars': ['error', {
            "vars": "all",
            "args": "after-used",
            "ignoreRestSiblings": false
        }],
        'semi' : ['error', 'never'],
        'newline-per-chained-call': 'off',
        'linebreak-style': ['error', 'unix'],
        'max-len': ['error', 120]
    },
    'overrides': [
        {
            'files': ['*.vue'],
            'rules': {
                'indent': 'off'
            }
        }
    ],
    parserOptions: {
        parser: 'babel-eslint',
        ecmaVersion: 6
    },
    globals: {
        'axios': true,
        'Echo': true,
        '_': true,
        '$': true,
        'CKEDITOR': true,
        'bus': true,
        'Vue': true
    },
}