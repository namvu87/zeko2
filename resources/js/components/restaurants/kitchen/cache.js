import SALE_INVOICES_QUERY from './../../../apollo/query/sale/sale_invoices.gql'

export default {
    data() {
        return {
            client: this.$apollo.provider.defaultClient
        }
    },
    computed: {
        groupId() {
            return this.$store.state.current.group.id
        },
        invoicesQuery() {
            return {
                query: SALE_INVOICES_QUERY,
                variables: {
                    groupId: this.groupId
                }
            }
        }
    },
    methods: {
        /**
         * Khi người dùng đặt món, dữ liệu nhận được từ web socket không thể cập nhật vào
         * bộ đệm của apollo client vì ko được chuẩn hoá, buộc phải cập nhật thủ công
         *
         * @from Socket
         * 
         * @param  {Object} invoice
         * @return {void}
         */
        invoiceCreated(invoice) {
            invoice.__typename = 'invoice'
            invoice.is_draft = false
            invoice.is_changed = false
            invoice.is_return = false
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices.push(invoice)
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Cập nhật hoá đơn
         * 
         * @from Socket
         *
         * @param  {invoice: Object, type: Int} payload
         * @return {void}
         */
        invoiceUpdated(invoice) {
            invoice.__typename = 'invoice'
            invoice.is_draft = false
            invoice.is_changed = false
            invoice.is_return = false
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            const index = invoicesData.sale_invoices.findIndex(item => {
                return item.id === invoice.id
            })
            if (index !== -1) {
                invoicesData.sale_invoices[index] = invoice
                this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
            }
        },
        /**
         * Huỷ đơn, thanh toán
         * 
         * @from Socket, Http Response
         *
         * @param {String} invoiceId
         */
        invoiceDestroyed(invoiceId) {
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.filter(item => {
                return item.id !== invoiceId
            })
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        }
    }
}