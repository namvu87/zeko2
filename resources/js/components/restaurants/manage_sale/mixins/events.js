import mixinGeneral from './general.js'
import { DISCOUNT_TYPE_FIXED } from '../../../../constants.js'
import SALE_INVOICES_QUERY from './../../../../apollo/query/sale/sale_invoices.gql'

export default {
  mixins: [ mixinGeneral ],
  computed: {
    groupId() {
      return this.$store.getters.groupId
    },
    placeIds() {
      return this.$store.getters.placesManage
    },
  },
  methods: {
    // Thay đổi hóa đơn hiện tại bằng hóa đơn có chưa tableId
    changeCurrentInvoiceByTableId(tableId) {
      const index = this.invoices.findIndex(invoice => invoice.table_ids.includes(tableId))
      if (index != -1) {
        this.changeCurrentInvoice(this.invoices[index])
      }
    },
    // Tạo hóa đơn mua hàng mới
    createPurchaseInvoice(table = null) {
      let tableIds = []
      if (table !== null) {
        tableIds = [table.table_id]
      }
      const identifier = `HD${this.indexInvoice.toString().padStart(5, '0')}`
      this.indexInvoice ++
      const invoice = {
        id: identifier,
        code: identifier,
        table_ids: tableIds,
        goods: [],
        status: '',
        request_status: '',
        note: '',
        created_at: '',
        discount: 0,
        discount_type: DISCOUNT_TYPE_FIXED,
        notifications: [],
        is_draft: true,    // client-data - Hóa đơn chưa được lưu vào DB
        is_changed: false, // client-data
        is_return: false,
        __typename: 'invoice'
      }
      this.initEmptyInvoice(invoice)
      this.changeCurrentInvoice(invoice)

      // Tạo nhanh hóa đơn
      if (table === null) {
        this.selectInvoiceTables()
      }
      bus.$emit('successfullyCreatedPurchaseInvoice')
    },
    /**
     * Tạo hóa đơn trả hàng
     * 
     * @param {Object} invoice 
     */
    createReturnInvoice(invoice) {
      invoice.__typename = 'invoice'
      invoice.is_return = true
      invoice.is_changed = false
      invoice.is_draft = false
      invoice.notifications = []
      invoice.goods.map(item => {
        return item.return_count = 0
      })

      const client = this.$apollo.provider.defaultClient
      const query = {
        query: SALE_INVOICES_QUERY,
        variables: {
          groupId: this.groupId,
          placeIds: this.placeIds
        }
      }
      const data = client.readQuery(query)
      data.sale_invoices.unshift(invoice)
      client.writeQuery({ ...query, data: data })
      
      this.currentInvoice = invoice
      this.currentInvoiceId = invoice.id
    }
  },
  mounted() {
    // Cập nhật lại danh sách hóa đơn khi thay đổi khu vực quản lý
    this.$nextTick(() => {
      // Thay đổi hóa đơn có table_ids chứa tableId
      bus.$on('changeCurrentInvoiceByTableId', this.changeCurrentInvoiceByTableId)
      // Tạo mới order
      bus.$on('createInvoiceWithTableSelected', this.createPurchaseInvoice)
      // Thiết lập số lượng món ăn đặt
      bus.$on('setItemNumber', this.setItemNumberCurrentInvoice)
      // Thiết lập lại trạng thái các món ăn trong hóa đơn mới được được thay đổi trạng thái
      // Tạo hóa đơn trả hàng
      bus.$on('returnInvoiceSelected', this.createReturnInvoice)
      // Chuyển sang màn hình thanh toán sau khi kiếm đồ
      bus.$on('checkoutInvoiceAfterCheckItems', this.performCheckout)
    })
  },
  beforeDestroy() {
    bus.$off('changeCurrentInvoiceByTableId', this.changeCurrentInvoiceByTableId)
    bus.$off('createInvoiceWithTableSelected', this.createPurchaseInvoice)
    bus.$off('setItemNumber', this.setItemNumberCurrentInvoice)
    bus.$off('returnInvoiceSelected', this.createReturnInvoice)
    bus.$off('checkoutInvoiceAfterCheckItems', this.performCheckout)
  }
}