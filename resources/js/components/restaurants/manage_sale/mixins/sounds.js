import { Howl } from 'howler'

export default {
  name: 'Sounds',
  data() {
    return {
      notifyCustomer: null
    }
  },
  computed: {
    isNotifySound() {
      return this.$store.getters.notifySound
    }
  },
  methods: {
    playNotifyCustomer() {
      if (this.isNotifySound) {
        this.notifyCustomer.play()
      }
    }
  },
  mounted() {
    this.$nextTick(() => {
      this.notifyCustomer = new Howl({
        src: '/sound/notify-from-customer.mp3',
      })
    })
  }
}