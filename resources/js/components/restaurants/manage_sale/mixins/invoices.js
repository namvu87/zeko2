// Chứa những methods gọi modal và emit từ modal trả về trong scr_invoices
import mixinGeneral from './general.js'
import {
  INVOICE_CREATE_REQUEST_STATUS,
  INVOICE_ADDITION_REQUEST_STATUS,
  INVOICE_PURCHASE_REQUEST,
  DISCOUNT_TYPE_FIXED,
} from './../../../../constants.js'
import { formatPrice, priceGoodAfterDiscount } from './../../../../helpers/handle_function.js'
import { momentTime, momentFromNow } from './../../../../helpers/helper_manage_sale.js'
import UPDATE_GOOD_STATUS from './../../../../apollo/mutation/sale/update_good_status.gql'

export default {
  mixins: [mixinGeneral],
  methods: {
    getStatusClassInvoice(invoice) {
      if (invoice.is_draft) return { color: '#ffffff' }
      switch (invoice.request_status) {
        case INVOICE_CREATE_REQUEST_STATUS:
          return {
            color: '#1b7dd5'
          }
        case INVOICE_ADDITION_REQUEST_STATUS:
          return {
            color: '#ffc800'
          }
        case INVOICE_PURCHASE_REQUEST:
          return {
            color: '#e74c3c'
          }
        default:
          return {
            color: '#4CB050'
          }
      }
    },
    momentTime(dateTime) {
      return momentTime(dateTime)
    },
    momentFromNow(dateTime) {
      return momentFromNow(dateTime)
    },
    invoiceType(tableIds) {
      return tableIds.length > 0 ? '- ' + this.invoicePosition(tableIds)
              : this.$t('manage_sale.modal.call_bring_home')
    },
    invoiceInformation(invoice) {
      const info = invoice.code + ' ' + this.invoiceType(invoice.table_ids || [])
      return invoice.is_return === true ? `${this.$t('manage_sale.invoices.returns')} - ${info}` : info
    },
    // Thay đổi trạng thái món ăn
    setItemsStatus(status) {
      // Hóa đơn nháp, chưa được khởi tạo
      if (this.isDraftInvoice) {
        return this.$awn.alert(
          this.$t('manage_sale.client_notify.you_need_initialize_invoice_before_changing_status')
        )
      }
      // Chưa cập nhật món ăn
      if (this.hasItemsAdded) {
        return this.$awn.alert(
          this.$t('manage_sale.client_notify.you_need_update_good_when_adding_to_invoice_before_changing_status')
        )
      }

      this.$apollo.mutate({
        mutation: UPDATE_GOOD_STATUS,
        variables: {
          groupId: this.groupId,
          invoiceId: this.currentInvoice.id,
          targetStatus: status,
          goodItems: this.itemsSelected
        }
      }).then(() => {
        this.$awn.success(this.$t('word.updated'))
      }).catch(() => {
        this.$awn.alert(this.$t('manage_sale.client_notify.cant_action'))
      })
    },
    formatPrice(amount) {
      return formatPrice(amount)
    },
    getPriceAfterDiscount(item) {
      return priceGoodAfterDiscount(item)
    },
    /**
     * Hiển thị modal thiết lập số lượng items trả lại
     * @param {Object} item 
     */
    showModalNumberItemReturn(item) {
      bus.$emit('modal', {
        component: 'm-s-number-goods-invoice',
        backdrop: 'static',
        payload: {
          count: item.return_count.toString(),
          count_max: item.count,
          goods_id: item.id,
          invoice_id: this.currentInvoice.id
        }
      })
    },
    showModalNoteInvoice() {
      bus.$emit('modal', {
        component: 'm-s-note-invoice',
        backdrop: 'static',
        payload: {
          order_id: this.currentInvoice.id,
          content: this.currentInvoice.note,
          is_return: this.currentInvoice.is_return
        }
      })
    },
    showModalTablesInvoice() {
      const tableIds = this.currentInvoice.table_ids || []
      bus.$emit('modal', {
        component: 'm-s-tables-invoice',
        backdrop: 'static',
        payload: {
          invoice_id: this.currentInvoice.id,
          invoice_code: this.currentInvoice.code,
          table_ids: tableIds,
          is_draft: this.currentInvoice.is_draft
        }
      })
    },
    showModalDiscountDish(item) {
      bus.$emit('modal', {
        component: 'm-s-discount-goods',
        backdrop: 'static',
        payload: {
          goods: item,
          invoiceId: this.currentInvoice.id
        }
      })
    },
    showModalDiscountInvoice() {
      bus.$emit('modal', {
        component: 'm-s-discount-invoice',
        backdrop: 'static',
        payload: {
          invoiceId: this.currentInvoice.id,
          discount: this.currentInvoice.discount || '0',
          discount_type: this.currentInvoice.discount_type || DISCOUNT_TYPE_FIXED,
          subtotal_order: this.subtotalCurrentInvoice
        }
      })
    },
    showModalMatchingInvoicesTables() {
      bus.$emit('modal', {
        component: 'm-s-matching-invoices-tables',
        backdrop: 'static',
        payload: {
          invoices: this.invoices,
          invoice_id: this.currentInvoice.id
        }
      })
    },
    showModalShortcut() {
      bus.$emit('modal', {
        component: 'm-s-shortcut',
        backdrop: 'static'
      })
    },
    showModalSetting() {
      bus.$emit('modal', {
        component: 'm-s-setting',
        backdrop: 'static'
      })
    },
    showModalCheckGoods() {
      if (this.currentInvoice.goods.length === 0) {
        return this.$awn.alert(
          this.$t(
            'manage_sale.client_notify.order_can_not_check_goods_because_order_have_not_goods',
            { invoice_code: this.currentInvoice.code }
          )
        )
      }
      if (!this.checkDeliveredItems()) {
        return this.$awn.alert(
          this.$t(
            'manage_sale.client_notify.order_can_not_check_goods_because_item_not_deliver',
            { invoice_code: this.currentInvoice.code }
          )
        )
      }
      const invoice = JSON.parse(JSON.stringify(this.currentInvoice))
      bus.$emit('modal', {
        component: 'm-s-check-goods',
        backdrop: 'static',
        payload: {
          invoice: invoice,
          invoice_position: this.invoicePosition(invoice.table_ids)
        }
      })
    },
    // Xác nhận xóa 1 item
    confirmDeleteItem(goodsId, orderItem) {
      bus.$emit('modal', {
        component: 'm-s-confirm',
        backdrop: 'static',
        size: 'sm',
        payload: {
          invoiceId: this.currentInvoice.id,
          goodsId: goodsId,
          orderCreatedAt: orderItem.created_at,
          orderCount: orderItem.count,
          title: this.$t('manage_sale.modal.confirm_delete_good'),
          type: 'confirm-delete-dish'
        }
      })
    },
    // Xác nhận xóa hóa đơn
    confirmDeleteInvoice() {
      bus.$emit('modal', {
        component: 'm-s-confirm',
        backdrop: 'static',
        size: 'sm',
        payload: {
          title: this.$t('manage_sale.modal.confirm_delete_order'),
          type: 'confirm-delete-order',
          isReturn: this.currentInvoice.is_return,
          invoiceId: this.currentInvoice.id
        }
      })
    }
  }
}
