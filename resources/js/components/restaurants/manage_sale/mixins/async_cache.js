import SALE_INVOICES_QUERY from './../../../../apollo/query/sale/sale_invoices.gql'
import SALE_PLACES_QUERY from './../../../../apollo/query/sale/sale_places.gql'
// import LOCAL_NOTIFICATIONS_QUERY from './../../../../apollo/query/sale/local_notifications.gql'
import { TABLE_STATUS, GOODS_STATUS } from './../../../../constants.js'
import moment from 'moment'

export default {
    data() {
        return {
            client: this.$apollo.provider.defaultClient
        }
    },
    computed: {
        groupId() {
            return this.$store.state.current.group.id
        },
        placeIds() {
            return this.$store.getters.placesManage
        },
        invoicesQuery() {
            return {
                query: SALE_INVOICES_QUERY,
                variables: {
                    groupId: this.groupId,
                    placeIds: this.placeIds
                }
            }
        },
        placesQuery() {
            return  {
                query: SALE_PLACES_QUERY,
                variables: {
                    groupId: this.groupId,
                    placeIds: this.placeIds
                }
            }
        },
        tables() {
            return this.$store.getters.tables
        },
        invoices() {
            return this.$store.getters.invoices
        }
    },
    methods: {
        /**
         * Khi người dùng đặt món, dữ liệu nhận được từ web socket không thể cập nhật vào
         * bộ đệm của apollo client vì ko được chuẩn hoá, buộc phải cập nhật thủ công
         *
         * @from Socket
         * 
         * @param  {Object} invoice
         * @return {void}
         */
        invoiceCreated(invoice) {
            invoice.__typename = 'invoice'
            invoice.is_draft = false
            invoice.is_changed = false
            invoice.is_return = false
            const tableIds = invoice.table_ids || []

            // Thêm hoá đơn vào danh sách
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices.unshift(invoice)
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })

            // Thay đổi trạng thái bàn
            const placesData = this.client.readQuery(this.placesQuery)
            placesData.sale_places = placesData.sale_places.map(place => {
                place.tables = place.tables.map(table => {
                    if (tableIds.includes(table.id)) {
                        table.status = TABLE_STATUS.ACTING
                    }
                    return table
                })
                return place
            })
            this.client.writeQuery({ ...this.placesQuery, data: placesData })
        },
        /**
         * Cập nhật hoá đơn
         * 
         * @from Socket
         *
         * @param  {invoice: Object, type: Int} payload
         * @return {void}
         */
        invoiceUpdated(payload) {
            let invoice = payload.invoice
            invoice.__typename = 'invoice'
            invoice.is_draft = false
            invoice.is_changed = false
            invoice.is_return = false

            // Phải cập nhật trạng thái bàn trước
            switch (payload.type) {
                case 45:
                // case EVENTS_TYPE.TABLES_CHANGED:
                    this.TABLES_CHANGED(invoice)
                    break
                case 46:
                // case EVENTS_TYPE.TABLES_COMBINED:
                    this.TABLES_COMBINED(invoice, payload.invoice_ids || [])
                    break
                case 47:
                // case EVENTS_TYPE.INVOICES_COMBINE:
                    this.INVOICES_COMBINE(payload.invoice_ids || [])
                    break
                default:
                    break
            }

            // Cập nhật invoices
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            const index = invoicesData.sale_invoices.findIndex(invoiceItem => {
                return invoiceItem.id === invoice.id
            })
            invoicesData.sale_invoices[index] = invoice
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Huỷ đơn, thanh toán
         * 
         * @from Socket, Http Response
         *
         * @param {String} invoiceId
         */
        invoiceDestroyed(invoiceId) {
            // Cập nhật trạng thái trống cho bàn
            const invoice = this.invoices.filter(item => item.id === invoiceId)[0]

            const tableIds = invoice && invoice.table_ids ? invoice.table_ids : []
            const placesData = this.client.readQuery(this.placesQuery)
            placesData.sale_places = placesData.sale_places.map(place => {
                place.tables = place.tables.map(table => {
                    if (tableIds.includes(table.id)) {
                        table.status = TABLE_STATUS.AVAILABLE
                    }
                    return table
                })
                return place
            })
            this.client.writeQuery({ ...this.placesQuery, data: placesData })

            // Xoá hoá đơn
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.filter(item => item.id !== invoiceId)
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Thay đổi (thêm, bớt) bàn cho hoá đơn
         *
         * @param {Object} invoice
         */
        TABLES_CHANGED(invoice) {
            let tableIds = invoice.table_ids || []
            let currentInvoice = this.invoices.filter(item => item.id === invoice.id)[0] || []
            let currentTableIds = currentInvoice.table_ids || []

            const availableTables = currentTableIds.filter(tableId => !tableIds.includes(tableId))
            const placesData = this.client.readQuery(this.placesQuery)

            placesData.sale_places = placesData.sale_places.map(place => {
                place.tables = place.tables.map(table => {
                    if (tableIds.includes(table.id)) {
                        table.status = TABLE_STATUS.ACTING
                    }
                    if (availableTables.includes(table.id)) {
                        table.status = TABLE_STATUS.AVAILABLE
                    }
                    return table
                })
                return place
            })
            this.client.writeQuery({ ...this.placesQuery, data: placesData })
        },
        /**
         * Gộp bàn
         * Trả lại trạng thái trống cho bàn được gộp
         *
         * @param {Object} invoice
         * @param {Array} invoiceIds
         */
        TABLES_COMBINED(invoice, invoiceIds) {
            const invoicesData = this.client.readQuery(this.invoicesQuery)

            // Cập nhật trạng thái trống cho bàn
            var tableIds = []
            invoicesData.sale_invoices.forEach(item => {
                if (invoiceIds.includes(item.id)) {
                    tableIds = tableIds.concat(item.table_ids || [])
                }
            })
            const placesData = this.client.readQuery(this.placesQuery)
            placesData.sale_places = placesData.sale_places.map(place => {
                place.tables = place.tables.map(table => {
                    if (tableIds.includes(table.id)) {
                        table.status = TABLE_STATUS.AVAILABLE
                    }
                    return table
                })
                return place
            })
            this.client.writeQuery({ ...this.placesQuery, data: placesData })

            // Xoá hoá đơn khỏi bộ đệm
            invoicesData.sale_invoices = invoicesData.sale_invoices.filter(invoice => !invoiceIds.includes(invoice.id))
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Gộp đơn
         * (Gộp bàn lại và giữ nguyên trạng thái)
         *
         * @param {Array} invoiceIds
         */
        INVOICES_COMBINE(invoiceIds) {
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.filter(invoice => !invoiceIds.includes(invoice.id))
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Tăng số lượng goods item trong hoá đơn hiện tại lên 1 đơn vị
         * Chỉ áp dụng được với những item có trạng thái `đã đặt`
         * 
         * @param {String} goodsId 
         */
        updateIncrementItem(goodsId) {
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.map(invoice => {
                if (invoice.id === this.currentInvoiceId) {
                    invoice.goods = invoice.goods.map(goods => {
                        if (goods.id === goodsId) {
                            goods.count ++
                            let index = goods.orders.findIndex(item => item.is_changing)
                            if (index !== -1) {
                                goods.orders[index].count += 1
                            } else {
                                goods.orders.push({
                                    count: 1,
                                    created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                                    status: GOODS_STATUS.BOOKED,
                                    is_changing: true
                                })
                            }
                            invoice.is_changed = true
                        }
                        return goods
                    })
                }
                return invoice
            })
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Giảm goods item trong hoá đơn hiện tại đi 1 đơn vị
         * Chỉ áp dụng với những item có trạng thái `đã đặt`
         * 
         * @param {String} goodsId 
         */
        updateDecrementItem(goodsId) {
            let invoicesData = this.client.readQuery(this.invoicesQuery)

            // Xác định hoá đơn
            let invoiceIndex = invoicesData.sale_invoices.findIndex(
                invoice => invoice.id === this.currentInvoiceId
            )
            if (invoiceIndex === -1) {
                return
            }
            let invoice = invoicesData.sale_invoices[invoiceIndex]

            // Xác định goods
            let goodsIndex = invoice.goods.findIndex(goods => goods.id === goodsId)
            if (goodsIndex === -1) {
                return
            }
            let goods = invoice.goods[goodsIndex]
            
            // Xác định vị trí goods item đang thao tác
            let index = goods.orders.findIndex(item => item.is_changing)
            if (index === -1) {
                return
            }
            goods.count -= 1
            if (goods.orders[index].count > 1) {
                goods.orders[index].count -=1
            } else {
                // Nếu item có count = 0 thì xoá luôn item đó
                goods.orders.splice(index, 1)
            }

            // Nếu goods không có orders nào (goods.orders.length = 0) thì xoá luôn goods
            if (goods.orders.length === 0) {
                invoice.goods = invoice.goods.filter(goods => goods.id !== goodsId)
            } else {
                invoice.goods[goodsIndex] = goods
            }
            invoicesData.sale_invoices[invoiceIndex] = invoice

            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Tăng số lượng item trả lại (trong hoá đơn trả hàng) lên 1 đơn vị
         * 
         * @param {String} goodsId 
         */
        incrementItemReturnInvoice(goodsId) {
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.map(invoice => {
                if (invoice.id === this.currentInvoiceId) {
                    invoice.goods = invoice.goods.map(goods => {
                        if (goods.id === goodsId && goods.return_count < goods.count) {
                            goods.return_count ++
                        }
                        return goods
                    })
                }
                return invoice
            })
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         *  Giảm số lượng item trả lại (trong hoá đơn trả hàng) lên 1 đơn vị
         * 
         * @param {String} goodsId 
         */
        decrementItemReturnInvoice(goodsId) {
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.map(invoice => {
                if (invoice.id === this.currentInvoiceId) {
                    invoice.goods = invoice.goods.map(goods => {
                        if (goods.id === goodsId && goods.return_count > 0) {
                            goods.return_count --
                        }
                        return goods
                    })
                }
                return invoice
            })
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Chọn/bỏ chọn orders item
         * 
         * @param {String} goodsId          ID goods được chọn
         * @param {Number} orderItemIndex   Index của orders item trong goods
         */
        selectOrderItem(goodsId, orderItemIndex) {
            let invoicesData = this.client.readQuery(this.invoicesQuery)
            let invoiceIndex = invoicesData.sale_invoices.findIndex(
                invoice => invoice.id === this.currentInvoiceId
            )
            if (invoiceIndex === -1) {
                return
            }
            let invoice = invoicesData.sale_invoices[invoiceIndex]
            let goodsIndex = invoice.goods.findIndex(goods => goods.id === goodsId)
            if (goodsIndex === -1) {
                return
            }
            let goods = invoice.goods[goodsIndex]
            let itemOrder = goods.orders[orderItemIndex]
            if (itemOrder.is_changing ||
                itemOrder.is_disable ||
                itemOrder.status === GOODS_STATUS.DELIVERED ||
                itemOrder.status === GOODS_STATUS.RETURNED) {
                return this.$awn.warning(
                    this.$t('manage_sale.invoices.cannot_change_delivered_item')
                )
            }
            itemOrder.is_selected = !itemOrder.is_selected ? true : false
            goods.orders[orderItemIndex] = itemOrder
            invoice.goods[goodsIndex] = goods
            invoicesData.sale_invoices[invoiceIndex] = invoice
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Chọn tất cả orders item
         */
        selectAllOrderItem() {
            let invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.map(invoice => {
                if (invoice.id === this.currentInvoiceId) {
                    invoice.goods = invoice.goods.map(goods => {
                        goods.orders.map(order => {
                            if (!order.is_disable &&
                                !order.is_changing &&
                                order.status !== GOODS_STATUS.DELIVERED &&
                                order.status !== GOODS_STATUS.RETURNED) {
                                order.is_selected = true
                            }
                            return order
                        })       
                        return goods
                    })
                }
                return invoice
            })
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Bỏ chọn tất cả orders item
         */
        deselectAllOrderItem() {
            let invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.map(invoice => {
                if (invoice.id === this.currentInvoiceId) {
                    invoice.goods = invoice.goods.map(goods => {
                        goods.orders.map(order => {
                            if (!order.is_disable &&
                                !order.is_changing &&
                                order.status !== GOODS_STATUS.DELIVERED &&
                                order.status !== GOODS_STATUS.RETURNED) {
                                order.is_selected = false
                            }
                            return order
                        })       
                        return goods
                    })
                }
                return invoice
            })
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Thêm goods item chưa tồn tại vào hoá đơn hiện tại
         * 
         * @param {String} currentInvoiceId 
         * @param {Object} goods 
         */
        addNewInvoiceItem(currentInvoiceId, goods) {
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices = invoicesData.sale_invoices.map(invoice => {
                if (invoice.id === currentInvoiceId) {
                    if (invoice.goods.length > 0) {
                        invoice.goods.push(goods)
                    } else {
                        invoice.goods = [goods]
                    }
                    invoice.is_changed = true
                }
                return invoice
            })
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })
        },
        /**
         * Thêm 1 hoá đơn trống theo table
         * 
         * @param {Object} invoice 
         */
        initEmptyInvoice(invoice) {
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            invoicesData.sale_invoices.unshift(invoice)
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })

            const tableIds = invoice.table_ids || []
            if (tableIds.length > 0) {
                const placesData = this.client.readQuery(this.placesQuery)
                placesData.sale_places = placesData.sale_places.map(place => {
                    place.tables = place.tables.map(table => {
                        if (table.id === tableIds[0]) {
                            table.status = TABLE_STATUS.ACTING
                        }
                        return table
                    })
                    return place
                })
                this.client.writeQuery({ ...this.placesQuery, data: placesData })
            }
        },
        /**
         * Yêu cầu phía người dùng đã được xác nhận
         * 
         * @param {String} invoiceId 
         */
        requestApproved(invoice) {
            // Cập nhật invoices
            const invoicesData = this.client.readQuery(this.invoicesQuery)
            const index = invoicesData.sale_invoices.findIndex(invoiceItem => {
                return invoiceItem.id === invoice.id
            })
            invoicesData.sale_invoices[index].notifications = invoice.notifications
            this.client.writeQuery({ ...this.invoicesQuery, data: invoicesData })

            // const data = this.client.readQuery({ query: LOCAL_NOTIFICATIONS_QUERY })
            // data.local_notifications = data.local_notifications.map(item => {
            //     if (item.invoice_id === invoiceId) {
            //         item.is_execute = true
            //     }
            //     return item
            // })
            // this.client.writeQuery({ query: LOCAL_NOTIFICATIONS_QUERY, data: data })
        }
    }
}