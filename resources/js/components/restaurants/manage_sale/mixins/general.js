import { invoicePosition } from './../../../../helpers/helper_manage_sale.js'
import { GOODS_STATUS } from './../../../../constants.js'
import asyncCache from './async_cache.js'

export default {
  mixins: [ asyncCache ],
	methods: {
    // Vị trí của hóa đơn
    invoicePosition(tableIds) {
      return invoicePosition(this.tables, tableIds)
    },
    // Kiểm tra xem có tồn tại item trong danh sách những item thay đổi trong invoice không
    checkItemInItemsInvoiceChanged(itemId, itemStatus) {
      return this.currentInvoice.items_changed.findIndex(item => {
        return item.id === itemId && item.status === itemStatus
      })
    },
    // Lấy ra index của invoice trong mảng invoices
    getIndexInvoice(invoiceId) {
      return this.invoices.findIndex(invoice => invoice.id === invoiceId)
    },
    // Lấy ra index của item trong goods của currentInvoice
    getIndexItems(itemId) {
      return this.currentInvoice.goods.findIndex(item => item.id === itemId)
    },
    // Lấy ra index của tất cả món ăn được chọn trong currentInvoice
    getItemsSelected() {
      const itemsSelected = []
      this.currentInvoice.goods.forEach((item, iItem) => {
        item.goods_order.forEach((itemOrder, iItemOrder) => {
          if (this.itemsSelected.indexOf(`${item.id}.${itemOrder.status}`) != -1) {
            itemsSelected.push({
              iItem: iItem,
              iItemOrder: iItemOrder,
              status: itemOrder.status
            })
          }
        })
      })
      return itemsSelected
    },
    /**
     * Thay đổi hóa đơn hiện tại bằng hóa đơn khác
     * @param {App\Models\Invoice} invoice 
     */
    changeCurrentInvoice(invoice) {
      // Nếu là hoá đơn trả hàng thì không cần set goods
      if (invoice.is_return) {
        this.currentInvoice = invoice
        this.currentInvoiceId = invoice.id
        return
      }
      this.maxCountSelect = 0
      this.itemsSelected = []
      this.goods = JSON.parse(JSON.stringify(invoice.goods))
      this.currentInvoice = invoice
      this.currentInvoice.is_changed = false
      this.currentInvoiceId = invoice.id
      this.goods = this.goods.map((goods, goodsIndex) => {
        goods.is_expand = true
        goods.orders = goods.orders.map((item, orderIndex) => {
          if (item.status !== GOODS_STATUS.DELIVERED &&
              item.status !== GOODS_STATUS.RETURNED &&
              !item.is_disable &&
              !item.is_changing) {
            this.maxCountSelect += 1
          }
          if (item.is_selected) {
            this.itemsSelected.push({
              item_index: goodsIndex,
              order_item_index: orderIndex
            })
          }
          if (item.is_changing) {
            this.currentInvoice.is_changed = true
          }
          return item
        })
        return goods
      })
    },
    // Kiểm tra những món ăn được đặt đã được giao chưa
    checkDeliveredItems() {
      return this.goods.every(item => {
        return item.orders.every(orderItem => {
          return orderItem.status !== GOODS_STATUS.BOOKED && orderItem.status !== GOODS_STATUS.PROCESSED
        })
      })
    },
    selectInvoiceTables() {
      bus.$emit('modal', {
        component: 'm-s-tables-invoice',
        backdrop: 'static',
        payload: {
          invoice_id: this.currentInvoice.id,
          invoice_code: this.currentInvoice.code,
          table_ids: [],
          is_draft: this.currentInvoice.is_draft
        }
      })
    },
    /**
     * Nút quay trở lại từ màn hình xác thực thanh toán
     */
    backSaleScreen() {
      this.$store.dispatch('isCheckout', false)
      this.processingCheckout = false
    }
	}
}