import PROPERTIES_QUERY from './../../../apollo/query/good/get_properties.gql'
import UNITS_QUERY from './../../../apollo/query/good/get_units.gql'
import GROUP_MENUS_QUERY from './../../../apollo/query/group_menu/list.gql'
import handleGroupMenus from './../../../mixins/list_menu.js'
import { DISCOUNT_TYPE_PERCENT, DISCOUNT_TYPE_FIXED, GOODS_TYPE_SELL } from './../../../constants'
var SetupUnits = () => import('./../setups/select_units.vue')
var ManageProperties = () => import('./../../elements/manage_properties.vue')

export default {
    components: {
        SetupUnits,
        ManageProperties
    },
    mixins: [handleGroupMenus],
    data() {
        return {
            code: '',
            units: [],
            goods_properties: []
        }
    },
    computed: {
        groupId() {
          return this.$store.getters.groupId
        },
        discountMoney() {
          return DISCOUNT_TYPE_FIXED
        },
        discountPercent() {
          return DISCOUNT_TYPE_PERCENT
        },
        typeSell() {
            return GOODS_TYPE_SELL
        }
    },
    apollo: {
        goods_properties: {
            query: PROPERTIES_QUERY,
            variables() {
                return {
                    groupId: this.groupId
                }
            }
        },
        goods_units: {
            query: UNITS_QUERY,
            variables() {
                return {
                    groupId: this.groupId
                }
            },
            manual: true,
            result ({ data }) {
                this.units = []
                data.goods_units.forEach(item => {
                    this.units.push({
                        value: item,
                        name: this.transUnit(item)
                    })
                })
            }
        },
        group_menus: {
            query: GROUP_MENUS_QUERY,
            variables() {
                return {
                    groupId: this.groupId
                }
            },
            result({ data }) {
                this.handleGroupMenus(data.group_menus)
            }
        }
    },
    methods: {
        transUnit(key) {
            if (this.$te(`config.goods.units.${key}`)) {
                return this.$t(`config.goods.units.${key}`)
            }
            return key
        }
    }
}