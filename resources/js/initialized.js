import currentGroup from './helpers/current_group.js'
import store from './store/index.js'

export default {
    install() {
        let group = JSON.parse(currentGroup.getCurrentGroup())
        if (currentGroup.getCurrentGroup() !== null) {
            window.axios.defaults.params['group_id'] = group.id
            store.dispatch('setCurrentGroup', group)
        }

        let groups = currentGroup.getTotalGroups()
        if (groups) {
            store.dispatch('setTotalGroups', groups)
        }

        let permissions = currentGroup.getCurrentPermissions()
        if (permissions) {
            store.dispatch('setCurrentPermissions', permissions)
        }
    }
}
