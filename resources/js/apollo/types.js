import gql from 'graphql-tag'

export const typeDefs = gql`
    type LocalNotification {
        invoice_id: String!,
        code: String!,
        created_at: String!,
        table_ids: [String],
        type: Int,
        from_type: Int,
        creator: String,
        is_execute: Boolean
    }
    extend type invoice {
        is_draft: Boolean,
        is_changed: Boolean,
        is_return: Boolean
    }
`