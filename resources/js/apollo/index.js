import Vue from 'vue'
import VueApollo from 'vue-apollo'
import AWN from 'awesome-notifications'
import token from './../helpers/token.js'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { BatchHttpLink } from 'apollo-link-batch-http'
import { onError } from 'apollo-link-error'
import { ApolloLink, from } from 'apollo-link'
import { typeDefs } from './types.js'
import { createUploadLink } from 'apollo-upload-client'

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
        if (process.env.NODE_ENV !== 'production') {
            console.log(JSON.stringify(graphQLErrors))
            graphQLErrors.forEach(({ message, locations, path }) => {
                console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
            })
        }
    }
    if (networkError) {
        if (networkError.statusCode === 403) {
            new AWN().alert(networkError.result.message)
        }
        if (process.env.NODE_ENV !== 'production') {
            console.log(JSON.stringify(networkError))
        }
    }
})

const host = window.Laravel.app_url + '/graphql'

const httpLink = new BatchHttpLink({
    uri: host,
    batchInterval: 100
})
const masterHttpOption = {
    uri: host + '/master',
    batchInterval: 100
}
const verifiedHttpLink = new HttpLink({ uri: host + '/verified' })
const guestHttpLink = new HttpLink({ uri: host + '/guest' })
const masterHttpLink = ApolloLink.split(
    operation => operation.getContext().hasUpload,
    createUploadLink(masterHttpOption),
    new BatchHttpLink(masterHttpOption)
)


const authLink = new ApolloLink((operation, forward) => {
    operation.setContext({
        headers: {
            'Authorization': 'Bearer ' + token.getItem(),
            'X-Socket-Id': window.Echo ? window.Echo.socketId() : ''
        }
    })
    return forward(operation)
})

const cache = new InMemoryCache({
    cacheRedirects: {
        Query: {
            invoice: (_, args, { getCacheKey }) => getCacheKey({ __typename: 'invoice', id: args.invoice_id }),
            place: (_, args, { getCacheKey }) => getCacheKey({ __typename: 'place', id: args.id }),
            shift: (_, args, { getCacheKey }) => getCacheKey({ __typename: 'shift', id: args.shift_id }),
            table: (_, args, { getCacheKey }) => getCacheKey({ __typename: 'table', id: args.table_id }),
            group_menu: (_, args, { getCacheKey }) => getCacheKey({ __typename: 'group_menu', id: args.group_menu_id })
        }
    }
})

const resolvers = {
    invoice: {
        is_return: () => {
            return false
        },
        is_changed: () => {
            return false
        },
        is_draft: () => {
            return false
        }
    }
}

const apolloClient = new ApolloClient({
    cache: cache,
    link: from([
        errorLink,
        authLink,
        httpLink
    ]),
    typeDefs,
    resolvers: resolvers,
    connectToDevTools: true
})

cache.writeData({
    data: {
        local_notifications: [],
    }
})

const verifiedApolloClient = new ApolloClient({
    link: from([
        errorLink,
        authLink,
        verifiedHttpLink
    ]),
    cache,
    connectToDevTools: true
})

const guestApolloClient = new ApolloClient({
    link: guestHttpLink,
    cache,
    connectToDevTools: true
})

const masterApolloClient = new ApolloClient({
    link: from([
        errorLink,
        authLink,
        masterHttpLink,
    ]),
    cache,
    connectToDevTools: true
})

Vue.use(VueApollo)

/**
 * apolloProvider
 * @type {Object}
 */
export default new VueApollo({
    clients: {
        guest: guestApolloClient,
        default: apolloClient,
        verified: verifiedApolloClient,
        master: masterApolloClient
    },
    defaultClient: apolloClient
})
