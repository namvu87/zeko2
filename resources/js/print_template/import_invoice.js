export default {
    'vi': {
    /* A4 */ 1: `<div>
<div style="text-align:center">&nbsp;</div>

<div style="text-align:center"><span style="color:#000000"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>PHIẾU NHẬP HÀNG</strong></span></span></span></div>

<div style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Mã phiếu: {{Ma_Phieu}}</strong></span>&nbsp;</span></div>

<div style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><em>Ngày: {{Ngay_Tao}}</em></span></span></div>

<div style="text-align:center">&nbsp;</div>

<table border="1" cellspacing="0" style="border-collapse:collapse; width:100%">
	<tbody>
		<tr>
			<td style="height:35px; text-align:center; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>STT</strong></span></span></td>
			<td style="height:35px; text-align:center; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Mã hàng</strong></span></span></td>
			<td style="height:35px; text-align:center; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Tên hàng hóa</strong></span></span></td>
			<td style="height:35px; text-align:right; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Số lượng</strong></span></span></td>
			<td style="height:35px; text-align:right; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Đơn giá</strong></span></span></td>
			<td style="height:35px; text-align:right; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Khuyến mại</strong></span></span></td>
			<td style="height:35px; text-align:right; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Thành tiền</strong></span></span></td>
		</tr>
		<tr>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{So_Thu_Tu}}</span></span></div>
			</td>
			<td style="height:35px; vertical-align:middle">
			<div style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Ma_Hang}}</span></span></div>
			</td>
			<td style="height:35px; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Ten_Hang}}</span></span></div>
			</td>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{So_Luong}}</span></span></div>
			</td>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Don_Gia}}</span></span></div>
			</td>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Khuyen_Mai}}</span></span></div>
			</td>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Thanh_Tien}}</span></span></div>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table cellspacing="0" style="border-collapse:collapse; width:100%">
	<tbody>
		<tr>
			<td style="text-align:right"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Tổng tiền hàng</strong>:</span></span></td>
			<td style="text-align:right"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Tong_Tien}}</span></span></td>
		</tr>
		<tr>
			<td colspan="3">
			<p>&nbsp;</p>

			<p><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Ghi chú</strong>: {{Ghi_Chu}}</span></span></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>Người lập</strong></span></span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{Nhan_Vien_Phu_Trach}}</span></span></p>
</div>`
    },
    'en': {
    /* A4 */ 1: `<div style="text-align:center">&nbsp;</div>

<div style="text-align:center"><span style="color:#000000; font-family:Times New Roman, Times, serif"><span style="font-size:16px"><strong>IMPORT COUPON</strong></span></span></div>

<div style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Code: {{Invoice_Code}}</strong></span>&nbsp;</span></div>

<div style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><em>Date: {{Date_Create_Invoice}}</em></span></span></div>

<div style="text-align:center">&nbsp;</div>

<table border="1" cellspacing="0" style="border-collapse:collapse; width:100%">
	<tbody>
		<tr>
			<td style="height:35px; text-align:center; vertical-align:middle"><span style="color:#000000; font-family:Times New Roman, Times, serif"><strong>No.</strong></span></td>
			<td style="height:35px; text-align:center; vertical-align:middle"><span style="color:#000000; font-family:Times New Roman, Times, serif"><strong>Code</strong></span></td>
			<td style="height:35px; text-align:center; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Name</strong></span></span></td>
			<td style="height:35px; text-align:right; vertical-align:middle"><span style="color:#000000; font-family:Times New Roman, Times, serif"><strong>Quantity</strong></span></td>
			<td style="height:35px; text-align:right; vertical-align:middle"><span style="color:#000000; font-family:Times New Roman, Times, serif"><strong>Price</strong></span></td>
			<td style="height:35px; text-align:right; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Discount</strong></span></span></td>
			<td style="height:35px; text-align:right; vertical-align:middle"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Total</strong></span></span></td>
		</tr>
		<tr>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Numerical_Invoice}}</span></span></div>
			</td>
			<td style="height:35px; vertical-align:middle">
			<div style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Goods_Code}}</span></span></div>
			</td>
			<td style="height:35px; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Goods_Name}}</span></span></div>
			</td>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Goods_Count}}</span></span></div>
			</td>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Goods_Price}}</span></span></div>
			</td>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Goods_Discount}}</span></span></div>
			</td>
			<td style="height:35px; text-align:right; vertical-align:middle">
			<div><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Goods_Total_Price}}</span></span></div>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table cellspacing="0" style="border-collapse:collapse; width:100%">
	<tbody>
		<tr>
			<td style="text-align:right"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Grand total</strong>:</span></span></td>
			<td style="text-align:right"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">{{Total_Money}}</span></span></td>
		</tr>
		<tr>
			<td colspan="3">
			<p>&nbsp;</p>

			<p><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Note</strong>: {{Note}}</span></span></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>Created by</strong></span></span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{Employee_Name}}</span></span></p>
`
    }
}