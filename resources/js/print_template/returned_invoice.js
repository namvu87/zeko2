export default {
    'vi': {
    /* K80 */ 2: `<table style="width:100%">
	<tbody>
		<tr>
			<td><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Tên nhà hàng: {{Ten_Nha_Hang}}</span></span></span></td>
		</tr>
		<tr>
			<td><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Địa chỉ: {{Dia_Chi_Nha_Hang}}</span></span></span></td>
		</tr>
	</tbody>
</table>

<div style="border-bottom:1px dashed black">&nbsp;</div>

<p style="text-align:center"><strong><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">HÓA ĐƠN TRẢ HÀNG</span></span></span></strong></p>

<div style="border-bottom:1px dashed gray">
<p><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Ngày bán:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{Ngay_Ban}}<br />
Mã hóa đơn:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{Ma_Hoa_Don}}<br />
Mã hóa đơn bán hàng: {{Ma_Hoa_Don_Ban_Hang}}<br />
Nhân viên:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{{Nhan_Vien_Ban_Hang}}</span></span></span></p>
</div>

<table border="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="2" rowspan="1"><strong><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Đơn giá</span></span></span></strong></td>
			<td style="text-align:center"><strong><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Số lượng</span></span></span></strong></td>
			<td style="text-align:right"><strong><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Thành tiền</span></span></span></strong></td>
		</tr>
		<tr>
			<td colspan="4"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Ten_Hang}}</span></span></span></td>
		</tr>
		<tr>
			<td colspan="2" rowspan="1"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Don_Gia}}</span></span></span></td>
			<td style="text-align:center"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{So_Luong}}</span></span></span></td>
			<td style="text-align:right"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Thanh_Tien}}</span></span></span></td>
		</tr>
	</tbody>
</table>

<div style="border-bottom:1px dashed gray">
<p>&nbsp;</p>
</div>

<div style="border-bottom:1px dashed gray">
<p><span style="color:#000000"><strong><span style="font-size:10px"><span style="font-family:Times New Roman,Times,serif">Ghi chú:</span></span></strong><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif"> {{Ghi_Chu}}</span></span></span></p>
</div>

<table style="width:100%">
	<tbody>
		<tr>
			<td><strong><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Tổng tiền:</span></span></span></strong></td>
			<td style="text-align:right"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Tong_Tien}}</span></span></span></td>
		</tr>
	</tbody>
</table>

<div style="border-bottom:1px dashed gray">
<p>&nbsp;</p>
</div>

<p style="text-align:center"><strong><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">CẢM ƠN QUÝ KHÁCH VÀ HẸN GẶP LẠI</span></span></span></strong></p>

<p style="text-align:center"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Hotline: 18000081&nbsp; &nbsp; Website: zeko.vn</span></span></span></p>
`
    },
    'en': {
    /* k80 */ 2: `<table style="width:100%">
	<tbody>
		<tr>
			<td><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Restaurant name: {{Restaurant_Name}}</span></span></span></td>
		</tr>
		<tr>
			<td><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Address: {{Restaurant_Address}}</span></span></span></td>
		</tr>
	</tbody>
</table>

<div style="border-bottom:1px dashed black">&nbsp;</div>

<p style="text-align:center"><span style="color:#000000; font-family:Times New Roman,Times,serif"><span style="font-size:11px"><strong>RETURNED INVOICE</strong></span></span></p>

<div style="border-bottom:1px dashed gray">
<p><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Date:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{Date_Time_Create_Invoice}}<br />
Bill No:&nbsp; &nbsp; &nbsp;{{Returned_Invoice_Code}}</span></span></span><br />
<span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Return for: {{Sale_Invoice_Code}}<br />
Employee: {{Employee_Name}}</span></span></span></p>
</div>

<table border="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="2" rowspan="1"><span style="color:#000000; font-family:Times New Roman,Times,serif"><span style="font-size:11px"><strong>Price</strong></span></span></td>
			<td style="text-align:center"><span style="color:#000000; font-family:Times New Roman,Times,serif"><span style="font-size:11px"><strong>Quantity</strong></span></span></td>
			<td style="text-align:right"><span style="color:#000000; font-family:Times New Roman,Times,serif"><span style="font-size:11px"><strong>Total</strong></span></span></td>
		</tr>
		<tr>
			<td colspan="4"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Goods_Name}}</span></span></span></td>
		</tr>
		<tr>
			<td colspan="2" rowspan="1"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Goods_Price}}</span></span></span></td>
			<td style="text-align:center"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Goods_Count}}</span></span></span></td>
			<td style="text-align:right"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Goods_Total_Price}}</span></span></span></td>
		</tr>
	</tbody>
</table>

<div style="border-bottom:1px dashed gray">
<p>&nbsp;</p>
</div>

<div style="border-bottom:1px dashed gray">
<p><span style="color:#000000"><strong><span style="font-size:10px"><span style="font-family:Times New Roman,Times,serif">Note:</span></span></strong><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif"> {{Note}}</span></span></span></p>
</div>

<table style="width:100%">
	<tbody>
		<tr>
			<td><strong><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Grand Total:</span></span></span></strong></td>
			<td style="text-align:right"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">{{Grand_Total}}</span></span></span></td>
		</tr>
	</tbody>
</table>

<div style="border-bottom:1px dashed gray">
<p>&nbsp;</p>
</div>

<p style="text-align:center"><span style="color:#000000; font-family:Times New Roman,Times,serif"><span style="font-size:11px"><strong>THANK YOU AND SEE YOU AGAIN!</strong></span></span></p>

<p style="text-align:center"><span style="color:#000000"><span style="font-size:11px"><span style="font-family:Times New Roman,Times,serif">Hotline: 18000081&nbsp; &nbsp; Website: zeko.vn</span></span></span></p>
`
    }
}