import Vue from 'vue'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
import lang from './../vue-i18n-locales.generated.js'
import language from './../helpers/language'

let locale = language.getLanguage()

export default new VueI18n({
    locale: locale,
    messages: lang
})