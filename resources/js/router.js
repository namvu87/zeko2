import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'
import token from './helpers/token'
import user from './helpers/user'

Vue.use(VueRouter)

const PageNotFound = () => import('./components/aborts/404.vue')
const Policy = () => import('./components/policy/index.vue')
const Login = () => import('./components/auth/login.vue')
const Register = () => import('./components/auth/register.vue')
const Verify = () => import('./components/auth/verify.vue')
const ForgetPassword = () => import('./components/auth/passwords/forget_password.vue')
const ResetPassword = () => import('./components/auth/passwords/reset_password.vue')
const ListUser = () => import('./components/companies/users/list.vue')
const UserDetail = () => import('./components/companies/users/detail.vue')
const Layout = () => import('./components/layout.vue')
const Verification = () => import('./components/auth/verification/index.vue')
const VerifyPhoneNumber = () => import('./components/auth/verification/verify_phone_number.vue')

const Home = () => import('./components/home/index.vue')
const RegisterService = () => import('./components/home/register_service.vue')

// Company
const ShiftList = () => import('./components/companies/shifts/index.vue')
const ShiftCreate = () => import('./components/companies/shifts/create.vue')
const ShiftEdit = () => import('./components/companies/shifts/edit.vue')
const ShiftUserList = () => import('./components/companies/shifts/user_list.vue')
const ShiftUserAdd = () => import('./components/companies/shifts/user_add.vue')
const Profile = () => import('./components/users/profile/index.vue')
const GroupEditInformation = () => import('./components/group/profile_edit.vue')
const ServicePayment = () => import('./components/users/service_payment.vue')

const PermissionManage = () => import('./components/companies/permission/role_permissions.vue')
const RoleManage = () => import('./components/companies/permission/user_roles.vue')

const TimeKeepingsList = () => import('./components/companies/time_keepings/list.vue')
const DetailTimeKeeping = () => import('./components/companies/time_keepings/detail.vue')
const CompanyReport = () => import('./components/companies/time_keepings/company_report.vue')
const EmployeeReport = () => import('./components/companies/time_keepings/employee_report.vue')

// Restaurant
const PlaceList = () => import('./components/restaurants/places/list')
const PlaceCreate = () => import('./components/restaurants/places/create')
const PlaceEdit = () => import('./components/restaurants/places/edit')

const TableList = () => import('./components/restaurants/tables/list')
const TableCreate = () => import('./components/restaurants/tables/create')
const TableEdit = () => import('./components/restaurants/tables/edit')

const MenuList = () => import('./components/restaurants/group_menus/index')
const MenuCreate = () => import('./components/restaurants/group_menus/create')
const MenuEdit = () => import('./components/restaurants/group_menus/edit')

const InvoiceList = () => import('./components/restaurants/invoices/list')
const ReturnedInvoiceList = () => import('./components/restaurants/returned_invoices/list')

const ImportInvoiceCreate = () => import('./components/restaurants/import_invoices/create')
const ImportInvoiceList = () => import('./components/restaurants/import_invoices/list')

const TemplateList = () => import('./components/restaurants/templates/index')

const GoodImportList = () => import('./components/restaurants/good/import/list.vue')
const GoodImportCreate = () => import('./components/restaurants/good/import/create.vue')
const GoodImportEdit = () => import('./components/restaurants/good/import/edit.vue')

const GoodSaleList = () => import('./components/restaurants/good/sale/list.vue')
const GoodSaleCreate = () => import('./components/restaurants/good/sale/create.vue')
const GoodSaleEdit = () => import('./components/restaurants/good/sale/edit.vue')

const ReportSale = () => import('./components/restaurants/report/sale/index')
const ReportGoods = () => import('./components/restaurants/report/goods/index')

// Master
const ManageSale = () => import('./components/restaurants/manage_sale/index.vue')
const OrdersHistory = () => import('./components/restaurants/manage_sale/history.vue')
const Kitchen = () => import('./components/restaurants/kitchen/index.vue')

// import LayoutMaster from './components/masters/layout.vue'

const HomeMaster = () => import('./components/home/master.vue')

const SliderList = () => import('./components/masters/sliders/index.vue')
const SliderCreate = () => import('./components/masters/sliders/create.vue')
const SliderEdit = () => import('./components/masters/sliders/edit.vue')

const CompanyListMaster = () => import('./components/masters/companies/index.vue')
const CompanyEditMaster = () => import('./components/masters/companies/edit.vue')

const GoodImage = () => import('./components/masters/good_image/index')

const Notifications = () => import('./components/notifications/index.vue')
const AdditionTimeKeeping = () => import('./components/requirements/addition_time_keeping.vue')
const TakeLeave = () => import('./components/requirements/take_leave.vue')
const SwitchShift = () => import('./components/requirements/switch_shift.vue')

const routes = [
    {
        path: '/',
        component: Layout,
        meta: { Auth: true },
        children: [
            { path: '/', component: Home, name: 'home' },
            { path: '/profile', component: Profile, name: 'user.profile' },
            { path: '/group/edit', component: GroupEditInformation, name: 'group.information.edit' },
            { path: '/service-payment', component: ServicePayment, name: 'service.payment' },

            {
                path: 'company/users',
                name: 'company.user.list',
                component: ListUser,
                props: route => ({
                    page: route.query.page || null,
                    keyword: route.query.keyword || '',
                })
            },
            { path: 'company/user/detail/:userId', name: 'company.user.detail', component: UserDetail },

            { path: 'company/shift', component: ShiftList, name: 'shift.list' },
            { path: 'company/shift/create', component: ShiftCreate, name: 'shift.create' },
            { path: 'company/shift/edit/:shiftId', component: ShiftEdit, name: 'shift.edit' },
            { path: 'company/shift/user-list/:shiftId', component: ShiftUserList, name: 'shift.user.list' },
            { path: 'company/shift/user-add/:shiftId', component: ShiftUserAdd, name: 'shift.user.add' },

            { path: 'company/time-keeping', component: TimeKeepingsList, name: 'timekeeping.list' },
            { path: 'company/time-keeping/detail/:id', component: DetailTimeKeeping, name: 'timekeeping.detail' },
            { path: 'company/report', component: CompanyReport, name: 'company.report' },
            { path: 'company/employee-report/:userId', component: EmployeeReport, name: 'employee.report' },

            { path: 'permission', component: PermissionManage, name: 'permission.manage' },
            { path: 'role', component: RoleManage, name: 'role.manage' },

            { path: 'notifications', component: Notifications, name: 'notification.list' },
            {
                path: 'requirement/addition-time-keeping/:id',
                component: AdditionTimeKeeping,
                name: 'requirement.addition_timekeeping'
            },
            { path: 'requirement/take-leave/:id', component: TakeLeave, name: 'requirement.take_leave' },
            { path: 'requirement/switch-shift/:id', component: SwitchShift, name: 'requirement.switch_shift' },

            // Restaurant
            { path: 'restaurant/place/list', component: PlaceList, name: 'restaurant.place.list' },
            { path: 'restaurant/place/create', component: PlaceCreate, name: 'restaurant.place.create' },
            { path: 'restaurant/place/edit/:placeId', component: PlaceEdit, name: 'restaurant.place.edit', props: true },
            {
                path: 'restaurant/table/list',
                component: TableList,
                name: 'restaurant.table.list',
                props: (route) => ({
                    page: parseInt(route.query.page) || 1,
                    name: route.query.name || '',
                    status: parseInt(route.query.status) || null,
                    placeId: route.query.placeId || '',
                    newTable: route.params.newTable || null
                })
            },
            { path: 'restaurant/table/create', component: TableCreate, name: 'restaurant.table.create' },
            { path: 'restaurant/table/edit/:tableId', component: TableEdit, name: 'restaurant.table.edit', props: true },

            { path: 'restaurant/group-menus/list', component: MenuList, name: 'restaurant.menu.list' },
            { path: 'restaurant/group-menus/create', component: MenuCreate, name: 'restaurant.menu.create' },
            { path: 'restaurant/group-menus/edit/:groupMenuId', component: MenuEdit, name: 'restaurant.menu.edit' },


            { path: 'restaurant/goods/list/imported', component: GoodImportList, name: 'restaurant.good_import.list' },
            { path: 'restaurant/goods/create/imported', component: GoodImportCreate, name: 'restaurant.good_import.create' },
            {
                path: 'restaurant/goods/edit/imported/:goodId',
                component: GoodImportEdit,
                name: 'restaurant.good_import.edit'
            },

            { path: 'restaurant/goods/list/sale', component: GoodSaleList, name: 'restaurant.good_sale.list' },
            { path: 'restaurant/goods/create/sale', component: GoodSaleCreate, name: 'restaurant.good_sale.create' },
            { path: 'restaurant/goods/edit/sale/:goodId', component: GoodSaleEdit, name: 'restaurant.good_sale.edit' },

            { path: 'restaurant/invoice/list', component: InvoiceList, name: 'restaurant.invoice.list' },

            { path: 'restaurant/import-invoice/create', component: ImportInvoiceCreate, name: 'restaurant.import_invoice.create' },
            { path: 'restaurant/import-invoice/list', component: ImportInvoiceList, name: 'restaurant.import_invoice.list' },

            // dupplicate route for multi link at sidebar
            { path: 'restaurant/invoice/imported/create', component: ImportInvoiceCreate, name: 'restaurant.invoice.imported.create' },
            { path: 'restaurant/invoice/imported/list', component: ImportInvoiceList, name: 'restaurant.invoice.imported.list' },


            {
                path: 'restaurant/invoice-form/list',
                component: TemplateList,
                name: 'restaurant.invoice-form.list'
            },

            {
                path: 'restaurant/returned-invoice/list',
                component: ReturnedInvoiceList,
                name: 'restaurant.returned_invoice.list'
            },

            { path: 'restaurant/report/sale', component: ReportSale, name: 'restaurant.report.sale' },
            { path: 'restaurant/report/goods', component: ReportGoods, name: 'restaurant.report.goods' },
            { path: '/verification', component: Verification },
            { path: '/verification/phone-number', component: VerifyPhoneNumber },
        ]
    },
    {
        path: '/restaurant/manage-sale/sale',
        component: ManageSale,
        name: 'restaurant.manage-sale.sale',
        meta: { Auth: true }
    },
    {
        path: '/restaurant/manage-sale/history',
        component: OrdersHistory,
        name: 'restaurant.manage-sale.history',
        meta: { Auth: true }
    },
    {
        path: '/restaurant/kitchen',
        component: Kitchen,
        name: 'restaurant.kitchen',
        meta: { Auth: true }
    },
    {
        path: '/master',
        component: Layout,
        meta: { Master: true },
        children: [
            { path: '/', component: HomeMaster, name: 'master.home' },
            { path: 'slider', component: SliderList, name: 'master.slider.list' },
            { path: 'slider/create', component: SliderCreate, name: 'master.slider.create' },
            { path: 'slider/edit/:id', component: SliderEdit, name: 'master.slider.edit' },
            { path: 'company', component: CompanyListMaster, name: 'master.company.list' },
            { path: 'company/edit/:id', component: CompanyEditMaster, name: 'master.company.edit' },
            { path: 'good/image', component: GoodImage, name: 'master.good_image' }
        ],
    },
    {
        path: '/policy',
        component: Policy,
        name: 'policy',
        meta: {
            title: 'Privacy policy'
        }
    },
    { path: '/register-service', component: RegisterService, name: 'register_service', meta: {} },
    { path: '/login', component: Login, name: 'login', meta: { Guest: true } },
    { path: '/register', component: Register, meta: { Guest: true } },
    { path: '/forget-password', component: ForgetPassword, meta: { Guest: true } },
    { path: '/reset-password', component: ResetPassword, meta: { Guest: true } },
    { path: '/api/email/verify/:userId', component: Verify },
    { path: '*', component: PageNotFound }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    if (token.getItem()) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token.getItem()
        store.dispatch('setUserInfo', user.getItem())
    }
    if (to.matched.some(record => record.meta.Auth)) {
        if (store.state.auth.authenticated || token.getItem()) {
            return next()
        }
        return next('/login')
    } else if (to.matched.some(record => record.meta.Guest)) {
        if (store.state.auth.authenticated || token.getItem()) {
            return next('/')
        }
        return next()
    } else if (to.matched.some(record => record.meta.Master)) {
        if (store.state.auth.authenticated && store.state.auth.user.is_master) {
            return next()
        }
        store.dispatch('removeCurrentGroup')
        return next('/')
    }

    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title)
    if (nearestWithTitle) {
        document.title = nearestWithTitle.meta.title
    }

    next()
})
export default router
