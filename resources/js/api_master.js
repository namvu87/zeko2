const baseUrl = window.Laravel.app_url

function url(path) {
    return baseUrl + '/api/master/' + path
}

//Master
export const GET_HOME_MASTER           = url('home')

export const GET_SLIDER_LIST           = url('slider/list')
export const GET_SLIDER_CREATE         = url('slider/create')
export const STORE_SLIDER              = url('slider/store')
export const GET_SLIDER_EDIT           = url('slider/edit')
export const UPDATE_SLIDER             = url('slider/update')
export const DELETE_SLIDER             = url('slider/delete')
export const GET_SLIDER_CHANGE_STATUS  = url('slider/change-status')

export const GET_TAG_LIST              = url('tag/list')
export const GET_TAG_CREATE            = url('tag/create')
export const STORE_TAG                 = url('tag/store')
export const GET_TAG_EDIT              = url('tag/edit')
export const UPDATE_TAG                = url('tag/update')
export const DELETE_TAG                = url('tag/delete')
export const RESTORE_TAG               = url('tag/restore')
export const GET_TAG_CHANGE_STATUS     = url('tag/change-status')

export const GET_NEWS_LIST             = url('news/list')
export const GET_NEWS_CREATE           = url('news/create')
export const STORE_NEWS                = url('news/store')
export const GET_NEWS_EDIT             = url('news/edit')
export const UPDATE_NEWS               = url('news/update')
export const DELETE_NEWS               = url('news/delete')
export const RESTORE_NEWS              = url('news/restore')
export const CHANGE_STATUS_NEWS        = url('news/change-status')

export const GET_GUIDE_LIST            = url('guide/list')
export const GET_GUIDE_CREATE          = url('guide/create')
export const STORE_GUIDE               = url('guide/store')
export const GET_GUIDE_EDIT            = url('guide/edit')
export const UPDATE_GUIDE              = url('guide/update')
export const DELETE_GUIDE              = url('guide/delete')
export const RESTORE_GUIDE             = url('guide/restore')
export const CHANGE_STATUS_GUIDE       = url('guide/change-status')

export const GET_CUSTOMER_CARE_LIST    = url('customer-care/list')
export const GET_CUSTOMER_CARE_CREATE  = url('customer-care/create')
export const STORE_CUSTOMER_CARE       = url('customer-care/store')
export const GET_CUSTOMER_CARE_EDIT    = url('customer-care/edit')
export const UPDATE_CUSTOMER_CARE      = url('customer-care/update')
export const DELETE_CUSTOMER_CARE      = url('customer-care/delete')

export const GET_CONTACT_INFORMATION   = url('contact')
export const STORE_CONTACT_INFORMATION = url('contact/store')

export const LIST_PRODUCT_MASTER       = url('product/list')
export const CREATE_PRODUCT_MASTER     = url('product/create')
export const STORE_PRODUCT_MASTER      = url('product/store')
export const EDIT_PRODUCT_MASTER       = url('product/edit')
export const UPDATE_PRODUCT_MASTER     = url('product/update')
export const DELETE_PRODUCT_MASTER     = url('product/delete')

export const GET_GROUP_LIST_MASTER     = url('group/list')
export const GET_GROUP_EDIT_MASTER     = url('group/edit')
export const UPDATE_GROUP_MASTER       = url('group/update')

export const GET_IMAGE_EDITOR          = url('images/list')
export const DELETE_IMAGE_EDITOR       = url('images/delete')
export const UPLOAD_IMAGE_EDITOR       = url('images/upload')