export const MASTER_SERVICE = -1
export const TIMEKEEPING_SERVICE = 1
export const RESTAURANT_SERVICE = 2

export const SUCCESS_STATUS = 1

/* Master */
export const SLIDE_TYPE_SHOW_WEB = 0
export const SLIDE_TYPE_SHOW_MOBILE = 1
/* Master.end */

/* Restaurant service */
export const TABLE_STATUS_INACTIVE = 0
export const TABLE_STATUS_AVAILABLE = 1
export const TABLE_STATUS_ACTING = 2
export const TABLE_STATUS_BOOKING = 3

export const GOODS_STATUS_ACTIVE = 1
export const GOODS_STATUS_INACTIVE = 2

export const GOODS_TYPE_SELL = 2
export const GOODS_TYPE_IMPORTED = 1

export const GOODS_BOOKED = 'booked'
export const GOODS_PROCESSING = 'processing'
export const GOODS_PROCESSED = 'processed'
export const GOODS_DELIVERED = 'delivered'
export const GOODS_RETURNED = 'returned'

export const TYPE_MATCHING_TABLES = 1
export const TYPE_MATCHING_ORDERS = 2

export const DISCOUNT_TYPE_FIXED = 1
export const DISCOUNT_TYPE_PERCENT = 2

export const RETURNED_INVOICE_PAID = 1
export const RETURNED_INVOICE_DISABLED = 2

export const INVOICE_STATUS_INITED = 1
export const INVOICE_STATUS_PURCHASED = 2
export const INVOICE_STATUS_DISABLE = 3

export const INVOICE_TYPE_USE_TABLE = 1
export const INVOICE_TYPE_NOT_USE_TABLE = 2

export const INVOICE_NONE_REQUEST = 0
export const INVOICE_CREATE_REQUEST_STATUS = 1
export const INVOICE_ADDITION_REQUEST_STATUS = 2
export const INVOICE_PURCHASE_REQUEST = 3

export const EVENTS_TYPE = {
    ITEM_ADDED: 1,
    INVOICE_UPDATED: 2,             // Cập nhật số lượng món (thêm món, kiểm kê)
    TABLES_CHANGED: 3,              // Thay đổi danh sách bàn
    TABLES_COMBINED: 4,             // Gộp bàn
    INVOICES_COMBINE: 5,            // Gộp hoá đơn
    INVOICE_NOTED: 6,               // Thêm ghi chú cho hoá đơn
    ITEM_REMOVED: 7,                // Huỷ 1 món trong danh sách
    DISCOUNT_CHANGED: 8,            // Thay đổi giảm giá món, hoá đơn
    ITEM_STATUS_CHANGED: 9,         // Thay đổi trạng thái món
    INVOICE_PURCHASED: 10,          // Thanh toán
    INVOICE_DESTROYED: 11,          // Huỷ đơn
    REQUEST_CHECKOUT: 12            // Yêu cầu thanh toán
}

export const TABLE_STATUS = {
    INACTIVE: 0,
    AVAILABLE: 1,
    ACTING: 2,
    BOOKING: 3
}

export const GOODS_STATUS = {
    BOOKED: 'booked',
    PROCESSING: 'processing',
    PROCESSED: 'processed',
    DELIVERED: 'delivered',
    RETURNED: 'returned'
}

export const INVOICE_EVENT_TYPE_MANAGE_UPDATED_INVOICE = 1
export const INVOICE_EVENT_TYPE_MANAGE_CHANGED_INVOICE_TABLES = 2
export const INVOICE_EVENT_TYPE_MANAGE_CHANGED_INVOICE_NOTE = 3
export const INVOICE_EVENT_TYPE_MANAGE_CHANGED_INVOICE_DISCOUNT = 4
export const INVOICE_EVENT_TYPE_MANAGE_COMBINED_TABLES = 5
export const INVOICE_EVENT_TYPE_MANAGE_COMBINED_INVOICES = 6
export const INVOICE_EVENT_TYPE_MANAGE_DELETED_INVOICE = 7
export const INVOICE_EVENT_TYPE_MANAGE_CHECKOUTED_INVOICE = 8
export const INVOICE_EVENT_TYPE_MANAGE_CHANGED_INVOICE_ITEMS_STATUS = 9
export const INVOICE_EVENT_TYPE_MANAGE_DELETED_INVOICE_ITEM = 10

export const INVOICE_FORM_TYPE_SALE_INVOICE = 1
export const INVOICE_FORM_TYPE_IMPORT_INVOICE = 2
export const INVOICE_FORM_TYPE_RETURNED_INVOICE = 3

export const SIZE_A4 = 1
export const SIZE_K80 = 2
export const SIZE_TEMPLATE = {
    1: {
        name: 'A4',
        width: '210mm',
        height: '297mm'
    },
    2: {
        name: 'K80',
        width: '72mm'
    }
}

export const NOTIFICATION_TYPE_60 = 60
export const NOTIFICATION_TYPE_61 = 61

export const NOTIFY_INVOICE_CREATION = 1
export const NOTIFY_REQUEST_ADDITION = 2
export const NOTIFY_REQUEST_PURCHASE = 3

export const NOTIFY_TYPE_40 = 40 // Hoá đơn được khởi tạo
export const NOTIFY_TYPE_41 = 41 // Yêu cầu thanh toán
export const NOTIFY_TYPE_42 = 42 // Yêu cầu thêm món
export const NOTIFY_TYPE_43 = 43 // Thay đổi trạng thái món
export const NOTIFY_TYPE_44 = 44 // Thay đổi số lượng món (kiểm kê)
export const NOTIFY_TYPE_45 = 45 // Thay đổi danh sách bàn
export const NOTIFY_TYPE_46 = 46 // Gộp bàn
export const NOTIFY_TYPE_47 = 47 // Gộp đơn
export const NOTIFY_TYPE_48 = 48 // Cập nhật ghi chú
export const NOTIFY_TYPE_49 = 49 // Huỷ món
export const NOTIFY_TYPE_50 = 50 // Cập nhật giảm giá
export const NOTIFY_TYPE_51 = 51 // Thanh toán
export const NOTIFY_TYPE_52 = 52 // Huỷ đơn
export const NOTIFY_TYPE_53 = 53 // Xác nhận yêu cầu hoá đơn
export const NOTIFY_TYPE_54 = 54 // Hoá đơn đã thêm thành viên tham gia

export const PAYMENT_METHOD = {
    'cash'     : 1, // Tiền mặt
    'transfer' : 2, // Chuyển khoản
    'vnpay'    : 3, // Vnpay
    'momo'     : 4, // Momo
    'ib'       : 5, // Internet banking
}

export const TIMEKEEPING_FEE_1 = 100000
export const TIMEKEEPING_MAX_STAFF_1 = 50

export const RESTAURANT_FEE_1 = 150
export const RESTAURANT_FEE_2 = 0.15
export const RESTAURANT_FEE_3 = 250000
