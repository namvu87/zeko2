const setting = {
    hideGoodIds: [],
    sound: true
}
export default {
    setItem(value) {
        window.localStorage.setItem('setting_kitchen', JSON.stringify(value))
    },
    getItem() {
        return {...setting, ...(JSON.parse(window.localStorage.getItem('setting_kitchen')) || {})}
    },
}
