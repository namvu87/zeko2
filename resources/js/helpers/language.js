export default {
    setLanguage(val) {
        window.axios.defaults.headers.common['X-Localization'] = val
        window.localStorage.setItem('language', val)
    },
    getLanguage() {
        return window.localStorage.getItem('language') || 'vi'
    }
}