const fields = {
    name: true,
    code: true,
    group_menus: false,
    price: true,
    price_origin: false,
    unit: true,
    inventory_number: true,
    inventory_min: false,
    inventory_max: false,
    discount: false,
    type: false,
    status: false,
    properties: true,
}
export default {
    setItem(fields) {
        window.localStorage.setItem('goods_fields', JSON.stringify(fields))
    },
    getItem() {
        return  JSON.parse(window.localStorage.getItem('goods_fields')) || fields
    }
}