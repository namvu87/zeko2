export default {
    setItem(token) {
        window.localStorage.setItem('firebase_token', token)
    },
    getItem() {
        return window.localStorage.getItem('firebase_token') || null
    },
    removeItem() {
        return window.localStorage.removeItem('firebase_token')
    }
}