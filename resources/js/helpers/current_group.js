export default {
    /**
     * Set current group data
     * @param Object groupData
     */
    setCurrentGroup(groupData) {
        window.localStorage.setItem('current_group', JSON.stringify(groupData))
    },
    getCurrentGroup() {
        return window.localStorage.getItem('current_group') || '{}'
    },
    removeCurrentGroup() {
        window.localStorage.removeItem('current_group')
    },
    /**
     * Set user's total groups data
     * @param array groups
     */
    setTotalGroups(groups) {
        window.localStorage.setItem('total_groups', JSON.stringify(groups))
    },
    getTotalGroups() {
        return window.localStorage.getItem('total_groups') != null &&
               window.localStorage.getItem('total_groups') != 'undefined' ?
            JSON.parse(window.localStorage.getItem('total_groups')) : []
    },
    removeTotalGroups() {
        window.localStorage.removeItem('total_groups')
    },
    /**
     * Set user's permissions in current group
     * @param array permissions
     */
    setCurrentPermission(permissions) {
        window.localStorage.setItem('current_permissions', JSON.stringify(permissions))
    },
    getCurrentPermissions() {
        return JSON.parse(window.localStorage.getItem('current_permissions')) || []
    },
    removeCurrentPermissions() {
        window.localStorage.removeItem('current_permissions')
    }
}