export default {
    setItem(template) {
        window.localStorage.setItem('template_import_invoice', JSON.stringify(template))
    },
    getItem() {
        return JSON.parse(window.localStorage.getItem('template_import_invoice'))
    },
    removeItem() {
        window.localStorage.removeItem('template_import_invoice')
    }
}