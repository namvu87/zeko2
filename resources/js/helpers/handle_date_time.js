import moment from 'moment'
import language from './language'

export function convertToDate(date) {
    if (language.getLanguage() === 'vi') {
        return moment(date).format('DD-MM-YYYY')
    }
    return moment(date).format('YYYY-MM-DD')
}

export function convertToYearMonthDay(date) {
    return moment(date).format('YYYY-MM-DD')
}

export function convertToDateMonthYear(date) {
    return moment(date).format('DD-MM-YYYY')
}

export function convertToDateTime(date) {
    if (language.getLanguage() === 'vi') {
        return moment(date).format('DD-MM-YYYY HH:mm')
    }
    return moment(date).format('YYYY-MM-DD HH:mm')
}

export function convertToTime(date) {
    return moment(date).format('HH:mm')
}

export function getStartAndEndDateByOption(option) {
    let startDate = moment().format('YYYY-MM-DD')
    let endDate = moment().format('YYYY-MM-DD')
    switch (option) {
        case 'yesterday':
            startDate = moment().subtract(1, 'days').format('YYYY-MM-DD')
            endDate = moment().subtract(1, 'days').format('YYYY-MM-DD')
            break
        case 'this_week':
            startDate = moment().startOf('isoWeek').format('YYYY-MM-DD')
            break
        case 'last_week':
            startDate = moment().subtract(1, 'weeks').startOf('isoWeek').format('YYYY-MM-DD')
            endDate = moment().subtract(1, 'weeks').endOf('isoWeek').format('YYYY-MM-DD')
            break
        case 'this_month':
            startDate = moment().startOf('month').format('YYYY-MM-DD')
            break
        case 'last_month':
            startDate = moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD')
            endDate = moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD')
            break
        case 'last_seven_days':
            startDate = moment().subtract(7, 'days').format('YYYY-MM-DD')
            break
        case 'last_thirty_days':
            startDate = moment().subtract(30, 'days').format('YYYY-MM-DD')
            break
        case 'this_quarter':
            startDate = moment().startOf('quarter').format('YYYY-MM-DD')
            break
        case 'last_quarter':
            startDate = moment().startOf('quarter').subtract(3, 'months').format('YYYY-MM-DD')
            endDate = moment().startOf('quarter').subtract(1, 'days').format('YYYY-MM-DD')
            break
        case 'this_year':
            startDate = moment().startOf('year').format('YYYY-MM-DD')
            break
        case 'last_year':
            startDate = moment().startOf('year').subtract(1, 'years').format('YYYY-MM-DD')
            endDate = moment().startOf('year').subtract(1, 'days').format('YYYY-MM-DD')
            break
        default:
            break
    }
    return { startDate: startDate, endDate: endDate }
}