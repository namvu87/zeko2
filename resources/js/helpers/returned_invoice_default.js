export default {
    setItem(template) {
        window.localStorage.setItem('template_returned_invoice', JSON.stringify(template))
    },
    getItem() {
        return JSON.parse(window.localStorage.getItem('template_returned_invoice'))
    },
    removeItem() {
        window.localStorage.removeItem('template_returned_invoice')
    }
}