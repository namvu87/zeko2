export default {
    setItem(template) {
        window.localStorage.setItem('template_sale_invoice', JSON.stringify(template))
    },
    getItem() {
        return JSON.parse(window.localStorage.getItem('template_sale_invoice'))
    },
    removeItem() {
        window.localStorage.removeItem('template_sale_invoice')
    }
}