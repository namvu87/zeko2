import { DISCOUNT_TYPE_PERCENT } from '../constants.js'

export function discountGoods(goods) {
    if (goods.discount_type === DISCOUNT_TYPE_PERCENT) {
        return parseInt(goods.price * goods.discount / 100)
    }
    return parseInt(goods.discount || 0)
}

export function priceGoodsAfterDiscount(goods) {
    return goods.price - discountGoods(goods)
}

export function subtotalInvoice(invoice) {
    let subtotal = 0
    for (let good of invoice.goods) {
        subtotal += priceGoodsAfterDiscount(good) * good.count
    }
    return parseInt(subtotal)
}

export function discountInvoice(invoice) {
    if (invoice.discount_type === DISCOUNT_TYPE_PERCENT) {
        return parseInt(subtotalInvoice(invoice) * invoice.discount / 100)
    }
    return parseInt(invoice.discount || 0)
}

export function totalInvoice(invoice) {
    let subtotal = subtotalInvoice(invoice)
    if (invoice.discount_type === DISCOUNT_TYPE_PERCENT) {
        return subtotal - parseInt(subtotal * invoice.discount / 100)
    }
    return subtotal - parseInt(invoice.discount || 0)
}