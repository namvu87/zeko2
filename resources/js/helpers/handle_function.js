import { DISCOUNT_TYPE_FIXED } from '../constants.js'
import language from './language.js'

export function handleSchedule(schedule) {
    Object.keys(schedule).forEach(function(key) {
        if (schedule[key] == 'on') {
            schedule[key] = true
        } else {
            schedule[key] = false
        }
    })
    return schedule
}

export function formatPrice(amount) {
    return new Intl.NumberFormat('vi-VN').format(amount || 0)
}

export function formatTextSearch(text) {
    var str = text
    str = str.toLowerCase()
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str.replace(/đ/g, 'd')
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, ' ')
    str = str.replace(/ + /g, ' ')
    str = str.trim()
    return str
}

// Giá discount của hàng hóa
export function priceGoodDiscount(good) {
    if (good.discount) {
        if (good.discount_type === DISCOUNT_TYPE_FIXED) {
            return parseInt(good.discount)
        } else {
            return parseInt(good.price * (parseFloat(good.discount) / 100))
        }
    }
    return 0
}

// Giá hàng hóa sau khi discount
export function priceGoodAfterDiscount(good) {
    if (good.discount) {
        if (good.discount_type === DISCOUNT_TYPE_FIXED) {
            return good.price - parseInt(good.discount)
        } else {
            return good.price * (1 - (parseFloat(good.discount) / 100))
        }
    }
    return good.price
}

export function getTotalPriceGoods(good) {
    return parseInt(good.count * this.priceGoodAfterDiscount(good))
}

export function getFullName(firstName, lastName) {
    let locale = language.getLanguage()
    if (locale === 'vi') {
        return lastName + ' ' + firstName
    }
    return firstName + ' ' + lastName
}

export function strToSlug(str) {
    var slug

    //Đổi chữ hoa thành chữ thường
    slug = str.toLowerCase()

    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a')
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e')
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i')
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o')
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u')
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y')
    slug = slug.replace(/đ/gi, 'd')
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/`|~|!|@|#|\||\$|%|\^|&|\*|\(|\)|\+|=|,|\.|\/|\?|>|<|'|"|:|;|_/gi, '')
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, '-')
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/-----/gi, '-')
    slug = slug.replace(/----/gi, '-')
    slug = slug.replace(/---/gi, '-')
    slug = slug.replace(/--/gi, '-')
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@'
    slug = slug.replace(/@-|-@|@/gi, '')
    //In slug ra textbox có id “slug”
    return slug
}
