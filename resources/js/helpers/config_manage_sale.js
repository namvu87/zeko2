export default {
    setItem(user) {
        window.localStorage.setItem('config_manage_sale', JSON.stringify(user))
    },
    getItem() {
        return JSON.parse(window.localStorage.getItem('config_manage_sale'))
    },
    removeItem() {
        window.localStorage.removeItem('config_manage_sale')
    }
}