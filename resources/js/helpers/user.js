export default {
    setItem(user) {
        window.localStorage.setItem('user_info', JSON.stringify(user))
    },
    getItem() {
        return window.localStorage.getItem('user_info') != null &&
               window.localStorage.getItem('user_info') != 'undefined' ?
            JSON.parse(window.localStorage.getItem('user_info')) : {}
    },
    removeItem() {
        window.localStorage.removeItem('user_info')
    }
}