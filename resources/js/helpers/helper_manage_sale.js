import moment from 'moment'
import { DISCOUNT_TYPE_FIXED, INVOICE_STATUS_PURCHASED, RETURNED_INVOICE_PAID } from '../constants.js'
import { priceGoodAfterDiscount } from './handle_function.js'
import i18n from './../i18n'
require('moment/locale/vi')
moment.locale(i18n.locale)

export function invoicePosition(tables, tableIds) {
    let result = ''
    if (typeof tableIds !== 'undefined') {
        const tablesFiltered = tables.filter(table => tableIds.includes(table.id))
        const position = []
        tablesFiltered.forEach(table => {
            position.push(`${table.name } - ${table.place_name}`)
        })
        result = (position.length > 0) ? `${position.toString()}` : ''
    }
    return result
}

export function momentTime(dateTime) {
    return moment(dateTime).format('HH:mm:ss')
}

export function momentFromNow(dateTime) {
    return moment(dateTime).fromNow()
}

export function momentDate(dateTime) {
    return moment(dateTime).format('DD-MM-YYYY')
}

export function filterTablesByTableIds(tables, tableIds) {
    return tables.filter(table => tableIds.includes(table.id))
}

export function getValueDiscountInvoice(discount, discountType) {
    const discountInvoice = discount ? discount : 0
    if (discountType === DISCOUNT_TYPE_FIXED) {
        return parseInt(discountInvoice)
    }
    return parseFloat(discountInvoice)
}

// Tính subtotal từ các món ăn trong hóa đơn trả hàng
export function calcSubtotalReturnInvoice(invoice) {
    if (typeof invoice.goods === 'undefined' || invoice.goods.length === 0) {
        return 0
    }
    return invoice.goods.reduce(((result, item) => result + item.return_count * priceGoodAfterDiscount(item)), 0)
}

// Lấy ra subtotal của hóa đơn trả hàng
export function subtotalReturnInvoice(invoice) {
    if (invoice.status === RETURNED_INVOICE_PAID) return invoice.total
    return calcSubtotalReturnInvoice(invoice)
}

// Tính subtotal từ các món ăn trong hóa đơn bán hàng
export function calcSubtotalPurchaseInvoice(invoice) {
    if (!invoice.goods || invoice.goods.length === 0){
        return 0
    }
    let subtotal = 0
    invoice.goods.forEach(item => {
        subtotal += item.count * priceGoodAfterDiscount(item)
    })
    return subtotal
}

// Lấy ra subtotal của hóa đơn bán hàng
export function subtotalPurchaseInvoice(invoice) {
    if (invoice.status === INVOICE_STATUS_PURCHASED) return invoice.total + discountPurchaseInvoice(invoice)
    return calcSubtotalPurchaseInvoice(invoice)
}

// Tính tiền giảm giá của hóa đơn mua hàng
export function discountPurchaseInvoice(invoice, subtotalInvoice = 0) {
    if (!invoice.discount) {
        return 0
    }
    // discount_type bằng DISCOUNT_TYPE_FIXED
    if (invoice.discount_type === DISCOUNT_TYPE_FIXED) {
        return parseInt(invoice.discount)
    }
    // discount_type bằng DISCOUNT_TYPE_PERCENT
    if (invoice.status === INVOICE_STATUS_PURCHASED) {
        return invoice.total / (1 - (parseFloat(invoice.discount) / 100))
    }
    return subtotalInvoice * (parseFloat(invoice.discount) / 100)
}

// Tính tiền khách trả cho khách
export function totalReturnInvoice(invoice) {
    if (invoice.status === RETURNED_INVOICE_PAID) return invoice.total
    return calcSubtotalReturnInvoice(invoice)
}

// Tính tiền hóa đơn mua hàng khách phải trả
export function totalPurchaseInvoice(invoice) {
    if (invoice.status === INVOICE_STATUS_PURCHASED) return invoice.total
    const subtotal = calcSubtotalPurchaseInvoice(invoice)
    const discount = discountPurchaseInvoice(invoice, subtotal)
    return subtotal - discount
}
