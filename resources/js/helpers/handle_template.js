export default {
    escapeRegExp(str) {
        return str.replace(/([.*+?^=!:${}()|[\]/\\])/g, '\\$1')
    },
    replaceAll(str, find, replace) {
        return str.replace(new RegExp(this.escapeRegExp(find), 'g'), replace)
    },
    matchAll(str, value) {
        return str.match(new RegExp(this.escapeRegExp(value), 'g')) || []
    }
}