export default {
    setItem(token) {
        window.localStorage.setItem('access_token', token)
    },
    getItem() {
        return window.localStorage.getItem('access_token')
    },
    removeItem() {
        window.localStorage.removeItem('access_token')
    }
}