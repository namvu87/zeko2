export default {
    'vi': {
        'Ma_Phieu': 'II000001',
        'Nhan_Vien_Phu_Trach': 'Trần Quang Việt',
        'Ngay_Tao': '20/02/2019 11:34',
        'Tong_Tien': '4,950,000',
        'Ghi_Chu': 'Nhập hàng lần tiếp được ưu đãi 10%',
        'Goods': [
            {
                'Ten_Hang': 'Rượu quê',
                'Ma_Hang': 'P0000001',
                'So_Luong': '20 Lit',
                'Don_Gia': '25,000',
                'Khuyen_Mai': '50,000 VND',
                'Thanh_Tien': '450,000',
            },
            {
                'Ten_Hang': 'Bia Hà Nội',
                'Ma_Hang': 'P0000100',
                'So_Luong': '500',
                'Don_Gia': '10,000',
                'Khuyen_Mai': '10%',
                'Thanh_Tien': '4,500,000',
            }
        ],
    },
    'en': {
        'Invoice_Code': 'II000001',
        'Employee_Name': 'Trần Quang Việt',
        'Date_Create_Invoice': '20/02/2019 11:34:54',
        'Total_Money': '4,950,000',
        'Note': '',
        'Goods': [
            {
                'Goods_Code': 'Rượu quê',
                'Goods_Name': 'P0000001',
                'Goods_Count': '20 Lit',
                'Goods_Price': '25,000',
                'Goods_Discount': '50,000',
                'Goods_Total_Price': '450,000',
            },
            {
                'Goods_Code': 'Bia Hà Nội',
                'Goods_Name': 'P0000100',
                'Goods_Count': '500',
                'Goods_Price': '10,000',
                'Goods_Discount': '10%',
                'Goods_Total_Price': '4,500,000',
            }
        ],
    }
}