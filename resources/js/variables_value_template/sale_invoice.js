export default {
    'vi': {
        'Ma_Hoa_Don': 'HD000001',
        'Ten_Nha_Hang': 'Quán ngon',
        'Dia_Chi_Nha_Hang': 'Số 63, Phường Hà Huy Tập, Nghệ An - Thành phố Vinh',
        'Ngay_Ban': '20/02/2019 11:34',
        'Danh_Sach_Ban': 'Bàn 2 - Tầng 1',
        'Nhan_Vien_Ban_Hang': 'Trần Quang Việt',
        'Tong_Tien': '900,000',
        'Giam_Gia_Don_Hang': '50,000',
        'Tong_Tien_Phai_Thanh_Toan': '850,000',
        'Ghi_Chu': 'Khách hài lòng với bữa ăn',
        'Goods': [
            {
                'Ten_Hang': 'Vịt quay bắc kinh',
                'Ma_Hang': 'P0000010',
                'So_Luong': '2',
                'Don_Gia': '160,000',
                'Giam_Gia': '10,000',
                'Gia_Sau_Khi_Giam': '150,000',
                'Thanh_Tien': '300,000',
            },
            {
                'Ten_Hang': 'Gà đắp đất',
                'Ma_Hang': 'P0000100',
                'So_Luong': '3',
                'Don_Gia': '200,000',
                'Giam_Gia': '0',
                'Gia_Sau_Khi_Giam': '200,000',
                'Thanh_Tien': '600,000',
            }
        ],
    },
    'en': {
        'Current_Invoice_Code': 'HD000001',
        'Restaurant_Name': 'Quán ngon',
        'Restaurant_Address': 'Số 63, Phường Hà Huy Tập, Nghệ An - Thành phố Vinh',
        'Date_Time_Create_Invoice': '20/02/2019 11:34:54',
        'Tables': 'Bàn 2 - Tầng 1',
        'Employee_Name': 'Trần Quang Việt',
        'Grand_Total': '900,000',
        'Discount_Invoice': '50,000',
        'Paid_Total_Money': '850,000',
        'Note': '',
        'Goods': [
            {
                'Goods_Name': 'Vịt quay bắc kinh',
                'Goods_Code': 'P0000010',
                'Goods_Count': '2',
                'Goods_Price': '160,000',
                'Goods_Discount': '10,000',
                'Price_After_Discount': '150,000',
                'Goods_Total_Price': '300,000',
            },
            {
                'Goods_Name': 'Gà đắp đất',
                'Goods_Code': 'P0000100',
                'Goods_Count': '3',
                'Goods_Price': '200,000',
                'Goods_Discount': '0',
                'Price_After_Discount': '200,000',
                'Goods_Total_Price': '600,000',
            }
        ],
    }
}