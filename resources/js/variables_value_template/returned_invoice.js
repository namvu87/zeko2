export default {
    'vi': {
        'Ma_Hoa_Don': 'RI000001',
        'Ma_Hoa_Don_Ban_Hang': 'HD000001',
        'Ten_Nha_Hang': 'Quán ngon',
        'Dia_Chi_Nha_Hang': 'Số 63, Phường Hà Huy Tập, Nghệ An - Thành phố Vinh',
        'Ngay_Ban': '20/02/2019 11:34',
        'Nhan_Vien_Ban_Hang': 'Trần Quang Việt',
        'Tong_Tien': '900,000',
        'Ghi_Chu': 'Khách hài lòng với bữa ăn',
        'Goods': [
            {
                'Ten_Hang': 'Vịt quay bắc kinh',
                'Ma_Hang': 'P0000010',
                'So_Luong': '2',
                'Don_Gia': '150,000',
                'Thanh_Tien': '300,000',
            },
            {
                'Ten_Hang': 'Gà đắp đất',
                'Ma_Hang': 'P0000100',
                'So_Luong': '3',
                'Don_Gia': '200,000',
                'Thanh_Tien': '600,000',
            }
        ],
    },
    'en': {
        'Returned_Invoice_Code': 'RI000001',
        'Sale_Invoice_Code': 'HD000001',
        'Restaurant_Name': 'Quán ngon',
        'Restaurant_Address': 'Số 63, Phường Hà Huy Tập, Nghệ An - Thành phố Vinh',
        'Date_Time_Create_Invoice': '20/02/2019 11:34:54',
        'Employee_Name': 'Trần Quang Việt',
        'Grand_Total': '900,000',
        'Note': '',
        'Goods': [
            {
                'Goods_Name': 'Vịt quay bắc kinh',
                'Goods_Code': 'P0000010',
                'Goods_Count': '2',
                'Goods_Price': '150,000',
                'Goods_Total_Price': '300,000',
            },
            {
                'Goods_Name': 'Gà đắp đất',
                'Goods_Code': 'P0000100',
                'Goods_Count': '3',
                'Goods_Price': '200,000',
                'Goods_Total_Price': '600,000',
            }
        ],
    }
}