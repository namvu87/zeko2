@component('mail::layout')

@slot('header')
	@component('mail::header', ['url' => config('app.url')])
		{{ config('app.name') }}
	@endcomponent
@endslot

@lang('mail.general.greeting') {{ $name }} <br>

@lang('mail.reset_password.content')

@component('mail::button', ['url' => $url])
@lang('mail.reset_password.action')
@endcomponent

@lang('mail.reset_password.expire_time', ['count' => config('auth.passwords.users.expire')])

@lang('mail.reset_password.additional_content')

@lang('mail.regards'), {{ config('app.name') }}

@slot('subcopy')
	@component('mail::subcopy')
		@lang('mail.reset_password.subcopy',
			[
				'url' => $url
			]
		)
	@endcomponent
@endslot

@slot('footer')
	@component('mail::footer')
		&copy; {{ date('Y') }} {{ config('app.name') }}. @lang('mail.all_right_reversed') <br>
		@lang('mail.general.to') {{ $email }}
	@endcomponent
@endslot

@endcomponent
