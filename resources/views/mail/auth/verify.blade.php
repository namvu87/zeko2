@component('mail::layout')

@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

@lang('mail.general.greeting') {{ $name }} <br>
@lang('mail.verify_email.line_1')

@component('mail::button', ['url' => $url])
@lang('mail.verify_email.action')
@endcomponent

@lang('mail.verify_email.line_2')
@lang('mail.regards'),
{{ config('app.name') }}


@slot('subcopy')
@component('mail::subcopy')

@lang('mail.verify_email.line_3',
    [
        'url' => $url
    ]
)

@endcomponent
@endslot

    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}. @lang('mail.all_right_reversed') <br>
            @lang('mail.general.to') {{ $email }}
        @endcomponent
    @endslot
@endcomponent
