@component('mail::layout')

    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
        {{ config('app.name') }}
        @endcomponent
    @endslot
    @lang('mail.payment_periodic.header', [
        'groupName' => $group->name
    ])
    @switch($group->service_type)
        @case(\App\Models\Group::TIMEKEEPING_SERVICE)
            @lang('mail.payment_periodic.fee_info_restaurant', [
                'fee' => number_format(config('payment_package.timekeeping.fee'))
            ])
            @break
        @case(\App\Models\Group::RESTAURANT_SERVICE)
            @php
                $fee1 = $fee->invoices_count * config('payment_package.restaurant.1.fee');
                $fee2 = $fee->revenue * config('payment_package.restaurant.2.fee') / 100;
                $fee3 = config('payment_package.restaurant.3.fee');
            @endphp
            @lang('mail.payment_periodic.fee_info_restaurant', [
                'invoice_quantity' => number_format($fee->invoices_count),
                'revenue' => number_format($fee->revenue),
                'fee1' => number_format($fee1),
                'fee2' => number_format($fee2),
                'fee3' => number_format($fee3),
                'fee' => number_format(min($fee1, $fee2, $fee3))
            ])
            @break
    @endswitch
    @lang('mail.payment_periodic.footer', [
        'date' => config('payment_package.out_of_date') . '/' . date('m')
    ])
    @component('mail::button', ['url' => config('app.url') . '/service-payment'])
        @lang('word.payment')
    @endcomponent

    @slot('footer')
        @component('mail::footer')
        &copy; {{ date('Y') }} {{ config('app.name') }}. Zeko Support Team. <br>
        @endcomponent
    @endslot
@endcomponent
