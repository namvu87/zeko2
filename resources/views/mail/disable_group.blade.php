@component('mail::layout')

    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot
    @lang('mail.disabled_group', [
        'group' => $group->name
    ])

    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}. Zeko Support Team. <br>
        @endcomponent
    @endslot

@endcomponent
