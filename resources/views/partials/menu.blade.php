<div class="menu-responsive">
    <ul class="nav-responsive">
        <li class="nav-responsive__li{{ Request::routeIs('home') ? ' active' : '' }}"><a href="{{ route('home') }}"
                                                                                         class="nav-responsive__link"><i
                        class="fas fa-home nav__icon"></i>@lang('word.home')</a></li>
        <li class="nav-responsive__li{{ Request::routeIs('news.list') || Request::is('list/news/*') ? ' active' : '' }}">
            <a href="{{ route('news.list') }}" class="nav-responsive__link"><i
                        class="far fa-edit nav__icon"></i>@lang('word.news')</a></li>
        <li class="nav-responsive__li{{ Request::routeIs('career') ? ' active' : '' }}"><a href="{{ route('career') }}"
                                                                                           class="nav-responsive__link"><i
                        class="fas fa-users nav__icon"></i>@lang('word.career')</a></li>
        <li class="nav-responsive__li{{ Request::routeIs('guide.1') || Request::routeIs('guide.2') ? ' active' : '' }} js-modal-service">
            <a href="#" class="nav-responsive__link">
                <i class="fas fa-chalkboard-teacher nav__icon"></i> @lang('word.guide')</a>
        </li>
        <li class="nav-responsive__li{{ Request::routeIs('contact') ? ' active' : '' }}"><a
                    href="{{ route('contact') }}" class="nav-responsive__link"><i
                        class="fas fa-map-marker-alt nav__icon"></i>@lang('word.contact')</a></li>
    </ul>
</div>

<button type="button" class="menuBtn">
    <span></span>
</button>

<button type="button" class="hideBtn">
    <span></span>
</button>
<!--end menu Responsive-->