<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="{{ $desc ?? __('tag.common.desc') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="author" content="ZEKO LTD">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.png') }}">
<title>{{ $title ?? __('tag.common.title') }}</title>

<link rel="shortcut icon" href="images/favicon.png">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('payment/css/main-styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/main-style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/main-app.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('awesome/css/all.css') }}">
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        'siteName'  => config('app.name'),
        'apiDomain' => config('app.sub_url'),
        'webDomain' => config('app.url'),
        'locale' => config('app.locale')
    ]) !!}
</script>
