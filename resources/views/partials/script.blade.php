<script src="{{ asset('payment/js/jquery.min.js') }}"></script>
<script src="{{ asset('payment/js/lib/bootstrap.min.js') }}"></script>
<!-- import datepicker for Orderlist -->
<script src="{{ asset('payment/js/lib/bootstrap-datepicker.min.js') }}"></script>

<script src="{{ asset('payment/js/main-scripts.js') }}"></script>

<!--js for Slide-->
<script src="{{ asset('payment/js/slide.js') }}"></script>