<header class="header">
    <!-- header top -->
    <div class="header__top">
        <div class="container">
            <div class="row">
                <!-- logo -->
                <div class="header__logo fl-l">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('payment/images/icon.png') }}" alt="logo" class="logo__img"/>
                    </a>
                </div>
                <!-- e logo -->

                <div class="header__link">
                    <a href="{{ route('about') }}">@lang('word.about')</a>
                    <a href="{{ route('policy') }}">@lang('word.policy')</a>
                    <a href="{{ route('rules') }}">@lang('word.rules')</a>
                    <a href="{{ route('costs') }}">@lang('word.costs')</a>
                </div>
                <!--header option-->
                <div class="header__option" style="justify-content: flex-end;">
                    <div class="header__account">
                        <a href="{{ config('app.sub_url') . '/login' }}" title="@lang('word.login')"
                           style="color:#FFF;" target="_blank">
                            <span style="padding-right: 15px"><strong>@lang('word.login')</strong></span>
                        </a>
                        <a href="{{ config('app.sub_url') . '/register' }}" title="@lang('word.sign_up')"
                           style="color:#FFF;" target="_blank"><span><strong>@lang('word.sign_up')</strong></span>
                        </a>
                    </div>
                    {{--<div class="locale">--}}
                        {{--<div class="locale-dropdown" id="#language">--}}
                            {{--@if (session('website_language') == 'vi' || config('app.locale') == 'vi' || !config('app.locale'))--}}
                                {{--<img class="lang-icon lang-select" src="/images/lang/vn.svg" alt="">--}}
                            {{--@elseif (session('website_language') == 'en' || config('app.locale') == 'en')--}}
                                {{--<img class="lang-icon lang-select" src="/images/lang/en.svg" alt="">--}}
                            {{--@endif--}}
                            {{--<div class="dropdown-lang">--}}
                                {{--<ul class="">--}}
                                    {{--<li>--}}
                                        {{--<a href="{{ route('change-language', 'vi') }}">--}}
                                            {{--<div class="img">--}}
                                                {{--<img class="lang-icon" src="/images/lang/vn.svg"--}}
                                                     {{--alt="@lang('config.language.0')">--}}
                                            {{--</div>--}}
                                            {{--<div class="text">@lang('config.language.0')</div>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="{{ route('change-language', 'en') }}">--}}
                                            {{--<div class="img">--}}
                                                {{--<img class="lang-icon" src="/images/lang/en.svg"--}}
                                                     {{--alt="@lang('config.language.1')">--}}
                                            {{--</div>--}}
                                            {{--<div class="text">@lang('config.language.1')</div>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
                <!--end header option-->
            </div>
        </div>
    </div>
    <!-- e header top -->
    <div class="header__bottom">
        <div class="container">
            <div class="row">
                <!-- logo -->
                <div class="header__logo fl-l">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('payment/images/icon.png') }}" alt="logo" class="logo__img"/>
                    </a>
                </div>
                <!-- e logo -->
                <!--menu main-->
                <nav class="nav fl-r">
                    <ul>
                        <li class="nav__li{{ Request::routeIs('home') ? ' active' : '' }}"><a href="{{ route('home') }}"
                                                                                              class="nav__link"><i
                                        class="fas fa-home nav__icon"></i>@lang('word.home')</a></li>
                        <li class="nav__li{{ Request::routeIs('news.list') ? ' active' : '' }}">
                            <a href="{{ route('news.list') }}" class="nav__link"><i
                                        class="far fa-edit nav__icon"></i>@lang('word.news')</a></li>
                        <li class="nav__li{{ Request::routeIs('career') ? ' active' : '' }}"><a
                                    href="{{ route('career') }}"
                                    class="nav__link"><i
                                        class="fas fa-users nav__icon"></i> @lang('word.career')</a></li>
                        <li class="nav__li{{ Request::routeIs('guide.1') || Request::routeIs('guide.2') ? ' active' : '' }} js-modal-service">
                            <a href="#" class="nav__link">
                                <i class="fas fa-chalkboard-teacher nav__icon"></i> @lang('word.guide')</a>
                        </li>
                        <li class="nav__li{{ Request::routeIs('contact') ? ' active' : '' }}"><a
                                    href="{{ route('contact') }}" class="nav__link"><i
                                        class="fas fa-map-marker-alt nav__icon"></i>@lang('word.contact')</a></li>
                    </ul>
                </nav>
                <!--menu main-->
            </div>
        </div>
    </div>
</header>
<div class="modal-service">
    <div class="modal-box">
        <div class="modal-box__heading text-center">
            <h3>
                <b>@lang("common.select_service_to_guide")</b>
            </h3>
        </div>
        <div class="row modal-box__body">
            <div class="col-sm-6 text-center">
                <a href="{{ route('guide.1') }}" class="box-item">
                    <i class="fa fa-calendar"></i>
                    <b>@lang("common.timekeeping_application_qr")</b>
                </a>
            </div>
            <div class="col-sm-6 text-center">
                <a href="{{ route('guide.2') }}" class="box-item">
                    <i class="fa fa-coffee"></i>
                    <b>@lang("common.restaurant_manage")</b>
                </a>
            </div>
        </div>
        <div class="modal-box__close float-right">
            <span><i class="fa fa-times"></i></span>
        </div>
    </div>
</div>