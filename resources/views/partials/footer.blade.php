<footer class="footer">
    <div class="footer__top">
        <div class="container">
            <div class="row">
                <!-- col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="footer__title"><span>@lang('word.zeko_company')</span></div>
                    <div class="footer__content">
                        <div class="footer__description" style="text-align: justify; padding: 10px 0;">
                            <span>@lang('word.text_footer')</span><br>
                        </div>
                        <div class="slogan" style="font-size: 14px; color: hsla(0,0%,100%,.6); text-align: center;">
                            <span>"@lang('word.slogan')"</span>
                        </div>
                    </div>
                </div>
                <!-- e col -->
                <!-- col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="footer__title"><span>@lang('word.local_information')</span></div>
                    <div class="footer__content">
                        <div class="footer__content__row">
                            <span><i class="fa fa-phone footer__icon"></i></span>
                            <span class="footer__text">@lang('word.hotline'): {{ $contact->phone_number ?? '' }}</span>
                        </div>
                        <div class="footer__content__row">
                            <span><i class="fa fa-map-marker-alt footer__icon"></i></span>
                            <span class="footer__text">@lang('word.address'): {{ $contact->address ?? '' }}</span>
                        </div>
                        <div class="footer__content__row">
                            <div class="goto-map-btn"><a href="{{ route('contact') }}">@lang('word.map_company')</a></div>
                        </div>
                    </div>
                </div>
                <!-- e col -->
                <!-- col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="footer__title"><span>Tags</span></div>
                    <div class="footer__content">
                        <div class="footer__content__row">
                            @if (count($topTags) > 0)
                                @foreach($topTags as $tag)
                                    <a href="{{ route('news.list', $tag->name) }}" class="tag-link">{{ $tag->name }}</a>
                                @endforeach
                            @endif
                        </div>
                        <div class="footer__content__row download-app">
                            <div class="link-download">
                                <a href="https://play.google.com/store/apps/details?id=vn.zeko.app" target="_blank">
                                    <img alt="Google Play" height="32" src="{{ asset('images/app/gg-play.svg') }}">
                                </a>
                            </div>
                            <div class="link-download">
                                <a href="https://itunes.apple.com/us/app/zeko/id1415955600?l=vi&ls=1&mt=8" target="_blank">
                                    <img alt="Google Play" height="32" src="{{ asset('images/app/ios-logo.svg') }}">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- e col -->
            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <span>© 2018. All rights reserved by <a href="http://zeko.vn">ZEKO</a></span>
    </div>
</footer>
