<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('manager.parts.header')
  </head>
<body class="fix-header">
  <div id="app">
    <app></app>
  </div>
  @include('manager.parts.script')
</body>
</html>