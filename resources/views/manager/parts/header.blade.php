<meta name="robots" content="noindex">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'>
<meta name="description" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="author" content="Zeko Inc">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.png') }}">
<title>Trang quản trị</title>
<link href="{{ asset('awesome/css/all.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-grid.min.css') }}" rel="stylesheet">

{{-- <link href="{{ asset('css/sidebar-nav.min.css') }}" rel="stylesheet"> --}}
{{-- <link href="{{ asset('css/animate.css') }}" rel="stylesheet"> --}}
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/colors/blue.css') }}" id="theme" rel="stylesheet">

<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        'siteName'  => config('app.name'),
        'app_url'   => config('app.url'),
        'webDomain' => config('app.url'),
        'locale'    => session('manage_language') != null ? session('manage_language') : config('app.locale')
    ]) !!}
</script>