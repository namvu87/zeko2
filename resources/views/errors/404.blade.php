<html>
<head>
<title>404 not found</title>
<!-- Custom Theme files -->
<link href="{{ asset('css/error.css') }}" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="404 not found"  />
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--Google Fonts-->
</head>
<body>
<!--404 page start here-->
<div class="not-found">
  <div class="notfound-top">
    <h1>404</h1>
  </div>
  <div class="content">
    <img src="{{ asset('images/error/blue.png') }}">
    <h3 style="margin-top: 75px;">@lang('error.404')</h3>
    <a href="javascript:history.back()">@lang('error.go_back')</a>
  </div>
  <div class="clear"></div>
</div>
</body>
</html>