<!DOCTYPE HTML>
<html>
<head>
<title>500 internal server error</title>
<!-- Custom Theme files -->
<link href="{{ asset('css/error.css') }}" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Creative 404  Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--Google Fonts-->
</head>
<body>
<!--404 page start here-->
<div class="not-found">
  <div class="notfound-top">
    <h1>500</h1>
  </div>
  <div class="content">
    <img src="/images/error/blue.png" alt="" title="">
    <h3 style="margin-top: 75px;">@lang('error.500')</h3>
    <a href="javascript:history.back()">@lang('error.go_back')</a>
  </div>
  <div class="clear"></div>
</div>
</body>
</html>