<!DOCTYPE html>
<html>
<head>
  <title>Social Login for ZEKO application</title>
  <script>
    window.opener.postMessage({
      token: "{{ $token }}",
      user: "{{ json_encode($user) }}"
    }, "{{ url('') }}")
    window.close()
  </script>
</head>
<body>
</body>
</html>