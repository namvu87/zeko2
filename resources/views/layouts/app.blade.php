<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials.header', [
          'title' => $title ?? null,
          'desc' => $desc ?? null,
          'keyword' => $keyword ?? null
        ])
    <style>
        .alert {
            margin-bottom: 0px;
        }

        .alert-warning {
            background: #EFAC56;
            color: #FFF;
        }

        .alert-success {
            background: #479C48;
            color: #FFF;
        }

        .alert-info {
            background: #EFAC56;
            color: #FFF;
        }

        .alert-error {
            background: #FFFFFF;
            color: #FFF;
        }

        .scroll-top {
            display: none;
            position: fixed;
            bottom: 30px;
            right: 100px;
            font-size: 30px;
            z-index: 1;
            cursor: pointer;
            color: #1792fc;
        }

        .locale {
            float: left;
            height: 50px;
            cursor: pointer;
        }

        .lang-icon {
            width: 30px;
        }

        .lang-select {
            height: 50px;
            width: 50px;
            padding-left: 20px;
        }

        .locale-dropdown .dropdown-lang {
            display: none;
            z-index: 1;
            position: absolute;
            width: 130px;
            top: 51px;
            right: 25px;
            border-radius: 0;
            background-color: #fff;
        }

        .lang-icon:hover + .dropdown-lang {
            display: block;
        }

        .dropdown-lang:hover {
            display: block;
        }

        .locale-dropdown .dropdown-lang li a {
            padding: 0px;
        }

        .locale-dropdown .dropdown-lang li a:hover {
            background-image: none;
            color: #0067b4;
        }

        .locale-dropdown .dropdown-lang .img {
            display: inline;
        }

        .locale-dropdown .dropdown-lang .text {
            display: inline;
        }

        .locale-dropdown .dropdown-lang img {
            margin: 5px 10px;
        }

        @media (max-width: 979px) {
            .header__account {
                margin-right: 0px;
            }

            .large_device {
                display: none;
            }
        }

        .modal-service {
            position: fixed;
            display: none;
            width: 100%;
            height: 100%;
            top: 0;
            z-index: 100;
            background-color: rgba(0, 0, 0, 0.3);
        }

        .modal-service .modal-box {
            margin: 5% auto;
            max-width: 550px;
            position: relative;
            border-radius: 10px;
            padding: 15px;
            border-color: #ddd;
            background-color: white;
        }

        .modal-box__heading {
            color: #333;
            margin: 0 100px;
        }

        .modal-box__body {
            padding: 15px 0;
        }

        @media (max-width: 768px) {
            .modal-service {
                padding: 15px;
            }

            .modal-box__heading {
                margin: 0;
            }
        }

        .modal-box__close {
            position: absolute;
            top: 3px;
            right: 11px;
            font-size: 20px;
            font-weight: 500;
            color: #CCCCCC;
            cursor: pointer;
        }

        .box-item {
            padding: 20px;
            display: grid;
            border-radius: 4px;
            /*margin: 15px;*/
            min-height: 170px;
            cursor: pointer;
        }

        .box-item i {
            color: #007bff;
            font-size: 60px;
            margin: 10px 0 20px 0;
        }

        .box-item b {
            color: #171717;
        }

        .box-item:hover {
            background: #007bff;
        }

        .box-item:hover i {
            color: #ffffff;
        }

        .box-item:hover b {
            color: #ffffff;
        }

        .categories-banner {
            padding: 30px 30px 0;
            font-size: 16px;
            text-align: center;
        }

        .categories-banner input {
            margin-bottom: 30px;
            border-radius: 25px;
            outline: none;
            height: 50px;
        }
    </style>
    @yield('style')
</head>
<body><!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml: true,
      version: 'v3.3'
    });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="330392040824782"
     theme_color="#0084ff"
     logged_in_greeting="@lang('word.start_messenger')"
     logged_out_greeting="@lang('word.start_messenger')">
</div>
@if (session('info'))
    <div class="alert alert-warning"><b>{{ session('info') }}</b></div>
@endif
@if (session('error'))
    <div class="alert alert-danger"><b>{{ session('error') }}</b></div>
@endif
@if (session('success'))
    <div class="alert alert-success"><b>{{ session('success') }}</b></div>
@endif
@if (session('warning'))
    <div class="alert alert-warning"><b>{{ session('warning') }}</b></div>
@endif
@include('partials.menu')
@include('partials.navbar-top')
@yield('navbar-bottom')
<div class="breadcrumb">
    <div class="container">
        <div class="row">
            @yield('breadcrumb')
        </div>
    </div>
</div>
<div class="homepage">
    <div class="container">
        @yield('content')
    </div>
</div>
<div class="scroll-top">
    <span><i class="fas fa-chevron-circle-up"></i></span>
</div>
@include('partials.footer')
@include('partials.script')
<script>
  setTimeout(function() {
    $('.alert').slideUp(500);
  }, 5000);
  $('.js-modal-service').click(function() {
    $('.modal-service').fadeIn()
  })
  $('.modal-box__close').click(function() {
    $('.modal-service').fadeOut()
  })
  $(document).ready(function() {
    $(window).scroll(function() {
      if ($(this).scrollTop() > $(window).height()) {
        $('.scroll-top').fadeIn();
      } else {
        $('.scroll-top').fadeOut();
      }
    })
    $('.scroll-top').click(function() {
      $('html, body').animate({ scrollTop: 0 }, 800);
      return false;
    })
  })
</script>
@yield('script')
</body>
</html>