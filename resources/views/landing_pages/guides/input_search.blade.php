<form action="{{ route('guide.search') }}" method="get" class="row">
    <input type="text" name="title" placeholder="@lang('word.can_i_help_you')" value="{{ app('request')->title }}"
           class="col-sm-6 col-sm-offset-3 input-search">
</form>