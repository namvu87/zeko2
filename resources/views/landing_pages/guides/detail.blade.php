@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/guide.css') }}">
@endsection

@section('navbar-bottom')
    <div class="categories-banner container-fluid">
        @include('landing_pages.guides.input_search')
        <div class="container p-relative js-container-category">
            <div class="btn-mouse-wheel" onclick="scrollToLeft()"><i class="fas fa-arrow-alt-left"></i></div>
            <div class="nav-category js-category">
                @foreach($categories as $key => $category)
                    <a class="{{ $key == $guide->category ? 'active' : '' }}"
                       href="{{ route('guide.detail.' . $service, ['slug' => $category['slug']]) }}">
                        <span>{{ $category['name'] }}</span>
                    </a>
                @endforeach
            </div>
            <div class="btn-mouse-wheel" onclick="scrollToRight()"><i class="fas fa-arrow-alt-right"></i></div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-12">
        <span class="breadcrumb__item">Zeko web</span>
        <span class="fas fa-caret-right"></span>
        <a class="breadcrumb__item" href="{{ route('guide.' . $service) }}">@lang('word.guide')</a>
        <span class="fas fa-caret-right"></span>
        <a class="breadcrumb__item"
           href="{{ route('guide.detail.' . $service, ['slug' => $guide->slug]) }}">{{ $categories[$guide->category]['name'] }}</a>
        <span class="fas fa-caret-right"></span>
        <span class="breadcrumb__item">{{ $guide->title }}</span>
    </div>
@endsection

@section('content')
    <div class="row p-relative">
        <div class="col-sm-3 guide-sidebar js-sticky">
            <h2 class="guide__name">{{ $categories[$guide->category]['name'] }}</h2>
            <ul class="guide-lv1">
                @foreach($guides as $key => $gui)
                    @if($guide->slug == $gui->slug)
                        <li class="active">
                            <a data-toggle="collapse" data-target="#collapse" aria-expanded="true">{{ $gui->title }}</a>
                            @if(count($guide->hashes) > 0)
                                <ul class="guide-lv2 collapse in" id="collapse" aria-expanded="true">
                                    @foreach($guide->hashes as $hash)
                                        <li class="" id="{{ $hash }}" onclick="scrollToTitle(this.id)">
                                            <span>{{ $hash }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @else
                        <li>
                            <a href="{{ route('guide.detail.' . $service, ['slug' => $gui->slug]) }}">{{ $gui->title }}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="col-sm-9 js-content">
            <h2>{{ $guide->title }}</h2>
            <div class="guide-content">
                <div>{!! $guide->content !!}</div>
                <p style="font-size: 1.1rem">@lang('word.end_guide')</p>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('js/jquery.mousewheel.js') }}"></script>
    <script type="text/javascript">
      function scrollToTitle(id) {
        $('html, body').animate({
          scrollTop: $(`h2[id=\'${id}\']`).offset().top
        }, 1000);
      }

      function getClass() {
        var sticky = $('.js-sticky');
        var content = $('.js-content');
        let fromTop = window.scrollY;
        if (fromTop > content.offset().top) {
          if (fromTop < (content.offset().top + content.height() - sticky.height())) {
            sticky.addClass('sticky');
            sticky.removeClass('sticky--absolute');
          } else {
            sticky.addClass('sticky--absolute');
            sticky.removeClass('sticky');
          }
          content.addClass('col-sm-offset-3')
        } else {
          sticky.removeClass('sticky');
          sticky.removeClass('sticky--absolute');
          content.removeClass('col-sm-offset-3')
        }
        var nav2 = document.querySelectorAll(".guide-lv2 li");
        var selectorOld = '';
        for (let selector of nav2) {
          let id = selector.getAttribute('id');
          let contentId = $(`h2[id=\'${id}\']`);
          if (fromTop >= parseInt(contentId.offset().top)) {
            $(selectorOld).removeClass('active');
            $(selector).addClass('active');
            selectorOld = selector;
          } else {
            $(selector).removeClass('active');
          }
        }
      }

      function scrollToLeft() {
        $('.js-category').animate({
          scrollLeft: window.scrollX
        }, 1000)
      }

      function scrollToRight() {
        $('.js-category').animate({
          scrollLeft: $(this).width()
        }, 1000)
      }

      $(window).load(function() {
        this.getClass();
        $(window).scroll(function() {
          this.getClass()
        })
        $('.js-category').mousewheel(function(e, delta) {
          this.scrollLeft -= delta * 40;
          e.preventDefault();
        })
        $('.js-container-category').mouseover(() => {
          $('.btn-mouse-wheel').fadeIn()
        })
        $('.js-container-category').mouseleave(() => {
          $('.btn-mouse-wheel').fadeOut()
        })
      })
    </script>
@endsection