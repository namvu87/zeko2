@extends('layouts.app')
@section('style')
    <style>
        .homepage {
            display: none;
        }

        .item-guide {
            padding: 15px;
            border-bottom: 1px solid #e8e8e8;
        }

        .icon {
            color: #007bff;
            vertical-align: middle;
        }

        .item-guide__title {
            font-weight: bold;
            margin-bottom: 10px;
        }
    </style>
@endsection
@section('navbar-bottom')
    <div class="categories-banner container-fluid">
        @include('landing_pages.guides.input_search')
    </div>
@endsection
@section('breadcrumb')
    @if($guides->total() > 0)
        <h3>@lang('word.result_guide_search', ['keyword' => app('request')->input('title')])</h3>
    @else
        <h3>@lang('word.no_result')</h3>
    @endif
    <div class="list-result">
        @foreach($guides as $guide)
            @foreach($guide->service_type as $service)
                <div class="item-guide">
                    <h4 class="item-guide__title">
                        <a href="{{ route('guide.detail.' . $service, ['slug' => $guide->slug]) }}">
                            <span class="icon"><i class="fa {{ $service == 1 ? 'fa-calendar' : 'fa-coffee' }}"></i>
                            </span> {{ $guide->title }}</a>
                    </h4>
                    <p>{{ $guide->meta_description }}</p>
                </div>
            @endforeach
        @endforeach
        <div class="text-center">
            {{ $guides->appends(app('request')->query())->links() }}
        </div>
    </div>
@endsection