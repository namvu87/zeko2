@extends('layouts.app')
@section('style')
    <style>
        .homepage {
            display: none;
        }

        .category-box {
            padding: 10px;
            display: grid;
            border-radius: 4px;
            /*margin: 15px;*/
            min-height: 170px;
            cursor: pointer;
            transition: all .3s ease-in-out;
            transform: scale(0.9);
        }

        .category-box i {
            color: #147ddc;
            font-size: 45px;
            margin: 10px 0 20px 0;
        }

        .category-box b {
            color: #171717;
        }

        .category-box:hover {
            background: #147ddc;
            transform: scale(1);
        }

        .category-box:hover i {
            color: #ffffff;
        }

        .category-box:hover b {
            color: #ffffff;
        }
    </style>
@endsection
@section('navbar-bottom')
    <div class="categories-banner container-fluid">
        @include('landing_pages.guides.input_search')
    </div>
@endsection
@section('breadcrumb')
    <div class="row">
        @foreach($categories as $key => $category)
            <div class="col-md-2 col-sm-3 col-xs-6 text-center">
                <a href="{{ route('guide.detail.' . $service, ['slug' => $category['slug']]) }}" class="category-box">
                    <span><i class="{{ $category['icon'] }}"></i></span>
                    <h4><b>{{ $category['name'] }}</b></h4>
                </a>
            </div>
        @endforeach
    </div>
@endsection