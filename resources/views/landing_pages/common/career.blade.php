@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main-app.css') }}">
@endsection

@section('content')
    @if ($careerNewses)
        <div class="homepage__left">
            <div class="career">
                <div class="career__title">
                    <h3>@lang('word.vacancies')</h3>
                </div>
                <div class="career__items">
                    @foreach($careerNewses as $career)
                        <div id="item-{{ $career->id }}" class="career__item">
                            <div class="career__item-date">{{ date('d-m-Y', strtotime($career->created_at)) }}
                                <span class="new-job">new</span>
                            </div>
                            <div class="career__item-job">{{ $career->title }}</div>
                            <div class="career__item-content">
                                {{ strip_tags($career->content) }}
                            </div>
                            <a href="#item-{{ $career->id }}" name="detail"
                               class="career__item-detail">@lang('word.details') <i
                                        class="fas fa-angle-down show-content"></i></a>
                        </div>
                    @endforeach
                </div>
                {{ $careerNewses->links() }}
            </div>
        </div>
        <div class="homepage__right">
            <div class="job-available">
                <div class="job-available__title">
                    <h3>@lang('word.career_opportunities')</h3>
                </div>
                <div class="job-available__tag">
                    <span>@lang('word.list_tag')</span>
                    <div class="job-available__tag__list">
                        <a href="{{ route('career') }}" class="job-tag">@lang('word.all')</a>
                        @foreach(array_unique(array_flatten($careerNewses->pluck('tags')->toArray())) as $tag)
                            <a href="{{ route('career', $tag) }}" class="job-tag">{{ $tag }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script type="text/javascript">
      $('.career__item-detail').click(function() {
        // Show/Hide content career
        var content = $(this).parent().find('.career__item-content');
        if (content.css('display') == 'none') {
          content.css('display', 'block');
          $(this).find('.show-content').replaceWith("<i class='fas fa-angle-up hide-content'></i>");
        } else {
          content.css('display', 'none');
          $(this).find('.hide-content').replaceWith("<i class='fas fa-angle-down show-content'></i>");
        }
      });
    </script>
@endsection