@extends('layouts.app')

@section('style')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/main-app.css') }}">
  <style type="text/css">
      .info h3 { margin: 10px 0; text-align: center; color: #1790df; }
      .info h5 { margin: 10px 0; }
      .address h3 { margin-bottom: 15px; }
      .customer-care {  width: 100%; display: inline-table; border-bottom: 1px dashed rgba(0,0,0,.1); padding: 15px 0;}
      .customer-care:first-child { padding-top: 0px!important; }
      .customer-care__name { margin-top: 20px; text-align: center; }
      .customer-care__contact { width: cacl(100% - 130px); }
      .customer-care__item { width: 100%; }
      .customer-care__item .support__icon { float: left; width: 60px; padding: 5px;}
      .customer-care__item .customer-care__info { float: left; width: calc(100% - 60px); }
      .customer-care__item .customer-care__info span { line-height: 60px; text-align: left; font-size: 14px; }
      .social-network { padding: 0px; }
  </style>
@endsection

@section('content')
  <div class="row">
    <div id="contact-info" class="col-md-4">
      <div class="local-title">
        <h3>@lang('word.local')</h3>
      </div>
      <div class="info local-info">
        <div class="address">
          <h3>@lang('word.headquarters')</h3>
          <span>@lang('word.address'):</span>
          <span class="highline">{{ $contact['address'] }}</span>
        </div>
        <div class="phone_number">
          <span>@lang('word.phone_number'):</span>
          <span class="highline">{{ $contact['phone_number'] }}</span>
        </div>
        <div class="fax">
          <span>@lang('word.fax'):</span>
          <span class="highline">{{ $contact['fax'] }}</span>
        </div>
      </div>
      <div class="info support-info">
        <div class="_support">
          <h3>@lang('word.customer_support')</h3>
          <ul>
            @foreach($listCustomerCare as $index => $customerCare)
              <li class="customer-care">
                <h5 class="customer-care__name">{{ $customerCare['name'] }}</h5>
                <div class="customer-care__contact">
                  @if (!empty($customerCare['phone_number']))
                    <div class="customer-care__item">
                      <div class="support__icon">
                        <i class="fas fa-phone-volume"></i>
                      </div>
                      <div class="customer-care__info">
                        <span>{{ $customerCare['phone_number'] }}</span>
                      </div>
                    </div>
                  @endif
                  @if (!empty($customerCare['skype']))
                    <div class="customer-care__item">
                      <div class="support__icon">
                        <i class="fab fa-skype"></i>
                      </div>
                      <div class="customer-care__info">
                        <span>{{ $customerCare['skype'] }}</span>
                      </div>
                    </div>
                  @endif
                  @if (!empty($customerCare['zalo']))
                    <div class="customer-care__item">
                      <div class="support__icon">
                        <div class="icon-img">
                          <img src="{{ asset('payment//images/zalo.png') }}" alt="icon zalo" class="icon-img" />
                        </div>
                      </div>
                      <div class="customer-care__info">
                        <span>{{ $customerCare['zalo'] }}</span>
                      </div>
                    </div>
                  @endif
                  @if (!empty($customerCare['email']))
                    <div class="customer-care__item">
                      <div class="support__icon">
                        <i class="far fa-envelope"></i>
                      </div>
                      <div class="customer-care__info">
                        <span>{{ $customerCare['email'] }}</span>
                      </div>
                    </div>
                  @endif
                </div>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
        @if($contact)
      <div class="local-social-network">
        <div class="support__icon social-network">
          @if (!empty($contact['fb']))
            <a href="{{ $contact['fb'] }}"><i class="fab fa-facebook-square"></i></a>
          @endif
          @if (!empty($contact['skype']))
          <a href="#"><i class="fab fa-skype"></i></a>
            <a href="{{ $contact['fb'] }}"><i class="fab fa-facebook-square"></i></a>
          @endif
          @if (!empty($contact['google']))
            <a href="#"><i class="fab fa-google"></i></a>
          @endif
        </div>
      </div>
        @endif
    </div>
    <div id="map" class="col-md-8">
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    function initMap() {
      var uluru = {lat: 20.985670, lng: 105.824377};
      var map = new google.maps.Map(document.getElementById('map'), {zoom: 16, center: uluru});
      var marker = new google.maps.Marker({position: uluru, map: map});
    }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATsR7e9c6XGkokxIcU6R-GYdwS2bxBJJI&callback=initMap"></script>
@endsection