@extends('layouts.app')

@section('content')
  <div class="sidebar-left">
    <div class="sidebar-left__title">
      @lang('word.information')
    </div>
    <div class="list-group">
      <a href="{{ route('about') }}" class="list-group__item {{ Request::routeIs('about') ? 'select' : '' }}"><span>@lang('word.about')</span></a>
      <a href="{{ route('policy') }}" class="list-group__item {{ Request::routeIs('policy') ? 'select' : '' }}"><span>@lang('word.policy')</span></a>
      <a href="{{ route('rules') }}" class="list-group__item {{ Request::routeIs('rules') ? 'select' : '' }}"><span>@lang('word.rules')</span></a>
      <a href="{{ route('costs') }}" class="list-group__item {{ Request::routeIs('costs') ? 'select' : '' }}"><span>@lang('word.costs')</span></a>
    </div>
  </div>
  <div class="main-content">
    @if (!empty($content->content))
        {!! $content->content !!}
    @else
        <h3>Chưa có nội dung</h3>
    @endif
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $( document ).ready(function() {
      $('.list-group__item').hover(function() {
        $(this).addClass('active');
      }, function() {
        $(this).removeClass('active');
      });
    });
  </script>
@endsection