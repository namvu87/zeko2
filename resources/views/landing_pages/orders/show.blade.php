@extends('layouts.app')

@section('content')
  <div class="col-md-12">
    <div class="more-info mr-b-15">
      <div class="more-info__title"><span>@lang('word.order_information')</span></div>
      <div class="more-info-list">
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.order_code') :</div>
          <div class="more-info__items-value">{{ $order->product->code }}</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.customer_name') :</div>
          <div class="more-info__items-value">{{ $order->customer_name }}</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.customer_email') :</div>
          <div class="more-info__items-value">{{ $order->customer_email }}</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.order_status') :</div>
          <div class="more-info__items-value">
            @if ($order->status == 2)
              <span class="text-success">@lang('word.paymented')</span>
            @else
              <span class="txt-delete">@lang('word.pending')</span>
            @endif
          </div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.created_at') :</div>
          <div class="more-info__items-value">{{ $order->created_at->format('d-m-Y') }}</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.order_amount') :</div>
          <div class="more-info__items-value">{{ convertPrice($order->amount) }} VNĐ</div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12">
    <div class="more-info mr-b-15">
      <div class="more-info__title"><span>@lang('word.order_detail')</span></div>
      <div class="more-info-list">
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.payer') :</div>
          <div class="more-info__items-value">{{ $order->transaction->customer_name ?? __('word.not_pay') }}</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.email_payer') :</div>
          <div class="more-info__items-value">{{ $order->transaction->customer_email ?? __('word.not_pay') }}</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.phone_payer') :</div>
          <div class="more-info__items-value">{{ $order->transaction->customer_phone ?? __('word.not_pay') }}</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.payment_date') :</div>
          <div class="more-info__items-value">{{ $order->status === 2 ? date('d-m-Y', $order->transaction->created_on) : __('word.not_pay') }}</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.order_amount') :</div>
          <div class="more-info__items-value">{{ number_format($order->amount, 0, '', '.') }} VNĐ</div>
        </div>
        <div class="more-info__items">
          <div class="more-info__items-label">@lang('word.amount_pay') :</div>
          <div class="more-info__items-value">{{ $order->status === 2 ? number_format($order->transaction->net_amount, 0, '', '.') : __('word.not_pay') }} VNĐ</div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-gr">
    <a class="btn btn--cancel" href="javascript: window.history.go(-1)">@lang('word.back')</a>
  </div>
@endsection