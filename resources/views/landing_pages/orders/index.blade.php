@extends('layouts.app')

@section('content')
{{--   <div class="col-md-12">
    <div class="more-info mr-b-15">
      <div class="more-info__title"><span>THÔNG TIN TÌM KIẾM</span></div>
      <div class="row">

          <div class="input-gr">
            <span>Mã đơn hàng :</span>
            <input type="text" class="register-info__input" placeholder="Nhập họ tên"/>
          </div>


          <div class="input-gr">
            <span>Trạng thái đơn hàng :</span>

            <select class="form-control select-input register-info__input">
              <option value="" selected="">Loại</option>
              <option value="" selected="">Window</option>
            </select>
          </div>


          <div class="input-gr">
            <span>Thời gian :</span>
            <div class="search-time">
              <input type="text" class="form-input" id="start-date" data-format="dd/mm/yyyy" placeholder="Từ ngày" />
              <input type="text" class="form-input" id="end-date" data-format="dd/mm/yyyy" placeholder="Đến ngày" />
            </div>
          </div>

      </div>
      <div class="row">
        <div class="btn-gr pd-0">
          <a class="btn" href=""><i class="fas fa-search"></i>Tìm kiếm</a>
        </div>
      </div>
    </div>
  </div> --}}
  <div class="col-md-12">
    <div class="more-info">
      <div class="more-info__title"><span>@lang('word.list_order')</span></div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>@lang('word.sort')</th>
            <th>@lang('word.order_code')</th>
            <th>@lang('word.created_at')</th>
            <th>@lang('word.order_amount')</th>
            <th>@lang('word.status')</th>
            <th>@lang('word.amount_pay')</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($orders as $key => $order)
            <tr>
              <td>{{ $key + 1 }}</td>
              <td><a href="{{ route('order.show', $order->id) }}">{{ $order->product->code }}</a></td>
              <td>{{ $order->created_at->format('d-m-Y') }}</td>
              <td>{{ convertPrice($order->amount) }}</td>
              <td>
                @if ($order->status === 2)
                  <span class="text-success">@lang('word.paymented')</span>
                @else
                  <span class="text-danger">@lang('word.pending')</span>
                @endif
              </td>
              <td>{{ !empty($order->transaction->net_amount) ? number_format($order->transaction->net_amount, 0, '', '.') : 0 }}</td>
              <td><a href="{{ route('order.show', $order->id) }}">@lang('word.detail')</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
@endsection