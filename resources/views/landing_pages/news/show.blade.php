@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main-app.css') }}">
@endsection

@section('content')
    <!--blog content-->
    <!--content-->
    <div class="page__content">
        <h1 class="page__content-title">{{ $news->title }}</h1>
        <div class="page__content-time">
            <span>{{ date('d-m-Y', strtotime($news->created_at)) }}</span>
        </div>
        <div class="page__content-body">{!! $news->content !!}</div>
    </div>
    <!--content-->
    <!--content-->
    <div class="page__sidebar">
        <div class="page__sidebar__title">
            <span>@lang('word.other_news')</span>
        </div>
        <div class="page__sidebar__content">
            <ul>
                @foreach($newNewses as $newNews)
                    <li class="active">
                        <a href="{{ route('news.show', $newNews->slug) }}">
                            <img src="{{ asset($newNews->photo_url[0]['origin']) }}"
                                 alt="img-service"/>
                            <span>{{ (strlen($newNews->title) > 48) ? substr($newNews->title, 0, 46) . '...' : $newNews->title }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <!--content-->
    <!--e blog content-->
@endsection