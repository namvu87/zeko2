@extends('layouts.app')

@section('content')
  <!--blog content-->
  <!-- card wrapper -->
  <div class="b-flex b-fuild b-flex--start b-flex--mobile">
    @foreach($newses as $news)
      <div class="card">
        <!-- icon >
        <div class="card__icon">
          <i class="fa fa-edit"></i>
        </div>
        <! icon -->
        <div class="card__img">
          @if (!empty($news->photo_url))
          <img src="{{ asset($news->photo_url[0]['origin']) }}" alt="Giam beo" />
          @endif
        </div>
        <div class="card__title">
          <a href="{{ route('news.show', $news->slug) }}">{{ (strlen($news->title) > 48) ? substr($news->title, 0, 46) . '...' : $news->title }}</a>
        </div>
        <!--des-->
        <div class="card__des">
          <p>{{ (strlen($news->content) > 98) ? substr(strip_tags($news->content), 0, 96) . '...' : strip_tags($news->content) }}</p>
        </div>
        <!--end des-->
        <!--button-->
        <div class="card__button">
          <a href="{{ route('news.show', $news->slug) }}">@lang('word.detail')</a>
        </div>
        <!--end button-->
      </div>
    @endforeach
  </div>
  <!--e card w-->

  <!--pagination-->
  {{ $newses->links() }}
  <!--e pagination-->
  <!--e blog content-->
@endsection