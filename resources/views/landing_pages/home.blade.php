@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing.css') }}">
@endsection

@section('content')
    <section class="slider">
        <div id="slider" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
        @if (!empty($sliders))
            <!-- Indicators -->
                <ol class="carousel-indicators">
                    @for($i = 0; $i < count($sliders); $i++)
                        <li data-target="#slider" data-slide-to="{{ $i }}" class="{{ ($i == 0) ? 'active' : '' }}"></li>
                    @endfor
                </ol>

                <div class="carousel-inner" role="listbox">
                    @foreach($sliders as $key => $slider)
                        <div class="item {{ $key == 0 ? 'active': '' }}">
                            <a class="slide-link" href="{{ $slider->link }}">
                                <img src="{{ asset($slider->photo_url['origin']) }}" alt="Slide">
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#slider" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            @endif
        </div>
    </section>
    @if (!empty($newsIntroduction))
        <section class="section service">
            <div class="title">
                <h2>Phần mềm chấm công Zeko</h2>
            </div>
            <div class="news__content">
                <div class="news__content__main card row">
                    <div class="news__content__image card col-md-4 col-xs-12">
                        <div id="product-image" class="carousel slide" data-ride="carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="{{ asset('images/app/home-page.jpg') }}" alt="...">
                                </div>
                                <div class="item">
                                    <img src="{{ asset('images/app/login.jpg') }}" alt="...">
                                </div>
                                <div class="item">
                                    <img src="{{ asset('images/app/history.jpg') }}" alt="...">
                                </div>
                                <div class="item">
                                    <img src="{{ asset('images/app/personal.jpg') }}" alt="...">
                                </div>
                            </div>
                            <ol class="carousel-indicators">
                                <li data-target="#product-image" data-slide-to="0" class="active"></li>
                                <li data-target="#product-image" data-slide-to="1"></li>
                                <li data-target="#product-image" data-slide-to="2"></li>
                            </ol>
                        </div>
                    </div>
                    <p class="text col-md-8 col-xs-12">
                        {{ strip_tags($newsIntroduction->content) }}
                    </p>
                </div>
            </div>
            @endif
        </section>
        @if (!empty($newsAddHomePage))
            @foreach($newsAddHomePage as $index => $newsAdd)
                <section class="section news-add" style="background: {{ $index % 2 == 0 ? "#f0eeef" : "" }};">
                    <div class="title">
                        <h2>{{ $newsAdd->title }}</h2>
                    </div>
                    <div class="news__content">
                        <div class="news__content__main card row">
                            @if (!empty($newsAdd->photo_url))
                                <div class="news__content__image news__content__image-card card col-md-4 col-sm-4 col-xs-12">
                                    <img src="{{ asset($newsAdd->photo_url[0]['origin'] ?? '') }}" alt="...">
                                </div>
                            @endif
                            <p class="text col-md-8 col-sm-8 col-xs-12">
                                {{ strip_tags($newsAdd->content) }}
                            </p>
                        </div>
                    </div>
                </section>
            @endforeach
        @endif
        <section class="section why">
            <div class="why__title title">
                <h2>Lý do lựa chọn Zeko</h2>
            </div>
            <div class="why__list">
                <div class="why__list__item">
                    <div class="icon">
                        <i class="far fa-paper-plane"></i>
                    </div>
                    <h3>Tiện tích</h3>
                    <div>
                        <p>Chỉ cần một tờ gíây có mã QR và một chiếc smartphone có cài ứng dụng Zeko, bạn có thể thực
                            hiện tất cả mọi thứ</p>
                    </div>
                </div>
                <div class="why__list__item">
                    <div class="icon">
                        <i class="fas fa-hand-holding-usd"></i>
                    </div>
                    <h3>Tiết kiệm chi phí</h3>
                    <div>
                        <p>Phần mềm chấm công Zeko chấm công bằng mã QR giúp giảm thiểu chi phí lắp đặt máy chấm
                            công</p>
                    </div>
                </div>
                <div class="why__list__item">
                    <div class="icon">
                        <i class="fas fa-info"></i>
                    </div>
                    <h3>Dữ liệu đầy đủ</h3>
                    <div>
                        <p>Thống kê dữ liệu đầy đủ về thời gian, địa điểm chấm công của từng thành viên sử dụng hệ
                            thống</p>
                    </div>
                </div>
                <div class="why__list__item">
                    <div class="icon">
                        <i class="fas fa-ruler-combined"></i>
                    </div>
                    <h3>Phù hợp các tổ chức</h3>
                    <div>
                        <p>Phần mềm chấm công Zeko phù hợp với nhiều tổ chức, doanh nghiệp có các hình thức hoạt động
                            khác nhau...</p>
                    </div>
                </div>
                <div class="why__list__clear"></div>
            </div>
        </section>
        <section class="section news">
            <div class="news__title title">
                <h2>Tin tức</h2>
            </div>
            @if(count($newNewsList) > 0)
                <div class="b-flex b-fuild b-flex--start b-flex--mobile news__list-item">
                    @foreach ($newNewsList as $newNews)
                        <div class="card">
                            <div class="card__img">
                                <a href="{{ route('news.show', $newNews->slug) }}">
                                    <img src="{{ asset($newNews->photo_url[0]['origin'] ?? '') }}" alt="Giam beo"/>
                                </a>
                            </div>
                            <div class="card__title">
                                <a href="{{ route('news.show', $newNews->slug) }}">{{ (strlen($newNews->title) > 48) ? substr($newNews->title, 0, 46) . '...' : $newNews->title }}</a>
                            </div>
                            <div class="card__des">
                                <p>{{ (strlen($newNews->content) > 98) ? substr(strip_tags($newNews->content), 0, 96) . '...' : strip_tags($newNews->content) }}</p>
                            </div>
                            <div class="card__button">
                                <a href="{{ route('news.show', $newNews->slug) }}">Chi tiết</a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="news__view-more">
                    <a href="{{ route('news.list') }}">Xem thêm</a>
                </div>
            @endif
        </section>
@endsection