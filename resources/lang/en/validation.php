<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is not regist.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'lte' => [
        'numeric' => 'Value must be less than or equal :attribute',
    ],
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'captcha' => 'The captcha is invalid or expired',
    'password_matched' => 'The password is not matched',
    'equal' => ':attribute has incorrect value',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'parent_id' => [
            'exists' => 'Parent group menu is not exists'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'captcha' => 'captcha',
        'first_name' => 'first name',
        'last_name' => 'last name',
        'username' => 'identifier',
        'service_type' => 'service type',
        'area' => 'area',
        'commune' => 'commune',
        'address' => 'address',
        'password' => 'password',
        'old_password' => 'Password',
        'current_password' => 'Current password',
        'email' => 'email',
        'name' => 'name',
        'avatar' => 'avatar',
        'birthday' => 'birthday',
        'gender' => 'gender',
        'company_name' => 'company name',
        'company_address' => 'company address',
        'tax_code' => 'tax code',
        'current_pasword' => 'old password',
        'new_password' => 'new password',
        'start_time_1' => 'start time',
        'end_time_1' => 'end time',
        'start_time_2' => 'start time',
        'end_time_2' => 'end time',
        'type' => 'type',
        'company_id' => 'company ID',
        'start_date' => 'start date',
        'end_date' => 'end date',
        'month_year' => 'month',
        'phone' => 'phone number',
        'expiration_date' => 'Expiration date',
        'phone_number' => 'phone number',
        'fax' => 'fax number',
        'fb' => 'facebook link',
        'title' => 'title',
        'sort' => 'ordinal',
        'tags' => 'tags list',
        'images.*' => 'image',
        'description' => 'description',
        'content' => 'content',
        'price' => 'price',
        'price_type' => 'price type',
        'detail' => 'detail',
        'user_guide' => 'user guide',
        'user_count' => 'number of users',
        'code' => 'code',
        'link' => 'link',
        'image' => 'image',
        'term' => 'term',
        'src' => 'src',
        'checkin1' => 'check-in time',
        'checkout1' => 'check-out time',
        'time_keeping_id' => 'time keeping ID',
        'checkin2' => 'check-in time',
        'checkout2' => 'check-out time',
        'date' => 'date',
        'customer_name' => 'customer name',
        'customer_phone' => 'customer phone number',
        'customer_email' => 'customer email',
        'order_id' => 'order ID',
        'transaction_id' => 'transaction ID',
        'place_id' => 'Place',
        'group_menu_ids' => 'Group menus',
        'unit' => 'Unit',
        'properties.*.name' => 'Properties name',
        'properties.*.value' => 'Properties value',
        'conversion_units.*.unit' => 'Unit',
        'conversion_units.*.value' => 'Conversion rate',
        'conversion_units.*.price' => 'Price',
        'price_origin' => 'Price origin',
        'inventory_number' => 'Inventory number',
        'inventory_min' => 'Inventory min',
        'inventory_max' => 'Inventory max',
        'images' => 'Photo',
        'ingredients' => 'Ingredients',
        'ingredients.*.count' => 'Ingredient count',
        'goods' => 'Goods',
        'start' => 'Start time',
        'end' => 'End time',
        'note' => 'Note',
        'area.name' => 'Area',
        'commune.name' => 'Commune',
        'user_id' => 'User',
        'role_id' => 'Role',
        'count_seat' => 'Count seat',
        'property' => 'Property',
        'goods.*.discount' => 'Discount',
        'shift_id' => 'Shift'
    ],
];
