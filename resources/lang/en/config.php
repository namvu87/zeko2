<?php
return [
    'language' => [
        0 => 'Tiếng Việt',
        1 => 'English'
    ],
    'product' => [
        'type' => [
            1 => 'The timekeeping application',
            2 => 'The restaurant application'
        ],
        'price_type' => [
            11 => 'Type fixed price with the maximum number of employees (eg 70k/max 50 people/month)',
            12 => 'Type the price depends on the number of employees (eg 30k/20 people/month)'
        ]
    ],
    'status' => [
        0 => 'unvisible',
        1 => 'visible'
    ],
    'import_file' => [
        'import_user_into_group' => [
            'example_csv' => 'sample_files/en/ImportUserToGroupTemplate.csv',
            'example_xlsx' => 'sample_files/en/ImportUserToGroupTemplate.xlsx',
            'api' => 'import/user-into-group',
            'file_header' => [
                0 => 'Sort',
                1 => 'Full name',
                2 => 'Phone number',
                3 => 'Email'
            ],
            'data_field' => [
                0 => 'sort',
                1 => 'full_name',
                2 => 'phone_number',
                3 => 'email'
            ]
        ],
        'import_goods' => [
            'example_csv' => 'sample_files/en/GoodsTemplate.csv',
            'example_xlsx' => 'sample_files/en/GoodsTemplate.xlsx',
            'api' => 'goods/store/import',
            'data_field' => [
                0 => 'sort',
                1 => 'code',
                2 => 'name',
                3 => 'group_menu_name',
                4 => 'price',
                5 => 'price_origin',
                6 => 'unit',
                7 => 'inventory_min',
                8 => 'inventory_max',
                9 => 'is_sale',
                10 => 'description',
            ]
        ],
        'import_place' => [
            'example_csv' => 'sample_files/en/PlaceTemplate.csv',
            'example_xlsx' => 'sample_files/en/PlaceTemplate.xlsx',
            'api' => '',
            'file_header' => [
                0 => 'STT',
                1 => 'Tên khu vực/phòng',
                2 => 'Mô tả'
            ],
            'data_field' => [
                0 => 'sort',
                1 => 'name',
                2 => 'description'
            ]
        ],
        'import_table' => [
            'example_csv' => 'sample_files/en/TableTemplate.csv',
            'example_xlsx' => 'sample_files/en/TableTemplate.xlsx',
            'api' => 'table/store/import',
            'data_field' => [
                0 => 'sort',
                1 => 'name',
                2 => 'count_seat',
                3 => 'place_name',
                4 => 'description'
            ]
        ]
    ],
    'goods' => [
        'units' => [
            'bowl' => 'Bowl',
            'dish' => 'Dish',
            'glass' => 'Glass',
            'package' => 'Package',
            'cup' => 'Cup',
            'bottle' => 'Bottole',
            'box' => 'Box',
            'can' => 'Can',
            'kg' => 'Kg',
            'lit' => 'Lit',
            'pot' => 'Pot',
        ],
    ],
    'mini_options_period_time' => [
        'today' => 'Today',
        'yesterday' => 'Yesterday',
        'this_week' => 'This week',
        'last_week' => 'Last week',
        'last_seven_days' => 'Last seven days',
        'this_month' => 'This month',
        'last_month' => 'Last month'
    ],
    'options_period_time' => [
        [
            'By day and week',
            [
                'today' => 'Today',
                'yesterday' => 'Yesterday',
                'this_week' => 'This week',
                'last_week' => 'Last week',
                'last_seven_days' => 'Last seven days'
            ]
        ],
        [
            'By month',
            [
                'this_month' => 'This month',
                'last_month' => 'Last month',
                'last_thirty_days' => 'Last thirty days'
            ]
        ],
        [
            'By quarter and year',
            [
                'this_quarter' => 'This quarter',
                'last_quarter' => 'Last quarter',
                'this_year' => 'This year',
                'last_year' => 'Last year'
            ]
        ]
    ],
    'day_of_week' => ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    'month_names' => ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
        'September', 'October', 'November', 'December'
    ],
    'invoice_form' => [
        'variables' => [
            'common' => [
                'Numerical_Invoice' => 'Number of value that in table'
            ],
            1 => [ //sale invoice
                'out_table' => [
                    'Current_Invoice_Code' => 'Code of current invoice',
                    'Restaurant_Name' => 'Name of restaurant',
                    'Restaurant_Address' => 'Address of resaurant',
                    'Date_Time_Create_Invoice' => 'Date that invoice created',
                    'Tables' => 'List tables',
                    'Employee_Name' => 'Name of employee created invoice',
                    'Grand_Total' => 'Total money that don\'t include discount and VAT',
                    'Discount_Invoice' => 'Discount for invoice',
                    'Paid_Total_Money' => 'Total money that customer paid',
                    'Note' => 'Note for the invoice'
                ],
                'in_table' => [
                    'Goods_Code' => 'Code of goods',
                    'Goods_Name' => 'Name of goods',
                    'Goods_Count' => 'Number of order goods',
                    'Goods_Price' => 'Price before discount of a goods',
                    'Goods_Discount' => 'Discount of a goods',
                    'Price_After_Discount' => 'Price after discount of a goods',
                    'Goods_Total_Price' => 'Total price of this goods',
                ],
                'other' => [
                    'Goods_Price_Discount' => 'Price before and after discount of a goods',
                ],
                'loop' => 'Goods'
            ],
            2 => [ //import invoice
                'out_table' => [
                    'Invoice_Code' => 'Code of imported goods invoice',
                    'Employee_Name' => 'Name of employee',
                    'Date_Create_Invoice' => 'Date that invoice created',
                    'Total_Money' => 'Money that paid publisher',
                    'Note' => 'Note for invoice'
                ],
                'in_table' => [
                    'Goods_Code' => 'Code of goods',
                    'Goods_Name' => 'Name of goods',
                    'Goods_Count' => 'Number of imported Goods',
                    'Goods_Price' => 'Price of goods',
                    'Goods_Discount' => 'Total discount for goods',
                    'Goods_Total_Price' => 'Total money of this goods',
                ],
                'other' => [],
                'loop' => 'Goods',
            ],
            3 => [ //returned invoice
                'out_table' => [
                    'Returned_Invoice_Code' => 'Code of current invoice',
                    'Sale_Invoice_Code' => 'Code of current invoice',
                    'Restaurant_Name' => 'Name of restaurant',
                    'Restaurant_Address' => 'Address of resaurant',
                    'Date_Time_Create_Invoice' => 'Date that invoice created',
                    'Employee_Name' => 'Name of employee created invoice',
                    'Grand_Total' => 'Total money of invoice',
                    'Note' => 'Note for the invoice'
                ],
                'in_table' => [
                    'Goods_Code' => 'Code of goods',
                    'Goods_Name' => 'Name of goods',
                    'Goods_Count' => 'Number of order goods',
                    'Goods_Price' => 'Price before discount of a goods',
                    'Goods_Total_Price' => 'Total price of this goods',
                ],
                'other' => [],
                'loop' => 'Goods'
            ],
        ],
    ]
];