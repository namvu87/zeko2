<?php
return [
    'deleted' => 'Deleted',
    'not_delete' => 'Not deleted',
    'slider_manager' => [
        'slider' => 'Slider',
        'slider_manager' => 'Slider management',
        'add_slider' => 'Add new slider',
        'add_slider_success' => 'New slider has been added successfully.',
        'edit_slider' => 'Edit slider',
        'edit_slider_success' => 'Slider has been updated successfully.',
        'delete_slider_confirm' => 'Are you sure want to delete this slider?',
        'delete_slider_success' => 'Slide has been deleted successfully.',
        'type' => 'Slider type',
        'type_show_web' => 'Show on the web',
        'type_show_mobile' => 'Show on the mobile'
    ],
    'tag_manager' => [
        'tag'   => 'Tag',
        'tag_manager' => 'Tag management',
        'add_tag' => 'Add new tag',
        'add_tag_success' => 'New tag has been added successfully.',
        'edit_tag' => 'Edit tag',
        'edit_tag_success' => 'Tag has been updated successfully.',
        'delete_tag_confirm' => 'Are you sure want to delete this tag?',
        'delete_teg_success' => 'Tag has been deleted successfully.',
        'restore_tag_confirm' => 'Are you sure want to restore this tag?',
        'restore_tag_success' => 'Tag has been restored successfully.',
        'error_tag_exists_news' => 'Tag already exist in the news, so they cannot be deleted.'
    ],
    'news_manager' => [
        'news_manager' => 'News management',
        'add_news' => 'Add new news',
        'add_news_success' => 'New news has been added successfully.',
        'edit_news' => 'Edit news',
        'edit_news_success' => 'News has been updated successfully.',
        'delete_news_confirm' => 'Are you sure want to delete this news?',
        'delete_news_success' => 'News has been deleted successfully.',
        'restore_news_confirm' => 'Are you sure want to restore this news?',
        'restore_news_success' => 'News has been restored successfully.'
    ],
    'product_manager' => [
        'product' => 'Product',
        'product_manager' => 'Product management',
        'add_product' => 'Add product',
        'delete_product_confirm' => 'Are you sure want to delete this product?',
        'delete_product_success' => 'Product has been deleted successfully.',
        'add_product_success' => 'New product has been added successfully.',
        'edit_product_success' => 'Product information has been updated successfully.',
    ],
    'customer_care_manager' => [
        'customer_care_manager' => 'Customer officer management',
        'add_customer_care' => 'Add new employee',
        'edit_customer_care' => 'Edit employee',
        'delete_customer_care_confirm' => 'Are you want to delete this employee?',
        'create_customer_care_success' => 'New employee has been added successfully.',
        'delete_customer_care_success' => 'Employee has been deleted successfully.',
        'update_customer_care_success' => 'Employee information has been updated successfully.'
    ],
    'contact' => [
        'contact' => 'Contact information',
        'update_contact_success' => 'Contact information has been updated successfully.'
    ],
    'group_manager' => [
        'group' => 'Group',
        'group_manager' => 'Group management',
        'edit_group' => 'Edit group',
        'set_expiration_date_success' => 'Expiration date has been updated successfully.'
    ],
    'user_guide' => [
        'user_guide' => 'User guide',
        'add_guide' => 'Add guide',
        'edit_guide' => 'Edit guide',
        'delete_guide_confirm' => 'Are you sure delete this guide?',
        'restore_guide_confirm' => 'Are you sure restore this guide?',
    ]
];