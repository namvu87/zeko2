<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'jwt' => [
        'expires_token' => 'Token expired, please login again.',
        'invalid_token' => 'Token is invalid, please login again.',
        'absent_token' => 'Authentication error, please login again.'
    ],
    'term' => 'You must agree to the terms of service.',
    'register' => [
        'success' => 'Registration successful, please activate your account from your email.'
    ],
    'action_unauthorized' => 'This action is invalid',
    'unauthorized' => 'Unauthorized',
    'expires_token' => 'The token have been expired',
    'not_verified' => 'Your account have not been verified. Please access the registered email and click the button <b><u>Verified Email</u></b>, or click the button  <a class="btn btn-primary btn-sm" id="resend">Resend</a>  to request a verification email again',
    'you_are_not_in_group' => 'You are not in this group',
    'verification' => 'Verification account',
    'welcome_content' => 'Thanks for register. Please verification your email or phone number to can use fully feature.',
    'contact_zeko' => 'Cannot verify your account? Please contact ZEKO support',
    'verify' => 'Verify',
    'attemp_too_many' => 'You have been attempted too many, please try again after :counter',
    'verify_email_and_phone_number_is_required' => 'You have to verify email and phone number before initialize group'
];
