<?php
return [
    'title' => 'Admin | Zeko application',
    'word' => [
        'sign_in' => 'sign in to admin',
        'sign_up' => 'sign up admin account',
        'reset_password' => 'reset password',
        'enter_detail' => 'Enter your details below',
        'organization_name' => 'Name of company, restaurant, organization',
        'email_desc' => '<b>Email</b> will be information for sign in and reset password.<br> <b>Email</b> have to be unique',
        'phone_number_desc' => '<b>Phone number</b> will be information for sign in and reset password.<br><b>Phone number</b> input have to be format 0xxx (10 characters).<br><b>Phone number</b> have to be unique',
        'why_email' => 'Why give a phone number?',
        'why_phone_number' => 'Why give a email?'
    ],
    'message' => [
        'auth_fail' => "Account information is incorrect.\n Please try again.",
        'error' => 'Error, please try again.',
        'success' => 'Login successfully'
    ],
    'payment' => [
        'auth_required' => 'Please login or register an account before buying the service.',
    ]
];