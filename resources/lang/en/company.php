<?php
return [
    'company' => [
        'name' => 'Company name',
        'profile' => 'Company profile',
        'information' => 'Information',
        'avatar' => 'Avatar',
        'require' => 'You need to enter complete information in the fields marked',
        'update_profile_success' => 'Profile has been updated successfully',
        'update_profile_error' => 'An error occurred while updating the profile, please check again.',
        'change_password_success' => 'Updated has been password successfully',
        'change_password_error' => 'An error occurred while updating password, please check again.',
        'your_current_password_not_matches' => 'Your current password does not match the old password. Please try again.',
        'new_password_same_current_password' => 'The new password can not be the same as your current password. Please choose a different password.',
    ],
    'branch' => [
        'create_success' => 'A branch has been created successfully.',
        'not_found' => 'The company do not exist or was deleted, please check again.',
        'update_success' => 'Update successfully',
        'disable_branch' => 'Are you sure want to close the company?',
        'enable_branch' => 'Are you sure want to open the company?',
        'username_not_allowed_edit' => 'Username is not allowed to edit',
    ],
    'user' => [
        'add_user_success' => 'User has been added successfully to your company.',
        'add_user_confirm' => ' Are you sure want to add user to your company?',
        'delete_user_confirm' => 'Are you sure want to delete this user from the list of company?',
        'delete_user_success' => 'The user has been deleted successfully from your company!',
        'user_exists' => 'This user is already a member of your company.',
        'add_user_fail' => 'Error, please check again.',
        'company_not_permission' => 'You do not has permission to perform this action.',
    ],
    'shift' => [
        'no_select_action' => 'You need to select a action before confirming.',
        'add_user_confirm' => 'Are you sure want to add user to this shift?',
        'add_user_success' => 'User has been added successfully into the shift.',
        'add_users_confirm' => 'Are you sure want to add users to this shift?',
        'add_users_success' => 'Users has been added successfully into thhe shift.',
        'delete_shift_confirm' => 'Are you sure want to delete this shift?',
        'delete_user_confirm' => 'Are you sure want to delete user from this shift?',
        'delete_user_success' => 'User has been deleted successfully from the shift.',
        'company_not_permission' => 'You do not has permission to perform this action.',
        'shift_existed' => 'This user already has a shifts in this group',
        'not_exists_shift' => 'You have no shifts here'
    ],
    'permission' => [
        'delete_user_confirm' => 'Bạn có chắc chắn muốn xóa vai trò của người dùng này không?',
        'delete_user_success' => 'Xóa vai trò của người dùng thành công',
        'update_role_permissions_success' => 'Bạn đã thay đổi quyền thành công'
    ]
];