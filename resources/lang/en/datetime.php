<?php
return [
    'week' => [
        1 => 'MON',
        2 => 'TUE',
        3 => 'WEB',
        4 => 'THU',
        5 => 'FRI',
        6 => 'SAT',
        7 => 'SUN',
    ],
    'mongo_week' => [
        1 => 'SUN',
        2 => 'MON',
        3 => 'TUE',
        4 => 'WEB',
        5 => 'THU',
        6 => 'FRI',
        7 => 'SAT'
    ],
    'weekdays' => [
        'monday' => 'Monday',
        'tuesday' => 'Tuesday',
        'wednesday' => 'Wednesday',
        'thursday' => 'Thursday',
        'friday' => 'Friday',
        'saturday' => 'Saturday',
        'sunday' => 'Sunday',
    ]
];