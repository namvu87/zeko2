<?php
return [
    'master' => [
        'number_group' => 'Number of groups',
        'number_product' => 'Number of product',
        'number_order' => 'Number of order',
        'number_employee' => 'Number of employee',
        'number_user' => 'Number of member',
        'revenue' => 'Revenue',
        'new_group_list' => 'List of the newest groups',
        'name' => 'Name',
        'type' => 'Type',
        'price' => 'Price',
        'register_date' => 'Registration date',
        'list_product_selling' => 'List of the most product was sold'
    ],
    'company' => [
        'number_company_branch' => 'Number of branch',
        'number_employee' => 'Number of employee',
        'number_shift' => 'Number of shift',
        'expiration_date' => 'Expiration date',
        'company_info' => 'Company information',
        'calendar' => 'Work schedule',
        'list_new_employee' => 'List of new employee',
        'shifts' => 'Shifts',
        'name' => 'Name',
        'type' => 'Type',
        'start_time' => 'Start time',
        'end_time' => 'End time',
    ],
];