<?php
return [
    'add' => 'Create a shift',
    'shift_name' => 'Shift name',
    'shift_info' => 'Shift information',
    'infoShift' => [
        'success' => 'Shift has been added successfully!',
        'editsuccess' => 'Shift has been updated successfully!',
        'deletesuccess' => 'Shift has been deleted successfully!',
        'error_danger' => "Shift do not exist!",
        'start_time' => 'Please enter start time',
        'end_time' => 'Please enter end time',
    ],
    'choose_type_shift' => "Select the type of shift that suits your company's requirement",
    '2_times_scan_per_day' => 'Scan 2 times a day',
    '4_times_scan_per_day' => 'Scan 4 times a day',
    'first_haft_shift' => 'haft first shift',
    'second_haft_shift' => 'haft second shift',
    'user_already_have_shift' => 'The employee has a shift already',
    'allow_time' => 'Time deviation allowed',
    'allow_time_desc' => 'For example: If you set it to 10 minutes, employees who are late for less than 10 minutes, or less than 10 minutes early will not be considered late or early. If not allowed set to 0',
    'user_not_shift' => 'This employee don\'t have shift in company.'
];