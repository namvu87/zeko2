<?php
return [
    'error_message' => 'Something has gone seriously wrong. Please check again!',
    'go_back' => 'Go back',
    'go_home' => 'Go home',
    '403' => 'Request can not be executed, please check again.',
    '404' => "The path you entered does not exist or has been deleted. Please check again.",
    '500' => "Something has gone seriously wrong. We're experiencing n internal server problem. Please try back later.",
    'invalid_router' => 'Invalid path'
];