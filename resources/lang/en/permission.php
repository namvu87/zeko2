<?php
return [
    'error' => 'Invalid request, Please, try agian!',
    'message' => [
        'cannot_assign_owner_role' => 'Cannot assign owner role',
        'cannot_revoke_owner_role' => 'Cannot remove owner role',
        'cannot_change_owner_role' => 'Cannot change permissions of owner role',
    ],
    'role' => [
        'owner' => 'Owner',
        'admin' => 'Admin',
        'hrm' => 'HRM',
        'cashier' => 'Cashier',
        'waiters' => 'Waiters',
        'chef' => 'Chef',
        'none' => ''
    ],
    'group_permission' => [
        'employee' => 'Employee',
        'shift' => 'Shift',
        'timekeeping' => 'Timekeeping',
        'place' => 'Place',
        'table' => 'Table',
        'group-menus' => 'Group menus',
        'goods' => 'Goods',
        'invoice' => 'Invoice',
        'imported-invoice' => 'Imported invoice',
        'form-invoice' => 'Print form invoice',
        'report' => 'Report & statistical',
        'returned-invoice' => 'Imported invoice',
        'group' => 'Group management',
        'permission' => 'Role & permission',
        'requirement' => 'Requirement'
    ],
    'permission' => [
        'employee' => [
            'list' => 'Listings',
            'add' => 'Add employee',
            'remove' => 'Remove employee'
        ],
        'shift' => [
            'list' => 'List of shifts',
            'create' => 'Create new',
            'update' => 'Update',
            'delete' => 'Delete shift',
            'user-list' => 'List of employees',
            'user-add' => 'Assign employee',
            'user-remove' => 'Remove'
        ],
        'timekeeping' => [
            'list' => 'Listings',
            'detail' => 'Detail',
        ],
        'report' => [
            'timekeeping-group' => 'Group\'s timekeepings report',
            'timekeeping-employee' => 'Employee\'s timekeepings report',
            'today' => 'Today report',
            'sale' => 'Sale report',
            'goods' => 'Goods report'
        ],
        'requirement' => [
            'list' => 'Listings',
            'handle' => 'Handle the requirement'
        ],
        'permission' => [
            'role' => 'Show list roles',
            'role-permission' => 'Show permissions of role',
            'role-assign' => 'Assign the role',
            'role-revoke' => 'Revoke the role',
            'update-role-permission' => 'Setup permissions of role'
        ],
        'group' => [
            'update-profile' => 'Update profile',
            'change-service' => 'Change type of service',
            'pay-fee' => 'Pay for the service fee'
        ],
        'place' => [
            'list' => 'Listings',
            'create' => 'Create new',
            'update' => 'Update',
            'delete' => 'Delete'
        ],
        'table' => [
            'list' => 'Listings',
            'create' => 'Create',
            'update' => 'Update',
            'delete' => 'Delete'
        ],
        'group-menus' => [
            'list' => 'Listings',
            'create' => 'Create new',
            'update' => 'Update',
            'delete' => 'Delete'
        ],
        'goods' => [
            'list' => 'Listings',
            'create' => 'Create new',
            'update' => 'Update',
            'delete' => 'Delete'
        ],
        'form-invoice' => [
            'list' => 'Listings',
            'create' => 'Create new',
            'update' => 'Update',
            'delete' => 'Delete'
        ],
        'invoice' => [
            'list' => 'Listings',
            'selling-list' => 'Show selling list',
            'create' => 'Create new',
            'detail' => 'Detail',
            'update' => 'Update',
            'delete' => 'Delete',
            'disable' => 'Disable',
            'checkout' => 'Checkout'
        ],
        'imported-invoice' => [
            'list' => 'Listings',
            'create' => 'Create new',
            'detail' => 'Detail',
            'disable' => 'Disable'
        ],
        'returned-invoice' => [
            'list' => 'Listings',
            'create' => 'Create new',
            'detail' => 'Detail',
            'disable' => 'Disable'
        ]
    ]
];
