<?php
return [
    'invoice_form_empty' => 'The print form is empty!',
    'create_success' => 'Create successfully!',
    'update_success' => 'Update successfully!',
    'delete_success' => 'Delete successfully!',
    'confirm_delete' => 'Are you sure delete this invoice form?',
    'confirm_change_template' => 'Are you sure change the form template?',
];