<?php
return [
    'no_requirement' => 'No requirement',
    'handle_join_group' => 'Process joining group request',
    'timekeeping_addition' => 'Process additional timekeeping request',
    'take_leave' => 'Process take leave request',
    'switch_shift' => 'Process switch shift request',
    'detail_request_timekeeping_addtion' => 'Detail additional timekeeping request',
    'handle_request' => 'Process request',
    'handle_request_detail' => 'Perform or denied timekeeping for requirements of employee.',
    'error' => 'Error, please try again',
    'success' => 'Process successfully!',
    'executed' => 'The notification has been executed before, you can\'t be executed again.',
    'confirm_addition' => 'Are you sure want to :status this employee\'s additional timekeeping request?',
    'confirm_take_leave' => 'Are you sure want to :status this take leave request?',
    'confirm_witch_shift' => 'Are you sure want to :status this switch shift request?',
    'choose_a_shift' => 'Select a shift you want to change',
    'user_not_in_shift' => 'User is not in the shift',
    'is_exec' => 'Executed',
    'rejected' => 'Rejected',
    'approved' => 'Approved',
    'messages' => [
        1 => "<b>:user_assign</b> required timekeeping additional",
        2 => "<b>:user_assign</b> required switch shift",
        3 => "<b>:user_assign</b> required take leave",
        4 => "<b>:user_assign</b> required take part in your organization",
    ]
];