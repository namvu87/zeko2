<?php
return [
    'common' => [
        'title' => 'Zeko.vn - Management software',
        'desc' => 'Management software use Qr code'
    ],
    'product' => [
        'title' => 'Zeko.vn - Product and service',
        'desc' => 'List of Zeko\'s product and service '
    ],
    'order' => [
        'title' => 'Zeko.vn - Order',
        'desc' => 'Order list'
    ],
    'order-detail' => [
        'title' => 'Zeko.vn - Order information',
        'desc' => 'Order detail'
    ],
    'news' => [
        'title' => 'Zeko.vn - News',
        'desc' => 'List Zeko\'s news'
    ],
    'career' => [
        'title' => 'Zeko.vn - Career',
        'desc' => 'Zeko\'s career'
    ],
    'contact' => [
        'title' => 'Zeko - Contact information',
        'desc' => 'Contact information, customer officer, address and map of Zeko'
    ],
    'about' => [
        'title' => 'Zeko - About us',
        'desc' => 'Introduce for TNHH CÔNG NGHỆ ZEKO Company'
    ],
    'policy' => [
        'title' => 'Zeko - Company policies',
        'desc' => 'Employment policies, treatment of Zeko'
    ],
    'rules' => [
        'title' => 'Zeko - Rules',
        'desc' => 'Terms committed to using the services of Zeko'
    ],
    'costs' => [
        'title' => 'Zeko - Costs',
        'desc' => 'Costs for using the services of Zeko'
    ],
    'guide' => [
        'title' => 'Zeko - Guide',
        'desc' => 'Zeko\'s service manual'
    ],
];