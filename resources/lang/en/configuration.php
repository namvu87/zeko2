<?php
return [
    'save_config' => 'Save change',
    'general' => 'General',
    'customer_care' => 'Customer care',
    'contact_information' => 'Contact information',
    'customer' => 'Customer',
    'saveConfig' => [
        'success' => 'Configuration has been saved successfully.',
    ],
    'customerCare' => [
        'name' => 'Staff\'s name',
        'phone_number' => 'Phone number',
        'zalo' => 'Zalo',
        'skype' => 'Skype',
        'email' => 'Email',
    ],
    'contactInformation' => [
        'address' => 'Address',
        'phone_number' => 'Phone number',
        'fax' => 'Fax',
        'fb' => 'Facebook',
    ],
    'category' => [
        1 => [
            'name' => 'Create organization',
            'icon' => 'fa fa-plus-circle'
        ],
        2 => [
            'name' => 'Employee management',
            'icon' => 'fa fa-users'
        ],
        3 => [
            'name' => 'User permission',
            'icon' => 'fa fa-user-shield'
        ],
        4 => [
            'name' => 'Overview',
            'icon' => 'fa fa-chart-bar'
        ],
        5 => [
            'name' => 'Shift management',
            'icon' => 'fa fa-calendar-alt'
        ],
        6 => [
            'name' => 'Timekeeping management',
            'icon' => 'fa fa-user-clock'
        ],
        7 => [
            'name' => 'Area/Table management',
            'icon' => 'fa fa-table'
        ],
        8 => [
            'name' => 'Goods catalog',
            'icon' => 'fa fa-cube'
        ],
        9 => [
            'name' => 'Invoice management',
            'icon' => 'fa fa-file-invoice'
        ],
        10 => [
            'name' => 'Print template management',
            'icon' => 'fa fa-file'
        ],
        11 => [
            'name' => 'Sale page',
            'icon' => 'fa fa-shopping-basket'
        ],
        12 => [
            'name' => 'Report-Statistic',
            'icon' => 'fa fa-chart-area'
        ],
        13 => [
            'name' => 'Timekeeping app mobile',
            'icon' => 'fa fa-mobile'
        ],
        14 => [
            'name' => 'Sale app mobile',
            'icon' => 'fa fa-mobile-alt'
        ],
        15 => [
            'name' => 'Sale management mobile',
            'icon' => 'fa fa-tablet'
        ],
        16 => [
            'name' => 'Other categories',
            'icon' => 'fa fa-sitemap'
        ],
    ],
    'service' => [
        1 => 'Timekeeping',
        2 => 'Restaurant'
    ],
    'news_type' => [
        1 => 'Normal post',
        2 => 'Introduce about company',
        3 => 'Homepage',
        4 => 'Term',
        5 => 'Career',
        6 => 'Policy',
        7 => 'About company',
        8 => 'Rules',
        9 => 'Costs'
    ],
];