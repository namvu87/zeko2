<?php
return [
    'word' => [
        'update_cover_image' => 'Update cover image',
        'update_avatar'      => 'Update avatar',
    ],
    'profile' => [
        'update_success' => 'User information has been updated successfully'
    ],
    'avatar' => [
        'update_success' => 'Avatar has been updated successfully',
        'update_fail' => 'Error, please try again'
    ]
];