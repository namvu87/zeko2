<?php
return [
    'all_right_reversed' => 'Copyright has been registered.',
    'general' => [
        'greeting' => 'Hello',
        'to' => 'The message was sent to',
        'sender' => 'ZEKO Application Development Team'
    ],
    'registration_success' => [
        'subject' => 'Welcome to ZEKO!',
        'content' => 'Congratulations! You have successfully registered a account in Zeko.vn',
        'action' => 'Confirm',
        'thanks' => 'Thank you for using our application!',
    ],
    'reset_password' => [
        'sent_content' => 'We have sent you a password authentication email. Please check your e-mail box',
        'subject' => 'Reset password',
        'content' => 'We have received your reset password ZEKO request.',
        'action' => 'Click here to reset your password',
        'expire_time' => 'This password reset link will expire in :count minutes.',
        'additional_content' => 'If you did not request a password reset, no further action is required.',
        'thanks' => 'Thank you for using our application!',
        'subcopy' => "If you’re having trouble clicking the \"Click here to reset your password\" button, copy and paste the URL below into your web browser: \n :url",
    ],
    'verify_email' => [
        'verifying' => 'Verifying, please wait at seconds ',
        'verified' => 'Account was already authenticated',
        'not_verified' => 'The Email is not verified',
        'sent' => 'Email has been sent, please check your mail hòm mail for authentication account. Message messages has in 1 hour',
        'subject' => 'Verify Email Address',
        'line_1' => 'Please click the button below to verify your email address.',
        'action' => 'Verify Email Address',
        'line_2' => 'If you did not create an account, no further action is required',
        'line_3' => "If you’re having trouble clicking the \"Verify Email Address\" button, copy and paste the URL below into your web browser: \n [:url](:url) ",
        'fail_message' => 'Invalid, please try again',
        'success_message' => 'Verification successful!',
        'auth_required' => 'You must be logged in before executing account authentication',
        'authorize_invalid' => 'Invalid activation information or expired. Please request to send email to authentication',
        'resend' => 'Resend verify email',
    ],
    'subject' => [
        'pay_service_fee' => 'Notify service fee',
        'disable_group' => 'Notify temporary stop working organization'
    ],
    'payment_periodic' => [
        'header' => "Dear customers!. <br> The deadline for payment of <b> ZEKO </b> service charge to the organization <b>:groupName </b>. <br> <br>",
        'fee_info_timekeeping' => 'Fee for using the company\'s timekeeping service in the past month is :fee',
        'fee_info_restaurant' => 'Your restaurant has a total of <b>:invoice_quantity </b> invoices paid with revenue <b>:revenue </b> in the last month. The cost of the payment packages are as follows: <br> <b> Payment package by invoice quantity: :fee1 </b> to <br> <b> Payment package by revenue: :fee2 </b><br> <b> Fixed payment package: :fee3 </b><br> The minimum amount you must pay is <b>:fee </b> <br> <br>',
        'footer' => 'Please click the button below and pay before the date of <b style: "color: red">:date </b> to continue using the service. <br> Thank you for using the service!',
    ],
    'disabled_group' => 'Welcome! <br> Sorry, we have to inform you that we\'ve suspended some functionality for <b>:group </b>. Please contact our support team to reactivate organization activity!',
];
