<?php
return [
    'content' => 'ZEKO - Your verification code is :code',
    'verify_phone_number' => 'The verification code has been sent to your phone number, enter code to verify your account, please',
    'reset_password_phone_number' => 'The verification code has been sent to your phone number, enter code to change password',
    'verify_success' => 'Verification has been successfully',
    'invalid_code' => 'The verification code has been invalid',
    'error_message' => 'An error has occurred, please contect ZEKO support',
    'enter_verification_code' => 'Please enter your varification code',
    '6_char' => '6 character',
    'update_your_phone_number' => 'Your account has not associated phone number yet. Please update your phone number first'
];
