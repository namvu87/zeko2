<?php
return [
    'messages' => [
        'default' => 'Click or Drag and drop files here',
        'replace' => 'Click or Drag and drop to replace',
        'remove' => 'Remove',
        'error' => 'Sorry, something went wrong.'
    ],
];