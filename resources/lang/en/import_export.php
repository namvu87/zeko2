<?php
return [
    'select_file_type_export' => 'Select the type of export file',
    'export_to_csv' => 'Export CSV file',
    'export_to_xlsx' => 'Export XLSX file',
    'download_file_example' => 'Download example file',
    'select_file' => 'Select file',
    'record_import_null' => 'Data in the file is empty',
    'progress_import' => 'Implementation progress imported from the file',
    'running_import_file' => 'Importing file...',
    'pause_import_file' => 'Pausing import',
    'completed_import_file' => 'Finished import',
    'result_import' => 'Results imported from file',
    'detail_result_pasre_file' => 'Detailed analysis results file',
    'total_record' => 'Total records',
    'total_record_imported' => 'Total records were imported',
    'record_imported_success' => 'Number of records imported successfully',
    'record_imported_fail' => 'Import record number failed',
    'user_not_existed' => 'Quantity users don\'t register an account',
    'user_already_added' => 'Quantity users was member',
    'error_response' => 'Error return',
    'too_many_record' => 'Too many allowed number rows',
    'note' => 'Note: File not over 60 rows',
    'error_parse_data' => [
    	'missing_quotes' => 'Missing quotes',
    	'undetectable_delimiter' => 'Undetectable delimiter',
    	'too_few_fields' => 'Too few fields',
    	'too_many_fields' => 'Too many fields',
    ],
    'import' => [
        'add_user_into_group' => 'Enter a list of employees',
        'import_goods' => 'Import list goods',
        'import_place' => 'Import list places',
        'import_table' => 'Import list tables'
    ],
    'message_error_import' => [
        1 => 'Import successful',
        2 => 'Import failed',
        3 => 'Error could not find the object in the system',
        4 => 'Validate data error'
    ],
    'message_accomplished_import' => 'Completed file import process'
];