<?php
return [
    'validate' => [
        'month_count_invalid' => 'Number of months is invalid. ',
        'user_count_invalid' => 'Number of employees is invalid',
        'authorize' => 'Please use company manager account to register the service!',
        'min_purchase' => 'Minimum payment amount is :value VNĐ',
    ],
    'description' => 'PurchaseService payment service: name Zeko',
    'fail_process' => 'Error, please try again!',
    'invalid_information' => 'Transaction failed, please try again!',
    'thankyou' => 'Registration successfully, thank you for using our service!',
    'restaurant' => [
        'package_1' => 'Payment package by invoice\'s quantity',
        'package_2' => 'Payment package by revenue',
        'package_3' => 'Fixed package',
    ],
    'total_fee' => 'Total service fee',
    'pay_with' => 'Pay with ',
    'select_purchase_method' => 'Select purchase method',
    'bad_payment_info' => 'Invalid payment information',
    'inprogress' => 'Inprogress',
    'how_to_charge' => 'How to charge',
    'select_how_to_charge' => 'Selected how to charge method'
];
