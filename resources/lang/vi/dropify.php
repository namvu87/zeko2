<?php
return [
    'messages' => [
        'default' => 'Kéo và thả tệp vào đây hoặc nhấp',
        'replace' => 'Kéo và thả hoặc nhấp để thay thế',
        'remove' => 'Xóa',
        'error' => 'Rất tiếc, đã xảy ra sự cố.'
    ],
];