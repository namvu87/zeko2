<?php
return [
    'all_right_reversed' => 'Đã đăng ký bản quyền.',
    'regards' => 'Trân trọng',
    'general' => [
        'greeting' => 'Xin chào',
        'to' => 'Tin nhắn này được gửi tới',
        'sender' => 'Nhóm phát triển ứng dụng ZEKO'
    ],
    'registration_success' => [
        'subject' => 'Chào mừng bạn đến với ZEKO!',
        'content' => 'Chúc mừng bạn đã đăng ký thành công tài khoản trang web Zeko.com',
        'action' => 'Xác nhận',
        'thanks' => 'Cảm ơn bạn đã sử dụng ứng dụng của chúng tôi!',
    ],
    'reset_password' => [
        'sent_content' => 'Chúng tôi đã gửi email xác thực mật khẩu cho bạn. Vui lòng kiểm tra hòm thư điện tử của bạn',
        'subject' => 'Yêu cầu đặt lại mật khẩu',
        'content' => 'Chúng tôi đã nhận được yêu cầu đặt lại mật khẩu ZEKO của bạn.',
        'action' => 'Nhấn vào đây để đặt lại mật khẩu của bạn',
        'expire_time' => 'Liên kết đặt lại mật khẩu này sẽ hết hạn sau :count phút.',
        'additional_content' => 'Nếu bạn không yêu cầu đặt lại mật khẩu, không cần thực hiện thêm hành động nào.',
        'thanks' => 'Cảm ơn bạn đã sử dụng ứng dụng của chúng tôi!',
        'subcopy' => "Nếu bạn gặp sự cố khi nhất nút \"Đặt lại mật khẩu\", hãy thử copy và dán đường dẫn dưới đây vào trình duyệt: \n :url"
    ],
    'verify_email' => [
        'verifying' => 'Thông tin đang được xác minh, vui lòng chờ trong giây lát ',
        'verified' => 'Tài khoản đã được xác thực trước đó',
        'not_verified' => 'Email chưa được xác minh',
        'sent' => 'Email đã được gửi lại, vui lòng kiểm tra hòm thư của bạn để xác thực tài khoản. Tin nhắn có hiệu lực trong 1 giờ',
        'subject' => 'Xác thực địa chỉ Email',
        'line_1' => 'Vui lòng bấm vào linh dưới đây để xác thực địa chỉ email của bạn.',
        'action' => 'Xác thực Email',
        'line_2' => 'Nếu bạn không tạo tài khoản, bạn không cần thực hiện hành động nào.',
        'line_3' => "Nếu bạn gặp gặp vấn đề khi bấm vào nút \"Xác thực Email\", hãy thử copy và dán đường dẫn dưới đây vào trình duyệt: \n [:url](:url) ",
        'fail_message' => 'Không hợp lệ, vui lòng kiểm tra lại',
        'success_message' => 'Xác thực thành công!',
        'auth_required' => 'Bạn phải đăng nhập trước khi thực hiện xác thực tài khoản',
        'authorize_invalid' => 'Thông tin kích hoạt không đúng hoặc đã hết hạn. Vui lòng yêu cầu gửi lại email để xác thực',
        'resend' => 'Gửi lại email xác thực'
    ],
    'subject' => [
        'pay_service_fee' => 'Thông báo thanh toán phí dịch vụ',
        'disable_group' => 'Thông báo tạm dừng hoạt động nhóm'
    ],
    'payment_periodic' => [
        'header' => "Kính chào quý khách hàng!.<br> Đã đến kỳ hạn thanh toán phí dịch vụ <b>ZEKO</b> cho tổ chức <b>:groupName</b>.<br><br>",
        'fee_info_timekeeping' => 'Phí sử dụng dịch vụ chấm công của công ty trong tháng vừa qua là :fee',
        'fee_info_restaurant' => 'Nhà hàng của bạn có tổng cộng <b>:invoice_quantity</b> hóa đơn thanh toán với doanh thu <b>:revenue</b> trong tháng vừa qua. Chi phí theo các gói thanh toán như sau:<br><b>Gói thanh toán theo số lượng hóa đơn: :fee1</b> đ<br><b>Gói thanh toán theo doanh thu: :fee2</b> đ<br><b>Gói thanh toán cố định: :fee3</b> đ<br>Chi phí bạn phải thanh toán tối thiểu là <b>:fee</b> đ <br><br>',
        'footer' => 'Vui lòng kích vào nút bên dưới và thanh toán trước ngày mùng <b style="color: red">:date</b> để tiếp tục sử dụng dịch vụ.<br> Cảm ơn quý khách đã sử dụng dịch vụ!',
    ],
    'disabled_group' => 'Kính chào quý khách!<br> Rất tiếc khi chúng tôi phải thông báo cho quý khách rằng chúng tôi đã tạm dừng hoạt động một số chức năng đối với tổ chức <b>:group</b>. Vui lòng liên hệ với nhóm hỗ trợ của chúng tôi để kích hoạt lại hoạt động của tổ chức!',
];
