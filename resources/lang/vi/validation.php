<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => ':attribute phải sau :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => ':attribute chỉ có thể là dạng chữ cái, số, dấu gạch ngang hoặc gạch dưới',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => ':attribute phải là một mảng.',
    'before' => ':attribute phải là một ngày sau :date.',
    'before_or_equal' => ':attribute phải sau hoặc bằng ngày :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => ':attribute phải có dạng boolean.',
    'confirmed' => ':attribute không khớp.',
    'date' => 'The :attribute is not a valid date.',
    'date_format' => ':attribute không đúng định dạng :format.',
    'different' => ':attribute phải kh ác:other.',
    'digits' => ':attribute phải dài đúng :digits chữ số.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => ':attribute không được trùng lặp.',
    'email' => ':attribute không có định dạng hợp lệ.',
    'exists' => ':attribute không tồn tại.',
    'file' => ':attribute phải là một file.',
    'filled' => 'The :attribute field must have a value.',
    'image' => ':attribute không đúng định dạng ảnh.',
    'in' => ':attribute không hợp lệ.',
    'in_array' => ':attribute không hợp lệ.',
    'integer' => ':attribute phải là 1 số nguyên.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'max' => [
        'numeric' => ':attribute không được lớn hơn :max.',
        'file' => ':attribute không được vượt quá  :max Kb.',
        'string' => ':attribute không được dài hơn :max ký tự.',
        'array' => ':attribute không được có nhiều hơn :max phần tử.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => ':attribute phải lớn hơn :min.',
        'file' => ':attribute phải có dung lượng lớn hơn :min Kb.',
        'string' => ':attribute phải chứa ít nhất :min ký tự.',
        'array' => ':attribute phải chứa ít nhất :min phần tử.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => ':attribute phải là dạng số.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => ':attribute không được để trống.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'lte' => [
        'numeric' => ':attribute phải nhỏ hơn hoặc bằng :value',
    ],
    'string' => ':attribute phải là dạng chuỗi.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => ":attribute đã thực sự tồn tại. \nVui lòng chọn một :attribute khác.",
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute phải là định dạng liên kết ví dụ: http://abc.com',
    'captcha' => 'Mã captcha không hợp lệ hoặc đã hết hết',
    'password_matched' => 'Mật khẩu không đúng',
    'equal' => ':attribute có giá trị không chính xác',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'parent_id' => [
            'exists' => 'Nhóm hàng hoá cha không tồn tại'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'captcha' => 'Mã captcha',
        'username' => 'Thông tin đăng nhập',
        'address' => 'Địa chỉ',
        'password' => 'Mật khẩu',
        'old_password' => 'Mật khẩu',
        'current_password' => 'Mật khẩu hiện tại',
        'email' => 'Địa chỉ email',
        'first_name' => 'Tên',
        'last_name' => 'Họ',
        'name' => 'Tên',
        'area' => 'Khu vực',
        'service_type' => 'Loại dịch vụ',
        'commune' => 'Địa chỉ phường xã',
        'birthday' => 'Ngày sinh',
        'gender' => 'Giới tính',
        'avatar' => 'Ảnh đại diện',
        'company_name' => 'Tên công ty',
        'company_address' => 'Địa chỉ công ty',
        'tax_code' => 'Mã số thuế',
        'current_pasword' => 'Mật khẩu hiện tại',
        'new_password' => 'Mật khẩu mới',
        'start_time_1' => 'Thời gian bắt đầu',
        'end_time_1' => 'Thời gian kết thúc',
        'start_time_2' => 'Thời gian bắt đầu',
        'end_time_2' => 'Thời gian kết thúc',
        'type' => 'Thể loại',
        'company_id' => 'ID công ty',
        'start_date' => 'Ngày bắt đầu',
        'end_date' => 'Ngày kết thúc',
        'month_year' => 'Tháng',
        'phone' => 'Số điện thoại',
        'expiration_date' => 'Ngày hết hạn',
        'phone_number' => 'Số điện thoại',
        'fax' => 'Số fax',
        'fb' => 'Địa chỉ facebook',
        'title' => 'Tiêu đề',
        'tags' => 'Danh sách thẻ tag',
        'description' => 'Mô tả',
        'content' => 'Nội dung',
        'price_type' => 'Loại giá',
        'detail' => 'Chi tiêt',
        'user_guide' => 'Hướng dẫn sử dụng',
        'user_count' => 'Số lượng người dùng',
        'code' => 'Mã',
        'link' => 'Địa chỉ liên kết',
        'image' => 'Hình ảnh',
        'term' => 'Thời hạn',
        'src' => 'Địa chỉ liên kết',
        'checkin1' => 'Thời gian vào',
        'checkout1' => 'Thời gian ra',
        'time_keeping_id' => 'ID chấm công',
        'checkin2' => 'Thời gian vào',
        'checkout2' => 'Thời gian ra',
        'date' => 'Ngày',
        'customer_name' => 'Tên khách hàng',
        'customer_phone' => 'Số điện thoại khách hàng',
        'customer_email' => 'Địa chỉ email khách hàng',
        'order_id' => 'Đơn hàng',
        'transaction_id' => 'Giao dịch',
        'sort' => 'Số thứ tự',
        'place_id' => 'Khu vực',
        'group_menu_ids' => 'Nhóm hàng hóa',
        'price' => 'Giá bán',
        'unit' => 'Đơn vị đo',
        'properties.*.name' => 'Tên thuộc tính',
        'properties.*.value' => 'Giá trị thuộc tính',
        'conversion_units.*.unit' => 'Đơn vị tính',
        'conversion_units.*.value' => 'Tỉ lệ quy đổi',
        'conversion_units.*.price' => 'Giá bán',
        'price_origin' => 'Giá gốc',
        'inventory_number' => 'Tồn kho',
        'inventory_min' => 'Tồn kho tối đa',
        'inventory_max' => 'Tồn kho tối thiểu',
        'images.*' => 'Ảnh',
        'images' => 'Ảnh',
        'ingredients' => 'Hàng hóa thành phần',
        'ingredients.*.count' => 'Số lượng',
        'goods' => 'Hàng hóa',
        'start' => 'Thời gian bắt đầu',
        'end' => 'Thời gian kết thúc',
        'note' => 'Ghi chú',
        'area.name' => 'Khu vực',
        'commune.name' => 'Xã, phường',
        'user_id' => 'Người dùng',
        'role_id' => 'Vai trò',
        'count_seat' => 'Số chỗ ngồi',
        'property' => 'Tên thuộc tính',
        'goods.*.discount' => 'Giảm giá',
        'shift_id' => 'Ca làm việc'
    ],

];
