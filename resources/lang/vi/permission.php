<?php
return [
    'error' => 'Truy câp không được cho phép, vui lòng kiểm tra lại',
    'message' => [
        'assign_user_role_success' => 'Gán vai trò cho người dùng thành công',
        'change_user_role_success' => 'Thay đổi vai trò cho người dùng thành công',
        'cannot_assign_owener_role' => 'Không thể gán quyền chủ sở hữu',
        'cannot_revoke_owner_role' => 'Không thể thu hồi quyền của chủ sở hữu',
        'cannot_change_owner_role' => 'Không thể thay đổi quyền của vai trò sở hữu',
    ],
    'role' => [
        'owner' => 'Chủ sở hữu',
        'admin' => 'Người quản lý',
        'hrm' => 'HRM',
        'receptionist' => 'Lễ tân',
        'cashier' => 'Thu ngân',
        'waiters' => 'Chạy bàn',
        'chef' => 'Đầu bếp',
        'none' => ''
    ],
    'group_permission' => [
        'employee' => 'Nhân viên',
        'shift' => 'Ca làm việc',
        'timekeeping' => 'Chấm công',
        'place' => 'Khu vực',
        'table' => 'Bàn',
        'group-menus' => 'Nhóm hàng hóa',
        'goods' => 'Hàng hóa',
        'invoice' => 'Hóa đơn',
        'imported-invoice' => 'Nhập hàng',
        'form-invoice' => 'Mẫu in',
        'report' => 'Báo cáo thống kê',
        'manage-sale' => 'Quản lý bán hàng',
        'returned-invoice' => 'Quản lý trả hàng',
        'group' => 'Quản lý nhóm',
        'permission' => 'Quản lý phân quyền',
        'requirement' => 'Quản lý yêu cầu'
    ],
    'permission' => [
        'employee' => [
            'list' => 'Xem danh sách',
            'add' => 'Thêm nhân viên',
            'remove' => 'Xóa nhân viên'
        ],
        'shift' => [
            'list' => 'Xem danh sách',
            'create' => 'Tạo mới ca',
            'update' => 'Cập nhật',
            'delete' => 'Xóa ca',
            'user-list' => 'Xem danh sách nhân viên trong ca',
            'user-add' => 'Gán nhân viên',
            'user-delete' => 'Xoá nhân viên'
        ],
        'timekeeping' => [
            'list' => 'Xem danh sách',
            'detail' => 'Xem chi tiết',
        ],
        'report' => [
            'timekeeping-group' => 'Báo cáo theo nhóm',
            'timekeeping-employee' => 'Báo cáo chi tiết nhân viên',
            'today' => 'Báo cáo cuối ngày',
            'sale' => 'Báo cáo bán hàng',
            'goods' => 'Báo cáo hàng hoá'
        ],
        'requirement' => [
            'list' => 'Xem danh sách yêu cầu',
            'handle' => 'Xử lý yêu cầu'
        ],
        'permission' => [
            'role' => 'Xem vai trò',
            'role-permission' => 'Xem quyền trong vai trò',
            'role-assign' => 'Gán vai trò',
            'role-revoke' => 'Thu hồi vai trò',
            'update-role-permission' => 'Thiết lập quyền cho vai trò'
        ],
        'group' => [
            'update-profile' => 'Cập nhật thông tin nhóm',
            'change-service' => 'Thay đổi loại hình dịch vụ',
            'pay-fee' => 'Xem và thanh toán chi phí sử dụng dịch vụ'
        ],
        'place' => [
            'list' => 'Xem danh sách',
            'create' => 'Tạo thêm khu vực',
            'update' => 'Cập nhật',
            'delete' => 'Xóa khu vực'
        ],
        'table' => [
            'list' => 'Xem danh sách',
            'create' => 'Tạo thêm bàn',
            'update' => 'Cập nhật',
            'delete' => 'Xóa bàn'
        ],
        'group-menus' => [
            'list' => 'Xem danh sách',
            'create' => 'Tạo thêm nhóm thực đơn',
            'update' => 'Cập nhật',
            'delete' => 'Xóa nhóm thực đơn'
        ],
        'goods' => [
            'list' => 'Xem danh sách',
            'create' => 'Tạo mới hàng hóa',
            'update' => 'Cập nhật',
            'delete' => 'Xóa hàng hóa'
        ],
        'form-invoice' => [
            'list' => 'Xem danh sách',
            'create' => 'Tạo mới mẫu',
            'update' => 'Cập nhật',
            'delete' => 'Xóa mẫu'
        ],
        'invoice' => [
            'list' => 'Xem danh sách',
            'selling-list' => 'Danh sách đang hoạt động',
            'create' => 'Tạo mới hóa đơn',
            'detail' => 'Xem chi tiết hoá đơn',
            'update' => 'Cập nhật hóa đơn',
            'delete' => 'Xóa hóa đơn',
            'disable' => 'Vô hiệu hóa đơn',
            'checkout' => 'Thanh toán'
        ],
        'imported-invoice' => [
            'list' => 'Xem danh sách',
            'create' => 'Tạo mới',
            'detail' => 'Xem chi tiết',
            'disable' => 'Vô hiệu hoá đơn'
        ],
        'returned-invoice' => [
            'list' => 'Xem danh sách',
            'create' => 'Tạo mới',
            'detail' => 'Xem chi tiết',
            'disable' => 'Vô hiệu hoá đơn'
        ]
    ]
];
