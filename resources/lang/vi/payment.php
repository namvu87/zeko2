<?php
return [
    'validate' => [
        'month_count_invalid' => 'Số lượng tháng không hợp lệ. ',
        'user_count_invalid' => 'Số lượng nhân viên không hợp lệ',
        'authorize' => 'Vui lòng sử dụng tài khoản công ty quản lý để đăng ký sử dụng dịch vụ',
        'min_purchase' => 'Số tiền thanh toán không được nhỏ hơn :value VNĐ',
    ],
    'description' => 'Thanh toán mua dịch vụ :name Zeko',
    'fail_process' => 'Xảy ra lỗi, vui lòng thực hiện lại thao tác',
    'invalid_information' => 'Giao dịch không thành công, vui lòng thực hiện lại',
    'thankyou' => 'Đăng ký thành công. Cảm ơn khách hàng đã sử dụng dịch vụ của chúng tôi',
    'restaurant' => [
        'package_1' => 'Gói thanh toán theo số lượng hóa đơn',
        'package_2' => 'Gói thanh toán theo doanh thu',
        'package_3' => 'Gói cố định',
    ],
    'total_fee' => 'Tổng chi phí',
    'pay_with' => 'Thanh toán bằng ',
    'select_purchase_method' => 'Lựa chọn phương thức thanh toán',
    'bad_payment_info' => 'Thông tin thanh toán không đúng, Vui lòng kiểm tra lại',
    'inprogress' => 'Đang thanh toán',
    'how_to_charge' => 'Cách tính phí',
    'select_how_to_charge' => 'Lựa chọn cách tính phí'
];
