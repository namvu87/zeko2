<?php
return [
    'common' => [
        'title' => 'Zeko.vn - Phần mềm quản lý',
        'desc' => 'Phầm mềm quản lý sử dụng công nghê QR code'
    ],
    'product' => [
        'title' => 'Zeko.vn - Sản phẩm và dịch vụ',
        'desc' => 'Danh sách các gói sản phẩm và dịch vụ của Zeko'
    ],
    'order' => [
        'title' => 'Zeko.vn - Đơn hàng',
        'desc' => 'Danh sách đơn hàng'
    ],
    'order-detail' => [
        'title' => 'Zeko.vn - Thông tin đơn hàng',
        'desc' => 'Thông tin chi tiết đơn hàng'
    ],
    'news' => [
        'title' => 'Zeko.vn - Tin tức',
        'desc' => 'Danh sách các bài viết của Zeko'
    ],
    'career' => [
        'title' => 'Zeko.vn - Tuyển dụng',
        'desc' => 'Tin tức tuyển dụng của Zeko'
    ],
    'contact' => [
        'title' => 'Zeko - Thông tin liên hệ',
        'desc' => 'Thông tin liên hệ, nhân viên chăm sóc khách hàng, địa chỉ và bản đồ vị trí của Zeko'
    ],
    'about' => [
        'title' => 'Zeko - Về chúng tôi',
        'desc' => 'Giới thiệu về công ty TNHH công nghệ Zeko'
    ],
    'policy' => [
        'title' => 'Zeko - Chính sách công ty',
        'desc' => 'Chính sách việc làm, chế độ đãi ngộ của Zeko'
    ],
    'rules' => [
        'title' => 'Zeko - Điều khoản sử dụng',
        'desc' => 'Điều khoản cam kết khi sử dụng dịch vụ của Zeko'
    ],
    'costs' => [
        'title' => 'Zeko - Phí dịch vụ',
        'desc' => 'Chi phí sử dụng dịch vụ của Zeko'
    ],
    'guide' => [
        'title' => 'Zeko - Hướng dẫn',
        'desc' => 'Hướng dẫn sử dụng dịch vụ của Zeko'
    ],
];