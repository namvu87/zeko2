<?php
return [
    'master' => [
        'number_group' => 'Số tổ chức',
        'number_product' => 'Số sản phẩm',
        'number_order' => 'Số đơn hàng',
        'number_employee' => 'Số nhân viên',
        'number_user' => 'Số thành viên',
        'revenue' => 'Doanh thu',
        'new_group_list' => 'DS các tổ chức mới nhất',
        'name' => 'Tên',
        'type' => 'Loại',
        'price' => 'Gía',
        'register_date' => 'Ngày đăng ký',
        'list_product_selling' => 'DS các sản phẩm bán nhiều nhất'
    ],
    'company' => [
        'number_company_branch' => 'Số công ty con',
        'number_employee' => 'Số nhân viên',
        'number_shift' => 'Số ca làm việc',
        'expiration_date' => 'Ngày hết hạn',
        'company_info' => 'Thông tin công ty',
        'calendar' => 'Lịch làm việc',
        'list_new_employee' => 'DS nhân viên mới đăng ký',
        'shifts' => 'Các ca làm việc',
        'name' => 'Tên',
        'type' => 'Loại',
        'calendar' => 'Lịch',
        'start_time' => 'Bắt đầu',
        'end_time' => 'Kết thúc',
    ],
];