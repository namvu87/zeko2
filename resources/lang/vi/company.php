<?php
return [
    'company' => [
        'name' => 'Tên công ty',
        'profile' => 'Hồ sơ công ty',
        'information' => 'Thông tin',
        'avatar' => 'Ảnh đại diện',
        'require' => 'Cần điền thông tin đầy đủ vào các trường đánh dấu',
        'update_profile_success' => 'Cập nhật hồ sơ thành công.',
        'update_profile_error' => 'Có lỗi xảy ra trong khi cập nhật hồ sơ, vui lòng kiểm tra lại.',
        'change_password_success' => 'Thay đổi mật khẩu thành công.',
        'change_password_error' => 'Có lỗi xảy ra trong khi thay đổi mật khẩu, vui lòng kiểm tra lại.',
        'your_current_password_not_matches' => 'Mật khẩu hiện tại của bạn không khớp với mật khẩu bạn đã cung cấp. Vui lòng thử lại.',
        'new_password_same_current_password' => 'Mật khẩu mới không thể giống với mật khẩu hiện tại của bạn. Vui lòng chọn một mật khẩu khác.',
    ],
    'branch' => [
        'create_success' => 'Thêm mới thành công 1 chi nhánh',
        'not_found' => 'Công ty không tồn tại hoặc đã bị xóa, vui lòng kiểm tra lại',
        'update_success' => 'Cập nhật hoàn tất.',
        'disable_branch' => 'Bạn có chắc chắn muốn dừng hoạt động chi nhánh này?',
        'enable_branch' => 'Bạn có chắc chắn muốn mở lại hoạt động chi nhánh này?',
        'username_not_allowed_edit' => 'Tên đăng nhập không được phép chỉnh sửa',
    ],
    'user' => [
        'add_user_success' => 'Đã thêm người dùng này vào nhân viên công ty của bạn thành công!',
        'add_user_confirm' => 'Bạn có chắc chắn muốn thêm người dùng này vào danh sách nhân viên công ty?',
        'delete_user_confirm' => 'Bạn có chắc chắn muốn xóa nhân viên này khỏi danh sách của công ty không?',
        'delete_user_success' => 'Xóa người dùng khỏi công ty thành công',
        'user_exists' => 'Người dùng này đã thực sự là thành viên của công ty bạn',
        'add_user_fail' => 'Xảy ra lỗi, vui lòng kiểm tra lại',
        'company_not_permission' => 'Bạn không có quyền thực hiện thao tác này',
    ],
    'shift' => [
        'no_select_action' => 'Bạn cần lựa chọn một thao tác trước khi xác nhận',
        'add_user_confirm' => 'Bạn có chắc chắn muốn thêm nhân viên vào ca làm việc này không?',
        'add_user_success' => 'Đã thêm nhân viên vào ca làm việc thành công',
        'add_users_confirm' => 'Bạn có chắc chắn muốn thêm các nhân viên vào ca làm việc này không?',
        'add_users_success' => 'Đã thêm những nhân viên này vào ca làm việc thành công',
        'delete_shift_confirm' => 'Bạn có chắc chắn muốn xóa ca làm việc này không?',
        'delete_user_confirm' => 'Bạn có chắc chắn muốn xóa nhân viên khỏi ca làm việc này không?',
        'delete_user_success' => 'Xóa người dùng khỏi ca làm việc thành công',
        'company_not_permission' => 'Bạn không có quyền thực hiện thao tác này',
        'shift_existed' => 'Người dùng đã có ca làm việc khác trong nhóm này',
        'not_exists_shift' => 'Bạn không có ca làm việc nào ở đây'
    ],
    'permission' => [
        'delete_user_confirm' => 'Bạn có chắc chắn muốn xóa vai trò của người dùng này không?',
        'delete_user_success' => 'Xóa vai trò của người dùng thành công',
        'update_role_permissions_success' => 'Bạn đã thay đổi quyền thành công'
    ]
];