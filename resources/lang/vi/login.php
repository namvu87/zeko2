<?php
return [
    'title' => 'Trang quản trị nội dung | Công ty ZEKO Việt Nam',
    'word' => [
        'sign_in' => 'Đăng nhập vào trang quản trị',
        'sign_up' => 'Đăng ký tài khoản',
        'reset_password' => 'Thay đổi mật khẩu',
        'enter_detail' => 'Vui lòng nhập đầy đủ thông tin',
        'organization_name' => 'Tên công ty, nhà hàng hoặc tổ chức',
        'email_desc' => '<b>Email</b> là thông tin để đăng nhập và để lấy lại mật khẩu khi quên.<br> <b>Email</b> là duy nhất',
        'phone_number_desc' => '<b>Số điện thoại</b> là thông tin để đăng nhập và để lấy lại mật khẩu khi quên.<br>
        <b>Số điện thoại</b> nhập vào phải có định dạng 0xxx (10 số).<br> <b>Số điện thoại</b> là duy nhất',
        'why_email' => 'Tại sao cần cung cấp email?',
        'why_phone_number' => 'Tại sao cần cung cấp số điện thoại?'
    ],
    'message' => [
        'auth_fail' => "Thông tin đăng nhập không đúng.\n Vui lòng thử lại.",
        'error' => 'Có lỗi trong quá trình đăng nhập, vui lòng thử lại',
        'success' => 'Đăng nhập thành công'
    ],
    'payment' => [
        'auth_required' => 'Vui lòng đăng nhập, hoặc đăng ký tài khoản trước khi thực hiện thao tác mua dịch vụ',
    ]
];