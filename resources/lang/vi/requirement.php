<?php
return [
    'no_requirement' => 'Không có yêu cầu',
    'handle_join_group' => 'Xử lý yêu cầu tham gia nhóm,tổ chức',
    'timekeeping_addition' => 'Xử lý yêu cầu chấm công bổ sung',
    'take_leave' => 'Xử lý yêu cầu xin nghỉ phép',
    'switch_shift' => 'Xử lý yêu cầu chuyển ca làm việc',
    'detail_request_timekeeping_addtion' => 'Nội dung chi tiết yêu cầu chấm công bổ sung',
    'handle_request' => 'Xử lý  yêu cầu',
    'handle_request_detail' => 'Thực hiện chấm công hoặc từ chối chấm công cho yêu cầu của nhân viên',
    'error' => 'Xảy ra lỗi, vui lòng thử lại',
    'success' => 'Xử lý thành công!',
    'executed' => 'Thông báo đã được xử lý trước đó, không thể thực hiện lại',
    'confirm_addition' => 'Bạn có chắc chắn :status chấm công bổ sung cho yêu cầu này?',
    'confirm_take_leave' => 'Bạn có chắc chắn :status yêu cầu xin nghỉ phép?',
    'confirm_witch_shift' => 'Bạn có chắc chắn :status thực hiện chuyển ca làm việc?',
    'choose_a_shift' => 'Chọn ca làm việc muốn chuyển',
    'user_not_in_shift' => 'Người dùng không còn trong ca làm việc',
    'is_exec' => 'Đã xử lý',
    'rejected' => 'Đã từ chối',
    'approved' => 'Đã chấp thuận',
    'messages' => [
        1 => "<b>:user_assign</b> yêu cầu chấm công bổ sung",
        2 => "<b>:user_assign</b> yêu cầu chuyển ca làm việc",
        3 => "<b>:user_assign</b> yêu cầu nghỉ phép",
        4 => "<b>:user_assign</b> yêu cầu tham gia tổ chức của bạn",
    ]
];