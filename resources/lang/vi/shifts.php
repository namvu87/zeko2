<?php
return [
    'add' => 'Thêm ca làm việc',
    'shift_name' => 'Tên ca làm việc',
    'shift_info' => 'Thông tin ca làm việc hiện tại',
    'infoShift' => [
        'success' => 'Thêm mới ca làm việc thành công!',
        'edit_success'=>'Ca làm việc đã được chỉnh sửa!',
        'delete_success' => 'Xóa ca làm việc thành công!',
        'error_danger' => "Ca làm việc không tồn tại!",
        'start_time' => 'Vui lòng nhập thời gian bắt đầu công việc',
        'end_time' => 'Vui lòng nhập thời gian kết thúc công việc',
    ],
    'choose_type_shift' => 'Chọn loại ca làm việc phù hợp với yêu cầu của công ty bạn',
    '2_times_scan_per_day' => 'Quét 2 lần 1 ngày',
    '4_times_scan_per_day' => 'Quét 4 lần 1 ngày',
    'first_haft_shift' => 'nửa ca đầu',
    'second_haft_shift' => 'nửa ca sau',
    'user_already_have_shift' => 'Nhân viên đã có ca làm việc trước đó!',
    'allow_time' => 'Thời gian sai lệch cho phép',
    'allow_time_desc' => 'Ví dụ: Nếu bạn đặt là 10 phút thì nhân viên đi muộn ít hơn 10 phút, hay về sớm ít hơn 10 phút sẽ không bị tính là đi muộn, hay về sớm. Nếu không cho phép hãy đặt bằng 0',
    'user_not_shift' => 'Nhân viên chưa có ca làm việc tại công ty'
];