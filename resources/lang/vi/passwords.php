<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Mật khẩu phải có ít nhất sáu ký tự và khớp với xác nhận.',
    'reset' => 'Mật khẩu của bạn đã được thiết lập lại!',
    'sent' => 'Chúng tôi đã gửi liên kết đặt lại mật khẩu qua email của bạn!',
    'token' => 'Mã thông báo đặt lại mật khẩu này không hợp lệ.',
    'user' => "Chúng tôi không thể tìm thấy người dùng có địa chỉ email đó.",
    'change_password_success' => 'Đã cập nhật mật khẩu thành công',
    'change_password_error' => 'Mật khẩu không đúng, vui lòng nhập lại',
    'reset_password_success' => 'Đã đặt lại mật khẩu thành công',
    'api' => [
        'phone_number_not_exists' => 'Số điện thoại chưa được đăng ký',
        'error' => 'Đã xảy ra lỗi, vui lòng kiểm tra lại',
        'over_retries' => 'Bạn đã thử lại quá nhiều lần, Vui lòng chờ sau :time để thử lại',
        'success' => 'Vui lòng nhập mã số từ tin nhắn nhận được của bạn'
    ]
];
