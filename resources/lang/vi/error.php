<?php
return [
    'error_message' => 'Có lỗi xảy ra. Vui lòng kiểm tra lại.',
    'go_back' => 'Quay lại',
    'go_home' => 'Trang chủ',
    '403' => 'Yêu cầu không thể thực hiện, vui lòng kiểm tra lại',
    '404' => 'Đường dẫn bạn truy cập không tồn tại hoặc đã bị xóa. Vui lòng kiểm tra lại.',
    '500' => 'Đã có lỗi xảy ra. Chúng tôi đang gặp sự cố máy chủ nội bộ. Vui lòng thử lại sau.',
    'invalid_router' => 'Đường dẫn không hợp lệ'
];