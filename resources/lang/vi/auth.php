<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Thông tin đăng nhập không chính xác. Vui lòng nhập lại!',
    'throttle' => 'Đăng nhập quá số lần được phép. Vui lòng đợi :seconds giây để đăng nhập lại.',
    'jwt' => [
        'expires_token' => 'Token đã hết hạn, vui lòng đăng nhập lại',
        'invalid_token' => 'Token không hợp lệ, vui lòng đăng nhập lại',
        'absent_token' => 'Lỗi xác thực, vui lòng đăng nhập lại'
    ],
    'term' => 'Bạn phải đồng ý với điều khoản sử dụng dịch vụ',
    'register' => [
        'success' => 'Đăng ký thành công, vui lòng kích hoạt tài khoản từ email của bạn.'
    ],
    'action_unauthorized' => 'Hành động không hợp lệ',
    'unauthorized' => 'Lỗi xác thực',
    'expires_token' => 'Phiên đăng nhập hết hạn',
    'not_verified' => "<b>Tài khoản của bạn chưa được xác thực</b>. Vui lòng xác thực email hoặc số điện thoại để tiếp tục sử dụng đầy đủ tính năng của ứng dụng.",
    'resend_email' => 'Gửi lại email xác thực',
    'resend_verification_code' => 'Xác thực số điện thoại',
    'you_are_not_in_group' => 'Bạn không nằm trong nhóm này',
    'verification' => 'Xác thực tài khoản',
    'welcome_content' => 'Cảm ơn bạn đã đăng ký tài khoản ZEKO. Để sử dụng đầy đủ chức năng bạn vui lòng xác thực tài khoản của mình qua email, hoặc số điện thoại. Vui lòng chọn',
    'contact_zeko' => 'Nếu không thể xác thực tài khoản, vui lòng liên hệ bộ phận hỗ trợ của ZEKO',
    'verify' => 'Xác thực',
    'attemp_too_many' => 'Bạn đã thử qua nhiều lần, vui lòng thử lại sau :counter',
    'verify_email_and_phone_number_is_required' => 'Bạn phải xác thực email và số điện thoại trước khi đăng ký cơ sở kinh doanh'
];
