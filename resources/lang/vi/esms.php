<?php
return [
    'content' => 'ZEKO - Ma so xac thuc cua ban la :code',
    'verify_phone_number' => 'Mã kích hoạt đã được gửi đến số điện thoại của bạn, vui lòng nhập mã kích hoạt để xác thực tài khoản.',
    'reset_password_phone_number' => 'Mã xác thực tài khoản đã được gửi đến số điện thoại của bạn, vui lòng nhập xác thực để đổi mật khẩu',
    'verify_success' => 'Xác thực thành công',
    'invalid_code' => 'Mã xác thực không hợp lệ hoặc đã hết hạn',
    'error_message' => 'Xảy ra lỗi, vui lòng liên hệ bộ phận hỗ trợ của ZEKO',
    'enter_verification_code' => 'Vui lòng nhập mã xác thực của bạn. Lưu ý, mã xác thực chỉ có tác dụng trong 5 phút',
    '6_char' => '6 kí tự',
    'update_your_phone_number' => 'Tài khoản của bạn chưa liên kết số điện thoại. Vui lòng cập nhật số điện thoại của bạn trước'
];
