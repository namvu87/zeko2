<?php
return [
    'invoice_form_empty' => 'Mẫu in đang trống!',
    'create_success' => 'Thêm mới mẫu in thành công!',
    'update_success' => 'Cập nhật mẫu in thành công!',
    'delete_success' => 'Xóa mẫu in thành công!',
    'confirm_delete' => 'Bạn có chắc muốn xóa mẫu in không?',
    'confirm_change_template' => 'Bạn có chắc chắn muốn thay đổi lại mẫu in không?',
];