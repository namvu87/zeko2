<?php
return [
    'select_file_type_export' => 'Lựa chọn loại file export',
    'export_to_csv' => 'Xuất file CSV',
    'export_to_xlsx' => 'Xuất file XLSX',
    'download_file_example' => 'Tải file mẫu về',
    'select_file' => 'Chọn file',
    'record_import_null' => 'Không có dữ liệu trong file Import',
    'progress_import' => 'Tiến độ thực hiện import từ file',
    'running_import_file' => 'Đang import...',
    'pause_import_file' => 'Đang tạm dừng import',
    'completed_import_file' => 'Đã hoàn thành import',
    'result_import' => 'Kết quả import từ file',
    'detail_result_pasre_file' => 'Kết quả chi tiết phân tích file',
    'total_record' => 'Tổng số bản ghi',
    'total_record_imported' => 'Tổng số bản ghi đã import',
    'record_imported_success' => 'Số bản ghi đã import thành công',
    'record_imported_fail' => 'Số bản ghi import thất bại',
    'user_not_existed' => 'Số người dùng chưa đăng ký tài khoản',
    'user_already_added' => 'Số người dùng đã là thành viên trước đó',
    'error_response' => 'Lỗi trả về',
    'too_many_record' => 'Vượt quá số lượng cho phép',
    'note' => 'Lưu ý: File không quá 60 hàng',
    'error_parse_data' => [
    	'missing_quotes' => 'Lỗi thiếu dấu ngoặc kép',
    	'undetectable_delimiter' => 'Không xác định được dấu phân tách',
    	'too_few_fields' => 'Quá ít trường',
    	'too_many_fields' => 'Quá nhiều trường',
    ],
    'import' => [
        'add_user_into_group' => 'Nhập danh sách nhân viên',
        'import_goods' => 'Nhập danh sách hàng nhập',
        'import_place' => 'Nhập danh sách khu vực',
        'import_table' => 'Nhập danh sách bàn'
    ],
    'message_error_import' => [
        1 => 'Import thành công',
        2 => 'Email hoặc số điện thoại chưa được đăng ký',
        3 => 'Người dùng đã được thêm trước đó',
        4 => 'Cấu trúc bảng dữ liệu không đúng'
    ],
    'message_accomplished_import' => 'Đã hoàn thành quá trình import file'
];