<?php
return [
    'week' => [
        1 => 'T2',
        2 => 'T3',
        3 => 'T4',
        4 => 'T5',
        5 => 'T6',
        6 => 'T7',
        7 => 'CN',
    ],
    'mongo_week' => [
        1 => 'CN',
        2 => 'T2',
        3 => 'T3',
        4 => 'T4',
        5 => 'T5',
        6 => 'T6',
        7 => 'T7'
    ],
    'weekdays' => [
        'monday' => 'Thứ hai',
        'tuesday' => 'Thứ ba',
        'wednesday' => 'Thứ tư',
        'thursday' => 'Thứ năm',
        'friday' => 'Thứ sáu',
        'saturday' => 'Thứ bảy',
        'sunday' => 'Chủ nhật',
    ]
];