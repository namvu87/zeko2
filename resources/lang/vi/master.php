<?php
return [
    'deleted' => 'Đã xóa',
    'not_delete' => 'Chưa xóa',
    'slider_manager' => [
        'slider' => 'Khung trình chiếu',
        'slider_manager' => 'Quản lý KTC',
        'add_slider' => 'Thêm mới khung trình chiếu',
        'add_slider_success' => 'Đã thêm mới khung trình chiếu thành công',
        'edit_slider' => 'Chỉnh sửa khung trình chiếu',
        'edit_slider_success' => 'Đã sửa đổi khung trình chiếu thành công',
        'delete_slider_confirm' => 'Bạn có chắc chắn muốn xóa khung trình chiếu này không?',
        'delete_slider_success' => 'Đã xóa khung trình chiếu thành công',
        'type' => 'Loại khung trình chiếu',
        'type_show_web' => 'Hiển thị trên web',
        'type_show_mobile' => 'Hiển thị trên mobile'
    ],
    'tag_manager' => [
        'tag' => 'Nhãn',
        'tag_manager' => 'Quản lý nhãn',
        'add_tag' => 'Thêm mới nhãn',
        'add_tag_success' => 'Đã thêm mới nhãn thành công',
        'edit_tag' => 'Chỉnh sửa nhãn',
        'edit_tag_success' => 'Đã sửa đổi nhãn thành công',
        'delete_tag_confirm' => 'Bạn có chắc chắn muốn xóa nhãn này không?',
        'delete_tag_success' => 'Đã xóa nhãn thành công',
        'restore_tag_confirm' => 'Bạn có chắc chắn muốn khôi phục nhãn này không?',
        'restore_tag_success' => 'Đã khôi phục nhãn thành công',
        'error_tag_exists_news' => 'Nhãn đã tồn tại trong bài viết nên không thể xóa'
    ],
    'news_manager' => [
        'news_manager' => 'Quản lý bài viết',
        'add_news' => 'Thêm mới bài viết',
        'add_news_success' => 'Đã thêm mới bài viết thành công',
        'edit_news' => 'Chỉnh sửa bài viết',
        'edit_news_success' => 'Đã sửa đổi bài viết thành công',
        'delete_news_confirm' => 'Bạn có chắc chắn muốn xóa bỏ bài viết này không?',
        'delete_news_success' => 'Đã xóa bài viết thành công',
        'restore_news_confirm' => 'Bạn có chắc chắn muốn khôi phục bài viết này không?',
        'restore_news_success' => 'Đã khôi phục bài viết thành công'
    ],
    'product_manager' => [
        'product'   => 'Sản phẩm',
        'product_manager' => 'Quản lý sản phẩm',
        'add_product' => 'Thêm mới sản phẩm',
        'delete_product_confirm' => 'Bạn có chắc chắn muốn xóa sản phẩm này không?',
        'delete_product_success' => 'Xóa sản phẩm khỏi nhóm thành công',
        'add_product_success' => 'Thêm mới sản phẩm thành công',
        'edit_product_success' => 'Thay đổi thông tin sản phẩm thành công',
    ],
    'customer_care_manager' => [
        'customer_care_manager' => 'Quản lý NV CSKH',
        'add_customer_care' => 'Thêm mới NV CSKH',
        'edit_customer_care' => 'Chỉnh sửa NV CSKH',
        'delete_customer_care_confirm' => 'Bạn có chắc chắn muốn xóa NV CSKH này không?',
        'create_customer_care_success' => 'Thêm mới NV CSKH thành công',
        'delete_customer_care_success' => 'Xóa NV CSKH thành công',
        'update_customer_care_success' => 'Thay đổi thông tin NV CSKH thành công'
    ],
    'contact' => [
        'contact' => 'Thông tin liên hệ',
        'update_contact_success' => 'Thay đổi thông tin liên hệ thành công'
    ],
    'group_manager' => [
        'group' => 'Tổ chức',
        'group_manager' => 'Quản lý tổ chức',
        'edit_group' => 'Chỉnh sửa tổ chức',
        'set_expiration_date_success' => 'Thiết lập ngày hết hạn thành công'
    ],
    'user_guide' => [
        'user_guide' => 'Hướng dẫn sử dụng',
        'add_guide' => 'Thêm mới bài viết',
        'edit_guide' => 'Chỉnh sửa bài viết',
        'delete_guide_confirm' => 'Bạn có chắc chắn muốn xóa bỏ bài viết này không?',
        'restore_guide_confirm' => 'Bạn có chắc chắn muốn khôi phục bài viết này không?',
    ]
];