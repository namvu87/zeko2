<?php
return [
    'word' => [
        'update_cover_image' => 'Cập nhật ảnh bìa',
        'update_avatar'      => 'Cập nhật ảnh đại diện',
    ],
    'profile' => [
        'update_success' => 'Cập nhật thông tin người dùng thành công'
    ],
    'avatar' => [
        'update_success' => 'Cập nhật ảnh đại diện thành công',
        'update_fail' => 'Xảy ra lỗi, vui lòng thử lại'
    ]
];