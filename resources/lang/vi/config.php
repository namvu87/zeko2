<?php
return [
    'language' => [
        0 => 'Tiếng Việt',
        1 => 'English'
    ],
    'product' => [
        'type' => [
            1 => 'Ứng dụng chấm công',
            2 => 'Ứng dụng nhà hàng'
        ],
        'price_type' => [
            11 => 'Loại giá cố định theo số lượng nhân viên tối đa (VD: 70k/max 50 người/tháng)',
            12 => 'Loại giá phụ thuộc số lượng nhân viên (VD: 30k/20 người/tháng)'
        ]
    ],
    'status' => [
        0 => 'unvisible',
        1 => 'visible'
    ],
    'import_file' => [
        'import_user_into_group' => [
            'example_csv' => 'sample_files/vi/MauFileThemNhanVien.csv',
            'example_xlsx' => 'sample_files/vi/MauFileThemNhanVien.xlsx',
            'api' => 'import/user-into-group',
            'file_header' => [
                0 => 'STT',
                1 => 'Họ và Tên',
                2 => 'Điện thoại',
                3 => 'Email'
            ],
            'data_field' => [
                0 => 'sort',
                1 => 'full_name',
                2 => 'phone_number',
                3 => 'email'
            ]
        ],
        'import_goods' => [
            'example_csv' => 'sample_files/vi/MauFileHangNhap.csv',
            'example_xlsx' => 'sample_files/vi/MauFileHangNhap.xlsx',
            'api' => 'goods/store/import',
            'data_field' => [
                0 => 'sort',
                1 => 'code',
                2 => 'name',
                3 => 'group_menu_name',
                4 => 'price',
                5 => 'price_origin',
                6 => 'unit',
                7 => 'inventory_min',
                8 => 'inventory_max',
                9 => 'is_sale',
                10 => 'description',
            ]
        ],
        'import_place' => [
            'example_csv' => 'sample_files/vi/MauFileKhuVuc.csv',
            'example_xlsx' => 'sample_files/vi/MauFileKhuVuc.xlsx',
            'api' => 'place/store/import',
            'file_header' => [
                0 => 'STT',
                1 => 'Tên khu vực/phòng',
                2 => 'Mô tả'
            ],
            'data_field' => [
                0 => 'sort',
                1 => 'name',
                2 => 'description'
            ]
        ],
        'import_table' => [
            'example_csv' => 'sample_files/vi/MauFileBan.csv',
            'example_xlsx' => 'sample_files/vi/MauFileBan.xlsx',
            'api' => 'table/store/import',
            'data_field' => [
                0 => 'sort',
                1 => 'name',
                2 => 'count_seat',
                3 => 'place_name',
                4 => 'description',
            ]
        ]
    ],
    'goods' => [
        'units' => [
            'bowl' => 'Tô',
            'dish' => 'Đĩa',
            'glass' => 'Ly',
            'package' => 'Gói',
            'cup' => 'Cốc',
            'bottle' => 'Chai',
            'box' => 'Hộp',
            'can' => 'Lon',
            'kg' => 'Kg',
            'lit' => 'Lít',
            'pot' => 'Nồi',
        ],
    ],
    'mini_options_period_time' => [
        'today' => 'Hôm nay',
        'yesterday' => 'Hôm qua',
        'this_week' => 'Tuần này',
        'last_week' => 'Tuần trước',
        'last_seven_days' => '7 ngày gần nhất',
        'this_month' => 'Tháng này',
        'last_month' => ' Tháng trước'
    ],
    'options_period_time' => [
        [
            'Theo ngày và tuần',
            [
                'today' => 'Hôm nay',
                'yesterday' => 'Hôm qua',
                'this_week' => 'Tuần này',
                'last_week' => 'Tuần trước',
                'last_seven_days' => '7 ngày qua'
            ]
        ],
        [
            'Theo tháng',
            [
                'this_month' => 'Tháng này',
                'last_month' => ' Tháng trước',
                'last_thirty_days' => '30 ngày qua'
            ]
        ],
        [
            'Theo quý và năm',
            [
                'this_quarter' => 'Quý này',
                'last_quarter' => 'Quý trước',
                'this_year' => 'Năm nay',
                'last_year' => 'Năm trước'
            ]
        ]
    ],
    'day_of_week' => ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    'month_names' => ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8',
        'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'
    ],
    'invoice_form' => [
        'variables' => [
            'common' => [
                'So_Thu_Tu' => 'Số thứ tự của giá trị trong bảng'
            ],
            1 => [ //sale-invoice
                'out_table' => [
                    'Ma_Hoa_Don' => 'Mã hóa đơn',
                    'Ten_Nha_Hang' => 'Tên nhà hàng',
                    'Dia_Chi_Nha_Hang' => 'Địa chỉ nhà hàng',
                    'Ngay_Ban' => 'Ngày tạo hóa đơn',
                    'Danh_Sach_Ban' => 'Danh sách bàn ăn',
                    'Nhan_Vien_Ban_Hang' => 'Nhân viên phụ trách hóa đơn',
                    'Tong_Tien' => 'Tổng tiền chưa tính giảm giá và VAT',
                    'Giam_Gia_Don_Hang' => 'Giảm giá tổng hóa đơn',
                    'Tong_Tien_Phai_Thanh_Toan' => 'Tổng số tiền khách phải trả',
                    'Ghi_Chu' => 'Ghi chú về thực đơn'
                ],
                'in_table' => [
                    'Ma_Hang' => 'Mã món đã gọi',
                    'Ten_Hang' => 'Tên món đã gọi',
                    'So_Luong' => 'Số lượng món đã gọi',
                    'Don_Gia' => 'Đơn giá của món',
                    'Giam_Gia' => 'Giảm giá của món ',
                    'Gia_Sau_Khi_Giam' => 'Đơn giá của món sau khi giảm giá',
                    'Thanh_Tien' => 'Tổng tiền của món',
                ],
                'other' => [
                    'Don_Gia_Chiet_Khau' => 'Đơn giá của món trước và sau khi chiết khấu',
                ],
                'loop' => 'Goods',
            ],
            2 => [ //import invoice
                'out_table' => [
                    'Ma_Phieu' => 'Mã phiếu nhập',
                    'Nhan_Vien_Phu_Trach' => 'Tên nhân viên phụ trách',
                    'Ngay_Tao' => 'Ngày tạo phiếu',
                    'Tong_Tien' => 'Tiền cần thanh toán cho nhà cung cấp',
                    'Ghi_Chu' => 'Ghi chú'
                ],
                'in_table' => [
                    'Ma_Hang' => 'Mã hàng hóa nhập',
                    'Ten_Hang' => 'Tên hàng hóa nhập',
                    'So_Luong' => 'Số lượng hàng hóa nhập',
                    'Don_Gia' => 'Đơn giá hàng hóa nhập',
                    'Khuyen_Mai' => 'Giảm giá một mặt hàng',
                    'Thanh_Tien' => 'Tổng tiền loại hàng hóa nhập',
                ],
                'other' => [],
                'loop' => 'Goods',
            ],
            3 => [ //returned-invoice
                'out_table' => [
                    'Ma_Hoa_Don' => 'Mã hóa đơn trả hàng',
                    'Ma_Hoa_Don_Ban_Hang' => 'Mã hóa đơn bán hàng',
                    'Ten_Nha_Hang' => 'Tên nhà hàng',
                    'Dia_Chi_Nha_Hang' => 'Địa chỉ nhà hàng',
                    'Ngay_Ban' => 'Ngày tạo hóa đơn',
                    'Nhan_Vien_Ban_Hang' => 'Nhân viên tạo hóa đơn',
                    'Tong_Tien' => 'Tổng giá trị hóa đơn trả',
                    'Ghi_Chu' => 'Ghi chú về hóa đơn'
                ],
                'in_table' => [
                    'Ma_Hang' => 'Mã món trả',
                    'Ten_Hang' => 'Tên món trả',
                    'So_Luong' => 'Số lượng món trả',
                    'Don_Gia' => 'Đơn giá của món',
                    'Thanh_Tien' => 'Tổng tiền của món',
                ],
                'other' => [],
                'loop' => 'Goods',
            ],
        ],
    ],
];