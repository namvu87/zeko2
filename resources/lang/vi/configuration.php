<?php
return [
    'save_config' => 'Lưu thay đổi',
    'general' => 'Chung',
    'customer_care' => 'Chăm sóc khách hàng',
    'contact_information' => 'Thông tin liên hệ',
    'customer' => 'Khách hàng',
    'saveConfig' => [
        'success' => 'Bạn đã lưu cấu hình thành công',
    ],
    'customerCare' => [
        'name' => 'Tên nhân viên',
        'phone_number' => 'Số điện thoại',
        'zalo' => 'Zalo',
        'skype' => 'Skype',
        'email' => 'Email',
    ],
    'contactInformation' => [
        'address' => 'Địa chỉ',
        'phone_number' => 'Số điện thoại',
        'fax' => 'Fax',
        'fb' => 'Trang Facebook',
    ],
    'category' => [
        1 => [
            'name' => 'Đăng ký dịch vụ',
            'icon' => 'fa fa-plus-circle'
        ],
        2 => [
            'name' => 'Quản lý nhân viên',
            'icon' => 'fa fa-users'
        ],
        3 => [
            'name' => 'Phân quyền người dùng',
            'icon' => 'fa fa-user-shield'
        ],
        4 => [
            'name' => 'Tổng quan',
            'icon' => 'fa fa-chart-bar'
        ],
        5 => [
            'name' => 'Quản lý ca làm việc',
            'icon' => 'fa fa-calendar-alt'
        ],
        6 => [
            'name' => 'Quản lý chấm công',
            'icon' => 'fa fa-user-clock'
        ],
        7 => [
            'name' => 'Quản lý khu vực/bàn',
            'icon' => 'fa fa-table'
        ],
        8 => [
            'name' => 'Danh mục hàng hóa',
            'icon' => 'fa fa-cube'
        ],
        9 => [
            'name' => 'Quản lý hóa đơn',
            'icon' => 'fa fa-file-invoice'
        ],
        10 => [
            'name' => 'Quản lý mẫu in',
            'icon' => 'fa fa-file'
        ],
        11 => [
            'name' => 'Bán hàng',
            'icon' => 'fa fa-shopping-basket'
        ],
        12 => [
            'name' => 'Báo cáo thống kê',
            'icon' => 'fa fa-chart-area'
        ],
        13 => [
            'name' => 'App chấm công',
            'icon' => 'fa fa-mobile'
        ],
        14 => [
            'name' => 'App bán hàng',
            'icon' => 'fa fa-mobile-alt'
        ],
        15 => [
            'name' => 'App quản lý bán hàng',
            'icon' => 'fa fa-tablet'
        ],
        16 => [
            'name' => 'Danh mục khác',
            'icon' => 'fa fa-sitemap'
        ],
    ],
    'service' => [
        1 => 'Chấm công',
        2 => 'Nhà hàng'
    ],
    'news_type' => [
        1 => 'Bài viết thường',
        2 => 'Giới thiệu về công ty',
        3 => 'Trang chủ',
        4 => 'Điều khoản sử dụng',
        5 => 'Tuyển dụng',
        6 => 'Chính sách công ty',
        7 => 'Về công ty',
        8 => 'Điều khoản',
        9 => 'Phí dịch vụ'
    ],
];