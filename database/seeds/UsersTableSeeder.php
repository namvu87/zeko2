<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    private $firstNameFemale;
    private $firstNameMale;
    private $lastNameFemale;
    private $lastNameMale;
    private $address;

    public function __construct()
    {
        $this->firstNameFemale = [
            'Anh', 'Linh', 'Huyền', 'Trang', 'Yến', 'Nhi', 'Thư', 'Hiền', 'Quyên', 'Thanh', 'Mai', 'Lan', 'Hoa',
            'Đào', 'Thảo', 'Hà', 'Hường', 'Ánh', 'Thu', 'Thủy', 'Chúc', 'Như', 'Nhung', 'Vy', 'Vân Anh'
        ];
        $this->firstNameMale = [
            'Bình', 'Hoàng', 'Hoàn', 'Cường', 'Dũng', 'Đức', 'Hải', 'Khải', 'Minh', 'Nam', 'Quyết', 'Phú', 'Phương',
            'Thảo', 'Thành', 'Trọng', 'Thiết', 'Quang', 'Vũ', 'Hiếu'
        ];
        $this->lastNameFemale = ['Nguyễn Thị', 'Lê Thị', 'Hoàng', 'Phan Thị', 'Phạm Thị'];
        $this->lastNameMale = [
            'Nguyễn Đình', 'Nguyễn Văn', 'Nguyễn Đức', 'Hoàng Văn', 'Hoàng Anh', 'Lê Đình', 'Lê Văn', 'Lê Đức', 'Lê',
            'Phan Văn', 'Phạm Văn', 'Phan Đức', 'Nguyễn Tất', 'Võ Nguyên', 'Nguyễn Chí'
        ];
        $this->address = [
            'Lào Cai', 'Yên Bái', 'Điện Biên', 'Hòa Bình', 'Lai Châu', 'Sơn La', 'Hà Giang', 'Cao Bằng', 'Bắc Kạn',
            'Lạng Sơn', 'Tuyên Quang', 'Thái Nguyên', 'Phú Thọ', 'Bắc Giang', 'Quảng Ninh', 'Bắc Ninh', 'Hà Nam',
            'Hà Nội', 'Hải Dương', 'Hưng Yên', 'Hải Phòng', 'Nam Định', 'Ninh Bình', 'Thái Bình', 'Vĩnh Phúc',
            'Thanh Hoá', 'Nghệ An', 'Hà Tĩnh', 'Quảng Bình', 'Quảng Trị', 'Thừa Thiên-Huế'
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = factory(App\Models\Area::class)->create();
        $commune = factory(App\Models\Commune::class)->create([
            'area_id' => $area->id
        ]);
//        factory(App\Models\User::class)->create([
//            'first_name' => 'Văn Dũng',
//            'last_name' => 'Pham',
//            'phone_number' => '0978958705',
//            'email' => 'phamvandung1508@gmail.com',
//            'password' => bcrypt('123456'),
//            'area' => [
//                'id' => $area->id,
//                'name' => $area->name
//            ],
//            'commune' => [
//                'id' => $commune->id,
//                'name' => $commune->name
//            ]
//        ]);
        for ($i = 0; $i < 500; $i++) {
            try {
                $firstName = array_random($this->firstNameMale);
                $lastName = array_random($this->lastNameMale);
                \App\Models\User::create([
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'phone_number' => '0339' . str_pad(rand(1, 999999), '6', '0', STR_PAD_LEFT),
                    'email' => str_slug($lastName . ' ' . $firstName, '') . '0339@gmail.com',
                    'password' => bcrypt('123456'),
                    'area' => [
                        'id' => $area->id,
                        'name' => $area->name
                    ],
                    'commune' => [
                        'id' => $commune->id,
                        'name' => $commune->name
                    ]
                ]);
                $firstName = array_random($this->firstNameFemale);
                $lastName = array_random($this->lastNameFemale);
                \App\Models\User::create([
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'phone_number' => '0339' . str_pad(rand(1, 999999), '6', '0', STR_PAD_LEFT),
                    'email' => str_slug($lastName . ' ' . $firstName, '') . '0339@gmail.com',
                    'password' => bcrypt('123456'),
                    'area' => [
                        'id' => $area->id,
                        'name' => $area->name
                    ],
                    'commune' => [
                        'id' => $commune->id,
                        'name' => $commune->name
                    ]
                ]);
            } catch (\Exception $exception) {
                continue;
            }
        }
    }
}
