<?php

use Illuminate\Database\Seeder;
use App\Models\Notification;
use App\Models\User;
use App\Models\Shift;
use App\Models\Group;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('phone_number', '0978958705')->first();

        $shift = Shift::where('_id', $user->shift_ids[0] ?? null)->first();
        $group = Group::where('_id', $user->group_ids[0] ?? null)->first();

        $creator = User::latest()->first();

        if ($group) {
            Notification::create([
                'user_id' => $user->id,
                'creator_id' => $creator->id,
                'type' => Notification::TYPE_10,
                'group_id' => $group->id
            ]);
            Notification::create([
                'user_id' => $user->id,
                'creator_id' => $creator->id,
                'type' => Notification::TYPE_11,
                'group_id' => $group->id
            ]);

            Notification::create([
                'user_id' => $user->id,
                'creator_id' => $creator->id,
                'type' => Notification::TYPE_12,
                'group_id' => $group->id
            ]);

            Notification::create([
                'user_id' => $user->id,
                'creator_id' => $creator->id,
                'type' => Notification::TYPE_13,
                'group_id' => $group->id
            ]);
        }

        if ($shift) {
            Notification::create([
                'user_id' => $user->id,
                'creator_id' => $creator->id,
                'type' => Notification::TYPE_14,
                'shift_id' => $shift->id,
                'group_id' => $shift->group->id
            ]);

            Notification::create([
                'user_id' => $user->id,
                'creator_id' => $creator->id,
                'type' => Notification::TYPE_15,
                'shift_id' => $shift->id,
                'group_id' => $shift->group->id
            ]);

            Notification::create([
                'user_id' => $user->id,
                'creator_id' => $creator->id,
                'type' => Notification::TYPE_16,
                'shift_id' => $shift->id,
                'group_id' => $shift->group->id
            ]);

            for ($i = 0; $i < 100; $i ++) {
                Notification::create([
                    'user_id' => $user->id,
                    'creator_id' => $creator->id,
                    'type' => Notification::TYPE_17,
                    'shift_id' => $shift->id,
                    'group_id' => $shift->group->id
                ]);
                Notification::create([
                    'user_id' => $user->id,
                    'creator_id' => $creator->id,
                    'type' => Notification::TYPE_18,
                    'shift_id' => $shift->id,
                    'group_id' => $shift->group->id
                ]);
                Notification::create([
                    'user_id' => $user->id,
                    'creator_id' => $creator->id,
                    'type' => Notification::TYPE_19,
                    'shift_id' => $shift->id,
                    'group_id' => $shift->group->id
                ]);
                Notification::create([
                    'user_id' => $user->id,
                    'creator_id' => $creator->id,
                    'type' => Notification::TYPE_20,
                    'shift_id' => $shift->id,
                    'group_id' => $shift->group->id
                ]);
                Notification::create([
                    'user_id' => $user->id,
                    'creator_id' => $creator->id,
                    'type' => Notification::TYPE_21,
                    'shift_id' => $shift->id,
                    'group_id' => $shift->group->id
                ]);
                Notification::create([
                    'user_id' => $user->id,
                    'creator_id' => $creator->id,
                    'type' => Notification::TYPE_22,
                    'shift_id' => $shift->id,
                    'group_id' => $shift->group->id
                ]);
            }
        }
    }
}
