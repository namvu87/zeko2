<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('phone_number', '0978958705')->first();
        factory(App\Models\Group::class, 15)->create([
            'owner_id' => $user->id
        ]);
    }
}
