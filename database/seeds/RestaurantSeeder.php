<?php

use Illuminate\Database\Seeder;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = factory(App\Models\Area::class)->create();
        $commune = factory(App\Models\Commune::class)->create([
            'area_id' => $area->id
        ]);
        $user = App\Models\User::where('phone_number', '0978958705')
                               ->orWhere('email', 'phamvandung1508@gmail.com')
                               ->first();
        // Nhóm Nhà hàng
        $group = factory(App\Models\Group::class)->create([
            'area' => [
                'id' => $area->id,
                'name' => $area->name,
            ],
            'commune' => [
                'id' => $commune->id,
                'name' => $commune->name
            ],
            'owner_id' => $user->id,
            'service_type' => 2
        ]);
        $user->push('group_ids', $group->id);
//
//        factory(App\Models\User::class, 100)->create([
//            'group_ids' => [$group->id]
//        ]);

        $places = factory(App\Models\Place::class, 15)->create([
            'group_id' => $group->id
        ]);
        foreach ($places as $place) {
            factory(App\Models\Table::class, 25)->create([
                'group_id' => $group->id,
                'place_id' => $place->id
            ]);
        }
        $groupMenu = factory(App\Models\GroupMenu::class)->create([
            'group_id' => $group->id
        ]);
        $goods = factory(App\Models\Good::class, 100)->create([
            'group_menu_ids' => [$groupMenu->id],
            'group_id' => $group->id
        ]);
    }
}
