<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class TimekeepingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = factory(App\Models\Area::class)->create();
        $commune = factory(App\Models\Commune::class)->create([
            'area_id' => $area->id
        ]);

        $user = App\Models\User::where('phone_number', '0978958705')
                               ->orWhere('email', 'phamvandung1508@gmail.com')
                               ->first();

        // Nhóm chấm công
        $group = factory(App\Models\Group::class)->create([
            'area' => [
                'id' => $area->id,
                'name' => $area->name,
            ],
            'commune' => [
                'id' => $commune->id,
                'name' => $commune->name
            ],
            'owner_id' => $user->id,
            'service_type' => 1
        ]);

        // Thêm người dùng vào nhóm
        $user->push('group_ids', $group->id);

        // Fake ca làm việc
        $shift = factory(App\Models\Shift::class)->create([
            'group_id' => $group->id
        ]);

        // Thêm người dùng vào ca làm việc
        $user->push('shift_ids', $shift->id);

        $period = CarbonPeriod::create('2019-03-10', Carbon::now()->format('Y-m-d'));

        foreach ($period->toArray() as $date) {
            factory(App\Models\Timekeeping::class)->create([
                'user_id' => $user->id,
                'group_id' => $group->id,
                'shift_name' => $shift->name,
                'shift_type' => $shift->type ?? 1,
                'date' => $date->format('Y-m-d')
            ]);
        }

        // Fake nhân viên trong nhóm
        factory(App\Models\User::class, 100)->create([
            'group_ids' => [$group->id]
        ]);
    }
}
