<?php

use Faker\Generator as Faker;
use App\Models\Area;
use App\Models\Commune;

$factory->define(Commune::class, function (Faker $faker) {
    return [
        'name' => $faker->streetName,
        'area_id' => factory(Area::class)->create()->id
    ];
});
