<?php

use Faker\Generator as Faker;
use App\Models\Group;
use App\Models\Place;
use App\Models\Table;

$factory->define(Table::class, function (Faker $faker) {
    return [
        'name'        => $faker->name,
        'status'      => $faker->numberBetween(0, 3),
        'description' => $faker->text(60),
        'count_seat'  => $faker->numberBetween(4, 8),
        'place_id'    => factory(Place::class)->create()->id,
        'group_id'    => factory(Group::class)->create()->id
    ];
});
