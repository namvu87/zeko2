<?php

use Faker\Generator as Faker;
use App\Models\Shift;
use App\Models\Group;

$factory->define(Shift::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'type' => 1,
        'schedules' => [
            'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'
        ],
        'allow_time' => 15,
        'times' => [
            [
                'checkin' => '08:00',
                'checkout' => '17:00'
            ]
        ],
        'group_id' => factory(Group::class)->create()->id
    ];
});
