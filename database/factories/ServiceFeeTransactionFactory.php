<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ServiceFeeTransaction;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(ServiceFeeTransaction::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'amount' => $faker->numberBetween(10000, 1000000),
        'status' => ServiceFeeTransaction::INITED_STATUS,
        'service_fess' => []
    ];
});
