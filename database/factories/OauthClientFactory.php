<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use DesignMyNight\Mongodb\Passport\Client;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'user_id' => null,
        'name' => $faker->name,
        'secret' => $faker->regexify('[A-Za-z0-9]{40}'),
        'redirect' => 'http://localhost',
        'personal_access_client' => false,
        'password_client' => true,
        'revoked' => false
    ];
});
