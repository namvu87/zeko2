<?php

use Faker\Generator as Faker;
use App\Models\Place;

$factory->define(Place::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(60)
    ];
});
