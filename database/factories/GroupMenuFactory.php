<?php

use Faker\Generator as Faker;
use App\Models\GroupMenu;

$factory->define(GroupMenu::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(200),
        'level' => GroupMenu::LEVER_0
    ];
});
