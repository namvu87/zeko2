<?php

use Faker\Generator as Faker;
use App\Models\Group;
use App\Models\Good;
use App\Models\GroupMenu;

$factory->define(Good::class, function (Faker $faker) {
    return [
        'code'          => uniqid(),
        'name'          => $faker->name,
        'price'         => $faker->numberBetween(10000, 100000),
        'price_origin'  => $faker->numberBetween(10000, 100000),
        'unit'          => $faker->name,
        'discount'      => $faker->numberBetween(1000, 20000),
        'discount_type' => 1,
        'type'          => 1,
        'is_sale'       => true,
        'status'        => 1,
        'properties'    => [],
        'inventory_min' => $faker->numberBetween(0, 10),
        'inventory_max' => $faker->numberBetween(20, 1000),
        'description'   => $faker->text(400),
        'group_id'      => factory(Group::class)->create()->id,
        'group_menu_ids' => [factory(GroupMenu::class)->create()->id]
    ];
});
