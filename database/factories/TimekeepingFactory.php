<?php

use Faker\Generator as Faker;
use App\Models\Timekeeping;
use App\Models\Group;
use App\Models\User;

$factory->define(Timekeeping::class, function (Faker $faker) {
    return [
        'date' => date('Y-m-d'),
        'target_time' => [
            [
                'checkin' => '07:30',
                'checkout' => '17:00'
            ]
        ],
        'times' => [
            [
                'checkin' => [
                    'time' => '07:30',
                    'address' => $faker->address
                ],
                'checkout' => [
                    'time' => '17:00',
                    'address' => $faker->address
                ]
            ]
        ],
        'user_id' => factory(User::class)->create()->id,
        'group_id' => factory(Group::class)->create()->id
    ];
});
