<?php

use Illuminate\Database\Eloquent\FactoryBuilder;
use Faker\Generator as Faker;
use App\Models\Group;

$factory->define(Group::class, function (Faker $faker) {
    return [
        'name' => $faker->catchPhrase,
        'address' => $faker->address,
        'service_type' => $faker->numberBetween(1, 3),
        'commune' => [
            'id' => '123',
            'name' => 'commune example'
        ],
        'area' => [
            'id' => '123',
            'name' => 'Area example'
        ]
    ];
});
$factory->state(Group::class, 'timekeeping', [
    'service_type' => Group::TIMEKEEPING_SERVICE
]);
$factory->state(Group::class, 'restaurant', [
    'service_type' => Group::RESTAURANT_SERVICE
]);
FactoryBuilder::macro('withoutEvents', function () {
    $this->class::flushEventListeners();
    return $this;
});
