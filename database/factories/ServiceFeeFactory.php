<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ServiceFee;
use App\Models\Group;
use Faker\Generator as Faker;

$factory->define(ServiceFee::class, function (Faker $faker) {
    return [
        'group_id' => factory(Group::class)->create()->id,
        'aggregated_month' => date('Y-m'),
        'status' => ServiceFee::UNPAID_STATUS,
        'invoices_count' => $faker->numberBetween(0, 10000),
        'revenue' => $faker->numberBetween(0, 100000000),
        'users_count' => $faker->numberBetween(0, 100)
    ];
});
