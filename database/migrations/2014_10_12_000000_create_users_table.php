<?php

use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $collection) {
            $collection->sparse_and_unique('phone_number');
            $collection->sparse_and_unique('email');
            $collection->index([
                    'first_name' => 'text',
                    'last_name' => 'text',
                    'address' => 'text',
                    'area' => 'text',
                    'commune' => 'text',
                    'email' => 'text',
                    'phone_number' => 'text'
                ],
                'company_full_text'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
