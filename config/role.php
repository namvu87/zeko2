<?php
return [
    'timekeeping_service' => [
        'permissions' => [
            'shift' => [
                'list',
                'create',
                'update',
                'delete',
                'user-list',
                'user-add',
                'user-remove'
            ],
            'employee' => [
                'list',
                'add',
                'remove'
            ],
            'timekeeping' => [
                'list',
                'detail'
            ],
            'report' => [
                'timekeeping-group',
                'timekeeping-employee'
            ],
            'requirement' => [
                'list',
                'handle'
            ],
            'permission' => [
                'role',
                'role-permission',
                'role-assign',
                'role-revoke',
                'update-role-permission',
            ],
            'group' => [
                'update-profile',
                'change-service'
            ]
        ],
        'roles' => [
            'owner' => [
                '.shift.list', '.shift.create', '.shift.update', '.shift.delete', '.shift.user-list', '.shift.user-add', '.shift.user-remove',
                '.employee.list', '.employee.add', '.employee.remove', '.timekeeping.list', '.timekeeping.detail',
                '.report.timekeeping-group', '.report.timekeeping-employee', '.requirement.list', '.requirement.handle',
                '.group.update-profile', '.group.change-service',
                '.permission.role', '.permission.role-permission', '.permission.role-assign', '.permission.role-revoke', '.permission.update-role-permission'
            ],
            'admin' => [
                '.shift.list', '.shift.create', '.shift.update', '.shift.delete', '.shift.user-list', '.shift.user-add', '.shift.user-remove',
                '.employee.list', '.employee.add', '.employee.remove', '.timekeeping.list', '.timekeeping.detail',
                '.report.timekeeping-group', '.report.timekeeping-employee', '.requirement.list', '.requirement.handle',
                '.permission.role', '.permission.role-permission', '.permission.role-assign', '.permission.role-revoke', '.permission.update-role-permission'
            ],
            'hrm' => [
                '.employee.list', '.employee.add', '.employee.remove'
            ]
        ]
    ],
    'restaurant_service' => [
        'permissions' => [
            'employee' => [
                'list',
                'add',
                'remove'
            ],
            'permission' => [
                'role',
                'role-permission',
                'role-assign',
                'role-revoke',
                'update-role-permission',
            ],
            'place' => [
                'list',
                'create',
                'update',
                'delete'
            ],
            'table' => [
                'list',
                'create',
                'update',
                'delete'
            ],
            'group-menus' => [
                'list',
                'create',
                'update',
                'delete'
            ],
            'goods' => [
                'list',
                'create',
                'update',
                'delete'
            ],
            'form-invoice' => [
                'list',
                'create',
                'update',
                'delete'
            ],
            'invoice' => [
                'list',
                'selling-list',
                'create',
                'detail',
                'update',
                'delete',
                'disable',
                'checkout'
            ],
            'imported-invoice' => [
                'list',
                'create',
                'detail',
                'disable'
            ],
            'returned-invoice' => [
                'list',
                'create',
                'detail',
                'disable'
            ],
            'report' => [
                'today',
                'sale',
                'goods'
            ],
            'group' => [
                'update-profile',
                'change-service'
            ]
        ],
        'roles' => [
            'owner' => [
                '.employee.list', '.employee.add', '.employee.remove',
                '.place.list', '.place.create', '.place.update', '.place.delete',
                '.table.list', '.table.create', '.table.update', '.table.delete',
                '.group-menus.list', '.group-menus.create', '.group-menus.update', '.group-menus.delete',
                '.goods.list', '.goods.create', '.goods.update', '.goods.delete',
                '.form-invoice.list', '.form-invoice.create', '.form-invoice.update', '.form-invoice.delete',
                '.invoice.list', '.invoice.selling-list', '.invoice.detail', '.invoice.create', '.invoice.update', '.invoice.delete', '.invoice.disable', '.invoice.checkout',
                '.imported-invoice.list', '.imported-invoice.create', '.imported-invoice.detail', '.imported-invoice.disable',
                '.returned-invoice.list', '.returned-invoice.create', '.returned-invoice.detail', '.returned-invoice.disable',
                '.report.today', '.report.sale', '.report.goods',
                '.group.update-profile', '.group.change-service',
                '.permission.role', '.permission.role-permission', '.permission.role-assign', '.permission.role-revoke', '.permission.update-role-permission'
            ],
            'admin' => [
                '.employee.list', '.employee.add', '.employee.remove',
                '.place.list', '.place.create', '.place.update', '.place.delete',
                '.table.list', '.table.create', '.table.update', '.table.delete',
                '.group-menus.list', '.group-menus.create', '.group-menus.update', '.group-menus.delete',
                '.goods.list', '.goods.create', '.goods.update', '.goods.delete',
                '.form-invoice.list', '.form-invoice.create', '.form-invoice.update', '.form-invoice.delete',
                '.invoice.list', '.invoice.selling-list', '.invoice.detail', '.invoice.create', '.invoice.update', '.invoice.delete', '.invoice.disable', '.invoice.checkout',
                '.imported-invoice.list', '.imported-invoice.create', '.imported-invoice.detail', '.imported-invoice.disable',
                '.returned-invoice.list', '.returned-invoice.create', '.returned-invoice.detail', '.returned-invoice.disable',
                '.report.today', '.report.sale', '.report.goods',
                '.permission.role', '.permission.role-permission', '.permission.role-assign', '.permission.role-revoke', '.permission.update-role-permission'
            ],
            'cashier' => [
                '.place.list', '.table.list', '.goods.list', '.group-menus.list',
                '.invoice.list', '.invoice.create', '.invoice.update', '.invoice.delete', '.invoice.checkout',
                '.returned-invoice.create', '.form-invoice.list',
            ],
            'waiters' => [
                '.place.list', '.table.list', '.goods.list', '.group-menus.list', '.invoice.selling-list',
                '.invoice.create', '.invoice.update', '.invoice.delete'
            ],
            'chef' => [
                '.invoice.selling-list', '.invoice.update', '.goods.list', '.group-menus.list'
            ]
        ]
    ]
];
