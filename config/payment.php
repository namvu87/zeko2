<?php
return [
    'nganluong' => [
        'URL_WS' => 'https://www.nganluong.vn/micro_checkout_api.php?wsdl',
        'RECEIVER' => 'phamvandung1508@gmail.com',
        'MERCHANT_ID' => '',
        'MERCHANT_PASS' => '554f3edddd98f5b9f44c257eeccbfc00',
    ],
    'baokim' => [
        'url' => 'https://www.baokim.vn/payment/order/version11?',
        'pass' => '75893b6ab2416f4c',
        'merchant_id' => '33785',
        'business' => 'phamvandung1508@gmail.com',
    ],
    'merchant' => [
        'vietcombank' => [
            'id' => '0451000422009',
            'owner_name' => 'Phạm Văn Dũng'
        ]
    ],
    'momo' => [
        'payment_url' => env('MOMO_PAYMENT_URL'),
        'partner_code' => env('MOMO_PARTNER_CODE'),
        'partner_name' => env('MOMO_PARTNER_NAME', 'Công ty TNHH Công nghệ Zeko Việt Nam'),
        'access_key' => env('MOMO_ACCESS_KEY'),
        'secret_key' => env('MOMO_SECRET_KEY'),
        'app_payment_url' => env('MOMO_APP_PAYMENT_URL'),
        'app_public_key' => env('MOMO_APP_PUBLIC_KEY'),
        'app_version' => 2,
        'app_pay_type' => 3,
        'app_confirm_url' => env('MOMO_APP_CONFIRM_URL')
    ]
];
