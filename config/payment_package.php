<?php
return [
    'timekeeping'  => [
        'fee'       => 100000,
        'max_staff' => 50
    ],
    'restaurant'   => [
        1 => [
            'fee' => 150,
        ],
        2 => [
            'fee' => 0.15,
        ],
        3 => [
            'fee' => 250000,
        ],
    ],
    'max_used_day' => env('MAX_USED_DAY', 15),
    'out_of_date'  => env('DATE_OUT_OF_PAY', 10)
];
