<?php

return [

    // The prefix for routes
    'prefix' => 'graphql',

    // The routes to make GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Route
    //
    // Example:
    //
    // Same route for both query and mutation
    //
    // 'routes' => 'path/to/query/{graphql_schema?}',
    //
    // or define each route
    //
    // 'routes' => [
    //     'query' => 'query/{graphql_schema?}',
    //     'mutation' => 'mutation/{graphql_schema?}',
    // ]
    //
    'routes' => '{graphql_schema?}',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\Rebing\GraphQL\GraphQLController@query',
    //     'mutation' => '\Rebing\GraphQL\GraphQLController@mutation'
    // ]
    //
    'controllers' => \Rebing\GraphQL\GraphQLController::class . '@query',

    // Any middleware for the graphql route group
    'middleware' => [],

    // Additional route group attributes
    //
    // Example:
    //
    // 'route_group_attributes' => ['guard' => 'api']
    //
    'route_group_attributes' => [],

    // The name of the default schema used when no argument is provided
    // to GraphQL::schema() or when the route is used without the graphql_schema
    // parameter.
    'default_schema' => 'default',

    // The schemas for query and/or mutation. It expects an array of schemas to provide
    // both the 'query' fields and the 'mutation' fields.
    //
    // You can also provide a middleware that will only apply to the given schema
    //
    'schemas' => [
        'default' => [
            'query' => [
                'version' => App\GraphQL\Queries\VersionQuery::class,
                'group' => App\GraphQL\Queries\GroupQuery::class,
                'groups' => App\GraphQL\Queries\GroupsQuery::class,
                'areas' => App\GraphQL\Queries\AreasQuery::class,
                'communes' => App\GraphQL\Queries\CommunesQuery::class,
                'roles' => App\GraphQL\Queries\RolesQuery::class,
                'permissions' => App\GraphQL\Queries\PermissionsQuery::class,
                'shifts' => App\GraphQL\Queries\ShiftsQuery::class,
                'shift' => App\GraphQL\Queries\ShiftQuery::class,
                'shift_user_in_group' => App\GraphQL\Queries\Timekeeping\ShiftOfUserInGroup::class,
                'timekeepings' => App\GraphQL\Queries\TimekeepingsQuery::class,
                'timekeeping' => App\GraphQL\Queries\TimekeepingQuery::class,
                'place' => App\GraphQL\Queries\PlaceQuery::class,
                'places' => App\GraphQL\Queries\PlacesQuery::class,
                'table' => App\GraphQL\Queries\TableQuery::class,
                'tables' => App\GraphQL\Queries\TablesQuery::class,
                'group_menus' => App\GraphQL\Queries\GroupMenusQuery::class,
                'group_menu' => App\GraphQL\Queries\GroupMenuQuery::class,
                'good' => App\GraphQL\Queries\Good\GoodQuery::class,
                'goods' => App\GraphQL\Queries\Good\GoodsQuery::class,
                'code_good' => App\GraphQL\Queries\Good\CodeQuery::class,
                'goods_properties' => App\GraphQL\Queries\Good\PropertiesQuery::class,
                'goods_units' => App\GraphQL\Queries\Good\UnitsQuery::class,
                'menu' => App\GraphQL\Queries\Sale\MenuQuery::class,
                'invoices' => App\GraphQL\Queries\InvoicesQuery::class,
                'invoice' => App\GraphQL\Queries\InvoiceQuery::class,
                'sale_places' => App\GraphQL\Queries\Sale\PlacesQuery::class,
                'sale_tables' => App\GraphQL\Queries\Sale\TablesQuery::class,
                'sale_invoices' => App\GraphQL\Queries\Sale\InvoicesQuery::class,
                'sale_menu' => App\GraphQL\Queries\Sale\SaleMenuQuery::class,
                'invoice_forms' => App\GraphQL\Queries\InvoiceFormsQuery::class,
                'returned_invoices' => App\GraphQL\Queries\ReturnedInvoicesQuery::class,
                'returned_invoice' => App\GraphQL\Queries\ReturnedInvoiceQuery::class,
                'requirements' => App\GraphQL\Queries\RequirementsQuery::class,
                'requirement' => App\GraphQL\Queries\RequirementQuery::class,

                //statistic
                'restaurant_overview' => App\GraphQL\Queries\Restaurant\Overview::class,
                'restaurant_revenue_report' => App\GraphQL\Queries\Restaurant\RevenueReport::class,
                'restaurant_revenue_chart' => App\GraphQL\Queries\Restaurant\RevenueChart::class,
                'restaurant_profit_report' => App\GraphQL\Queries\Restaurant\ProfitReport::class,
                'top_selling_goods' => App\GraphQL\Queries\Restaurant\TopSellingGoods::class,
                'goods_report' => App\GraphQL\Queries\Restaurant\GoodsReportQuery::class,

                // home page
                'restaurant' => App\GraphQL\Queries\FindAround\RestaurantQuery::class,
                'restaurants' => App\GraphQL\Queries\FindAround\RestaurantsQuery::class,
                'restaurant_images' => App\GraphQL\Queries\FindAround\RestaurantImagesQuery::class,
                'provinces' => App\GraphQL\Queries\FindAround\ProvincesQuery::class,
                'current_province' => App\GraphQL\Queries\FindAround\CurrentProvinceQuery::class,

                //search
                'search_groups' => App\GraphQL\Queries\Search\GroupsQuery::class,
                'search_restaurants' => App\GraphQL\Queries\Search\RestaurantsQuery::class,
                'search_users' => App\GraphQL\Queries\Search\UsersQuery::class,
                'sliders' => App\GraphQL\Queries\SlidersQuery::class,

                // user
                'user' => App\GraphQL\Queries\User\UserQuery::class,
                // 'users' => App\GraphQL\Queries\User\UsersQuery::class,
                'users_group' => App\GraphQL\Queries\User\UsersGroupQuery::class,
                'users_shift' => App\GraphQL\Queries\User\UsersShiftQuery::class,
                'users_role' => App\GraphQL\Queries\User\UsersRoleQuery::class,

                // personal
                'me' => App\GraphQL\Queries\Me\UserQuery::class,
                'my_invoices' => App\GraphQL\Queries\Me\MyInvoicesQuery::class,
                'my_timekeepings' => App\GraphQL\Queries\Me\MyTimekeepingsQuery::class,
                'my_timekeeping' => App\GraphQL\Queries\Me\MyTimekeepingQuery::class,
                'my_notifications' => App\GraphQL\Queries\NotificationsQuery::class,
                'my_requirements' => App\GraphQL\Queries\Me\RequirementsQuery::class,
                'count_requirement' => App\GraphQL\Queries\Me\CountRequirementQuery::class,

                // good images
                'good_images' => \App\GraphQL\Queries\GoodImagesQuery::class,

                'service_fees' => \App\GraphQL\Queries\Me\ServiceFeesQuery::class,
            ],
            'mutation' => [
                'create_group' => App\GraphQL\Mutations\Group\Create::class,
                'update_group' => App\GraphQL\Mutations\Group\Update::class,
                'add_group_acting_user' => App\GraphQL\Mutations\Group\OnActingUser::class,
                'remove_group_acting_user' => App\GraphQL\Mutations\Group\OffActingUser::class,
                'update_permission_role' => App\GraphQL\Mutations\Group\UpdatePermissionRole::class,
                'create_shift' => App\GraphQL\Mutations\Shift\Create::class,
                'update_shift' => App\GraphQL\Mutations\Shift\Update::class,
                'delete_shift' => App\GraphQL\Mutations\Shift\Delete::class,
                'checkin_timekeeping' => App\GraphQL\Mutations\Timekeeping\Checkin::class,
                'checkout_timekeeping' => App\GraphQL\Mutations\Timekeeping\Checkout::class,
                'create_place' => App\GraphQL\Mutations\Place\Create::class,
                'update_place' => App\GraphQL\Mutations\Place\Update::class,
                'delete_place' => App\GraphQL\Mutations\Place\Delete::class,
                'create_table' => App\GraphQL\Mutations\Table\Create::class,
                'update_table' => App\GraphQL\Mutations\Table\Update::class,
                'delete_table' => App\GraphQL\Mutations\Table\Delete::class,
                'create_group_menu' => App\GraphQL\Mutations\GroupMenu\Create::class,
                'update_group_menu' => App\GraphQL\Mutations\GroupMenu\Update::class,
                'delete_group_menu' => App\GraphQL\Mutations\GroupMenu\Delete::class,
                'create_good_property' => App\GraphQL\Mutations\Good\CreateProperty::class,
                'delete_good_property' => App\GraphQL\Mutations\Good\DeleteProperty::class,
                'update_good_units' => App\GraphQL\Mutations\Good\UpdateUnit::class,
                'delete_good_image' => App\GraphQL\Mutations\Good\DeleteImage::class,
                'update_good' => App\GraphQL\Mutations\Good\Update::class,
                'delete_good' => App\GraphQL\Mutations\Good\Delete::class,
                'delete_many_goods' => App\GraphQL\Mutations\Good\DeleteMany::class,
                'disable_good' => App\GraphQL\Mutations\Good\Disable::class,
                'disable_many_goods' => App\GraphQL\Mutations\Good\DisableMany::class,
                'enable_good' => App\GraphQL\Mutations\Good\Enable::class,
                'enable_many_goods' => App\GraphQL\Mutations\Good\EnableMany::class,
                'update_good_group_menu' => App\GraphQL\Mutations\Good\UpdateGroupMenu::class,
                'assign_sale_good' => App\GraphQL\Mutations\Good\AssignSaleGood::class,

                //Sale
                'create_invoice' => App\GraphQL\Mutations\Invoice\Create::class,
                'update_note_invoice' => App\GraphQL\Mutations\Sale\UpdateNote::class,
                'addition_good_invoice' => App\GraphQL\Mutations\Invoice\AdditionGood::class,
                'join_invoice' => App\GraphQL\Mutations\Invoice\JoinInvoice::class,
                'purchase_request_invoice' => App\GraphQL\Mutations\Invoice\RequestPurchase::class,
                'sale_accept_request' => App\GraphQL\Mutations\Sale\AcceptRequest::class,
                'update_goods_status' => App\GraphQL\Mutations\Sale\UpdateGoodsStatus::class,
                'sale_update_goods_discount' => App\GraphQL\Mutations\Sale\UpdateGoodsDiscount::class,
                'update_goods_count' => App\GraphQL\Mutations\Sale\UpdateItemCount::class,
                'sale_create_invoice' => App\GraphQL\Mutations\Sale\CreateInvoice::class,
                'sale_addition_good_invoice' => App\GraphQL\Mutations\Sale\AdditionGood::class,
                'sale_delete_goods_item' => App\GraphQL\Mutations\Sale\DeleteGoodsItem::class,
                'sale_inventory' => App\GraphQL\Mutations\Sale\Inventory::class,
                'sale_delete_invoice' => App\GraphQL\Mutations\Sale\DeleteInvoice::class,
                'update_invoice_discount' => App\GraphQL\Mutations\Sale\UpdateInvoiceDiscount::class,
                'async_goods_items_status' => App\GraphQL\Mutations\Sale\AsyncGoodsItemsStatus::class,
                'sale_change_table' => App\GraphQL\Mutations\Sale\ChangeTable::class,
                'merge_tables' => App\GraphQL\Mutations\Sale\MergeTables::class,
                'merge_invoices' => App\GraphQL\Mutations\Sale\MergeInvoices::class,
                'purchase_invoice' => App\GraphQL\Mutations\Sale\Purchase::class,
                'purchase_return_invoice' => App\GraphQL\Mutations\Sale\PurchaseReturnInvoice::class,
                'update_conversation_invoice' => App\GraphQL\Mutations\Invoice\UpdateConversation::class,

                // kitchen
                'update_processed_by_invoice' => \App\GraphQL\Mutations\Kitchen\UpdateProcessedByInvoice::class,
                'update_processed_by_goods' => \App\GraphQL\Mutations\Kitchen\UpdateProcessedByGoods::class,

                // requirements
                'addition_timekeeping' => App\GraphQL\Mutations\Timekeeping\AdditionTimekeeping::class,
                'take_leave' => App\GraphQL\Mutations\Timekeeping\TakeLeave::class,
                'switch_shift' => App\GraphQL\Mutations\Timekeeping\SwitchShift::class,
                'approve_addition_requirement' => App\GraphQL\Mutations\Requirement\ApproveAddition::class,
                'reject_addition_requirement' => App\GraphQL\Mutations\Requirement\RejectAddition::class,
                'approve_take_leave_requirement' => App\GraphQL\Mutations\Requirement\ApproveTakeLeave::class,
                'reject_take_leave_requirement' => App\GraphQL\Mutations\Requirement\RejectTakeLeave::class,

                //user
                'assign_group' => App\GraphQL\Mutations\User\AssignGroup::class,
                'remove_group' => App\GraphQL\Mutations\User\RemoveUserFromGroup::class,
                'assign_role' => App\GraphQL\Mutations\User\AssignRole::class,
                'update_role' => App\GraphQL\Mutations\User\UpdateRole::class,
                'revoke_role' => App\GraphQL\Mutations\User\RevokeRole::class,
                'assign_shift' => App\GraphQL\Mutations\User\AssignShift::class,
                'multi_assign_shift' => App\GraphQL\Mutations\User\MultiAssignShift::class,
                'remove_shift' => App\GraphQL\Mutations\User\RemoveShift::class,
                'leave_group' => App\GraphQL\Mutations\User\LeaveGroup::class,

                // Search
                'join_group_requirement' => App\GraphQL\Mutations\Requirement\JoinGroup::class,

                // Payment
                'pay_service_fee_with_momo_web' => App\GraphQL\Mutations\Payment\ServiceFee\MomoAllInOne::class,
                'pay_service_fee_with_momo_app' => App\GraphQL\Mutations\Payment\ServiceFee\MomoAppToApp::class,

                //personal
                'logout' => App\GraphQL\Mutations\Auth\LogoutMutation::class,
                'verify_phone_number' => App\GraphQL\Mutations\Auth\VerifyPhoneNumber::class,
                'change_password' => App\GraphQL\Mutations\Auth\ChangePassword::class,
                'update_firebase_token' => App\GraphQL\Mutations\User\UpdateFirebaseToken::class,
                'update_profile' => App\GraphQL\Mutations\User\UpdateProfile::class,
                'delete_notification' => App\GraphQL\Mutations\Notification\Delete::class,
                'mark_seen_notification' => App\GraphQL\Mutations\Notification\MarkSeen::class,
            ],
            'middleware' => ['graphql', 'throttle:300,1'],
            'method' => ['get', 'post'],
        ],
        'verified' => [
            'query' => [
                'group' => App\GraphQL\Queries\GroupQuery::class,
            ],
            'mutation' => [
                'create_group' => App\GraphQL\Mutations\Group\Create::class,
            ],
            'middleware' => [
                'graphql', 'verified',
                // 'phone_verified',
                'throttle:10,1'
            ],
            'method' => ['get', 'post']
        ],
        'guest' => [
            'mutation' => [
                'register' => App\GraphQL\Mutations\Auth\RegisterMutation::class,
                'login' => App\GraphQL\Mutations\Auth\LoginMutation::class,
                'oauth' => App\GraphQL\Mutations\Auth\OauthMutation::class,
                'forgot_password' => App\GraphQL\Mutations\Auth\ForgotPassword::class,
                'reset_password'  => App\GraphQL\Mutations\Auth\ResetPassword::class,
            ],
            'middleware' => ['throttle:10,1']
        ],
        'master' => [
            'query' => [
                'groups' => App\GraphQL\Queries\Master\GroupsQuery::class,
                'statistic_groups' => App\GraphQL\Queries\Master\GroupsReportQuery::class,
            ],
            'mutation' => [
                'store_good_image' => App\GraphQL\Mutations\GoodImage\Create::class,
                'add_good_images' => App\GraphQL\Mutations\GoodImage\AddGoodImages::class,
                'delete_good_image' => App\GraphQL\Mutations\GoodImage\DeleteImage::class,
                'delete_good_image_model' => App\GraphQL\Mutations\GoodImage\Delete::class,
                'update_care_status' => App\GraphQL\Mutations\Master\UpdateCareStatus::class,
                'disable_group' => App\GraphQL\Mutations\Master\DisableGroup::class,
            ],
            'middleware' => ['graphql', 'master', 'throttle:200,1'],
            'method' => ['get', 'post']
        ]
    ],

    // The types available in the application. You can then access it from the
    // facade like this: GraphQL::type('user')
    'types' => [
        'AreaInput' => App\GraphQL\Inputs\AreaInput::class,
        'CommuneInput' => App\GraphQL\Inputs\CommuneInput::class,
        'LocationInput' => App\GraphQL\Inputs\LocationInput::class,
        'PropertyInput' => App\GraphQL\Inputs\PropertyInput::class,
        'ConversionUnitInput' => App\GraphQL\Inputs\ConversionUnitInput::class,
        'IngredientGoodInput' => App\GraphQL\Inputs\IngredientGoodInput::class,
        'InvoiceGoodInput' => App\GraphQL\Inputs\InvoiceGoodInput::class,
        'InvoiceItemStatusInput' => App\GraphQL\Inputs\InvoiceItemStatusInput::class,
        'GoodsItemsInventory' => App\GraphQL\Inputs\GoodsItemsInventory::class,
        'PayServiceFeeInput' => App\GraphQL\Inputs\PayServiceFeeInput::class,
        // 'InvoiceNotificationCreator' => App\GraphQL\Types\Invoice\NotificationCreatorType::class,
        // 'InvoiceNotificationGoods' => App\GraphQL\Types\Invoice\NotificationGoodsType::class,
        // 'InvoiceNotificationItem' => App\GraphQL\Types\Invoice\NotificationItemType::class,

        'version' => App\GraphQL\Types\VersionType::class,
        'auth' => App\GraphQL\Types\AuthType::class,
        'user' => App\GraphQL\Types\UserType::class,
        'group' => App\GraphQL\Types\GroupType::class,
        'area' => App\GraphQL\Types\AreaType::class,
        'commune' => App\GraphQL\Types\CommuneType::class,
        'avatar' => App\GraphQL\Types\AvatarType::class,
        'role' => App\GraphQL\Types\RoleType::class,
        'permission' => App\GraphQL\Types\PermissionType::class,
        'response' => App\GraphQL\Types\ResponseType::class,
        'shift' => App\GraphQL\Types\ShiftType::class,
        'shift_time' => App\GraphQL\Types\ShiftTimeType::class,
        'location' => App\GraphQL\Types\LocationType::class,
        'timekeeping' => App\GraphQL\Types\TimekeepingType::class,
        'timekeeping_time_item' => App\GraphQL\Types\TimekeepingTimeItemType::class,
        'timekeeping_time' => App\GraphQL\Types\TimekeepingTimeType::class,
        'place' => App\GraphQL\Types\PlaceType::class,
        'table' => App\GraphQL\Types\TableType::class,
        'group_menu' => App\GraphQL\Types\GroupMenuType::class,
        'good_property' => App\GraphQL\Types\GoodPropertyType::class,
        'good_ingredient' => App\GraphQL\Types\GoodIngredientType::class,
        'good' => App\GraphQL\Types\GoodType::class,
        'invoice_good_item' => App\GraphQL\Types\InvoiceGoodItemType::class,
        'invoice_good' => App\GraphQL\Types\InvoiceGoodType::class,
        'invoice' => App\GraphQL\Types\InvoiceType::class,
        'geo_json' => App\GraphQL\Types\Search\GeoJsonType::class,
        'restaurant' => App\GraphQL\Types\Search\RestaurantType::class,
        'restaurant_image' => App\GraphQL\Types\Search\RestaurantImageType::class,
        'province' => App\GraphQL\Types\Search\ProvinceType::class,
        'image' => App\GraphQL\Types\Search\ImageType::class,
        'price_range' => App\GraphQL\Types\Search\PriceRangeType::class,
        'opening_time' => App\GraphQL\Types\Search\OpeningTimeType::class,
        'slider_image' => App\GraphQL\Types\SliderImageType::class,
        'slider' => App\GraphQL\Types\SliderType::class,
        'requirement' => App\GraphQL\Types\RequirementType::class,
        'notification' => App\GraphQL\Types\NotificationType::class,
        'returned_invoice' => App\GraphQL\Types\ReturnedInvoiceType::class,
        'invoice_form' => App\GraphQL\Types\InvoiceFormType::class,

        'restaurant_overview' => App\GraphQL\Types\Restaurant\Overview::class,
        'restaurant_sale_report' => App\GraphQL\Types\Restaurant\SaleReport::class,
        'top_selling_goods' => App\GraphQL\Types\Restaurant\TopSellingGoods::class,
        'goods_report' => App\GraphQL\Types\Restaurant\GoodsReport::class,
        'date_time_object' => App\GraphQL\Types\Report\DateTimeObject::class,
        'descartes' => App\GraphQL\Types\Report\Descartes::class,

        'good_image' => \App\GraphQL\Types\GoodImageType::class,

        'service_fee' => \App\GraphQL\Types\ServiceFeeType::class,
        'service_fee_transaction' => \App\GraphQL\Types\ServiceFeeTransactionType::class,

        'scalar' => App\GraphQL\Scalars\Scalar::class,
        \Rebing\GraphQL\Support\UploadType::class,
    ],

    // The types will be loaded on demand. Default is to load all types on each request
    // Can increase performance on schemes with many types
    // Presupposes the config type key to match the type class name property
    'lazyload_types' => true,

    // This callable will be passed the Error object for each errors GraphQL catch.
    // The method should return an array representing the error.
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    'error_formatter' => ['\Rebing\GraphQL\GraphQL', 'formatError'],

    /**
     * Custom Error Handling
     *
     * Expected handler signature is: function (array $errors, callable $formatter): array
     *
     * The default handler will pass exceptions to laravel Error Handling mechanism
     */
    'errors_handler' => ['\Rebing\GraphQL\GraphQL', 'handleErrors'],

    // You can set the key, which will be used to retrieve the dynamic variables
    'params_key'    => 'variables',

    /*
     * Options to limit the query complexity and depth. See the doc
     * @ https://github.com/webonyx/graphql-php#security
     * for details. Disabled by default.
     */
    'security' => [
        'query_max_complexity' => null,
        'query_max_depth' => null,
        'disable_introspection' => false
    ],

    'pagination_type' => \App\GraphQL\Pagination\PaginationType::class,

    // You can define custom paginators to override the out-of-the-box fields
    // Useful if you want to inject some parameters of your own that apply at the top
    // level of the collection rather than to each instance returned. Can also use this
    // to add in more of the Laravel pagination data (e.g. last_page).
    // 'custom_paginators' => [
    //     'user_pagination'  => App\GraphQL\Pagination\CustomPaginationFields::class,
    //     'group_pagination' => App\GraphQL\Pagination\CustomPaginationFields::class,
    // ],

    /*
     * Config for GraphiQL (see (https://github.com/graphql/graphiql).
     */
    'graphiql' => [
        'prefix' => '/graphiql',
        'controller' => \Rebing\GraphQL\GraphQLController::class . '@graphiql',
        'middleware' => [],
        'view' => 'graphql::graphiql',
        'display' => env('ENABLE_GRAPHIQL', true),
    ],

    /*
     * Overrides the default field resolver
     * See http://webonyx.github.io/graphql-php/data-fetching/#default-field-resolver
     *
     * Example:
     *
     * ```php
     * 'defaultFieldResolver' => function ($root, $args, $context, $info) {
     * },
     * ```
     * or
     * ```php
     * 'defaultFieldResolver' => [SomeKlass::class, 'someMethod'],
     * ```
     */
    'defaultFieldResolver' => null,
    /*
     * Any headers that will be added to the response returned by the default controller
     */
    'headers' => [],
    /*
     * Any JSON encoding options when returning a response from the default controller
     * See http://php.net/manual/function.json-encode.php for the full list of options
     */
    'json_encoding_options' => 0
];
