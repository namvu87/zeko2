<?php

return [
    'status'         => [
        'fail'    => 0,
        'success' => 1
    ],
    'payment_method' => [
        'cash'     => 1, // Tiền mặt
        'transfer' => 2, // Chuyển khoản
        'vnpay'    => 3, // Vnpay
        'momo'     => 4, // Momo
        'ib'       => 5, // Internet banking
    ]
];
