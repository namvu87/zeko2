<?php
return [
    'version' => [
        'android' => env('ANDROID_VERSION', '10000'),
        'ios' => env('IOS_VERSION', '10000')
    ]
];
