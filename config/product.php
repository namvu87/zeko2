<?php
return [
    'type' => [
        1 => 'Ứng dụng chấm công',
        2 => 'Ứng dụng nhà hàng'
    ],
    'price_type' => [
        11 => 'Loại giá cố định theo số lượng nhân viên tối đa (VD: 70k/max 50 người/tháng)',
        12 => 'Loại giá phụ thuộc số lượng nhân viên (VD: 30k/20 người/tháng)'
    ]
];