<?php
return [
    'server_key' => env('FIREBASE_SERVER_KEY'),
    'legacy_server_key' => env('FIREBASE_LEGACY_SERVER_KEY'),
    'url' => 'https://android.googleapis.com/gcm/send',
    'fcm_url' => 'https://fcm.googleapis.com/fcm/send',
    'fcm_notification_url' => 'https://fcm.googleapis.com/fcm/notification'
];