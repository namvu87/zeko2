<?php
return [
    100 => 'Gửi tin nhắn thành công',
    99 => 'Lỗi không xác định, thử lại sau',
    101 => 'Đăng nhập thất bại (api key hoặc secrect key không đúng)',
    102 => 'Tài khoản đã bị khóa',
    103 => 'Số dư tài khoản không đủ dể gửi tin',
    104 => 'Mã Brandname không đúng',
    'url' => 'http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_post',
    'to_mail' => 'phamvandung1508@gmail.com',
    'api_key' => env('ESMS_API_KEY'),
    'secret_key' => env('ESMS_SECRET_KEY')
];
