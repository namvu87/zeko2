module.exports = {
    root: true,
    env: {
        node: true,
        es6: true
    },
    'extends': [
        'plugin:vue/recommended'
    ],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'quotes': ['error', 'single'],
        'semi' : ['error', 'never'],
        'newline-per-chained-call': 'off',
        "indent": ["error", 4, {
            "SwitchCase": 1
        }],
        'linebreak-style': ['error', 'unix'],
        'max-len': 'off'
    },
    'overrides': [
        {
            'files': ['*.vue'],
            'rules': {
                'indent': 'off'
            }
        }
    ],
    parserOptions: {
        parser: 'babel-eslint',
        ecmaVersion: 6
    },
    globals: {
        'axios': true,
        'Echo': true,
        '_': true,
        '$': true,
        'CKEDITOR': true,
        'bus': true,
        'Vue': true
    },
}
