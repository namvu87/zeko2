FROM php:7.3-fpm-alpine

RUN apk add --no-cache --virtual .build-deps \
    wget \
    curl \
    git \
    grep \
    build-base \
    libmcrypt-dev \
    libxml2-dev \
    zlib-dev \
    autoconf \
    cyrus-sasl-dev \
    libgsasl-dev \
    libpng \
    libpng-dev \
    supervisor

RUN pecl install mongodb \
    &&  echo "extension=mongodb.so" > /usr/local/etc/php/mongodb.ini \
    && docker-php-ext-enable mongodb
RUN pecl install redis \
    && docker-php-ext-enable redis

RUN docker-php-ext-install mysqli pdo pdo_mysql tokenizer xml pcntl gd

RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

RUN composer global require "laravel/envoy=~1.0"