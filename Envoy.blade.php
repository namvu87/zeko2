@servers(['production' => 'deployer@207.148.125.20'])

@setup
    $repository = 'git@gitlab.com:zekoVN/web-2.0.git';
    $releases_dir = '/var/www/zeko_app/releases';
    $app_dir = '/var/www/zeko_app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy', ['on' => 'production'])
    clone_repository
    run_composer
    remove_old_releases
    update_symlinks
    restart_supervisor
    caching_config
    reload_php_fpm
@endstory

@task('clone_repository')
    echo 'Cloning respository ...'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo 'Starting deployment ({{ $release }})'
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('restart_supervisor')
    echo 'Restarting supervisor'
    supervisorctl restart all
@endtask

@task('caching_config')
    echo 'Caching config ...'
    cd {{ $app_dir }}/current
    php artisan config:clear
    php artisan config:cache
@endtask

@task('reload_php_fpm')
    sudo service php7.3-fpm restart
@endtask

@task('remove_old_releases')
    echo 'Removing old releases sub folder (only keep 3 newest folders)'
    purging=$(ls -dt {{ $releases_dir }}/* | tail -n +4)
    if [ "$purging" != "" ]
    then
        echo Purging old releases: $purging
        rm -rf $purging;
    else
        echo 'No releases found for purging at this time'
    fi
@endtask

@task('update_symlinks')
    echo Linking storage directory
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo Linking .env file
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    {{-- Must run after link .env file --}}
    echo 'npm running ...'
    cd {{ $new_release_dir }}
    npm install
    npm run production

    echo Linking current release from {{ $new_release_dir }} to {{ $app_dir }}/current
    ln -nfs {{ $new_release_dir }}/* {{ $app_dir }}/current
    ln -nfs {{ $app_dir }}/.env {{ $app_dir }}/current/.env
@endtask