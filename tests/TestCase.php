<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\MigrateDatabase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, MigrateDatabase;
}
