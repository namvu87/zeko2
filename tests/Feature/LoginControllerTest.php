<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use DesignMyNight\Mongodb\Passport\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class LoginControllerTest extends TestCase
{
    const LOGIN_API = '/api/login';
    const LOGOUT_API = '/api/logout';

    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'password' => bcrypt('12345678')
        ]);
        $client = Client::create([
            '_id' => config('passport.password_grant.client_id'),
            'secret' => config('passport.password_grant.secret'),
            'personal_access_client' => false,
            'password_client' => true,
            'user_id' => null,
            'name' => Str::random(100),
            'redirect' => 'http://localhost',
            'revoked' => false
        ]);
        Config::set('passport.password_grant.client_id', $client->id);
    }

    public function testLoginFailed()
    {
        $response = $this->json('POST', self::LOGIN_API, [
            'username' => $this->user->email,
            'password' => '12345677'
        ]);

        $response->assertStatus(401);
        $response->assertJson([
            'message' => __('login.message.auth_fail')
        ]);
    }

    public function testLoginSuccess()
    {
        $response = $this->json('POST', self::LOGIN_API, [
            'username' => $this->user->email,
            'password' => '12345678'
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'access_token',
            'user' => [
                'id',
                'first_name',
                'email',
                'phone_number'
            ]
        ]);
    }

    public function testLogout()
    {
        $response = $this->json('POST', self::LOGIN_API, [
            'username' => $this->user->email,
            'password' => '12345678'
        ])->decodeResponseJson();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $response['access_token']
        ])->json('POST', self::LOGOUT_API, []);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'ok'
        ]);
    }
}
