<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use DesignMyNight\Mongodb\Passport\Client;
use DesignMyNight\Mongodb\Passport\PersonalAccessClient;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;

class RegisterControllerTest extends TestCase
{
    use WithFaker;

    const REGISTER_API = '/api/register';

    public function setUp(): void
    {
        parent::setUp();
        $this->setUpFaker();

        $client = Client::create([
            'secret' => config('passport.personal_access.secret'),
            'personal_access_client' => true,
            'password_client' => false,
            'user_id' => null,
            'name' => Str::random(100),
            'redirect' => 'http://localhost',
            'revoked' => false
        ]);
        PersonalAccessClient::create(['client_id' => $client->id]);

        Passport::personalAccessClientId($client->id);
    }

    public function testRegistInvalid()
    {
        $response = $this->json('POST', self::REGISTER_API, [
            'first_name' => 'Foo',
            'last_name' => 'Bar',
            'email' => $this->faker->unique()->safeEmail,
            'password' => '1234',
            'password_confirmation' => '1234'
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.'
        ]);
    }

    public function testRegistSuccess()
    {
        $response = $this->json('POST', self::REGISTER_API, [
            'first_name' => 'Foo',
            'last_name' => 'Bar',
            'email' => $this->faker->unique()->safeEmail,
            'phone_number' => $this->faker->regexify('[0-9]{10}'),
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'access_token',
            'user' => [
                'id',
                'first_name',
                'email',
                'phone_number'
            ]
        ]);
    }
}
