<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Shift;
use App\Models\Group;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;

class TimekeepingTest extends TestCase
{
    const CHECKIN_API = '/api/timekeeping/checkin';
    const CHECKOUT_API = '/api/timekeeping/checkout';

    private $group;
    private $shift;
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->group = factory(Group::class)->withoutEvents()->create();
        $this->shift = factory(Shift::class)->create([
            'group_id' => $this->group->id,
            'times' => [
                [
                    'checkin' => Carbon::now()->subMinutes(1)->format('H:i'),
                    'checkout' => Carbon::now()->addMinutes(1)->format('H:i')
                ]
            ]
        ]);
        $this->user = factory(User::class)->create();
    }

    /**
     * Test chưa được thêm vào nhóm
     *
     * @return void
     */
    public function testUserWithoutGroup()
    {
        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::CHECKIN_API, [
                'group_id' => $this->group->id,
                'location' => ['langtutide' => 100]
            ]);
        $response->assertStatus(403);
        $response->assertJson([
            'message' => __('auth.you_are_not_in_group')
        ]);

        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::CHECKOUT_API, [
                'group_id' => $this->group->id,
                'location' => ['langtutide' => 100]
            ]);
        $response->assertStatus(403);
        $response->assertJson([
            'message' => __('auth.you_are_not_in_group')
        ]);
    }

    /**
     * Kiểm tra user không có ca làm việc trong nhóm
     *
     * @return void
     */
    public function testUserWithoutShift()
    {
        $this->user->group_ids = [$this->group->id];
        $this->user->save();
        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::CHECKIN_API, [
                'group_id' => $this->group->id,
                'location' => ['langtutide' => 100]
            ]);

        $response->assertStatus(403);
        $response->assertJson([
            'message' => __('company.shift.not_exists_shift')
        ]);

        $this->user->group_ids = [$this->group->id];
        $this->user->save();
        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::CHECKOUT_API, [
                'group_id' => $this->group->id,
                'location' => ['langtutide' => 100]
            ]);

        $response->assertStatus(403);
        $response->assertJson([
            'message' => __('company.shift.not_exists_shift')
        ]);
    }

    public function testUploadImage()
    {
        $file = UploadedFile::fake()->image('avatar.png');

        $this->user->group_ids = [$this->group->id];
        $this->user->shift_ids = [$this->shift->id];
        $this->user->save();

        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::CHECKIN_API, [
                'group_id' => $this->group->id,
                'location' => json_encode(['langtutide' => 100]),
                'image' => $file
            ]);

        $response->assertStatus(200);
        $response->assertJson([
            'code' => 200
        ]);

        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::CHECKOUT_API, [
                'group_id' => $this->group->id,
                'location' => json_encode(['langtutide' => 100]),
                'image' => $file
            ]);
        $response->assertStatus(200);
        $response->assertJson([
            'code' => 200
        ]);
    }
}
