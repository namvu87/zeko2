<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Shift;
use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Eloquent\FactoryBuilder;
use App\Http\Controllers\Company\CheckinController;

class CheckinTest extends TestCase
{
    private $group;
    private $shift;
    private $user;

    private $checkinController;

    public function setUp(): void
    {
        parent::setUp();

        $this->checkinController = new CheckinController(
            $this->app->make('App\Contracts\UserContract'),
            $this->app->make('App\Contracts\ShiftContract'),
            $this->app->make('App\Contracts\TimekeepingContract')
        );

        FactoryBuilder::macro('withoutEvents', function () {
            $this->class::flushEventListeners();
            return $this;
        });

        $this->group = factory(Group::class)->withoutEvents()->create();
        $this->shift = factory(Shift::class)->create([
            'group_id' => $this->group->id,
        ]);
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCheckinSingleScanShift()
    {
        $this->user->group_ids = [$this->group->id];
        $this->user->shift_ids = [$this->shift->id];
        $this->user->save();

        $time = [
            'time'      => '17:01',
            'location'  => ['langtutide' => 100]
        ];

        try {
            $this->checkinController->create([], $time, $this->shift, null);
            $this->assertTrue(false);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), __('time_keeping.over_time_checkin'));
        }
    }

    public function testOverTimeCheckinMultiScanShift()
    {
        $this->user->group_ids = [$this->group->id];

        $shift = factory(\App\Models\Shift::class)->create([
            'times' => [
                [
                    'checkin' => '08:00',
                    'checkout' => '12:00'
                ],
                [
                    'checkin' => '13:00',
                    'checkout' => '17:00'
                ]
            ]
        ]);
        $this->user->shift_ids = [$shift->id];
        $this->user->save();

        $time = [
            'time'      => '17:01',
            'location'  => ['langtutide' => 100]
        ];

        try {
            $this->checkinController->create([], $time, $shift, null);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), __('time_keeping.over_time_checkin'));
        }
    }

    public function testCreateCheckinSuccess()
    {
        $this->user->group_ids = [$this->group->id];
        $this->user->shift_ids = [$this->shift->id];
        $this->user->save();
        $time = [
            'time'      => '08:01',
            'location'  => ['langtutide' => 100]
        ];
        $timekeeping = $this->checkinController->create([], $time, $this->shift, null);
        $this->assertEquals($timekeeping->times[0]['checkin']['time'], $time['time']);
    }

    public function testSingleCheckinExisted()
    {
        $this->user->group_ids = [$this->group->id];
        $this->user->shift_ids = [$this->shift->id];
        $this->user->save();

        $time = [
            'time'      => '08:01',
            'location'  => ['langtutide' => 100]
        ];
        $timekeeping = $this->checkinController->create([], $time, $this->shift, null);
        try {
            $this->checkinController->create([], $time, $this->shift, $timekeeping);
            $this->assertTrue(false);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), __('time_keeping.already_checkin'));
        }
    }

    public function testMultiCheckinExisted()
    {
        $this->user->group_ids = [$this->group->id];

        $shift = factory(\App\Models\Shift::class)->create([
            'times' => [
                [
                    'checkin' => '08:00',
                    'checkout' => '12:00'
                ],
                [
                    'checkin' => '13:00',
                    'checkout' => '17:00'
                ]
            ]
        ]);
        $this->user->shift_ids = [$shift->id];
        $this->user->save();

        $time = [
            'time'      => '07:50',
            'location'  => ['langtutide' => 100]
        ];
        $timekeeping = $this->checkinController->create([], $time, $this->shift, null);
        try {
            $this->checkinController->create([], $time, $this->shift, $timekeeping);
            $this->assertTrue(false);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), __('time_keeping.already_checkin'));
        }

        $time = [
            'time'      => '12:50',
            'location'  => ['langtutide' => 100]
        ];
        $timekeeping = $this->checkinController->create([], $time, $this->shift, null);
        try {
            $this->checkinController->create([], $time, $this->shift, $timekeeping);
            $this->assertTrue(false);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), __('time_keeping.already_checkin'));
        }
    }
}
