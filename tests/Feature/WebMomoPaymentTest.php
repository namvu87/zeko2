<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\ServiceFee;
use App\Models\ServiceFeeTransaction;
use App\Models\Group;
use App\Models\User;
use App\Http\Controllers\Payment\WebMomoController;
use App\Traits\Payment\MomoHashes;
use Faker\Factory;
use Illuminate\Support\Arr;

class WebMomoPaymentTest extends TestCase
{
    use MomoHashes;

    const API = '/api/payment/momo/web-callback';

    private $serviceFee1;
    private $serviceFee2;
    private $serviceFee3;
    private $transaction;
    private $payload;
    private $controller;
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->controller = new WebMomoController(
            $this->app->make('App\Contracts\ServiceFeeContract'),
            $this->app->make('App\Contracts\ServiceFeeTransactionContract'),
            $this->app->make('App\Contracts\NotificationContract')
        );
        $this->user = factory(User::class)->create();

        $this->serviceFee1 = factory(ServiceFee::class)->create([
            'group_id' => factory(Group::class)->state('timekeeping')->withoutEvents()->create()->id
        ]);
        $this->serviceFee2 = factory(ServiceFee::class)->create([
            'group_id' => factory(Group::class)->state('restaurant')->withoutEvents()->create()->id
        ]);
        $this->serviceFee3 = factory(ServiceFee::class)->create([
            'group_id' => factory(Group::class)->state('restaurant')->withoutEvents()->create()->id
        ]);
        $this->transaction = factory(ServiceFeeTransaction::class)->create([
            'user_id' => $this->user->id,
            'service_fees' => [
                [
                    'service_fee_id' => $this->serviceFee1->id
                ], [
                    'service_fee_id' => $this->serviceFee2->id,
                    'package' => 3
                ], [
                    'service_fee_id' => $this->serviceFee3->id,
                    'package' => 3
                ]
            ]
        ]);

        $faker = Factory::create();
        $this->payload = [
            'partnerCode' => $faker->regexify('[A-Za-z0-9]{20}'),
            'accessKey' => $faker->regexify('[A-Za-z0-9]{256}'),
            'requestId' => $faker->regexify('[A-Za-z0-9]{11}'),
            'amount' => $faker->numberBetween(10000, 1000000),
            'orderId' => $this->transaction->id,
            'orderInfo' => $faker->regexify('[A-Za-z0-9]{128}'),
            'orderType' => 'momo_wallet',
            'transId' => $faker->regexify('[A-Za-z0-9]{11}'),
            'message' => 'Succesful',
            'localMessage' => 'Thành công',
            'responseTime' => date('Y-m-d H:i:s'),
            'errorCode' => 0,
            'payType' => 'web',
            'extraData' => NULL
        ];
        $this->payload['signature'] = $this->generateSignature($this->payload);
    }

    public function testInvalidSignature()
    {
        $this->payload['signature'] = Factory::create()->sha256;
        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::API, $this->payload);

        $response->assertStatus(403);
        $response->assertJson([
            'message' => 'Chữ ký điện tử không hợp lệ'
        ]);
    }

    public function testPaymentFailed()
    {
        $this->payload['errorCode'] = 1;
        $this->payload['signature'] = $this->generateSignature(Arr::except($this->payload, 'signature'));

        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::API, $this->payload);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Thanh toán không hoàn tất'
        ]);
    }

    public function testPaymentSuccess()
    {
        $response = $this->actingAs($this->user, 'api')
            ->json('POST', self::API, $this->payload);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Thanh toán thành công'
        ]);
    }
}
