<?php

namespace Tests;

trait MigrateDatabase
{
    protected static $migrated = false;

    public function setUp(): void
    {
        parent::setUp();

        if (!static::$migrated) {
            $this->artisan('migrate:refresh');
            $this->artisan('passport:keys');
            static::$migrated = true;
        }
    }
}
