/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
  // config.extraPlugins = 'xml,filetools,ajax,cloudservices';
  // config.cloudServices_tokenUrl = 'https://example.com/cs-token-endpoint';
  // config.cloudServices_uploadUrl = 'https://your-organization-id.cke-cs.com/easyimage/upload/';
  // config.extraPlugins   = 'easyimage';
  config.removePlugins  = 'easyimage'; 
  config.extraPlugins   = 'popup'; 
  config.extraPlugins   = 'filebrowser';
};
