/*
Theme Name: PHAN TRONG BIEN
Author: PHAN TRONG BIEN
Author URL: phantrongbien.com
*/
$(document).ready(function() {
	/*
	MENU RESPONSIVE
	*/
	var menu     = document.getElementsByClassName("menu-responsive")[0];
	var menuBtn  = document.getElementsByClassName("menuBtn")[0];
	var closeBtn = document.getElementsByClassName("hideBtn")[0];

	menuBtn.addEventListener("click",openMenu);

	function openMenu()
	{
	  menu.classList.add("menuOpened");
	  menuBtn.classList.add("hideBtn");
	  closeBtn.classList.add("closeBtn");
	}

	closeBtn.addEventListener("click",hideMenu);

	function hideMenu()
	{
	  menu.classList.remove("menuOpened");
	  menuBtn.classList.remove("hideBtn");
	  closeBtn.classList.remove("closeBtn");
	}

	// CHECKBOX SHOW PAYMENT METHOD
	$('.payment-method .payment-method__items.active input[type="radio"]').attr('checked', 'checked');
	$('.payment-method .payment-method__items.active .box-select-payment').css('display', 'block');

	$('.payment-method .payment-method__items input[type="radio"]').click(function () {
		$('.box-select-payment').hide();
		$(this).parent().parent().parent().find('.box-select-payment').show();
	});

	$('.region-info .check-guest.active input[type="radio"]').attr('checked', 'checked');
	$('.region-info .check-guest input[type="radio"]').click(function () {
		$('.company-input').toggle();
		$('.personal-input').toggle();
	});

	// Datetimepicker
	$('#start-date').datepicker({
    	format : 'dd/mm/yyyy',
      todayBtn: false,
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      },
  });

	$('#end-date').datepicker({
    	format : 'dd/mm/yyyy',
      todayBtn: false,
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      },
  });

});
