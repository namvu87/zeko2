require('dotenv').config()
const Echo = require('laravel-echo-server')

var config = {
    'authHost': process.env.ECHO_AUTH_HOST,
    'authEndpoint': process.env.ECHO_AUTH_ENDPOINT,
    'clients': [
        {
            'appId': '1f8d07f5f0f0e09d',
            'key': '3ec1fc9f7da238091b8ee7f2d13a6318'
        }
    ],
    'database': process.env.ECHO_DATABASE,
    'databaseConfig': {
        'redis': {},
        'sqlite': {
            'databasePath': '/database/laravel-echo-server.sqlite'
        }
    },
    'devMode': process.env.ECHO_DEBUG,
    'host': process.env.ECHO_HOST,
    'port': process.env.ECHO_PORT,
    'protocol': process.env.ECHO_PROTOCOL,
    'socketio': {},
    'sslCertPath': process.env.ECHO_SSL_CERT_PATH,
    'sslKeyPath': process.env.ECHO_SSL_KEY_PATH,
    'sslCertChainPath': '',
    'sslPassphrase': '',
    'subscribers': {
        'http': true,
        'redis': true
    },
    'apiOriginAllow': {
        'allowCors': true,
        'allowOrigin': process.env.ECHO_ALLOW_ORIGIN,
        'allowMethods': 'GET, POST',
        'allowHeaders': 'Origin, Content-Type, X-Auth-Token, X-Requested-With, Accept, Authorization, X-CSRF-TOKEN, X-Socket-Id'
    }
}

Echo.run(config)
